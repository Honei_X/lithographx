# LithoGraphX #

LithoGraphX is a software to visualize, process and analyze 3D images and meshes.

You might be interested in:

- The main website: http://www.lithographx.org
- Windows installer: https://bitbucket.org/PierreBdR/lithographx/downloads/LithoGraphX-Windows-MinGW64-1.1.0-Extra.exe
- Ubuntu Linux packages: https://code.launchpad.net/~pierre-barbierdereuille/+archive/ubuntu/ppa
- Online documentation: http://lithographx.readthedocs.org/

LithoGraphX is a fork of MorphoGraphX v1.0 rev 1256, and is being developed by
Pierre Barbier de Reuille at the University of Bern.
