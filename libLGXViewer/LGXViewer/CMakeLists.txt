########################################################
# LibQLGViewer

option(NO_VECTORIAL_RENDER "If true, vectorial rendering is not compiled." false)

if (NOT NO_VECTORIAL_RENDER)
  qt5_wrap_ui(QGLVRender_UIS_H "VRenderInterface.ui")
  set(QGLVRender_SRCS
    VRender/BackFaceCullingOptimizer.cpp
    VRender/BSPSortMethod.cpp
    VRender/EPSExporter.cpp
    VRender/Exporter.cpp
    VRender/FIGExporter.cpp
    VRender/gpc.cpp
    VRender/ParserGL.cpp
    VRender/Primitive.cpp
    VRender/PrimitivePositioning.cpp
    VRender/TopologicalSortMethod.cpp
    VRender/VisibilityOptimizer.cpp
    VRender/Vector2.cpp
    VRender/Vector3.cpp
    VRender/NVector3.cpp
    VRender/VRender.cpp
    ${QGLVRender_UIS_H}
    )

  set(QGLVRender_VRender_HEADERS
    VRender/AxisAlignedBox.h
    VRender/Exporter.h
    VRender/gpc.h
    VRender/NVector3.h
    VRender/Optimizer.h
    VRender/ParserGL.h
    VRender/Primitive.h
    VRender/PrimitivePositioning.h
    VRender/SortMethod.h
    VRender/Types.h
    VRender/Vector2.h
    VRender/Vector3.h
    VRender/VRender.h
    )
else(NOT NO_VECTORIAL_RENDER)
  set(QGLVRender_SRCS "")
endif (NOT NO_VECTORIAL_RENDER)

qt5_wrap_ui(LGXViewer_UIS_H "ImageInterface.ui")

set(LGXViewer_SRCS
  qglviewer.cpp
  camera.cpp
  manipulatedFrame.cpp
  manipulatedCameraFrame.cpp
  frame.cpp
  saveSnapshot.cpp
  constraint.cpp
  keyFrameInterpolator.cpp
  mouseGrabber.cpp
  quaternion.cpp
  vec.cpp
  )

set(LGXViewer_HEADERS
  keyFrameInterpolator.h
  frame.h
  camera.h
  qglviewer.h
  manipulatedFrame.h
  manipulatedCameraFrame.h
  ImageInterface.h
  camera.h
  config.h
  constraint.h
  domUtils.h
  frame.h
  keyFrameInterpolator.h
  manipulatedCameraFrame.h
  manipulatedFrame.h
  mouseGrabber.h
  qglviewer.h
  quaternion.h
  vec.h
  ${LGXViewer_UIS_H}
  )

add_library(LGXViewer SHARED ${LGXViewer_SRCS} ${QGLVRender_SRCS} ${LGXViewer_HEADERS})

target_link_libraries(LGXViewer ${GLUT_LIBRARIES})
target_link_libraries(LGXViewer ${OPENGL_gl_LIBRARY} ${OPENGL_glu_LIBRARY})
target_link_libraries(LGXViewer Qt5::Core Qt5::Widgets Qt5::OpenGL Qt5::Xml)

set_property(TARGET LGXViewer PROPERTY PUBLIC_HEADER  ${LGXViewer_HEADERS} ${QGLVRender_UIS_H})

if(WIN32)
  set_target_properties(LGXViewer PROPERTIES DEFINE_SYMBOL CREATE_QGLVIEWER_DLL)
endif()
install(TARGETS LGXViewer EXPORT LithoGraphX
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
  PUBLIC_HEADER DESTINATION include/LithoGraphX/LGXViewer)

if(MSVC)
  set_target_properties(LGXViewer PROPERTIES LINK_FLAGS "/NODEFAULTLIB:\"LIBCMT.lib\" /NODEFAULTLIB:\"libcpmt.lib\"")
endif()

if(NOT NO_VECTORIAL_RENDER)
  install(FILES ${QGLVRender_VRender_HEADERS} DESTINATION include/LithoGraphX/LGXViewer/VRender)
endif()

