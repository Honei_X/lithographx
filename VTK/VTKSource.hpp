/*
 * This file is part of MorphoGraphX
 *
 * MorphoGraphX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MorphoGraphX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MorphoGraphX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef VTKSOURCE_HPP
#define VTKSOURCE_HPP

#include <VTKProcess.hpp>

namespace process {
class VTKImageReader : public StackProcess {
};
}

#endif // VTKSOURCE_HPP
