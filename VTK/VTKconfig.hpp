#ifndef VTKCONFIG_HPP
#define VTKCONFIG_HPP

#include <LGXConfig.hpp>

#if defined(WIN32) || defined(WIN64)

#  ifdef lgxVTKutil_EXPORTS
#    define vtkutil_EXPORT __declspec(dllexport)
#  else
#    define vtkutil_EXPORT __declspec(dllimport)
#  endif

#  ifdef lgxVTK_EXPORTS
#    define vtk_EXPORT __declspec(dllexport)
#  else
#    define vtk_EXPORT __declspec(dllimport)
#  endif

#else

#  define vtkutil_EXPORT
#  define vtk_EXPORT

#endif

#endif // VTKCONFIG_HPP
