#include "VTKProcess.hpp"
#include <vtkPolyData.h>
#include <vtkCellArray.h>

#include <Store.hpp>
#include <Stack.hpp>

namespace lgx {
namespace process {
VTKImageConverter::VTKImageConverter()
  : vtkImageImport()
  , store(0)
{
}

VTKImageConverter::~VTKImageConverter() {
}

void VTKImageConverter::SetStore(const Store* s)
{
  store = s;
  const Stack* stack = s->stack();
  Point3u size = stack->size();
  Point3f step = stack->step();
  Point3f origin = stack->origin();
  this->SetDataScalarTypeToUnsignedShort();
  this->SetNumberOfScalarComponents(1);
  this->SetDataExtent(0, size.x() - 1, 0, size.y() - 1, 0, size.z() - 1);
  this->SetWholeExtent(0, size.x() - 1, 0, size.y() - 1, 0, size.z() - 1);
  this->SetDataSpacing(step.x(), step.y(), step.z());
  this->SetDataOrigin(origin.x(), origin.y(), origin.z());
  this->SetImportVoidPointer((void*)(&(s->data()[0])));
}

VTKImageConverter* VTKImageConverter::New() {
  return new VTKImageConverter;
}
}
}
