/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef ITKSINK_HPP
#define ITKSINK_HPP

#include <ITKConfig.hpp>
#include <ITKProcess.hpp>

#include <Stack.hpp>
#include <Store.hpp>

namespace lgx {
namespace process {
class itk_EXPORT ITKVTKWriter : public StackProcess {
public:
  ITKVTKWriter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    bool res = (*this)(input, parms[0]);
    return res;
  }

  bool operator()(const Store* input, QString filename);

  QString name() const {
    return "ITK VTK Writer";
  }
  QString folder() const {
    return "ITK/System";
  }
  QString description() const {
    return "Write a VTK Image File";
  }
  QStringList parmNames() const {
    return QStringList() << "Filename";
  }
  QStringList parmDefaults() const {
    return QStringList() << "";
  }
  QIcon icon() const {
    return QIcon(":/images/save.png");
  }
};
}
}

#endif // ITKSINK_HPP
