/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef ITKSEGMENTATION_HPP
#define ITKSEGMENTATION_HPP

#include <ITKConfig.hpp>
#include <ITKProcess.hpp>

#include <Stack.hpp>
#include <Store.hpp>

namespace lgx {
namespace process {
class itk_EXPORT ITKWatershed : public StackProcess {
public:
  ITKWatershed(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_MAIN | STORE_VISIBLE | STORE_NON_LABEL).store(STORE_WORK | STORE_VISIBLE
                                                                               | STORE_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* main = stack->main();
    Store* labels = stack->work();
    bool connect8 = stringToBool(parms[0]);
    bool markLine = stringToBool(parms[1]);
    return (*this)(main, labels, connect8, markLine);
  }

  bool operator()(const Store* image, Store* labels, bool connect8, bool markLine);

  QString name() const {
    return "ITK Watershed";
  }
  QString folder() const {
    return "ITK/Segmentation";
  }
  QString description() const {
    return "Watershed segmentation";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Fully connected"
                         << "Mark Watershed Line";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Fully connected"
                         << "Mark Watershed Line";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "No"
                         << "No";
  }
  ParmChoiceMap stringChoice() const
  {
    ParmChoiceMap map;
    map[0] = map[1] = booleanChoice();
    return map;
  }
  QIcon icon() const {
    return QIcon(":/images/SegmentMesh.png");
  }
};

class itk_EXPORT ITKWatershedAutoSeeded : public StackProcess {
public:
  ITKWatershedAutoSeeded(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_VISIBLE | STORE_NON_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* main = stack->currentStore();
    Store* labels = stack->work();
    bool connect8 = stringToBool(parms[0]);
    bool markLine = stringToBool(parms[1]);
    if((*this)(stack, main, labels, parms[2].toUInt(), connect8, markLine)) {
      main->hide();
      labels->show();
      return true;
    }
    return false;
  }

  bool operator()(Stack* stack, const Store* image, Store* labels, ushort level, bool connect8, bool markLine);

  QString name() const {
    return "ITK Watershed Auto Seeded";
  }
  QString folder() const {
    return "ITK/Segmentation";
  }
  QString description() const {
    return "Auto Seeded Watershed segmentation";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Fully connected"
                         << "Mark Watershed Line"
                         << "Level";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "No"
                         << "No"
                         << "1500";
  }
  ParmChoiceMap stringChoice() const
  {
    ParmChoiceMap map;
    map[0] = map[1] = booleanChoice();
    return map;
  }
  QIcon icon() const {
    return QIcon(":/images/SegmentMesh.png");
  }
};
}
}

#endif // ITKSEGMENTATION_HPP
