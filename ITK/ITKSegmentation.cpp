/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ITKProgress.hpp"

#include "ITKSegmentation.hpp"

#include <itkMorphologicalWatershedFromMarkersImageFilter.h>
#include <itkMorphologicalWatershedImageFilter.h>

namespace lgx {
namespace process {
bool ITKWatershed::operator()(const Store* image, Store* labels, bool connect8, bool markLine)
{
  UImageConverter::Pointer converterImage = UImageConverter::New();
  UImageConverter::Pointer converterLabel = UImageConverter::New();

  converterImage->SetStore(image);
  converterLabel->SetStore(labels);

  typedef itk::MorphologicalWatershedFromMarkersImageFilter<UImageType, UImageType> FilterType;

  FilterType::Pointer filter = FilterType::New();

  filter->SetFullyConnected(connect8);
  filter->SetMarkWatershedLine(markLine);
  filter->SetInput1(converterImage->GetOutput());
  filter->SetInput2(converterLabel->GetOutput());

  ITKProgress progress("Seeded Watershed");
  progress.setFilter(filter);

  filter->Update();

  UImageType::ConstPointer result = filter->GetOutput();

  if(!UImageConverter::TransferImage(labels->stack(), labels, result)) {
    setErrorMessage("ITK Process tried to change the size of the stack");
    return false;
  }

  labels->changed();

  return true;
}

REGISTER_STACK_PROCESS(ITKWatershed);

bool ITKWatershedAutoSeeded::operator()(Stack* stack, const Store* image, Store* labels, ushort level, bool connect8,
                                        bool markLine)
{
  UImageConverter::Pointer converterImage = UImageConverter::New();
  UImageConverter::Pointer converterLabel = UImageConverter::New();

  converterImage->SetStore(image);
  converterLabel->SetStore(labels);

  typedef itk::MorphologicalWatershedImageFilter<UImageType, UImageType> FilterType;

  FilterType::Pointer filter = FilterType::New();

  filter->SetLevel(level);
  filter->SetFullyConnected(connect8);
  filter->SetMarkWatershedLine(markLine);
  filter->SetInput(converterImage->GetOutput());

  ITKProgress progress("Auto-Seeded Watershed");
  progress.setFilter(filter);

  filter->Update();
  if(filter->GetAbortGenerateData())
    return false;

  UImageType::ConstPointer result = filter->GetOutput();

  if(!UImageConverter::TransferImage(labels->stack(), labels, result)) {
    setErrorMessage("ITK Process tried to change the size of the stack");
    return false;
  }

  labels->setLabels(true);

  setCurrentStackId(stack->id());
  labels->show();

  if(!runProcess("Stack", "Relabel", {"1", "1"}))
    return false;

  labels->changed();

  return true;
}

REGISTER_STACK_PROCESS(ITKWatershedAutoSeeded);
}
}
