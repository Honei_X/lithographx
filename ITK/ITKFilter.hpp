/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef ITKFILTER_HPP
#define ITKFILTER_HPP

#include <ITKConfig.hpp>
#include <ITKProcess.hpp>

#include <Stack.hpp>
#include <Store.hpp>
#include <Misc.hpp>

namespace lgx {
namespace process {
class itk_EXPORT ITKDiscreteGaussianImageFilter : public StackProcess {
public:
  ITKDiscreteGaussianImageFilter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool ok;
    float x = parms[0].toFloat(&ok);
    if(!ok)
        return setErrorMessage("X parameter must be a number");
    float y = parms[1].toFloat(&ok);
    if(!ok)
        return setErrorMessage("Y parameter must be a number");
    float z = parms[2].toFloat(&ok);
    if(!ok)
        return setErrorMessage("Z parameter must be a number");
    Point3f sigma(x, y, z);
    bool res = (*this)(input, output, sigma);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, const Point3f& sigma);

  QString name() const {
    return "ITK Discrete Gaussian Image Filter";
  }
  QString folder() const {
    return "ITK/Filters";
  }
  QString description() const {
    return "Discrete Gaussian Blur. Simply perform the convolution explicitly.";
  }
  QStringList parmNames() const {
    return QStringList() << QString("X Sigma (%1)").arg(UM)
                         << QString("Y Sigma (%1)").arg(UM)
                         << QString("Z Sigma (%1)").arg(UM);
  }
  QStringList parmDescs() const {
    return QStringList() << QString("X Sigma (%1)").arg(UM)
                         << QString("Y Sigma (%1)").arg(UM)
                         << QString("Z Sigma (%1)").arg(UM);
  }
  QStringList parmDefaults() const {
    return QStringList() << "1.0" << "1.0" << "1.0";
  }
  QIcon icon() const {
    return QIcon(":/images/Blur.png");
  }
};

class itk_EXPORT ITKSmoothingRecursiveGaussianImageFilter : public StackProcess {
public:
  ITKSmoothingRecursiveGaussianImageFilter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool ok;
    float x = parms[0].toFloat(&ok);
    if(!ok)
        return setErrorMessage("X parameter must be a number");
    float y = parms[1].toFloat(&ok);
    if(!ok)
        return setErrorMessage("Y parameter must be a number");
    float z = parms[2].toFloat(&ok);
    if(!ok)
        return setErrorMessage("Z parameter must be a number");
    Point3f sigma(x, y, z);
    bool res = (*this)(input, output, sigma);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, const Point3f& sigma);

  QString name() const {
    return "ITK Smoothing Recursive Gaussian Image Filter";
  }
  QString folder() const {
    return "ITK/Filters";
  }
  QString description() const {
    return "Smoothing Recursive Gaussian Blur";
  }
  QStringList parmNames() const {
    return QStringList() << QString("X Sigma (%1)").arg(UM)
                         << QString("Y Sigma (%1)").arg(UM)
                         << QString("Z Sigma (%1)").arg(UM);
  }
  QStringList parmDescs() const {
    return QStringList() << QString("X Sigma (%1)").arg(UM)
                         << QString("Y Sigma (%1)").arg(UM)
                         << QString("Z Sigma (%1)").arg(UM);
  }
  QStringList parmDefaults() const {
    return QStringList() << "1.0" << "1.0" << "1.0";
  }
  QIcon icon() const {
    return QIcon(":/images/Blur.png");
  }
};

class itk_EXPORT ITKCurvatureFlowImageFilter : public StackProcess {
public:
  ITKCurvatureFlowImageFilter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(input, output, parms[0].toFloat(), parms[1].toInt());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, float timestep, int steps);

  QString name() const {
    return "ITK Curvature Flow Image Filter";
  }
  QString folder() const {
    return "ITK/Filters";
  }
  QString description() const {
    return "Curvature Flow Edge Preserving Smoothing";
  }
  QStringList parmNames() const
  {
    return QStringList() << "TimeStep"
                         << "Steps";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "TimeStep"
                         << "Steps";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << ".0.0625"
                         << "10";
  }
  QIcon icon() const {
    return QIcon(":/images/Blur.png");
  }
};

class itk_EXPORT ITKCurvatureAnisotropicDiffusionImageFilter : public StackProcess {
public:
  ITKCurvatureAnisotropicDiffusionImageFilter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(input, output, parms[0].toFloat(), parms[1].toInt(), parms[2].toFloat());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, float timestep, int iterations, float conductance);

  QString name() const {
    return "ITK Curvature Anisotropic Diffusion Image Filter";
  }
  QString folder() const {
    return "ITK/Filters";
  }
  QString description() const {
    return "Curvature Anisotropic Diffusion";
  }
  QStringList parmNames() const
  {
    return QStringList() << "TimeStep"
                         << "Iterations"
                         << "Conductance";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "TimeStep"
                         << "Iterations"
                         << "Conductance";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "0.03"
                         << "5"
                         << "9.0";
  }
  QIcon icon() const {
    return QIcon(":/images/Blur.png");
  }
};

class itk_EXPORT ITKGradientMagnitudeRecursiveGaussianImageFilter : public StackProcess {
public:
  ITKGradientMagnitudeRecursiveGaussianImageFilter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(input, output, parms[0].toFloat());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, float sigma);

  QString name() const {
    return "ITK Gradient Magnitude Recursive Gaussian Image Filter";
  }
  QString folder() const {
    return "ITK/Filters";
  }
  QString description() const {
    return "Gradient Magnitude Recursive Gaussian";
  }
  QStringList parmNames() const {
    return QStringList() << QString("Sigma (%1)").arg(UM);
  }
  QStringList parmDescs() const {
    return QStringList() << QString("Sigma (%1)").arg(UM);
  }
  QStringList parmDefaults() const {
    return QStringList() << "0.5";
  }
  QIcon icon() const {
    return QIcon(":/images/Blur.png");
  }
};

class itk_EXPORT ITKSigmoidImageFilter : public StackProcess {
public:
  ITKSigmoidImageFilter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(input, output, parms[0].toFloat(), parms[1].toFloat());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, float alpha, float beta);

  QString name() const {
    return "ITK Sigmoid Image Filter";
  }
  QString folder() const {
    return "ITK/Filters";
  }
  QString description() const {
    return "Sigmoid Filter";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Alpha"
                         << "Beta";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Alpha"
                         << "Beta";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "1000.0"
                         << "2000.0";
  }
  QIcon icon() const {
    return QIcon(":/images/Blur.png");
  }
};
}
}

#endif // ITKFILTER_HPP
