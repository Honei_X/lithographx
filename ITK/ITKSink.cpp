/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ITKSink.hpp"
#include <itkVTKImageIO.h>

namespace lgx {
namespace process {
bool ITKVTKWriter::operator()(const Store* input, QString filename)
{
  UImageConverter::Pointer converter = UImageConverter::New();

  converter->SetStore(input);

  converter->Update();

  UImageType::Pointer img = converter->GetOutput();

  typedef itk::ImageFileWriter<UImageType> WriterType;

  WriterType::Pointer filter = WriterType::New();

  QByteArray ba = filename.toLocal8Bit();
  filter->SetFileName(ba.data());

  itk::VTKImageIO::Pointer ImageIO = itk::VTKImageIO::New();

  filter->SetImageIO(ImageIO);

  filter->SetInput(img);

  filter->Write();

  return true;
}

REGISTER_STACK_PROCESS(ITKVTKWriter);
}
}
