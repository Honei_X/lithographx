/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ITKProgress.hpp"

#include "ITKFilter.hpp"

#include <itkSmoothingRecursiveGaussianImageFilter.h>
#include <itkCurvatureFlowImageFilter.h>
#include <itkRescaleIntensityImageFilter.h>
#include <itkCurvatureAnisotropicDiffusionImageFilter.h>
#include <itkGradientMagnitudeRecursiveGaussianImageFilter.h>
#include <itkDiscreteGaussianImageFilter.h>
#include <itkSigmoidImageFilter.h>

namespace lgx {
namespace process {
bool ITKDiscreteGaussianImageFilter::operator()(const Store* input, Store* output, const Point3f& sigma)
{
  UImageConverter::Pointer converter = UImageConverter::New();

  converter->SetStore(input);

  typedef itk::DiscreteGaussianImageFilter<UImageType, UImageType> FilterType;

  FilterType::Pointer filter = FilterType::New();
  filter->SetUseImageSpacingOn();
  filter->SetVariance(sigma.c_data());
  filter->SetInput(converter->GetOutput());

  ITKProgress progress("Discrete Gaussian Image Filter");
  progress.setFilter(filter);

  filter->Update();

  UImageType::ConstPointer result = filter->GetOutput();

  if(!UImageConverter::TransferImage(output->stack(), output, result)) {
    setErrorMessage("ITK Process tried to change the size of the stack");
    return false;
  }

  output->changed();
  return true;
}
REGISTER_STACK_PROCESS(ITKDiscreteGaussianImageFilter);

bool ITKSmoothingRecursiveGaussianImageFilter::operator()(const Store* input, Store* output, const Point3f& sigma)
{
  SImageConverter::Pointer converter = SImageConverter::New();

  converter->SetStore(input);

  typedef itk::SmoothingRecursiveGaussianImageFilter<SImageType, SImageType> FilterType;

  FilterType::Pointer filter = FilterType::New();
  FilterType::SigmaArrayType sig;
  sig[0] = sigma[0];
  sig[1] = sigma[1];
  sig[2] = sigma[2];
  filter->SetSigmaArray(sig);
  filter->SetNormalizeAcrossScale(false);
  filter->SetInput(converter->GetOutput());

  ITKProgress progress("Smoothing Recursive Gaussian Image Filter");
  progress.setFilter(filter);

  filter->Update();

  SImageType::ConstPointer result = filter->GetOutput();

  if(!SImageConverter::TransferImage(output->stack(), output, result)) {
    setErrorMessage("ITK Process tried to change the size of the stack");
    return false;
  }

  output->changed();
  return true;
}
REGISTER_STACK_PROCESS(ITKSmoothingRecursiveGaussianImageFilter);

bool ITKCurvatureFlowImageFilter::operator()(const Store* input, Store* output, float timestep, int steps)
{
  UImageConverter::Pointer converter = UImageConverter::New();

  converter->SetStore(input);

  typedef itk::CurvatureFlowImageFilter<UImageType, FImageType> FilterType;
  FilterType::Pointer filter = FilterType::New();
  filter->SetInput(converter->GetOutput());
  filter->SetTimeStep(timestep);
  filter->SetNumberOfIterations(steps);

  typedef itk::RescaleIntensityImageFilter<FImageType, UImageType> RescalerType;
  RescalerType::Pointer rescaler = RescalerType::New();
  rescaler->SetInput(filter->GetOutput());
  rescaler->SetOutputMinimum(0);
  rescaler->SetOutputMaximum(65535);

  ITKProgress progress("Curvature Flow Image Filter");
  progress.setFilter(filter);

  rescaler->Update();

  UImageType::ConstPointer result = rescaler->GetOutput();

  if(!UImageConverter::TransferImage(output->stack(), output, result)) {
    setErrorMessage("ITK Process tried to change the size of the stack");
    return false;
  }

  output->changed();
  return true;
}
REGISTER_STACK_PROCESS(ITKCurvatureFlowImageFilter);

bool ITKCurvatureAnisotropicDiffusionImageFilter::operator()(const Store* input, Store* output, float timestep,
                                                             int iterations, float conductance)
{
  FImageConverter::Pointer converter = FImageConverter::New();

  converter->SetStore(input);

  typedef itk::CurvatureAnisotropicDiffusionImageFilter<FImageType, FImageType> FilterType;
  FilterType::Pointer filter = FilterType::New();
  filter->SetInput(converter->GetOutput());
  filter->SetTimeStep(timestep);
  filter->SetNumberOfIterations(iterations);
  filter->SetConductanceParameter(conductance);

  ITKProgress progress("Curvature Anisotropic Diffusion Image Filter");
  progress.setFilter(filter);

  filter->Update();

  FImageType::ConstPointer result = filter->GetOutput();

  if(!FImageConverter::TransferImage(output->stack(), output, result)) {
    setErrorMessage("ITK Process tried to change the size of the stack");
    return false;
  }

  output->changed();
  return true;
}
REGISTER_STACK_PROCESS(ITKCurvatureAnisotropicDiffusionImageFilter);

bool ITKGradientMagnitudeRecursiveGaussianImageFilter::operator()(const Store* input, Store* output, float sigma)
{
  UImageConverter::Pointer converter = UImageConverter::New();

  converter->SetStore(input);

  typedef itk::GradientMagnitudeRecursiveGaussianImageFilter<UImageType, UImageType> FilterType;
  FilterType::Pointer filter = FilterType::New();
  filter->SetInput(converter->GetOutput());
  filter->SetSigma(sigma);

  typedef itk::RescaleIntensityImageFilter<UImageType, UImageType> RescalerType;
  RescalerType::Pointer rescaler = RescalerType::New();
  rescaler->SetInput(filter->GetOutput());
  rescaler->SetOutputMinimum(0);
  rescaler->SetOutputMaximum(65535);

  ITKProgress progress("Gradient Magnitude Recursive Gaussian Image Filter");
  progress.setFilter(filter);

  rescaler->Update();

  UImageType::ConstPointer result = rescaler->GetOutput();

  if(!UImageConverter::TransferImage(output->stack(), output, result)) {
    setErrorMessage("ITK Process tried to change the size of the stack");
    return false;
  }

  output->changed();
  return true;
}
REGISTER_STACK_PROCESS(ITKGradientMagnitudeRecursiveGaussianImageFilter);

bool ITKSigmoidImageFilter::operator()(const Store* input, Store* output, float alpha, float beta)
{
  UImageConverter::Pointer converter = UImageConverter::New();

  converter->SetStore(input);

  typedef itk::SigmoidImageFilter<UImageType, UImageType> FilterType;
  FilterType::Pointer filter = FilterType::New();
  filter->SetInput(converter->GetOutput());
  filter->SetAlpha(alpha);
  filter->SetBeta(beta);

  ITKProgress progress("Sigmoid Image Filter");
  progress.setFilter(filter);

  filter->Update();

  UImageType::ConstPointer result = filter->GetOutput();

  if(!UImageConverter::TransferImage(output->stack(), output, result)) {
    setErrorMessage("ITK Process tried to change the size of the stack");
    return false;
  }

  output->changed();
  return true;
}
REGISTER_STACK_PROCESS(ITKSigmoidImageFilter);
}
}
