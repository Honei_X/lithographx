/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef ITKSOURCE_HPP
#define ITKSOURCE_HPP

#include <QObject>
#include <ITKConfig.hpp>
#include <ITKProcess.hpp>
#include <itkImageFileReader.h>
#include <ui_ImportITKStack.h>

#include <Geometry.hpp>

namespace lgx {
namespace process {
typedef itk::Image<unsigned short int, 5> U5ImageType;
typedef itk::ImageFileReader<U5ImageType> ReaderType;

class itk_EXPORT ITKImageReader : public QObject, public StackProcess {
  Q_OBJECT
public:
  ITKImageReader(const StackProcess& process)
    : Process(process)
    , QObject()
    , StackProcess(process)
  {
  }

  bool initialize(QStringList& parms, QWidget* parent) override;

  bool operator()(const QStringList& parms) override;
  bool operator()(Stack* stack, Store* store, QString filename);

  QString name() const override {
    return "ITK Image Reader";
  }
  QString folder() const override {
    return "ITK/System";
  }
  QString description() const override {
    return "Read a 3D image from a single file";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Filename"
                         << "Store"
                         << "Stack number"
                         << "Choose file";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Filename"
                         << "Store"
                         << "Stack number"
                         << "If false and if a filename is provided, the dialog box to choose a file won't be shown";
  }
  QStringList parmDefaults() const override
  {
    return QStringList() << ""
                         << "Main"
                         << "0"
                         << "True";
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = storeChoice();
    map[3] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/open.png");
  }

protected:
  void LoadSeries(bool next);

protected slots:
  void NextSeriesSlot() {
    LoadSeries(true);
  };
  void PrevSeriesSlot() {
    LoadSeries(false);
  };

protected:
  QString filename;
  Point5i size;
  Point5f step;

  QString pixelType;
  int componentSize;
  int components;

  int selectSeries;
  int selectComponent;
  int selectChannel;
  int selectTimePoint;

  Ui_ImportITKStackDlg* ui;
  itk::ImageIOBase::Pointer imageIO;
};
} // namespace process
} // namespace lgx

#endif // ITKSOURCES_HPP
