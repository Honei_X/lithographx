#ifndef CREATE_CUBE_HPP
#define CREATE_CUBE_HPP

#include <Process.hpp>

namespace process
{
  class ComputeVolume : public MeshProcess
  {
    public:
      ComputeVolume(const MeshProcess& process)
        : Process(process)
        , MeshProcess(process)
        { }

      bool operator()(const QStringList& strings, const QList<float>& values)
      {

        if(!checkState().mesh(MESH_NON_EMPTY))
        {
          return false;
        }
        Mesh *m = currentMesh();
        bool res = (*this)(m);
        if(res)
            m->showHeat();
        return res;
      }

      bool operator()(Mesh *mesh);

      QString folder() const { return "Heat Map"; }
      QString name() const { return "Compute Volume"; }
      QString description() const { return "Compute the volume of closed cells."; }
      QStringList parameters() const { return QStringList(); }
      QIcon icon() const
      {
        return QIcon(":/images/ComputeVolume.png");
      }

  };
}

#endif // CREATE_CUBE_HPP

