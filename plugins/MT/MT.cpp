#include "MT.hpp"
#include "Information.hpp"
#include "Progress.hpp"

#include <unorderedmap.hpp> // les tables de hachage
#include <vector>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_eigen.h>


namespace
{

struct PCA
{
    Point3f p1, p2, p3;
    Point3f ev;
    gsl_matrix *mat ;
    gsl_vector *eval ;
    gsl_matrix *evec ;
    gsl_eigen_symmv_workspace *w ;

    //constructeur
    PCA()
        : mat(gsl_matrix_alloc(3, 3))
        , eval(gsl_vector_alloc(3))
        , evec(gsl_matrix_alloc(3,3))
        , w(gsl_eigen_symmv_alloc(3))
    {  }

    // destructeur
    ~PCA()
    {
        gsl_eigen_symmv_free(w);
        gsl_vector_free(eval);
        gsl_matrix_free(evec);
        gsl_matrix_free(mat);
    }



    bool operator()(const Matrix3f& corr)
    {

        //Information::out << "Decompose correlation matrix" << endl;
        // Eigen-decomposition of the matrices
        for(int i = 0 ; i < 3 ; ++i)
            for(int j = 0 ; j < 3 ; ++j)
                gsl_matrix_set(mat, i, j, corr(i,j));
        gsl_eigen_symmv(mat, eval, evec, w);
        gsl_eigen_symmv_sort(eval, evec, GSL_EIGEN_SORT_VAL_DESC);

        p1=Point3d(gsl_matrix_get(evec, 0, 0),
                   gsl_matrix_get(evec, 1, 0),
                   gsl_matrix_get(evec, 2, 0));
        p2= Point3d(gsl_matrix_get(evec, 0, 1),
                    gsl_matrix_get(evec, 1, 1),
                    gsl_matrix_get(evec, 2, 1));
        p3=Point3d(gsl_matrix_get(evec, 0, 2),
                   gsl_matrix_get(evec, 1, 2),
                   gsl_matrix_get(evec, 2, 2));
        ev= Point3d(gsl_vector_get(eval, 0),
                    gsl_vector_get(eval, 1),
                    gsl_vector_get(eval, 2));
        if((p1^p2)*p3 < 0)
            p3 = -p3;
        return true;
    }
};

}

namespace process
{

bool MTorientation::operator()(Mesh* mesh,QString output, float bsize)
{
    typedef std::unordered_map<int,size_t> label_map_t;
    label_map_t label_map;

    std::vector<int> labels;
    std::vector<Point3f> centers;
    std::vector<std::vector<vertex> > cells;

    int lab;

    //on recupere le graphe
    const vvgraph& G=mesh->graph();


    // on parcourt tous les vertexes, et on les tri selon leur label
    // --> on construit :
    // - un vecteur contenant tous les labels : labels
    // - un vecteur contenant tous les centres de cellule : centers
    //(centers[i] est le centre de la cellule avec label labels[i]
    // - un vecteur ayant comme elements le vecteur de vertexes appartenant a une cellule : cells
    // ----------------------------------------------------------
    forall(const vertex& v, G)
    {
        size_t cell_id;

        lab = v->label;

        if (lab>0) // pour ne pas prendre en compte les bordures de cellules (label -1)
        {
            label_map_t::iterator found=label_map.find(lab);
            if(found==label_map.end())
            {
                //insert in labels the new label
                cell_id = labels.size();
                label_map[lab]=cell_id;
                labels.push_back(lab);
                cells.push_back(std::vector<vertex>());
                centers.push_back(Point3f());
            }
            else
            {
                cell_id = found->second;
            }
            cells[cell_id].push_back(v);
            centers[cell_id]+=v->pos;
        }
    }

    // dans un vecteur declare et initialise avec des elements 0 autant de matrices 3x3 que de cellules
    std::vector<Matrix3f> correlations(labels.size());
    std::vector<Point3f> normals(labels.size());
    std::vector<Point3f> u1(labels.size());
    std::vector<Point3f> u2(labels.size());

    // pour chaque cellule
    // - on construit les matrices de correlation
    // - on fait un ACP et en prenant les deux premiers axes on determine le plan principal de la cellule
    // - pour la visualisation on met a jours cellAxisScaled, cellAxisColor et labelCenter
    // ----------------------------------------------------------
    mesh->cellAxisScaled().clear();
    mesh->cellAxisColor().clear();
    mesh->labelCenter().clear();
    mesh->cellAxisWidth()=1;

    float lref=HUGE_VAL; //lambda1 minimal sur les cellules

    { // limiter la portee de la variable pca
        Matrix3f color;
        color[0]=Point3f(1,0,0);
        color[1]=Point3f(0,1,0);
        color[2]=Point3f(1,1,1);
        PCA pca;
        for (size_t i=0;i<labels.size();i++)
        {
            Point3f& c = centers[i];
            const std::vector<vertex>& cell = cells[i];
            Matrix3f& corr = correlations[i];
            c /= cell.size();
            // on itere sur chaque vertex de la cellule et
            // pour chaque vertex on calcule la mat de correlation
            forall(const vertex& v,cell)
            {
                Point3f dp = v->pos-c;
                for(int i=0;i<3;i++)
                    for(int j=0;j<3;j++)
                        corr(i,j)+=dp[i]*dp[j];
            }
            corr /= cell.size(); //il va le faire element par element
            if(!pca(corr))
                return setErrorMessage("Error during PCA calculation!");
            // the normal to the principal plane
            normals[i]=pca.p3;
            u1[i]=pca.p1;
            u2[i]=pca.p2;
            if (lref>pca.ev[0]) lref=pca.ev[0];// on cherche lambda1 minimal sur les cellules
            // pour visualiser
            // a. les axes :
            /*
            Matrix3f mat;
            mat[0]=pca.p1;
            mat[1]=pca.p2;
            mat[2]=pca.p3;
            m->cellAxisScaled()[labels[i]]=mat;
            */
            // b. les couleurs :
            mesh->cellAxisColor()[labels[i]]=color;
            // c. les centres :
            mesh->labelCenter()[labels[i]]=c;

        }
    }


    // eroder les cellules pour prendre en suite seulement l'intérieur
    mesh->markBorder(bsize); // modifie le minb en different de 0 sur les vertexes plus pres du bord que bsize

    // par cellule calculer la matrice de correlation des MT
    //--------------------------------
    // 1. allouer vecteur des matrices
    std::vector<Matrix2f> mt_corr(labels.size());
    float l1max=0;
    // 2. pour chaque cellule
    for (size_t i=0;i<labels.size();i++)
    {
        Matrix2f& M = mt_corr[i];
        float Atotal=0; // aire totale de la cellule
        int Ntriangle=0; //nombre de triangles dans la cellule
        // a. prendre 2 vecteurs du plan u1[i], u2[i]
        const std::vector<vertex>& cell = cells[i];
        forall(const vertex& v,cell) if(v->minb==0)
        {
            forall(const vertex& n, G.neighbors(v))  if(n->minb==0)
            {
                const vertex& m=G.nextTo(v,n); // (v,n,m) est un triangle
                if(m->minb==0)
                {

                    if (mesh->uniqueTri(v,n,m))
                    {
                        Ntriangle++;
                        // projette les points sur (u1,u2)
                        Point3f Pv, Pn, Pm;
                        Pv=Point3f(v->pos*u1[i],v->pos*u2[i],v->signal);
                        Pn=Point3f(n->pos*u1[i],n->pos*u2[i],n->signal);
                        Pm=Point3f(m->pos*u1[i],m->pos*u2[i],m->signal);
                        // calcule le gradient sur le triangle
                        // calcule la normale au triangle (Pv,Pn,Pm)
                        Point3f lambdan;
                        lambdan=(Pn-Pv)^(Pm-Pv); // dans son module contient l'aire du triangle*2
                        float A = fabs(lambdan[2]/2); // aire de la projection
                        Atotal+=A;
                        Point3f normal=normalized(lambdan);
                        Point2f NormGrad = A*Point2f(-normal[1], normal[0])/normal[2];
                        M(0,0)+=NormGrad[0]*NormGrad[0];
                         M(1,1)+=NormGrad[1]*NormGrad[1];
                         M(0,1)+=NormGrad[0]*NormGrad[1];
                         M(1,0)+=NormGrad[0]*NormGrad[1];

                    }

                }
            }
        }
         M /= Atotal;
         Information::out << "Atotal="<<Atotal<< "  - Ntotal="<<Ntriangle<<" - M="<<M<<endl;
        // MT orientations
         float tM=M(0,0)+M(1,1);
         float dM=M(0,0)*M(1,1)-M(0,1)*M(0,1);
         float deltaM=sqrt(tM*tM-4*dM);
         float l1=(tM+deltaM)/2;
         float l2=(tM-deltaM)/2;
         if (l1>l1max) l1max=l1;
         Information::out << "l1="<<l1<<" - l2="<<l2<<endl;
         Point2f v1, v2;
         v1[1]=(l1-M(0,0))/sqrt(M(0,1)*M(0,1)+(l1-M(0,0))*(l1-M(0,0)));
         v1[0]=sqrt(1-v1[1]*v1[1]);
         v2[1]=(l2-M(0,0))/sqrt(M(0,1)*M(0,1)+(l2-M(0,0))*(l2-M(0,0)));
         v2[0]=sqrt(1-v2[1]*v2[1]);
        // Re-projeter en 3D
         Point3f v13D=v1[0]*u1[i]+v1[1]*u2[i];
         Point3f v23D=v2[0]*u1[i]+v2[1]*u2[i];
         Matrix3f mat;
         mat[0]=v13D*l1;
         mat[1]=v23D*l2;
         mat[2]=normals[i];
         mesh->cellAxisScaled()[labels[i]]=mat;


        //
    }
    forall(int lab,labels)
    {
        mesh->cellAxisScaled()[lab]*=lref/l1max;
    }

    // pour ecrire les resultats dans un fichier:
    if ( not output.isEmpty())
    {
        QFile f(output);
        if (not f.open(QIODevice::WriteOnly|QIODevice::Text))
        {
            return setErrorMessage(QString("Can't open file %1 for writing").arg(output));
        }
        QTextStream ts(&f);
        ts<<"label, sqrt(label)\n";
        ts.setRealNumberPrecision(10);
        for (size_t i=0;i<labels.size();i++)
        {
            ts << labels[i]<<","<<sqrt(labels[i])<<"\n";

        }
    }

    return true;
}

REGISTER_MESH_PROCESS(MTorientation);
}


