#include "CellCurvature.hpp"
#include "Information.hpp"
#include "Progress.hpp"
#include <curvature.h>
#include <thrust.hpp>
#include <vector>
#include <unorderedset.hpp>
#include <QFile>
#include <QTextStream>
#include <Misc.hpp>

namespace
{
struct ComputeCellCurvatureKernel
{
    ComputeCellCurvatureKernel(const vvgraph& _S, const std::vector<vertex>& _cells, bool uce, bool uco, int nh, std::vector<Curvature>& cs)
        : S(_S)
        , cells(_cells)
        , use_center(uce)
        , use_contour(uco)
        , neighborhood(nh)
        , curvs(cs)
    { }

    void operator()(int ci)
    {
        const vertex& center = cells[ci];
        std::vector<Point3f> points, normals;
        std::unordered_set<vertex> used_vertices;
        std::vector<vertex> externs;
        externs.push_back(center);

        for(int count = 0 ; count <= neighborhood ; ++count)
        {
            std::vector<vertex> new_externs;
            forall(const vertex& c, externs)
            {
                if(use_center or count == 0) // The first point MUST be the one the curvature is evaluated on
                {
                    points.push_back(c->pos);
                    normals.push_back(c->nrml);
                }
                forall(const vertex& v, S.neighbors(c))
                {
                    if(used_vertices.find(v) == used_vertices.end())
                    {
                        used_vertices.insert(v);
                        if(use_contour)
                        {
                            points.push_back(v->pos);
                            normals.push_back(v->nrml);
                        }
                        forall(const vertex& cn, S.neighbors(c))
                        {
                            if((cn->type == 'c') and (used_vertices.find(cn) == used_vertices.end()))
                            {
                                new_externs.push_back(cn);
                                used_vertices.insert(cn);
                            }
                        }
                    }
                }
            }
            swap(externs, new_externs);
        }

        curvs[ci].update(points, normals);
    }

    const vvgraph& S;
    const std::vector<vertex>& cells;
    bool use_center, use_contour;
    int neighborhood;
    std::vector<Curvature>& curvs;
};
}

namespace process
{

bool CellCurvature::operator()(Mesh *mesh, const QString& filename, bool use_center, bool use_contour, bool show_tensors, int neighborhood,
                               float tensor_scale, const QColor& pos, const QColor& neg)
{
    const vvgraph& S = mesh->graph();

    // First, gather the cell centers
    std::vector<vertex> cells;
    std::vector<Curvature> curvs;

    forall(const vertex& c, S)
    {
        if(c->type == 'c')
            cells.push_back(c);
    }
    curvs.resize(cells.size());

    thrust::for_each(THRUST_RETAG(thrust::counting_iterator<uint>(0)),
                     THRUST_RETAG(thrust::counting_iterator<uint>(cells.size())),
                     ComputeCellCurvatureKernel(S, cells, use_center, use_contour, neighborhood, curvs));

    if(show_tensors)
    {
        IntMatrix3x3fMap& cellTensors = mesh->cellAxis();
        IntMatrix3x3fMap& scaledCellTensors = mesh->cellAxisScaled();
        IntMatrix3x3fMap& colorCellTensors = mesh->cellAxisColor();
        cellTensors.clear();
        scaledCellTensors.clear();
        colorCellTensors.clear();

        mesh->cellAxisWidth() = 2;

        Point3f colorPos(pos.redF(), pos.greenF(), pos.blueF());
        Point3f colorNeg(neg.redF(), neg.greenF(), neg.blueF());

        for(int i = 0 ; i < cells.size() ; ++i)
        {
            const vertex& c = cells[i];
            Curvature& curv = curvs[i];
            Point3f e1, e2;
            float c1, c2;
            curv.get_eigenVectors(e1, e2);
            curv.get_eigenValues(c1, c2);
            cellTensors[c->label][0] = e1;
            cellTensors[c->label][1] = e2;
            cellTensors[c->label][2] = Point3f();

            scaledCellTensors[c->label][0] = e1*c1*tensor_scale;
            scaledCellTensors[c->label][1] = e2*c2*tensor_scale;
            scaledCellTensors[c->label][2] = Point3f();

            colorCellTensors[c->label][0] = (c1 < 0 ? colorNeg : colorPos);
            colorCellTensors[c->label][1] = (c2 < 0 ? colorNeg : colorPos);
        }
    }

    if(not filename.isEmpty())
    {
        QFile f(filename);
        if(not f.open(QIODevice::WriteOnly))
            return setErrorMessage(QString("Cannot open file '%1' for writing").arg(filename));
        QTextStream ts(&f);
        ts << QString("Cell,X1,Y1,Z1,X2,Y2,Z2,k1 (1/%1),k2 (1/%1)").arg(micrometers) << endl;
        for(size_t i = 0 ; i < cells.size() ; ++i)
        {
            QStringList fields;
            fields << QString::number(cells[i]->label);
            Curvature& curv = curvs[i];
            Point3f e1, e2;
            float c1, c2;
            curv.get_eigenVectors(e1, e2);
            curv.get_eigenValues(c1, c2);
            fields << QString::number(e1.x(), 'g', 10) << QString::number(e1.y(), 'g', 10) << QString::number(e1.z(), 'g', 10)
                   << QString::number(e2.x(), 'g', 10) << QString::number(e2.y(), 'g', 10) << QString::number(e2.z(), 'g', 10)
                   << QString::number(c1, 'g', 10)     << QString::number(c2, 'g', 10);
            ts << fields.join(",") << endl;
        }
    }

    return true;
}

REGISTER_MESH_PROCESS(CellCurvature);
}


