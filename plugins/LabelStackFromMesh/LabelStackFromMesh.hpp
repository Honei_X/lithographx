#ifndef LabelStackFromMesh_HPP
#define LabelStackFromMesh_HPP

#include <Process.hpp>

namespace process
{
class LabelStackFromMesh : public StackProcess
{
public:
    LabelStackFromMesh(const StackProcess& process)
        : Process(process)
        , StackProcess(process)
    { }
    
    bool operator()(const QStringList& strings, const QList<float>& values)
    {
        if(!checkState().store(STORE_LABEL).mesh(MESH_NON_EMPTY))
            return false;
        return operator()(currentStack()->currentStore(), currentStack()->work(), currentMesh(), stringToBool(strings[0]));
    }
    
    bool operator()(const Store *store, Store* output, const Mesh* mesh, bool erase_extra);
    
    QString folder() const { return "Segmentation"; }
    QString name() const { return "Label Stack from Mesh"; }
    QString description() const { return "Relabel the stack from the mesh. The mesh must be a previous or modified 3D cell mesh taken from the same or a very similar stack. In short, the barycenter of the various labels in the mesh will decide of the conversion."; }
    QStringList parameters() const { return QStringList() << "Erase extra labels"; }
    QStringList default_strings() const { return QStringList() << "No"; }
    StringChoiceMap string_choice() const {
        StringChoiceMap map;
        map[0] = boolean_choice();
        return map;
    }
    QList<float> default_values() const { return QList<float>(); }
    QIcon icon() const
    {
        return QIcon(":/images/LabelStackFromMesh.png");
    }
    
};
}

#endif // LabelStackFromMesh_HPP


