#include "LabelStackFromMesh.hpp"
#include "Information.hpp"
#include "Progress.hpp"

#include "unorderedmap.hpp"

namespace process
{

bool LabelStackFromMesh::operator()(const Store *store, Store* output, const Mesh* mesh, bool erase_extra)
{
    std::unordered_map<int,Point3f> centers;
    std::unordered_map<int,unsigned long long> nb_vtx;
    std::unordered_map<int,int> relabeling;
    std::vector<int> labels;
    int max_label = 0;

    const vvgraph& S = mesh->graph();
    forall(const vertex& v, S)
    {
        std::unordered_map<int,Point3f>::iterator found = centers.find(v->label);
        if(found == centers.end())
        {
            centers[v->label] = v->pos;
            nb_vtx[v->label] = 1;
            labels.push_back(v->label);
            if(v->label > max_label) max_label = v->label;
        }
        else
        {
            found->second += v->pos;
            nb_vtx[v->label]++;
        }
    }

    const Stack* stk = store->stack();
    const HVecUS& data = store->data();

    forall(int l, labels)
    {
        centers[l] /= float(nb_vtx[l]);
        Point3i px = stk->worldToImagei(centers[l]);
        if(stk->boundingBox().contains(px))
            relabeling[data[stk->offset(px)]] = l;
    }

    HVecUS& odata = output->data();
    for(int k = 0 ; k < data.size() ; ++k)
    {
        ushort us = data[k];
        if(us != 0)
        {
            std::unordered_map<int,int>::iterator found = relabeling.find(us);
            int nl = 0;
            if(found == relabeling.end())
            {
                if(erase_extra)
                    relabeling[us] = 0;
                else
                {
                    nl = ++max_label;
                    relabeling[us] = nl;
                }
            }
            else
                nl = found->second;
            odata[k] = nl;
        }
    }

    output->copyMetaData(store);
    output->changed();

    return true;
}

REGISTER_STACK_PROCESS(LabelStackFromMesh);
}


