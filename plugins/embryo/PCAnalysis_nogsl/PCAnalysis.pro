MGX_PATH=$$system(dirname $(readlink -f $(which MorphoGraphX)))/..
MGX_INCLUDE=$$system(MorphoGraphX --include)
MGX_PROCESSES=$$system(MorphoGraphX --process)
CUDA_INCLUDE=/usr/local/stow/cuda/include
CIMG_INCLUDE= # /Users/pbdr/apps/science/CImg-1.4.8_testing

TEMPLATE = lib

TARGET=PCAnalysis

DEFINES += cimg_display=0
SOURCES += PCAnalysis.cpp
HEADERS += PCAnalysis.hpp
RESOURCES = PCAnalysis.qrc

CONFIG += qt release plugin
QT += xml opengl

LIBS += -lgsl -lgslcblas

INCLUDEPATH += $$MGX_INCLUDE $$MGX_PROCESSES/include $$CUDA_INCLUDE $$CIMG_INCLUDE
macx: {
LIBS += -F$$MGX_PATH/Frameworks -framework mgx
} else {
LIBS += -L$$MGX_PATH/lib -lmgx
}

target.path = $$MGX_PROCESSES
INSTALLS = target


