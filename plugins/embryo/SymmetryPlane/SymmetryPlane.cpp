#include <complex>
#include <QTextStream>
#include <QString>
template <typename T>
static QTextStream& operator<<(QTextStream& ts, const std::complex<T>& c)
{
    ts << c.real();
    QTextStream::NumberFlags nf = ts.numberFlags();
    ts.setNumberFlags(QTextStream::ForceSign);
    ts << c.imag();
    ts.setNumberFlags(nf);
    ts << "j";
    return ts;
}

#include "SymmetryPlane.hpp"
#include "Information.hpp"
#include "Progress.hpp"

#include <gsl/gsl_linalg.h>
#include <gsl/gsl_eigen.h>

namespace process
{

typedef Vector<3,std::complex<double> > Point3c;
typedef Matrix<3,3,std::complex<double> > Matrix3c;


bool SymmetryPlane::operator()(Stack *stk, int axis_inverted, bool apply_to_stack, bool apply_to_other_stack)
{
    Matrix4d transform(stk->getFrame().matrix(), GL_STYLE);
    // Invert the z axis to account for the reflexion
    for(int i = 0 ; i < 3 ; ++i)
        transform(i,axis_inverted) *= -1;
    Information::out << transform << endl;
    // Find the center of the symmetry
    Matrix3d eq;
    for(int i = 0 ; i < 3 ; ++i)
    {
        for(int j = 0 ; j < 3 ; ++j)
            eq(i,j) = transform(i,j);
        eq(i,i) -= 1;
    }

    int gsl_error = 0;

    Point3d zz(-transform(0,3), -transform(1,3), -transform(2,3));
    Point3d x; // Solution vector

    // Solve the system eq*X = zz
    gsl_matrix_view EQ = gsl_matrix_view_array(eq.data(), 3, 3);
    gsl_vector_view ZZ = gsl_vector_view_array(zz.data(), 3);
    gsl_vector_view X = gsl_vector_view_array(x.data(), 3);

    int s;
    // Use LU decomposition
    gsl_permutation * p = gsl_permutation_alloc(3);
    gsl_error = gsl_linalg_LU_decomp(&EQ.matrix, p, &s);
    if(gsl_error)
        return setErrorMessage(QString("Error during LU decomposition: %1").arg(gsl_strerror(gsl_error)));
    gsl_error = gsl_linalg_LU_solve(&EQ.matrix, p, &ZZ.vector, &X.vector);
    if(gsl_error)
        return setErrorMessage(QString("Error during LU solving: %1").arg(gsl_strerror(gsl_error)));

    //Information::out << "eq = " << eq << endl;
    //Information::out << "zz = " << zz << endl;
    Information::out << "Center of symmetry = " << x << endl;

    gsl_permutation_free(p);

    // Now, find the eigen-value decomposition for the normal to the plane
    for(int i = 0 ; i < 3 ; ++i)
        for(int j = 0 ; j < 3 ; ++j)
            eq(i,j) = transform(i,j);

    Point3c eval;
    Matrix3c evec;

    gsl_matrix_complex_view EVEC = gsl_matrix_complex_view_array(reinterpret_cast<double*>(evec.data()), 3, 3);
    gsl_vector_complex_view EVAL = gsl_vector_complex_view_array(reinterpret_cast<double*>(eval.data()), 3);

    gsl_eigen_nonsymmv_workspace *w =  gsl_eigen_nonsymmv_alloc(3); // Allocate space for computation

    gsl_eigen_nonsymmv_params(0, w); // Don't try to balance as the matrix is already a unit matrix

    gsl_error = gsl_eigen_nonsymmv(&EQ.matrix, &EVAL.vector, &EVEC.matrix, w);
    if(gsl_error)
        return setErrorMessage(QString("Error during eigen-value decomposition: %1").arg(gsl_strerror(gsl_error)));

    //Information::out << "eq = " << eq << endl;
    //Information::out << "evec = " << evec << endl;
    //Information::out << "eval = " << eval << endl;

    gsl_eigen_nonsymmv_free(w); // Free computation space

    // Make sure the first eigen value is real

    if(eval[0].imag() != 0)
        return setErrorMessage(QString("Error, first eigen value is not real: %1").arg(eval[0].imag()));

    Point3d u(evec(0,0).real(), evec(1,0).real(), evec(2,0).real());
    Information::out << "Normal vector = " << u << endl;

    // And now, find the points closest to the current origin in this plane
    double lambda = u*x;
    x = lambda*u;

    // Now move the cutting surface
    CuttingSurface *cs = cuttingSurface();
    qglviewer::Frame frame;
    // Reset the frame
    frame.setPositionAndOrientation(qglviewer::Vec(0,0,0), qglviewer::Quaternion(0,0,0,1));
    // Now rotate so the u vector is the z axis
    qglviewer::Quaternion q;
    Point3d v1, v2;
    if(norm(u^Point3d(0,0,1)) > 0.1)
        v1 = normalized(u ^ Point3d(0,0,1));
    else
        v1 = normalized(u ^ Point3d(1,0,0));
    v2 = normalized(u ^ v1);
    q.setFromRotatedBase(qglviewer::Vec(v1), qglviewer::Vec(v2), qglviewer::Vec(u));
    frame.setRotation(q);
    frame.setTranslation(qglviewer::Vec(x));

    if(apply_to_stack)
    {
        QStringList args;
        for(int i = 0 ; i < 3 ; ++i)
        {
            if(i == axis_inverted)
                args << "Yes";
            else
                args << "No";
        }
        Information::out << "Reverse axes with arguments: " << args.join(", ") << endl;
        if(!RunProcess("Stack", "Reverse Axes", args, QList<float>()))
            return false;
        cs->frame().setPositionAndOrientation(qglviewer::Vec(0,0,0), qglviewer::Quaternion(0,0,0,1));
        cs->setMode(CuttingSurface::THREE_AXIS);
        (qglviewer::Frame&)stk->trans() = frame.inverse();
        stk->setShowTrans(true);
        if(apply_to_other_stack)
        {
            Stack *ostk = stack(0);
            if(ostk == stk)
                ostk = stack(1);
            (qglviewer::Frame&)ostk->trans() = frame.inverse();
            ostk->setShowTrans(true);
        }
    }
    else
    {
        cs->setMode(CuttingSurface::PLANE);
        (qglviewer::Frame&)cs->frame() = frame;
    }
    cs->showGrid();
    cs->show();
    cs->setSize(multiply(stk->step(),Point3f(stk->size())));

    QString result;
    QTextStream ts(&result);
    ts << "Equation of the plane: " << u.x() << " x";
    ts.setNumberFlags(QTextStream::ForceSign);
    ts << u.y() << " y" << u.z() << " z" << (-u*x) << " = 0";

    Information::out << result << endl;

    return true;
}

REGISTER_STACK_PROCESS(SymmetryPlane);
}


