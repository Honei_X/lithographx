EXECUTE_PROCESS(
  COMMAND ${XDG_ICON_RESOURCE} install --mode system --size 64 ${CMAKE_INSTALL_PREFIX}/share/LithoGraphX/Icon64.png morphographx-LithoGraphX
  COMMAND ${XDG_ICON_RESOURCE} install --mode system --size 22 ${CMAKE_INSTALL_PREFIX}/share/LithoGraphX/Icon22.png morphographx-LithoGraphX
  COMMAND ${XDG_DESKTOP_MENU} install --mode system ${CMAKE_INSTALL_PREFIX}/share/LithoGraphX/morphographx-LithoGraphX.desktop
  COMMAND ${XDG_MIME} install --mode system ${CMAKE_INSTALL_PREFIX}/share/LithoGraphX/morphographx-LithoGraphX.xml
  COMMAND ${XDG_ICON_RESOURCE} install --mode system --context mimetypes --size 48 ${CMAKE_INSTALL_PREFIX}/share/LithoGraphX/FileIcon.png application-morphographx.mgxv
  COMMAND ${XDG_ICON_RESOURCE} install --mode system --context mimetypes --size 48 ${CMAKE_INSTALL_PREFIX}/share/LithoGraphX/StackIcon.png application-morphographx.mgxs
  COMMAND ${XDG_ICON_RESOURCE} install --mode system --context mimetypes --size 48 ${CMAKE_INSTALL_PREFIX}/share/LithoGraphX/MeshIcon.png application-morphographx.mgxm
  )

