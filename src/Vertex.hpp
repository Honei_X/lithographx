/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef VERTEX_HPP
#define VERTEX_HPP

/**
 * \file Vertex.hpp
 *
 * This file contain the definition of the graph::Vertex class.
 * For now, the graph::Vertex class can only be used with the graph::VVGraph
 * class.
 */

#include <LGXConfig.hpp>

#include <Mangling.hpp>
#include <Thrust.hpp>
#include <UnorderedMap.hpp>

#include <iostream>
#include <map>
#include <memory>
#ifndef _MSC_VER
#  include <stdint.h>
#endif
#include <typeinfo>
#include <utility>

namespace lgx {
namespace graph {
/**
 * Type of the identifier of a vertex
 */
typedef intptr_t vertex_identity_t;

/**
 * Number used to enumerate the vertices of all types.
 */
extern LGX_EXPORT size_t vertex_counter;

#define TEMPLATE_VERTEX typename VertexContent, typename Alloc
#define VERTEX_ARGS VertexContent, Alloc

template <TEMPLATE_VERTEX> class WeakVertex;

/**
 * \class Vertex Vertex.hpp <Vertex.hpp>
 * Vertex of a vv graph.
 *
 * The vertexes handle their associated data using a reference counting scheme.
 * As such, they can be used as smart pointers. They are also comparable
 * (<,>,==,!=), which allow for use in any sorted structure and hashable for
 * use in any hash table-based structure.
 *
 * They also all have a unique identifier. This identifier can be used to
 * retrieve a weak reference on the data.
 *
 * \warning A weak reference won't keep the data alive! So be careful when you
 * use those. You can test if a vertex hold a weak reference using the
 * Vertex<VertexContent>::isWeakRef() method.
 */
template <TEMPLATE_VERTEX> class Vertex {
  friend class WeakVertex<VERTEX_ARGS>;

protected:
  /**
   * Type of the reference counted content
   */
  struct CountedContent : public VertexContent {
private:
    CountedContent()
      : VertexContent()
      , num(vertex_counter++)
    {
      count = 1u;       // To be used with tbb::atomic
    }

    CountedContent(const CountedContent& copy)
      : VertexContent(copy)
      , count(copy.count)
      , num(vertex_counter++)
    {
    }

    CountedContent(const VertexContent& copy)
      : VertexContent(copy)
      , num(vertex_counter++)
    {
      count = 1u;       // To be used with tbb::atomic
    }

public:
    typedef typename Alloc::template rebind<CountedContent>::other alloc_t;
    static alloc_t alloc;

    static CountedContent* New()
    {
      CountedContent* cc = alloc.allocate(1);
      ::new (cc) CountedContent();
      return cc;
    }

    static CountedContent* New(const VertexContent& copy)
    {
      CountedContent* cc = alloc.allocate(1);
      ::new (cc) CountedContent(copy);
      return cc;
    }

    static void Delete(CountedContent* cc)
    {
      cc->~CountedContent();
      alloc.deallocate(cc, 1);
    }

    /**
     * Reference count on the vertex
     */
    unsigned int count;

    size_t num;
  };

public:
  /**
   * Type of the identifier of the vertex
   */
  typedef vertex_identity_t identity_t;

  /**
   * Type of the content of the vertex
   */
  typedef VertexContent content_t;

  typedef WeakVertex<VERTEX_ARGS> weak_ref_t;

  /**
   * Type of the equivalent pointer
   */
  typedef VertexContent* pointer;

  /**
   * Creates a new vertex with a new content.
   *
   * Example:
   * \code
   *   struct VertexContent { int a; }
   *   // ...
   *   Vertex<VertexContent> v;
   *   v->a = 10;
   * \endcode
   */
  Vertex();

  /**
   * Creates a reference on the vertex of identifier \c id. If \c id
   * is 0, creates a null vertex.
   *
   * \param[in] id Label of the vertex to retrieve.
   *
   * \warning This function is very unsafe if used with anything but 0 as
   * an identifier.
   *
   * Example:
   * \code
   *   typedef Vertex<VertexContent> vertex;
   *   vertex v;
   *   vertex::identity_t i = v.id();
   *   vertex v1(i);
   *   assert(v1 == v);
   * \endcode
   */
  explicit Vertex(identity_t id);

  /**
   * Copy a vertex.
   *
   * The data is not copied. The quality of the copy (i.e. weak/strong
   * reference) is the same as the copied.
   */
  Vertex(const Vertex& copy);

  /**
   * Construct a strong reference from a weak one
   */
  explicit Vertex(const weak_ref_t& w);

  /**
   * Desctructor
   */
  ~Vertex();

  /**
   * Access to the data.
   *
   * \warning Do not try to access the data of the null vertex.
   */
  VertexContent* operator->() const {
    return content;
  }
  /**
   * Access to the data.
   *
   * \warning Do not try to access the data of the null vertex.
   */
  VertexContent& operator*() const {
    return *content;
  }

  /**
   * Change the vertex held by the current object.
   *
   * The data is never modified by this operation. If you wish to copy the data
   * of a vertex v1 into a vertex v2 use:
   * \code
   * *v2 = *v1;
   * \endcode
   */
  Vertex& operator=(const Vertex& other);

  Vertex& operator=(const identity_t& id);

  Vertex& operator=(const weak_ref_t& other);

  Vertex& operator=(const VertexContent* value);

  /**
   * Comparison operators.
   *
   * \note the comparisons work on the identifiers, not the contents!
   */
  bool operator==(const Vertex& other) const {
    return id() == other.id();
  }
  /**
   * Comparison operators.
   *
   * \note the comparisons work on the identifiers, not the contents!
   */
  bool operator!=(const Vertex& other) const {
    return id() != other.id();
  }
  /**
   * Comparison operators.
   *
   * \note the comparisons work on the identifiers, not the contents!
   */
  bool operator>(const Vertex& other) const {
    return id() > other.id();
  }
  /**
   * Comparison operators.
   *
   * \note the comparisons work on the identifiers, not the contents!
   */
  bool operator<(const Vertex& other) const {
    return id() < other.id();
  }
  /**
   * Comparison operators.
   *
   * \note the comparisons work on the identifiers, not the contents!
   */
  bool operator>=(const Vertex& other) const {
    return id() >= other.id();
  }
  /**
   * Comparison operators.
   *
   * \note the comparisons work on the identifiers, not the contents!
   */
  bool operator<=(const Vertex& other) const {
    return id() <= other.id();
  }

  /**
   * Test if a vertex is a null vertex.
   */
  bool isNull() const {
    return content == 0;
  }

  /**
   * Return the identifier of a vertex.
   */
  identity_t id() const {
    return (identity_t)content;
  }

  /**
   * Return a number unique to each vertex, globally.
   */
  size_t num() const {
    return content->num;
  }

  /**
   * Convert a vertex to \c true if it is not null.
   */
  operator bool() const { return content; }

  /**
   * Return \c true if the current object hold a weak reference on a vertex.
   */
  bool isWeakRef() const {
    return false;
  }

  /**
   * Construct a weak reference on the current vertex
   */
  weak_ref_t weakRef() const;

  /**
   * Null vertex.
   *
   * Useful to return a constant reference on a vertex all the time.
   *
   * Example:
   * \code
   * const vertex& fct(bool cond)
   * {
   *   static vertex a;
   *   if(cond)
   *     return a;
   *    return vertex::null;
   * }
   * \endcode
   */
  static Vertex null;

  /**
   * Serialization method
   *
   * \warning You need to include <storage/graph.h> to use this
   * serialization method
   */
  // bool serialize(storage::VVEStorage&);

  unsigned int count() const
  {
    if(content)
      return content->count;
    return 0;
  }

protected:
  /**
   * Content of the vertex
   *
   * This member is mutable to allow for modification of constant
   * references. This is useful as no operation on the vertex depend on
   * this.
   */
  mutable CountedContent* content;

  /**
   * Release the current pointer
   */
  void release();

  /**
   * Acquire the current pointer
   */
  void acquire();
};

template <TEMPLATE_VERTEX>
typename Vertex<VERTEX_ARGS>::CountedContent::alloc_t Vertex<VERTEX_ARGS>::CountedContent::alloc;

template <TEMPLATE_VERTEX> Vertex<VERTEX_ARGS> Vertex<VERTEX_ARGS>::null(0);

template <TEMPLATE_VERTEX> class WeakVertex : public Vertex<VERTEX_ARGS> {
  typedef typename Vertex<VERTEX_ARGS>::CountedContent CountedContent;

public:
  /**
   * Type of the identifier of the vertex
   */
  typedef vertex_identity_t identity_t;

  /**
   * Type of the content of the vertex
   */
  typedef VertexContent content_t;

  /**
   * Strong reference corresponding to the weak one
   */
  typedef Vertex<VERTEX_ARGS> strong_ref;

  /**
   * Type of the equivalent pointer
   */
  typedef VertexContent* pointer;

  /**
   * Construct an empty weak vertex.
   *
   * At the difference of normal vertices, constructing a weak vertex creates
   * a null one.
   */
  WeakVertex()
    : Vertex<VERTEX_ARGS>(0)
  {
  }

  /**
   * Construct a weak reference from a strong one
   */
  WeakVertex(const strong_ref& v)
    : Vertex<VERTEX_ARGS>(0)
  {
    this->content = v.content;
  }

  /**
   * Copy constructor
   */
  WeakVertex(const WeakVertex& copy)
    : Vertex<VERTEX_ARGS>(0)
  {
    this->content = copy.content;
  }

  /**
   * Construct a weak reference from an id
   */
  explicit WeakVertex(const identity_t& id)
    : Vertex<VERTEX_ARGS>(0)
  {
    this->content = reinterpret_cast<CountedContent*>(id);
    if(this->content and this->content->count == 0)
      this->content = 0;
  }

  /**
   * Set the content of the weak reference
   */
  WeakVertex& operator=(const WeakVertex& other)
  {
    this->content = other.content;
    return *this;
  }

  WeakVertex& operator=(const identity_t& id)
  {
    this->content = reinterpret_cast<CountedContent*>(id);
    return *this;
  }

  WeakVertex& operator=(const strong_ref& other)
  {
    this->content = other.content;
    return *this;
  }

  ~WeakVertex() {
    this->content = 0;
  }

  bool isNull() const {
    return this->content == 0 or this->content->count == 0;
  }

protected:
};

template <TEMPLATE_VERTEX> Vertex<VERTEX_ARGS>::~Vertex() {
  this->release();
}

template <TEMPLATE_VERTEX>
Vertex<VERTEX_ARGS>::Vertex()
  : content(0)
{
  content = CountedContent::New();
}

template <TEMPLATE_VERTEX>
Vertex<VERTEX_ARGS>::Vertex(identity_t id)
  : content(reinterpret_cast<CountedContent*>(id))
{
  acquire();
}

//  template <TEMPLATE_VERTEX>
//    Vertex<VERTEX_ARGS>::Vertex( VertexContent* data )
//    : content(0)
//  {
//    CountedContent *cdata = dynamic_cast<CountedContent*>(data);
//    if(cdata != 0)
//    {
//      content = cdata;
//      ++(content->count);
//    }
//    else
//    {
//      content = CountedContent::New();
//      *(VertexContent*)content = *data;
//    }
//  }

template <TEMPLATE_VERTEX>
Vertex<VERTEX_ARGS>::Vertex(const typename Vertex<VERTEX_ARGS>::weak_ref_t& copy)
  : content(copy.content)
{
  acquire();
}

template <TEMPLATE_VERTEX>
Vertex<VERTEX_ARGS>::Vertex(const Vertex& copy)
  : content(copy.content)
{
  acquire();
}

template <TEMPLATE_VERTEX> Vertex<VERTEX_ARGS>& Vertex<VERTEX_ARGS>::operator=(const identity_t& id)
{
  if((identity_t)content == id)
    return *this;
  this->release();
  content = reinterpret_cast<CountedContent*>(id);
  acquire();
  return *this;
}

template <TEMPLATE_VERTEX> Vertex<VERTEX_ARGS>& Vertex<VERTEX_ARGS>::operator=(const VertexContent* id)
{
  const CountedContent* cid = dynamic_cast<const CountedContent*>(id);
  if(cid != 0)
    return *this = (const identity_t&)cid;
  else {
    return *this = (const identity_t&)(*(CountedContent::New(*id)));
  }
}

template <TEMPLATE_VERTEX> Vertex<VERTEX_ARGS>& Vertex<VERTEX_ARGS>::operator=(const weak_ref_t& copy)
{
  if(content == copy.content)
    return *this;
  this->release();
  content = copy.content;
  acquire();
  return *this;
}

template <TEMPLATE_VERTEX> void Vertex<VERTEX_ARGS>::acquire()
{
  if(content) {
    if(content->count == 0)
      content = 0;
    else
#pragma omp atomic
      ++(content->count);
  }
}

template <TEMPLATE_VERTEX> void Vertex<VERTEX_ARGS>::release()
{
  if(content) {
#pragma omp atomic
    --(content->count);
    if(content->count == 0) {
      CountedContent::Delete(content);
    }
  }
}

template <TEMPLATE_VERTEX> typename Vertex<VERTEX_ARGS>::weak_ref_t Vertex<VERTEX_ARGS>::weakRef() const
{
  return weak_ref_t(*this);
}

template <TEMPLATE_VERTEX> Vertex<VERTEX_ARGS>& Vertex<VERTEX_ARGS>::operator=(const Vertex& other)
{
  if(content == other.content) {
    return *this;
  } else
    this->release();
  content = other.content;
  acquire();
  return *this;
}

template <TEMPLATE_VERTEX, typename charT>
std::basic_ostream<charT>& operator<<(std::basic_ostream<charT>& ss, const Vertex<VERTEX_ARGS>& v)
{
  ss << "Vertex<" << util::demangle(typeid(VertexContent).name()) << ">(" << v.id() << ")";
  return ss;
}
} // namespace graph
} // namespace lgx

namespace std {
#ifdef HASH_NEED_TR1
namespace tr1 {
#endif
template <TEMPLATE_VERTEX>
struct hash<lgx::graph::Vertex<VERTEX_ARGS> > {
  size_t operator()(const lgx::graph::Vertex<VERTEX_ARGS>& v) const
  {
    return ((size_t)v.id()) >> 2;     // Shift to consider memory alignment!
  }
};

template <TEMPLATE_VERTEX>
struct hash<lgx::graph::WeakVertex<VERTEX_ARGS> > {
  size_t operator()(const lgx::graph::WeakVertex<VERTEX_ARGS>& v) const
  {
    return ((size_t)v.id()) >> 2;     // Shift to consider memory alignment!
  }
};
#ifdef HASH_NEED_TR1
} // namespace tr1
#endif
} // namespace std

#endif // VERTEX_HPP
