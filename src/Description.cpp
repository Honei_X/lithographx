/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

/*
 * This file is part of LithoGraphX
 *
 * LithoGraphX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LithoGraphX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Description.hpp"

namespace lgx {
QString& Description::fieldName(size_t idx) {
  return _fields[idx].first;
}

const QString& Description::fieldName(size_t idx) const {
  return _fields[idx].first;
}

QStringList Description::fieldNames() const
{
  QStringList res;
  for(const_iterator it = begin(); it != end(); ++it)
    res << it->first;
  return res;
}

QString& Description::operator[](size_t idx) {
  return _fields[idx].second;
}

const QString& Description::operator[](size_t idx) const {
  return _fields[idx].second;
}

QString& Description::operator[](const QString& name)
{
  for(iterator it = begin(); it != end(); ++it)
    if(it->first == name)
      return it->second;
  _fields.push_back(std::make_pair(name, QString()));
  return _fields.back().second;
}

QString Description::field(const QString& name, const QString& def) const
{
  for(const_iterator it = begin(); it != end(); ++it)
    if(it->first == name)
      return it->second;
  return def;
}

bool Description::remove(const QString& name)
{
  for(iterator it = begin(); it != end(); ++it)
    if(it->first == name) {
      erase(it);
      return true;
    }
  return false;
}

bool Description::remove(size_t i)
{
  if(i < size()) {
    iterator it = begin() + i;
    erase(it);
    return true;
  }
  return false;
}

QString Description::field(size_t i, const QString& def) const
{
  if(i < size())
    return _fields[i].second;
  return def;
}

Description& Description::operator<<(const std::pair<QString, QString>& p)
{
  _fields.push_back(p);
  return *this;
}

namespace {
QString shieldCSV(const QString& s)
{
  if(s.contains(","))
    return QString("%1").arg(s);
  return s;
}

bool prepareNextField(QStringList& fields)
{
  QString val = fields[0];
  if(not val.isEmpty() and val[0] == '"') {
    if(val.size() > 1) {
      if(val[val.size() - 1] == '"') {
        // We have a full field, remove the quotes and return
        val = val.mid(1, val.size() - 2);
        fields[0] = val;
        return true;
      }
    }
    // Otherwise, we need to merge the two first entries and process further
    if(fields.size() < 2)
      return false;
    fields.pop_front();
    fields[0] = QString("%1,%2").arg(val).arg(fields[0]);
    return prepareNextField(fields);
  }
  // We can use the value as-is
  return true;
}
} // namespace

QString Description::toCSV() const
{
  QStringList clean;
  clean << shieldCSV(_type);
  for(const_iterator it = begin(); it != end(); ++it)
    clean << shieldCSV(it->first) << shieldCSV(it->second);
  return clean.join(",");
}

Description Description::fromCSV(const QString& value, bool* ok)
{
  QStringList fields = value.split(",");
  Description res;
  bool good = prepareNextField(fields);
  if(ok)
    *ok = true;
  if(not good) {
    if(ok)
      *ok = false;
    return res;
  }
  res._type = fields.front();
  fields.pop_front();
  while(not fields.empty()) {
    good = prepareNextField(fields);
    if(not good) {
      res.clear();
      if(ok)
        *ok = false;
      return res;
    }
    QString name = fields.front();
    fields.pop_front();

    if(fields.empty()) {
      res.clear();
      if(ok)
        *ok = false;
      return res;
    }

    good = prepareNextField(fields);
    if(not good) {
      res.clear();
      if(ok)
        *ok = false;
      return res;
    }
    QString value = fields.front();
    fields.pop_front();

    res._fields.push_back(std::make_pair(name, value));
  }
  return res;
}
} // namespace lgx
