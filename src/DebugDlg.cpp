/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "DebugDlg.hpp"

DebugDlg::DebugDlg(LithoViewer* v, QWidget* parent, Qt::WindowFlags f)
  : QDialog(parent, f)
  , viewer(v)
{
  ui.setupUi(this);
  if(ui.peeling->isChecked())
    viewer->_showSlice = ui.peelingSlice->value();
  else
    viewer->_showSlice = -1;
  viewer->_sliceType = ui.peelingType->currentIndex();
}

void DebugDlg::on_peeling_toggled(bool on)
{
  if(on)
    viewer->_showSlice = ui.peelingSlice->value();
  else
    viewer->_showSlice = -1;
  viewer->update3D();
}

void DebugDlg::on_peelingSlice_valueChanged(int val)
{
  viewer->_showSlice = val;
  viewer->update3D();
}

void DebugDlg::on_peelingType_currentIndexChanged(int val)
{
  viewer->_sliceType = val;
  viewer->update3D();
}
