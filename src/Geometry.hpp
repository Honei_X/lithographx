/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef GEOMETRY_H
#define GEOMETRY_H
/**
 * \file Geometry.hpp
 *
 * Common definitions and utilities for all geometry algorithms
 * This file is shared by cuda, do not include headers that nvcc can't handle (i.e. Qt)
 */

#include <LGXConfig.hpp>

#include <BoundingBox.hpp>
#include <Matrix.hpp>
#include <Vector.hpp>

#include <cmath>
#include <cuda/CudaGlobal.hpp>
#include <float.h>
#include <cmath>

namespace lgx {
/**
 * Algorithmic %geometry
 *
 * This namespace contains different useful %geometry %algorithms, like
 * intersections, areas, ...
 */
typedef unsigned char ubyte;
#if !defined(_MSC_VER) || !defined(Q_OS_WIN32) // otherwise, a bug in VC++ prevents using hash maps
typedef unsigned int uint;
typedef unsigned short ushort;
typedef unsigned long ulong;
#endif

typedef util::Vector<2, float> Point2f;
typedef util::Vector<3, float> Point3f;
typedef util::Vector<4, float> Point4f;
typedef util::Vector<5, float> Point5f;
typedef util::Vector<6, float> Point6f;
typedef util::Vector<12, float> Point12f;

typedef util::Vector<2, double> Point2d;
typedef util::Vector<3, double> Point3d;
typedef util::Vector<4, double> Point4d;
typedef util::Vector<5, double> Point5d;
typedef util::Vector<6, double> Point6d;
typedef util::Vector<16, double> Point16d;

typedef util::Vector<2, int> Point2i;
typedef util::Vector<3, int> Point3i;
typedef util::Vector<4, int> Point4i;
typedef util::Vector<5, int> Point5i;
typedef util::Vector<6, int> Point6i;

typedef util::Vector<2, uint> Point2u;
typedef util::Vector<3, uint> Point3u;
typedef util::Vector<4, uint> Point4u;
typedef util::Vector<5, uint> Point5u;
typedef util::Vector<6, uint> Point6u;

typedef util::Vector<2, size_t> Point2s;
typedef util::Vector<3, size_t> Point3s;
typedef util::Vector<4, size_t> Point4s;
typedef util::Vector<5, size_t> Point5s;
typedef util::Vector<6, size_t> Point6s;

typedef util::Vector<3, ushort> Point3us;

typedef util::Matrix<2, 2, float> Matrix2f;
typedef util::Matrix<3, 3, float> Matrix3f;
typedef util::Matrix<4, 4, float> Matrix4f;

typedef util::Matrix<2, 2, double> Matrix2d;
typedef util::Matrix<3, 3, double> Matrix3d;
typedef util::Matrix<4, 4, double> Matrix4d;

typedef util::BoundingBox<3, uint> BoundingBox3u;
typedef util::BoundingBox<3, int> BoundingBox3i;
typedef util::BoundingBox<3, float> BoundingBox3f;

// Min
template <typename T>
CU_HOST_DEVICE T min(const T a, const T b)
{
  if(a <= b)
    return a;
  return b;
}

// Max
template <typename T>
CU_HOST_DEVICE T max(const T a, const T b)
{
  if(a >= b)
    return a;
  return b;
}

// Trim to min/max
template <typename T>
CU_HOST_DEVICE T trim(const T x, const T minx, const T maxx) {
  return max(minx, min(maxx, x));
}

CU_HOST_DEVICE
Point3u toVoxelsCeil(const Point3f& p, const Point3f& step)
{
  Point3u r(0, 0, 0);
  if(p.x() > 0 and step.x() > 0)
    r.x() = uint(ceil(p.x() / step.x()));
  if(p.y() > 0 and step.y() > 0)
    r.y() = uint(ceil(p.y() / step.y()));
  if(p.z() > 0 and step.z() > 0)
    r.z() = uint(ceil(p.z() / step.z()));
  return r;
}

// Multiple openGL 4D matrix by 3D point and return 3D point
template <typename T>
CU_HOST_DEVICE util::Vector<3, T> multMatrix4Point3(const util::Matrix<4, 4, T>& m, const util::Vector<3, T>& p)
{
  util::Vector<4, T> t(p.x(), p.y(), p.z(), T(1.0));
  t = m * t;
  util::Vector<3, T> res(t.x() / t.t(), t.y() / t.t(), t.z() / t.t());
  return res;
}

template <typename T> T CU_HOST_DEVICE interpolate(const T a, const T b, const T s)
{
  T t = trim(s, (T)0.0, (T)1.0);
  return ((1.0 - t) * a + t * b);
}

// CU_HOST_DEVICE
// Point3f getEulerAngles(Matrix4d &m)
//{
//  Point3f r(0,0,0);
//  if(fabs(m(1,0)) > .1)
//    r.x() = atan2(m(0,2), m(2,2)) / M_PI * 180;
//  else
//    r.x() = atan2(-m(2,0), m(0,0)) / M_PI * 180;
//  r.y() = atan2(-m(1,2), m(2,2)) / M_PI * 180;
//  r.z() = asin(m(1,0)) / M_PI * 180;
//  return(r);
//}

// Plane-Line Intersection, 0 <= s <= 1, on line, s >= 0 ray intersect
CU_HOST_DEVICE
bool planeLineIntersect(Point3f p, Point3f n, Point3f u1, Point3f u2, float& s, Point3f& u)
{
  n.normalize();
  Point3f u1u2 = u2 - u1;
  float t = n * u1u2;
  // Check if parallel
  if(t == 0)
    return (false);
  s = n * (p - u1) / t;
  u = u1 + s * u1u2;
  return (true);
}

// Triangle area
CU_HOST_DEVICE
float triangleArea(Point3f a, Point3f b, Point3f c) {
  return ((a - b).cross(a - c).norm() / 2.0f);
}

// Signed volume of a tetrahedron
CU_HOST_DEVICE
float signedTetraVolume(Point3f a, Point3f b, Point3f c)
{
  return ((-c.x() * b.y() * a.z() + b.x() * c.y() * a.z() + c.x() * a.y() * b.z() - a.x() * c.y() * b.z()
           - b.x() * a.y() * c.z() + a.x() * b.y() * c.z()) / 6.0f);
}

// Get (a set of) basis vectors from a plane
CU_HOST_DEVICE
void getBasisFromPlane(const Point3f& nrml, Point3f& x, Point3f& y, Point3f& z)
{
  Point3f n = nrml;
  n.normalize();
  x = Point3f(1, 0, 0);
  y = Point3f(0, 1, 0);
  z = Point3f(0, 0, 1);
  if(z.cross(n).norm() != 0) {
    y = z.cross(n);
    y.normalize();
    x = y.cross(n);
    x.normalize();
  } else
    x = n * z * x;
  z = n;
}

// Intersect a ray with a 3D triangle
//    Input:  ray r1,r2 triangle t0,t1,t2
//    Output: intp = intersection point (when it exists)
//    Return: -1 = triangle is degenerate (a segment or point)
//             0 = disjoint (no intersect)
//             1 = intersect in unique point I1
//             2 = are in the same plane
CU_HOST_DEVICE
int rayTriangleIntersect(const Point3f& r0, const Point3f& r1, const Point3f& t0, const Point3f& t1, const Point3f& t2,
                         Point3f& intp)
{
  // get triangle edge vectors and plane normal
  Point3f u = t1 - t0;
  Point3f v = t2 - t0;
  Point3f n = u.cross(v);
  if(n.x() == 0 && n.y() == 0 && n.z() == 0)   // triangle is degenerate
    return -1;

  Point3f dir = r1 - r0;   // ray direction vector
  Point3f w0 = r0 - t0;
  float a = -n * w0;
  float b = n * dir;
  if(b == 0) {     // ray is parallel to triangle plane
    if(a == 0)     // ray lies in triangle plane
      return 2;
    else
      return 0;       // ray disjoint from plane
  }

  // get intersect point of ray with triangle plane
  float r = a / b;
  if(r < 0.0f)    // ray goes away from triangle
    return 0;     // => no intersect
  // for a segment, also test if (r > 1.0) => no intersect

  intp = r0 + r * dir;   // intersect point of ray and plane

  // is intersect point inside the triangle?
  float uu, uv, vv, wu, wv, d;
  uu = u * u;
  uv = u * v;
  vv = v * v;
  Point3f w = intp - t0;
  wu = w * u;
  wv = w * v;
  d = uv * uv - uu * vv;

  // get and test parametric coords
  float s, t;
  s = (uv * wv - vv * wu) / d;
  if(s < 0.0f || s > 1.0f)   // outside
    return 0;
  t = (uv * wu - uu * wv) / d;
  if(t < 0.0f || (s + t) > 1.0f)   // outside
    return 0;

  return 1;   // in
}

// Return distance from a point to a line (or segment)
CU_HOST_DEVICE
float distLinePoint(Point3f v1, Point3f v2, Point3f p, bool segment)
{
  v2 -= v1;
  float n = norm(v2);
  // Points are the same
  if(n == 0)
    return norm(p - v1);

  Point3f vn = v2 / n;
  p -= v1;
  Point3f q = vn * (p * vn);
  float dist = norm(p - q);
  if(!segment)
    return dist;

  // Projected point on line segment
  float qn = p * vn;
  if(qn >= 0 and qn <= n)
    return dist;

  // Otherwise pick closest endpoint
  dist = norm(p);
  float d = norm(p - v2);
  if(d < dist)
    dist = d;

  return dist;
}
} // namespace lgx
#endif
