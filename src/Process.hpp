/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PROCESS_HPP
#define PROCESS_HPP

/**
 * \file Process.hpp
 *
 * File containing the definition of a Process.
 */

#include <LGXConfig.hpp>

#include <Information.hpp>
#include <Mangling.hpp>

#include <QIcon>
#include <QString>
#include <QStringList>
#include <QTextStream>

#include <algorithm>
#include <functional>
#include <iostream>
#include <typeinfo>

class QWidget;
class LithoGraphX;

/**
 * \def PROCESS_VERSION
 *
 * Number identifying the running version of the process API.
 * This makes sure the loaded processes will run with the current API. Otherwise, an error informing correctly the user
 * will be shown in the standard error.
 *
 * The version correspond to the last release where the process has changed,
 * with the major, minor and micro versions using each 2 hexadecimal digits.
 */
#define PROCESS_VERSION 0x010100

/**
 * \mainpage LithoGraphX Plug-in documentation
 *
 * Plug-ins in LithoGraphX are called processes. Most features in LithoGraphX are implemented
 * internally as processes.
 *
 * All processes are inherited from the process::Process class, a base class for the three
 * process types. If you want to create a process that modifies stack (voxel) data, you must
 * inherit from the process::StackProcess class. For mesh data use the process::MeshProcess
 * class. A process that inherits from the process::GlobalProcess class can change both
 * Stack and Mesh data.
 *
 * Plug-ins are compiled into shared object (.so) files and are loaded when LithoGraphX starts.
 * They can be installed in a system area for all users, or in the user's home directory. Run
 * the command:
 *
 * \verbatim $ LithoGraphX --all-process \endverbatim
 *
 * to print the plug-in directories.
 *
 * The best way to start is from a sample plug-in available from the LithoGraphX
 * website: www.LithoGraphX.org. An overview of the documentation for processes can
 * be found in the \ref process namespace.
 *
 * If you write a useful plug-in, please let us know so that we can incorporate it
 * into LithoGraphX.
 *
 * \defgroup StackProcess Stack Processes
 *
 * List of Stack processes
 *
 * \defgroup MeshProcess Mesh Processes
 *
 * List of Mesh processes
 *
 * \defgroup GlobalProcess Global Processes
 *
 * List of Global processes
 *
 * \defgroup ProcessUtils Process utilities
 *
 * Classes and functions needed to create your own processes or call other processes.
 *
 * \defgroup ProcessIntern Process internals
 *
 * Classes and functions used to implement the process, with registration, enumeration, calling, ...
 */

/**
 * \namespace lgx
 *
 * This namespace contains all the API of LithoGraphX
 */
namespace lgx {
/**
 * \namespace process
 *
 * This namespace contains all the classes needed to define processes.
 *
 * To create a new process, you first need to decide the category of the process:
 * Stack, Mesh or Global. Remember it will appear in the appropriate section. Also,
 * stack processes will be able to access only 3D data, and mesh
 * processes only the meshes. Here is a template of a minimal stack process:
 *
 * \section Basics
 *
 * \code
 * class DoNothing : public StackProcess
 * {
 * public:
 *   // Only the copy constructor is needed
 *   DoNothing(const StackProcess& proc)
 *     : Process(proc)
 *     , StackProcess(proc)
 *   { }
 *   // Define what the class do ... here it's nothing
 *   bool operator()(const QStringList&) { return true; }
 *
 *   // Now define the properties of the process
 *   QString name() const { return "Nothing"; }
 *   QString description() const { return "This process really does nothing."; }
 *   QStringList parmNames() const { return QStringList(); }
 *   QStringList parmDescs() const { return QStringList(); }
 * };
 * \endcode
 *
 * Then, in the implementation file, you need to add:
 * \code
 * REGISTER_STACK_PROCESS(DoNothing);
 * \endcode
 * Which will take care of registering the process to the system when the
 * library is loaded.
 *
 * \section Recommendations
 *
 * Beside the minimal process, it is recommended to structure the process in two functions:
 * the first one taking generic arguments (i.e. a string list and a float list), the other
 * one taking specific arguments. This way, your process will
 * be easier to use from another C++ process.
 *
 * Also, to help providing meaningful (and uniform) error messages, and to help you
 * testing the current state of the process, the checkState() method is provided.
 *
 * The structure then typically becomes:
 *
 * \code
 * class DoSomething : public StackProcess
 * {
 * public:
 *    DoSomething(const StackProcess& proc)
 *      : Process(proc)
 *      , StackProcess(proc)
 *    { }
 *
 *    // the operator() with generic arguments is mandatory to work
 *    bool operator()(const QStringList& parms)
 *    {
 *      if(!checkState().stack(STACK_NON_EMPTY)
 *                      .store(STORE_LABEL | STORE_WORK)
 *                      .mesh(MESH_NON_EMPTY))
 *        return false;
 *      Store *store = currentStack()->work();
 *      bool isneeded = stringToBool(parms[1]);
 *      QString aname = parms[2];
 *      float param = parms[3].toFloat();
 *      if((*this)(store, isneeded, aname, param))
 *      {
 *        work->show();
 *        return true;
 *      }
 *      return false;
 *    }
 *
 *    // The operator() with specialized arguments is recommended to be used by other C++ processes
 *    bool operator()(Store *store, bool isneeded, const QString& aname, float param)
 *    {
 *      // Do what is needed
 *      store->changed();
 *      return true;
 *    }
 *
 *    // Properties of the process
 *    QString name() const { return "DoSomething"; }
 *
 *    QString description() const { return "A process that does something"; }
 *
 *    // This time, we have four parameters
 *    QStringList parmNames() const {
 *      return QStringList() << "Store" << "Needed" << "Name" << "Param";
 *    }
 *
 *    // Defaults for paramters
 *    QStringList parmDefaults() const {
 *      return QStringList() << "Work" << "Yes" << "" << "0.0";
 *    }
 *
 *    // Some of the parms come from a choice list
 *    ParmChoiceMap parmChoice() const
 *    {
 *      ParmChoiceMap choice;
 *      choice[0] = storeChoice(); // Helper function to list "main" and "work"
 *      choice[1] = booleanChoice(); // Helper function to list "yes" and "no"
 *      return choice;
 *    }
 * };
 * \endcode
 */

class Clip;
class CuttingSurface;
class Mesh;
class Stack;
class Store;

/**
 * This scoped enumeration is used to select between the main, work and current
 * stores of a stack.
 *
 * \relates Stack
 */
enum STORE : unsigned char {
  CURRENT_STORE = 0, ///< Current store, if any
  MAIN_STORE = 1,    ///< Main store
  WORK_STORE = 2     ///< Work store
};

namespace process {
/**
 * Type of the dictionary giving the list of possible strings for each argument.
 *
 * If the choice is free, the position should just not be present in the dictionnary.
 * \ingroup ProcessUtils
 */
typedef QHash<int, QStringList> ParmChoiceMap;

#ifndef DOXYGEN
enum SystemCommand { LOAD_VIEW = 1, SAVE_VIEW, UPDATE_STRUCTURE, SET_CURRENT_STACK, TAKE_SNAPSHOT, RESET_PROJECT };

class PrivateProcess;
class SetupProcess;
class GlobalProcess;
#endif

/**
 * \class UserCancelException Process.hpp <Process.hpp>
 *
 * Exception launched when a user clicks the Cancel button.
 * When writing your own processes, you should use the userCancel() method.
 * \ingroup ProcessUtils
 */
class LGX_EXPORT UserCancelException : public std::exception {
public:
  UserCancelException()
    : std::exception()
  {
  }

  virtual ~UserCancelException() throw() override { }

  const char* what() const throw() override {
    return "Process canceled by user.";
  }
};

/**
 * \class NoSuchProcess Process.hpp <Process.hpp>
 *
 * Exception raised when trying to create a process that doesn't exist
 * \ingroup ProcessUtils
 */
class LGX_EXPORT NoSuchProcess : public std::exception {
public:
  NoSuchProcess(const QString& type, const QString& name)
    : std::exception()
  {
    message = QString("No process called %1.%2")
                .arg(type).arg(name).toUtf8();
  }

  virtual ~NoSuchProcess() throw() override { }

  const char* what() const throw() override {
    return message.data();
  }

  QByteArray message;
};


/**
 * \class BadProcessType Process.hpp <Process.hpp>
 *
 * Exception raised when trying to create a process of an incompatible type
 * (e.g. create a Mesh process from a Stack process or vice versa).
 * \ingroup ProcessUtils
 */
class LGX_EXPORT BadProcessType : public std::exception {
public:
  BadProcessType(const QString& sourceType, const QString& targetType)
    : std::exception()
  {
    message = QString("Cannot create a process of type %1 from a process a type %2")
                .arg(targetType).arg(sourceType).toUtf8();
  }

  virtual ~BadProcessType() throw() override { }

  const char* what() const throw() override {
    return message.data();
  }

  QByteArray message;
};


/**
 * \class ProcessError Process.hpp <Process.hpp>
 *
 * Exception notifying a serious process error, that typically cannot be
 * recovered from.
 * \ingroup ProcessUtils
 */
class LGX_EXPORT ProcessError : public std::exception {
public:
  ProcessError(const QString& msg)
    : std::exception()
  {
    message = msg.toUtf8();
  }

  virtual ~ProcessError() throw() override { }

  const char* what() const throw() override {
    return message.data();
  }

  QByteArray message;
};

/**
 * \class Process Process.hpp <Process.hpp>
 *
 * This is the main process class, the one all process inherit from.
 * Note that you should \b never inherit this class directly, as it won't
 * allow anything to be done. Instead, you should inherit one of the daughter
 * class: StackProcess, MeshProcess or GlobalProcess.
 * \ingroup ProcessUtils
 */
class LGX_EXPORT Process {
public:
  static unsigned int processVersion;

  typedef std::vector<Stack*>::iterator stack_iterator;
  typedef std::vector<Stack*>::const_iterator const_stack_iterator;

  typedef std::vector<Mesh*>::iterator mesh_iterator;
  typedef std::vector<Mesh*>::const_iterator const_mesh_iterator;

  /**
   * Default constructor.
   */
  Process();
  /**
   * Copy constructor.
   *
   * Note that you will always need to call this constructor explicitly from your process,
   * as Process is inherited virtually. So all leaf class need to initialize it.
   */
  Process(const Process& p);
  /**
   * Virtual destructor
   */
  virtual ~Process() {
  }

  /**
   * Method to be called anytime a file is acted on (i.e. saved/loaded).
   *
   * If needed, it will set the current folder to the one containing the file and start a session
   *
   * If it is a project file (i.e. ending in lgxp), then project_file should be set to true to force the change in
   * folder.
   */
  void actingFile(const QString& filename, bool project_file = false);

  /**
   * Get the file currently defining the path of the system
   */
  QString actingFile() const;

  /**
   * Return the python call describing the current process
   */
  QString pythonCall(const QStringList& parms) const;

  /**
   * Number of stacks available to the process
   */
  int stackCount() const;
  /**
   * Returns the ith stack, or 0 if there is no such stack.
   */
  Stack* stack(int i);
  /**
   * Returns the current stack (i.e. the stack currently selected by the user).
   */
  Stack* currentStack();
  /**
   * Return the id (i.e. number) of the current stack.
   */
  int currentStackId() const;
  /**
   * Change which stack is current.
   */
  void setCurrentStackId(int i);

  /// Iterate over all the stacks
  std::pair<stack_iterator, stack_iterator> stacks();
  /// Iterate over all the stacks
  std::pair<const_stack_iterator, const_stack_iterator> stacks() const;

  /**
   * Add a new stack to the process
   */
  Stack* addStack();

  /**
   * Delete the stack of given id
   *
   * \note The current implementation assumes there are at least two stacks always available
   */
  bool deleteStack(int i);

  /**
   * Returns the number of mesh available to the process
   */
  int meshCount() const;
  /**
   * Returns the ith mesh
   */
  Mesh* mesh(int i);
  /**
   * Returns the current mesh (i.e. the mesh currently selected by the user)
   */
  Mesh* currentMesh();
  /**
   * Returns the id (i.e. number) of the current mesh
   */
  int currentMeshId() const;
  /**
   * Change which mesh is current
   */
  void setCurrentMeshId(int i);

  /// Iterate over all the meshs
  std::pair<mesh_iterator, mesh_iterator> meshes();
  /// Iterate over all the meshs
  std::pair<const_mesh_iterator, const_mesh_iterator> meshes() const;

  /**
   * Add a mesh to the process for the given stack
   */
  Mesh* addMesh(const Stack* stack);

  /**
   * Remove a mesh from the process
   */
  bool deleteMesh(int i);

  /**
   * Get the current selected label
   */
  int selectedLabel() const;

  /**
   * Change the current selected label
   */
  void setSelectedLabel(int label);

  /**
   * Return the current setting for the global brightness level
   */
  float globalBrightness();

  /**
   * Return the current setting for the global contrast level
   */
  float globalContrast();

  /**
   * Change the current setting for the global brightness level
   *
   * The brightness is clamped to the range [-1;1]
   */
  void setGlobalBrightness(float value);

  /**
   * Change the current setting for the global contrast level
   *
   * The contrast is clamped to the range [0;2]
   */
  void setGlobalContrast(float value);

  /**
   * Returns if the user has mesh selection active
   */
  bool meshSelection() const;
  /**
   * Returns is the user has line border selection active
   */
  bool lineBorderSelection() const;

  /**
   * Update the state of the application from the current data.
   * The process is paused until it's done.
   */
  void updateState();
  /**
   * Force the viewer to refresh, without pausing the process.
   */
  void updateViewer();

  /**
   * Set an error message that will be displayed if the process returns false.
   */
  bool setErrorMessage(const QString& str);

  /**
   * Get the current error message
   */
  QString errorMessage() const;

  /**
   * Set a warning message that will be displayed if the process returns true.
   */
  void setWarningMessage(const QString& str);
  /**
   * Get the current warning message
   */
  QString warningMessage() const;

  /**
   * Throw an exception informing the system that the user canceled the current process.
   */
  void userCancel() const {
    throw UserCancelException();
  }

  /**
   * Get the name of the file that was used to load the current process (i.e. the lgxp file), if any.
   */
  const QString& file() const;

  /**
   * Return the object defining the first clipping region
   */
  Clip* clip1();
  /**
   * Return the object defining the second clipping region
   */
  Clip* clip2();
  /**
   * Return the object defining the third clipping region
   */
  Clip* clip3();

  /**
   * Return the object defining the first clipping region
   */
  const Clip* clip1() const;
  /**
   * Return the object defining the second clipping region
   */
  const Clip* clip2() const;
  /**
   * Return the object defining the third clipping region
   */
  const Clip* clip3() const;

  /**
   * Return the cutting surface.
   */
  CuttingSurface* cuttingSurface();
  /**
   * Return the cutting surface.
   */
  const CuttingSurface* cuttingSurface() const;

  // @{
  /// \name Functions to be overridden by the user

  /**
   * Implementation of the process with generic arguments.
   *
   * The number of arguments is guaranteed to be at least as many as the ones named in the process.
   */
  virtual bool operator()(const QStringList& parms) = 0;
  /**
   * Returns the name of the process. Note that the name must be a valid identifier in Python once the spaces are
   * replaced by underscores. Also, all underscore in the name will be replaced by spaces for user presentation.
   */
  virtual QString name() const = 0;
  /**
   * Returns a description of the process for the GUI.
   */
  virtual QString description() const = 0;
  /**
   * Folder in which to place the process.
   *
   * Sub-folders can be specified by placing '/' in the path
   *
   * \note This is a purely presentation parameter. It doesn't allow two processes to have the same name if they are
   * in different folders.
   */
  virtual QString folder() const {
    return QString();
  }
  /**
   * List of named parameters.
   *
   * There must be as many parameters than defaults.
   */
  virtual QStringList parmNames() const = 0;
  /**
   * List of parameters descriptions.
   *
   * There must be as many parameters than defaults.
   */
  virtual QStringList parmDescs() const {
    return QStringList();
  }
  /**
   * List of default parms.
   */
  virtual QStringList parmDefaults() const {
    return QStringList();
  }
  /**
   * Purely for GUI purposes, provides for some of the parms parameter a choice. In this case, the user will be
   * presented with a combo box presenting the provided list. Note that no mechanism forces the user to choose one
   * of this choice.
   */
  virtual ParmChoiceMap parmChoice() const {
    return ParmChoiceMap();
  }
  /**
   * Icon to use to represent the process in the GUI.
   *
   * Note that to use an icon present in the resources, you must precede the path with a colon (i.e.
   * ":/images/myicon.png".
   */
  virtual QIcon icon() const {
    return QIcon();
  }
  /**
   * This is an optional method that is called only when a process is launched from the GUI.
   *
   * If reimplemented, it receives a potential argument list, and should provide a GUI for editing it. Once the user
   * finishing the edition, this method should modify the provided arguments and return true. If the user aborts,
   * the method should return false. Note that returning false does not produce an error.
   *
   * \param parms List of arguments
   * \param parent Pointer to the parent widget. If the initialize method creates a GUI, it should be a child of this
   * widget.
   */
  virtual bool initialize(QStringList& /*parms*/, QWidget* /*parent*/) {
    return true;
  }
  // @}


  //@{
  ///\name Methods needed to launch other processes

  /**
   * Creates a process object by name
   */
  Process* makeProcess(const QString& processType, const QString& processName);

  /**
   * Launch a process with generic arguments.
   *
   * Note that exceptions are filtered and converted into an error message and the process returning false.
   */
  bool runProcess(Process& proc, const QStringList& parms) throw();

  /**
   * Launch a process by name
   *
   * Note that exceptions are filtered and converted into an error message and the process returning false.
   */
  bool runProcess(const QString& processType, const QString& processName, const QStringList& parms) throw();
  //@}

  /**
   * Returns a string identifying the process type.
   */
  virtual QString type() const = 0;

  /**
   * Returns the number of named string for this process
   */
  virtual uint numParms() const {
    return parmDefaults().size();
  }

  //@{
  ///\name Methods to get and check elements

  /**
   * Enumeration for the bitfield that identifies stack properties
   */
  enum StackCheckType {
    STACK_ANY = 0,              ///< Any stack
    STACK_NON_EMPTY = 0x01,     ///< Non-empty stack
    STACK_VISIBLE = 0x02,       ///< Visible stack
    STACK_EMPTY = 0x04,         ///< Empty stack

    STACK_SCALED = 0x08,              ///< Scaled stack
    STACK_TRANSFORMED = 0x10,         ///< Transformed stack
    STACK_NON_SCALED = 0x20,          ///< Non-Scaled stack
    STACK_NON_TRANSFORMED = 0x40,     ///< Non-Transformed stack
  };

  /**
   * Enumeration for the bitfield that identifies store properties
   *
   * \note This enumeration is compatible with the STORE enumeration, so they
   * can be combined safely.
   */
  enum StoreCheckType {
    STORE_CURRENT = 0x0000,         ///< Current store
    STORE_WORK = 0x0001,            ///< Work store
    STORE_MAIN = 0x0002,            ///< Main store

    STORE_NON_EMPTY = 0x0004,       ///< Non-Empty store
    STORE_VISIBLE = 0x0008,         ///< Visible store
    STORE_EMPTY = 0x0010,           ///< Empty store

    STORE_LABEL = 0x0020,           ///< Label store
    STORE_NON_LABEL = 0x0040,       ///< Non-Label store

    STORE_SCALED = 0x0080,          ///< Scaled store
    STORE_TRANSFORMED = 0x0100,     ///< Transformed store
    STORE_NON_SCALED = 0x0200,      ///< Non-Scaled store
    STORE_NON_TRANSFORMED = 0x0400, ///< Non-Transformed store
  };

  /**
   * Enumeration for the bitfield that identifies mesh properties
   */
  enum MeshCheckType {
    MESH_ANY = 0,                    ///< Any mesh
    MESH_NON_EMPTY = 0x000001,       ///< Non-empty mesh
    MESH_VISIBLE = 0x000002,         ///< Either mesh or surface are visible

    MESH_HEAT = 0x000004,            ///< Show the heat map
    MESH_LABEL = 0x000008,           ///< Show the label
    MESH_NORMAL = 0x000010,          ///< Show the normal

    MESH_SIGNAL = 0x000020,          ///< Show the signal
    MESH_TEXTURE = 0x000040,         ///< Show the texture
    MESH_IMAGE = 0x000080,           ///< Show the image

    MESH_SHOW_MESH = 0x000100,       ///< Show the mesh
    MESH_SHOW_SURF = 0x000200,       ///< Show the surface

    MESH_ALL = 0x000400,             ///< Show the all mesh
    MESH_BORDER = 0x000800,          ///< Show border only
    MESH_CELLMAP = 0x001000,         ///< Show the cell map

    MESH_CELLS = 0x002000,           ///< This mesh is a cell mesh
    MESH_IMG_TEX = 0x004000,         ///< This mesh has a texture
    MESH_SCALED = 0x008000,          ///< This mesh is scaled
    MESH_TRANSFORMED = 0x010000,     ///< This mesh is transformed

    MESH_EMPTY = 0x020000,           ///< Empty mesh
    MESH_NON_CELLS = 0x040000,       ///< This mesh is not a cell mesh
    MESH_NON_IMG_TEX = 0x080000,     ///< This mesh doesn't have an image texture
    MESH_NON_SCALED = 0x100000,      ///< This mesh is non-scaled
    MESH_NON_TRANSFORMED = 0x200000, ///< This mesh is non-transformed

    MESH_PARENT = 0x400000,          ///< Show the parent label
    MESH_LABEL_PARENT = 0x800000     ///< Show the parent or label
  };

  /**
   * Enumeration of the type of checks that can be performed
   */
  enum CheckType {
    CHECK_STACK = 0,     ///< Check stack properties
    CHECK_STORE = 1,     ///< Check store properties
    CHECK_MESH = 2       ///< Check mesh properties
  };

  enum CheckWhich {
    CHECK_CURRENT = -1     ///< Constant marking the element to check is the current one
  };

  /**
   * \class CheckState
   *
   * Class that construct a state check on the current process.
   */
  class LGX_EXPORT CheckState {
public:
    CheckState(const CheckState& copy);
    CheckState(Process* process);

    /**
     * Add a check for a store
     */
    CheckState& store(int checks = STORE_CURRENT, int which = CHECK_CURRENT);
    /**
     * Add a check for a stack
     */
    CheckState& stack(int checks = STACK_ANY, int which = CHECK_CURRENT);
    /**
     * Add a check for a mesh
     */
    CheckState& mesh(int checks = MESH_ANY, int which = CHECK_CURRENT);

    /**
     * A CheckState object converts to true if the checks hold on the current process
     */
    operator bool();

protected:
    void setError();

    struct ProcessReqs {
      int type;
      int checks;
      int which;
    };

    QList<ProcessReqs> reqs;
    Process* process;
  };

  friend class Process::CheckState;

  /**
   * Call this function and convert the result to a boolean. If the checks hold, it will return true. If not, it
   * will return false and set the error message to a standard, fully descriptive description of what conditions
   * should be met to use this process.
   *
   * Syntax:
   *
   * checkState().TYPE(CONSTRAINTS).TYPE(CONSTRAINTS)...;
   *
   * Example:
   *
   * if(!checkState().store(STORE_MAIN | STORE_NON_EMPTY)
   *                 .store(STORE_WORK | STORE_LABEL))
   *   return false;
   *
   */
  CheckState checkState();
  //@}

  PrivateProcess* p;

protected:
  //@{
  ///\name Internal methods

  /**
   * Method that can be used to launch a system command. The process programmer should not use this directly but use
   * one of the public convenience method instead.
   */
  bool systemCommand(SystemCommand cmd, const QStringList& parms);
  /**
   * Check stack properties
   */
  bool stackCheck(int checks, int which);
  /**
   * Check store properties
   */
  bool storeCheck(int checks, int which);
  /**
   * Check mesh properties
   */
  bool meshCheck(int checks, int which);

  /**
   * Generate a standardised string describing how the stack should be to not generate an error
   */
  QString stackError(int checks, int which);
  /**
   * Generate a standardised string describing how the store should be to not generate an error
   */
  QString storeError(int checks, int which);
  /**
   * Generate a standardised string describing how the mesh should be to not generate an error
   */
  QString meshError(int checks, int which);
  //@}
};

/**
 * \class StackProcess Process.hpp <Process.hpp>
 *
 * Stack processes have non-mutable access to meshes and mutable access to stacks.
 *
 * As such, they can only create other stack processes.
 * \ingroup ProcessUtils
 */
class LGX_EXPORT StackProcess : public virtual Process {
  friend class SetupProcess;
  friend class GlobalProcess;
  StackProcess() {
  }

public:
  /**
   * Copy constructor.
   *
   * This is the only way to create a stack process. It cannot be created without a pre-existing context.
   */
  StackProcess(const StackProcess& copy)
    : Process(copy)
  {
  }

  virtual QString type() const {
    return "Stack";
  }
  const Mesh* mesh(int i) {
    return Process::mesh(i);
  }
  const Mesh* currentMesh() {
    return Process::currentMesh();
  }
  std::pair<const_mesh_iterator, const_mesh_iterator> meshes() const {
    return Process::meshes();
  }
};

/**
 * \class MeshProcess Process.hpp <Process.hpp>
 *
 * Mesh processes have mutable access to meshes and non-mutable access to stacks
 *
 * As such, they can only create other mesh processes.
 * \ingroup ProcessUtils
 */
class LGX_EXPORT MeshProcess : public virtual Process {
  friend class SetupProcess;
  friend class GlobalProcess;
  MeshProcess() {
  }

public:
  /**
   * Copy constructor.
   *
   * This is the only way to create a mesh process. It cannot be created without a pre-existing context.
   */
  MeshProcess(const MeshProcess& copy)
    : Process(copy)
  {
  }

  virtual QString type() const {
    return "Mesh";
  }
  const Stack* stack(int i) {
    return Process::stack(i);
  }
  const Stack* currentStack() {
    return Process::currentStack();
  }
  std::pair<const_stack_iterator, const_stack_iterator> stacks() const {
    return Process::stacks();
  }
};

/**
 * \class GlobalProcess Process.hpp <Process.hpp>
 *
 * Global processes have full mutable access to all properties of the process.
 *
 * As such, they can create any kind of process.
 * \ingroup ProcessUtils
 */
class LGX_EXPORT GlobalProcess : public MeshProcess, public StackProcess {
  friend class SetupProcess;
  GlobalProcess() {
  }

public:
  /**
   * Copy constructor.
   *
   * This is the only way to create a global process. It cannot be created without a pre-existing context.
   */
  GlobalProcess(const GlobalProcess& copy)
    : Process(copy)
    , MeshProcess(copy)
    , StackProcess(copy)
  {
  }

  virtual QString type() const {
    return "Global";
  }
  Mesh* mesh(int i) {
    return Process::mesh(i);
  }
  Mesh* currentMesh() {
    return Process::currentMesh();
  }
  Stack* stack(int i) {
    return Process::stack(i);
  }
  Stack* currentStack() {
    return Process::currentStack();
  }
  std::pair<stack_iterator, stack_iterator> stacks() {
    return Process::stacks();
  }
  std::pair<const_stack_iterator, const_stack_iterator> stacks() const {
    return Process::stacks();
  }
  std::pair<mesh_iterator, mesh_iterator> meshes() {
    return Process::meshes();
  }
  std::pair<const_mesh_iterator, const_mesh_iterator> meshes() const {
    return Process::meshes();
  }

  bool setCurrentStack(int id, STORE which);

  bool resetProject();

  bool takeSnapshot(QString filename, float overSampling = 1.0f, int width = 0, int height = 0, int quality = 95,
                    bool expand_frustum = false);

  using Process::systemCommand;   //(SystemCommand cmd, const QStringList& parms);
};

///\addtogroup ProcessIntern
///@{

template <typename P> struct ProcessFactory {
  typedef P ProcessType;

  ProcessFactory()
    : cnt(0)
  {
  }
  ProcessFactory(const ProcessFactory&)
    : cnt(0)
  {
  }
  virtual ~ProcessFactory() {
  }

  virtual ProcessType* operator()(const ProcessType& process) const = 0;

  int cnt;
};

#  ifdef WIN32
template struct LGX_EXPORT ProcessFactory<StackProcess>;
template struct LGX_EXPORT ProcessFactory<MeshProcess>;
template struct LGX_EXPORT ProcessFactory<GlobalProcess>;
#  endif

template <typename P, typename Process>
struct ClassProcessFactory : public ProcessFactory<P> {
  ClassProcessFactory()
    : ProcessFactory<P>()
  {
  }
  ClassProcessFactory(const ClassProcessFactory& copy)
    : ProcessFactory<P>(copy)
  {
  }

  typedef P ProcessType;
  virtual ProcessType* operator()(const ProcessType& process) const {
    return new Process(process);
  }
};

/**
 * \class FactoryPointer
 *
 * Smart pointer to a factory, with reference counting.
 */
template <typename X>
struct FactoryPointer {
  typedef X ProcessType;
  typedef ProcessFactory<X> value;
  typedef ProcessFactory<X>* pointer;
  typedef ProcessFactory<X>& reference;
  typedef const ProcessFactory<X>* const_pointer;
  typedef const ProcessFactory<X>& const_reference;

  FactoryPointer()
    : p(0)
  {
  }

  FactoryPointer(pointer ptr)
    : p(ptr)
  {
    acquire();
  }

  template <typename T>
  FactoryPointer(T* ptr)
    : p(ptr)
  {
    acquire();
  }

  FactoryPointer(const FactoryPointer& other)
    : p(other.p)
  {
    acquire();
  }

  template <typename T>
  FactoryPointer(const FactoryPointer<T>& other)
    : p(other.p)
  {
    acquire();
  }

  ~FactoryPointer() {
    release();
  }

  operator bool() const { return bool(p); }

  ProcessType* operator()(const ProcessType& process) const {
    return (*p)(process);
  }

  template <typename T>
  FactoryPointer& operator=(const T* other)
  {
    if(p != other) {
      release();
      p = other;
      acquire();
    }
    return *this;
  }

  FactoryPointer& operator=(const FactoryPointer& other)
  {
    if(p != other.p) {
      release();
      p = other.p;
      acquire();
    }
    return *this;
  }

  template <typename T>
  FactoryPointer& operator=(const FactoryPointer<T>& other)
  {
    if(p != other.p) {
      release();
      p = other.p;
      acquire();
    }
    return *this;
  }

  bool operator==(const FactoryPointer& other) const {
    return p == other.p;
  }

  bool operator!=(const FactoryPointer& other) const {
    return p != other.p;
  }

  reference operator*() {
    return *p;
  }
  pointer operator->() {
    return p;
  }

  const_reference operator*() const {
    return *p;
  }
  const_pointer operator->() const {
    return p;
  }

  uintptr_t id() const {
    return (uintptr_t)p;
  }

private:
  void release()
  {
    if(p) {
      p->cnt--;
      if(p->cnt == 0) {
        if(DEBUG)
          Information::out << "Deleting factory for " << util::qdemangle(typeid(*p).name())
                           << " at address 0x" << QString::number(id(), 16) << endl;
        delete p;
      }
      p = 0;
    }
  }

  void acquire()
  {
    if(p)
      p->cnt++;
  }

  pointer p;
};

/**
 * \class Registration
 *
 * Class managing the registration of a single process of a given type P
 *
 * P must be one of StackProcess, MeshProcess or GlobalProcess.
 */
template <typename P>
struct LGX_EXPORT Registration {
  typedef P ProcessType;
  typedef FactoryPointer<P> processFactory;
  typedef QList<processFactory> factoryList;

  Registration(processFactory f, QString class_name, unsigned int compiledVersion);
  Registration(Registration&& other);
  ~Registration();
  static factoryList& processFactories();

  processFactory factory;
  QString classname;

private:
  static factoryList factories;
};

#ifndef WIN32
extern template struct Registration<StackProcess>;
extern template struct Registration<MeshProcess>;
extern template struct Registration<GlobalProcess>;
#endif

typedef Registration<StackProcess> StackRegistration;
typedef Registration<MeshProcess> MeshRegistration;
typedef Registration<GlobalProcess> GlobalRegistration;

/**
 * \class MacroRegistrarDefinition
 *
 * Class managing a macro registrar, that is a pair of functions to load and unload
 * macro processes.
 */
class LGX_EXPORT MacroRegistrarDefinition
{
public:
  typedef std::function<bool(void)> fct_t;
  MacroRegistrarDefinition(const QString& name, fct_t loader, fct_t unloader);
  ~MacroRegistrarDefinition();

  bool valid() const { return not _name.isEmpty(); }
  operator bool() const { return valid(); }

  const QString& name() const { return _name; }

private:
  QString _name;
};

/**
 * \def REGISTER_MACRO_LOADER(FctName)
 *
 * Register a function loading macro modules
 */
#define REGISTER_MACRO(Name, Loader, Unloader) \
  static lgx::process::MacroRegistrarDefinition macro ## Name ## registration(#Name, Loader, Unloader)

LGX_EXPORT QStringList macroRegistrars();

/**
 * Return the loading function of a macro.
 *
 * If the macro doesn't exist, returns a function that returns always false and display an appropriate error message
 */
LGX_EXPORT std::function<bool(void)> macroLoader(const QString& name);
/**
 * Return the unloading function of a macro.
 *
 * If the macro doesn't exist, returns a function that returns always false and display an appropriate error message
 */
LGX_EXPORT std::function<bool(void)> macroUnloader(const QString& name);

///@}

/**
 * \struct BaseProcessDefinition Process.hpp <Process.hpp>
 *
 * Definition of a process, without the process factory.
 * \ingroup ProcessUtils
 */
struct LGX_EXPORT BaseProcessDefinition {
  BaseProcessDefinition() { }
  BaseProcessDefinition(const BaseProcessDefinition&) = default;
  virtual ~BaseProcessDefinition() { }

  BaseProcessDefinition& operator=(const BaseProcessDefinition&) = default;

#ifndef _MSC_VER
  BaseProcessDefinition(BaseProcessDefinition&&) = default;
  BaseProcessDefinition& operator=(BaseProcessDefinition&&) = default;
#else
  BaseProcessDefinition(BaseProcessDefinition&& other)
    : name(std::move(other.name))
    , description(std::move(other.description))
    , parmNames(std::move(other.parmNames))
    , parmDescs(std::move(other.parmDescs))
    , parms(std::move(other.parms))
    , folder(std::move(other.folder))
    , icon(std::move(other.icon))
    , parmChoice(std::move(other.parmChoice))
  { }
  BaseProcessDefinition& operator=(BaseProcessDefinition&& other)
  {
    name = std::move(other.name);
    description = std::move(other.description);
    parmNames = std::move(other.parmNames);
    parmDescs = std::move(other.parmDescs);
    parms = std::move(other.parms);
    folder = std::move(other.folder);
    icon = std::move(other.icon);
    parmChoice = std::move(other.parmChoice);
    return *this;
  }
#endif

  QString name;            ///< Name of the process
  QString description;     ///< Description of the process
  QStringList parmNames;   ///< List of named parameters of the process
  QStringList parmDescs;   ///< List of descriptions of named parameters
  QStringList parms;       ///< List of parameters/parameter defaults
  QString folder;          ///< Folder in which to place the process
  QIcon icon;              ///< Icon of the process
  ParmChoiceMap parmChoice;   ///< Map of choices for parameters
};

/**
 * \class ProcessDefinition Process.hpp <Process.hpp>
 *
 * Definition of a process, including the process factory.
 * \ingroup ProcessUtils
 */
template <typename P> struct ProcessDefinition : public BaseProcessDefinition {
  /**
   * \typedef process_t
   *
   * Type of the generated process.
   */
  typedef P process_t;
  /**
   * \typedef Type of the process factory
   */
  typedef FactoryPointer<P> processFactory;
  /**
   * Process factory
   */
  processFactory factory;
};

///\addtogroup ProcessUtils
///@{

/**
 * \def REGISTER_STACK_PROCESS(ClassName)
 *
 * Register \c ClassName has a stack process. It must inherit StackProcess and have a constructor accepting a single
 * StackProcess.
 * \ingroup ProcessUtils
 */
#define REGISTER_STACK_PROCESS(ClassName)                                                                       \
  static lgx::process::StackRegistration ClassName ## registration(new ClassProcessFactory<StackProcess, ClassName>(), \
                                                                   typeid(ClassName).name(), PROCESS_VERSION)

/**
 * \def REGISTER_MESH_PROCESS(ClassName)
 *
 * Register \c ClassName has a mesh process. It must inherit MeshProcess and have a constructor accepting a single
 * MeshProcess.
 * \ingroup ProcessUtils
 */
#define REGISTER_MESH_PROCESS(ClassName)                                                                      \
  static lgx::process::MeshRegistration ClassName ## registration(new ClassProcessFactory<MeshProcess, ClassName>(), \
                                                                  typeid(ClassName).name(), PROCESS_VERSION)

/**
 * \def REGISTER_GLOBAL_PROCESS(ClassName)
 *
 * Register \c ClassName has a global process. It must inherit GlobalProcess and have a constructor accepting a single
 * GlobalProcess.
 * \ingroup ProcessUtils
 */
#define REGISTER_GLOBAL_PROCESS(ClassName)                                                                        \
  static lgx::process::GlobalRegistration ClassName ## registration(new ClassProcessFactory<GlobalProcess, ClassName>(), \
                                                                    typeid(ClassName).name(), PROCESS_VERSION)


/**
 * Retrieves the process definition from the type and name of the process
 */
LGX_EXPORT BaseProcessDefinition* getBaseProcessDefinition(const QString& processType, const QString& processName);

/// Get the parameters for a given process
LGX_EXPORT bool getLastParms(const Process& proc, QStringList& parms);
/// Get the default parameters for a given process (i.e. the ones defined by the process)
LGX_EXPORT bool getDefaultParms(const Process& proc, QStringList& parms);
/// Save the default parameters in memory
LGX_EXPORT bool saveDefaultParms(const Process& proc, const QStringList& parms);
/// Check if the parameters have enough defaults
LGX_EXPORT bool checkProcessParms(const Process& proc, const QStringList& parms, size_t* nbParms = 0);
/// Get the parameters for a given process
LGX_EXPORT bool getLastParms(const QString& processType, const QString& processName, QStringList& parms);
/// Get the default parameters for a given process (i.e. the ones defined by the process)
LGX_EXPORT bool getDefaultParms(const QString& processType, const QString& processName, QStringList& parms);
/// Save the default parameters in memory
LGX_EXPORT bool saveDefaultParms(const QString& processType, const QString& processName, const QStringList& parms);
/// Check if the parameters have enough defaults
LGX_EXPORT bool checkProcessParms(const QString& processType, const QString& processName, const QStringList& parms,
                                  size_t* nbParms = 0);

/**
 * Returns the list of names of the processes of a given type
 */
LGX_EXPORT QStringList listProcesses(const QString& processType);

/**
 * Check if \c processType is a valid type (i.e. it exists)
 */
LGX_EXPORT bool validProcessType(const QString& processType);
/**
 * Check if the \c processName exist in the list of \c processType.
 */
LGX_EXPORT bool validProcessName(const QString& processType, const QString& processName);

/// Helper function converting a string into a boolean.
LGX_EXPORT bool stringToBool(const QString& string);
/// Returns which store has been selected by a process
LGX_EXPORT STORE stringToStore(const QString& string);
/// Return a string representing the store
LGX_EXPORT QString storeToString(STORE which);
/**
 * Helper function that provides a list of choices for a boolean argument
 */
inline QStringList booleanChoice()
{
  return QStringList() << "Yes"
                       << "No";
}
/**
 * Helper function that provides a list of choices for choosing from the main or work stack
 */
inline QStringList storeChoice()
{
  return QStringList() << "Main"
                       << "Work";
}

/// Helper function converting a boolean into a string
inline QString boolToString(bool b) {
  return (b ? "Yes" : "No");
}

///@}
} // namespace process
} // namespace lgx

#endif
