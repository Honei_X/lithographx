uniform sampler2D depth;
uniform sampler2D color;
uniform sampler2D background; // depth!

varying vec2 texCoord;

void main()
{
  float d = texture2D(depth, texCoord).a;
  float a = texture2D(color, texCoord).a;
  float bg = texture2D(background, texCoord).a;
  if((d >= bg) || (d >= 0.999999) || (a >= 0.99))
    discard;
  else
    gl_FragColor = vec4(0,1,0,1);
}

