/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ProcessThread.hpp"
#include <QCoreApplication>
#ifdef _OPENMP
#  include <omp.h>
#endif // _OPENMP

#include <exception>

namespace lgx {
static const QString& system_error = "System exception caught";

void ProcessThread::run()
{
  try {
#ifdef _OPENMP
    int nb_threads = omp_get_max_threads();

#  ifdef __linux__
    {
      size_t mask_size = CPU_ALLOC_SIZE(nb_threads);
      cpu_set_t* mask = CPU_ALLOC(nb_threads);
      CPU_ZERO_S(mask_size, mask);
      for(int i = 0; i < nb_threads; ++i)
        CPU_SET_S(i, mask_size, mask);
      if(sched_setaffinity(0, mask_size, mask) != 0)
        perror("Error, could not set affinity to all threads: ");
      CPU_FREE(mask);
    }
#  endif // __linux__

    // Set OpenMP default behaviour
    omp_set_schedule(omp_sched_static, 0);
#endif // _OPENMP

    _status = (*process)(parms);
    if(!_status)
      _error = process->errorMessage();
    else
      _warning = process->warningMessage();
  }
  catch(std::string s) {
    _status = false;
    _error = QString::fromStdString(s);
  }
  catch(QString s) {
    _status = false;
    _error = s;
  }
  catch(std::exception& ex) {
    _status = false;
    _error = QString::fromLocal8Bit(ex.what());
  }
  catch(...) {
    _status = false;
    _error = system_error;
  }
}
} // namespace lgx
