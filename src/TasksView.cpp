/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "TasksView.hpp"

#include <QKeyEvent>
#include <QItemSelectionModel>
#include <QDragEnterEvent>
#include <QDragLeaveEvent>
#include <QDragMoveEvent>

#include <QMimeData>

#include <QTextStream>
#include <stdio.h>

static QTextStream err(stderr);

TasksView::TasksView(QWidget* parent)
  : QTreeView(parent)
{
  setDragDropMode(QAbstractItemView::InternalMove);
}

void TasksView::keyPressEvent(QKeyEvent* event)
{
  if(event->matches(QKeySequence::Delete)) {
    QItemSelectionModel* select = selectionModel();
    emit deleteItems(select->selectedIndexes());
    event->accept();
    return;
  }
  QTreeView::keyPressEvent(event);
}

const QString itemlist_format = QString("application/x-qabstractitemmodeldatalist");
const QString internal_format = QString("LithoGraphX/modeldatalist");

void TasksView::dragEnterEvent(QDragEnterEvent* event)
{
  const QStringList& lst = event->mimeData()->formats();
  if(lst.contains(itemlist_format) or lst.contains(internal_format))
    QTreeView::dragEnterEvent(event);
  else
    event->ignore();
}

void TasksView::dragLeaveEvent(QDragLeaveEvent* event) {
  QTreeView::dragLeaveEvent(event);
}

void TasksView::dragMoveEvent(QDragMoveEvent* event)
{
  QRect rect = event->answerRect();
  QModelIndex idx = indexAt(rect.center());
  if(idx.isValid()) {
    QTreeView::dragMoveEvent(event);
    return;
  }
  event->ignore();
}
