/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

/*
 * This file is part of LithoGraphX
 *
 * LithoGraphX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LithoGraphX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ALGORITHMS_HPP
#define ALGORITHMS_HPP

#include <iterator>

namespace lgx {
namespace util {
/**
 * \class Counter Algorithms.hpp <Algorithms.hpp>
 *
 * Counter iterator
 */
struct Counter {
  typedef size_t value_type;
  typedef const size_t* pointer;
  typedef const size_t& reference;
  typedef ptrdiff_t difference_type;
  typedef std::random_access_iterator_tag iterator_category;

  explicit Counter(size_t value = 0)
    : _value(value)
  {
  }

  Counter(const Counter& copy)
    : _value(copy._value)
  {
  }

  reference operator*() const {
    return _value;
  }
  pointer operator->() const {
    return &_value;
  }

  Counter& operator++()
  {
    ++_value;
    return *this;
  }
  Counter& operator--()
  {
    --_value;
    return *this;
  }

  Counter operator++(int)
  {
    Counter copy(*this);
    ++(*this);
    return copy;
  }
  Counter operator--(int)
  {
    Counter copy(*this);
    --(*this);
    return copy;
  }

  Counter& operator+=(size_t value)
  {
    _value += value;
    return *this;
  }
  Counter& operator-=(size_t value)
  {
    _value -= value;
    return *this;
  }

  bool operator==(const Counter& other) const {
    return _value == other._value;
  }
  bool operator!=(const Counter& other) const {
    return _value != other._value;
  }
  bool operator>=(const Counter& other) const {
    return _value >= other._value;
  }
  bool operator<=(const Counter& other) const {
    return _value <= other._value;
  }
  bool operator>(const Counter& other) const {
    return _value > other._value;
  }
  bool operator<(const Counter& other) const {
    return _value < other._value;
  }

private:
  size_t _value;
};

inline Counter operator+(const Counter& c1, ptrdiff_t value)
{
  Counter copy(c1);
  copy += value;
  return copy;
}

inline Counter operator+(ptrdiff_t value, const Counter& c1)
{
  Counter copy(c1);
  copy += value;
  return copy;
}

inline Counter operator-(const Counter& c1, ptrdiff_t value)
{
  Counter copy(c1);
  copy -= value;
  return copy;
}

inline ptrdiff_t operator-(const Counter& c1, const Counter& c2) {
  return *c1 - *c2;
}

/**
 * Apply a kernel on all the elements of container (given by iterators)
 *
 * If chunk_size is equal or less than 0, or if there aren't enough elements to justify dynamic scheduling, static
 * scheduling is used.
 *
 * Progress must be a progress bar with the following methods:
 *  - advance(i) : the process is now at point i
 *  - canceled() : returns true if it has been canceled
 *
 * The progress will go from progress_start to progress_start+container.size()
 */
template <typename Kernel, typename Iterator, typename Progress>
bool apply_kernel_it(Kernel& op, Iterator first, Iterator last, Progress& progress, int progress_start = 0,
                     int chunk_size = 1000)
{
  size_t size = last - first;
  int toView = size / 100;
  int nextThreshold = progress_start;
  int taskCounter = progress_start, threadCounter = 0;
  bool success = true, global_success = true;

#ifdef _OPENMP
  int factor = omp_get_max_threads();
  if(chunk_size > 0 and size <= chunk_size * factor) {
#  pragma omp parallel for schedule(static) reduction(&& : success) firstprivate(taskCounter, \
    nextThreshold) shared(global_success)
    for(Iterator it = first; it < last; ++it) {
      taskCounter += factor;
      if(taskCounter >= nextThreshold) {
        if(omp_get_thread_num() == 0) {
          progress.advance(nextThreshold);
          nextThreshold += toView;
        }
#  pragma omp atomic update
        global_success &= success;
        success = global_success;
      }
      if(not success or progress.canceled())
        continue;
      success = success and op(*it);
    }
    return success;
  }
#endif

#pragma omp parallel for schedule(dynamic, chunk_size) firstprivate(threadCounter) shared(taskCounter, global_success) \
  reduction(&& : success)
  for(Iterator it = first; it < last; ++it) {
#ifdef _OPENMP
    if(omp_get_thread_num() == 0)
#endif
    {
      if(taskCounter >= nextThreshold) {
        progress.advance(nextThreshold);
        nextThreshold += toView;
      }
    }
    if(not success or progress.canceled())
#ifdef _OPENMP
      continue;
#else
      break;
#endif

    if(++threadCounter % chunk_size == 0) {
#pragma omp atomic update
      taskCounter += chunk_size;
#pragma omp atomic update
      global_success &= success;

      success = global_success;
    }
    success = success and op(*it);
  }
  return success;
}

template <typename Kernel, typename Container, typename Progress>
bool apply_kernel(Kernel& op, Container& container, Progress& progress, int progress_start = 0, int chunk_size = 100)
{
  return apply_kernel_it(op, container.begin(), container.end(), progress, progress_start, chunk_size);
}
} // namespace util // namespace util
} // namespace lgx // namespace lgx
#endif // ALGORITHMS_HPP
