/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef CUDA_GLOBAL_H
#define CUDA_GLOBAL_H

/*
 * \file CudaGlobal.hpp
 *
 * Defines global items for cuda
 */
#ifdef __CUDACC__
#  define CU_HOST_DEVICE __host__ __device__ inline
#else
#  define CU_HOST_DEVICE inline
#endif

#ifdef WIN32
#  ifdef __MINGW32__
#    ifdef cuda_EXPORTS
#      define CU_DLLEXPORT extern "C" __declspec(dllexport)
#    else
#      define CU_DLLEXPORT extern "C" __declspec(dllimport)
#    endif
#  else
#    ifdef cuda_EXPORTS
#      define CU_DLLEXPORT __declspec(dllexport)
#    else
#      define CU_DLLEXPORT __declspec(dllimport)
#    endif
#  endif
#else
#  define CU_DLLEXPORT
#endif

#define CU_EPSILON 1e-10f

#endif
