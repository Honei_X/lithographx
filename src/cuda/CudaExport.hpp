/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef CU_EXPORT_H
#define CU_EXPORT_H

#include <LGXConfig.hpp>
#include <cuda/CudaGlobal.hpp>
#include <thrust/host_vector.h>

#include <Matrix.hpp>
#include <Vector.hpp>

namespace lgx {
typedef unsigned char ubyte;
#if !defined(_MSC_VER) || !defined(Q_OS_WIN32) // otherwise, a bug in VC++ prevents using hash maps
typedef unsigned int uint;
typedef unsigned short ushort;
typedef unsigned long ulong;
#endif
typedef util::Vector<3, float> Point3f;
typedef util::Vector<4, float> Point4f;
typedef util::Vector<6, float> Point6f;
typedef util::Vector<12, float> Point12f;
// typedef Matrix<4, 4, float> Matrix4f;
typedef util::Vector<3, int> Point3i;
typedef util::Vector<3, uint> Point3u;
typedef util::Vector<3, ulong> Point3ul;
typedef util::Vector<3, ushort> Point3us;
typedef thrust::host_vector<ubyte> HVecUB;
typedef thrust::host_vector<uint> HVecU;
typedef thrust::host_vector<ushort> HVecUS;
typedef thrust::host_vector<float> HVecF;
typedef thrust::host_vector<Point3u> HVec3U;
typedef thrust::host_vector<Point3f> HVec3F;
typedef thrust::host_vector<Point4f> HVec4F;
} // namespace lgx

CU_DLLEXPORT int initGPU();
CU_DLLEXPORT size_t memAvailGPU();
CU_DLLEXPORT void freeMemGPU();
CU_DLLEXPORT void holdMemGPU();
CU_DLLEXPORT int setHoldMemGPU(uint holdmem);
CU_DLLEXPORT int getHoldMemGPU();

CU_DLLEXPORT int averageGPU(const lgx::Point3u& imgSize, const lgx::Point3u& radius,
                            const lgx::HVecUS& srcdata, lgx::HVecUS& dstdata);
CU_DLLEXPORT int dilateGPU(const lgx::Point3u& imgSize, const lgx::Point3u& radius, const lgx::HVecUS& srcdata,
                           lgx::HVecUS& dstdata);
CU_DLLEXPORT int erodeGPU(const lgx::Point3u& imgSize, const lgx::Point3u& radius, const lgx::HVecUS& srcdata,
                            lgx::HVecUS& dstdata, bool byLabel);
CU_DLLEXPORT int gaussianBlurGPU(const lgx::Point3u& imgSize, const lgx::Point3f& imgStep,
                                 const lgx::Point3f& sigma, const lgx::HVecUS& srcdata, lgx::HVecUS& dstdata);
CU_DLLEXPORT int sharpenGPU(const lgx::Point3u& imgSize, const lgx::Point3f& imgStep,
                            const lgx::Point3f& sigma, const float amount, const lgx::HVecUS& srcdata,
                            lgx::HVecUS& dstdata);
CU_DLLEXPORT int applyKernelGPU(const lgx::Point3u& imgSize, const lgx::HVecF& kernelX,
                                const lgx::HVecF& kernelY, const lgx::HVecF& kernelZ,
                                const lgx::HVecUS& srcdata, lgx::HVecUS& dstdata);
CU_DLLEXPORT int colorGradGPU(const lgx::Point3u& imgSize, float div, const lgx::HVecUS& srcdata,
                              lgx::HVecUS& dstdata);
CU_DLLEXPORT int edgeDetectGPU(const lgx::Point3u& imgSize, ushort lowthresh, ushort highthresh, float mult,
                               ushort fillval, const lgx::HVecUS& srcdata, lgx::HVecUS& dstdata);
CU_DLLEXPORT int insideMeshGPU(const lgx::Point3u& base, const lgx::Point3u& size, const lgx::Point3f& step,
                               const lgx::Point3f& shift, const lgx::HVec3F& pts, const lgx::HVec3U& tris,
                               const lgx::HVec3F& nrmls, lgx::HVecUS& dstdata);
CU_DLLEXPORT int nearMeshGPU(const lgx::Point3u& base, const lgx::Point3u& size, const lgx::Point3f& step,
                             const lgx::Point3f& shift, const lgx::HVec3F& pts, const lgx::HVec3F& nrmls,
                             float minDist, float maxDist, lgx::HVecUS& dstdata);
CU_DLLEXPORT int clipStackGPU(const lgx::Point3u& imgSize, const lgx::Point3f& step, const lgx::Point3f& shift,
                              const lgx::Point3u& clipDo, const lgx::HVec4F& pn, const lgx::HVecUS& srcdata,
                              lgx::HVecUS& dstdata);
CU_DLLEXPORT int pixelEditGPU(const lgx::Point3u& base, const lgx::Point3u& size, const lgx::Point3u& imgSize,
                              const lgx::Point3f& step, const lgx::Point3f& shift, const lgx::Point3f& p,
                              const lgx::Point3f& pz, float radius, ushort color, const lgx::Point3u& clipDo,
                              const lgx::HVec4F& pn, lgx::HVecUS& data);
#endif
