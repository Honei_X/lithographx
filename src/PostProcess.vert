varying vec2 texCoord;

void main()
{
  texCoord = (gl_TextureMatrix[0]*gl_MultiTexCoord0).xy;
  gl_Position = ftransform();
  gl_FrontColor = gl_Color;
}
