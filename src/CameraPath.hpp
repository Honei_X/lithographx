/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef CAMERAPATH_HPP
#define CAMERAPATH_HPP

#include <LGXConfig.hpp>

#include <Misc.hpp>

#include <QObject>
#include <QTimer>
#include <QPointer>
#include <QList>

class LGXCamera;

class CameraPath : public QObject {
  Q_OBJECT
public:
  CameraPath(QObject* parent);
  ~CameraPath();
  void addFrame(LGXCamera* camera, float time = 1.0);
  void addRotation(const lgx::Point3f& axis, double angle, float time = 2.0);

  void animatePath(LGXCamera* camera, float dt);

protected slots:
  void nextFrame();

signals:
  void endPath();
  void frameUpdated();

protected:
  struct Action : public QObject {
    Action(float t, QObject* parent = 0)
      : QObject(parent)
      , time(t)
    {
    }

    // Returns true when the end is reached
    virtual bool startInterpolation(LGXCamera* camera) = 0;
    virtual bool interpolateFrame(LGXCamera* camera, float t) = 0;
    float time;
  };

  struct ToPosition : public Action {
    ToPosition(const lgx::Point3f& c, const lgx::Point3f& p, const lgx::Point3f& d, float z, float t, QObject* parent = 0)
      : Action(t, parent)
      , center(c)
      , position(p)
      , direction(d)
      , zoom(z)
    {
    }

    virtual bool startInterpolation(LGXCamera* camera);
    virtual bool interpolateFrame(LGXCamera* camera, float t);
    lgx::Point3f center;
    lgx::Point3f position;
    lgx::Point3f direction;
    float zoom;

    // Quaternions for camera orientation
    lgx::Quaternion oq1, oq2;
    // Quaternions for camera position
    lgx::Quaternion pq1, pq2;
  };

  struct Rotation : public Action {
    Rotation(const lgx::Point3f& c, const lgx::Point3f& ax, float an, float t, QObject* parent = 0)
      : Action(t, parent)
      , center(c)
      , axis(ax)
      , angle(an)
    {
    }
    virtual bool startInterpolation(LGXCamera*) {
      return true;
    }
    virtual bool interpolateFrame(LGXCamera*, float) {
      return true;
    }
    lgx::Point3f center;
    lgx::Point3f axis;
    double angle;
  };

  QList<QPointer<Action> > frames;
  LGXCamera* camera;
  float dt;
  float current_time;
  int current_index;
  QTimer* timer;
};

#endif // CAMERAPATH_HPP
