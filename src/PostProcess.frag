uniform sampler2D texId;
uniform ivec2 texSize;
uniform bool unsharp;
uniform float brightness;
uniform float contrast;
uniform float amount;
uniform float kernel[9];
// 0 1 2
// 3 4 5
// 6 7 8

varying vec2 texCoord;

void main()
{
  vec4 col = texture2D(texId, texCoord);
  if(unsharp)
  {
    vec2 dt = vec2(1.0/texSize.x, 1.0/texSize.y);
    vec4 mask_col = kernel[4] * col;
    mask_col += kernel[3] * texture2D(texId, vec2(texCoord.x-dt.x, texCoord.y));
    mask_col += kernel[5] * texture2D(texId, vec2(texCoord.x+dt.x, texCoord.y));
    mask_col += kernel[1] * texture2D(texId, vec2(texCoord.x, texCoord.y+dt.y));
    mask_col += kernel[7] * texture2D(texId, vec2(texCoord.x, texCoord.y-dt.y));
    mask_col += kernel[2] * texture2D(texId, texCoord + dt);
    mask_col += kernel[6] * texture2D(texId, texCoord - dt);
    mask_col += kernel[0] * texture2D(texId, texCoord + vec2(-dt.x, dt.y));
    mask_col += kernel[8] * texture2D(texId, texCoord + vec2(dt.x, -dt.y));

    mask_col = col - mask_col;

    col += amount * mask_col;
  }
  col.rgb = (col.rgb - 0.5) * contrast + 0.5 + brightness;
  gl_FragColor = col;
}

