/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "LithoViewer.hpp"

#include "cuda/CudaExport.hpp"
#include "CutSurf.hpp"
#include "Dir.hpp"
#include "ImageData.hpp"
#include "Information.hpp"
#include "LithoGraphX.hpp"
#include "Process.hpp"

#include <iostream>
#include <QDomElement>
#include <QFileDialog>
#include <QFileInfo>
#include <QGLFormat>
#include <QGLFormat>
#include <QGLFramebufferObject>
#include <QGLFramebufferObjectFormat>
#include <QImage>
#include <QMessageBox>
#include <QShowEvent>
#include <QWheelEvent>
#include <string>
#include <QTimer>

#ifdef EPSILON
#  undef EPSILON
#endif
#define EPSILON 1e-3

using namespace lgx;
using namespace qglviewer;

static const float unsharp_kernel[9]
  = { 1.0 / 16.0, 2.0 / 16.0, 1.0 / 16.0, 2.0 / 16.0, 4.0 / 16.0, 2.0 / 16.0, 1.0 / 16.0, 2.0 / 16.0, 1.0 / 16.0 };

LGXKeyFrameInterpolator::LGXKeyFrameInterpolator(Frame* fr)
  : KeyFrameInterpolator(fr)
{
}

void LGXKeyFrameInterpolator::interpolateAtTime(float time) {
  KeyFrameInterpolator::interpolateAtTime(time);
}

LGXCameraFrame::LGXCameraFrame()
  : qglviewer::ManipulatedCameraFrame()
{
}


void LGXCameraFrame::wheelEvent(QWheelEvent* const event, Camera* const camera)
{
  const float wheelSensitivityCoef = 8E-4f;
  int delta = event->angleDelta().y();
  switch(action_) {
  case QGLViewer::ZOOM:
    if(delta == 0) return;
    _zoom -= wheelSensitivity() * delta * wheelSensitivityCoef;
    emit modified();
    if(DEBUG)
      Information::out << "  zoom = " << _zoom << endl;
    emit manipulated();
    break;
  default:
    qglviewer::ManipulatedCameraFrame::wheelEvent(event, camera);
    return;
  }

  if(previousConstraint_)
    setConstraint(previousConstraint_);

  const int finalDrawAfterWheelEventDelay = 400;

  flyTimer_.setSingleShot(true);
  flyTimer_.start(finalDrawAfterWheelEventDelay);

  // This could also be done *before* manipulated is emitted, so that isManipulated() returns false.
  // But then fastDraw would not be used with wheel.
  // Detecting the last wheel event and forcing a final draw() is done using the timer_.
  action_ = QGLViewer::NO_MOUSE_ACTION;
}

void LGXCameraFrame::getMatrix(GLdouble m[4][4]) const
{
  qglviewer::ManipulatedCameraFrame::getMatrix(m);
  //Information::out << "zoom = " << zoom() << endl;
  double z = 0.5f * exp(zoom());
  m[3][3] = z;
}

void LGXCameraFrame::getMatrix(GLdouble m[16]) const
{
  qglviewer::ManipulatedCameraFrame::getMatrix(m);
  //Information::out << "zoom = " << zoom() << endl;
  double z = 0.5f * exp(zoom());
  m[15] = z;
}

const GLdouble* LGXCameraFrame::worldMatrix() const
{
  // This test is done for efficiency reasons (creates lots of temp objects otherwise).
  if(referenceFrame()) {
    static LGXCameraFrame fr;
    fr.setTranslation(position());
    fr.setRotation(orientation());
    fr.setZoom(zoom());
    return fr.matrix();
  } else
    return matrix();
}

void LGXCameraFrame::setFromMatrix(const GLdouble m[4][4])
{
  GLdouble nozoom[4][4];
  for(int i = 0 ; i < 4 ; ++i)
    for(int j = 0 ; j < 4 ; ++j)
      nozoom[i][j] = m[i][j];
  nozoom[3][3] = 1;
  _zoom = std::log(2.*m[3][3]);

  qglviewer::ManipulatedCameraFrame::setFromMatrix(nozoom);
}

void LGXCameraFrame::setFromMatrix(const GLdouble m[16])
{
  GLdouble nozoom[16];
  for(int i = 0 ; i < 15 ; ++i)
    nozoom[i] = m[i];
  nozoom[15] = 1;
  qglviewer::ManipulatedCameraFrame::setFromMatrix(nozoom);
  _zoom = std::log(2.*m[15]);
}

void LGXCameraFrame::setZoom(float z)
{
  _zoom = z;
  emit modified();
}

LGXCamera::LGXCamera()
  : qglviewer::Camera()
{
  setType(qglviewer::Camera::ORTHOGRAPHIC);
  auto frame = new LGXCameraFrame;
  setFrame(frame);
}

void LGXCamera::getOrthoWidthHeight(GLdouble& halfWidth, GLdouble& halfHeight) const
{
  qglviewer::Camera::getOrthoWidthHeight(halfWidth, halfHeight);
  float z = exp(frame()->zoom());
  halfWidth *= z;
  halfHeight *= z;
}

void LGXCamera::setFrame(ManipulatedCameraFrame* const mcf)
{
  auto lgx_frame = dynamic_cast<LGXCameraFrame*>(mcf);
  if(not lgx_frame) {
    lgx_frame = new LGXCameraFrame();
    lgx_frame->setReferenceFrame(mcf);
  }
  qglviewer::Camera::setFrame(lgx_frame);
}

void LGXCamera::resetZoom() {
  frame()->setZoom(0.0f);
}

void LGXCamera::fitSphere(const Vec& center, float radius)
{
  frame()->setZoom(0.0f);
  qglviewer::Camera::fitSphere(center, radius);
}

void LGXCamera::addKeyFrameToPath(int i) {
  Camera::addKeyFrameToPath(i);
}

void LGXCamera::playPath(int i) {
  Camera::playPath(i);
}

void LGXCamera::deletePath(int i) {
  Camera::deletePath(i);
}

void LGXCamera::resetPath(int i) {
  Camera::resetPath(i);
}

void LGXCamera::drawAllPaths() {
  Camera::drawAllPaths();
}

LithoViewer::LithoViewer(QWidget* parent)
  : QGLViewer(parent)
  , _stack1(nullptr)
  , _stack2(nullptr)
  , _c1(nullptr)
  , _c2(nullptr)
  , _c3(nullptr)
  , _cutSurf(nullptr)
  , _quitting(false)
  , _needShowScene(true)
  , _drawCellMap(false)
  , _drawClipBox(false)
  , _pixelEditCursor(false)
  , _pixelRadius(1)
  , _selectMode(NONE)
  , _shiftPressed(false)
  , _altPressed(false)
  , _controlPressed(false)
  , _leftButton(false)
  , _editionActive(false)
  , _flySpeed(0.005f)
  , _selectedLabel(0)
  , _sampling(2)
  , _fast_draw(false)
  , _maxNbPeels(20)
  , _globalBrightness(0.0)
  , _globalContrast(1.0)
  , _unsharpStrength(1.0)
  , _spinning(10000)
  , _camera(new LGXCamera)
  , sceneRadius(1e7)
  , initialized(false)
  , lastInitCamera(0)
  , _lastButtons(0)
  , _current_device(0)
  , _need3Dupdate(true)
{
  initObject(parent);
  setCamera(_camera);
}

void LithoViewer::initObject(QWidget*)
{
  _showSlice = -1;

  setAcceptDrops(true);
  setAnimationPeriod(0);

  // Remap movement to manipulated frame
  setMouseBinding(Qt::ShiftModifier, Qt::LeftButton, CAMERA, TRANSLATE);
  setMouseBinding(Qt::ControlModifier | Qt::ShiftModifier, Qt::LeftButton, FRAME, TRANSLATE);
  // Prevent manipulated frame zooming
  setWheelBinding(Qt::NoModifier, CAMERA, ZOOM);
  // Set that to move along the z axis
  setMouseBinding(Qt::ControlModifier, Qt::MidButton, FRAME, MOVE_FORWARD);
  setKeyDescription(Qt::ALT | Qt::CTRL | Qt::Key_F, tr("Fit the entire scene in the screen", "FIT_ENTIRE_SCENE action description"));

  // Include shift in selection
  setShortcut(SAVE_SCREENSHOT, Qt::SHIFT + Qt::Key_Print);
  setShortcut(CAMERA_MODE, 0);
  setShortcut(EXIT_VIEWER, 0);
  setShortcut(INCREASE_FLYSPEED, 0);
  setShortcut(DECREASE_FLYSPEED, 0);
  setContextMenuPolicy(Qt::ActionsContextMenu);
  QGLFormat f = format();

  // Enable capture of mouse move events
  setMouseTracking(true);

  // Hotkey camera locations

  // Turn off buffer swap, we'll do it ourselves
  setAutoBufferSwap(false);

  auto* lgx = mainWindow();
  if(lgx) {
      _stack1 = lgx->_stack1;
      _stack2 = lgx->_stack2;
      _cutSurf = &lgx->_cutSurf;
  }

  // 3D changes
  connect(this, &LithoViewer::changedNbPeels, this, &LithoViewer::update3D);
  connect(this, &LithoViewer::changedShininess, this, &LithoViewer::update3D);
  connect(this, &LithoViewer::changedSpecular, this, &LithoViewer::update3D);
  connect(this, &LithoViewer::changedUnsharpening, this, &LithoViewer::update3D);
  connect(this, &QGLViewer::gridIsDrawnChanged, this, &LithoViewer::update3D);
  connect(this, &QGLViewer::axisIsDrawnChanged, this, &LithoViewer::update3D);
  connect(this, &QGLViewer::cameraIsEditedChanged, this, &LithoViewer::update3D);

  // 2D changes
  connect(this, &QGLViewer::textIsEnabledChanged, this, &LithoViewer::update2D);
  connect(this, &QGLViewer::FPSIsDisplayedChanged, this, &LithoViewer::update2D);
}

LithoViewer::~LithoViewer()
{
  saveStateToFile();

  // Clean FBO

  opengl->glDeleteTextures(NB_FRAMES, colorTexId);
  opengl->glDeleteTextures(NB_FRAMES, depthTexId);
  for(int i = 0; i < NB_FRAMES; ++i) {
    colorTexId[i] = 0;
    depthTexId[i] = 0;
  }
  opengl->glDeleteRenderbuffers(1, &depthBuffer);
  depthBuffer = 0;

  if(fboId != 0)
    opengl->glDeleteFramebuffers(1, &fboId);
  fboId = 0;
  if(fboCopyId != 0)
    opengl->glDeleteFramebuffers(1, &fboCopyId);
  delete opengl;
  opengl = 0;
  fboCopyId = 0;
}

void LithoViewer::init()
{
  // Check multi-sampling is de-activate, but in practice it seems useless ..
  if(format().sampleBuffers()) {
    QMessageBox::critical(this, "Problem with OpenGL context",
                          "Multi-sampling is activated in your OpenGL context.\n"
                          "Make sure your drivers are not setup to overide application preferences.\n"
                          "Note that as long as this persists, selection will not work properly in LithoGraphX.");
  }

  // Information::out << "Initializing OpenGL context" << endl;

  QGLViewer::init();

  // Initialize the OpenGLFunctions object
  opengl = new lgx::OpenGLFunctions();
  if(not opengl->initializeOpenGLFunctions()) {
    SETSTATUS("Error, couldn't initialize QOpenGL functions objects");
    ::exit(2);
  }

  {
    auto vendor = glGetString(GL_VENDOR);
    auto version = glGetString(GL_VERSION);
    auto renderer = glGetString(GL_RENDERER);
    Information::out << "OpenGL:"
                     << "\n - Version : " << (const char*)version
                     << "\n - Renderer: " << (const char*)renderer
                     << "\n - Vendor  : " << (const char*)vendor
                     << endl;
  }

  initCamera();

  raycasting_shader1.setVerbosity(0);
  raycasting_shader1.init();

  raycasting_shader1.addVertexShader(":/shaders/RayCasting.vert");
  raycasting_shader1.addFragmentShader(":/shaders/Utils.frag");
  raycasting_shader1.addFragmentShader(":/shaders/3DColoring.frag");
  raycasting_shader1.addFragmentShaderCode("");   // To be replaced by the needed function
  raycasting_shader1.addFragmentShader(":/shaders/RayCasting.frag");

  raycasting_shader2.setVerbosity(0);
  raycasting_shader2.init();

  raycasting_shader2.addVertexShader(":/shaders/RayCasting.vert");
  raycasting_shader2.addFragmentShader(":/shaders/Utils.frag");
  raycasting_shader2.addFragmentShader(":/shaders/3DColoring.frag");
  raycasting_shader2.addFragmentShaderCode("");   // To be replaced by the needed function
  raycasting_shader2.addFragmentShader(":/shaders/RayCasting.frag");

  final_combine_shader.setVerbosity(0);
  final_combine_shader.init();

  final_combine_shader.addVertexShader(":/shaders/Combine.vert");
  final_combine_shader.addFragmentShader(":/shaders/Utils.frag");
  final_combine_shader.addFragmentShader(":/shaders/FinalCombine.frag");

  combine_shader.setVerbosity(0);
  combine_shader.init();

  combine_shader.addVertexShader(":/shaders/Combine.vert");
  combine_shader.addFragmentShader(":/shaders/Utils.frag");
  combine_shader.addFragmentShader(":/shaders/Combine.frag");

  occlusion_shader.setVerbosity(0);
  occlusion_shader.init();

  occlusion_shader.addVertexShader(":/shaders/PostProcess.vert");
  occlusion_shader.addFragmentShader(":/shaders/Occlusion.frag");

  post_process_shader.setVerbosity(0);
  post_process_shader.init();

  post_process_shader.addVertexShader(":/shaders/PostProcess.vert");
  post_process_shader.addFragmentShader(":/shaders/PostProcess.frag");

  render_depth_shader.setVerbosity(0);
  render_depth_shader.init();

  render_depth_shader.addVertexShader(":/shaders/RenderDepth.vert");
  render_depth_shader.addFragmentShader(":/shaders/RenderDepth.frag");

  texture_surf_shader.setVerbosity(0);
  texture_surf_shader.init();

  texture_surf_shader.addVertexShader(":/shaders/Light.vert");
  texture_surf_shader.addVertexShader(":/shaders/TextureSurf.vert");
  texture_surf_shader.addVertexShader(":/shaders/PeelingSurf.vert");
  texture_surf_shader.addFragmentShader(":/shaders/Light.frag");
  texture_surf_shader.addFragmentShader(":/shaders/TextureSurf.frag");
  texture_surf_shader.addFragmentShader(":/shaders/PeelingSurf.frag");

  volume_surf_shader1.setVerbosity(0);
  volume_surf_shader1.init();

  volume_surf_shader1.addVertexShader(":/shaders/Light.vert");
  volume_surf_shader1.addVertexShader(":/shaders/VolumeSurf.vert");
  volume_surf_shader1.addVertexShader(":/shaders/PeelingSurf.vert");
  volume_surf_shader1.addFragmentShader(":/shaders/Light.frag");
  volume_surf_shader1.addFragmentShader(":/shaders/Utils.frag");
  volume_surf_shader1.addFragmentShader(":/shaders/3DColoring.frag");
  volume_surf_shader1.addFragmentShaderCode("");
  volume_surf_shader1.addFragmentShader(":/shaders/VolumeSurf.frag");
  volume_surf_shader1.addFragmentShader(":/shaders/PeelingSurf.frag");

  volume_surf_shader2.setVerbosity(0);
  volume_surf_shader2.init();

  volume_surf_shader2.addVertexShader(":/shaders/Light.vert");
  volume_surf_shader2.addVertexShader(":/shaders/VolumeSurf.vert");
  volume_surf_shader2.addVertexShader(":/shaders/PeelingSurf.vert");
  volume_surf_shader2.addFragmentShader(":/shaders/Light.frag");
  volume_surf_shader2.addFragmentShader(":/shaders/Utils.frag");
  volume_surf_shader2.addFragmentShader(":/shaders/3DColoring.frag");
  volume_surf_shader2.addFragmentShaderCode("");
  volume_surf_shader2.addFragmentShader(":/shaders/VolumeSurf.frag");
  volume_surf_shader2.addFragmentShader(":/shaders/PeelingSurf.frag");

  index_surf_shader.setVerbosity(0);
  index_surf_shader.init();

  index_surf_shader.addVertexShader(":/shaders/Light.vert");
  index_surf_shader.addVertexShader(":/shaders/IndexSurf.vert");
  index_surf_shader.addVertexShader(":/shaders/PeelingSurf.vert");
  index_surf_shader.addFragmentShader(":/shaders/Light.frag");
  index_surf_shader.addFragmentShader(":/shaders/IndexSurf.frag");
  index_surf_shader.addFragmentShader(":/shaders/PeelingSurf.frag");

  // camera()->setType(qglviewer::Camera::ORTHOGRAPHIC);
  camera()->setSceneRadius(sceneRadius);
  camera()->setSceneCenter(Vec(0, 0, 0));
  camera()->centerScene();
  camera()->showEntireScene();
  camera()->frame()->setSpinningSensitivity(_spinning);

  mainWindow()->setSpinningSensitivity(_spinning);

  // Color4f clearColor = Colors::getColor(Colors::BackgroundColor);
  // opengl->glClearColor(clearColor.r(), clearColor.g(), clearColor.b(), 1.0);
  // opengl->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_ACCUM_BUFFER_BIT);
  opengl->glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
  opengl->glShadeModel(GL_SMOOTH);
  // opengl->glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  // opengl->glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);
  opengl->glEnable(GL_DEPTH_CLAMP_NV);   // Allow to zoom more, but cause reverse later. DO NOT REMOVE

  _stack1->initTex();
  _stack2->initTex();
  ImgData::scaleBar.init(this);

  // Init FBO
  opengl->glGenFramebuffers(1, &fboId);
  opengl->glGenFramebuffers(1, &fboCopyId);

  // Create textures for depth and color buffer
  depthBuffer = 0;

  for(int i = 0; i < NB_FRAMES; ++i) {
    colorTexId[i] = 0;
    depthTexId[i] = 0;
  }

  _prevWidth = _prevHeight = 0;
  // updateFBOTex(width(), height());

  // To start with, we always render on the screen
  baseFboId = 0;
  // Information::out << "OpenGL initialized" << endl;
  // Connect the frames to update3D
  connect(_camera->frame(), &qglviewer::ManipulatedCameraFrame::modified, this, &LithoViewer::update3D);
  connect(&_stack1->getMainFrame(), &qglviewer::ManipulatedFrame::modified, this, &LithoViewer::update3D);
  connect(&_stack2->getMainFrame(), &qglviewer::ManipulatedFrame::modified, this, &LithoViewer::update3D);
  connect(&_stack1->getTransFrame(), &qglviewer::ManipulatedFrame::modified, this, &LithoViewer::update3D);
  connect(&_stack2->getTransFrame(), &qglviewer::ManipulatedFrame::modified, this, &LithoViewer::update3D);
  connect(_c1.data(), &Clip::modified, this, &LithoViewer::update3D);
  connect(_c2.data(), &Clip::modified, this, &LithoViewer::update3D);
  connect(_c3.data(), &Clip::modified, this, &LithoViewer::update3D);
  connect(&_c1->frame(), &qglviewer::ManipulatedFrame::modified, this, &LithoViewer::update3D);
  connect(&_c2->frame(), &qglviewer::ManipulatedFrame::modified, this, &LithoViewer::update3D);
  connect(&_c3->frame(), &qglviewer::ManipulatedFrame::modified, this, &LithoViewer::update3D);
  connect(&_cutSurf->getFrame(), &qglviewer::ManipulatedFrame::modified, this, &LithoViewer::update3D);
  connect(_cutSurf.data(), &CutSurf::modified, this, &LithoViewer::update3D);
}

void LithoViewer::postDraw()
{
  if(_quitting)
    return;
  opengl->glEnable(GL_DEPTH_TEST);
  QGLViewer::postDraw();
  swapBuffers();
}

void LithoViewer::initFromDOMElement(const QDomElement& element)
{
  // Restore standard state
  QGLViewer::initFromDOMElement(element);

  QGLFormat def = QGLFormat::defaultFormat();

  QDomElement child = element.firstChild().toElement();
  while(!child.isNull()) {
    if(child.tagName() == "GLFormat") {
      if(child.hasAttribute("alpha"))
        def.setAlpha(child.attribute("alpha").toLower() == "yes");
      if(child.hasAttribute("depth"))
        def.setDepth(child.attribute("depth").toLower() == "yes");
      if(child.hasAttribute("rgba"))
        def.setRgba(child.attribute("rgba").toLower() == "yes");
      if(child.hasAttribute("stereo"))
        def.setStereo(child.attribute("stereo").toLower() == "yes");
      if(child.hasAttribute("stencil"))
        def.setStencil(child.attribute("stencil").toLower() == "yes");
      if(child.hasAttribute("doubleBuffer"))
        def.setDoubleBuffer(child.attribute("doubleBuffer").toLower() == "yes");
      if(child.hasAttribute("sampleBuffers"))
        def.setSampleBuffers(child.attribute("sampleBuffers").toLower() == "yes");
      if(child.hasAttribute("directRendering"))
        def.setDirectRendering(child.attribute("directRendering").toLower() == "yes");
      if(child.hasAttribute("hasOverlay"))
        def.setOverlay(child.attribute("hasOverlay").toLower() == "yes");
      if(def.accum())
        if(child.hasAttribute("accumBufferSize"))
          def.setAccumBufferSize(child.attribute("accumBufferSize").toInt());
      if(def.alpha())
        if(child.hasAttribute("alphaBufferSize"))
          def.setAlphaBufferSize(child.attribute("alphaBufferSize").toInt());
      if(def.depth())
        if(child.hasAttribute("depthBufferSize"))
          def.setDepthBufferSize(child.attribute("depthBufferSize").toInt());
      if(def.sampleBuffers())
        if(child.hasAttribute("samples"))
          def.setSamples(child.attribute("samples").toInt());
      if(def.stencil())
        if(child.hasAttribute("stencilBufferSize"))
          def.setStencilBufferSize(child.attribute("stencilBufferSize").toInt());
      if(def.rgba()) {
        if(child.hasAttribute("redBufferSize"))
          def.setRedBufferSize(child.attribute("redBufferSize").toInt());
        if(child.hasAttribute("greenBufferSize"))
          def.setGreenBufferSize(child.attribute("greenBufferSize").toInt());
        if(child.hasAttribute("blueBufferSize"))
          def.setBlueBufferSize(child.attribute("blueBufferSize").toInt());
      }
    }
    child = child.nextSibling().toElement();
  }
  QGLFormat::setDefaultFormat(def);
}

QDomElement LithoViewer::domElement(const QString& name, QDomDocument& document) const
{
  // Creates a custom node for a light
  QDomElement de = document.createElement("GLFormat");
  QGLFormat def = QGLFormat::defaultFormat();
  de.setAttribute("accum", (def.accum() ? "yes" : "no"));
  if(def.accum() && def.accumBufferSize() != -1)
    de.setAttribute("accumBufferSize", std::max(0, def.accumBufferSize()));
  de.setAttribute("alpha", (def.alpha() ? "yes" : "no"));
  if(def.alpha() && def.alphaBufferSize() != -1)
    de.setAttribute("alphaBufferSize", std::max(0, def.alphaBufferSize()));
  de.setAttribute("depth", (def.depth() ? "yes" : "no"));
  if(def.depth() && def.depthBufferSize() != -1)
    de.setAttribute("depthBufferSize", std::max(0, def.depthBufferSize()));
  de.setAttribute("doubleBuffer", (def.doubleBuffer() ? "yes" : "no"));
  de.setAttribute("directRendering", (def.directRendering() ? "yes" : "no"));
  de.setAttribute("hasOverlay", (def.hasOverlay() ? "yes" : "no"));
  de.setAttribute("rgba", (def.rgba() ? "yes" : "no"));
  if(def.rgba()) {
    if(def.redBufferSize() != -1)
      de.setAttribute("redBufferSize", std::max(0, def.redBufferSize()));
    if(def.greenBufferSize() != -1)
      de.setAttribute("greenBufferSize", std::max(0, def.greenBufferSize()));
    if(def.blueBufferSize() != -1)
      de.setAttribute("blueBufferSize", std::max(0, def.blueBufferSize()));
  }
  de.setAttribute("sampleBuffers", (def.sampleBuffers() ? "yes" : "no"));
  if(def.sampleBuffers() && def.samples() != -1)
    de.setAttribute("samples", std::max(0, def.samples()));
  de.setAttribute("stereo", (def.stereo() ? "yes" : "no"));
  de.setAttribute("stencil", (def.stencil() ? "yes" : "no"));
  if(def.stencil() && def.stencilBufferSize() != -1)
    de.setAttribute("stencilBufferSize", std::max(0, def.stencilBufferSize()));

  // Get default state domElement and append custom node
  QDomElement res = QGLViewer::domElement(name, document);
  res.appendChild(de);
  return res;
}

void LithoViewer::initFormat()
{
  QGLFormat def = QGLFormat::defaultFormat();
  QFile file(".qglviewer.xml");
  if(file.open(QIODevice::ReadOnly)) {
    QDomDocument doc("QGLViewer");
    doc.setContent(&file);
    file.close();
    QDomElement root = doc.firstChildElement("QGLViewer");
    QDomElement child = root.firstChildElement("GLFormat");
    if(!child.isNull()) {
      if(child.hasAttribute("alpha"))
        def.setAlpha(child.attribute("alpha").toLower() == "yes");
      if(child.hasAttribute("depth"))
        def.setDepth(child.attribute("depth").toLower() == "yes");
      if(child.hasAttribute("rgba"))
        def.setRgba(child.attribute("rgba").toLower() == "yes");
      if(child.hasAttribute("stereo"))
        def.setStereo(child.attribute("stereo").toLower() == "yes");
      if(child.hasAttribute("stencil"))
        def.setStencil(child.attribute("stencil").toLower() == "yes");
      if(child.hasAttribute("doubleBuffer"))
        def.setDoubleBuffer(child.attribute("doubleBuffer").toLower() == "yes");
      if(child.hasAttribute("sampleBuffers"))
        def.setSampleBuffers(child.attribute("sampleBuffers").toLower() == "yes");
      if(child.hasAttribute("directRendering"))
        def.setDirectRendering(child.attribute("directRendering").toLower() == "yes");
      if(child.hasAttribute("hasOverlay"))
        def.setOverlay(child.attribute("hasOverlay").toLower() == "yes");
      if(def.accum())
        if(child.hasAttribute("accumBufferSize"))
          def.setAccumBufferSize(child.attribute("accumBufferSize").toInt());
      if(def.alpha())
        if(child.hasAttribute("alphaBufferSize"))
          def.setAlphaBufferSize(child.attribute("alphaBufferSize").toInt());
      if(def.depth())
        if(child.hasAttribute("depthBufferSize"))
          def.setDepthBufferSize(child.attribute("depthBufferSize").toInt());
      if(def.sampleBuffers())
        if(child.hasAttribute("samples"))
          def.setSamples(child.attribute("samples").toInt());
      if(def.stencil())
        if(child.hasAttribute("stencilBufferSize"))
          def.setStencilBufferSize(child.attribute("stencilBufferSize").toInt());
      if(def.rgba()) {
        if(child.hasAttribute("redBufferSize"))
          def.setRedBufferSize(child.attribute("redBufferSize").toInt());
        if(child.hasAttribute("greenBufferSize"))
          def.setGreenBufferSize(child.attribute("greenBufferSize").toInt());
        if(child.hasAttribute("blueBufferSize"))
          def.setBlueBufferSize(child.attribute("blueBufferSize").toInt());
      }
    }
  }
  QGLFormat::setDefaultFormat(def);
}

/*
 *void LithoViewer::showEvent(QShowEvent* event)
 *{
 *  QGLViewer::showEvent(event);
 *  if(!initialized) {
 *    restoreStateFromFile();
 *    lastInitCamera = camera();
 *    initialized = true;
 *  }
 *}
 */

void LithoViewer::initCamera()
{
  if(camera() != lastInitCamera) {
    restoreStateFromFile();
    lastInitCamera = camera();
  }
}

void LithoViewer::preDraw()
{
  if(_quitting)
    return;
  QGLViewer::preDraw();
  camera()->setSceneRadius(sceneRadius);

  opengl->glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

  if(!final_combine_shader.initialized())
    if(!final_combine_shader.setupShaders()) {
      Information::err << "Error compiling combine shaders" << endl;
    }
  if(!combine_shader.initialized())
    if(!combine_shader.setupShaders()) {
      Information::err << "Error compiling combine shaders" << endl;
    }
  if(!occlusion_shader.initialized())
    if(!occlusion_shader.setupShaders()) {
      Information::err << "Error compiling combine shaders" << endl;
    }
  if(!post_process_shader.initialized())
    if(!post_process_shader.setupShaders()) {
      Information::err << "Error compiling combine shaders" << endl;
    }
  if(!render_depth_shader.initialized())
    if(!render_depth_shader.setupShaders()) {
      Information::err << "Error compiling render_depth shaders" << endl;
    }
  _stack1->setupVolumeShader(raycasting_shader1, 2);
  if(!raycasting_shader1.initialized())
    if(!raycasting_shader1.setupShaders()) {
      Information::err << "Error compiling colormap shaders" << endl;
    }
  _stack2->setupVolumeShader(raycasting_shader2, 2);
  if(!raycasting_shader2.initialized())
    if(!raycasting_shader2.setupShaders()) {
      Information::err << "Error compiling colormap shaders" << endl;
    }
  if(!texture_surf_shader.initialized())
    if(!texture_surf_shader.setupShaders()) {
      Information::err << "Error compiling texture_surf shaders" << endl;
    }
  _stack1->setupVolumeShader(volume_surf_shader1, 3);
  // if(DEBUG)
  // Information::out << "code for volume_surf_shader1 color:\n" <<
  // volume_surf_shader1.getFragmentShader(2).toStdString() << endl;
  if(!volume_surf_shader1.initialized())
    if(!volume_surf_shader1.setupShaders()) {
      Information::err << "Error compiling volume_surf shaders" << endl;
    }
  _stack2->setupVolumeShader(volume_surf_shader2, 3);
  // if(DEBUG)
  // Information::out << "code for volume_surf_shader2 color:\n" <<
  // volume_surf_shader2.getFragmentShader(2).toStdString() << endl;
  if(!volume_surf_shader2.initialized())
    if(!volume_surf_shader2.setupShaders()) {
      Information::err << "Error compiling volume_surf shaders" << endl;
    }
  if(!index_surf_shader.initialized())
    if(!index_surf_shader.setupShaders()) {
      Information::err << "Error compiling index_surf shaders" << endl;
    }
}

static bool checkFBO(const char* file, size_t ln)
{
  GLenum status = opengl->glCheckFramebufferStatus(GL_FRAMEBUFFER);

  if(status == 0) {
    Information::err << "Error line " << ln << " creating framebuffer" << endl;
    return false;
  } else {
    if(status != GL_FRAMEBUFFER_COMPLETE) {
      Information::err << "Error file " << file << " on line " << ln << ": ";
      switch(status) {
      case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
        Information::err << "Framebuffer is incomplete: incomplete attachment" << endl;
        break;
      case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
        Information::err << "Framebuffer is incomplete: missing attachment" << endl;
        break;
      case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
        Information::err << "Framebuffer is incomplete: incomplete draw buffer" << endl;
        break;
      case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
        Information::err << "Framebuffer is incomplete: incomplete read buffer" << endl;
        break;
      case GL_FRAMEBUFFER_UNSUPPORTED:
        Information::err << "Framebuffer unsupported" << endl;
        break;
      default:
        Information::err << "Framebuffer is incomplete: status = " << status << endl;
      }
      return false;
    }
  }
  return true;
}

void LithoViewer::setScreenSampling(int val) {
  _sampling = val / 10.0f + 1.0f;
}

bool LithoViewer::updateFBOTex(int width, int height, bool fast_draw)
{
  if(DEBUG)
    Information::out << "updateFBOTex(" << width << ", " << height << ", " << fast_draw << ")" << endl;
  if(fast_draw) {
    _texWidth = int(ceil(width / _sampling));
    _texHeight = int(ceil(height / _sampling));
  } else {
    _texWidth = width;
    _texHeight = height;
  }
  if(_texWidth == _prevWidth and _texHeight == _prevHeight)
    return false;
  // Information::err << "width = " << _texWidth << " - height = " << _texHeight << endl;

  _prevWidth = _texWidth;
  _prevHeight = _texHeight;

  if(colorTexId[0] == 0)
    opengl->glGenTextures(NB_FRAMES, colorTexId);
  if(depthTexId[0] == 0)
    opengl->glGenTextures(NB_FRAMES, depthTexId);
  if(depthBuffer == 0)
    opengl->glGenRenderbuffers(1, &depthBuffer);

  // Create textures
  for(int i = 0; i < NB_FRAMES; ++i) {
    opengl->glBindTexture(GL_TEXTURE_2D, colorTexId[i]);

    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    opengl->glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _texWidth, _texHeight, 0, GL_RGBA, GL_UNSIGNED_SHORT, nullptr);

    opengl->glBindTexture(GL_TEXTURE_2D, depthTexId[i]);

    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_ALPHA);
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

    opengl->glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, _texWidth, _texHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT,
                 nullptr);
  }

  opengl->glBindTexture(GL_TEXTURE_2D, 0);

  // Bind textures to framebuffer

  opengl->glBindFramebuffer(GL_FRAMEBUFFER, fboId);

  opengl->glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
  opengl->glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32F, _texWidth, _texHeight);
  opengl->glBindRenderbuffer(GL_RENDERBUFFER, 0);

  opengl->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorTexId[0], 0);
  opengl->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTexId[0], 0);
  REPORT_GL_ERROR("CREATE_FBO");

  checkFBO(__FILE__, __LINE__);
  return true;
}

void LithoViewer::resizeGL(int width, int height)
{
  _drawWidth = width;
  _drawHeight = height;
  QGLViewer::resizeGL(width, height);
}

void LithoViewer::setupCopyFB(GLuint depth, GLint color)
{
  opengl->glBindFramebuffer(GL_FRAMEBUFFER, fboCopyId);
  if(color != 0)
    opengl->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color, 0);
  if(depth != 0)
    opengl->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth, 0);
}

void LithoViewer::setupFramebuffer(GLuint depth, GLuint color, GLbitfield clear)
{
  GLdouble proj[16], mv[16];
  opengl->glGetDoublev(GL_MODELVIEW_MATRIX, mv);
  opengl->glGetDoublev(GL_PROJECTION_MATRIX, proj);

  /*
   * if(DEBUG)
   *{
   *  Information::out << "GL_PROJECTION_MATRIX = " << endl;
   *  for(int i = 0 ; i < 4 ; ++i)
   *  {
   *    Information::out << "{ ";
   *    for(int j = 0 ; j < 4 ; ++j)
   *    {
   *      Information::out << proj[i+4*j] << " ";
   *    }
   *    Information::out << "}\n";
   *  }
   *  Information::out << endl;
   *}
   */

  opengl->glBindFramebuffer(GL_FRAMEBUFFER, fboId);
  opengl->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color, 0);
  if(depth != 0) {
    opengl->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth, 0);
  } else {
    opengl->glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
  }

  // checkFBO(__LINE__);

  opengl->glPushAttrib(GL_VIEWPORT_BIT);
  opengl->glViewport(0, 0, _texWidth, _texHeight);

  opengl->glClear(clear);

  opengl->glMatrixMode(GL_PROJECTION);
  opengl->glPushMatrix();
  opengl->glLoadIdentity();
  opengl->glMultMatrixd(proj);

  opengl->glMatrixMode(GL_MODELVIEW);
  opengl->glPushMatrix();
  opengl->glLoadIdentity();
  opengl->glMultMatrixd(mv);
}

void LithoViewer::resetupFramebuffer(GLuint depth, GLuint color, GLbitfield clear)
{
  opengl->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color, 0);
  if(depth > 0) {
    opengl->glBindTexture(GL_TEXTURE_2D, depth);
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
    opengl->glBindTexture(GL_TEXTURE_2D, 0);
    opengl->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth, 0);
  } else {
    opengl->glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
  }

  opengl->glClear(clear);
}

void LithoViewer::resetFramebuffer()
{
  opengl->glMatrixMode(GL_PROJECTION);
  opengl->glPopMatrix();
  opengl->glMatrixMode(GL_MODELVIEW);
  opengl->glPopMatrix();

  opengl->glPopAttrib();

  opengl->glBindFramebuffer(GL_FRAMEBUFFER, baseFboId);
}

void LithoViewer::startScreenCoordinatesSystem(bool upward) const
{
  opengl->glMatrixMode(GL_PROJECTION);
  opengl->glPushMatrix();
  opengl->glLoadIdentity();
  if(upward)
    opengl->glOrtho(0, _current_device->width(), 0, _current_device->height(), 0.0, -1.0);
  else
    opengl->glOrtho(0, _current_device->width(), _current_device->height(), 0, 0.0, -1.0);

  opengl->glMatrixMode(GL_MODELVIEW);
  opengl->glPushMatrix();
  opengl->glLoadIdentity();
}

void LithoViewer::drawColorTexture(int i, bool draw_depth)
{
  // Check the texture ...
  opengl->glPolygonMode(GL_FRONT, GL_FILL);
  opengl->glEnable(GL_TEXTURE_2D);
  opengl->glMatrixMode(GL_TEXTURE);
  opengl->glPushMatrix();
  opengl->glLoadIdentity();
  opengl->glMatrixMode(GL_MODELVIEW);

  if(i >= 0) {
    if(draw_depth)
      opengl->glBindTexture(GL_TEXTURE_2D, depthTexId[i]);
    else
      opengl->glBindTexture(GL_TEXTURE_2D, colorTexId[i]);
  }

  // For now, render the depth buffer instead of the color one
  startScreenCoordinatesSystem(true);

  opengl->glBegin(GL_QUADS);
  opengl->glColor4f(1, 1, 1, 1);
  opengl->glTexCoord2d(0, 0);
  opengl->glVertex2d(0, 0);
  opengl->glTexCoord2d(1, 0);
  opengl->glVertex2d(_drawWidth, 0);
  opengl->glTexCoord2d(1, 1);
  opengl->glVertex2d(_drawWidth, _drawHeight);
  opengl->glTexCoord2d(0, 1);
  opengl->glVertex2d(0, _drawHeight);
  opengl->glEnd();

  stopScreenCoordinatesSystem();

  opengl->glMatrixMode(GL_TEXTURE);
  opengl->glPopMatrix();
  opengl->glMatrixMode(GL_MODELVIEW);

  if(i >= 0)
    opengl->glBindTexture(GL_TEXTURE_2D, 0);
  opengl->glDisable(GL_TEXTURE_2D);
}

void LithoViewer::alternatePeels(int& curPeelId, int& prevPeelId, int fullImgId)
{
  if(curPeelId == -1) {
    curPeelId = 3;
  } else if(curPeelId == 3) {
    prevPeelId = 3;
    curPeelId = 4;
  } else {
    prevPeelId = 4;
    curPeelId = 3;
  }

  if(fullImgId != -1) {
    Shader::activeTexture(Shader::AT_FRONT_TEX);
    opengl->glBindTexture(GL_TEXTURE_2D, depthTexId[fullImgId]);
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
    Shader::activeTexture(Shader::AT_NONE);
  }

  resetupFramebuffer(depthTexId[curPeelId], colorTexId[curPeelId]);
}

void LithoViewer::combinePeels(int& fullImgId, int curPeelId, int volume1, int volume2)
{
  int newFinalId = (fullImgId == FI_FULL_IMG1) ? FI_FULL_IMG2 : FI_FULL_IMG1;
  resetupFramebuffer(depthTexId[newFinalId], colorTexId[newFinalId]);
  combine_shader.setUniform("front", GLSLValue(1));
  Shader::activeTexture(1);
  opengl->glBindTexture(GL_TEXTURE_2D, colorTexId[fullImgId]);

  combine_shader.setUniform("front_depth", GLSLValue(2));
  Shader::activeTexture(2);
  opengl->glBindTexture(GL_TEXTURE_2D, depthTexId[fullImgId]);
  opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);

  combine_shader.setUniform("back", GLSLValue(3));
  Shader::activeTexture(3);
  opengl->glBindTexture(GL_TEXTURE_2D, colorTexId[curPeelId]);

  combine_shader.setUniform("back_depth", GLSLValue(4));
  Shader::activeTexture(4);
  opengl->glBindTexture(GL_TEXTURE_2D, depthTexId[curPeelId]);
  opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);

  combine_shader.setUniform("volume1", GLSLValue(5));
  Shader::activeTexture(5);
  opengl->glBindTexture(GL_TEXTURE_2D, colorTexId[volume1]);

  combine_shader.setUniform("volume2", GLSLValue(6));
  Shader::activeTexture(6);
  opengl->glBindTexture(GL_TEXTURE_2D, colorTexId[volume2]);

  Shader::activeTexture(Shader::AT_NONE);
  opengl->glBindTexture(GL_TEXTURE_2D, 0);

  combine_shader.useShaders();
  combine_shader.setupUniforms();

  drawColorTexture(-1);

  combine_shader.stopUsingShaders();
  fullImgId = newFinalId;
}

void LithoViewer::fastDraw()
{
  _fast_draw = true;
  draw();
  _fast_draw = false;
}

void LithoViewer::draw() {
  draw(this);
}

bool LithoViewer::render3D()
{
  REPORT_GL_ERROR("Start of render3D");
  // Draw the stack
  if(!raycasting_shader1.initialized() or !raycasting_shader2.initialized() or !volume_surf_shader1.initialized()
     or !volume_surf_shader2.initialized() or !texture_surf_shader.initialized()) {
    SETSTATUS("Error, shaders not correctly initialized. Cannot render objects");
    return false;
  }

  raycasting_shader1.setUniform("tex", GLSLValue(Shader::AT_TEX3D));
  raycasting_shader1.setUniform("colormap", GLSLValue(Shader::AT_CMAP_TEX));
  raycasting_shader1.setUniform("second_tex", GLSLValue(Shader::AT_SECOND_TEX3D));
  raycasting_shader1.setUniform("second_colormap", GLSLValue(Shader::AT_SECOND_CMAP_TEX));
  raycasting_shader1.setUniform("front", GLSLValue(Shader::AT_FRONT_TEX));
  raycasting_shader1.setUniform("front_color", GLSLValue(Shader::AT_FRONT_COLOR_TEX));
  raycasting_shader1.setUniform("occlusion", GLSLValue(Shader::AT_OCCLUSION_TEX));
  raycasting_shader1.setUniform("back", GLSLValue(Shader::AT_BACK_TEX));
  raycasting_shader1.setUniform("solid", GLSLValue(Shader::AT_DEPTH_TEX));
  raycasting_shader1.setUniform("labelcolormap", GLSLValue(Shader::AT_LABEL_TEX));
  raycasting_shader1.setUniform("nb_colors", GLSLValue(int(ImgData::LabelColors.size())));

  raycasting_shader2.setUniform("tex", GLSLValue(Shader::AT_TEX3D));
  raycasting_shader2.setUniform("colormap", GLSLValue(Shader::AT_CMAP_TEX));
  raycasting_shader2.setUniform("second_tex", GLSLValue(Shader::AT_SECOND_TEX3D));
  raycasting_shader2.setUniform("second_colormap", GLSLValue(Shader::AT_SECOND_CMAP_TEX));
  raycasting_shader2.setUniform("front", GLSLValue(Shader::AT_FRONT_TEX));
  raycasting_shader2.setUniform("front_color", GLSLValue(Shader::AT_FRONT_COLOR_TEX));
  raycasting_shader2.setUniform("occlusion", GLSLValue(Shader::AT_OCCLUSION_TEX));
  raycasting_shader2.setUniform("back", GLSLValue(Shader::AT_BACK_TEX));
  raycasting_shader2.setUniform("solid", GLSLValue(Shader::AT_DEPTH_TEX));
  raycasting_shader2.setUniform("labelcolormap", GLSLValue(Shader::AT_LABEL_TEX));
  raycasting_shader2.setUniform("nb_colors", GLSLValue(int(ImgData::LabelColors.size())));

  volume_surf_shader1.setUniform("backDepth", GLSLValue(Shader::AT_DEPTH_TEX));
  volume_surf_shader1.setUniform("frontDepth", GLSLValue(Shader::AT_FRONT_TEX));
  volume_surf_shader1.setUniform("testFront", GLSLValue(false));
  volume_surf_shader1.setUniform("tex", GLSLValue(Shader::AT_TEX3D));
  volume_surf_shader1.setUniform("colormap", GLSLValue(Shader::AT_CMAP_TEX));
  volume_surf_shader1.setUniform("second_tex", GLSLValue(Shader::AT_SECOND_TEX3D));
  volume_surf_shader1.setUniform("second_colormap", GLSLValue(Shader::AT_SECOND_CMAP_TEX));
  volume_surf_shader1.setUniform("labelcolormap", GLSLValue(Shader::AT_LABEL_TEX));
  volume_surf_shader1.setUniform("nb_colors", GLSLValue(int(ImgData::LabelColors.size())));

  volume_surf_shader2.setUniform("backDepth", GLSLValue(Shader::AT_DEPTH_TEX));
  volume_surf_shader2.setUniform("frontDepth", GLSLValue(Shader::AT_FRONT_TEX));
  volume_surf_shader2.setUniform("testFront", GLSLValue(false));
  volume_surf_shader2.setUniform("tex", GLSLValue(Shader::AT_TEX3D));
  volume_surf_shader2.setUniform("colormap", GLSLValue(Shader::AT_CMAP_TEX));
  volume_surf_shader2.setUniform("second_tex", GLSLValue(Shader::AT_SECOND_TEX3D));
  volume_surf_shader2.setUniform("second_colormap", GLSLValue(Shader::AT_SECOND_CMAP_TEX));
  volume_surf_shader2.setUniform("labelcolormap", GLSLValue(Shader::AT_LABEL_TEX));
  volume_surf_shader2.setUniform("nb_colors", GLSLValue(int(ImgData::LabelColors.size())));

  index_surf_shader.setUniform("backDepth", GLSLValue(Shader::AT_DEPTH_TEX));
  index_surf_shader.setUniform("frontDepth", GLSLValue(Shader::AT_FRONT_TEX));
  index_surf_shader.setUniform("testFront", GLSLValue(false));
  index_surf_shader.setUniform("surfcolormap", GLSLValue(Shader::AT_SURF_TEX));
  index_surf_shader.setUniform("labelcolormap", GLSLValue(Shader::AT_LABEL_TEX));
  index_surf_shader.setUniform("heatcolormap", GLSLValue(Shader::AT_HEAT_TEX));
  index_surf_shader.setUniform("nb_colors", GLSLValue(int(ImgData::LabelColors.size())));
  index_surf_shader.setUniform("labels", GLSLValue(false));
  index_surf_shader.setUniform("heatmap", GLSLValue(false));

  texture_surf_shader.setUniform("backDepth", GLSLValue(Shader::AT_DEPTH_TEX));
  texture_surf_shader.setUniform("frontDepth", GLSLValue(Shader::AT_FRONT_TEX));
  texture_surf_shader.setUniform("testFront", GLSLValue(false));

  REPORT_GL_ERROR("uniforms setup");

  // Save projection and modelview matrices
  Color4f clearColor = Colors::getColor(Colors::BackgroundColor);
  opengl->glClearColor(clearColor.r(), clearColor.g(), clearColor.b(), 1.0);

  setupFramebuffer(depthTexId[FI_BACKGROUND], colorTexId[FI_BACKGROUND]);

  texture_surf_shader.setUniform("peeling", GLSLValue(false));
  index_surf_shader.setUniform("peeling", GLSLValue(false));
  volume_surf_shader1.setUniform("peeling", GLSLValue(false));
  volume_surf_shader2.setUniform("peeling", GLSLValue(false));

  // ### Begin draw opaque

  opengl->glDisable(GL_BLEND);

  _clip1.drawGrid(sceneRadius);
  _clip2.drawGrid(sceneRadius);
  _clip3.drawGrid(sceneRadius);

  if(gridIsDrawn()) {
    opengl->glLineWidth(1.0);
    drawGrid(camera()->sceneRadius());
  }
  if(axisIsDrawn()) {
    opengl->glLineWidth(2.0);
    drawAxis(camera()->sceneRadius());
  }

  _stack1->drawBBox();
  _stack2->drawBBox();

  _clip1.drawClip();
  _clip2.drawClip();
  _clip3.drawClip();
  REPORT_GL_ERROR("Clipping planes setup");

  _stack1->drawMesh(false);
  _stack2->drawMesh(false);

  opengl->glEnable(GL_LIGHTING);
  if(_stack1->showOpaqueSurface()) {
    setLighting(0);
    _stack1->drawSurf(false, &texture_surf_shader, &index_surf_shader, &volume_surf_shader1);
  }
  if(_stack2->showOpaqueSurface()) {
    setLighting(1);
    _stack2->drawSurf(false, &texture_surf_shader, &index_surf_shader, &volume_surf_shader2);
  }

  _cutSurf->drawCutSurfGrid(*_stack1);
  _cutSurf->drawCutSurfGrid(*_stack2);

  if(_cutSurf->showOpaqueSurface(*_stack1)) {
    setLighting(0);
    _cutSurf->drawCutSurf(*_stack1, false, &volume_surf_shader1);
  }

  if(_cutSurf->showOpaqueSurface(*_stack2)) {
    setLighting(1);
    _cutSurf->drawCutSurf(*_stack2, false, &volume_surf_shader2);
  }

  _stack1->drawCellAxis();
  _stack2->drawCellAxis();
  _stack1->drawVertexVertexLine(_stack2);
  _stack2->drawVertexVertexLine(_stack1);

  // ### End draw opaque

  opengl->glClearColor(0, 0, 0, 0);

  Shader::activeTexture(Shader::AT_DEPTH_TEX);
  opengl->glBindTexture(GL_TEXTURE_2D, depthTexId[FI_BACKGROUND]);
  opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
  Shader::activeTexture(Shader::AT_NONE);

  opengl->glEnable(GL_DEPTH_TEST);

  int fullImgId = FI_FULL_IMG1;

  // First, clean the first peel and set it "full front"
  opengl->glClearDepth(0.0);
  opengl->glClearColor(0, 0, 0, 0);
  resetupFramebuffer(depthTexId[fullImgId], colorTexId[fullImgId]);

  opengl->glClearDepth(1.0);

  texture_surf_shader.setUniform("peeling", GLSLValue(true));
  index_surf_shader.setUniform("peeling", GLSLValue(true));
  volume_surf_shader1.setUniform("peeling", GLSLValue(true));
  volume_surf_shader2.setUniform("peeling", GLSLValue(true));

  // Clear occlusion texture
  opengl->glClearColor(0, 1, 0, 1);
  resetupFramebuffer(0, colorTexId[FI_OCCLUSION]);

  opengl->glClearColor(0, 0, 0, 0);

  // Create query
  GLuint occ_query, nb_pixels;
  opengl->glGenQueries(1, &occ_query);
  for(int current_pass = 0; current_pass < _maxNbPeels; ++current_pass) {
    // Setup the last full image as front texture

    // Set the framebuffer to write the current peel

    resetupFramebuffer(depthTexId[FI_CUR_PEEL], colorTexId[FI_CUR_PEEL]);

    if(current_pass == 0 and !_fast_draw) {
      // Now, copy the depth buffer, only on the first pass, to get the correct
      // depth of the surface
      setupCopyFB(depthTexId[FI_BACKGROUND], colorTexId[FI_BACKGROUND]);

      opengl->glBindFramebuffer(GL_READ_FRAMEBUFFER, fboCopyId);
      opengl->glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fboId);

      opengl->glBlitFramebuffer(0, 0, _texWidth, _texHeight, 0, 0, _texWidth, _texHeight, GL_DEPTH_BUFFER_BIT, GL_NEAREST);

      opengl->glBindFramebuffer(GL_FRAMEBUFFER, fboId);
    }

    Shader::activeTexture(Shader::AT_DEPTH_TEX);
    opengl->glBindTexture(GL_TEXTURE_2D, depthTexId[FI_BACKGROUND]);
    Shader::activeTexture(Shader::AT_FRONT_TEX);
    opengl->glBindTexture(GL_TEXTURE_2D, depthTexId[fullImgId]);
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
    Shader::activeTexture(Shader::AT_NONE);

    // ### Begin draw transparent

    opengl->glEnable(GL_LIGHTING);
    if(_stack1->showTransparentSurface()) {
      setLighting(0);
      _stack1->drawSurf(false, &texture_surf_shader, &index_surf_shader, &volume_surf_shader1);
    }
    if(_stack2->showTransparentSurface()) {
      setLighting(1);
      _stack2->drawSurf(false, &texture_surf_shader, &index_surf_shader, &volume_surf_shader2);
    }
    // Next, the cutting surface
    if(_cutSurf->showTransparentSurface(*_stack1)) {
      setLighting(0);
      _cutSurf->drawCutSurf(*_stack1, false, &volume_surf_shader1);
    }
    if(_cutSurf->showTransparentSurface(*_stack2)) {
      setLighting(1);
      _cutSurf->drawCutSurf(*_stack2, false, &volume_surf_shader2);
    }

    // ### End draw transparent

    // And the volume

    // Setup depth buffers
    Shader::activeTexture(Shader::AT_BACK_TEX);
    opengl->glBindTexture(GL_TEXTURE_2D, depthTexId[FI_CUR_PEEL]);
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
    Shader::activeTexture(Shader::AT_FRONT_TEX);
    opengl->glBindTexture(GL_TEXTURE_2D, depthTexId[fullImgId]);
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
    Shader::activeTexture(Shader::AT_FRONT_COLOR_TEX);
    opengl->glBindTexture(GL_TEXTURE_2D, colorTexId[fullImgId]);
    Shader::activeTexture(Shader::AT_OCCLUSION_TEX);
    opengl->glBindTexture(GL_TEXTURE_2D, colorTexId[FI_OCCLUSION]);
    Shader::activeTexture(Shader::AT_NONE);
    opengl->glBindTexture(GL_TEXTURE_2D, 0);

    resetupFramebuffer(0, colorTexId[FI_VOLUME1]);
    _stack1->drawStack(&raycasting_shader1);

    resetupFramebuffer(0, colorTexId[FI_VOLUME2]);
    _stack2->drawStack(&raycasting_shader2);

    combinePeels(fullImgId, FI_CUR_PEEL, FI_VOLUME1, FI_VOLUME2);

    // Check if there is anything else to be done

    resetupFramebuffer(0, colorTexId[FI_OCCLUSION]);

    Shader::activeTexture(1);
    opengl->glBindTexture(GL_TEXTURE_2D, depthTexId[fullImgId]);
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
    Shader::activeTexture(2);
    opengl->glBindTexture(GL_TEXTURE_2D, colorTexId[fullImgId]);
    Shader::activeTexture(3);
    opengl->glBindTexture(GL_TEXTURE_2D, depthTexId[FI_BACKGROUND]);
    Shader::activeTexture(Shader::AT_NONE);
    opengl->glBindTexture(GL_TEXTURE_2D, 0);

    occlusion_shader.setUniform("depth", GLSLValue(1));
    occlusion_shader.setUniform("color", GLSLValue(2));
    occlusion_shader.setUniform("background", GLSLValue(3));

    occlusion_shader.useShaders();
    occlusion_shader.setupUniforms();

    opengl->glBeginQuery(GL_SAMPLES_PASSED, occ_query);

    drawColorTexture(-1);

    opengl->glEndQuery(GL_SAMPLES_PASSED);

    opengl->glGetQueryObjectuiv(occ_query, GL_QUERY_RESULT, &nb_pixels);

    occlusion_shader.stopUsingShaders();

    if(DEBUG and current_pass == _maxNbPeels - 1 and nb_pixels > 0) {
      int prevFullImg = fullImgId;
      fullImgId = (fullImgId == FI_FULL_IMG1) ? FI_FULL_IMG2 : FI_FULL_IMG1;
      resetupFramebuffer(0, colorTexId[fullImgId]);
      opengl->glDisable(GL_DEPTH_TEST);
      drawColorTexture(prevFullImg);
      opengl->glEnable(GL_BLEND);
      drawColorTexture(FI_OCCLUSION);
      opengl->glDisable(GL_BLEND);
      opengl->glEnable(GL_DEPTH_TEST);
      SETSTATUS("Number of left pixels on pass #" << current_pass << " = " << nb_pixels);
    }

    // End check

    if(current_pass == 0 and !_fast_draw) {
      // Now, copy the depth buffer!
      opengl->glEnable(GL_TEXTURE_2D);
      opengl->glBindTexture(GL_TEXTURE_2D, depthTexId[FI_CUR_PEEL]);
      depthTexture.resize(_texWidth * _texHeight);
      opengl->glGetTexImage(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, GL_FLOAT, depthTexture.data());
      opengl->glBindTexture(GL_TEXTURE_2D, 0);
      opengl->glDisable(GL_TEXTURE_2D);
    }
    if(_showSlice > -1) {
      if(current_pass == _showSlice) {
        int prevFullImg = fullImgId;
        fullImgId = (fullImgId == FI_FULL_IMG1) ? FI_FULL_IMG2 : FI_FULL_IMG1;

        opengl->glBindTexture(GL_TEXTURE_2D, depthTexId[FI_CUR_PEEL]);
        opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
        opengl->glBindTexture(GL_TEXTURE_2D, depthTexId[prevFullImg]);
        opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
        opengl->glBindTexture(GL_TEXTURE_2D, 0);

        // opengl->glEnable(GL_BLEND);
        // opengl->glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        opengl->glDisable(GL_DEPTH_TEST);

        opengl->glClearColor(0, 0, 0, 1);
        resetupFramebuffer(0, colorTexId[fullImgId]);
        switch(_sliceType) {
        case 0:
          drawColorTexture(FI_CUR_PEEL);
          break;
        case 1:
          drawColorTexture(FI_CUR_PEEL, true);
          break;
        case 2:
          drawColorTexture(FI_VOLUME1);
          break;
        case 3:
          drawColorTexture(FI_VOLUME2);
          break;
        case 4:
          drawColorTexture(prevFullImg);
          break;
        case 5:
          drawColorTexture(prevFullImg, true);
          break;
        case 6:
          drawColorTexture(FI_OCCLUSION);
          break;
        default:
          Information::out << "Unknown slice type: " << _sliceType << endl;
        }
        break;
      }
    }
    REPORT_GL_ERROR("Framebuffer section");
    if(nb_pixels == 0)     // TODO: Find out why some pixels are never empty!
    {
      if(DEBUG)
        Information::out << "# peels = " << current_pass + 1 << endl;
      if(_showSlice > current_pass) {
        int prevFullImg = fullImgId;
        fullImgId = (fullImgId == FI_FULL_IMG1) ? FI_FULL_IMG2 : FI_FULL_IMG1;
        opengl->glDisable(GL_DEPTH_TEST);

        opengl->glBindTexture(GL_TEXTURE_2D, depthTexId[prevFullImg]);
        opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);

        opengl->glClearColor(0, 0, 0, 1);
        resetupFramebuffer(0, colorTexId[fullImgId]);
        switch(_sliceType) {
        case 0:
          drawColorTexture(FI_BACKGROUND);
          break;
        case 1:
          drawColorTexture(FI_BACKGROUND, true);
          break;
        case 5:
          drawColorTexture(prevFullImg, true);
          break;
        default:
          drawColorTexture(prevFullImg);
          break;
        }
        break;
      }
      break;
    } else {
      if(DEBUG)
        Information::out << "# pixels at step " << current_pass << " = " << nb_pixels << endl;
    }
  }
  opengl->glDeleteQueries(1, &occ_query);

  _finalTexId = (fullImgId == FI_FULL_IMG1) ? FI_FULL_IMG2 : FI_FULL_IMG1;
  resetupFramebuffer(0, colorTexId[_finalTexId]);
  REPORT_GL_ERROR("Framebuffer section");

  Shader::activeTexture(Shader::AT_DEPTH_TEX);
  opengl->glBindTexture(GL_TEXTURE_2D, 0);
  Shader::activeTexture(Shader::AT_NONE);
  REPORT_GL_ERROR("Framebuffer section");

  REPORT_GL_ERROR("Framebuffer section");

  _clip1.disable();
  _clip2.disable();
  _clip3.disable();

  {
    int fullImgId = (_finalTexId == FI_FULL_IMG1) ? FI_FULL_IMG2 : FI_FULL_IMG1;
    resetupFramebuffer(depthTexId[_finalTexId], colorTexId[_finalTexId]);
    final_combine_shader.setUniform("front", GLSLValue(1));
    Shader::activeTexture(1);
    opengl->glBindTexture(GL_TEXTURE_2D, colorTexId[fullImgId]);

    final_combine_shader.setUniform("back", GLSLValue(3));
    Shader::activeTexture(3);
    opengl->glBindTexture(GL_TEXTURE_2D, colorTexId[FI_BACKGROUND]);

    Shader::activeTexture(Shader::AT_NONE);
    opengl->glBindTexture(GL_TEXTURE_2D, 0);

    final_combine_shader.useShaders();
    final_combine_shader.setupUniforms();

    drawColorTexture(-1);

    final_combine_shader.stopUsingShaders();
  }

  resetFramebuffer();

  return true;
}

void LithoViewer::draw(QPaintDevice* device)
{
  if(_quitting)
    return;

  REPORT_GL_ERROR("Begin of draw");
  _current_device = device;
  if(ImgData::MeshLineWidth == 0.0f) {
    GLfloat range[2];
    opengl->glGetFloatv(GL_LINE_WIDTH_RANGE, range);
    ImgData::MeshLineWidth = range[0];
  }
  if(updateFBOTex(_drawWidth, _drawHeight, _fast_draw))
    _need3Dupdate = true;

  uint oldSlices = ImgData::Slices;
  if(_fast_draw)
    ImgData::Slices /= _sampling;

  _stack1->reloadLabelTex();
  _stack2->reloadLabelTex();

  // Set the clipping region
  REPORT_GL_ERROR("Start of draw");

  if(_need3Dupdate)
    if(not render3D())
      return;
  _need3Dupdate = false;

  // Now, render on the final texture, before the screen

  // drawColorTexture(0);

  /*
     opengl->glEnable(GL_BLEND);
     opengl->glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
     opengl->glDisable(GL_DEPTH_TEST);

     if(_showSlice == -1)
     drawColorTexture(FI_BACKGROUND);

     if(fullImgId >= 0)
     {
     drawColorTexture(fullImgId);
     }
   */

  REPORT_GL_ERROR("resetFramebuffer()");

  opengl->glDisable(GL_DEPTH_TEST);

  post_process_shader.activeTexture(Shader::AT_FINAL_VOLUME_TEX);
  opengl->glBindTexture(GL_TEXTURE_2D, colorTexId[_finalTexId]);
  post_process_shader.activeTexture(Shader::AT_NONE);
  post_process_shader.setUniform("texId", GLSLValue(Shader::AT_FINAL_VOLUME_TEX));
  post_process_shader.setUniform("texSize", GLSLValue(Point2i(_texWidth, _texHeight)));
  post_process_shader.setUniform("brightness", GLSLValue(_globalBrightness));
  post_process_shader.setUniform("contrast", GLSLValue(_globalContrast));
  if(not _fast_draw and _unsharpStrength != 0) {
    post_process_shader.setUniform("unsharp", GLSLValue(true));
    post_process_shader.setUniform("amount", GLSLValue(_unsharpStrength));
    post_process_shader.setUniform("kernel", GLSLValue(unsharp_kernel, 9));
  }
  else
    post_process_shader.setUniform("unsharp", GLSLValue(false));
  post_process_shader.useShaders();
  post_process_shader.setupUniforms();

  drawColorTexture(-1);

  post_process_shader.stopUsingShaders();

  REPORT_GL_ERROR("drawColorTexture(0)");

  // ### Begin draw overlay

  // Draw cell correspondence
  drawCellMap();

  drawSelectRect();
  drawPixelCursor();

  // Draw the legend and scale bar
  drawColorBar();
  drawScaleBar();

  // ### End draw overlay

  opengl->glEnable(GL_DEPTH_TEST);

  if(_fast_draw)
    ImgData::Slices = oldSlices;

  REPORT_GL_ERROR("Draw End");
}

void LithoViewer::clipEnable()
{
  // Turn on clipping planes
  _clip1.drawClip();
  _clip2.drawClip();
  _clip3.drawClip();
}

void LithoViewer::clipDisable()
{
  // Turn off clipping planes
  _clip1.disable();
  _clip2.disable();
  _clip3.disable();
}

void LithoViewer::drawScaleBar()
{
  bool stk1 = (!_stack1->mesh->empty() and (_stack1->mesh->isSurfaceVisible() or _stack1->mesh->isMeshVisible()))
    or (_stack1->valid() and _stack1->isVisible());
  bool stk2 = (!_stack2->mesh->empty() and (_stack2->mesh->isSurfaceVisible() or _stack2->mesh->isMeshVisible()))
    or (_stack2->valid() and _stack2->isVisible());

  // Drawing must be on for at least one
  if(!stk1 and !stk2)
    return;

  try {
    ImgData::scaleBar.setScale(1e-6);
    ImgData::scaleBar.draw(this, _current_device);
  }
  catch(const QString& s) {
    SETSTATUS("LithoViewer::drawScaleBar() - " << s);
  }
  catch(...) {
    SETSTATUS("LithoViewer::drawScaleBar() - Unknown exception");
  }
}

void LithoViewer::drawColorBar()
{
  bool stk1 = _stack1->mesh->needsColorbar();
  bool stk2 = _stack2->mesh->needsColorbar();

  // Drawing must be on for at least one
  if(!stk1 and !stk2)
    return;

  // Leave if both on and ranges different
  if(stk1 and stk2) {
    if(_stack1->mesh->toShow() != _stack2->mesh->toShow())
      return;

    switch(_stack1->mesh->toShow()) {
    case Mesh::HEAT:
      if(_stack1->mesh->heatMapBounds() != _stack2->mesh->heatMapBounds())
        return;
      break;

    case Mesh::NORMAL:
      if(_stack1->mesh->signalBounds() != _stack2->mesh->signalBounds())
        return;
      break;

    case Mesh::LABEL:     // Cannot happen
    case Mesh::PARENT:     // Cannot happen
      return;
    }
  }

  ImgData* stk = (stk1 ? _stack1 : _stack2);

  switch(stk->mesh->toShow()) {
  case Mesh::HEAT: {
    stk->setupHeatColorMap();

    ImgData::colorBar.vmin = stk->mesh->heatMapBounds()[0];
    ImgData::colorBar.vmax = stk->mesh->heatMapBounds()[1];
    ImgData::colorBar.label = stk->mesh->heatMapUnit();
    ImgData::colorBar.draw(this, stk->heatTexId, _current_device);
  } break;
  case Mesh::NORMAL: {
    stk->setupSurfColorMap();

    ImgData::colorBar.vmin = stk->mesh->signalBounds()[0];
    ImgData::colorBar.vmax = stk->mesh->signalBounds()[1];
    ImgData::colorBar.label = stk->mesh->signalUnit();
    ImgData::colorBar.draw(this, stk->surfTexId, _current_device);
  }
  case Mesh::LABEL:
  case Mesh::PARENT:
    break;
  }
}

void LithoViewer::drawCellMap()
{
  if(!mainWindow()->runningProcess()) {
    bool stack1Draw
      = (_stack1->mesh->showMeshCellMap() and _stack1->VCount > 0 and !_stack1->mesh->labelCenter().empty());
    bool stack2Draw
      = (_stack2->mesh->showMeshCellMap() and _stack2->VCount > 0 and !_stack2->mesh->labelCenter().empty());

    if(!stack1Draw and !stack2Draw)
      return;

    opengl->glDisable(GL_TEXTURE_1D);
    opengl->glDisable(GL_TEXTURE_2D);
    opengl->glDisable(GL_TEXTURE_3D);
    opengl->glDisable(GL_BLEND);
    opengl->glDisable(GL_AUTO_NORMAL);
    opengl->glDisable(GL_LIGHTING);

    // opengl->glPolygonMode(GL_FRONT, GL_LINE);
    // opengl->glLineWidth(ImgData::MeshLineWidth);
    // Color4f meshColor = Colors::getColor(_stack1->MeshColor);
    // Color4f meshColor = ImgData::palette->getColor(_stack1->MeshColor);
    // meshColor /= 255.0;
    // opengl->glColor3fv(meshColor.data());

    // if(stack1Draw and stack2Draw) {
    //  opengl->glBegin(GL_LINES);
    //  forall(const IntPoint3fPair &p, _stack1->LabelCenter)
    //    if(p.first > 0 and _stack1->LabelCenter.find(p.first) != _stack1->LabelCenter.end()
    //                 and _stack2->LabelCenter.find(p.first) != _stack2->LabelCenter.end()) {
    //      Point3f p1 = _stack1->LabelCenter[p.first];
    //      Point3f p2 = _stack2->LabelCenter[p.first];
    //      p1 = Point3f(_stack1->getFrame()->inverseCoordinatesOf(Vec(p1)));
    //      p2 = Point3f(_stack2->getFrame()->inverseCoordinatesOf(Vec(p2)));
    //      opengl->glVertex3fv(p1.c_data());
    //      opengl->glVertex3fv(p2.c_data());
    //    }
    //  opengl->glEnd();
    //}
    opengl->glDisable(GL_DEPTH_TEST);
    if(stack1Draw) {
      opengl->glColor3fv(Colors::getColor(Colors::Mesh1CellsColor).c_data());
      forall(const IntPoint3fPair& p, _stack1->mesh->labelCenter()) {
        Point3f pos = Point3f(camera()->projectedCoordinatesOf(Vec(p.second), &_stack1->getFrame()));
        // Information::out << "Pos:" << pos << endl;
        drawText((int)pos[0], (int)pos[1], QString::number(p.first));
      }
    }
    if(stack2Draw) {
      opengl->glColor3fv(Colors::getColor(Colors::Mesh2CellsColor).c_data());
      forall(const IntPoint3fPair& p, _stack2->mesh->labelCenter()) {
        Point3f pos = Point3f(camera()->projectedCoordinatesOf(Vec(p.second), &_stack2->getFrame()));
        drawText((int)pos[0], (int)pos[1], QString::number(p.first));
      }
    }
  }
}

void LithoViewer::drawPixelCursor()
{
  if(!_pixelEditCursor)
    return;

  {
    startScreenCoordinatesSystem();
    opengl->glDisable(GL_LIGHTING);
    opengl->glDisable(GL_TEXTURE_1D);
    opengl->glDisable(GL_TEXTURE_2D);
    opengl->glDisable(GL_TEXTURE_3D);

    Color4f pixelColor = Colors::getColor(Colors::PixelEditColor);

    float pixelEditRadius = float(ImgData::PixelEditRadius);

    if(DEBUG) {
      Information::out << "Drawing pixelEditCursor around mouse position " << _mousePos.x() << "x" << _mousePos.y()
                       << endl;
      Information::out << "   Color: " << pixelColor << " - Radius: " << pixelEditRadius << endl;
    }

    opengl->glLineWidth(1.0);
    Point2f c(_mousePos.x(), _mousePos.y());
    opengl->glBegin(GL_LINE_LOOP);
    opengl->glColor4fv(pixelColor.c_data());
    opengl->glNormal3f(0, 0, 1);
    for(int i = 0; i < 24; ++i) {
      double a = 2 * M_PI * float(i) / 24;
      Point2f p = c + pixelEditRadius * Point2f(cos(a), sin(a));
      opengl->glVertex2fv(p.c_data());
    }
    opengl->glEnd();
    stopScreenCoordinatesSystem();
  }

  /*
     {
     QPainter paint(this);
     paint.setRenderHint(QPainter::HighQualityAntialiasing);
     QPen pen(Colors::getColor(Colors::PixelEditColor));
     pen.setWidth(0);
     paint.setPen(pen);
     paint.setBrush(Qt::NoBrush);
     paint.drawEllipse(mousePos, ImgData::PixelEditRadius, ImgData::PixelEditRadius);
     }
   */

  if(_drawClipBox) {
    opengl->glDisable(GL_LIGHTING);
    opengl->glDisable(GL_TEXTURE_1D);
    opengl->glDisable(GL_TEXTURE_2D);
    opengl->glDisable(GL_TEXTURE_3D);

    ImgData* stk = findEditedStack(false);
    if(stk) {
      Point3f bBox[2] = { stk->imageToWorld(stk->bBoxTex[0]), stk->imageToWorld(stk->bBoxTex[1]) };
      if(bBox[0].x() < bBox[1].x() and bBox[0].y() < bBox[1].y() and bBox[0].z() < bBox[1].z()) {
        opengl->glMatrixMode(GL_MODELVIEW);
        opengl->glPushMatrix();
        opengl->glMultMatrixd(stk->getFrame().worldMatrix());
        opengl->glBegin(GL_LINES);
        for(int i = 0; i < 2; i++)
          for(int j = 0; j < 2; j++) {
            for(int k = 0; k < 2; k++)
              opengl->glVertex3f(bBox[k].x(), bBox[i].y(), bBox[j].z());
            for(int k = 0; k < 2; k++)
              opengl->glVertex3f(bBox[i].x(), bBox[k].y(), bBox[j].z());
            for(int k = 0; k < 2; k++)
              opengl->glVertex3f(bBox[i].x(), bBox[j].y(), bBox[k].z());
          }
        opengl->glEnd();

        opengl->glPopMatrix();
      }
    }
  }
}

void LithoViewer::drawSelectRect()
{
  if(_selectMode != SELRECT)
    return;

  startScreenCoordinatesSystem();
  opengl->glDisable(GL_LIGHTING);
  opengl->glDisable(GL_TEXTURE_1D);
  opengl->glDisable(GL_TEXTURE_2D);
  opengl->glDisable(GL_TEXTURE_3D);
  opengl->glEnable(GL_BLEND);
  opengl->glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  opengl->glDisable(GL_DEPTH_TEST);

  opengl->glColor4f(0.2, 0.2, 0.2f, 0.5f);
  opengl->glPolygonMode(GL_FRONT, GL_FILL);
  opengl->glBegin(GL_QUADS);
  opengl->glVertex2i(_selectRect.left(), _selectRect.top());
  opengl->glVertex2i(_selectRect.right(), _selectRect.top());
  opengl->glVertex2i(_selectRect.right(), _selectRect.bottom());
  opengl->glVertex2i(_selectRect.left(), _selectRect.bottom());
  opengl->glEnd();

  opengl->glLineWidth(2.0);
  opengl->glColor4f(0.5f, 0.5f, 0.5f, 0.5f);
  opengl->glBegin(GL_LINE_LOOP);
  opengl->glVertex2i(_selectRect.left(), _selectRect.top());
  opengl->glVertex2i(_selectRect.right(), _selectRect.top());
  opengl->glVertex2i(_selectRect.right(), _selectRect.bottom());
  opengl->glVertex2i(_selectRect.left(), _selectRect.bottom());
  opengl->glEnd();

  stopScreenCoordinatesSystem();
}

void LithoViewer::setLighting(int stackid)
{
  opengl->glEnable(GL_COLOR_MATERIAL);
  opengl->glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
  opengl->glMatrixMode(GL_MODELVIEW);
  opengl->glPushMatrix();
  opengl->glLoadIdentity();

  ImgData* stack = (stackid == 0) ? _stack1 : _stack2;

  // Set lighting, 4 lights
  // GLfloat lightc[4] = {stack->mesh->brightness()/8.0f, stack->mesh->brightness()/8.0f,
  // stack->mesh->brightness()/8.0f, 1.0f };
  float br = stack->mesh->brightness();
  float lc = br / 8.0f;
  float la = br / 8.0f;
  float ls = _specular * br;
  Colorf lightc(lc, lc, lc, 1.0f);
  Colorf lights(ls, ls, ls, 1.0f);
  Colorf lighta(la, la, la, 1.0f);

  for(int i = 0; i < 4; ++i) {
    opengl->glLightfv(GL_LIGHT0 + i, GL_AMBIENT, lighta.c_data());
    opengl->glLightfv(GL_LIGHT0 + i, GL_DIFFUSE, lightc.c_data());
    opengl->glLightfv(GL_LIGHT0 + i, GL_SPECULAR, lights.c_data());
  }

  opengl->glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, _shininess);
  opengl->glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, lights.c_data());

  Point4f lightp0(1.0f, -1.0f, 1.0f, 0.0f);
  Point4f lightp1(-1.0f, -1.0f, 1.0f, 0.0f);
  Point4f lightp2(1.0f, 1.0f, 1.0f, 0.0f);
  Point4f lightp3(-1.0f, 1.0f, 1.0f, 0.0f);

  opengl->glLightfv(GL_LIGHT0, GL_POSITION, lightp0.c_data());
  opengl->glLightfv(GL_LIGHT1, GL_POSITION, lightp1.c_data());
  opengl->glLightfv(GL_LIGHT2, GL_POSITION, lightp2.c_data());
  opengl->glLightfv(GL_LIGHT3, GL_POSITION, lightp3.c_data());

  opengl->glEnable(GL_LIGHT0);
  opengl->glEnable(GL_LIGHT1);
  opengl->glEnable(GL_LIGHT2);
  opengl->glEnable(GL_LIGHT3);

  opengl->glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
  Colorf black(0, 0, 0, 1);
  opengl->glLightModelfv(GL_LIGHT_MODEL_AMBIENT, black.c_data());

  opengl->glPopMatrix();
}

// Read from parameter file
void LithoViewer::readParms(util::Parms& parms, QString section)
{
  parms(section, "DrawCellMap", _drawCellMap, false);
  parms(section, "DrawClipBox", _drawClipBox, false);
  parms(section, "Spinning", _spinning, 10000.0f);
  parms(section, "FlySpeed", _flySpeed, .005f);
  parms(section, "Brightness", _globalBrightness, .0f);
  parms(section, "Contrast", _globalContrast, 1.0f);
  parms.optional(section, "Shininess", _shininess);
  parms.optional(section, "Specular", _specular);
  parms.optional(section, "UnsharpStrength", _unsharpStrength);
  parms.optional(section, "MaxNbPeels", _maxNbPeels);
  if(_maxNbPeels < 1)
    _maxNbPeels = 1;
  Matrix4d m = 1;
  m(3,2) = sceneRadius; // OpenGL matrix are transposed ...
  if(parms(section, "CameraFrame", m, m))
    _needShowScene = false;
  camera()->frame()->setFromMatrix(m.c_data());

  _clip1.readParms(parms, "Clip1");
  _clip2.readParms(parms, "Clip2");
  _clip3.readParms(parms, "Clip3");
}

// Write to parameter file
void LithoViewer::writeParms(QTextStream& pout, QString section)
{
  pout << endl;
  pout << "[" << section << "]" << endl;
  pout << "DrawCellMap: " << (_drawCellMap ? "true" : "false") << endl;
  pout << "DrawClipBox: " << (_drawClipBox ? "true" : "false") << endl;
  pout << "Spinning: " << _spinning << endl;
  pout << "FlySpeed: " << _flySpeed << endl;
  pout << "Brightness: " << _globalBrightness << endl;
  pout << "Contrast: " << _globalContrast << endl;
  pout << "Shininess: " << _shininess << endl;
  pout << "Specular: " << _specular << endl;
  pout << "UnsharpStrength: " << _unsharpStrength << endl;
  pout << "MaxNbPeels: " << _maxNbPeels << endl;
  pout << "CameraFrame: " << Point16d(camera()->frame()->matrix()) << endl;
  pout << endl;

  _clip1.writeParms(pout, "Clip1");
  _clip2.writeParms(pout, "Clip2");
  _clip3.writeParms(pout, "Clip3");
}

// Set the current label and color
void LithoViewer::setLabel(int label)
{
  uint idx = label % ImgData::LabelColors.size();
  if(idx >= ImgData::LabelColors.size())
    return;

  QPixmap pix(24, 24);
  if(label == 0)
    pix.fill(QColor(0, 0, 0, 0));
  else {
    Point4f col = Point4f(ImgData::LabelColors[idx]);
    pix.fill(QColor(col.x() * 255, col.y() * 255, col.z() * 255));
  }
  QIcon icon = QIcon(pix);
  _selectedLabel = label;
  emit selectLabelChanged(label);
  emit setLabelColor(icon);
}

LithoGraphX* LithoViewer::mainWindow()
{
  QWidget* p = this;
  LithoGraphX* grx = 0;
  do {
    p = p->parentWidget();
    grx = dynamic_cast<LithoGraphX*>(p);
  } while(p and !grx);
  return grx;
}

// Find which stack to use for pixel editing
ImgData* LithoViewer::findEditedStack(bool printMsg)
{
  LithoGraphX* grx = mainWindow();
  if(!grx) {
    SETSTATUS("Could not find main window");
    return 0;
  }
  if(grx->activeStack() == 0 and _stack1->stack->work()->isVisible() and _stack1->valid())
    return _stack1;
  if(grx->activeStack() == 1 and _stack2->stack->work()->isVisible() and _stack2->valid())
    return _stack2;
  if(printMsg)
    SETSTATUS("Stack edit operations require a stack loaded with work stack on");
  return (0);
}

lgx::ImgData* LithoViewer::findSelectStack(bool printMsg)
{
  LithoGraphX* grx = mainWindow();
  if(!grx) {
    SETSTATUS("Could not find main window");
    return 0;
  }
  if(grx->activeStack() == 0 and _stack1->stack->currentStore() and _stack1->valid())
    return _stack1;
  if(grx->activeStack() == 1 and _stack2->stack->currentStore() and _stack2->valid())
    return _stack2;
  if(printMsg)
    SETSTATUS("Stack selection operations require a stack loaded with a store on");
  return (0);
}

// Find which stack to use for label selection
ImgData* LithoViewer::findSelectSurf(bool printMsg)
{
  LithoGraphX* grx = mainWindow();
  if(!grx) {
    SETSTATUS("Could not find main window");
    return 0;
  }
  if(grx->activeMesh() == 0 and _stack1->mesh->isSurfaceVisible())   // and _stack1->SurfLabels)
    return _stack1;
  if(grx->activeMesh() == 1 and _stack2->mesh->isSurfaceVisible())   // and _stack2->SurfLabels)
    return _stack2;
  if(printMsg)
    SETSTATUS("Surface edit/label operations require a surface loaded with draw labels on.");
  return (0);
}

// Find which mesh to use for mesh selection
ImgData* LithoViewer::findSelectMesh(bool printMsg)
{
  LithoGraphX* grx = mainWindow();
  if(!grx) {
    SETSTATUS("Could not find main window");
    return 0;
  }
  if(grx->activeMesh() == 0 and _stack1->mesh->isMeshVisible()
     and (_stack1->mesh->showMeshPoints() or _stack1->mesh->showMeshLines()))
    return _stack1;
  if(grx->activeMesh() == 1 and _stack2->mesh->isMeshVisible()
     and (_stack2->mesh->showMeshPoints() or _stack1->mesh->showMeshLines()))
    return _stack2;
  if(printMsg)
    SETSTATUS("Mesh edit operations require a mesh loaded with draw points or draw lines on");
  return (0);
}

// Find selected triangle, respect the clipping planes
void LithoViewer::findSelectTriangle(ImgData* stk, uint x, uint y, std::vector<uint>& vList, int& label,
                                      bool useParentLabel)
{
  clipEnable();
  stk->findSelectTriangle(x, y, vList, label, useParentLabel);
  clipDisable();
}

// Get interface variables
void LithoViewer::getGUIFlags(QMouseEvent* e)
{
  _mousePos = e->pos();

  _shiftPressed = (QGuiApplication::keyboardModifiers() & Qt::ShiftModifier);
  _altPressed = (QGuiApplication::keyboardModifiers() & Qt::AltModifier);
  _controlPressed = (QGuiApplication::keyboardModifiers() & Qt::ControlModifier);

  _leftButton = (QGuiApplication::mouseButtons() == Qt::LeftButton or (QGuiApplication::mouseButtons() & Qt::LeftButton));
}

void LithoViewer::keyReleaseEvent(QKeyEvent* e)
{
  //if(!(QGuiApplication::keyboardModifiers() & Qt::AltModifier)) {
  if(_editionActive and not(QGuiApplication::keyboardModifiers() & Qt::AltModifier)) {
    stopEdition();
  }
  QGLViewer::keyReleaseEvent(e);
}

void LithoViewer::stopEdition()
{
  Information::out << "stopEdition()" << endl;
  _editionActive = false;
  switch(ImgData::tool) {
    case ImgData::ST_PIXEL_EDIT:
      {
        ImgData* stk = findEditedStack(true);
        if(stk) stk->pixelEditStop();
        checkPixelCursor(false);
        update3D();
      }
      break;
    case ImgData::ST_MESH_SELECT:
      meshEditStop();
      update3D();
      break;
    case ImgData::ST_ADD_NEW_SEED:
    case ImgData::ST_ADD_CURRENT_SEED:
    case ImgData::ST_GRAB_SEED:
    case ImgData::ST_PICK_LABEL:
    case ImgData::ST_FILL_LABEL:
    case ImgData::ST_SELECT_LABEL:
    case ImgData::ST_SELECT_CONNECTED:
      labelEditStop();
      update3D();
      break;
    case ImgData::ST_PICK_3D_LABEL:
    case ImgData::ST_DELETE_3D_LABEL:
    case ImgData::ST_FILL_3D_LABEL:
      break;
    case ImgData::ST_DELETE_LABEL:
      deletePickedLabel();
      update3D();
      break;
  }
}

void LithoViewer::enterEvent(QEvent* e)
{
  setFocus(Qt::MouseFocusReason);
  update();
  QGLViewer::enterEvent(e);
}

void LithoViewer::leaveEvent(QEvent* e)
{
  checkPixelCursor(false);
  update();
  QGLViewer::leaveEvent(e);
}

bool LithoViewer::handleKeyPress(int key, Qt::KeyboardModifiers mod)
{
  float fly = _flySpeed;
  // Grab x,y,z directions in correct frame
  Vec x(1.0, 0.0, 0.0), y(0.0, 1.0, 0.0), z(0.0, 0.0, 1.0), revpt(0, 0, 0);
  if(manipulatedFrame() and manipulatedFrame() != camera()->frame()) {
    x = manipulatedFrame()->transformOf(camera()->frame()->inverseTransformOf(x));
    y = manipulatedFrame()->transformOf(camera()->frame()->inverseTransformOf(y));
    z = manipulatedFrame()->transformOf(camera()->frame()->inverseTransformOf(z));
    fly *= -1;
    // Change revolve aroung pt or not?
  }
  if(key == Qt::Key_Alt and ImgData::tool == ImgData::ST_PIXEL_EDIT) {
    checkPixelCursor(true);
    return true;
  }
  switch(mod) {
    case Qt::NoModifier:{
      switch(key) {
        case Qt::Key_Minus:
          _flySpeed *= .9;
          _flySpeed = trim(_flySpeed, .001f, float(M_PI));
          return true;
        case Qt::Key_Right:     // rotate left/right
          manipulatedFrame()->rotateAroundPoint(Quaternion(z, fly), revpt);
          return true;
        case Qt::Key_Left:
          manipulatedFrame()->rotateAroundPoint(Quaternion(z, -fly), revpt);
          return true;
        case Qt::Key_Up:     // move in/out, for translations always use camera frame
          manipulatedFrame()->translate(camera()->frame()->inverseTransformOf(Vec(0.0, 0.0, -fly)));
          return true;
        case Qt::Key_Down:
          manipulatedFrame()->translate(camera()->frame()->inverseTransformOf(Vec(0.0, 0.0, fly)));
          return true;
        case Qt::Key_Delete:
        case Qt::Key_Backspace:
          emit deleteSelection();
          return true;
      }
    }
    break;
    case Qt::ShiftModifier: {
      switch(key) {
        case Qt::Key_Plus:
          _flySpeed *= 1.1;
          _flySpeed = trim(_flySpeed, .001f, float(M_PI));
          return true;
        case Qt::Key_Right:     // the translations
          manipulatedFrame()->translate(camera()->frame()->inverseTransformOf(Vec(-fly, 0.0, 0.0)));
          return true;
        case Qt::Key_Left:
          manipulatedFrame()->translate(camera()->frame()->inverseTransformOf(Vec(fly, 0.0, 0.0)));
          return true;
        case Qt::Key_Up:
          manipulatedFrame()->translate(camera()->frame()->inverseTransformOf(Vec(0.0, -fly, 0.0)));
          return true;
        case Qt::Key_Down:
          manipulatedFrame()->translate(camera()->frame()->inverseTransformOf(Vec(0.0, fly, 0.0)));
          return true;
      }
    }
    break;
    case Qt::ControlModifier: {
      switch(key) {
        case Qt::Key_Right:     // the other rotations
          manipulatedFrame()->rotateAroundPoint(Quaternion(y, fly), revpt);
          return true;
        case Qt::Key_Left:
          manipulatedFrame()->rotateAroundPoint(Quaternion(y, -fly), revpt);
          return true;
        case Qt::Key_Up:
          manipulatedFrame()->rotateAroundPoint(Quaternion(x, fly), revpt);
          return true;
        case Qt::Key_Down:
          manipulatedFrame()->rotateAroundPoint(Quaternion(x, -fly), revpt);
          return true;
      }
    }
    break;
#ifdef _MSC_VER
	case int(Qt::ControlModifier) | int(Qt::AltModifier):
#else
    case Qt::ControlModifier | Qt::AltModifier:
#endif
    switch(key) {
      case Qt::Key_F:
        showEntireScene();
        return true;
    }
  default:
    break;
  }
  return false;
}

// Keyboard actions, return true if handled, false to pass through
void LithoViewer::keyPressEvent(QKeyEvent* e)
{
  int key = e->key();

  if(handleKeyPress(key, QGuiApplication::keyboardModifiers()))
    update();
  else
    QGLViewer::keyPressEvent(e);
}

void LithoViewer::checkPixelCursor(bool altPressed)
{
  if(!_pixelEditCursor and altPressed and ImgData::tool == ImgData::ST_PIXEL_EDIT) {
    ImgData* stk = findEditedStack(false);
    if(stk) {
      _pixelEditCursor = true;
      qApp->setOverrideCursor(QCursor(Qt::BlankCursor));
      update();
    }
  } else if(_pixelEditCursor and !(altPressed and ImgData::tool == ImgData::ST_PIXEL_EDIT)) {
    _pixelEditCursor = false;
    qApp->restoreOverrideCursor();
    update();
  }
}

// Do pixel editing operations
void LithoViewer::pixelEdit(const QPoint& pos)
{
  // Find which surface selected
  ImgData* stk = findEditedStack(true);
  if(not stk) return;

  Point3f p(0, 0, 0), x(1, 0, 0), y(0, 1, 0), z(0, 0, 1);
  // Find select point
  bool surf = false;
  if(stk->stack->work()->isVisible() and _cutSurf->cut->isVisible()) {
    if(!stk->findSeedPoint(pos.x(), pos.y(), *_cutSurf, p))
      return;
    else
      surf = true;
  } else {
    p = (Point3f)camera()->unprojectedCoordinatesOf(Vec(pos.x(), pos.y(), 0.0), &stk->getFrame());
    x = (Point3f)camera()->unprojectedCoordinatesOf(Vec(pos.x() + 1, pos.y(), 0.0), &stk->getFrame());
    y = (Point3f)camera()->unprojectedCoordinatesOf(Vec(pos.x(), pos.y() - 1, 0.0), &stk->getFrame());
    z = (Point3f)camera()->unprojectedCoordinatesOf(Vec(pos.x(), pos.y(), 1.0), &stk->getFrame());
    x = (x - p).normalize();
    y = (y - p).normalize();
    z = (z - p).normalize();
  }
  _pixelRadius = ImgData::PixelEditRadius * camera()->pixelGLRatio(Vec(p));
  stk->pixelEdit(_pixelRadius, p, x, y, z, surf, _selectedLabel);
  if(not stk->pixelsChanged)
    update2D();
}

int LithoViewer::findLabel(const Store* store, Point3f start, Point3f dir) const
{
  const Stack* stk = store->stack();
  size_t loop_count = 0;
  start = Point3f(stk->getFrame().coordinatesOf(Vec(start)));
  dir = Point3f(stk->getFrame().transformOf(Vec(dir)));
  start = stk->worldToImagef(start);
  dir = stk->worldToImageVectorf(dir);
  Point3f end = start + dir;
  dir = normalized(dir);
  if(DEBUG) {
    Information::out << "findLabel() - stack coordinates" << endl;
    Information::out << "Start = " << start << " - End = " << end << " - dir = " << dir << endl;
  }
  int incx = dir.x() > 0 ? 1 : -1;
  int incy = dir.y() > 0 ? 1 : -1;
  int incz = dir.z() > 0 ? 1 : -1;
  Point3f incr_l(0, 0, 0);
  if(dir.x() > 0)
    incr_l.x() = 1;
  if(dir.y() > 0)
    incr_l.y() = 1;
  if(dir.z() > 0)
    incr_l.z() = 1;
  Point3u grid_size = stk->size();
  if(fabs(dir.x()) > EPSILON) {
    if(incx > 0) {
      start -= dir * (start.x() / dir.x());
      end -= dir * ((end.x() - grid_size.x()) / dir.x());
    } else {
      start -= dir * (start.x() - grid_size.x()) / dir.x();
      end -= dir * (end.x() / dir.x());
    }
    if(fabs(dir.y()) > EPSILON) {
      if(incy > 0) {
        if(start.y() < 0)
          start -= dir * (start.y() / dir.y());
        if(end.y() > grid_size.y())
          end -= dir * ((end.y() - grid_size.y()) / dir.y());
      } else {
        if(start.y() > grid_size.y())
          start -= dir * (start.y() - grid_size.y()) / dir.y();
        if(end.y() <= 0)
          end -= dir * (end.y() / dir.y());
      }
    }
    if(fabs(dir.z()) > EPSILON) {
      if(incz > 0) {
        if(start.z() < 0)
          start -= dir * (start.z() / dir.z());
        if(end.z() > grid_size.z())
          end -= dir * ((end.z() - grid_size.z()) / dir.z());
      } else {
        if(start.z() > grid_size.z())
          start -= dir * (start.z() - grid_size.z()) / dir.z();
        if(end.z() <= 0)
          end -= dir * (end.z() / dir.z());
      }
    }
  } else if(fabs(dir.y()) > EPSILON) {
    if(incy > 0) {
      start -= dir * (start.y() / dir.y());
      end -= dir * ((end.y() - grid_size.y()) / dir.y());
    } else {
      start -= dir * (start.y() - grid_size.y()) / dir.y();
      end -= dir * (end.y() / dir.y());
    }
    if(fabs(dir.z()) > EPSILON) {
      if(incz > 0) {
        if(start.z() < 0)
          start -= dir * (start.z() / dir.z());
        if(end.z() > grid_size.z())
          end -= dir * ((end.z() - grid_size.z()) / dir.z());
      } else {
        if(start.z() > grid_size.z())
          start -= dir * (start.z() - grid_size.z()) / dir.z();
        if(end.z() <= 0)
          end -= dir * (end.z() / dir.z());
      }
    }
  } else {
    if(incz > 0) {
      start -= dir * (start.z() / dir.z());
      end -= dir * ((end.z() - grid_size.z()) / dir.z());
    } else {
      start -= dir * (start.z() - grid_size.z()) / dir.z();
      end -= dir * (end.z() / dir.z());
    }
  }
  DEBUG_OUTPUT("Before clipping, line from " << start << " to " << end << endl);
  // Check if we cross the grid at all
  if((start.x() < 0 and end.x() < 0)or (start.x() > grid_size.x() and end.x() > grid_size.x())
     or (start.y() < 0 and end.y() < 0) or (start.y() > grid_size.y() and end.y() > grid_size.y())
     or (start.z() < 0 and end.z() < 0) or (start.z() > grid_size.z() and end.z() > grid_size.z())) {
    // No label possible
    return 0;
  }
  // Now, compute the intersection with the clipping planes
  Clip* clips[3] = { _c1, _c2, _c3 };
  double smm[16];
  stk->getFrame().getMatrix(smm);
  Matrix4f sm(smm, util::C_STYLE);
  Point4f hstart(stk->imageToWorld(start));
  hstart[3] = 1.f;
  Point4f hend(stk->imageToWorld(end));
  hend[3] = 1.f;
  DEBUG_OUTPUT("Before clipping, line from " << hstart << " to " << hend << endl);
  for(int i = 0; i < 3; ++i) {
    Clip* c = clips[i];
    if(c->enabled()) {
      DEBUG_OUTPUT("Clipping with clip " << i << endl);
      // Get the transform from clipping plane to stack frame
      double mm[16];
      c->frame().inverse().getMatrix(mm);
      Matrix4f cm(mm, util::C_STYLE);
      Matrix4f m = sm * cm;
      Point4f c0 = m * c->normalFormPos();
      Point4f _c1 = m * c->normalFormNeg();
      DEBUG_OUTPUT("c0 = " << c0 << endl);
      DEBUG_OUTPUT("_c1 = " << _c1 << endl);
      // Now perform the tests
      float testS0 = hstart * c0;
      float testE0 = hend * c0;
      float testS1 = hstart * _c1;
      float testE1 = hend * _c1;
      if((testS0 < 0 and testE0 < 0)or (testS1 < 0 and testE1 < 0)) {
        DEBUG_OUTPUT("Completely clipped" << endl);
        return 0;                                                     // This is completly clipped
      } else if((testS0 * testE0 < 0)or (testS1 * testE1 < 0))        // Partial clip
      {
        float lambda0 = testS0 / (testS0 - testE0);
        float lambda1 = testS1 / (testS1 - testE1);
        if(lambda0 > lambda1) {
          float a = lambda0;
          lambda0 = lambda1;
          lambda1 = a;
        }
        Point4f newStart = (lambda0 > 0) ? (1 - lambda0) * hstart + lambda0 * hend : hstart;
        Point4f newEnd = (lambda1 < 1) ? (1 - lambda1) * hstart + lambda1 * hend : hend;
        hstart = newStart;
        hend = newEnd;
        DEBUG_OUTPUT("Clipped to " << hstart << " to " << hend << endl);
      }
    }
  }

  hstart /= hstart[3];
  hend /= hend[3];

  start = stk->worldToImagef(Point3f(hstart));
  end = stk->worldToImagef(Point3f(hend));
  DEBUG_OUTPUT("Clipped to " << start << " to " << end << endl);

  Point3f cp = min(Point3f(grid_size - 1u), max(Point3f(0, 0, 0), start));
  Point3u cell(map(floorf, cp));
  if(DEBUG) {
    Information::out << "********* Start computeLine ***************" << endl;
    Information::out << "dir = " << dir << endl;
    Information::out << "start = " << start << " => " << cell << endl << endl;
  }
  // bool add_corners = false;
  size_t max_loop = grid_size.x() + grid_size.y() + grid_size.z();
  const HVecUS& data = store->data();
  while((end - start) * dir > EPSILON and loop_count++ < max_loop and stk->offset(cell) < stk->storeSize()) {
    // Check label
    ushort label = data[stk->offset(cell)];
    if(label > 0) {
      if(DEBUG)
        Information::out << "Found label: " << label << endl;
      return label;
    }
    Point3f l = cell;
    Point3f dl = l - start + incr_l;
    if(DEBUG)
      Information::out << "l = " << l << endl;
    Point3f r = divide(dl, dir);
    if(fabs(dir.x()) < EPSILON)
      r.x() = HUGE_VAL;
    if(fabs(dir.y()) < EPSILON)
      r.y() = HUGE_VAL;
    if(fabs(dir.z()) < EPSILON)
      r.z() = HUGE_VAL;
    Point3f ar = fabs(r);
    if(DEBUG)
      Information::out << "dl = " << dl << endl;
    if(DEBUG)
      Information::out << "r = " << r << endl;
    if(ar.x() <= ar.y() and ar.x() <= ar.z()) {
      start += dir * r.x();
      cell.x() += incx;
      if(ar.y() == ar.x())
        cell.y() += incy;
      if(ar.z() == ar.x())
        cell.z() += incz;
    } else if(ar.y() <= ar.x() and ar.y() <= ar.z()) {
      start += dir * r.y();
      cell.y() += incy;
      if(ar.z() == ar.y())
        cell.z() += incz;
    } else {
      start += dir * r.z();
      cell.z() += incz;
    }
    if(DEBUG)
      Information::out << "New start point = " << start << " => " << cell << endl << endl;
  }
  if(DEBUG) {
    if(loop_count >= max_loop) {
      Information::err << "Error, too many loop count" << endl;
    }
    Information::out << "######### End computeLine ###############" << endl;
  }
  return 0;
}

void LithoViewer::deleteVolumeLabel(const QPoint& pos)
{
  ImgData* img = findSelectStack(true);
  if(img) {
    Stack *stk = img->stack;
    Store* str = stk->currentStore();
    if(str and str->labels()) {
      Vec vorig, vdir;
      camera()->convertClickToLine(pos, vorig, vdir);
      int l = findLabel(str, Point3f(vorig), Point3f(vdir));
      if(l) {
        QStringList parms;
        parms << QString::number(l) << "0";
        LithoGraphX* grx = mainWindow();
        grx->launchProcess("Stack", "Fill Label", parms, false, false);
      }
      return;
    }
  }
  SETSTATUS("You need the active stack to be labeled.");
}

void LithoViewer::fillVolumeLabel(const QPoint& pos)
{
  ImgData* img = findSelectStack(true);
  if(img) {
    Stack* stk = img->stack;
    Store* str = stk->currentStore();
    if(str and str->labels()) {
      Vec vorig, vdir;
      camera()->convertClickToLine(pos, vorig, vdir);
      int l = findLabel(str, Point3f(vorig), Point3f(vdir));
      if(l) {
        QStringList parms;
        parms << QString::number(l) << QString::number(_selectedLabel);
        LithoGraphX* grx = mainWindow();
        grx->launchProcess("Stack", "Fill Label", parms, false, false);
      }
      return;
    }
  }
  SETSTATUS("You need the active stack to be labeled.");
}

void LithoViewer::pickVolumeLabel(const QPoint& pos)
{
  ImgData* img = findSelectStack(true);
  if(img) {
    Stack* stk = img->stack;
    Store* str = stk->currentStore();
    if(str and str->labels()) {
      Vec vorig, vdir;
      camera()->convertClickToLine(pos, vorig, vdir);
      int l = findLabel(str, Point3f(vorig), Point3f(vdir));
      SETSTATUS("Picked label " << l);
      setLabel(l);
      return;
    }
  }
  SETSTATUS("You need the active stack to be labeled.");
}

// Start mesh selecting/editing operations
void LithoViewer::meshEditStart(const QPoint& pos)
{
  // Save position
  _oldPos = pos;

  // Point id
  int pid = -1;
  Point3f vpos;

  // Handle cutting surface select
  if(_cutSurf->cut->drawGrid() and _cutSurf->cut->showPoints()) {
    clipEnable();
    pid = _cutSurf->findSelectPoint(pos.x(), pos.y());
    clipDisable();

    // If on a valid point
    if(pid >= 0) {
      // Find the control point position
      vpos = _cutSurf->cut->bezierV()[pid];
      // See if it is in select list and if not clear the pid
      if(_cutSurf->selectV.find(pid) == _cutSurf->selectV.end())
        pid = -1;
    }
    // If shift not pressed clear selection
    if(!_shiftPressed and !_controlPressed)
      _cutSurf->clearSelect();
  } else {   // Process mesh point select
    // Find which stack
    ImgData* stk = findSelectMesh(true);
    if(not stk) return;

    // Find selected point
    clipEnable();
    pid = stk->findSelectPoint(pos.x(), pos.y());
    clipDisable();

    // If on a valid point
    if(pid >= 0) {
      // Find the vertex
      vertex v(stk->pidVA[pid]);
      vpos = v->pos;
      // See if it is in select list and if not clear the pid
      if(stk->selectV.find(pid) == stk->selectV.end())
        pid = -1;
    }
    // If shift not pressed clear selection
    if(!_shiftPressed and !_controlPressed)
      stk->clearMeshSelect();
  }
  // If vertex not found, or not in select list, or shift not pressed, we are in SELRECT mode
  if(pid < 0 or !_shiftPressed or _controlPressed) {
    _selectMode = SELRECT;
    _selectRect.setBottomRight(pos);
    _selectRect.setTopLeft(pos);
  } else {
    // If we get here, we are in point move mode
    _selectMode = MOVE;
    _oldVPos = vpos;
    Information::out << "Selected point id " << pid << endl;
  }
}

void LithoViewer::meshEditMove(const QPoint& pos)
{
  if(_selectMode == MOVE) {
    // handle the moving of points
    Point3f newVPos = _oldVPos;
    if(_cutSurf->cut->drawGrid() and _cutSurf->cut->showPoints()) {
      // Move bezier points
      Point3f s = Point3f(camera()->projectedCoordinatesOf(Vec(_oldVPos), &_cutSurf->getFrame()));
      s.x() = pos.x();
      s.y() = pos.y();
      newVPos = Point3f(camera()->unprojectedCoordinatesOf(Vec(s), &_cutSurf->getFrame()));
      for(const uint u : _cutSurf->selectV)
        _cutSurf->cut->bezierV()[u] += newVPos - _oldVPos;
      _cutSurf->cut->hasChanged();
    } else {
      // Move mesh points
      ImgData* stk = findSelectMesh(false);
      if(not stk) return;

      Point3f s = Point3f(camera()->projectedCoordinatesOf(Vec(_oldVPos), &stk->getFrame()));
      s.x() = pos.x();
      s.y() = pos.y();
      newVPos = Point3f(camera()->unprojectedCoordinatesOf(Vec(s), &stk->getFrame()));
      forall(const uint u, stk->selectV) {
        vertex v = stk->pidVA[u];
        v->pos += newVPos - _oldVPos;
      }
      stk->updPos(stk->selectV, true);
    }
    _oldVPos = newVPos;
  } else if(_selectMode == SELRECT) {
    // Handle sizing of select rectangle
    _selectRect.setBottomRight(_oldPos);
    _selectRect.setTopLeft(pos);
    _selectRect = _selectRect.normalized();
  }
}

// Stop editing mesh
void LithoViewer::meshEditStop()
{
  if(_cutSurf->cut->drawGrid() and _cutSurf->cut->showPoints()) {
    // Add points to selected set
    if(_selectMode == SELRECT)
      for(uint i = 0; i < _cutSurf->cut->bezPoints() * _cutSurf->cut->bezPoints(); i++) {
        Point3f pos
          = Point3f(camera()->projectedCoordinatesOf(Vec(_cutSurf->cut->bezierV()[i]), &_cutSurf->getFrame()));
        if(_selectRect.contains(QPoint(int(pos.x() + .5), int(pos.y() + .5))))
          _cutSurf->selectV.insert(i);
      }
  } else {
    ImgData* stk = findSelectMesh(false);
    if(stk != 0) {
      // Add points to selected set
      // First find the position of the origin and the normal vector
      Point3f orig1 = Point3f(_c1->frame().inverseCoordinatesOf(Vec(0, 0, 0)));
      Point3f orig2 = Point3f(_c2->frame().inverseCoordinatesOf(Vec(0, 0, 0)));
      Point3f orig3 = Point3f(_c3->frame().inverseCoordinatesOf(Vec(0, 0, 0)));
      // Then the normal
      Point3f norm1 = Point3f(_c1->frame().inverseCoordinatesOf(Vec(_c1->normal()))) - orig1;
      Point3f norm2 = Point3f(_c2->frame().inverseCoordinatesOf(Vec(_c2->normal()))) - orig2;
      Point3f norm3 = Point3f(_c3->frame().inverseCoordinatesOf(Vec(_c3->normal()))) - orig3;

      std::vector<uint> vlist;
      if(_selectMode == SELRECT) {
        for(uint i = 0; i < stk->pntsVA.size(); i++) {
          vertex v = stk->pidVA[i];
          Point3f vpos = multiply(v->pos, stk->stack->scale());

          Point3f cvpos = Point3f(stk->getFrame().inverseCoordinatesOf(Vec(vpos)));

          if((!_c1->enabled() or (fabs((cvpos - orig1) * norm1) < _c1->width()))
             and (!_c2->enabled() or (fabs((cvpos - orig2) * norm2) < _c2->width()))
             and (!_c3->enabled() or (fabs((cvpos - orig3) * norm3) < _c3->width()))) {
            Point3f pos = Point3f(camera()->projectedCoordinatesOf(Vec(vpos), &stk->getFrame()));
            if(_selectRect.contains(QPoint(int(pos.x() + .5), int(pos.y() + .5))))
              vlist.push_back(i);
          }
        }
        if(_controlPressed)
          stk->removeSelect(vlist);
        else
          stk->addSelect(vlist);
      } else if(_selectMode == MOVE) {
        // Move selected points
        std::set<uint> vlist;
        for(uint i = 0; i < stk->idVA.size(); i++) {
          vertex v = stk->idVA[i];
          if(v->selected)
            vlist.insert(i);
        }
        stk->updPos(vlist, false);
      }
    }
  }
  _selectMode = NONE;
}

void LithoViewer::deletePickedLabel()
{
  ImgData* stk = findSelectSurf(true);
  if(stk != 0 and stk->hasSelectedVertices()) {
    // Find selected triangle in AUX0 buffer
    QStringList parms;
    parms << "-1";
    LithoGraphX* grx = mainWindow();
    grx->launchProcess("Mesh", "Delete Selection", parms, false, false);
  }
}

// Repeat is used from the label dialog to select a specific color.
void LithoViewer::selectMeshLabel(int label, int repeat, bool replace)
{
  ImgData* stk = findSelectSurf(true);
  if(stk and stk->mesh->isMeshVisible()) {
    if(label > 0 or repeat > 0) {
      if(replace)
        stk->clearMeshSelect();
      if(stk->mesh->toShow() == Mesh::PARENT) {
        stk->selectParent(label, repeat);
        SETSTATUS("Selected parent " << label);
      } else {
        stk->selectLabel(label, repeat);
        SETSTATUS("Selected label " << label);
      }
    }
  } else
    SETSTATUS("Selection operations require a visible mesh");
}

void LithoViewer::unselectMeshLabel(int label)
{
  ImgData* stk = findSelectSurf(true);
  if(not stk) return;
  if(stk->mesh->isMeshVisible()) {
    if(label > 0) {
      stk->unselectLabel(label);
      SETSTATUS("Area with label " << label << " unselected.");
    }
  } else
    SETSTATUS("Selection operations require a visible mesh");
}

void LithoViewer::grabLabel(const QPoint& pos)
{
  ImgData* stk = findSelectSurf(false);
  if(not stk) return;
  bool showParent = stk->mesh->toShow() == Mesh::PARENT;
  std::vector<uint> vlist;
  int label;
  findSelectTriangle(stk, pos.x(), pos.y(), vlist, label, false);
  if(label >= 0) {
    setLabel(label);
    SETSTATUS("Picked " << (showParent ? "parent" : "label ") << label);
  }
}

void LithoViewer::setParent(const QPoint& pos)
{
  ImgData* stk = findSelectSurf(false);
  if(not stk) return;
  std::vector<uint> vlist;
  int label;
  findSelectTriangle(stk, pos.x(), pos.y(), vlist, label, false);
  if(label > 0) {
    stk->setParent(label, _selectedLabel);
    SETSTATUS("Area with label " << label << " set to parent label " << _selectedLabel << ".");
  }
}
void LithoViewer::addCurrentSeed(const QPoint& pos)
{
  ImgData* stk = findSelectSurf(false);
  if(not stk) return;
  bool showParent = stk->mesh->toShow() == Mesh::PARENT;
  std::vector<uint> vlist;
  int label;
  findSelectTriangle(stk, pos.x(), pos.y(), vlist, label, false);
  if(vlist.empty()) return;
  stk->addSeed(_selectedLabel, vlist);
  if(showParent)
    SETSTATUS("Label and parent " << _selectedLabel << " seeded.");
  else
    SETSTATUS("Label " << _selectedLabel << " seeded.");
}
void LithoViewer::addNewSeed(const QPoint& pos)
{
  ImgData* stk = findSelectSurf(false);
  if(not stk) return;
  bool showParent = stk->mesh->toShow() == Mesh::PARENT;
  std::vector<uint> vlist;
  int label;
  findSelectTriangle(stk, pos.x(), pos.y(), vlist, label, false);
  if(vlist.empty()) return;
  if(showParent and stk->mesh->parentLabelMap().find(label) == stk->mesh->parentLabelMap().end())
    stk->mesh->parentLabelMap()[label] = label;
  label = stk->mesh->nextLabel();
  stk->addSeed(label, vlist);
  setLabel(label);
  if(showParent)
    SETSTATUS("Label and parent " << label << " added.");
  else
    SETSTATUS("Label " << label << " added.");
}

void LithoViewer::selectLabel(const QPoint& pos)
{
  ImgData* stk = findSelectSurf(false);
  if(not stk) return;
  std::vector<uint> vlist;
  int label;
  findSelectTriangle(stk, pos.x(), pos.y(), vlist, label, false);
  if(label > 0) {
    if(_controlPressed and not _shiftPressed)
      unselectMeshLabel(label);
    else
      selectMeshLabel(label, 0, _shiftPressed);
    setLabel(label);
    SETSTATUS("Area with label " << label << " selected.");
  }
}

void LithoViewer::fillLabel(const QPoint& pos)
{
  ImgData* stk = findSelectSurf(false);
  if(not stk) return;
  std::vector<uint> vlist;
  int label;
  findSelectTriangle(stk, pos.x(), pos.y(), vlist, label, false);
  if(label > 0) {
    stk->fillLabel(label, _selectedLabel);
    SETSTATUS("Area with label " << label << " filled with label " << _selectedLabel << ".");
  }
}

void LithoViewer::selectConnected(const QPoint& pos)
{
  ImgData* stk = findSelectSurf(false);
  if(not stk) return;
  std::vector<uint> vlist;
  int label;
  findSelectTriangle(stk, pos.x(), pos.y(), vlist, label, false);
  if(!_shiftPressed and !_controlPressed)
    stk->clearMeshSelect();
  stk->selectConnected(vlist, _controlPressed);
}

void LithoViewer::grabSeed(const QPoint& pos)
{
  ImgData* stk = findSelectSurf(false);
  if(not stk) return;
  bool showParent = stk->mesh->toShow() == Mesh::PARENT;
  std::vector<uint> vlist;
  int label;
  findSelectTriangle(stk, pos.x(), pos.y(), vlist, label, false);
  // Get other stack
  ImgData* ostk = _stack1;
  if(stk == _stack1)
    ostk = _stack2;
  if(ostk->mesh->size() <= 0)
    return;
  // Find center of selected triangle
  /*
   *Point3f seed_pos(0.0, 0.0, 0.0);
   *forall(uint u, vlist) {
   *  vertex v = stk->idVA[u];
   *  seed_pos += v->pos;
   *}
   *seed_pos /= float(vlist.size());
   */

  // Find selected triangle in AUX0 buffer
  std::vector<uint> ovlist;
  int olabel;
  findSelectTriangle(ostk, pos.x(), pos.y(), ovlist, olabel);
  if(ovlist.size() == 0)
    return;

  // Return if unlabeled in other mesh
  if(olabel <= 0) {
    setLabel(0);
    return;
  }

  // If grabbing parent add new seed (and map) or just map if already colored
  if(showParent) {
    // re-grab the actual label rather than the parent
    findSelectTriangle(stk, pos.x(), pos.y(), vlist, label, false);
    if(label > 0)
      stk->setParent(label, olabel);
  } else {
    // Just grabbing the seed
    stk->addSeed(olabel, vlist);
  }
  setLabel(olabel);

  if(showParent and label > 0)
    SETSTATUS("Parent label " << olabel << " grabbed.");
  else if(label > 0)
    SETSTATUS("Seed " << olabel << " grabbed.");
}


// Stop label editing
void LithoViewer::labelEditStop()
{
  // If we have been seeding, update the cell border lines if required.
  // We are painting on the surface
  ImgData* stk = findSelectSurf(false);
  if(not stk) return;
  stk->correctSelection(true);
  stk->updateSelection();
  stk->updateImgData();
}

bool LithoViewer::handleMousePress(const QPoint& pos, bool& redraw)
{
  // Information::err << "mousePressEvent(" << e->button() << ")" << endl;
  // Find which surface selected
  ImgData* stk = findSelectSurf(false);

  // Label vs parent radio button checked
  bool showParent = stk and stk->mesh->toShow() == Mesh::PARENT;
  bool showLabel = stk and stk->mesh->toShow() == Mesh::LABEL;

  // Alt-left button for select/edit
  if(_altPressed and _leftButton) {
    switch(ImgData::tool) {
      case ImgData::ST_PICK_LABEL:
        // Set current label (color)
        if((showLabel or showParent)) {
          grabLabel(pos);
          return true;
        }
        break;
      case ImgData::ST_ADD_NEW_SEED:
        // Add a new seed
        if(showLabel or showParent) {
          if(_shiftPressed)
            addCurrentSeed(pos);
          else
            addNewSeed(pos);
          redraw = true;
          return true;
        }
        break;
      case ImgData::ST_ADD_CURRENT_SEED:
        if(showLabel or showParent) {
          addCurrentSeed(pos);
          redraw = true;
          return true;
        }
        break;
      case ImgData::ST_FILL_LABEL:
        if(showLabel) {
          fillLabel(pos);
          redraw = true;
          return true;
        } else if(showParent) {
          setParent(pos);
          redraw = true;
          return true;
        }
        break;
      case ImgData::ST_SELECT_LABEL:
        if(showLabel or showParent) {
          _editionActive = true;
          selectLabel(pos);
          return true;
        }
        break;
      case ImgData::ST_SELECT_CONNECTED:
        _editionActive = true;
        selectConnected(pos);
        redraw = true;
        return true;
      case ImgData::ST_GRAB_SEED:
        if(showLabel or showParent)
          grabSeed(pos);
        break;
      case ImgData::ST_PIXEL_EDIT:
        stk = findEditedStack(true);
        if(stk) {
          stk->pixelEditStart(_clip1, _clip2, _clip3);
          _editionActive = true;
          pixelEdit(pos);
          redraw = true;
          return true;
        }
        break;
      case ImgData::ST_PICK_3D_LABEL:
        pickVolumeLabel(pos);
        return true;
      case ImgData::ST_DELETE_3D_LABEL:
        deleteVolumeLabel(pos);
        redraw = true;
        return true;
      case ImgData::ST_MESH_SELECT:
        _editionActive = true;
        meshEditStart(pos);
        redraw = true;
        return true;
      case ImgData::ST_DELETE_LABEL:
        stk = findSelectSurf(true);
        if(stk) {
          _editionActive = true;
          stk->clearMeshSelect();
          selectLabel(pos);
          redraw = true;
          return true;
        }
        break;
      case ImgData::ST_FILL_3D_LABEL:
        fillVolumeLabel(pos);
        redraw = true;
        return true;
    }
  }
  return false;
}

void LithoViewer::mousePressEvent(QMouseEvent *e)
{
  _lastButtons = e->buttons();
  getGUIFlags(e);
  bool redraw = false;
  if(handleMousePress(e->pos(), redraw)) {
    if(redraw)
      update3D();
  }
  else
    QGLViewer::mousePressEvent(e);
}

bool LithoViewer::handleMouseMove(const QPoint& pos, bool& redraw)
{
  ImgData* stk = findSelectSurf(false);
  bool showParent = stk and stk->mesh->toShow() == Mesh::PARENT;
  bool showLabel = stk and stk->mesh->toShow() == Mesh::LABEL;
  bool handled = false;

  // Check 3D editing cursor
  if(_altPressed and ImgData::tool == ImgData::ST_PIXEL_EDIT) {
    checkPixelCursor(true);
    redraw = true;
    handled = true;
  }

  if(_altPressed and _leftButton) {
    switch(ImgData::tool) {
      case ImgData::ST_PIXEL_EDIT:
        pixelEdit(pos);
        redraw = true;
        return true;
      case ImgData::ST_PICK_3D_LABEL:
        pickVolumeLabel(pos);
        return true;
      case ImgData::ST_MESH_SELECT:
        meshEditMove(pos);
        redraw = true;
        return true;
      case ImgData::ST_DELETE_LABEL:
        selectLabel(pos);
        redraw = true;
        return true;
      case ImgData::ST_GRAB_SEED:
        if(showParent) {
          setParent(pos);
          redraw = true;
          return true;
        } else if(showLabel) {
          addCurrentSeed(pos);
          redraw = true;
          return true;
        }
        break;
      case ImgData::ST_ADD_NEW_SEED:
      case ImgData::ST_ADD_CURRENT_SEED:
        if(showLabel) {
          addCurrentSeed(pos);
          redraw = true;
          return true;
        }
        break;
      case ImgData::ST_FILL_LABEL:
        if(showParent)
          setParent(pos);
        else
          fillLabel(pos);
        redraw = true;
        return true;
      case ImgData::ST_SELECT_LABEL:
        if(showLabel) {
          selectLabel(pos);
          redraw = true;
          return true;
        }
        break;
      case ImgData::ST_SELECT_CONNECTED:
        selectConnected(pos);
        redraw = true;
        return true;
      case ImgData::ST_DELETE_3D_LABEL:
      case ImgData::ST_FILL_3D_LABEL:
      case ImgData::ST_PICK_LABEL:
        // Do nothing ... too dangerous!
        break;
    }
  }
  return handled;
}

void LithoViewer::mouseMoveEvent(QMouseEvent* e)
{
  // Sometimes, Qt misses mouseReleaseEvent
  // So we fake them here
  int bts = _lastButtons & (~e->buttons());
  while(bts != 0) {
    int left_bts = e->buttons() | bts;
    int new_bts = bts & (bts - 1);
    int btn = bts - new_bts;
    QMouseEvent ev(QEvent::MouseButtonRelease, e->pos(), (Qt::MouseButton)btn, (Qt::MouseButtons)left_bts,
                   e->modifiers());
    mouseReleaseEvent(&ev);
    bts = new_bts;
  }
  _lastButtons = e->buttons();
  getGUIFlags(e);
  if(not _altPressed)
    checkPixelCursor(false);
  // For when we don't see the release of the Alt key
  if(_editionActive and not _altPressed)
    stopEdition();
  bool redraw = false;
  if(handleMouseMove(e->pos(), redraw) and redraw)
    update3D();
  else
    QGLViewer::mouseMoveEvent(e);
}

void LithoViewer::mouseReleaseEvent(QMouseEvent* e)
{
  // Information::err << "mouseReleaseEvent(" << e->button() << ")" << endl;
  _lastButtons = e->buttons();
  getGUIFlags(e);

  if(_editionActive and not _leftButton) {
    stopEdition();
  }

  // Always pass through
  QGLViewer::mouseReleaseEvent(e);
}

Point3f LithoViewer::pointUnderPixel(const QPoint& pos, bool& found)
{
  QPoint texPos = QPoint(pos.x(), _drawHeight - pos.y() - 1);
  float depth = depthTexture[texPos.y() * _drawWidth + texPos.x()];
  found = depth < 1.0;
  Vec point(pos.x(), pos.y(), depth);
  point = camera()->unprojectedCoordinatesOf(point);
  return Point3f(point);
}

bool LithoViewer::setRevolveAroundPointFromPixel(const QPoint& px) {
  bool found;
  Point3f pup = pointUnderPixel(px, found);
  if(found) {
    camera()->setRevolveAroundPoint(Vec(pup));
    return true;
  }
  return false;
}

// Toggle heatmap button
void LithoViewer::ToggleHeatMapSlot()
{
  //  if(ImgData::DrawLabels) {
  //    ImgData::DrawLabels = false;
  //    LithoGraphX::ui.DrawLabels->toggle();
  //  }
  //  if(!ImgData::DrawHeat) {
  //    ImgData::DrawHeat = true;
  //    LithoGraphX::ui.DrawHeat->toggle();
  //  }
}

//  Takes care of GUI others
void LithoViewer::resetView()
{
  Matrix4d m = Matrix4d::identity();
  camera()->setFromModelViewMatrix(m.data());
  camera()->setSceneCenter(Vec(0, 0, 0));
  camera()->setSceneRadius(sceneRadius);
  camera()->showEntireScene();
  _stack1->getMainFrame().setFromMatrix(m.data());
  _stack1->getTransFrame().setFromMatrix(m.data());
  _stack2->getMainFrame().setFromMatrix(m.data());
  _stack2->getTransFrame().setFromMatrix(m.data());
  _c1->frame().setFromMatrix(m.data());
  _c2->frame().setFromMatrix(m.data());
  _c3->frame().setFromMatrix(m.data());
  _cutSurf->cut->frame().setFromMatrix(m.data());
  _camera->resetZoom();
  update3D();
}

void LithoViewer::reloadShaders()
{
  texture_surf_shader.invalidate();
  volume_surf_shader1.invalidate();
  volume_surf_shader2.invalidate();
  index_surf_shader.invalidate();
  raycasting_shader1.invalidate();
  raycasting_shader2.invalidate();
  update3D();
}

void LithoViewer::UpdateLabels()
{
  _stack1->LabelColorsChanged = true;
  _stack2->LabelColorsChanged = true;
  update3D();
}

void LithoViewer::update3D()
{
  _need3Dupdate = true;
  update();
}

void LithoViewer::update2D()
{
  update();
}

void LithoViewer::loadFile(const QString& pth, bool load_stack2, bool load_work)
{
  QFileInfo fi(pth);
  QString ext = fi.suffix();
  ImgData* stack = (load_stack2) ? _stack2 : _stack1;

  if(ext == "fct") {
    stack->setColorMap(pth, load_work);
    update3D();
  }
}

bool LithoViewer::saveImageSnapshot(const QString& fileName, QSize finalSize, double oversampling, bool expand,
                                     bool has_gui)
{
  return saveImageSnapshot(fileName, finalSize, oversampling, expand, has_gui, 0);
}

bool LithoViewer::saveImageSnapshot(const QString& fileName, QSize finalSize, double oversampling, bool,
                                     bool has_gui, QString* error)
{
  if(!finalSize.isValid())
    finalSize = QSize(this->width(), this->height());
  QSize textureSize = oversampling * finalSize;
  if(oversampling != 1.0) {
    ImgData::scaleBar.scaleDrawing(oversampling);
    ImgData::colorBar.scaleDrawing(oversampling);
  }
  int savedWidth = _drawWidth;
  int savedHeight = _drawHeight;
  // _drawWidth = textureSize.width();
  // _drawHeight = textureSize.height();
  QGLFramebufferObjectFormat fboFormat;
  fboFormat.setAttachment(QGLFramebufferObject::Depth);
  QGLFramebufferObject fbo(textureSize, fboFormat);
  if(!fbo.bind()) {
    if(has_gui)
      QMessageBox::critical(0, "Error creating FBO",
                            QString("Unable to create a FBO of size %1x%2").arg(_drawWidth).arg(_drawHeight));
    if(error)
      *error = QString("Unable to create a FBO of size %1x%2").arg(_drawWidth).arg(_drawHeight);
    resizeGL(savedWidth, savedHeight);
    return false;
  }
  resizeGL(textureSize.width(), textureSize.height());
  baseFboId = fbo.handle();

  preDraw();
  draw(&fbo);
  postDraw();

  baseFboId = 0;
  resizeGL(savedWidth, savedHeight);

  if(!fbo.release()) {
    if(has_gui)
      QMessageBox::critical(0, "Error releasing FBO",
                            QString("Unable to release the FBO of size %1x%2").arg(_drawWidth).arg(_drawHeight));
    if(error)
      *error = QString("Unable to release the FBO of size %1x%2").arg(_drawWidth).arg(_drawHeight);
    return false;
  }

  QImage img = fbo.toImage();

  if(oversampling != 1.0) {
    img = img.scaled(finalSize, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    ImgData::scaleBar.restoreScale();
    ImgData::colorBar.restoreScale();
  }

  img.save(fileName);

  return true;
}

static const QString ext_to_filter[8][2] = { { QString("jpeg"), QString("JPEG") },
                                             { QString("jpg"), QString("JPEG") },
                                             { QString("png"), QString("PNG") },
                                             { QString("bmp"), QString("BMP") },
                                             { QString("ppm"), QString("PPM") },
                                             { QString("eps"), QString("EPS") },
                                             { QString("ps"), QString("PS") },
                                             { QString("xfig"), QString("XFIG") } };

static const QString filters = QObject::tr("JPEG images") + " (*.jpeg *.jpg)" + ";;" + QObject::tr("PNG images")
  + " (*.png)" + ";;" + QObject::tr("PPM images") + " (*.ppm)" + ";;"
  + QObject::tr("BMP images") + " (*.bmp)";

void LithoViewer::recordMovie(bool on)
{
  if(on) {
    QString filename, filetype;
    QString selectedFilter;
    filename = QFileDialog::getSaveFileName(this, "Record movie", "", filters, &selectedFilter);
    if(!filename.isEmpty()) {
      // Find out the type of the selected file
      QString filetype;
      for(int i = 0; i < 8; ++i) {
        QString ext = "." + ext_to_filter[i][0];
        if(filename.endsWith(ext, Qt::CaseInsensitive)) {
          filetype = ext_to_filter[i][1];
          filename = filename.left(filename.size() - filetype.size());
          break;
        }
      }
      if(filetype.isEmpty()) {
        int index = selectedFilter.indexOf(' ');
        filetype = selectedFilter.left(index);
      }
      setSnapshotCounter(0);
      setSnapshotFileName(filename);
      setSnapshotFormat(filetype);
      connect(this, SIGNAL(drawFinished(bool)), this, SLOT(saveScreenshot(bool)));
    } else {
      emit recordingMovie(false);
    }
  } else
    disconnect(this, SIGNAL(drawFinished(bool)), this, SLOT(saveScreenshot(bool)));
}

void LithoViewer::setSceneBoundingBox(const Point3f& bbox)
{
  if(norm(bbox) > 0) {
    sceneRadius = norm(bbox);
    emit changeSceneRadius(sceneRadius);

    if(_needShowScene) {
      setSceneRadius(sceneRadius);
      showEntireScene();
      _needShowScene = false;
    }
  }
  update3D();
}

void LithoViewer::enableClip(Clip* c, bool _val)
{
  if(_val)
    c->enable();
  else
    c->disable();
  update3D();
}

void LithoViewer::showClipGrid(Clip* c, bool _val)
{
  if(_val)
    c->showGrid();
  else
    c->hideGrid();
  update3D();
}

void LithoViewer::setClipWidth(Clip* c, int _val)
{
  c->setWidth(0.5 * sceneRadius * (float)exp(double(_val) / 2000.0));
  if(c->grid() or c->enabled())
    update3D();
}

void LithoViewer::dragEnterEvent(QDragEnterEvent* event)
{
  auto *grx = mainWindow();
  auto mime = event->mimeData();
  if(mime->hasUrls()) {
    auto urls = mime->urls();
    if(urls.size() == 1) {
      auto url = urls[0];
      if(grx->canAutoOpen(url.toLocalFile())) {
        event->accept();
        return;
      } else
        DEBUG_OUTPUT("Wrong type of files" << endl);
    } else
      DEBUG_OUTPUT("Too many files" << endl);
  } else
    DEBUG_OUTPUT("No urls" << endl);
  event->ignore();
}

void LithoViewer::dropEvent(QDropEvent* event)
{
  auto *grx = mainWindow();
  auto pth = event->mimeData()->urls()[0].toLocalFile();
  auto stack = grx->globalProcess()->currentStack();
  grx->autoOpen(pth, stack->id(), stack->currentStore() == stack->main());
}

void LithoViewer::resetLabel()
{
  setLabel(0);
}

void LithoViewer::wheelEvent(QWheelEvent* event)
{
  //QPoint delta = event->angleDelta();
  /*
   *Information::out << "Wheel event, delta = " << delta.x() << ", " << delta.y() << " / "
   *  << event->buttons() << endl;
   */
  QGLViewer::wheelEvent(event);
}

void LithoViewer::updateTextures()
{
  _stack1->check3DTexture();
  _stack2->check3DTexture();
}

