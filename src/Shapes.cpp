/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Shapes.hpp"

#include <ConvexHull.hpp>
#include <Progress.hpp>

#include <Information.hpp>
#include <Random.hpp>

namespace lgx {
namespace shape {

bool addTriangle(vvgraph& S, std::vector<vertex>& vertices, Point3i tri)
{
  vertex x = vertices[tri.x()];
  vertex y = vertices[tri.y()];
  vertex z = vertices[tri.z()];
  if(S.valence(x) == 0) {
    S.insertEdge(x, y);
    S.spliceAfter(x, y, z);
  } else {
    bool yinx = S.edge(x, y);
    bool zinx = S.edge(x, z);

    if(yinx and zinx) {     // Triangle already there
      if(S.nextTo(x, y) != z)
        SETSTATUS("addTriangle::Error:Error in nhbd");
      return true;
    } else if(yinx) {
      S.spliceAfter(x, y, z);
      return true;
    } else if(zinx) {
      S.spliceBefore(x, z, y);
      return true;
    }
  }
  return false;   // Did not insert triangle
}

bool meshFromTriangles(vvgraph& S, std::vector<vertex>& vertices, std::vector<Point3i>& triangles)
{
  // Insert vertices into mesh
  forall(const vertex& v, vertices)
    S.insert(v);

  // Make triangle list with all orientations.
  std::vector<Point3i> tris;
  tris.reserve(3 * triangles.size());
  forall(const Point3i tri, triangles) {
    tris.push_back(tri);
    tris.push_back(Point3i(tri.y(), tri.z(), tri.x()));
    tris.push_back(Point3i(tri.z(), tri.x(), tri.y()));
  }

  // Extract neighborhood from triangles,  make multiple passes until list empty
  while(!tris.empty()) {
    std::vector<Point3i> new_tris;
    new_tris.reserve(tris.size() / 4);
    bool success = false;
    for(size_t i = 0; i < tris.size(); i++) {
      if(!addTriangle(S, vertices, tris[i]))
        new_tris.push_back(tris[i]);
      else
        success = true;
    }
    if(!success)
      return false;       // Failed to add any triangle on this turn ... won't help again
    if(tris.size() == new_tris.size()) // We haven't improved!
      return false;
    swap(tris, new_tris);
  }
  return true;
}

AbstractShape::AbstractShape() { }
AbstractShape::~AbstractShape() { }
AbstractSphere::AbstractSphere() { }
AbstractSphere::~AbstractSphere() { }

bool AbstractSphere::addTo(Mesh* mesh,
                           const Point3f& center,
                           const Point3f& rx, const Point3f& ry, const Point3f& rz,
                           int label)
{
  vvgraph& S = mesh->graph();
  std::vector<vertex> vs(positions.size(), vertex(0));

  for(size_t i = 0; i < vs.size(); ++i) {
    Point3f p = positions[i];
    vertex v;
    v->pos = center + p.x() * rx + p.y() * ry + p.z() * rz;
    v->label = label;
    vs[i] = v;
  }

  bool res = meshFromTriangles(S, vs, triangles);
  if(not res)
    _errorString = "Failed to add all triangles, make sure they are connected by an edge or not at all.";
  return res;
}

UniformSphere::UniformSphere(size_t nb_points)
  : AbstractSphere()
{
  if(nb_points < 10) nb_points = 10;
  positions.reserve(nb_points);
  // Draw points uniformely on the surface of a sphere, using a poisson process
  float avg_radius = 4.*M_PI/(3*nb_points); // 4/3 π -> surface of a unit sphere
  float min_radius = avg_radius / 4;
  float min_radius_sq = min_radius *min_radius;

  Point3f range(2.f,2.f,2.f);

  Information::out << "Position points" << endl;

  while(positions.size() < nb_points) {
    Point3f dir = util::ran(range) - 1.f;
    auto l = norm(dir);
    if(l < 1e-8) continue; // too small to normalize

    dir /= l;
    bool ok = true;
    for(const Point3f& p: positions) {
      if(normsq(dir - p) < min_radius_sq) {
        ok = false;
        break;
      }
    }
    if(ok) {
      positions.push_back(dir);
    }
  }

  // Now, move points around until stability
  const auto N = positions.size();
  std::vector<Point3f> forces(N);
  bool moving = true;
  float min_movement = 1e-3*avg_radius;
  float min_movement_2 = min_movement * min_movement;
  float strength = 2/avg_radius;
  const size_t max_iterations = 10000;
  const size_t next_iter = max_iterations / 100;
  Progress progress("Compute Uniform Sphere", 103);
  for(size_t iter = 0 ; iter < max_iterations and moving ; ++iter) {
    if(iter % next_iter == 0) {
      if(not progress.advance(iter / next_iter)) {
        _errorString = "User cancelled computation";
        positions.clear();
        triangles.clear();
        moving = false;
      }
    }
    if(moving) {
#pragma omp parallel for
      for(size_t i = 0 ; i < N ; ++i) {
        Point3f f;
        for(size_t j = 0 ; j < N ; ++j) {
          if(i != j) {
            auto du = (positions[i] - positions[j]);
            f += strength * du / normsq(du);
          }
          forces[i] = f;
        }
      }
      moving = false;
#pragma omp parallel for reduction (|| : moving)
      for(size_t i = 0 ; i < N ; ++i) {
        Point3f new_pos = positions[i] + forces[i];
        new_pos.normalize();
        if(normsq(new_pos - positions[i]) > min_movement_2)
          moving = true;
        positions[i] = new_pos;
      }
    }
  }
  if(moving)
    Information::err << "Warning: convergence not reached while finding points positions" << endl;

  Information::out << "Create convex hull" << endl;

  progress.advance(101);
  triangles = qhull::convexHull(positions, &_errorString);

  Information::out << "Triangles created: " << (not triangles.empty()) << endl;
  if(not triangles.empty()) _valid = true;

  using std::swap;
  progress.advance(102);

  // Now, make sure triangles are correctly oriented
  for(auto& tr: triangles) {
    auto p1 = positions[tr[0]];
    auto p2 = positions[tr[1]];
    auto p3 = positions[tr[2]];
    // If the normal is not toward outside the sphere
    if(((p2-p1) ^ (p3-p1)) * (p1+p2+p3) < 0) {
      swap(tr[1], tr[2]);
    }
  }
  progress.advance(103);
}

namespace {
struct pair_hash {
  int operator()(const std::pair<int, int>& p) const {
    return p.first ^ p.second;
  }
};
}

RegularSphere::RegularSphere(size_t nb_subdivisions)
  : AbstractSphere()
{
  positions.resize(12);
  triangles.resize(20);
  const double t = (1 + sqrt(5.)) / 2;
  const double s = sqrt(1 + t * t);
  positions[0] = Point3f(t, 1, 0) / s;
  positions[1] = Point3f(-t, 1, 0) / s;
  positions[2] = Point3f(t, -1, 0) / s;
  positions[3] = Point3f(-t, -1, 0) / s;
  positions[4] = Point3f(1, 0, t) / s;
  positions[5] = Point3f(1, 0, -t) / s;
  positions[6] = Point3f(-1, 0, t) / s;
  positions[7] = Point3f(-1, 0, -t) / s;
  positions[8] = Point3f(0, t, 1) / s;
  positions[9] = Point3f(0, -t, 1) / s;
  positions[10] = Point3f(0, t, -1) / s;
  positions[11] = Point3f(0, -t, -1) / s;

  triangles[0] = Point3i(0, 8, 4);
  triangles[1] = Point3i(1, 10, 7);
  triangles[2] = Point3i(2, 9, 11);
  triangles[3] = Point3i(7, 3, 1);
  triangles[4] = Point3i(0, 5, 10);
  triangles[5] = Point3i(3, 9, 6);
  triangles[6] = Point3i(3, 11, 9);
  triangles[7] = Point3i(8, 6, 4);
  triangles[8] = Point3i(2, 4, 9);
  triangles[9] = Point3i(3, 7, 11);
  triangles[10] = Point3i(4, 2, 0);
  triangles[11] = Point3i(9, 4, 6);
  triangles[12] = Point3i(2, 11, 5);
  triangles[13] = Point3i(0, 10, 8);
  triangles[14] = Point3i(5, 0, 2);
  triangles[15] = Point3i(10, 5, 7);
  triangles[16] = Point3i(1, 6, 8);
  triangles[17] = Point3i(1, 8, 10);
  triangles[18] = Point3i(6, 1, 3);
  triangles[19] = Point3i(11, 7, 5);

#define MAKE_EDGE(i1, i2) (i1 < i2 ? std::make_pair(i1, i2) : std::make_pair(i2, i1))

  // Subdivide triangles
  for(size_t i = 0; i < nb_subdivisions; ++i) {
    typedef std::unordered_map<std::pair<int, int>, int, pair_hash> edge_map_t;
    typedef edge_map_t::iterator edge_iterator;
    edge_map_t edges;
    std::vector<Point3i> new_triangles(4 * triangles.size());
    for(size_t j = 0; j < triangles.size(); ++j) {
      const Point3i tr = triangles[j];
      int p1 = tr[0], p2 = tr[1], p3 = tr[2];
      std::pair<int, int> e1 = MAKE_EDGE(p1, p2);
      std::pair<int, int> e2 = MAKE_EDGE(p2, p3);
      std::pair<int, int> e3 = MAKE_EDGE(p1, p3);
      int n1, n2, n3;
      edge_iterator found = edges.find(e1);
      if(found == edges.end()) {
        n1 = positions.size();
        positions.push_back(normalized(positions[p1] + positions[p2]));
        edges[e1] = n1;
      } else
        n1 = found->second;
      found = edges.find(e2);
      if(found == edges.end()) {
        n2 = positions.size();
        positions.push_back(normalized(positions[p2] + positions[p3]));
        edges[e2] = n2;
      } else
        n2 = found->second;
      found = edges.find(e3);
      if(found == edges.end()) {
        n3 = positions.size();
        positions.push_back(normalized(positions[p3] + positions[p1]));
        edges[e3] = n3;
      } else
        n3 = found->second;
      new_triangles[4 * j] = Point3i(p1, n1, n3);
      new_triangles[4 * j + 1] = Point3i(p2, n2, n1);
      new_triangles[4 * j + 2] = Point3i(p3, n3, n2);
      new_triangles[4 * j + 3] = Point3i(n1, n2, n3);
    }
    swap(triangles, new_triangles);
  }
#undef MAKE_EDGE

  _valid = true;
}

Cube::Cube()
{
  _valid = true;
}

bool Cube::addTo(Mesh* mesh,
                 const Point3f& center,
                 const Point3f& rx, const Point3f& ry, const Point3f& rz,
                 int label)
{
  // Information::out << "Draw PCA for label " << lab << endl;
  vvgraph& S = mesh->graph();

  std::vector<vertex> vs(8, vertex(0));

  for(size_t i = 0 ; i < 8 ; ++i) {
    vertex v;
    v->label = label;
    vs[i] = v;
  }

  vs[0]->pos = center - rx - ry - rz;
  vs[1]->pos = center + rx - ry - rz;
  vs[2]->pos = center - rx + ry - rz;
  vs[3]->pos = center + rx + ry - rz;
  vs[4]->pos = center - rx - ry + rz;
  vs[5]->pos = center + rx - ry + rz;
  vs[6]->pos = center - rx + ry + rz;
  vs[7]->pos = center + rx + ry + rz;

  std::vector<Point3i> triangles(12);
  triangles[0] = Point3i(0, 1, 4);   // 1
  triangles[1] = Point3i(1, 5, 4);

  triangles[2] = Point3i(1, 3, 5);   // 2
  triangles[3] = Point3i(3, 7, 5);

  triangles[4] = Point3i(3, 2, 7);   // 3
  triangles[5] = Point3i(2, 6, 7);

  triangles[6] = Point3i(2, 0, 6);   // 4
  triangles[7] = Point3i(0, 4, 6);

  triangles[8] = Point3i(2, 3, 0);   // 5
  triangles[9] = Point3i(3, 1, 0);

  triangles[10] = Point3i(4, 5, 6);   // 6
  triangles[11] = Point3i(5, 7, 6);

  // Information::out << "Calling meshFromTriangles" << endl;
  if(not shape::meshFromTriangles(S, vs, triangles)) {
    _errorString = "Failed to add the triangles from the cube ...";
    return false;
  }
  return true;
}

Cylinder::Cylinder(size_t nb_slices)
  : _slices(nb_slices)
{
  _valid = _slices > 0;
}

bool Cylinder::addTo(Mesh* mesh,
                     const Point3f& center,
                     const Point3f& rx, const Point3f& ry, const Point3f& rz,
                     int label)
{
  vvgraph& S = mesh->graph();
  std::vector<vertex> vs(5 * _slices + 2, vertex(0));

  vertex c1, c2;
  c1->label = c2->label = label;
  c1->pos = center - rx;
  c2->pos = center + rx;
  int nc1 = 5 * _slices;
  int nc2 = 5 * _slices + 1;
  vs[nc1] = c1;
  vs[nc2] = c2;

  float alpha = 2 * M_PI / _slices;
  for(int i = 0; i < _slices; ++i) {
    vertex v1, v2, v3, v4, v5;
    v1->label = v2->label = v3->label = v4->label = v5->label = label;

    Point3f v1p = Point3f(-1, cos(i * alpha), sin(i * alpha));
    v3->pos = v1->pos = center + v1p.x() * rx + v1p.y() * ry + v1p.z() * rz;
    Point3f v2p = Point3f(1, cos(i * alpha), sin(i * alpha));
    v4->pos = v2->pos = center + v2p.x() * rx + v2p.y() * ry + v2p.z() * rz;

    v5->pos = center + v2p.y() * ry + v2p.z() * rz;

    vs[i] = v1;
    vs[_slices + i] = v2;
    vs[2 * _slices + i] = v3;
    vs[3 * _slices + i] = v4;
    vs[4 * _slices + i] = v5;
  }

  std::vector<Point3i> triangles(6 * _slices);
  int i = _slices - 1;
  for(int i1 = 0; i1 < _slices; ++i1) {
    int j = _slices + i;
    int j1 = _slices + i1;
    int k = 2 * _slices + i;
    int k1 = 2 * _slices + i1;
    triangles[i] = Point3i(nc1, i1, i);
    triangles[i + _slices] = Point3i(nc2, j, j1);
    triangles[i + 2 * _slices] = int(2 * _slices) + Point3i(i, i1, k);
    triangles[i + 3 * _slices] = int(2 * _slices) + Point3i(i1, k1, k);
    triangles[i + 4 * _slices] = int(2 * _slices) + Point3i(k, k1, j);
    triangles[i + 5 * _slices] = int(2 * _slices) + Point3i(k1, j1, j);
    i = i1;
  }

  if(not shape::meshFromTriangles(S, vs, triangles)) {
    _errorString = "Failed to add the triangles from the cylinder ...";
    return false;
  }
  return true;
}

} // namespace shape
} // namespace lgx
