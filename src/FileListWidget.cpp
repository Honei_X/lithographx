/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "FileListWidget.hpp"

#include <QDropEvent>
#include <QDragEnterEvent>
#include <QDragMoveEvent>
#include <QDragLeaveEvent>
#include <QMimeData>
#include <QUrl>
#include <QFileInfo>

#include "Information.hpp"

namespace lgx {
namespace gui {
FileListWidget::FileListWidget(QWidget* parent)
  : QListWidget(parent)
  , handlingDrop(false)
{
  setAcceptDrops(true);
}

void FileListWidget::dropEvent(QDropEvent* event)
{
  handlingDrop = false;
  const QMimeData* data = event->mimeData();
  if(data->hasUrls()) {
    QStringList result;
    QList<QUrl> urls = data->urls();
    foreach(QUrl url, urls) {
      QString file = url.toLocalFile();
      result << file;
    }
    if(!result.empty()) {
      event->acceptProposedAction();
      emit filesDropped(result);
      return;
    }
  }
  QListWidget::dropEvent(event);
}

void FileListWidget::dragEnterEvent(QDragEnterEvent* event)
{
  const QMimeData* data = event->mimeData();
  if(data->hasUrls()) {
    bool accepted = true;
    QList<QUrl> urls = data->urls();
    foreach(QUrl url, urls) {
      QString file = url.toLocalFile();
      if(file.isEmpty()) {
        accepted = false;
        break;
      }
      QFileInfo fi(file);
      if(!fi.isFile() or !fi.isReadable()) {
        accepted = false;
        break;
      }
    }
    if(accepted and (event->proposedAction() == Qt::CopyAction)) {
      handlingDrop = true;
      event->accept();
      event->acceptProposedAction();
      return;
    }
  }
  handlingDrop = false;
  QListWidget::dragEnterEvent(event);
}

void FileListWidget::dragMoveEvent(QDragMoveEvent* event)
{
  if(!handlingDrop)
    QListWidget::dragMoveEvent(event);
  else {
    event->acceptProposedAction();
  }
}

void FileListWidget::dragLeaveEvent(QDragLeaveEvent*)
{
  if(handlingDrop) {
    handlingDrop = false;
  }
}
} // namespace gui
} // namespace lgx
