/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef VERSION_H
#define VERSION_H

#include <LGXConfig.hpp>

#include <QString>

#define VERSION_MAJOR ${LGX_VERSION_MAJOR}
#define VERSION_MINOR ${LGX_VERSION_MINOR}
#define VERSION_PATCH ${LGX_VERSION_PATCH}

#define VERSION "${LGX_VERSION}"
#define REVISION "${LGX_REVISION}"

#define LGX_OS "${LGX_OS}${LGX_OS_RELEASE}${LGX_CUDA}"

#ifdef major
#  undef major
#endif

#ifdef minor
#  undef minor
#endif

struct LGX_EXPORT LGXVersion
{
  LGXVersion() { }
  LGXVersion(int major, int minor, int patch);
  explicit LGXVersion(const QString& version);
  LGXVersion(const LGXVersion&) = default;

  LGXVersion& operator=(const LGXVersion&) = default;

  QString toString() const;

  int major = -1;
  int minor = -1;
  int patch = -1;

  bool valid() const;
  explicit operator bool() const;
};

LGX_EXPORT bool operator==(const LGXVersion& v1, const LGXVersion& v2);
LGX_EXPORT bool operator!=(const LGXVersion& v1, const LGXVersion& v2);
LGX_EXPORT bool operator>(const LGXVersion& v1, const LGXVersion& v2);
LGX_EXPORT bool operator<(const LGXVersion& v1, const LGXVersion& v2);
LGX_EXPORT bool operator>=(const LGXVersion& v1, const LGXVersion& v2);
LGX_EXPORT bool operator<=(const LGXVersion& v1, const LGXVersion& v2);

extern LGX_EXPORT LGXVersion lgxVersion;

#endif // VERSION_H


