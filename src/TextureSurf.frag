uniform sampler2D tex;
uniform float opacity;
uniform float brightness;
uniform bool blending;

varying vec2 texCoord;
/*
 *varying vec3 normal;
 *varying vec3 lightDir[4], halfVector[4];
 */

void setColor()
{
  vec4 color = texture2D(tex, texCoord);

/*
 *  vec3 n = normalize(normal);
 *
 *  vec4 light = vec4(0,0,0,0);
 *
 *  for(int i = 0 ; i < 4 ; ++i)
 *  {
 *    vec4 diffuseLight, specularLight;	
 *    float shine = 32.0;
 *
 *    float lightIntensity = max(dot(n, normalize(lightDir[i])), 0.0);
 *
 *    diffuseLight = lightIntensity * gl_LightSource[0].diffuse;
 *    specularLight = pow(max(dot(n, normalize(halfVector[i])), 0.0), shine) * gl_LightSource[i].specular;
 *
 *    light += (diffuseLight + specularLight + gl_LightSource[i].ambient);
 *  }
 */
  color = light(color);
  gl_FragColor = brightness*color;//*light;
  if(blending)
    gl_FragColor.a *= opacity;
  else
    gl_FragColor.a = 1.0;
}

