/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Mesh.hpp"
#include "ImageData.hpp"
#include "Stack.hpp"
#include "Information.hpp"
#include "Thrust.hpp"

#include <algorithm>
#include <iterator>
#include <QFileInfo>
#include "Dir.hpp"

namespace lgx {

Mesh::Mesh(const Stack* stack)
  : _id(-1)
  , _stack(stack)
{
  init();
}

Mesh::Mesh(int id, const Stack* stack)
  : _id(id)
  , _stack(stack)
{
  init();
}

void Mesh::setStack(const Stack* s) {
  _stack = s;
}

void Mesh::init()
{
  changed_surf_function = false;
  changed_heat_function = false;
  changed_label = false;
  _changed = 0;
  _show = NORMAL;
  _color = SIGNAL;
  _culling = true;
  _blending = true;
  _isSurfaceVisible = false;
  _isMeshVisible = false;
  _currLabel = 0;
  _opacity = 1.0f;
  _brightness = 1.0f;
  _meshView = SELECTED_MESH;
  _showMeshLines = false;
  _showMeshPoints = false;
  _showMeshCellMap = false;
  _cells = false;
  _imgtex = false;
  _scaled = false;
  _transformed = false;
  _showBBox = false;
  _cellAxisOffset = 0;
  _cellAxisParents = false;
  _cellAxisWidth = 1;
  _attached.clear();
  _heatMapUnit.clear();
  _signalUnit.clear();
  _signalBounds = Point2f(0, 1);
  _heatMapBounds = Point2f(0, 1);
  _heatMapDesc.clear();
  _signalDesc.clear();
}

Mesh::~Mesh() {
}

void Mesh::clearHeatmap()
{
  _labelHeat.clear();
  _wallHeat.clear();
  _heatMapUnit.clear();
  _heatMapDesc.clear();
  _heatMapBounds = Point2f(0.f, 1.f);
}

void Mesh::updateAll() {
  _changed = ImgData::RELOAD_VBO;
}

void Mesh::updateTriangles() {
  _changed |= ImgData::RELOAD_TRIS;
}

void Mesh::updateLines() {
  _changed |= ImgData::RELOAD_LINES;
}

void Mesh::updatePositions() {
  _changed |= ImgData::RELOAD_POS;
}

void Mesh::updateSelection() {
  _changed |= ImgData::UPDATE_SELECTION;
}

void Mesh::updateWallGeometry(float borderSize)
{
  // First, mark the borders
  markBorder(borderSize);

  _labelNeighbors.clear();
  _wallVId.clear();
  _wallGeom.clear();

  forall(const vertex& v, S) {
    if(v->label != -1)
      continue;
    forall(const vertex& n, S.neighbors(v)) {
      if(n->label != -1 or n > v)
        continue;
      IntSet Sv, Sn;
      // Get list of neighbors for each vertex.
      forall(const vertex& s, S.neighbors(v))
        if(s->label != -1)
          Sv.insert(s->label);
      forall(const vertex& s, S.neighbors(n))
        if(s->label != -1)
          Sn.insert(s->label);
      IntVec labelV;
      forall(int lab, Sv)
        if(Sn.find(lab) != Sn.end())
          labelV.push_back(lab);
      if(labelV.size() <= 1)
        continue;
      // Find wall length, add to neighbors array, and add vertices in wall to set
      for(size_t i = 0; i < labelV.size() - 1; i++) {
        IntIntPair wall;

        wall = IntIntPair(labelV[i], labelV[i + 1]);
        if(wall.first > 0) {
          _wallGeom[wall] += (v->pos - n->pos).norm();
          _labelNeighbors[wall.first].insert(wall.second);
          _wallVId[wall].insert(v.id());
          _wallVId[wall].insert(n.id());
        }

        wall = IntIntPair(labelV[i + 1], labelV[i]);
        if(wall.first > 0) {
          _wallGeom[wall] += (v->pos - n->pos).norm();
          _labelNeighbors[wall.first].insert(wall.second);
          _wallVId[wall].insert(v.id());
          _wallVId[wall].insert(n.id());
        }
      }
    }
  }
}

int Mesh::getLabel(vertex v1, vertex v2, vertex v3, bool useParentLabel) const
{
  if(!v1->label or !v2->label or !v3->label)
    return 0;
  if(v1->label < 0 and v2->label < 0 and v3->label < 0)
    return -1;

  int labcnt = 0, label = 0;
  if(v1->label > 0) {
    labcnt++;
    label = v1->label;
  }
  if(v2->label > 0) {
    if(v2->label != label) {
      labcnt++;
      label = v2->label;
    }
  }
  if(v3->label > 0) {
    if(v3->label != label) {
      labcnt++;
      label = v3->label;
    }
  }
  if(labcnt != 1)
    return 0;

  if(useParentLabel and toShow() == Mesh::PARENT) {
    IntIntMap::const_iterator i = parentLabelMap().find(label);
    if(i == parentLabelMap().end())
      return 0;
    else
      return i->second;
  }
  return label;
}

bool Mesh::isBordTriangle(int label, vertex v, vertex n, vertex m, IntIntPair& wall) const
{
  if(_wallVId.empty() or v->minb == 0 or n->minb == 0 or m->minb == 0)
    return (false);

  // Search through neighbor list and sum signal and area
  IntIntSetMap::const_iterator found = _labelNeighbors.find(label);
  if(found == _labelNeighbors.end())
    return false;
  forall(int lab, found->second) {
    IntIntPair p(label, lab);
    IntIntVIdSetMap::const_iterator found = _wallVId.find(p);
    if(found == _wallVId.end())
      continue;

    const VIdSet& VtxSet = found->second;

    // If triangle belongs to this side of wall
    int count = 0;
    if(VtxSet.find(v->minb) != VtxSet.end())
      count++;
    if(VtxSet.find(n->minb) != VtxSet.end())
      count++;
    if(VtxSet.find(m->minb) != VtxSet.end())
      count++;

    if(count >= 2) {
      wall = p;
      return true;
    }
  }
  return false;
}

void Mesh::markBorder(float borderSize)
{
  if(S.empty())
    return;

  // For cellular mesh we will mark all vertices
  if(cells()) {
    int bcount = 0;
    forall(const vertex& v, S)
      if(v->type == 'j') {
        v->minb = v.id();
        bcount++;
      } else
        v->minb = S.anyIn(v).id();
  } else {
    // Process non cellular mesh, first all border vertices in R
    std::set<vertex> R;
    forall(const vertex& v, S)
      if(v->label == -1) {
        R.insert(v);
        v->minb = v.id();
      } else
        v->minb = 0;
    // uint bsize = R.size();

    // Start with border vertices and propogate
    std::set<vertex> T = R;     // Temp list
    std::set<vertex> D;         // delete list
    std::set<vertex> I;         // insert list

    uint rsize;
    do {
      rsize = R.size();
      forall(const vertex& v, T) {
        // If a vertex doesn't add any neighbors, we'll assume it is interior
        bool del = true;
        forall(const vertex& n, S.neighbors(v)) {
          // If not in border front
          if(R.find(n) == R.end()) {
            vertex w = vertex(v->minb);
            if(w.isNull()) {
              SETSTATUS("markBorder::Error:Null vertex w");
              continue;
            }
            vertex minb = w;
            double min = (n->pos - w->pos).norm();
            // forall(const vertex &m, S.neighbors(w))
            forall(const vertex& m, S.neighbors(n)) {
              if(m->minb != 0) {
                vertex mb(m->minb);
                if(mb.isNull()) {
                  SETSTATUS("markBorder::Error:Null vertex mb");
                  continue;
                }
                double dis = (n->pos - mb->pos).norm();
                if(dis < min) {
                  min = dis;
                  minb = mb;
                }
              }
            }
            if(min <= borderSize) {
              n->minb = minb.id();
              R.insert(n);
              I.insert(n);
            }
            del = false;
          }
        }
        if(del)
          D.insert(v);
      }
      forall(const vertex& v, I)
        T.insert(v);
      forall(const vertex& v, D)
        T.erase(v);
      I.clear();
      D.clear();
    } while(rsize < R.size());     // Do till R stops growing
  }
}

// Set normal
bool Mesh::setNormal(vvgraph& S, vertex v)
{
  //  RSS: Changed to use the same algorithm as in ImageData.
  //
  v->nrml = Point3f(0, 0, 0);
  forall(const vertex& n, S.neighbors(v)) {
    vertex m = S.nextTo(v, n);
    // add to normal
    if(n != m and S.edge(n, m)) {
      Point3f vn = n->pos - v->pos;
      Point3f vm = m->pos - v->pos;
      if(S.nextTo(n, m) != v)
        v->nrml -= vn.cross(vm);
      else
        v->nrml += vn.cross(vm);
    }
  }
  // Check for 0 normals
  if(v->nrml.norm() == 0) {
    v->nrml = Point3f(0, 0, 0);
    return false;
  }
  v->nrml.normalize();

  // Check for nan
  if(fabs(1.0 - v->nrml.norm()) < 1e-4)
    ;
  else {
    v->nrml = Point3f(0, 0, 0);
    return false;
  }
  return true;
}

// Set normals
bool Mesh::setNormals()
{
  if(S.empty())
    return true;

  size_t nb_invalid = 0;

  forall(const vertex& v, S)
    if(!setNormal(S, v))
      nb_invalid++;

  if(nb_invalid > 0)
    SETSTATUS("Mesh::setNormals -- " << nb_invalid << " vertices with invalid normals");
  return nb_invalid == 0;
}

void Mesh::reset()
{
  S.clear();
  _cellAxis.clear();
  _cellAxisScaled.clear();
  _VVCorrespondance.clear();

  _labelHeat.clear();
  _wallHeat.clear();
  _labelCenter.clear();
  _labelCenterVis.clear();
  _parentCenter.clear();
  _parentCenterVis.clear();
  _labelNormal.clear();
  _labelNormalVis.clear();
  _parentNormal.clear();
  _parentNormalVis.clear();
  _labelNeighbors.clear();
  _wallVId.clear();
  _wallGeom.clear();
  _parentLabel.clear();

  deleteAllAttachments();

  setFile();
  clearImgTex();
  setScaled(true);
  setTransformed(false);
  setCells(false);
}

void Mesh::setFile(const QString& file)
{
  if(file.isEmpty()) {
    _file = file;
  } else {
    _file = util::absoluteFilePath(file);
  }
}

// Get set of vertices that are connected
void Mesh::getConnectedVertices(vvgraph& V) const
{
  using std::swap;
  vvgraph vNew(V);
  do {
    vvgraph N;
    forall(const vertex& v, vNew)
      forall(const vertex& n, S.neighbors(v))
        if(not V.contains(n)) {
          V.insert(n);
          N.insert(n);
        }
    swap(vNew, N);
    N.clear();
  } while(!vNew.empty());
}

// Get list of connected regions. Most often used to find 3D cells.
// cellV is a vector of vvgraphs representing all the regions (cells)
// vCell maps vertices to region (cell) index numbers
int Mesh::getConnectedRegions(const vvgraph& S, VVGraphVec& cellV, VIntMap& vCell) const
{
  int number = 0;
  VtxSet T(S.begin(), S.end());
  while(!T.empty()) {
    vvgraph V;
    V.insert(*T.begin());
    getConnectedVertices(V);
    forall(const vertex& v, V) {
      vCell[v] = number;
      T.erase(v);
    }
    cellV.push_back(V);
    number++;
  }
  // Return number of cells
  return number;
}

// Get the neighborhood of a vertex, Euclidean distance, must be connected
VtxSet Mesh::getNeighborhood(const vvgraph& S, const vertex& v, float radius) // static
{
  float radius_sq = radius * radius;
  VtxSet neighbors;
  std::vector<vertex> vlast;
  neighbors.clear();
  neighbors.insert(v);
  vlast.push_back(v);
  Point3f pos = v->pos;
  while(not vlast.empty()) {
    std::vector<vertex> vnew;
    forall(const vertex& w, vlast)
      forall(const vertex& n, S.neighbors(w))
        if(neighbors.find(n) == neighbors.end() and normsq(n->pos - pos) < radius_sq) {
          vnew.push_back(n);
          neighbors.insert(n);
        }
    std::swap(vlast, vnew);
  }
  return neighbors;
}

std::vector<vertex> Mesh::selectedVertices() const
{
  std::vector<vertex> vs;
  if(isMeshVisible() and (showMeshLines() or showMeshPoints())) {
    forall(const vertex& v, S) {
      if(v->selected)
        vs.push_back(v);
    }
  }
  return vs;
}

std::vector<vertex> Mesh::activeVertices() const
{
  std::vector<vertex> vs;
  vs.reserve(S.size());
  if(isMeshVisible() and (showMeshLines() or showMeshPoints())) {
    forall(const vertex& v, S) {
      if(v->selected)
        vs.push_back(v);
    }
  }
  if(vs.empty())
    std::copy(S.begin(), S.end(), std::back_inserter(vs));
  return vs;
}

struct CorrectSelectionKernel {
  CorrectSelectionKernel(bool inc, const vvgraph& SS, HVecUB& r)
    : inclusive(inc)
    , S(SS)
    , result(r)
  {
  }

  void operator()(size_t i)
  {
    const vertex& v = S[i];
    if(v->label < 0 and v->type != 'l') {
      bool has_non_border = false;
      bool has_nb_selected = false;
      bool has_nb_non_selected = false;
      bool has_border_selected = false;
      bool has_nb_labeled = false;
      forall(const vertex& n, S.neighbors(v)) {
        if(n->label >= 0) {
          has_non_border = true;
          if(n->label > 0)
            has_nb_labeled = true;
          if(n->selected)
            has_nb_selected = true;
          else
            has_nb_non_selected = true;
        } else if(n->selected)
          has_border_selected = true;
      }
      if(inclusive and has_nb_selected)
        result[i] = 1;
      else if(has_non_border and not has_nb_non_selected)
        result[i] = 1;
      else if(!has_non_border and has_border_selected)
        result[i] = 1;
      else if(v->selected and !has_nb_labeled)
        result[i] = 1;
      else
        result[i] = 0;
    } else
      result[i] = v->selected;
  }

  bool inclusive;
  const vvgraph& S;
  HVecUB& result;
};

struct CopyResultToSelection {
  CopyResultToSelection(vvgraph& SS, const HVecUB& r)
    : S(SS)
    , result(r)
  {
  }

  void operator()(size_t i) {
    S[i]->selected = (result[i] != 0);
  }

  vvgraph& S;
  const HVecUB& result;
};

void Mesh::correctSelection(bool inclusive)
{
  if(S.empty() or toShow() == NORMAL)
    return;
  HVecUB result(S.size());
  {
    CorrectSelectionKernel kernel(inclusive, S, result);
#pragma omp parallel for
    for(size_t i = 0; i < S.size(); ++i)
      kernel(i);
  }

  {
    CopyResultToSelection kernel(S, result);
#pragma omp parallel for
    for(size_t i = 0; i < S.size(); ++i)
      kernel(i);
  }
}

void Mesh::resetModified()
{
  changed_surf_function = false;
  changed_heat_function = false;
  changed_label = false;
  _changed = 0;
}

void Mesh::updateBBox()
{
  _bbox = BoundingBox3f(Point3f(FLT_MAX, FLT_MAX, FLT_MAX), Point3f(-FLT_MAX, -FLT_MAX, -FLT_MAX));
  forall(const vertex& v, S)
    _bbox |= v->pos;
}

// Update the mesh labelCenter and labelNormal, to display the cell axis on mesh surface.
// if parent labels are selected, the centers and normals are computed based on parents
// if mesh is cellular, simply take the average positions and normals
void Mesh::updateCentersNormals()
{
  vvgraph S = graph();

  _labelCenter.clear();
  _labelNormal.clear();
  _labelCenterVis.clear();
  _labelNormalVis.clear();

  _parentCenter.clear();
  _parentNormal.clear();
  _parentCenterVis.clear();
  _parentNormalVis.clear();

  std::map<int, std::set<vertex> > vertices;

  // Compute label centers and normals
  //
  // If the mesh is simplified, then the center is set to vertex of type 'c', whose normal
  // and position should represent the one of the whole cell
  if(_cells) {
    forall(const vertex& v, S) {
      if(v->type == 'c') {
        int label = v->label;
        _labelCenter[label] = v->pos;
        _labelNormal[label] = v->nrml;
        _labelCenterVis[label] = v->pos;
        _labelNormalVis[label] = v->nrml;
      }
    }
  } else   // Otherwise, we need to compute it
  {
    IntFloatMap areas;
    // Find cell centers, areas and normals.
    forall(const vertex& v, S) {
      forall(const vertex& n, S.neighbors(v)) {
        const vertex& m = S.nextTo(v, n);
        if(!uniqueTri(v, n, m))
          continue;
        int label = getLabel(v, n, m, false);
        if(label == 0)
          continue;
        vertices[label].insert(v);
        vertices[label].insert(n);
        vertices[label].insert(m);
        float area = triangleArea(v->pos, n->pos, m->pos);
        if(area > 0) {
          areas[label] += area;
          _labelCenter[label] += (v->pos + n->pos + m->pos) / 3.0 * area;
          _labelNormal[label] += (v->nrml + n->nrml + m->nrml) * area;
        }
      }
    }

    forall(const IntFloatPair& p, areas) {
      int label = p.first;
      float area = p.second;
      _labelCenter[label] /= area;
      _labelNormal[label].normalize();
      Point3f center = _labelCenter[label];
      // find closest point on surface and set it to the center
      vertex minv = *(vertices[label].begin());
      float mindis = norm(minv->pos - center);
      forall(const vertex& v, vertices[label]) {
        float dis = norm(v->pos - center);
        if(dis < mindis) {
          mindis = dis;
          minv = v;
        }
      }
      _labelCenterVis[label] = minv->pos;
      _labelNormalVis[label] = minv->nrml;
    }
  }

  // If cellAxis uses the parent labels, compute parent centers and normals
  if(this->isParentAxis()) {
    IntIntMap nDaughters;
    std::map<int, std::set<vertex> > verticesParents;

    for(IntIntMap::iterator it = _parentLabel.begin(); it != _parentLabel.end(); ++it) {
      int lDaughter = it->first;
      int lParent = it->second;
      if(lParent == 0 or lDaughter == 0 or _labelCenter.count(lDaughter) == 0)
        continue;

      nDaughters[lParent]++;
      _parentCenter[lParent] += _labelCenter[lDaughter];
      _parentNormal[lParent] += _labelNormal[lDaughter];
      if(!_cells) {
        std::set<vertex> vDaughter = vertices[lDaughter];
        verticesParents[lParent].insert(vDaughter.begin(), vDaughter.end());
      }
    }

    forall(const IntPoint3fPair& lc, _parentCenter) {
      int labelP = lc.first;
      _parentCenter[labelP] /= nDaughters[labelP];
      _parentNormal[labelP] /= nDaughters[labelP];
      Point3f center = _parentCenter[labelP];
      if(!_cells) {
        // find closest point on surface and set it to the center
        float mindis = HUGE_VAL;
        // vertex minv;
        vertex minv = *(verticesParents[labelP].begin());
        forall(const vertex& v, verticesParents[labelP]) {
          float dis = norm(v->pos - center);
          if(dis < mindis) {
            mindis = dis;
            minv = v;
          }
        }
        _parentCenterVis[labelP] = minv->pos;
        _parentNormalVis[labelP] = minv->nrml;
      } else {
        _parentCenterVis[labelP] = _parentCenter[labelP];
        _parentNormalVis[labelP] = _parentNormal[labelP];
      }
    }
  }
}

// Calculate scaled values of cell axis for display
void Mesh::prepareAxisDrawing(float scaleAxisLength, bool (*predicate)(float), Colorf colorTrue, Colorf colorFalse,
                              const Point3b& showAxis)
{
  // Scale the cell axis for display
  _cellAxisScaled.clear();
  _cellAxisColor.clear();

  forall(const IntSymTensorPair& p, _cellAxis) {
    int cell = p.first;
    const util::SymmetricTensor& tensor = p.second;

    Point3f sev1 = (showAxis[0] ? (tensor.evals()[0] * tensor.ev1() * scaleAxisLength) : Point3f());
    Point3f sev2 = (showAxis[1] ? (tensor.evals()[1] * tensor.ev2() * scaleAxisLength) : Point3f());
    Point3f sev3 = (showAxis[2] ? (tensor.evals()[2] * tensor.ev3() * scaleAxisLength) : Point3f());

    _cellAxisScaled[cell][0] = sev1;
    _cellAxisScaled[cell][1] = sev2;
    _cellAxisScaled[cell][2] = sev3;

    for(size_t i = 0; i < 3; ++i) {
      if(predicate) {
        if(predicate(tensor.evals()[i]))
          _cellAxisColor[cell][i] = Point3f(colorTrue);
        else
          _cellAxisColor[cell][i] = Point3f(colorFalse);
      } else
        _cellAxisColor[cell][i] = Point3f(colorTrue);
    }
  }
}

void Mesh::clearCellAxis()
{
  _cellAxis.clear();
  _cellAxisScaled.clear();
  _cellAxisColor.clear();
  _cellAxisDesc.clear();
}

void Mesh::finalizeTSignal()
{
  forall(const vertex& v, S)
    v->signal = v->tsignal;
}

void Mesh::_attachObject(QString name, AttachedData* data)
{
  QHash<QString, AttachedData*>::iterator found = _attached.find(name);
  if(found != _attached.end()) {
    found.value()->deletePointer();
    _attached.erase(found);
  }
  _attached[name] = data;
}

bool Mesh::deleteAttached(QString name)
{
  AttachedData* data = _attached.value(name, 0);
  if(data) {
    data->deletePointer();
    _attached.remove(name);
    return true;
  }
  return false;
}

void Mesh::deleteAllAttachments()
{
  typedef QHash<QString, AttachedData*>::iterator iterator;
  for(iterator it = _attached.begin(); it != _attached.end(); ++it) {
    it.value()->deletePointer();
  }
  _attached.clear();
}

bool isPositive(float a) {
  return a >= 0;
}
} // namespace lgx
