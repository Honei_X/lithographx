uniform int nb_colors;
uniform sampler1D labelcolormap;

uniform sampler3D tex;
uniform sampler1D colormap;
uniform float brightness;
uniform float opacity;

uniform sampler3D second_tex;
uniform sampler1D second_colormap;
uniform float second_brightness;
uniform float second_opacity;

vec4 base_index_color(float value)
{
  if(value == 0) return vec4(0,0,0,0);
  value = value * 256.0;
  float low = floor(value + 0.5);
  float high = floor((value - low) * 256 + 1./512) * 256;
  float real_value = low + high;
  float col_idx = (real_value+0.5)/float(nb_colors);
  vec4 col = texture1D(labelcolormap, col_idx);
  return col;
}

vec4 index_color1(vec3 texCoord)
{
  float value = texture3D(tex, texCoord).a;
  vec4 col = base_index_color(value);
  col.rgb *= brightness;
  col.a *= opacity;
  return col;
}

vec4 index_color2(vec3 texCoord)
{
  float value = texture3D(second_tex, texCoord).a;
  vec4 col = base_index_color(value);
  col.rgb *= second_brightness;
  col.a *= second_opacity;
  return col;
}

vec4 colormap_color1(vec3 texCoord)
{
  float value = texture3D(tex, texCoord).a;
  vec4 col = texture1D(colormap, value);
  col.rgb *= brightness;
  col.a *= opacity;
  return col;
}

vec4 colormap_color2(vec3 texCoord)
{
  float value = texture3D(second_tex, texCoord).a;
  vec4 col = texture1D(second_colormap, value);
  col.rgb *= second_brightness;
  col.a *= second_opacity;
  return col;
}

