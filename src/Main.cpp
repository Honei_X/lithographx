/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "LithoGraphX.hpp"
#include <QApplication>
#include <QString>
#include <QGLFormat>
#include <QDir>
#include <QFileInfo>
#include <QMetaType>
#include <QTimer>
#include "Information.hpp"
#include <QMessageBox>
#include <QStringBuilder>
#include "Dir.hpp"
#include "Thrust.hpp"
#include "LGXVersion.hpp"
#include "Random.hpp"

using std::cin;
using lgx::Information::out;

#if defined(WIN32) || defined(WIN64)
#  include <Winreg.h>

LONG associateFile(HKEY root, QByteArray ext, QByteArray name, QByteArray descr, int icon_num)
{
  LONG error;
  HKEY ext_handle, file_handle;
  // Create extension key
  error = RegCreateKeyEx(root, ext.data(), 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &ext_handle, 0);
  if(error != ERROR_SUCCESS)
    return error;
  error = RegSetValueEx(ext_handle, 0, 0, REG_SZ, (BYTE*)name.data(), name.size() + 1);
  if(error != ERROR_SUCCESS)
    return error;
  // Create file type key
  error = RegCreateKeyEx(root, name.data(), 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &file_handle, 0);
  if(error != ERROR_SUCCESS)
    return error;
  error = RegSetValueEx(file_handle, 0, 0, REG_SZ, (BYTE*)descr.data(), descr.size() + 1);
  if(error != ERROR_SUCCESS)
    return error;
  // Set default icon
  HKEY icon;
  error
    = RegCreateKeyEx(file_handle, "DefaultIcon", 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &icon, 0);
  if(error != ERROR_SUCCESS)
    return error;
  QString icon_path = QString("%1,%2").arg(QCoreApplication::applicationFilePath()).arg(icon_num).replace("/", "\\");
  QByteArray array = icon_path.toLatin1();
  error = RegSetValueEx(icon, 0, 0, REG_SZ, (BYTE*)array.data(), array.size() + 1);
  if(error != ERROR_SUCCESS)
    return error;
  // Set program association
  HKEY shell, open, command;
  error = RegCreateKeyEx(file_handle, "shell", 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &shell, 0);
  if(error != ERROR_SUCCESS)
    return error;
  error = RegCreateKeyEx(shell, "Open", 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &open, 0);
  if(error != ERROR_SUCCESS)
    return error;
  error = RegCreateKeyEx(open, "Command", 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &command, 0);
  if(error != ERROR_SUCCESS)
    return error;
  QString command_path
    = QString("\"" % QCoreApplication::applicationDirPath() % "/lgx.bat\" \"%1\"").replace("/", "\\");
  QByteArray cp_array = command_path.toLatin1();
  error = RegSetValueEx(command, 0, 0, REG_SZ, (BYTE*)cp_array.data(), cp_array.size() + 1);
  return error;
}

LONG createFileAssociation(HKEY root)
{
  LONG error;
  error = associateFile(root, ".mgxv", "MGX.ViewFile", "MorphoGraphX ViewFile", 1);
  if(error != ERROR_SUCCESS)
    return error;
  error = associateFile(root, ".mgxm", "MGX.MeshFile", "MorphoGraphX MeshFile", 2);
  if(error != ERROR_SUCCESS)
    return error;
  error = associateFile(root, ".mgxs", "MGX.StackFile", "MorphoGraphX StackFile", 3);
  if(error != ERROR_SUCCESS)
    return error;
  error = associateFile(root, ".lgxp", "LGX.ProjectFile", "LithoGraphX ProjectFile", 1);
  if(error != ERROR_SUCCESS)
    return error;
  error = associateFile(root, ".lgxm", "LGX.MeshFile", "LithoGraphX MeshFile", 2);
  if(error != ERROR_SUCCESS)
    return error;
  error = associateFile(root, ".lgxs", "LGX.StackFile", "LithoGraphX StackFile", 3);
  if(error != ERROR_SUCCESS)
    return error;
  error = associateFile(root, ".tif", "LGX.StackFile", "TIFF File", 3);
  if(error != ERROR_SUCCESS)
    return error;
  error = associateFile(root, ".tiff", "LGX.StackFile", "TIFF File", 3);
  if(error != ERROR_SUCCESS)
    return error;
  return ERROR_SUCCESS;
}

void associateFiles()
{
  out << "Associating .mgxv, .mgxm, .mgxs, .lgxp, .lgxm and .lgxs files with LithoGraphX ..." << endl;
  LONG error;
  out << "System-wide associations ... " << flush;
  error = createFileAssociation(HKEY_CLASSES_ROOT);
  if(error != ERROR_SUCCESS) {
    out << "Failed" << endl;
    out << "User-specific associations ..." << flush;
    HKEY software;
    error = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_ALL_ACCESS, &software);
    if(error == ERROR_SUCCESS) {
      HKEY root;
      error = RegOpenKeyEx(software, "Classes", 0, KEY_ALL_ACCESS, &root);
      if(error == ERROR_SUCCESS) {
        error = createFileAssociation(root);
      }
    }
  }
  if(error != ERROR_SUCCESS) {
    char buffer[10000];
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, error, 0, buffer, 10000, 0);
    out << "Error creating registry keys: " << buffer << endl;
    return;
  } else
    out << "Success!" << endl;
}

#endif

enum TO_DO
{
  TD_RUN = 0,
  TD_HELP,
  TD_VERSION,
  TD_APP_DIR,
  TD_USER_PROCESSES_DIR,
  TD_SYSTEM_PROCESSES_DIR,
  TD_ALL_PROCESSES_DIRS,
  TD_INCLUDE_DIR,
  TD_LIBS_DIR,
  TD_USER_MACROS_DIR,
  TD_SYSTEM_MACROS_DIR,
  TD_ALL_MACROS_DIRS,
/*
 *#ifdef WIN32
 *  TD_ASSOCIATIONS
 *#endif
 */
};

void usage()
{
  out << "Usage: LithoGraphX [--debug|--dir|--process|--all-process|--help|-h] [FILE.lgxp]\n"
    "    --debug        - Launch LithoGraphX in debug mode\n"
    "    --dir          - Print the application directory and exit\n"
    "    --process      - Print the system process directory and exit\n"
    "    --user-process - Print the user process directory and exit\n"
    "    --all-process  - Print all the directories searched for processes and exit\n"
    "    --macros       - Print the system macro directory and exit\n"
    "    --user-macros  - Print the user macro directory and exit\n"
    "    --all-macros   - Print all the directories searched for processes and exit\n"
    "    --include      - Print the include directory and exit\n"
    "    --libs         - Print the lib directory and exit\n"
    "    --version      - Display the version and revision and exit\n"
/*
 *#if defined(WIN32) || defined(WIN64)
 *    "    --install      - Define LithoGraphx files associations and icon and exit\n"
 *#endif
 */
    "    --help |-h     - Print this help" << endl;
}

int main(int argc, char** argv)
{
  QCoreApplication::setOrganizationDomain("lithographx.org");
  QCoreApplication::setApplicationName("LithoGraphX");
  QCoreApplication::setApplicationVersion(VERSION);

  bool is_debug = false;
  TO_DO toDo = TD_RUN;
#if defined(WIN32) || defined(WIN64)
  bool associate_files = false;
#endif

  auto todo = [&toDo](TO_DO other) -> void {
    if(toDo != TD_RUN) {
      lgx::Information::err << "Error, only one option can be specified on the command line." << endl;
      ::exit(2);
    }
    toDo = other;
  };

  QString filename;
  // QStringList args = app->arguments();
  for(int i = 1; i < argc; ++i) {
    QString arg = QString::fromLocal8Bit(argv[i]);
    if(arg == "--debug")
      is_debug = true;
    else if((arg == "--help")or (arg == "-h")) {
      todo(TD_HELP);
    } else if(arg == "--dir") {
      todo(TD_APP_DIR);
    } else if(arg == "--process") {
      todo(TD_SYSTEM_PROCESSES_DIR);
    } else if(arg == "--user-process") {
      todo(TD_USER_PROCESSES_DIR);
    } else if(arg == "--all-process") {
      todo(TD_ALL_PROCESSES_DIRS);
    } else if(arg == "--macros") {
      todo(TD_SYSTEM_MACROS_DIR);
    } else if(arg == "--user-macros") {
      todo(TD_USER_MACROS_DIR);
    } else if(arg == "--all-macros") {
      todo(TD_ALL_MACROS_DIRS);
    } else if(arg == "--include") {
      todo(TD_INCLUDE_DIR);
    } else if(arg == "--libs") {
      todo(TD_LIBS_DIR);
    } else if(arg == "--version") {
      todo(TD_VERSION);
    }
/*
 *#if defined(WIN32) || defined(WIN64)
 *    else if(arg == "--install") {
 *      todo(TD_ASSOCIATIONS);
 *    }
 *#endif
 */
    else if(arg.startsWith("--")) {
      lgx::Information::err << "Unknown option: '" << arg << "'" << endl;
      usage();
      return 2;
    }
    else if(filename.isEmpty()) {
      filename = arg;
      QString currdir = QFileInfo(filename).dir().path();
      if(currdir.length() > 0)
        lgx::util::setCurrentPath(currdir);
    }
  }

  QCoreApplication* app = 0;
  if(toDo == TD_RUN)
    app = new QApplication(argc, argv);
  else {
    if(not filename.isEmpty()) {
      lgx::Information::err << "Error, you cannot specify a filename with an option." << endl;
      return 2;
    }
    app = new QCoreApplication(argc, argv);
  }

  switch(toDo) {
    case TD_HELP:
      usage();
      return 0;
    case TD_APP_DIR:
      out << QCoreApplication::applicationDirPath() << endl;
      return 0;
    case TD_SYSTEM_PROCESSES_DIR:
      out << lgx::util::processesDirs()[0].absolutePath() << endl;
      return 0;
    case TD_USER_PROCESSES_DIR:
      out << lgx::util::userProcessesDir(true).absolutePath() << endl;
      return 0;
    case TD_ALL_PROCESSES_DIRS: {
        QList<QDir> dirs = lgx::util::processesDirs();
        forall(const QDir& dir, dirs) {
          out << dir.absolutePath() << endl;
        }
      }
      return 0;
    case TD_SYSTEM_MACROS_DIR:
      out << lgx::util::macroDirs()[0].absolutePath() << endl;
      return 0;
    case TD_USER_MACROS_DIR:
      out << lgx::util::userMacroDir(true).absolutePath() << endl;
      return 0;
    case TD_ALL_MACROS_DIRS: {
        QList<QDir> dirs = lgx::util::macroDirs();
        forall(const QDir& dir, dirs) {
          out << dir.absolutePath() << endl;
        }
      }
      return 0;
    case TD_INCLUDE_DIR:
      out << lgx::util::includesDir().absolutePath() << endl;
      return 0;
    case TD_LIBS_DIR:
      out << lgx::util::libsDir().absolutePath() << endl;
      return 0;
    case TD_VERSION:
      out << "LithoGraphX version " VERSION " rev. " REVISION << endl;
      return 0;
/*
 *#if defined(WIN32) || defined(WIN64)
 *    case TD_ASSOCIATIONS:
 *      associateFiles();
 *      return 0;
 *#endif
 */
    case TD_RUN:
      break; // Nothing to do, we just go on
  }

  // Unfortunately, once you turn multisampling on, it seems to be impossible to turn it off.
  // This screws up selection by color.
  QGLFormat format = QGLFormat::defaultFormat();
  format.setSampleBuffers(false);
  QGLFormat::setDefaultFormat(format);

  // Register extra meta type
  qRegisterMetaType<floatList>("floatList");
  qRegisterMetaTypeStreamOperators<floatList>("floatList");

  lgx::DEBUG = is_debug;
  out << "Welcome to LithoGraphX!" << endl << endl;
  out << "Thrust host evaluation: " << THRUST_TAG << endl << endl;

  // Create the user processes and macros dirs
  lgx::util::userProcessesDir(true);
  lgx::util::userMacroDir(true);

// Add path for ITK Loci tools import
#ifdef UNIX // This is UNIX-only code!!
  QDir dir = resourcesDir();
  out << "Loci tools ITK plugin (bf-itk): ";
  if(dir.cd("bf-itk")) {
    out << "found" << endl;
    QString dirName(QDir::toNativeSeparators(dir.canonicalPath()));
    QString path(getenv("ITK_AUTOLOAD_PATH"));
    if(path.length() == 0)
      setenv("ITK_AUTOLOAD_PATH", dirName.toStdString().data(), 1);
    else
#  if defined(WIN32) || defined(WIN64)
      setenv("ITK_AUTOLOAD_PATH", (dirName + ";" + path).toStdString().data(), 1);
#  else
      setenv("ITK_AUTOLOAD_PATH", (dirName + ":" + path).toStdString().data(), 1);
#  endif
  } else
    out << "not found" << endl;
#endif

  {
      // Update the path to include the processes folder
      QList<QDir> procDirs = lgx::util::processesDirs();
      QStringList procPaths;
      for(const QDir& dir: procDirs) {
          procPaths << QDir::toNativeSeparators(dir.absoluteFilePath("lib"));
      }
#ifdef WIN32
      QString path_sep = ";";
#else
      QString path_sep = ":";
#endif
      //lgx::Information::out << "Adding to PATH:" << procPaths.join(path_sep) << endl;
      auto env = QProcessEnvironment::systemEnvironment();
      QString path = procPaths.join(path_sep) + path_sep + env.value("PATH");
      //lgx::Information::out << "New PATH:\n" << path << endl;
#ifdef WIN32
      SetEnvironmentVariable("PATH", path.toUtf8().data());
#else
      env.insert("PATH", path);
#endif
  }

  // Initialize random numbers generator
  lgx::util::sran_time();

  LithoGraphX* gui = new LithoGraphX(QCoreApplication::applicationDirPath());
  QObject::connect(gui, &QWidget::destroyed, app, &QCoreApplication::quit);
  gui->setDebug(is_debug);
  gui->show();
  if(filename.toLower().endsWith(".lgxp") or filename.toLower().endsWith(".mgxv"))
    gui->loadProject(filename, false);
  else {
    gui->setLoadFile(filename);
    QObject::connect(gui, SIGNAL(processFinished()), gui, SLOT(autoOpen()));
    gui->initProject();
  }

  // Program a show for when the GUI is ready
  // QTimer::singleShot(0, gui, SLOT(show()));
  int result = app->exec();
  delete app;
  if(result == 0)
    out << "Bye bye!" << endl;
  else
    out << "Oops, didn't intend to quit?" << endl;
  return result;
}
