/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Information.hpp"

#include <QMainWindow>
#include <QStatusBar>
#include <QThread>
#include <QCoreApplication>

#include <iostream>
using namespace std;

#include <QTextStream>
#include <stdio.h>

namespace lgx {

bool DEBUG = false;

namespace Information {
QTextStream out(stdout);
QTextStream err(stderr);

static QStatusBar* status = 0;
static QMainWindow* mainWnd = 0;

void setMainWindow(QMainWindow* wnd)
{
  mainWnd = wnd;
  status = wnd->statusBar();
  out.setCodec("UTF-8");
  err.setCodec("UTF-8");
}

void Print(const QString& _text)
{
  if(status and status->thread() != QThread::currentThread()) {
    QCoreApplication::postEvent(mainWnd, new Event(_text));
    return;
  }
  out << _text << endl;
  if(status != 0)
    status->showMessage(_text);
}

void Print(const string _text)
{
  QString txt = QString::fromStdString(_text);
  Print(txt);
}
} // namespace Information
} // namespace lgx
