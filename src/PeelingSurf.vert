/*varying vec3 normal;*/
/*varying vec3 lightDir[4], halfVector[4];*/
varying vec4 realPos;
uniform float shift;

varying float depthProj;

void main()
{
  setTexCoord();

  light();
  /*normal = normalize(gl_NormalMatrix * gl_Normal);*/
  /*
   *for(int i = 0 ; i < 4 ; ++i)
   *{
   *  lightDir[i] = normalize(vec3(gl_LightSource[i].position - gl_Vertex));
   *  halfVector[i] = normalize(gl_LightSource[i].halfVector.xyz);
   *}
   */

  vec4 v = gl_Vertex - shift*vec4(gl_Normal,0.0);
  vec4 vproj = gl_ModelViewProjectionMatrix * v;
  depthProj = vproj.z/vproj.w;
  realPos = gl_ModelViewProjectionMatrix * gl_Vertex;
  gl_Position = vproj;
  gl_FrontColor = gl_Color;
  gl_ClipVertex = gl_ModelViewMatrix * gl_Vertex;
}

