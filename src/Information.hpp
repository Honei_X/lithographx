/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef INFORMATION_H
#define INFORMATION_H

#include <LGXConfig.hpp>

#include <QEvent>
#include <QString>
#include <QString>
#include <QTextStream>

class QMainWindow;

#define SETSTATUS(msg)                             \
  do {                                             \
    QString __status_msg;                          \
    {                                              \
      QTextStream __status_msgstrm(&__status_msg); \
      __status_msgstrm << msg;                     \
    }                                              \
    ::lgx::Information::Print(__status_msg);       \
  } while(false)

#define DEBUG_OUT ::lgx::Information::out

#define DEBUG_OUTPUT(msg) \
  if(::lgx::DEBUG)        \
    do {                  \
      DEBUG_OUT << msg;   \
    } while(false)

namespace lgx {
LGX_EXPORT extern bool DEBUG;

namespace Information {
LGX_EXPORT extern QTextStream out;
LGX_EXPORT extern QTextStream err;

struct LGX_EXPORT Event : public QEvent {
  Event(const QString& msg)
    : QEvent(QEvent::User)
    , message(msg)
  {
  }

  QString message;
};

LGX_EXPORT void setMainWindow(QMainWindow* wnd);
LGX_EXPORT void Print(const QString& _text);
LGX_EXPORT void Print(const std::string _text);
} // namespace Information
} // namespace lgx
#endif
