/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Image.hpp"

#include "Information.hpp"
#include <QImageReader>
#include <QImageWriter>
#include <cmath>
#include "Progress.hpp"
#include "Process.hpp"

#include <limits>
#include <CImg.h>
#include <QDomDocument>
#include <QDomElement>
#include <Misc.hpp>

extern "C" {
#include <tiffio.h>
#include <tiff.h>
}

namespace lgx {
using namespace cimg_library;
typedef CImg<ushort> CImgUS;

QStringList supportedImageReadFormats()
{
  static QStringList formats;
  if(formats.empty()) {
    formats << "dlm"
            << "tif"
            << "cimg"
            << "CImg Auto";
    // foreach(QByteArray ba, QImageReader::supportedImageFormats())
    //{
    //  formats << QString::fromLatin1(ba).toLower();
    //}
    // formats.removeDuplicates();
    formats.sort();
  }
  return formats;
}

QStringList supportedImageWriteFormats()
{
  static QStringList formats;
  if(formats.empty()) {
    formats << "dlm"
            << "tif"
            << "cimg"
            << "CImg Auto";
    // foreach(QByteArray ba, QImageWriter::supportedImageFormats())
    //{
    //  formats << QString::fromLatin1(ba).toLower();
    //}
    // formats.removeDuplicates();
    formats.sort();
  }
  return formats;
}

namespace {
void save_dlm(const QString& fn, const CImgUS& img) { img.save_dlm(fn.toLocal8Bit()); }

void save_cimg(const QString& fn, const CImgUS& img) { img.save_cimg(fn.toLocal8Bit()); }

void save_tiff(const QString& fn, const CImgUS& img) { img.save_tiff(fn.toLocal8Bit()); }

void save_auto(const QString& fn, const CImgUS& img) { img.save(fn.toLocal8Bit()); }
}

bool saveImage(QString filename, const Image3D& data, QString type, unsigned int nb_digits)
{
  if(type.isEmpty()) {
    if(filename.contains("."))
      type = filename.mid(filename.indexOf('.') + 1);
    else
      throw QString("Either the type is set, or the filename must specify an extension.");
  }

  const Point3u& size = data.size;

  unsigned int min_digits = (unsigned int)ceil(log10((float)size.z()));

  if(nb_digits > 0 and nb_digits < min_digits)
    throw QString("Error, you specified %1 digits for %2 images. This is not enough.").arg(nb_digits).arg(size.z());

  if(type != "CImg Auto") {
    if(filename.contains(".")) {
      QString ext = filename.mid(filename.lastIndexOf(".") + 1);
      if(ext.toLower() != type)
        filename += "." + type;
    }
    else {
      filename += "." + type;
    }
  }
  else if(nb_digits == 0)
    nb_digits = min_digits;

  void (*save)(const QString& fn, const CImgUS& img) = 0;
  if(type == "dlm")
    save = &save_dlm;
  else if(type == "cimg")
    save = &save_cimg;
  else if(type == "tif")
    save = &save_tiff;
  else
    save = &save_auto;
  if(nb_digits == 0) {
    CImgUS image(&(*data.data)[0], size.x(), size.y(), size.z(), 1, true);
    save(filename, image);
  }
  else {
    int index_dot = filename.lastIndexOf(".");
    QString prefix = filename.left(index_dot);
    QString suffix = filename.mid(index_dot);
    unsigned int shift = size.x() * size.y();
    for(unsigned int z = 0; z < size.z(); ++z) {
      CImgUS image(&(*data.data)[z * shift], size.x(), size.y(), 1, 1, true);
      QString fn = QString("%1%2%3").arg(prefix).arg(z, nb_digits, 10, QLatin1Char('0')).arg(suffix);
      save(fn, image);
    }
  }
  return true;
}

HVecUS resize(const HVecUS& data, const Point3u& before, const Point3u& after, bool center)
{
  HVecUS result;
  size_t tot_before = size_t(before.x()) * before.y() * before.z();
  size_t tot_after = size_t(after.x()) * after.y() * after.z();
  if(tot_before != data.size())
    return result;
  result.resize(tot_after, 0);
  uint length_x = std::min(before.x(), after.x());
  uint length_y = std::min(before.y(), after.y());
  uint length_z = std::min(before.z(), after.z());
  uint before_shift_x = 0;
  uint before_shift_y = 0;
  uint before_shift_z = 0;
  uint after_shift_x = 0;
  uint after_shift_y = 0;
  uint after_shift_z = 0;
  uint before_inter_y = before.x() * (before.y() - length_y);
  uint after_inter_y = after.x() * (after.y() - length_y);
  if(center) {
    if(length_x == before.x())
      after_shift_x = (after.x() - length_x) / 2;
    else
      before_shift_x = (before.x() - length_x) / 2;
    if(length_y == before.y())
      after_shift_y = (after.y() - length_y) / 2;
    else
      before_shift_y = (before.y() - length_y) / 2;
    if(length_z == before.z())
      after_shift_z = (after.z() - length_z) / 2;
    else
      before_shift_z = (before.z() - length_z) / 2;
  }
  const ushort* b = &data[(before_shift_z * before.y() + before_shift_y) * before.x() + before_shift_x];
  ushort* a = &result[(after_shift_z * after.y() + after_shift_y) * after.x() + after_shift_x];
  for(uint z = 0; z < length_z; ++z) {
    for(uint y = 0; y < length_y; ++y) {
      memcpy(a, b, sizeof(ushort) * length_x);
      b += before.x();
      a += after.x();
    }
    b += before_inter_y;
    a += after_inter_y;
  }
  return result;
}

Image3D::Image3D()
  : ImageInfo()
{
}

Image3D::Image3D(const QString& filename)
  : ImageInfo(filename)
{ }

Image3D::Image3D(const QString& filename, HVecUS& data, const Point3u& size, const Point3f& step)
  : ImageInfo(filename, size, step)
  , data(&data)
{
}

void Image3D::allocate(const Point3u& s)
{
  size = s;
  allocate();
}

void Image3D::allocate()
{
  minc = 0;
  maxc = 0;
  size_t s = size_t(size.x()) * size.y() * size.z();
  if(s > 0) {
    if(data) {
      data->resize(s);
    }
    else {
      allocated = true;
      data = new HVecUS(s);
    }
  }
  else {
    if(allocated) {
      delete data;
      allocated = false;
      data = 0;
    }
    else
      data->clear();
  }
}

Image3D::~Image3D()
{
  if(data and allocated)
    delete data;
  allocated = false;
  data = 0;
}

namespace {
void tiffErrorHandler(const char* module, const char* fmt, va_list ap)
{
  SETSTATUS("TIFF Error in module " << module);
  vfprintf(stderr, fmt, ap);
}
} // namespace

struct StripDataIncrement {
  int start_row;
  int start_column;
  int shift_row;
  int shift_column;
  int line_end;
};

template <typename T> struct StripBuffer {
  StripBuffer(T* buf)
      : buffer(buf)
  {
  }
  T* buffer;
  StripBuffer& operator++()
  {
    ++buffer;
    return *this;
  }
  void newLine() {}
  uint operator*() const;
};

template <> uint StripBuffer<signed char>::operator*() const
{
  int val = *buffer;
  if(val < 0)
    return 0;
  return uint(val << 8);
}

template <> uint StripBuffer<unsigned char>::operator*() const { return uint(*buffer) << 8; }

template <> uint StripBuffer<short>::operator*() const
{
  int val = *buffer;
  if(val < 0)
    return 0;
  return uint(val);
}

template <> uint StripBuffer<ushort>::operator*() const { return uint(*buffer); }

template <> uint StripBuffer<int>::operator*() const
{
  int val = *buffer;
  if(val < 0)
    return 0;
  return uint(val >> 16);
}

template <> uint StripBuffer<uint>::operator*() const
{
  int val = *buffer;
  return (val >> 16);
}

template <> uint StripBuffer<float>::operator*() const
{
  float val = *buffer;
  if(val < 0)
    return 0;
  return uint(floor(val));
}

template <typename T> StripBuffer<T> stripBuffer(T* buf) { return StripBuffer<T>(buf); }

namespace {
template <typename Buffer>
void readStrip(Image3D& data, ushort*& p, const StripDataIncrement& incr, Buffer buf, size_t nb_pixels, uint16 spp)
{
  int col_count = 0;
  for(size_t i = 0; i < nb_pixels; ++i) {
    uint val = 0;
    for(size_t j = 0; j < spp; ++j, ++buf)
      val += *buf;
    val *= data.brightness;
    if(val > 0xFFFF)
      *p = 0xFFFF;
    else
      *p = ushort(val);
    p += incr.shift_column;
    if(++col_count >= incr.line_end) {
      col_count = 0;
      p += incr.shift_row;
      buf.newLine();
    }
    if(data.minc > val)
      data.minc = val;
    if(data.maxc < val)
      data.maxc = val;
  }
}

struct TIFFHandler {
  typedef TIFF* pointer;
  TIFFHandler(TIFF* t)
      : tif(t)
  {
  }

  TIFFHandler(TIFFHandler&& other)
    : tif(other.tif)
  { other.tif = nullptr; }

  TIFFHandler& operator=(TIFFHandler&& other)
  {
    free();
    tif = other.tif;
    other.tif = nullptr;
    return *this;
  }

  ~TIFFHandler()
  {
    free();
  }

  operator bool() const { return bool(tif); }

  operator pointer() { return tif; }

private:
  void free() {
    if(tif)
      TIFFClose(tif);
    tif = 0;
  }

  TIFF* tif;
};

struct TIFFAllocated {
  TIFFAllocated(tdata_t t)
      : tif(t)
  {
  }

  ~TIFFAllocated()
  {
    if(tif)
      _TIFFfree(tif);
    tif = 0;
  }

  operator bool() const { return bool(tif); }

  operator tdata_t() { return tif; }

  tdata_t& pointer() { return tif; }

  tdata_t tif;
};

#define NB_UNITS 22

static const QString units[NB_UNITS] = {
  "ym",  // yocto - 10^-24
  "zm",  // zepto - 10^-21
  "am",  // atto  - 10^-18
  "fm",  // femto - 10^-15
  "pm",  // pico  - 10^-12
  "nm",  // nano  - 10^-9
  "um",  // micro - 10^-6
  UM,    // micro - 10^-6
  "mm",  // milli - 10^-3
  "cm",  // centi - 10^-2
  "dm",  // deci  - 10^-1
  "m",   // unit  - 10^0
  "dam", // deca  - 10^1
  "hm",  // hecto - 10^2
  "km",  // kilo  - 10^3
  "Mm",  // mega  - 10^6
  "Gm",  // giga  - 10^9
  "Tm",  // tera  - 10^12
  "Pm",  // peta  - 10^15
  "Em",  // exa   - 10^18
  "Zm",  // zetta - 10^21
  "Ym"   // yota  - 10^24
};

static const float unit_value[NB_UNITS] = { 1e-24f, 1e-21f, 1e-18f, 1e-15f, 1e-12f, 1e-9f, 1e-6f, 1e-6f, 1e-3f, 1e-2f, 1e-1f, 1.f,
  1e1f, 1e2f, 1e3f, 1e6f, 1e9f, 1e12f, 1e15f, 1e18f, 1e21f, 1e24f };

std::array<ImageInfo::Axis, 5> readOMETIFFDimensionOrder(const QDomElement& pixels)
{
  QString val = pixels.attribute("DimensionOrder", "XYZCT");
  if(val.startsWith("XY") and val.contains('Z') and val.contains('C') and val.contains('T')) {
    std::array<ImageInfo::Axis, 5> order;
    order[0] = ImageInfo::X;
    order[1] = ImageInfo::Y;
    auto idx = val.indexOf('Z');
    order[idx] = ImageInfo::Z;
    idx = val.indexOf('C');
    order[idx] = ImageInfo::C;
    idx = val.indexOf('T');
    order[idx] = ImageInfo::T;
    return order;
  }
  SETSTATUS("Error, the DimensionOrder attribute of the OME TIFF must contain X, Y, Z, C and T and start with 'XY'.");
  auto bad = ImageInfo::T;
  return { bad, bad, bad, bad, bad };
}

float readOMETIFFresolution(const QDomElement& pixels, QString axis)
{
  bool ok;
  float unit = 0;
  QString val;
  val = pixels.attribute(QString("PhysicalSize%1Unit").arg(axis));
  if(!val.isEmpty()) {
    for(size_t i = 0; i < NB_UNITS; ++i) {
      if(units[i] == val) {
        unit = unit_value[i];
        break;
      }
    }
  }
  if(unit == 0)
    unit = 1e-6f;
  val = pixels.attribute(QString("PhysicalSize%1").arg(axis));
  if(!val.isEmpty()) {
    float res = val.toDouble(&ok);
    if(not ok) {
      SETSTATUS(QString("Invalid OME TIFF property PhysicalSize%1").arg(axis));
      return false;
    }
    return res * unit;
  }
  return -1;
}

int readOMETIFFSize(const QDomElement& pixels, QString axis)
{
  bool ok;
  QString val = pixels.attribute(QString("Size%1").arg(axis));
  if(!val.isEmpty()) {
    size_t size = val.toUInt(&ok);
    if(not ok) {
      SETSTATUS(QString("Invalid OME TIFF property Size%1").arg(axis));
      return false;
    }
    return size;
  }
  return -1;
}

bool getTIFFInfo(TIFF* tif, ImageInfo& info)
{
  // First, check if this is ImageJ-like
  float xres = info.step.x(), yres = info.step.y(), zres = info.step.z();
  float unit = 1e-6; // um by default
  uint16 resunit;
  char* desc;

  // Read standard tags
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_IMAGEWIDTH, &info.size.x()) != 1) {
    SETSTATUS("Error, no width");
    return false;
  }
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_IMAGELENGTH, &info.size.y()) != 1) {
    SETSTATUS("Error, no height");
    return false;
  }
  bool has_xres = TIFFGetField(tif, TIFFTAG_XRESOLUTION, &xres);
  bool has_yres = TIFFGetField(tif, TIFFTAG_YRESOLUTION, &yres);
  bool has_zres = false;
  bool has_desc = TIFFGetField(tif, TIFFTAG_IMAGEDESCRIPTION, &desc);
  bool has_resunit = TIFFGetFieldDefaulted(tif, TIFFTAG_RESOLUTIONUNIT, &resunit);
  // Set default values
  info.labels = false;
  info.nb_channels = 1;
  info.nb_timepoints = 1;
  if(has_xres and xres > 0 and has_yres and yres > 0) {
    xres = 1.0 / xres;
    yres = 1.0 / yres;
  }
  if(has_resunit) {
    if(resunit == RESUNIT_INCH) {
      unit = 0.0254;
    }
    else if(resunit == RESUNIT_CENTIMETER) {
      unit = 1e-2;
    }
  }

  // And check description
  bool known_format = false;
  QString description = (has_desc ? QString::fromLatin1(desc) : "");
  if(description.startsWith("ImageJ")) {
    known_format = true;
    // Ordering of stacks in ImageJ:
    info.axisOrder = { ImageInfo::X, ImageInfo::Y, ImageInfo::C, ImageInfo::Z, ImageInfo::T };
    Information::out << "**** Detected ImageJ TIFF" << endl;
    QStringList fields = description.split("\n");
    for(QString f : fields) {
      QStringList entry = f.split("=");
      if(entry.size() == 2) {
        QString key = entry[0].trimmed();
        QString value = entry[1].trimmed();
        if(key == "spacing") {
          SETSTATUS("Found spacing = " << value);
          has_zres = true;
          bool ok;
          zres = value.toFloat(&ok);
          if(!ok or zres <= 0)
            zres = 1;
        }
        else if(key == "unit") {
          for(int i = 0; i < NB_UNITS; ++i) {
            if(value == units[i]) {
              unit = unit_value[i];
              SETSTATUS("Found unit = " << value << " => " << unit << "m");
              break;
            }
          }
        }
        else if(key == "labels") {
          info.labels = process::stringToBool(value);
          SETSTATUS("Found labels = " << value << " -> " << process::boolToString(info.labels));
        }
        else if(key == "channels") {
          SETSTATUS("Found channels = " << value);
          bool ok;
          info.nb_channels = value.toUInt(&ok);
          if(not ok)
            info.nb_channels = 1;
        }
        else if(key == "frames") {
          SETSTATUS("Found channels = " << value);
          bool ok;
          info.nb_timepoints = value.toUInt(&ok);
          if(not ok)
            info.nb_timepoints = 1;
        }
        else if(key == "slices") {
          SETSTATUS("Found nb slices = " << value);
          bool ok;
          info.size.z() = value.toUInt(&ok);
          if(not ok) {
            SETSTATUS("Error, invalid number of slices");
            return false;
          }
        }
        else if(key == "origin_x") {
          bool ok;
          info.origin.x() = value.toFloat(&ok);
          SETSTATUS("Found origin_x = " << value << " => " << info.origin.x());
          if(not ok) {
            SETSTATUS("Error, invalid origin_x value");
          }
        }
        else if(key == "origin_y") {
          bool ok;
          info.origin.y() = value.toFloat(&ok);
          SETSTATUS("Found origin_y = " << value << " => " << info.origin.y());
          if(not ok) {
            SETSTATUS("Error, invalid origin_y value");
          }
        }
        else if(key == "origin_z") {
          bool ok;
          info.origin.z() = value.toFloat(&ok);
          SETSTATUS("Found origin_z = " << value << " => " << info.origin.z());
          if(not ok) {
            SETSTATUS("Error, invalid origin_z value");
          }
        }
      }
    }
  }
  else if(description.startsWith("<?xml")) { // This may not be OME TIFF, but it is at least XML
    QDomDocument doc;
    Information::out << "**** Detected XML-TIFF" << endl;
    if(doc.setContent(description)) {
      QDomElement root = doc.documentElement();
      if(root.tagName() == "OME") {
        known_format = true;
        Information::out << "**** Detected OME-TIFF" << endl;
        // We have an OME-TIFF: find the resolution
        QDomElement image = root.firstChildElement("Image");
        if(!image.isNull()) {
          QDomElement pixels = image.firstChildElement("Pixels");
          if(!pixels.isNull()) {
            auto order = readOMETIFFDimensionOrder(pixels);
            if(order[0] != ImageInfo::X)
              return false;
            info.axisOrder = order;
            xres = readOMETIFFresolution(pixels, "X");
            has_xres = xres > 0;
            if(has_xres)
              info.step.x() = xres;
            yres = readOMETIFFresolution(pixels, "Y");
            has_yres = yres > 0;
            if(has_yres)
              info.step.y() = yres;
            zres = readOMETIFFresolution(pixels, "Z");
            has_zres = zres > 0;
            if(has_zres)
              info.step.z() = zres;

            auto sizex = readOMETIFFSize(pixels, "X");
            if(sizex > 0)
              info.size.x() = sizex;
            else
              return false;
            auto sizey = readOMETIFFSize(pixels, "Y");
            if(sizey > 0)
              info.size.y() = sizey;
            else
              return false;
            auto sizez = readOMETIFFSize(pixels, "Z");
            if(sizez > 0)
              info.size.z() = sizez;
            else
              return false;
            auto sizet = readOMETIFFSize(pixels, "T");
            if(sizet > 0)
              info.nb_timepoints = sizet;
            auto sizec = readOMETIFFSize(pixels, "C");
            if(sizec > 0)
              info.nb_channels = sizec;
          }
        }
      }
    }
  }
  if(not known_format) {
    Information::out << "*** TIFF of unknown source" << endl;
    size_t nb_images = 0;
    tdir_t first_dir = TIFFCurrentDirectory(tif);
    do {
      nb_images++;
    } while(TIFFReadDirectory(tif));
    TIFFSetDirectory(tif, first_dir);
    info.size.z() = nb_images;
    info.nb_timepoints = 1;
    info.nb_channels = 1;
  }
  unit *= 1e6;
  if(not has_zres)
    Information::out << QString::fromWCharArray(L"Warning, no z resolution found, using default: %1 %2 px\x207B\xb9")
                            .arg(unit * zres)
                            .arg(UM) << endl;
  info.step = unit * Point3f(xres, yres, zres);
  return true;
}

bool readImagePlane(TIFF* tif, Image3D& data, size_t offset)
{
  uint32 rw, rh, rps;
  uint32* bc;
  uint16 planar_config, bps, spp, format, orientation;
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_IMAGEWIDTH, &rw) != 1) {
    SETSTATUS("Error, no width");
    return false;
  }
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_IMAGELENGTH, &rh) != 1) {
    SETSTATUS("Error, no height");
    return false;
  }
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_BITSPERSAMPLE, &bps) != 1) {
    SETSTATUS("Error, no bits per sample");
    return false;
  }
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_SAMPLESPERPIXEL, &spp) != 1) {
    SETSTATUS("Error, no samples per pixel");
    return false;
  }
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_PLANARCONFIG, &planar_config) != 1) {
    SETSTATUS("Error, no planar configuration");
    return false;
  }
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_STRIPBYTECOUNTS, &bc) != 1) {
    SETSTATUS("Error, no strip bute counts");
    return false;
  }
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_SAMPLEFORMAT, &format) != 1) {
    format = 1;
  }
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_ORIENTATION, &orientation) != 1) {
    orientation = 1;
  }
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_ROWSPERSTRIP, &rps) != 1) {
    SETSTATUS("Error, no rows per strip");
    return false;
  }
  int32 w = rw, h = rh;
  /* Note: If we try this, then it crashes when reading ZIP files
   *bool has_compression = TIFFGetField(tif, TIFFTAG_ZIPQUALITY, &data.compression_level);
   *if(not has_compression)
   *  data.compression_level = 0;
   */
  if(bps != 8 and bps != 16 and bps != 32) {
    SETSTATUS("Error, this reader handles only 8, 16 and 32 bits per sample");
    return false;
  }
  StripDataIncrement incr;
  switch(orientation) {
  case ORIENTATION_TOPRIGHT: // line = top, column = right
    incr.shift_column = -1;
    incr.shift_row = 0;
    incr.start_column = w - 1;
    incr.start_row = h - 1;
    incr.line_end = w;
    break;
  case ORIENTATION_BOTRIGHT: // line = bottom column = right
    incr.shift_column = -1;
    incr.shift_row = 2 * w;
    incr.start_column = w - 1;
    incr.start_row = 0;
    incr.line_end = w;
    break;
  case ORIENTATION_BOTLEFT: // line = bottom, column = left
    incr.shift_column = 1;
    incr.shift_row = 0;
    incr.start_column = 0;
    incr.start_row = 0;
    incr.line_end = w;
    break;
  case ORIENTATION_LEFTTOP: // line = left, column = top
    std::swap(w, h);
    incr.shift_column = -w;
    incr.shift_row = h * w + 1;
    incr.start_column = 0;
    incr.start_row = h - 1;
    incr.line_end = h;
    break;
  case ORIENTATION_RIGHTTOP: // line = right, column = top
    std::swap(w, h);
    incr.shift_column = -w;
    incr.shift_row = h * w - 1;
    incr.start_column = w - 1;
    incr.start_row = h - 1;
    incr.line_end = h;
    break;
  case ORIENTATION_RIGHTBOT: // line = right, column = bottom
    std::swap(w, h);
    incr.shift_column = w;
    incr.shift_row = -h * w - 1;
    incr.start_column = w - 1;
    incr.start_row = 0;
    incr.line_end = h;
    break;
  case ORIENTATION_LEFTBOT: // line = left, column = bottom
    std::swap(w, h);
    incr.shift_column = w;
    incr.shift_row = -h * w + 1;
    incr.start_column = 0;
    incr.start_row = 0;
    incr.line_end = h;
    break;
  case ORIENTATION_TOPLEFT: // line = top, column = left
  default:
    incr.shift_column = 1;
    incr.shift_row = -2 * w;
    incr.start_column = 0;
    incr.start_row = h - 1;
    incr.line_end = w;
    break;
  }
  // SETSTATUS(QString("TIFF file %1x%2 with %3 bps").arg(w).arg(h).arg(bps));
  size_t tot_pixels = 0, max_pixels = data.size.x() * data.size.y();
  ushort* p = &data[offset + incr.start_column + incr.start_row * w];
  switch(planar_config) {
  case PLANARCONFIG_SEPARATE: {
    SETSTATUS("Error, PLANARCONFIG_SEPARATE is not yet handled.");
  } break;
  case PLANARCONFIG_CONTIG: {
    uint32 strip_size = TIFFStripSize(tif);
    size_t nb_pixels = rps * w;
    TIFFAllocated alloc = _TIFFmalloc(strip_size+1);
    for(size_t strip = 0; strip < TIFFNumberOfStrips(tif); ++strip) {
      size_t size_buf = TIFFReadEncodedStrip(tif, strip, alloc.pointer(), strip_size+1);
      if(size_buf == 0)
        Information::err << "Warning: empty buffer!" << endl;
      if(size_buf > strip_size+1) {
        Information::err << "Error when reading encoded strip" << endl;
      }
      // Now, convert the data
      if((tot_pixels + nb_pixels) > max_pixels)
        nb_pixels = max_pixels - tot_pixels;
      tot_pixels += nb_pixels;
      switch(bps) {
      /*
         case 1:
         {
         readStrip(data, p, incr, BitStripBuffer((unsigned char*)alloc.pointer(), 1), nb_pixels,
         spp);
         }
         break;
         case 2:
         {
         readStrip(data, p, incr, BitStripBuffer((unsigned char*)alloc.pointer(), 2), nb_pixels,
         spp);
         }
         break;
         case 4:
         {
         readStrip(data, p, incr, BitStripBuffer((unsigned char*)alloc.pointer(), 4), nb_pixels,
         spp);
         }
         break;
         */
      case 8: {
        switch(format) {
        case SAMPLEFORMAT_UINT:
          readStrip(data, p, incr, stripBuffer((unsigned char*)alloc.pointer()), nb_pixels, spp);
          break;
        case SAMPLEFORMAT_INT:
          readStrip(data, p, incr, stripBuffer((signed char*)alloc.pointer()), nb_pixels, spp);
          break;
        default:
          SETSTATUS(QString("Error, cannot handle 8 bits data format: %1").arg(format));
          return false;
        }
      } break;
      case 16: {
        switch(format) {
        case SAMPLEFORMAT_UINT:
          readStrip(data, p, incr, stripBuffer((unsigned short*)alloc.pointer()), nb_pixels, spp);
          break;
        case SAMPLEFORMAT_INT:
          readStrip(data, p, incr, stripBuffer((signed short*)alloc.pointer()), nb_pixels, spp);
          break;
        default:
          SETSTATUS(QString("Error, cannot handle 16 bits data format: %1").arg(format));
          return false;
        }
      } break;
      case 32: {
        switch(format) {
        case SAMPLEFORMAT_UINT:
          readStrip(data, p, incr, stripBuffer((uint*)alloc.pointer()), nb_pixels, spp);
          break;
        case SAMPLEFORMAT_INT:
          readStrip(data, p, incr, stripBuffer((int*)alloc.pointer()), nb_pixels, spp);
          break;
        case SAMPLEFORMAT_IEEEFP:
          readStrip(data, p, incr, stripBuffer((float*)alloc.pointer()), nb_pixels, spp);
          break;
        default:
          SETSTATUS(QString("Error, cannot handle 32 bits data format: %1").arg(format));
          return false;
        }
      } break;
      default:
        SETSTATUS(QString("Error, cannot handle %1 bits per sample").arg(bps));
        return false;
      }
    }
  } break;
  default:
    SETSTATUS("Error, unknown planar configuration: " << planar_config);
    return false;
  }
  if(tot_pixels != max_pixels)
    SETSTATUS("Warning, read " << tot_pixels << " when " << max_pixels << " where expected");
  return true;
}

template <typename Selection>
size_t loadTIFFImage(TIFF* tif, Image3D& data, ImageInfo& info, Progress* progress, Selection selection,
                     size_t nb_planes = std::numeric_limits<size_t>::max())
{
  bool planeReady = true;
  size_t planes_read = 0;
  Point3u zct; // Current position in the files
  Point3u dims(info.size.z(), info.nb_channels, info.nb_timepoints);
  std::array<int, 5> axes = { -1, -1, 0, 1, 2 };
  while(planeReady) {
    DEBUG_OUTPUT("Reading plane -> zct = " << zct << endl);
    size_t plane = selection(zct);
    bool limit_reads = nb_planes <= data.size.z();
    if(plane <= data.size.z()) {
      if(not readImagePlane(tif, data, data.size.x() * data.size.y() * plane))
        return 0;
      ++planes_read;
      if(progress and !progress->advance(planes_read))
        throw process::UserCancelException();
      if(limit_reads and nb_planes == planes_read)
        break;
    }
    planeReady = TIFFReadDirectory(tif);
    for(size_t i = 2; i < 5; ++i) {
      auto d = axes[info.axisOrder[i]];
      ++zct[d];
      if(zct[d] >= dims[d])
        zct[d] = 0;
      else
        break;
    }
  }
  return planes_read;
}

} // namespace

bool loadTIFFSamples(QString filename, Image3D& data, Progress* progress)
{
  QByteArray ba = filename.toLocal8Bit();
  TIFFSetErrorHandler(tiffErrorHandler);
  TIFFHandler tif = TIFFOpen(ba.data(), "r");
  data.step = Point3f(1, 1, 1);
  size_t depth = 0;
  if(tif) {
    // First, scan the number of directories
    ImageInfo info(filename);
    if(not getTIFFInfo(tif, info))
      return false;
    depth = info.size.z();
    SETSTATUS(depth << " images in this TIFF file");
    static_cast<ImageInfo&>(data) = info;
    SETSTATUS("data.size = " << data.size);
    data.size.z() = info.nb_channels * info.nb_timepoints;
    data.allocate();
    if(progress)
      progress->setMaximum(data.size.z());

    size_t mid_z = info.size.z() / 2;

    auto planes_read = loadTIFFImage(tif, data, info, progress,
                                     [mid_z,&info](const Point3u& zct) -> size_t {
                                       if(zct[0] == mid_z)
                                         return zct[1] + zct[2]*info.nb_channels;
                                       return std::numeric_limits<size_t>::max();
                                     }, info.size.z());

    if(planes_read < data.size.z()) {
      SETSTATUS("Error, not enough images in the file!");
      return false;
    }

  } else {
    SETSTATUS("Cannot open image file " << filename);
    return false;
  }
  return true;
}

bool loadTIFFImage(QString filename, Image3D& data,
                   size_t channel, size_t timepoint, bool allocate_data, Progress* progress)
{
  QByteArray ba = filename.toLocal8Bit();
  TIFFSetErrorHandler(tiffErrorHandler);
  TIFFHandler tif = TIFFOpen(ba.data(), "r");
  data.step = Point3f(1, 1, 1);
  size_t depth = 0;
  if(tif) {
    // First, scan the number of directories
    ImageInfo info(filename);
    if(not getTIFFInfo(tif, info))
      return false;
    depth = info.size.z();
    SETSTATUS(depth << " images in this TIFF file");
    if(channel >= info.nb_channels) {
      SETSTATUS("Error, invalid channel selected");
      return false;
    }
    if(timepoint >= info.nb_timepoints) {
      SETSTATUS("Error, invalid channel selected");
      return false;
    }
    if(progress)
      progress->setMaximum(depth);
    if(allocate_data) {
      static_cast<ImageInfo&>(data) = info;
      SETSTATUS("data.size = " << data.size);
      data.allocate();
    } else {
      if(data.data) {
        if(data.plane == -1) {
          if(data.size.x() != info.size.x() or data.size.y() != info.size.z() or data.size.z() != info.size.z()) {
            SETSTATUS(QString("Error, writing an image of size %1x%2x%3, but an image of size %4x%5x%6 "
                              "has been allocated")
                          .arg(info.size.x())
                          .arg(info.size.y())
                          .arg(info.size.z())
                          .arg(data.size.x())
                          .arg(data.size.y())
                          .arg(data.size.z()));
            return false;
          }
        } else {
          if(data.size.x() != info.size.x() or data.size.y() != info.size.y() or data.plane < 0
              or (size_t) data.plane >= data.size.z()) {
            SETSTATUS(QString("Error, writing on plane %3 an image of size %1x%2, but an image of size "
                              "%4x%5x%6 has been allocated")
                          .arg(info.size.x())
                          .arg(info.size.y())
                          .arg(data.plane)
                          .arg(data.size.x())
                          .arg(data.size.y())
                          .arg(data.size.z()));
            return false;
          }
        }
      } else {
        SETSTATUS("Error, trying to write on not-allocated image!");
        return false;
      }
    }
    size_t planes_read = loadTIFFImage(tif, data, info, progress,
                                       [channel,timepoint](const Point3u& zct) -> size_t {
                                         if(zct[1] == channel and zct[2] == timepoint)
                                           return zct[0];
                                         return std::numeric_limits<size_t>::max();
                                       }, info.size.z());
    if(planes_read < data.size.z()) {
      SETSTATUS("Error, not enough images in the file!");
      return false;
    }
  } else {
    SETSTATUS("Cannot open image file " << filename);
    return false;
  }
  return true;
}

bool saveTIFFImage(QString filename, const Image3D& data)
{
  QByteArray ba = filename.toLocal8Bit();
  TIFFSetErrorHandler(tiffErrorHandler);
  TIFFHandler tif = TIFFOpen(ba.data(), "w");
  if(tif) {
    tsize_t image_size = data.size.x() * data.size.y();
    for(uint z = 0; z < data.size.z(); ++z) {
      uint32 w = data.size.x(), h = data.size.y(), rps = data.size.y();
      if(TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 16) != 1) {
        SETSTATUS("Error writing bits per sample");
        return false;
      }
      if(TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1) != 1) {
        SETSTATUS("Error writing samples per pixel");
        return false;
      }
      if(TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG) != 1) {
        SETSTATUS("Error writing planar configuration");
        return false;
      }
      if(TIFFSetField(tif, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT) != 1) {
        SETSTATUS("Error writing sample format");
        return false;
      }
      if(TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_BOTLEFT) != 1) {
        SETSTATUS("Error writing orientation");
        return false;
      }
      if(TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, w) != 1) {
        SETSTATUS("Error writing width");
        return false;
      }
      if(TIFFSetField(tif, TIFFTAG_IMAGELENGTH, h) != 1) {
        SETSTATUS("Error writing height");
        return false;
      }
      if(TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, rps) != 1) {
        SETSTATUS("Error writing rows per strip");
        return false;
      }
      if(data.compression_level > 0) {
        if(TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_DEFLATE) != 1) {
          SETSTATUS("Error setting DEFLATE compression scheme");
          return false;
        }
        if(TIFFSetField(tif, TIFFTAG_ZIPQUALITY, data.compression_level) != 1) {
          SETSTATUS("Error setting ZIP quality");
          return false;
        }
      } else if(TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_LZW) != 1) {
        SETSTATUS("Error setting LZW compression scheme");
        return false;
      }
      /*
       *if(TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_LZW) != 1) {
       *  SETSTATUS("Error setting LZW compression scheme");
       *  return false;
       *}
       */

      float xres = 1 / data.step.x(), yres = 1 / data.step.y(), zres = data.step.z();
      uint16 resunit = RESUNIT_NONE;

      if(TIFFSetField(tif, TIFFTAG_XRESOLUTION, xres) != 1) {
        SETSTATUS("Error writing x resolution");
        return false;
      }
      if(TIFFSetField(tif, TIFFTAG_YRESOLUTION, yres) != 1) {
        SETSTATUS("Error writing y resolution");
        return false;
      }
      QString description = QString("ImageJ=1.45l\nimages=%1\nslices=%1\nunit=um\nspacing=%2\nloop=false\nlabels=%3\n"
                                    "origin_x=%4\norigin_y=%5\norigin_z=%6")
                                .arg(data.size.z())
                                .arg(zres)
                                .arg(data.labels ? "true" : "false")
                                .arg(data.origin.x())
                                .arg(data.origin.y())
                                .arg(data.origin.z());
      Information::out << "description='" << description << "'" << endl;
      QByteArray desc = description.toLatin1();
      if(TIFFSetField(tif, TIFFTAG_IMAGEDESCRIPTION, desc.data()) != 1) {
        SETSTATUS("Error writing description");
        return false;
      }
      if(TIFFSetField(tif, TIFFTAG_RESOLUTIONUNIT, resunit) != 1) {
        SETSTATUS("Error writing resolution unit");
        return false;
      }

      if(TIFFWriteEncodedStrip(tif, 0, (tdata_t) & (*data.data)[image_size * z], 2 * image_size) == -1) {
        SETSTATUS("Error writing image data");
        return false;
      }

      TIFFWriteDirectory(tif);
    }
  }
  return true;
}

bool loadImage(QString filename, Image3D& data, bool allocate_data)
{
  if(!(filename.endsWith(".tif", Qt::CaseInsensitive) or filename.endsWith(".tiff", Qt::CaseInsensitive))) {
    CImgUS image(filename.toLocal8Bit().data());
    if(allocate_data) {
      if((int)data.size.x() != image.width() or (int)data.size.y() != image.height()
          or (data.plane == -1 and (int)data.size.z() != image.depth())) {
        Point3u size = Point3u(image.width(), image.height(), 1);
        data.allocate(size);
      }
    }
    if(data.data) {
      if(int(data.size.x()) != image.width() or int(data.size.y()) != image.height()) {
        SETSTATUS(
            QString("Error: Bad image file: %1 size: %2x%3").arg(filename).arg(image.width()).arg(image.height()));
        return false;
      }
      ushort* p = 0;
      if(data.plane == -1) {
        p = &data[0];
      }
      else {
        p = &data[data.size.x() * data.size.y() * data.plane];
      }
      for(uint y = 0; y < data.size.y(); y++)
        for(uint x = 0; x < data.size.x(); x++) {
          float voxel = 0;
          for(uint s = 0; s < (uint)image.spectrum(); s++)
            voxel += image(x, y, 0, s);
          voxel *= data.brightness;
          *p++ = ushort(voxel);
          if(voxel > data.maxc)
            data.maxc = voxel;
          if(voxel < data.minc)
            data.minc = voxel;
        }
    }
    else {
      data.size.x() = image.width();
      data.size.y() = image.height();
      data.size.z() = image.depth();
    }
    return true;
  }
  else // Use libtiff!
    return loadTIFFImage(filename, data, allocate_data);
}

ImageInfo getTIFFInfo(QString filename)
{
  QByteArray ba = filename.toLocal8Bit();
  TIFFSetErrorHandler(tiffErrorHandler);
  TIFFHandler tif = TIFFOpen(ba.data(), "r");
  if(tif) {
    ImageInfo result(filename);
    if(not getTIFFInfo(tif, result))
      return ImageInfo();
    return result;
  }
  return ImageInfo();
}

} // namespace lgx
