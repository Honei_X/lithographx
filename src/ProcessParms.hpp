/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PROCESSPARMS_HPP
#define PROCESSPARMS_HPP

#include <Process.hpp>

#include <QAbstractTableModel>
#include <QStyledItemDelegate>

class FreeFloatDelegate : public QStyledItemDelegate {
  Q_OBJECT
public:
  FreeFloatDelegate(QObject* parent = 0);
  QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const;
  void setEditorData(QWidget* editor, const QModelIndex& index) const;
  void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;
  void updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const;
};

class ProcessParmsModel : public QAbstractTableModel {
  Q_OBJECT
public:
  ProcessParmsModel(QObject* parent = 0)
    : QAbstractTableModel(parent)
  {
  }
  void setParms(const lgx::process::BaseProcessDefinition& def);
  void setParms(const QStringList& parms);
  void clear();

  int rowCount(const QModelIndex& /*parent*/ = QModelIndex()) const {
    return names.size();
  }
  int columnCount(const QModelIndex& /*parent*/ = QModelIndex()) const {
    return 2;
  }
  QVariant data(const QModelIndex& index, int role) const;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
  Qt::ItemFlags flags(const QModelIndex& index) const;
  bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole);
  const QStringList& parms() const {
    return _parms;
  }

  QStringList parmChoice(int pos) const;

signals:
  void valuesChanged();

protected:
  QStringList names;
  QStringList descs;
  QStringList _parms;
  lgx::process::ParmChoiceMap _parmChoice;
};
#endif
