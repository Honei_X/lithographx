/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Store.hpp"
#include "Stack.hpp"

#include <QFileInfo>
#include "Information.hpp"
#include <string.h>
#include "Dir.hpp"
#include <climits>

namespace lgx {
Store::Store(Stack* stack)
  : _label(false)
  , changed_function(false)
  , _opacity(1.0f)
  , _brightness(1.0f)
  , _changed()
  , _isVisible(false)
  , _stack(stack)
{
}

Store::Store(const Store& copy)
  : _label(copy._label)
  , changed_function(false)
  , _opacity(copy._opacity)
  , _brightness(copy._brightness)
  , _changed()
  , _isVisible(copy._isVisible)
  , _stack(copy._stack)
{
}

Store::~Store() {
  _data.clear();
}

void Store::allocate()
{
  Point3u size = _stack->size();
  size_t s = size_t(size.x()) * size.y() * size.z();

  _data.resize(s);
}

void Store::reset()
{
  allocate();
  setFile();
  memset(_data.data(), 0, _data.size() * sizeof(ushort));
}

void Store::copyMetaData(const Store* other)
{
  if(other != this) {
    setFile(other->file());
    setLabels(other->labels());
  }
}

void Store::setFile(const QString& file)
{
  if(file.isEmpty()) {
    _filename = file;
  } else {
    _filename = util::absoluteFilePath(file);
  }
}

void Store::resetModified()
{
  changed_function = false;
  _changed = BoundingBox3i();
}

void Store::setStack(Stack* s) {
  _stack = s;
}

void Store::changed() {
  _changed = _stack->boundingBox();
}

void swapMetaData(Store* s1, Store* s2)
{
  if(s1 == s2)
    return;
  // Labels
  bool l1 = s1->labels();
  s1->setLabels(s2->labels());
  s2->setLabels(l1);

  // Files
  QString f1 = s1->file();
  s1->setFile(s2->file());
  s2->setFile(f1);
}
} // namespace lgx
