/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef CUT_SURF_H
#define CUT_SURF_H

#include <LGXConfig.hpp>
#include <GL.hpp>

#include <Color.hpp>
#include <CuttingSurface.hpp>
#include <Geometry.hpp>
#include <LGXViewer/qglviewer.h>
#include <Parms.hpp>

namespace lgx {
typedef util::Color<float> Color4f;
class Shader;
class ImgData;
class Stack;

typedef util::Vector<2, int> Point2i;
typedef util::Vector<3, float> Point3f;

// Class to handle rotatable cutting plane.
class LGX_EXPORT CutSurf : public QObject {
  Q_OBJECT
public:
  CutSurf();
  ~CutSurf();

  CuttingSurface* cut;

  std::set<uint> selectV;   // List of selected vertices

protected:
  uint BlendSlices;
  uint Material;
  float LineWidth;

  void drawSurface(ImgData& stk, bool select);

public:
  // Read clipping plane parameters
  void readParms(util::Parms& parms, QString section);

  // write parms to file
  void writeParms(QTextStream& out, QString section);

  // Draw cutting plane
  void drawCutSurf(ImgData& stk, bool select, Shader* shader = 0);
  void drawCutSurfGrid(ImgData& stk);
  void drawSelect();

  // Get frame
  qglviewer::ManipulatedFrame& getFrame() {
    return cut->frame();
  }

  // Clear selection
  void clearSelect();

  // Find the spline point selected
  int findSelectPoint(uint x, uint y);

  // Return true if the cutting surface is opaque if drawn for this stack
  bool showOpaqueSurface(const ImgData& stk);

  // Return true if the cutting surface is transparent if drawn for this stack
  bool showTransparentSurface(const ImgData& stk);

  float getSceneRadius() {
    return SceneRadius;
  };

protected:
  void initBez();
  double SceneRadius;
  float getSize(int val);

public slots:
  // Set sizes
  void setVisible(bool val);
  void selectThreeAxis(bool val);
  void selectPlane(bool val);
  void selectBezier(bool val);
  void showGrid(bool val);
  void setSizeX(int val);
  void setSizeY(int val);
  void setSizeZ(int val);
  void reset(double sceneRadius);
  void setSceneBoundingBox(const Point3f& bbox);
  void setBrightness(int val);
  void setOpacity(int val);
  void setBlend(bool val);
  void showPoints(bool val);

signals:
  void modified();
};
} // namespace lgx
#endif /*CUTTING_SURFACE_H*/
