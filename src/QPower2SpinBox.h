#ifndef QPOWER2SPINBOX_H
#define QPOWER2SPINBOX_H

#include <QSpinBox>

class QPower2SpinBox : public QSpinBox
{
public:
  QPower2SpinBox(QWidget *parent);

  void stepBy(int i) override;
  QValidator::State validate(QString& input, int& pos) const override;

protected:
  void fixup(QString& text) const override;
};

#endif // QPOWER2SPINBOX_H

