/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Misc.hpp"

#include <QTextStream>
#include <QCoreApplication>
#include <QString>
#include <QDir>
#include <QStandardPaths>
#include <iostream>
#include <QRegExp>
#include <QFileInfo>
#include <limits>
#include "Information.hpp"
#include "Dir.hpp"
//#include <QNetworkRequest>
//#include <QNetworkManager>

#ifndef WIN32
#  include <unistd.h>
#  include <errno.h>
#endif

using namespace std;

namespace lgx {
namespace util {

QDir resourcesDir()
{
  QDir dir = QCoreApplication::applicationDirPath();
  if(dir.cd("..") and
     dir.cd("share") and
     dir.cd("LithoGraphX"))
    return dir;
  else
  {
    dir = QCoreApplication::applicationDirPath();
    Information::err << "Warning, no system resources folder! Should located at: '"
                     << dir.absolutePath()
                     << "/../share/LithoGraphX'.\nTrying to use standard folders." << endl;
  }
  for(QString folder: QStandardPaths::standardLocations(QStandardPaths::DataLocation)) {
    dir = QDir(folder);
    if(dir.exists())
      return dir;
  }
  Information::err << "No standard folder found, fall back to ~/.LithoGraphX" << endl;
  return resolvePath("~/.LithoGraphX");
}


QDir configurationDir(bool create)
{
  static const QString default_path = resolvePath("~/.LithoGraphX");
  QStringList dataPaths = QStandardPaths::standardLocations(QStandardPaths::DataLocation);
  dataPaths << default_path;
  for(const auto& d: dataPaths) {
      DEBUG_OUTPUT("Trying configuration folder '" << d << "'" << endl);
      if(QFileInfo(d).isDir() or (create and createPath(d)))
          return d;
  }
  return QDir(dataPaths[0]);
}

QDir userProcessesDir(bool create)
{
  QDir dir = configurationDir(create);
  if(not dir.cd("processes")) {
    dir.mkdir("processes");
    dir.cd("processes");
  }
  return dir;
}

QDir userMacroDir(bool create)
{
  QDir dir = configurationDir(create);
  if(not dir.cd("macros")) {
    dir.mkdir("macros");
    dir.cd("macros");
  }
  return dir;
}

QList<QDir> processesDirs()
{
  QList<QDir> result;
  QDir dir = resourcesDir();
  if(!dir.cd("processes"))
    dir = QDir(".");
  result << dir << userProcessesDir();
  return result;
}

QList<QDir> macroDirs()
{
  QList<QDir> result;
  QDir dir = resourcesDir();
  if(dir.cd("macros"))
    result << dir;
  else
    Information::err << "Warning, no system macro folder! Should located at: '"
                     << dir.absolutePath() << "/macros'" << endl;
  result << userMacroDir();
  return result;
}

QDir includesDir()
{
  QDir dir = QCoreApplication::applicationDirPath();
  if(!dir.cd(".."))
    dir = QDir(".");
  else if(!dir.cd("include"))
    dir = QDir(".");
  else if(!dir.cd("LithoGraphX"))
    dir = QDir(".");
  return dir;
}

QDir libsDir()
{
  QDir dir = QCoreApplication::applicationDirPath();
  if(!dir.cd(".."))
    dir = QDir(".");
  else if(!dir.cd("lib"))
    dir = QDir(".");
  return dir;
}

QDir docsDir()
{
  QDir dir = QCoreApplication::applicationDirPath();
  if(!dir.cd(".."))
    dir = QDir(".");
  else if(!dir.cd("share"))
    dir = QDir(".");
  else if(!dir.cd("doc"))
    dir = QDir(".");
  else if(!dir.cd("LithoGraphX"))
    dir = QDir(".");
  return dir;
}

Point3f stringToPoint3f(QString s)
{
  QStringList fields = s.split(" ");
  bool ok;
  if(fields.size() == 1) {
    float val = fields[0].toFloat(&ok);
    return Point3f(val);
  } else if(fields.size() == 3) {
    float vx, vy, vz;
    vx = fields[0].toFloat(&ok);
    if(ok)
      vy = fields[1].toFloat(&ok);
    if(ok)
      vz = fields[2].toFloat(&ok);
    if(ok)
      return Point3f(vx, vy, vz);
  }
  return Point3f(std::numeric_limits<float>::quiet_NaN());
}

} // namespace util
} // namespace lgx
