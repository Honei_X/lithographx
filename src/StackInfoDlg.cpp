/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

/*
 * This file is part of LithoGraphX
 *
 * LithoGraphX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LithoGraphX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <StackInfoDlg.hpp>
#include <Stack.hpp>
#include <Mesh.hpp>
#include <QTreeWidgetItem>
#include <ImageData.hpp>
#include <QAbstractButton>

namespace lgx {
namespace gui {
StackInfoDlg::StackInfoDlg(ImgData* d, QWidget* parent, Qt::WindowFlags f)
  : QDialog(parent, f)
  , data(d)
{
  ui.setupUi(this);
  setAttribute(Qt::WA_DeleteOnClose);
  updateData();
}

void StackInfoDlg::updateData()
{
  setWindowTitle(QString("Information for Stack %1").arg(data->stack->userId()));

  updateStack();
  updateMesh();
  updateOther();
}

void StackInfoDlg::updateStack()
{
  Stack* stack = data->stack;
  ui.StackInfo->clear();

  QString value = QString("%1x%2x%3").arg(stack->size().x()).arg(stack->size().y()).arg(stack->size().z());
  ui.StackInfo->addTopLevelItem(new QTreeWidgetItem(QStringList() << "Size" << value));

  value = QString("%1x%2x%3").arg(stack->step().x()).arg(stack->step().y()).arg(stack->step().z());
  ui.StackInfo->addTopLevelItem(new QTreeWidgetItem(QStringList() << "Voxel size" << value));

  value = QString("%1x%2x%3").arg(stack->origin().x()).arg(stack->origin().y()).arg(stack->origin().z());
  ui.StackInfo->addTopLevelItem(new QTreeWidgetItem(QStringList() << "origin" << value));
}

void StackInfoDlg::updateMesh()
{
  Mesh* mesh = data->mesh;

  ui.MeshInfo->clear();

  QString value = QString::number(data->VCount);
  ui.MeshInfo->addTopLevelItem(new QTreeWidgetItem(QStringList() << "# Vertices" << value));

  value = QString::number(data->LCount);
  ui.MeshInfo->addTopLevelItem(new QTreeWidgetItem(QStringList() << "# Lines" << value));

  value = QString::number(data->TCount);
  ui.MeshInfo->addTopLevelItem(new QTreeWidgetItem(QStringList() << "# Triangles" << value));

  value = QString::number(data->selectV.size());
  ui.MeshInfo->addTopLevelItem(new QTreeWidgetItem(QStringList() << "Selection" << value));

  {
    QTreeWidgetItem* item = new QTreeWidgetItem(QStringList() << "Signal");
    const Description& signalDesc = mesh->signalDesc();
    new QTreeWidgetItem(item, QStringList() << "Type" << signalDesc.type());
    const Point2f& bounds = mesh->signalBounds();
    value = QString("%1 %3 -- %2 %3").arg(bounds[0]).arg(bounds[1]).arg(mesh->signalUnit());
    new QTreeWidgetItem(item, QStringList() << "Bounds" << value);
    forall(Description::const_reference p, signalDesc)
      new QTreeWidgetItem(item, QStringList() << p.first << p.second);
    ui.MeshInfo->addTopLevelItem(item);
    item->setExpanded(true);
  }

  if(not mesh->labelHeat().empty()) {
    QTreeWidgetItem* item = new QTreeWidgetItem(QStringList() << "Heat map");
    const Description& labelHeat = mesh->heatMapDesc();
    new QTreeWidgetItem(item, QStringList() << "Type" << labelHeat.type());
    value = QString::number(mesh->labelHeat().size());
    new QTreeWidgetItem(item, QStringList() << "Size" << value);
    new QTreeWidgetItem(item, QStringList() << "Unit" << mesh->heatMapUnit());
    value = QString("%1 %3- %2 %3").arg(mesh->heatMapBounds()[0]).arg(mesh->heatMapBounds()[1]).arg(
        mesh->heatMapUnit());
    new QTreeWidgetItem(item, QStringList() << "Bounds" << value);
    forall(Description::const_reference p, labelHeat)
      new QTreeWidgetItem(item, QStringList() << p.first << p.second);
    ui.MeshInfo->addTopLevelItem(item);
    item->setExpanded(true);
  }

  if(not mesh->wallHeat().empty()) {
    QTreeWidgetItem* item = new QTreeWidgetItem(QStringList() << "Wall map");
    const Description& labelHeat = mesh->heatMapDesc();
    new QTreeWidgetItem(item, QStringList() << "Type" << labelHeat.type());
    value = QString::number(mesh->wallHeat().size());
    new QTreeWidgetItem(item, QStringList() << "Size" << value);
    new QTreeWidgetItem(item, QStringList() << "Unit" << mesh->heatMapUnit());
    value = QString("%1 %3- %2 %3").arg(mesh->heatMapBounds()[0]).arg(mesh->heatMapBounds()[1]).arg(
        mesh->heatMapUnit());
    new QTreeWidgetItem(item, QStringList() << "Bounds" << value);
    forall(Description::const_reference p, labelHeat)
      new QTreeWidgetItem(item, QStringList() << p.first << p.second);
    ui.MeshInfo->addTopLevelItem(item);
    item->setExpanded(true);
  }

  if(not mesh->cellAxis().empty()) {
    QTreeWidgetItem* item = new QTreeWidgetItem(QStringList() << "Cell Axis");
    const Description& cellAxis = mesh->cellAxisDesc();
    new QTreeWidgetItem(item, QStringList() << "Type" << cellAxis.type());
    QString unit = mesh->cellAxisUnit();
    value = (mesh->isParentAxis() ? "Yes" : "No");
    new QTreeWidgetItem(item, QStringList() << "On Parents" << value);

    forall(Description::const_reference p, cellAxis)
      new QTreeWidgetItem(item, QStringList() << p.first << p.second);

    new QTreeWidgetItem(item, QStringList() << "Unit" << unit);
    ui.MeshInfo->addTopLevelItem(item);
    item->setExpanded(true);
  }
}

void StackInfoDlg::updateOther() {
  ui.OtherInfo->clear();
}

StackInfoDlg::~StackInfoDlg() {
}

void StackInfoDlg::on_buttonBox_clicked(QAbstractButton* btn)
{
  switch(ui.buttonBox->standardButton(btn)) {
  case QDialogButtonBox::Reset:
    updateData();
    break;
  default:
    break;
  }
}

void StackInfoDlg::showEvent(QShowEvent* event)
{
  updateData();
  QDialog::showEvent(event);
}
} // namespace gui
} // namespace lgx
