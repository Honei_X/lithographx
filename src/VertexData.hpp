/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef VERTEXDATA_HPP
#define VERTEXDATA_HPP

/**
 * \file VertexData.hpp
 *
 * This file contains the definition of a vertex.
 */

#include <LGXConfig.hpp>

#include <Geometry.hpp>

namespace lgx {
/**
 * \class VertexData VertexData.hpp <VertexData.hpp>
 *
 * This class defines the content of a vertex. That is all properties available
 * directly from a vertex.
 */
class LGX_EXPORT VertexData {
public:
  Point3f pos;     ///< Position in space
  Point3f nrml;    ///< Normal vector
  Point2f txpos;   ///< Texture coordinates
  float tsignal;   ///< Temp signal
  float signal;    ///< Projected signal
  int label;       ///< Region to which vertex belongs
  char type;       ///< Type of vertex
  float area;      ///< Area of cell
  int saveId;      ///< Used when saving vertex
  int labcount;    ///< # of labels (cells) vertex connected to
  bool margin;     ///< Is vertex on the margin?
  bool inqueue;    ///< Is this in the queue? Used for segmentation
  bool selected;   ///< Is vertex selected?
  ulong minb;      ///< Closest border vertex

  // Constructor, set initial values
  VertexData()
    : pos(0, 0, 0)
    , nrml(0, 0, 0)
    , txpos(0, 0)
    , tsignal(0)
    , signal(1)
    , label(0)
    , type('j')
    , area(0)
    , saveId(0)
    , labcount(0)
    , margin(false)
    , inqueue(false)
    , selected(false)
    , minb(0)
  {
  }

  virtual ~VertexData() {
  }
};
} // namespace lgx

#endif /*VERTEX_DATA_H*/
