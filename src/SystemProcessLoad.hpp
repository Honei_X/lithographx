/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef SYSTEMPROCESSLOAD_HPP
#define SYSTEMPROCESSLOAD_HPP

#include <Process.hpp>

#include <QObject>
#include <Mesh.hpp>

#include <QDialog>

#include <memory>
#include <vector>

/**
 * \file SystemProcessLoad.hpp
 * This file contains processes defined in LithoGraphX to load data
 */

class QDialog;
class Ui_LoadStackDialog;
class Ui_LoadMeshDialog;
class Ui_ImportMeshDialog;
class Ui_TIFFOpenDlg;
class QFile;
class QIODevice;
class QPixmap;

namespace lgx {

struct ImageInfo;

namespace process {
LGX_EXPORT QList<int> extractVersion(QIODevice& file);

/**
 * \class StackSwapBytes SystemProcessLoad.hpp <SystemProcessLoad.hpp>
 *
 * Process that swap bytes
 *
 * \ingroup StackProcess
 */
class LGX_EXPORT StackSwapBytes : public StackProcess {
public:
  StackSwapBytes(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList&) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    bool res = (*this)(input, output);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output);

  QString name() const override {
    return "Swap Bytes";
  }
  QString description() const override {
    return "Swap the bytes of the values in the stack.";
  }
  QString folder() const override {
    return "Filters";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/SwapBytes.png");
  }
};

class LGX_EXPORT StackImportDlg : public QDialog {
  Q_OBJECT
public:
  StackImportDlg(QWidget *parent, Qt::WindowFlags f = 0);
  ~StackImportDlg();

  Point3f imageResolution();
  float brightness() const;
  QStringList imageFiles() const;

public slots:
  void setBrightness(float value);
  void clearImageFiles();

  void LoadProfile(QString filename = QString());
  void setImageSize(Point3u size);
  void setImageSize(uint x, uint y, uint z) {
    setImageSize(Point3u(x, y, z));
  }
  void setImageResolution(Point3f step);
  void setImageResolution(float x, float y, float z) {
    setImageResolution(Point3f(x, y, z));
  }

  QString loadedFile() const { return _loadedFile; }

protected slots:
  void on_AddFiles_clicked();
  void on_RemoveFiles_clicked();
  void on_LoadProfile_clicked();
  void on_SaveProfile_clicked();
  void on_FilterFiles_clicked();
  void on_SortAscending_clicked();
  void on_SortDescending_clicked();
  void on_ImageFiles_filesDropped(const QStringList& files);

private:
  std::unique_ptr<Ui_LoadStackDialog> ui;
  QString _loadedFile;
  Point3u imageSize;
};

/**
 * \class StackImport SystemProcessLoad.hpp <SystemProcessLoad.hpp>
 *
 * Import images forming a stack.
 *
 * \ingroup StackProcess
 */
class LGX_EXPORT StackImport : public StackProcess {
public:
  StackImport(const StackProcess& proc)
    : Process(proc)
    , StackProcess(proc)
  {
  }

  bool initialize(QStringList& parms, QWidget* parent) override;

  bool operator()(const QStringList& parms) override
  {
    int stackId = parms[0].toInt();
    QString storeName = parms[1];
    Point3f step(parms[2].toFloat(), parms[3].toFloat(), parms[4].toFloat());
    float brightness(parms[5].toFloat());
    QString filename(parms[6]);

    // Get stack and store
    Stack* stk = currentStack();
    if(stackId != -1)
      stk = stack(parms[0].toInt());
    if(!checkState().stack(STACK_ANY, stackId))
      return false;

    Store* store = stk->currentStore();
    if(storeName == "Main")
      store = stk->main();
    else if(storeName == "Work")
      store = stk->work();

    return (*this)(stk, store, step, brightness, filename);
  }
  /**
   * Import the stack
   * \param stack Stack that will contain the image
   * \param store Store that will contain the image
   * \param filenames Either a single text file containing all the
   * parameters, or a list of image files, one per z slice
   * \param brightness Multiplication coefficient to convert less than 16
   * bits images to 16 bits
   * \param step Size of a voxel
   */
  bool operator()(Stack* stack, Store* store, Point3f step, float brightness, QString filename);

  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Import";
  }
  QString description() const override {
    return "Import stack from a series of images";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Stack"
                         << "Store"
                         << "X Step"
                         << "Y Step"
                         << "Z Step"
                         << "Brightness"
                         << "Profile File";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Stack"
                         << "Store"
                         << "X Step"
                         << "Y Step"
                         << "Z Step"
                         << "Brightness"
                         << "Profile File";
  }
  QStringList parmDefaults() const override
  {
    return QStringList() << "-1"
                         << "Current"
                         << "1.0"
                         << "1.0"
                         << "1.0"
                         << "1.0"
                         << "";
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = storeChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/open.png");
  }

  static bool LoadProfile(QString filename, Point3u& size, Point3f& step, float& brightness, QStringList& files);
};

class LGX_EXPORT TIFFOpenDlg : public QDialog {
  Q_OBJECT
public:
  TIFFOpenDlg(const ImageInfo& info, QWidget* p = 0, Qt::WindowFlags f = 0);
  ~TIFFOpenDlg();

  int channel() const;
  int timepoint() const;
  void setChannel(int value);
  void setTimepoint(int value);

  bool valid() const { return nb_channels * nb_timepoints > 0; }

protected slots:
  void on_channel_valueChanged(int);
  void on_timePoint_valueChanged(int);

private:
  std::unique_ptr<Ui_TIFFOpenDlg> ui;
  size_t nb_channels, nb_timepoints;
  std::vector<QPixmap> samples;
};

/**
 * \class StackOpen SystemProcessLoad.hpp <SystemProcessLoad.hpp>
 *
 * Open a stack in either the MGXS or INRIA formats.
 *
 * \ingroup StackProcess
 */
class LGX_EXPORT StackOpen : public StackProcess {
public:
  StackOpen(const StackProcess& proc)
    : Process(proc)
    , StackProcess(proc)
  {
  }

  bool initialize(QStringList& parms, QWidget* parent) override;
  bool operator()(const QStringList& parms) override;
  /**
   * Open the image
   * \param stack Stack that will contain the image
   * \param store Store that will contain the image
   * \param filename File containing the image.
   */
  bool operator()(Stack* stack, Store* store, QString filename);
  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Open";
  }
  QString description() const override {
    return "Open a stack from a known 3D image format";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Filename"
                         << "Store"
                         << "Stack number"
                         << "Choose File";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Filename"
                         << "Store"
                         << "Stack number"
                         << "If false and a filename is provided, it won't show the dialog box to choose a file";
  }
  QStringList parmDefaults() const override
  {
    return QStringList() << ""
                         << "Main"
                         << "0"
                         << "True";
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = storeChoice();
    map[5] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/open.png");
  }
protected:
  bool loadMGXS_1_3(QIODevice& file, Stack* stack, Store* store);
  bool loadMGXS_1_2(QIODevice& file, Stack* stack, Store* store);
  bool loadMGXS_1_1(QIODevice& file, Stack* stack, Store* store);
  bool loadMGXS_1_0(QIODevice& file, Stack* stack, Store* store);
  bool loadMGXS_0(QIODevice& file, Stack* stack, Store* store);
  bool parseFilename(QString& filename, size_t& channel, size_t& timepoint);
  void centerImage();
};

/**
 * \class MeshLoad SystemProcessLoad.hpp <SystemProcessLoad.hpp>
 *
 * Load a MGXM mesh file.
 *
 * \ingroup MeshProcess
 */
class LGX_EXPORT MeshLoad : public QObject, public MeshProcess {
  Q_OBJECT
public:
  MeshLoad(const MeshProcess& proc)
    : Process(proc)
    , QObject()
    , MeshProcess(proc)
  {
  }

  bool initialize(QStringList& parms, QWidget* parent) override;
  bool operator()(const QStringList& parms) override;
  /**
   * Load a MGXM file
   * \param mesh Mesh that will contain the data
   * \param filename File containing the mesh.
   * \param scale If true, the mesh will be scaled according to the current
   * scale factor
   * \param transform If true, the mesh will be transformed according to
   * the current transformation frame
   * \param add If true, the mesh will be added to the current one instead
   * of replacing it.
   */
  bool operator()(Mesh* mesh, QString filename, bool transform, bool add);
  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Load";
  }
  QString description() const override {
    return "Load a mesh from one of the known formats.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Filename"
                         << "Transform"
                         << "Add"
                         << "Stack number"
                         << "Choose file";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Filename"
                         << "Transform"
                         << "Add"
                         << "Stack number"
                         << "If true, or if filename is empty, display a dialog box on initialization";
  }
  QStringList parmDefaults() const override
  {
    return QStringList() << ""
                         << "No"
                         << "No"
                         << "0"
                         << "Yes";
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = map[2] = map[4] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/open.png");
  }

  bool loadMGXM_0(QIODevice& file, Mesh* mesh, bool scale, bool transform, bool add, bool has_color = true);
  bool loadMGXM_1_0(QIODevice& file, Mesh* mesh, bool& scale, bool& transform, bool add, bool has_color = true);
  bool loadMGXM_1_1(QIODevice& file, Mesh* mesh, bool& scale, bool& transform, bool add, bool has_color = true);
  bool loadMGXM_1_2(QIODevice& file, Mesh* mesh, bool& scale, bool& transform, bool add, bool has_color = true);
  bool loadMGXM_1_3(QIODevice& file, Mesh* mesh, bool& scale, bool& transform, bool add);

  void findSignalBounds(Mesh* mesh);

protected slots:
  void selectMeshFile();

protected:
  void setMeshFile(const QString& filename);

  QDialog* dlg;
  Ui_LoadMeshDialog* ui;
};

/**
 * \class MeshImport SystemProcessLoad.hpp <SystemProcessLoad.hpp>
 *
 * Import a mesh from a file of another format
 *
 * \ingroup MeshProcess
 */
class LGX_EXPORT MeshImport : public QObject, public MeshProcess {
  Q_OBJECT

public:
  MeshImport(const MeshProcess& proc)
    : Process(proc)
    , QObject()
    , MeshProcess(proc)
  {
  }

  bool initialize(QStringList& parms, QWidget* parent) override;
  bool operator()(const QStringList& parms) override;
  /**
   * Import a mesh
   * \param mesh Mesh that will contain the data
   * \param filename File describing the mesh
   * \param type Type of the file. Must be one of 'Text', 'Cells', 'Keyence' or 'MeshEdit', or empty (i.e.
   * auto-detect)
   * \param scale If true, the mesh will be scaled
   * \param transform If true, the mesh will be transformed
   * \param add If true, the mesh won't be cleared before loading the file
   *
   * \sa MeshOpen
   */
  bool operator()(Mesh* mesh, QString filename, QString type, bool scale, bool transform, bool add);
  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Import";
  }
  QString description() const override {
    return "Import a mesh from one of the known formats.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Filename"
                         << "Kind"
                         << "Scale"
                         << "Transform"
                         << "Add"
                         << "Stack number"
                         << "Choose file";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Filename"
                         << "Kind of mesh."
                         << "Scale"
                         << "Transform"
                         << "Add"
                         << "Stack number"
                         << "If true or filename is empty, shows a dialog box to choose the file to load.";
  }
  QStringList parmDefaults() const override
  {
    return QStringList() << ""
                         << "PLY"
                         << "yes"
                         << "no"
                         << "no"
                         << "0"
                         << "Yes";
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = QStringList() << "PLY"
                           << "VTK Mesh"
                           << "Text"
                           << "Cells"
                           << "Keyence"
                           << "MeshEdit"
                           << "OBJ"
                           << "From extension";
    map[2] = map[3] = map[4] = map[6] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/open.png");
  }

protected slots:
  void selectMeshFile();
  void selectMeshType(const QString& type);

protected:
  void setMeshFile(const QString& filename, const QString& type = QString());
  QString properFile(QString filename, const QString& type) const;
  QString typeToExt(QString type) const;
  QString extToType(QString extension) const;
  int extToTypeId(QString extension) const;
  int typeToTypeId(QString extension) const;

  bool loadText(Mesh* mesh, const QString& filename, bool scale, bool transform, bool add);
  bool loadCells(Mesh* mesh, const QString& filename, bool scale, bool transform, bool add);
  bool loadKeyence(Mesh* mesh, const QString& filename, bool scale, bool transform, bool add);
  bool loadMeshEdit(Mesh* mesh, const QString& filename, bool scale, bool transform, bool add);
  bool loadMeshVTK(Mesh* mesh, const QString& filename, bool& scale, bool transform, bool add);
  bool loadMeshOBJ(Mesh* mesh, const QString& filename, bool scale, bool transform, bool add);
  bool loadMeshPLY(Mesh* mesh, const QString& filename, bool scale, bool transform, bool add);

  QDialog* dlg;
  Ui_ImportMeshDialog* ui;
};

/**
 * \class LoadAllData SystemProcessLoad.hpp <SystemProcessLoad.hpp>
 *
 * Load all the files as specified in the various objects (filenames).
 *
 * Note that currently, it will load only the main store of the stacks as
 * only they can have any data.
 *
 * \ingroup GlobalProcess
 */
class LGX_EXPORT LoadAllData : public GlobalProcess {
public:
  LoadAllData(const GlobalProcess& proc)
    : Process(proc)
    , GlobalProcess(proc)
  {
  }

  bool operator()(const QStringList&) override;
  bool operator()();

  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Load All";
  }
  QString description() const override
  {
    return "Load the data for all existing objects, using the filename and properties set in them.";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/open.png");
  }

protected:
  bool loadStore(Stack* stack, Store* store, QStringList& errors);
};

/**
 * \class LoadProjectFile SystemProcessLoad.hpp <SystemProcessLoad.hpp>
 * Load a view file and all the associated files.
 *
 * \ingroup GlobalProcess
 */
class LGX_EXPORT LoadProjectFile : public GlobalProcess {
public:
  LoadProjectFile(const GlobalProcess& proc)
    : Process(proc)
    , GlobalProcess(proc)
  {
  }

  bool operator()(const QStringList& parms) override;
  /**
   * Load the file \c filename
   */
  bool operator()(QString filename);

  bool initialize(QStringList& parms, QWidget* parent) override;

  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Load Project";
  }
  QString description() const override
  {
    return "Load a project file and set all the fields and interface. Does not load the data though.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Filename"
                         << "Choose File";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Filename"
                         << "If false and a filename is given, the dialog box to choose a file won't be shown.";
  }
  QStringList parmDefaults() const override {
    return QStringList() << ""
                         << "True";
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/open.png");
  }
};

/**
 * \class ResetMeshProcess SystemProcessLoad.hpp <SystemProcessLoad.hpp>
 *
 * Reset a mesh
 *
 * \ingroup MeshProcess
 */
class LGX_EXPORT ResetMeshProcess : public MeshProcess {
public:
  ResetMeshProcess(const MeshProcess& proc)
    : Process(proc)
    , MeshProcess(proc)
  {
  }

  bool operator()(const QStringList& parms) override;
  bool operator()(Mesh* m);

  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Reset";
  }
  QString description() const override {
    return "Reset a mesh, -1 for current.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Mesh";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Mesh";
  }
  QStringList parmDefaults() const override {
    return QStringList() << "-1";
  }
  QIcon icon() const override {
    return QIcon(":/images/ClearStack.png");
  }
};
} // namespace process
} // namespace lgx

#endif
