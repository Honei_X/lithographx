/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "CameraPath.hpp"

#include "LithoViewer.hpp"

using lgx::Point3f;
using lgx::Point4f;
using lgx::Quaternion;

CameraPath::CameraPath(QObject* parent)
  : QObject(parent)
  , timer(new QTimer(this))
{
  connect(timer, SIGNAL(timeout()), this, SLOT(nextFrame()));
  timer->setInterval(100);
  timer->setSingleShot(false);
  connect(this, SIGNAL(endPath()), timer, SLOT(stop()));
}

CameraPath::~CameraPath()
{
  if(timer)
    delete timer;
  timer = 0;
}

void CameraPath::addFrame(LGXCamera* camera, float time)
{
  //LGXCameraFrame* frame = camera->frame();
  frames << new ToPosition(Point3f(camera->sceneCenter()), Point3f(camera->position()),
                           Point3f(camera->viewDirection()), camera->zoom(), time, this);
}

void CameraPath::addRotation(const Point3f& axis, double angle, float time)
{
  //LGXCameraFrame* frame = camera->frame();
  frames << new Rotation(Point3f(camera->sceneCenter()), axis, angle, time, this);
}

void CameraPath::animatePath(LGXCamera* camera, float dt)
{
  if(frames.empty())
    return;
  current_time = 0;
  this->dt = dt;
  this->camera = camera;
  current_index = 0;
  frames[0]->startInterpolation(camera);
  timer->start();
}

void CameraPath::nextFrame()
{
  if(current_time > 1) {
    if(current_index == frames.size() - 1) {
      frames[current_index]->interpolateFrame(camera, 1.0f);
      emit frameUpdated();
      emit endPath();
      return;
    }
    current_index++;
    frames[current_index]->startInterpolation(camera);
    current_time -= 1.0f;
  }
  frames[current_index]->interpolateFrame(camera, current_time);
  emit frameUpdated();
}

bool CameraPath::ToPosition::startInterpolation(LGXCamera* ) {
  return true;
}

bool CameraPath::ToPosition::interpolateFrame(LGXCamera*, float ) {
  return true;
}
