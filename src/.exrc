set shiftwidth=2
set softtabstop=2
set makeprg=make\ -C\ ../Build
vmap <Leader>ff m`:!../format.sh<CR>``zz
nmap <Leader>ff m`:%!../format.sh<CR>``zz
vmap <Leader>fu m`:!../format.sh -u<CR>``zz
nmap <Leader>fu m`:%!../format.sh -u<CR>``zz
