/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

//
// polygonizer.h
//
// This is Jules Bloomenthal's implicit surface polygonizer from GRAPHICS GEMS IV.
// Converted to C++ by J. Andreas Berentzen 2003.
// Adapted for use in LithoGraphX Richard S. Smith 2010
//
#ifndef POLYGONIZER_H
#define POLYGONIZER_H

#include <LGXConfig.hpp>

#include <Geometry.hpp>
#include <Progress.hpp>

#include <set>
#include <vector>

namespace lgx {
namespace util {
//
// The implicit function class represents the implicit function we wish
// to polygonize. Derive a class from this one and return true if the point
// is inside the object at the point passed in.
//
class LGX_EXPORT ImplicitFunction {
public:
  virtual bool eval(Point3f) = 0;
};


// Polygonizer is the class used to perform polygonization.
class LGX_EXPORT Polygonizer {
  class Process;
  Process* process;

public:
  // Constructor of Polygonizer.
  // Arguments:
  // 1. The ImplicitFunction defining the surface(s)
  // 2. The cube size to scan the space.
  // 3. The size of the voxels.
  // 4. Standard vector to put vertices.
  // 5. Standard vector to put triangles.
  Polygonizer(ImplicitFunction& _func, float _cubeSize, Point3f _voxSize, std::vector<Point3f>& vertices,
              std::vector<Point3i>& triangles);
  ~Polygonizer();

  // March adds to the vertex and triangle lists.
  // Arguments:
  // 1. Bounding box of the space to explore.
  void march(Point3f* bBox);
};
} // namespace util
} // namespace lgx
#endif
