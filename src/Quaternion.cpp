/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Quaternion.hpp"
#include <cmath>

#ifdef EPSILON
#  undef EPSILON
#endif
#define EPSILON 1e-6

namespace lgx {
namespace util {
Quaternion::Quaternion(const Point3f& from, const Point3f& to)
  : Point4f(0, 0, 0, 1)
{
  const float fromSqNorm = lgx::util::normsq(from);
  const float toSqNorm = lgx::util::normsq(to);
  // Identity Quaternion when one vector is null
  if((fromSqNorm < EPSILON) || (toSqNorm < EPSILON)) {
    return;
  } else {
    Point3f axis = from ^ to;
    const float axisSqNorm = lgx::util::normsq(axis);

    // Aligned vectors, pick any axis, not aligned with from or to
    if(axisSqNorm < EPSILON)
      axis = orthogonal(from);

    float angle = std::asin(std::sqrt(axisSqNorm / (fromSqNorm * toSqNorm)));

    if(from * to < 0.0)
      angle = M_PI - angle;

    setAxisAngle(axis, angle);
  }
}

Quaternion& Quaternion::operator=(const Quaternion& other)
{
  for(size_t i = 0; i < 4; ++i)
    elems[i] = other.elems[i];
  return *this;
}

Quaternion Quaternion::operator*(const Quaternion& other) const
{
  float w_ = w() * other.w() - x() * other.x() - y() * other.y() - z() * other.z();
  float x_ = w() * other.x() + other.w() * x() + y() * other.z() - z() * other.y();
  float y_ = w() * other.y() + other.w() * y() + z() * other.x() - x() * other.z();
  float z_ = w() * other.z() + other.w() * z() + x() * other.y() - y() * other.x();
  return Quaternion(x_, y_, z_, w_);
}

Quaternion& Quaternion::operator*=(const Quaternion& other)
{
  Quaternion res = *this * other;
  *this = res;
  return *this;
}

Quaternion::Quaternion(const Point3f& axis, float angle)
  : Point4f()
{
  setAxisAngle(axis, angle);
}

void Quaternion::setAxisAngle(const Point3f& axis, float angle)
{
  const float norm = lgx::util::norm(axis);
  if(norm < EPSILON) {
    x() = 0;
    y() = 0;
    z() = 0;
    w() = 1;
  } else {
    const float sin_half_angle = std::sin(angle / 2.0);
    x() = sin_half_angle * axis.x() / norm;
    y() = sin_half_angle * axis.y() / norm;
    z() = sin_half_angle * axis.z() / norm;
    w() = std::cos(angle / 2.0);
  }
}

Quaternion::Quaternion(const Matrix3f& m)
{
  // Compute one plus the trace of the matrix
  const float onePlusTrace = 1.0 + m.trace();
  if(onePlusTrace > EPSILON) {
    const float s = std::sqrt(onePlusTrace) * 2.0;
    x() = (m(2, 1) - m(1, 2)) / s;
    y() = (m(0, 2) - m(2, 0)) / s;
    z() = (m(1, 0) - m(0, 1)) / s;
    w() = 0.25 * s;
  } else {
    // Computation depends on major diagonal term
    if(m(0, 0) > m(1, 1) and m(0, 0) > m(2, 2)) {
      const float s = std::sqrt(1 + m(0, 0) - m(1, 1) - m(2, 2)) * 2.0;
      x() = 0.25 * s;
      y() = (m(1, 0) + m(0, 1)) / s;
      z() = (m(2, 0) + m(0, 2)) / s;
      w() = (m(1, 2) - m(2, 1)) / s;
    } else if(m(1, 1) > m(2, 2)) {
      const float s = std::sqrt(1 + m(1, 1) - m(0, 0) - m(2, 2)) * 2.0;
      x() = (m(0, 1) + m(1, 0)) / s;
      y() = 0.25 * s;
      z() = (m(1, 2) + m(2, 1)) / s;
      w() = (m(0, 2) - m(2, 0)) / s;
    } else {
      const float s = std::sqrt(1 + m(2, 2) - m(0, 0) - m(1, 1)) * 2.0;
      x() = (m(0, 2) + m(0, 0)) / s;
      y() = (m(1, 2) + m(2, 1)) / s;
      z() = 0.25 * s;
      w() = (m(0, 1) - m(1, 0)) / s;
    }
  }
  normalize();
}

template <class matrix> static void setMatrixFromQuaternion(matrix& m, const Quaternion& q)
{
  const float xx = q.x() * q.x();
  const float xy = q.x() * q.y();
  const float xz = q.x() * q.z();
  const float yy = q.y() * q.y();
  const float yz = q.y() * q.z();
  const float zz = q.z() * q.z();

  const float wx = q.w() * q.x();
  const float wy = q.w() * q.y();
  const float wz = q.w() * q.z();

  m(0, 0) = 1 - 2 * (yy + zz);
  m(0, 1) = 2 * (xy - wz);
  m(0, 2) = 2 * (wy + xz);
  m(1, 0) = 2 * (xy + wz);
  m(1, 1) = 1 - 2 * (xx + zz);
  m(1, 2) = 2 * (yz - wx);
  m(2, 0) = 2 * (xz - wy);
  m(2, 1) = 2 * (yz + wx);
  m(2, 2) = 1 - 2 * (xx + yy);
}

void Quaternion::setMatrix(Matrix3f& m) const {
  setMatrixFromQuaternion(m, *this);
}

void Quaternion::setMatrix(Matrix4f& m) const
{
  setMatrixFromQuaternion(m, *this);
  for(size_t i = 0; i < 4; ++i) {
    m(i, 3) = 0.0;
    m(3, i) = 0.0;
  }
  m(3, 3) = 1.0;
}

Point3f Quaternion::axis() const
{
  Point3f res(x(), y(), z());
  const float sin = res.norm();
  if(sin > EPSILON)
    res /= sin;
  return (std::acos(w()) < M_PI / 2.0) ? res : -res;
}

float Quaternion::angle() const
{
  const float a = 2.0 * std::acos(w());
  return (a <= M_PI) ? a : 2.0 * M_PI - a;
}

Point3f Quaternion::rotate(const Point3f& v) const
{
  /*
     Quaternion q(v.x(), v.y(), v.z(), 0);
     Quaternion result = *this * q * conjugate();
     return Point3f(result.x(), result.y(), result.z());
   */

  const float xx = 2.0l * x() * x();
  const float yy = 2.0l * y() * y();
  const float zz = 2.0l * z() * z();
  const float xy = 2.0l * x() * y();
  const float xz = 2.0l * x() * z();
  const float yz = 2.0l * y() * z();

  const float wx = 2.0l * w() * x();
  const float wy = 2.0l * w() * y();
  const float wz = 2.0l * w() * z();

  return Point3f((1.0 - yy - zz) * v.x() + (xy - wz) * v.y() + (xz + wy) * v.z(),
                 (xy + wz) * v.x() + (1.0 - zz - xx) * v.y() + (yz - wx) * v.z(),
                 (xz - wy) * v.x() + (yz + wx) * v.y() + (1.0 - xx - yy) * v.z());
}

Point3f Quaternion::inverseRotate(const Point3f& v) const {
  return inverse().rotate(v);
}

Quaternion slerp(const Quaternion& q1, const Quaternion& q2, float t)
{
  float ca = static_cast<const Point4f&>(q1) * static_cast<const Point4f&>(q2);
  float a = std::acos(ca);
  if(a > M_PI / 2)
    a -= M_PI;
  return (q1 * std::sin(a * (1 - t)) + q2 * std::sin(a * t)) / std::sin(a);
}

} // namespace util
} // namespace lgx

namespace std {
lgx::util::Quaternion pow(const lgx::util::Quaternion& q, float p)
{
  float a = q.angle();
  lgx::Point3f v = q.axis();
  return lgx::util::Quaternion(v, a * p);
}
} // namespace std
