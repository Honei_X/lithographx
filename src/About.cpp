/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "About.hpp"
#include "LGXVersion.hpp"
#include <QThread>
#include <QString>

QString aboutText()
{
  return QString("<h2>LithoGraphX " VERSION "</h2>"
                 "A tool for 2.5 & 3D cell segmentation, image processing, and more!<br><br>"
                 "&#169; 2015 Barbier de Reuille, Pierre.<br><br>"
                 "LithoGraphX is licensed under the "
                 "<html><style type='text/css'></style><a href='http://www.gnu.org/licenses/gpl-3.0.html'>GNU "
                 "GPLv3.</a></html><br><br>"
                 /*
                  *"Please visit "
                  *"<html><style type='text/css'></style><a href='http://www.LithoGraphX.org'>LithoGraphX.org</a></html>"
                  *" for more information.<br><br>"
                  */
                 "LithoGraphX is a fork of the MorphoGraphX project and is based on the version 1.0 rev. 1256<br>"
                 "If you use LithoGraphX in your research, please "
                 "<a href='http://dx.doi.org/10.7554/eLife.05864'>reference</a>"
                 " us, including the LithoGraphX website: <a href='http://www.lithographx.org'>http://www.lithographx.org</a> .<br><br>"
                 "Developers:"
                 "<ul>"
                 "<li>Barbier de Reuille, Pierre</li>"
                 "</ul>"
                 "Acknowledgments:"
                 "<ul>"
                 "<li>Robinson, Sarah</li>"
                 "<li>Burian, Agata</li>"
                 "<li>Summers, Holly</li>"
                 "<li>Nakayama, Naomi</li>"
                 "<li>Lavenus, Julien</li>"
                 "</ul>"
                 "MorphoGraphX developers:"
                 "<ul>"
                 "<li>Weber, Alain</li>"
                 "<li>Schuepbach, Thierry</li>"
                 "<li>Toriello, Gerardo</li>"
                 "<li>Strauss, Soeren</li>"
                 "<li>Richard Smith's team</li>"
                 "</ul>"
                 "Funding support:<dl>"
                 "<ul>"
                 "<li> SystemsX.ch & the Swiss National Science Foundation</li>"
                 "<li> University of Bern, Switzerland </li>"
                 "</ul>");
}

QString introText()
{
  return QString("LithoGraphX version: " VERSION " revision: " REVISION "\n   created in thread: 0x%1")
         .arg(quintptr(QThread::currentThread()), -16);
}
