/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "GlobalProcess.hpp"
#include "MeshProcessCreation.hpp"
#include "Information.hpp"
#include "Progress.hpp"
#include <QFileDialog>

using qglviewer::Vec;

namespace lgx {
namespace process {
bool SetCurrentStack::operator()(STORE which, int id) {
  return setCurrentStack(id, which);
}
REGISTER_GLOBAL_PROCESS(SetCurrentStack);

bool SaveGlobalTransform::initialize(QStringList& parms, QWidget* parent)
{
  QString& filename = parms[0];
  if(filename.isEmpty())
    filename = QFileDialog::getSaveFileName(parent, "Choose transform file to save", QDir::currentPath(),
                                            "Text files (*.txt)");
  if(filename.isEmpty())
    return false;
  if(!filename.endsWith(".txt", Qt::CaseInsensitive))
    filename += ".txt";
  parms[0] = filename;
  return true;
}

bool SaveGlobalTransform::operator()(const QString& filename)
{
  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly)) {
    setErrorMessage(QString("File '%1' cannot be opened for writing").arg(filename));
    return false;
  }
  QTextStream out(&file);

  // Get difference between stacks
  qglviewer::Frame f1(stack(0)->getFrame().worldInverse()), f2;
  f2.setFromMatrix(stack(1)->getFrame().worldMatrix());
  f2.setReferenceFrame(&f1);

  // Write to file
  Matrix4d m(f2.worldMatrix(), util::GL_STYLE);
  out << m << endl;
  file.close();

  SETSTATUS(QString("Transform saved to: %1").arg(filename));
  return true;
}
REGISTER_GLOBAL_PROCESS(SaveGlobalTransform);

bool JoinRegionsSegment::operator()(Store* work, Mesh* mesh, float cubeSize)
{
  const Stack* stack = work->stack();
  HVecUS& data = work->data();

  // Get selected vertices, and find connected region
  vvgraph V(mesh->activeVertices());
  mesh->getConnectedVertices(V);

  // Find center of all cells (should be closed surfaces)
  typedef std::pair<int, int> IntIntPair;
  typedef std::pair<int, Point3f> IntPoint3fPair;
  std::map<int, Point3f> posMap;
  std::map<int, int> cntMap;
  int count = 0;
  while(!V.empty()) {
    vvgraph T;
    T.insert(*V.begin());
    mesh->getConnectedVertices(T);
    forall(const vertex& v, T) {
      posMap[count] += v->pos;
      cntMap[count]++;
      V.erase(v);
    }
    count++;
  }
  forall(const IntIntPair& p, cntMap)
    posMap[p.first] /= p.second;

  // Find stack label of center of regions and put in a set
  std::set<int> stackLabelSet;
  forall(const IntPoint3fPair& p, posMap) {
    Point3i imgPos = stack->worldToImagei(p.second);
    int stackLabel = data[stack->offset(imgPos)];
    if(stackLabel > 0)
      stackLabelSet.insert(data[stack->offset(imgPos)]);
  }

  // Update voxels
  int newLabel = *(stackLabelSet.begin());
  for(uint i = 0; i < data.size(); i++) {
    if(data[i] == 0)
      continue;
    if(stackLabelSet.count(data[i]) > 0)
      data[i] = newLabel;
  }

  // Also update mesh labels
  V = vvgraph(mesh->activeVertices());
  mesh->getConnectedVertices(V);
  forall(const vertex& v, V) {
    v->selected = false;
    v->label = newLabel;
  }

  // Now re-march the label
  if(cubeSize > 0) {
    MarchingCube3D mc(*this);
    mc(mesh, work, cubeSize, 1, 0, newLabel);
  }

  mesh->updateTriangles();
  mesh->updateSelection();
  work->changed();
  work->setLabels(true);
  return true;
}
REGISTER_GLOBAL_PROCESS(JoinRegionsSegment);
} // namespace process
} // namespace lgx
