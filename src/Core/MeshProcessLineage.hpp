/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MESH_PROCESS_LINEAGE_HPP
#define MESH_PROCESS_LINEAGE_HPP

#include <Process.hpp>

#include <Mesh.hpp>

namespace lgx {
namespace process {
///\addtogroup MeshProcess
///@{
/**
 * \class SaveParents MeshProcessLineage.hpp <MeshProcessLineage.hpp>
 *
 * Save mapping from labels to parents
 */
class LGXCORE_EXPORT SaveParents : public MeshProcess {
public:
  SaveParents(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool initialize(QStringList& parms, QWidget* parent);

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_ANY))
      return false;
    Mesh* m = currentMesh();
    return (*this)(m, parms[0]);
  }

  bool operator()(Mesh* mesh, const QString& filename);

  QString folder() const {
    return "Lineage Tracking";
  }
  QString name() const {
    return "Save Parents";
  }
  QString description() const {
    return "Save map of labels to parents labels to a file";
  }
  QStringList parmNames() const {
    return QStringList() << "Filename";
  }
  QStringList parmDescs() const {
    return QStringList() << "Path to spreadsheet file.";
  }
  QStringList parmDefaults() const {
    return QStringList() << "";
  }
  QIcon icon() const {
    return QIcon(":/images/ParentsSave.png");
  }
};

/**
 * \class LoadParents MeshProcessLineage.hpp <MeshProcessLineage.hpp>
 *
 * Load mapping from labels to parents
 */
class LGXCORE_EXPORT LoadParents : public MeshProcess {
public:
  LoadParents(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool initialize(QStringList& parms, QWidget* parent);

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_ANY))
      return false;
    Mesh* m = currentMesh();
    bool res = (*this)(m, parms[0]);
    if(res)
      m->showParent();
    return res;
  }

  bool operator()(Mesh* mesh, const QString& filename);

  QString folder() const {
    return "Lineage Tracking";
  }
  QString name() const {
    return "Load Parents";
  }
  QString description() const {
    return "Load map of labels to parents from a file";
  }
  QStringList parmNames() const {
    return QStringList() << "Filename";
  }
  QStringList parmDescs() const {
    return QStringList() << "Path to spreadsheet file.";
  }
  QStringList parmDefaults() const {
    return QStringList() << "";
  }
  QIcon icon() const {
    return QIcon(":/images/ParentsOpen.png");
  }
};

/**
 * \class ClearParents MeshProcessLineage.hpp <MeshProcessLineage.hpp>
 *
 * Clear label to parent mapping
 */
class LGXCORE_EXPORT ClearParents : public MeshProcess {
public:
  ClearParents(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& )
  {
    if(!checkState().mesh(MESH_ANY))
      return false;
    Mesh* m = currentMesh();
    return (*this)(m);
  }

  bool operator()(Mesh* mesh);

  QString folder() const {
    return "Lineage Tracking";
  }
  QString name() const {
    return "Clear Parents";
  }
  QString description() const {
    return "Clear mapping from parents to labels";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/ParentsClear.png");
  }
};

/**
 * \class CorrectParents MeshProcessLineage.hpp <MeshProcessLineage.hpp>
 *
 * Take out non-existing labels from parent map.
 */
class LGXCORE_EXPORT CorrectParents : public MeshProcess {
public:
  CorrectParents(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& )
  {
    if(!checkState().mesh(MESH_NON_EMPTY, 0).mesh(MESH_NON_EMPTY, 1))
      return false;
    Mesh* mesh1, *mesh2;
    if(currentMesh() == mesh(0)) {
      mesh2 = mesh(0);
      mesh1 = mesh(1);
    } else if(currentMesh() == mesh(1)) {
      mesh2 = mesh(1);
      mesh1 = mesh(0);
    } else
      return false;

    bool res = (*this)(mesh1, mesh2);
    return res;
  }

  bool operator()(Mesh* mesh1, Mesh* mesh2);

  QString folder() const {
    return "Lineage Tracking";
  }
  QString name() const {
    return "Correct Parents";
  }
  QString description() const {
    return "Take out non-existing labels from parent map.";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/ParentsCheck.png");
  }
};

/**
 * \class HeatMapDaughterCells MeshProcessLineage.hpp <MeshProcessLineage.hpp>
 *
 * Compute the heap map that shows how many daughter cells a parent cell has.
 */
class HeatMapDaughterCells : public MeshProcess {
public:
  HeatMapDaughterCells(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& )
  {
    if(!checkState().mesh(MESH_NON_EMPTY) or currentMesh()->toShow() != Mesh::PARENT) {
      setErrorMessage("The current mesh must have show parents selected.");
      return false;
    }
    Mesh* m = currentMesh();

    bool res = (*this)(m);
    if(res)
      m->showHeat();
    return res;
  }

  bool operator()(Mesh* m);

  QString folder() const {
    return "Lineage Tracking";
  }
  QString name() const {
    return "Heat Map Daughter Cells";
  }
  QString description() const {
    return "Compute the heat map that shows how may daughter cells a parent cell has.";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QStringList parmDefaults() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/HeatMapDaughterCells.png");
  }
};

/**
 * \class CopyParentsToLabels MeshProcessLineage.hpp <MeshProcessLineage.hpp>
 *
 * Copy parents to labels, and clear parent table.
 */
class CopyParentsToLabels : public MeshProcess {
public:
  CopyParentsToLabels(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& )
  {

    if(!checkState().mesh(MESH_NON_EMPTY) or currentMesh()->toShow() != Mesh::PARENT) {
      setErrorMessage("The current mesh must have show parents selected.");
      return false;
    }
    Mesh* m = currentMesh();

    bool res = (*this)(m);
    if(res)
      m->showLabel();
    return res;
  }

  bool operator()(Mesh* m);

  QString folder() const {
    return "Lineage Tracking";
  }
  QString name() const {
    return "Copy Parents to Labels";
  }
  QString description() const {
    return "Copy parents to labels, and clear parent table.";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QStringList parmDefaults() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/ParentsCopyToLabel.png");
  }
};
///@}
} // namespace process
} // namespace lgx

#endif
