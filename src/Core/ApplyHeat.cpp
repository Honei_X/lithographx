/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ApplyHeat.hpp"

#include <Mesh.hpp>

namespace lgx {
namespace process {
bool ApplyHeat::operator()(Mesh* mesh, const QString& heatUnit, float heat, float minHeat, float maxHeat)
{
  const vvgraph& vs = mesh->selectedVertices();

  // Rescale existing heat if required
  if(maxHeat - minHeat != 0) {
    Point2f oldBounds = mesh->heatMapBounds();
    if(oldBounds != Point2f(minHeat, maxHeat)) {
      forall(const IntFloatPair& p, mesh->labelHeat()) {
        float value = oldBounds.x() + p.second * (oldBounds.y() - oldBounds.x());
        mesh->labelHeat()[p.first] = (value - minHeat) / (maxHeat - minHeat);
      }
      mesh->heatMapBounds() = Point2f(minHeat, maxHeat);
    }
  }

  std::set<int> labels;
  forall(const vertex& v, vs)
    labels.insert(v->label);

  if(maxHeat - minHeat != 0)
    heat = (heat - minHeat) / (maxHeat - minHeat);

  IntFloatMap& labelHeat = mesh->labelHeat();
  forall(int label, labels)
    labelHeat[label] = heat;

  mesh->heatMapUnit() = heatUnit;
  mesh->showHeat();
  mesh->updateTriangles();

  return (true);
}
REGISTER_MESH_PROCESS(ApplyHeat);
}
}
