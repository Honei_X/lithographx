/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "StackProcessMorphology.hpp"

#include "Forall.hpp"
#include "Information.hpp"
#include "StackProcess.hpp"
#include "cuda/CudaExport.hpp"
#include "Image.hpp"
#include "CImg.h"
#include <set>

using namespace cimg_library;

namespace lgx {
namespace process {

typedef CImg<ushort> CImgUS;

bool EdgeDetectProcess::operator()(Store* input, Store* output, float threshold, float multiplier, float factor,
                                   uint fillValue)
{
  ushort lowthresh = trim(ushort(float(threshold) * factor), ushort(0), ushort(0xFFFF));
  ushort fill = trim(fillValue, uint(0), uint(0xFFFF));
  Point3i size = input->stack()->size();
  edgeDetectGPU(size, lowthresh, threshold, multiplier, fill, input->data(), output->data());

  output->copyMetaData(input);
  output->changed();
  return true;
}

REGISTER_STACK_PROCESS(EdgeDetectProcess);

bool DilateStack::operator()(Stack* stack, const Store* input, Store* output, uint xradius, uint yradius, uint zradius,
                             bool auto_resize)
{
  Point3i radius(xradius, yradius, zradius);
  if(auto_resize) {
    ResizeCanvas resize(*this);
    resize(stack, true, true, 2 * radius);
  }
  Point3u size = stack->size();
  if(dilateGPU(size, radius, input->data(), output->data())) {
    setErrorMessage("Error while running CUDA process");
    return false;
  }
  output->copyMetaData(input);
  output->changed();
  return true;
}

REGISTER_STACK_PROCESS(DilateStack);

bool ErodeStack::operator()(Stack* stack, const Store* input, Store* output, uint xradius, uint yradius, uint zradius,
                            bool auto_resize)
{
  Point3i radius(xradius, yradius, zradius);
  Point3u size = stack->size();
  if(erodeGPU(size, radius, input->data(), output->data(), input->labels())) {
    setErrorMessage("Error while running CUDA process");
    return false;
  }
  if(auto_resize) {
    ResizeCanvas resize(*this);
    resize(stack, true, true, -2 * radius);
  }
  output->changed();
  output->copyMetaData(input);
  return true;
}

REGISTER_STACK_PROCESS(ErodeStack);

bool OpeningStack::operator()(Stack* stack, const Store* input, Store* output, uint xradius, uint yradius, uint zradius)
{
  Point3i radius(xradius, yradius, zradius);
  Point3u size = stack->size();
  if(erodeGPU(size, radius, input->data(), output->data(), input->labels())) {
    setErrorMessage("Error while running CUDA process");
    return false;
  }
  if(dilateGPU(size, radius, output->data(), output->data())) {
    setErrorMessage("Error while running CUDA process");
    return false;
  }
  output->changed();
  output->copyMetaData(input);
  return true;
}

REGISTER_STACK_PROCESS(OpeningStack);

bool ClosingStack::operator()(Stack* stack, const Store* input, Store* output, uint xradius, uint yradius, uint zradius)
{
  Point3i radius(xradius, yradius, zradius);

  Point3u size = stack->size();

  const HVecUS& data = input->data();
  HVecUS& outputData = output->data();

  Point3u newSize = size + 2 * radius;

  {
    HVecUS resized = resize(data, size, newSize, true);
    outputData.swap(resized);
  }

  if(dilateGPU(newSize, radius, outputData, outputData)) {
    setErrorMessage("Error while running CUDA process");
    return false;
  }
  if(erodeGPU(newSize, radius, outputData, outputData, false)) {
    setErrorMessage("Error while running CUDA process");
    return false;
  }

  {
    HVecUS resized = resize(outputData, newSize, size, true);
    outputData.swap(resized);
  }

  output->changed();
  output->copyMetaData(input);
  return true;
}

REGISTER_STACK_PROCESS(ClosingStack);

#define OFFSET(_x, _y) ((_y)*size.x() + (_x))
bool FillHolesProcess::operator()(const Store* input, Store* output, uint xradius, uint yradius, uint threshold,
                                  uint fillval)
{
  fillval = trim(fillval, uint(0), uint(0xFFFF));
  const Stack* stack = input->stack();
  Point3u size = stack->size();

  const HVecUS& data = input->data();
  HVecUS& output_data = output->data();

  // First create height map, allocate update map
  std::vector<int> hmap(size.x() * size.y());
  std::vector<int> umap(size.x() * size.y());

  for(uint x = 0; x < size.x(); x++) {
    for(uint y = 0; y < size.y(); y++) {
      uint idx = OFFSET(x, y);
      hmap[idx] = umap[idx] = 0;
      for(uint z = 0; z < size.z(); z++)
        if(data[stack->offset(x, y, z)] >= threshold)
          hmap[idx] = z;
    }
  }

  // Scan in X direction
  if(xradius > 0) {
    for(uint y = 0; y < size.y(); y++) {
      // first look for range of interest
      int firstx = 0;
      while(firstx < int(size.x()) and hmap[OFFSET(firstx, y)] == 0)
        firstx++;
      if(firstx >= int(size.x() - 1))
        continue;
      int lastx = size.x() - 1;
      while(lastx > firstx and hmap[OFFSET(lastx, y)] == 0)
        lastx--;
      if(--lastx <= ++firstx)
        continue;

      for(int x = firstx; x < lastx; x++) {
        uint idx = OFFSET(x, y);
        // max prev and next z's in range
        int pz = hmap[idx], nz = hmap[idx];
        int px = x, nx = x;
        int startx = x - xradius;
        int endx = x + xradius;
        if(startx < firstx - 1)
          startx = firstx - 1;
        if(endx > lastx + 1)
          endx = lastx + 1;
        for(int ix = startx; ix < x; ix++)
          if(hmap[OFFSET(ix, y)] > pz) {
            pz = hmap[OFFSET(ix, y)];
            px = ix;
          }
        for(int ix = endx; ix > x; ix--)
          if(hmap[OFFSET(ix, y)] > nz) {
            nz = hmap[OFFSET(ix, y)];
            nx = ix;
          }
        // Calculate line intersection with pixel
        if(px < nx) {
          int newz = pz + int(float(x - px) * float(nz - pz) / float(nx - px) + .5);
          if(newz > hmap[idx] and newz > umap[idx]) {
            umap[idx] = newz;
          }
        }
      }
    }
  }

  // Scan in Y direction
  if(yradius > 0) {
    for(uint x = 0; x < size.x(); x++) {
      // first look for range of interest
      int firsty = 0;
      while(firsty < int(size.y()) and hmap[OFFSET(x, firsty)] == 0)
        firsty++;
      if(firsty >= int(size.y() - 1))
        continue;
      int lasty = size.y() - 1;
      while(lasty > firsty and hmap[OFFSET(x, lasty)] == 0)
        lasty--;
      if(--lasty <= ++firsty)
        continue;

      for(int y = firsty; y < lasty; y++) {
        uint idx = OFFSET(x, y);
        // max prev and next z's in range
        int pz = hmap[idx], nz = hmap[idx];
        int py = y, ny = y;
        int starty = y - yradius;
        int endy = y + yradius;
        if(starty < firsty - 1)
          starty = firsty - 1;
        if(endy > lasty + 1)
          endy = lasty + 1;
        for(int iy = starty; iy < y; iy++)
          if(hmap[OFFSET(x, iy)] > pz) {
            pz = hmap[OFFSET(x, iy)];
            py = iy;
          }
        for(int iy = endy; iy > y; iy--)
          if(hmap[OFFSET(x, iy)] > nz) {
            nz = hmap[OFFSET(x, iy)];
            ny = iy;
          }
        // Calculate line intersection with pixel
        if(py < ny) {
          int newz = pz + int(float(y - py) * float(nz - pz) / float(ny - py));
          if(newz > hmap[idx] and newz > umap[idx]) {
            umap[idx] = newz;
          }
        }
      }
    }
  }

  // Update stack
  for(int x = 0; x < int(size.x()); x++)
    for(int y = 0; y < int(size.y()); y++)
      for(int z = umap[OFFSET(x, y)]; z >= 0; z--)
        output_data[stack->offset(x, y, z)] = fillval;

  output->copyMetaData(input);
  output->changed();
  return true;
}
#undef OFFSET

REGISTER_STACK_PROCESS(FillHolesProcess);

static bool openCloseStack(Stack* stack, const Store* input, Store* output, uint xradius, uint yradius, uint zradius,
                           bool is_opening, Process* proc)
{
  Point3i radius(xradius, yradius, zradius);
  Point3u size = stack->size();

  const HVecUS& data = input->data();
  HVecUS& outputData = output->data();
  std::vector<BoundingBox3u> bBoxImg(65536, BoundingBox3u());

  if(input != output)
    outputData = data;

  // Progress progress(QString("Finding object bounds-%1").arg(mesh->userId()), data.size());
  // int progress_step = data.size()/100;

  const ushort* pdata = data.data();
  for(uint z = 0; z < size.z(); z++)
    for(uint y = 0; y < size.y(); y++)
      for(uint x = 0; x < size.x(); x++, pdata++) {
        int label = *pdata;
        if(label == 0)
          continue;
        bBoxImg[label] |= Point3u(x, y, z);
      }

#define OFFSET(x, y, z, xsz, ysz) ((size_t(z) * (ysz) + (y)) * (xsz) + (x))
  for(int i = 0; i < 65536; ++i) {
    // Check for valid label
    if(bBoxImg[i].empty())
      continue;
    // Copy data to temp area
    Point3u bSize = bBoxImg[i].pmax() - bBoxImg[i].pmin() + 1u;
    HVecUS tempData(size_t(bSize.x()) * bSize.y() * bSize.z());
    uint x, y, z, xb, yb, zb;
    for(zb = 0, z = bBoxImg[i].pmin().z(); z <= bBoxImg[i].pmax().z(); z++, zb++)
      for(yb = 0, y = bBoxImg[i].pmin().y(); y <= bBoxImg[i].pmax().y(); y++, yb++)
        for(xb = 0, x = bBoxImg[i].pmin().x(); x <= bBoxImg[i].pmax().x(); x++, xb++)
        {
          size_t offset = OFFSET(xb, yb, zb, bSize.x(), bSize.y());
          if(data[stack->offset(x, y, z)] == i)
            tempData[offset] = 1;
          else
            tempData[offset] = 0;
        }

    if(is_opening) {
      if(erodeGPU(bSize, radius, tempData, tempData, input->labels())) {
        proc->setErrorMessage("Error while running CUDA process");
        return false;
      }
      if(dilateGPU(bSize, radius, tempData, tempData)) {
        proc->setErrorMessage("Error while running CUDA process");
        return false;
      }
      for(zb = 0, z = bBoxImg[i].pmin().z(); z <= bBoxImg[i].pmax().z(); z++, zb++)
        for(yb = 0, y = bBoxImg[i].pmin().y(); y <= bBoxImg[i].pmax().y(); y++, yb++)
          for(xb = 0, x = bBoxImg[i].pmin().x(); x <= bBoxImg[i].pmax().x(); x++, xb++) {
            size_t off = stack->offset(x, y, z);
            if(outputData[off] == i)
              outputData[off] *= tempData[OFFSET(xb, yb, zb, bSize.x(), bSize.y())];
          }
    } else {
      if(dilateGPU(bSize, radius, tempData, tempData)) {
        proc->setErrorMessage("Error while running CUDA process");
        return false;
      }
      if(erodeGPU(bSize, radius, tempData, tempData, false)) {
        proc->setErrorMessage("Error while running CUDA process");
        return false;
      }
      for(zb = 0, z = bBoxImg[i].pmin().z(); z <= bBoxImg[i].pmax().z(); z++, zb++)
        for(yb = 0, y = bBoxImg[i].pmin().y(); y <= bBoxImg[i].pmax().y(); y++, yb++)
          for(xb = 0, x = bBoxImg[i].pmin().x(); x <= bBoxImg[i].pmax().x(); x++, xb++)
            if(tempData[OFFSET(xb, yb, zb, bSize.x(), bSize.y())] == 1)
              outputData[stack->offset(x, y, z)] = i;
    }
  }
#undef OFFSET
  output->changed();
  output->copyMetaData(input);
  return true;
}

bool OpenStackLabel::operator()(Stack* stack, const Store* input, Store* output, uint xradius, uint yradius,
                                uint zradius)
{
  return openCloseStack(stack, input, output, xradius, yradius, zradius, true, this);
}

REGISTER_STACK_PROCESS(OpenStackLabel);

bool CloseStackLabel::operator()(Stack* stack, const Store* input, Store* output, uint xradius, uint yradius,
                                 uint zradius)
{
  return openCloseStack(stack, input, output, xradius, yradius, zradius, false, this);
}

REGISTER_STACK_PROCESS(CloseStackLabel);

bool ApplyMaskToStack::operator()(Stack*, const Store* input, Store* output, const QString& mode, uint threshold)
{
  const ushort* pin = &input->data()[0];
  ushort* pout = &output->data()[0];
  if(mode == "Normal") {
    for(size_t i = 0; i < output->size(); ++i, ++pin, ++pout)
      if(*pout > threshold)
        *pout = *pin;
      else
        *pout = 0;
  } else if(mode == "Invert") {
    for(size_t i = 0; i < output->size(); ++i, ++pin, ++pout)
      if(*pout <= threshold)
        *pout = *pin;
      else
        *pout = 0;
  } else if(mode == "Combine") {
    for(size_t i = 0; i < output->size(); ++i, ++pin, ++pout)
      if(*pout <= threshold)
        *pout = *pin;
  }

  output->changed();
  return true;
}

REGISTER_STACK_PROCESS(ApplyMaskToStack);

bool ApplyMaskLabels::operator()(Stack*, const Store* input, Store* output, bool invert, uint threshold)
{
  const ushort* pin = &input->data()[0];
  ushort* pout = &output->data()[0];
  std::set<int> labelSet;
  for(size_t i = 0; i < output->size(); ++i, ++pin, ++pout)
    if(*pout >= threshold)
      labelSet.insert(*pin);

  pin = &input->data()[0];
  pout = &output->data()[0];
  if(!invert) {
    for(size_t i = 0; i < output->size(); ++i, ++pin, ++pout)
      if(labelSet.count(*pin) > 0)
        *pout = *pin;
      else
        *pout = 0;
  } else {
    for(size_t i = 0; i < output->size(); ++i, ++pin, ++pout)
      if(labelSet.count(*pin) > 0)
        *pout = 0;
      else
        *pout = *pin;
  }

  output->changed();
  return true;
}

REGISTER_STACK_PROCESS(ApplyMaskLabels);
} // namespace process
} // namespace lgx
