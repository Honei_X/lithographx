/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MESH_PROCESSS_CELLAXIS_HPP
#define MESH_PROCESSS_CELLAXIS_HPP

#include <Process.hpp>

#include <MeshProcessCellMesh.hpp>

namespace lgx {
namespace process {
///\addtogroup MeshProcess
///@{
/**
 * \class SaveCellAxis <MeshProcessCellAxis.hpp>
 */
class LGXCORE_EXPORT SaveCellAxis : public MeshProcess {
public:
  SaveCellAxis(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool initialize(QStringList& parms, QWidget* parent);

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0]);
  }

  virtual bool operator()(Mesh* mesh, const QString& filename) = 0;

  bool saveFile(Mesh* mesh, const QString& filename, const QString& k1Name, const QString& k2Name = QString(),
                const QString& k3Name = QString());

  /*
   * QString folder() const { return "Cell Axis"; }
   * QString name() const { return "Cell Axis Save"; }
   * QString description() const { return "Save PDG, Principal Orientations or Curvature to a spreadsheet file"; }
   */
  QStringList parmNames() const {
    return QStringList() << "Output File";
  }
  QStringList parmDescs() const {
    return QStringList() << "Output File";
  }
  QStringList parmDefaults() const {
    return QStringList() << "";
  }
  QIcon icon() const {
    return QIcon(":/images/CellAxisSave.png");
  }
};

/**
 * \class LoadCellAxis <MeshProcessCellAxis.hpp>
 */
class LGXCORE_EXPORT LoadCellAxis : public MeshProcess {
public:
  LoadCellAxis(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool initialize(QStringList& parms, QWidget* parent);

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0]);
  }

  virtual bool operator()(Mesh* mesh, const QString& filename) = 0;

  /**
   * Show the result based on 'axisType'.
   *
   * \note axisType is set by readFile and readOldFile if they succeed
   */
  void showResult(Mesh* mesh);

  bool readFile(Mesh* mesh, const QString& type, const QString& filename, QRegExp& k1Name, QRegExp& k2Name,
                QRegExp& k3Name, bool* wrong_type = 0);
  bool readOldFile(Mesh* mesh, const QString& type, const QString& filename, QString& k1, QString& k2, QString& k3);

  // QString folder() const { return "Cell Axis"; }
  // QString name() const { return "Cell Axis Load"; }
  // QString description() const { return "Load cell axis from a spreadsheet file"; }
  QStringList parmNames() const {
    return QStringList() << "Input File";
  }
  QStringList parmDescs() const {
    return QStringList() << "Input File";
  }
  QStringList parmDefaults() const {
    return QStringList() << "";
  }
  QIcon icon() const {
    return QIcon(":/images/CellAxisOpen.png");
  }

protected:
  QString axisType;
};

/**
 * \class ClearCellAxis <MeshProcessCellAxis.hpp>
 *
 * Clear the cell axis on the mesh
 */
class LGXCORE_EXPORT ClearCellAxis : public MeshProcess {
public:
  ClearCellAxis(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList&)
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh());
  }

  bool operator()(Mesh* mesh);

  QString folder() const {
    return "Cell Axis";
  }
  QString name() const {
    return "Cell Axis Clear";
  }
  QString description() const {
    return "Remove any cell axis information from the current mesh.";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/CellAxisClear.png");
  }
};

/**
 * \class HideCellAxis <MeshProcessCellAxis.hpp>
 *
 * Hide the cell axis on the mesh
 */
class LGXCORE_EXPORT HideCellAxis : public MeshProcess {
public:
  HideCellAxis(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList&)
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh());
  }

  bool operator()(Mesh* mesh);

  QString folder() const {
    return "Cell Axis";
  }
  QString name() const {
    return "Cell Axis Hide";
  }
  QString description() const {
    return "Hide the cell axis on the current mesh.";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/CellAxisHide.png");
  }
};
///@}
} // namespace process
} // namespace lgx
#endif
