/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "StackProcessAnalyses.hpp"

#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include "Misc.hpp"

#ifdef _OPENMP
#  include <omp.h>
#endif

namespace lgx {
namespace process {

bool ComputeVolume::operator()(const Store* input, QString filename)
{
  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly))
    return setErrorMessage(QString("Error, cannot open file '%1' for writing").arg(filename));
  std::vector<unsigned long> volumes(1 << 16);   // List of volumes per label
  const Stack* stk = input->stack();
  float dv = stk->step().x() * stk->step().y() * stk->step().z();   // volume of a voxel
  const HVecUS& data = input->data();
  for(uint i = 0; i < data.size(); ++i) {
    ushort label = data[i];
    ++volumes[label];
  }

  QTextStream ts(&file);
  ts << QString("Label,Volume (%1)").arg(UM3) << endl;
  for(uint lab = 1; lab < 1 << 16; ++lab) {
    if(volumes[lab] > 0)
      ts << lab << "," << (volumes[lab] * dv) << endl;
  }
  file.close();
  return true;
}

REGISTER_STACK_PROCESS(ComputeVolume);

bool ComputeStackSignal::initialize(QStringList& parms, QWidget* parent)
{
  QString filename = parms[0];
  filename = QFileDialog::getSaveFileName(parent, QString("Select CSV file"), filename,
                                          "CSV Files(*.csv);;All files (*.*)");
  if(filename.isEmpty())
    return false;
  if(not filename.endsWith(".csv"))
    filename += ".csv";
  parms[0] = filename;
  return true;
}

bool ComputeStackSignal::operator()(const Store* labels, const Store* signal, bool volumetric, const QString& filename)
{
  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly))
    return setErrorMessage(QString("Error, cannot open file '%1' for writing").arg(filename));

  const HVecUS& lab = labels->data();
  const HVecUS& sig = signal->data();

#ifdef _OPENMP
  int nb_threads = omp_get_max_threads();
#else
  int nb_threads = 1;
#endif

  std::vector<std::vector<double> > sigs(nb_threads, std::vector<double>(65536, 0.0));
  std::vector<std::vector<size_t> > volumes(nb_threads, std::vector<size_t>(65536, 0));

#pragma omp parallel for
  for(size_t i = 0; i < lab.size(); ++i) {
#ifdef _OPENMP
    int cur_thread = omp_get_thread_num();
#else
    int cur_thread = 0;
#endif
    sigs[cur_thread][lab[i]] += (double)sig[i];
    volumes[cur_thread][lab[i]]++;
  }

  std::vector<double> lab_signal;
  std::vector<size_t> lab_volumes;

  if(nb_threads > 1) {
    lab_signal.resize(65536, 0.0);
    lab_volumes.resize(65536, 0);

    for(int i = 0; i < nb_threads; ++i)
      for(size_t j = 0; j < lab_signal.size(); ++j) {
        lab_signal[j] += sigs[i][j];
        lab_volumes[j] += volumes[i][j];
      }
  } else {
    std::swap(lab_signal, sigs[0]);
    std::swap(lab_volumes, volumes[0]);
  }

  const Stack* stk = labels->stack();
  float dv = stk->step().x() * stk->step().y() * stk->step().z();   // volume of a voxel

  QTextStream ts(&file);
  if(volumetric)
    ts << ("Label,VolumetricSignal (" + UM3 + ")") << endl;
  else
    ts << "Label,Signal" << endl;

  for(size_t i = 0; i < lab_signal.size(); ++i)
    if(lab_volumes[i] > 0) {
      double qty = lab_signal[i];
      if(volumetric)
        qty /= lab_volumes[i] * dv;
      ts << i << "," << qty << endl;
    }

  return true;
}

REGISTER_STACK_PROCESS(ComputeStackSignal);

bool ComputeCellLength::initialize(QStringList& parms, QWidget* parent)
{
  QString filename = parms[0];
  filename = QFileDialog::getSaveFileName(parent, "Select file to save the result into",
                                          filename, "CSV files (*.csv);;All files (*.*)");
  if(filename.isEmpty())
    return false;
  if(not filename.endsWith(".csv"))
    filename += ".csv";
  parms[0] = filename;
  return true;
}

bool ComputeCellLength::operator()(Store* store, const Point3f axis, float percentile, const QString filename)
{
  Information::out << "Starting ComputeCellLength" << endl;
  const HVecUS& data = store->data();
  // For each possible label, store each position
  std::vector<std::vector<float> > positions(65536);
  // Compute vector in image coordinates
  Information::out << __LINE__ << endl;
  auto stk = store->stack();
  Information::out << __LINE__ << endl;
  const auto& frame = stk->getFrame();
  Information::out << __LINE__ << endl;
  auto vaxis = qglviewer::Vec(axis);
  Information::out << __LINE__ << endl;
  Point3f worldAxis = util::normalized(Point3f(frame.transformOf(vaxis)));
  Information::out << __LINE__ << endl;
  Point3f dir = stk->worldToImageVectorf(worldAxis);
  Information::out << __LINE__ << endl;
  dir /= normsq(dir);
  Information::out << __LINE__ << endl;
  Information::out << "World direction: " << worldAxis << endl;
  Information::out << "Image direction: " << dir << endl;
  Point3u s = stk->size();
  for(size_t z = 0, k = 0 ; z < s.z() ; ++z)
    for(size_t y = 0 ; y < s.y() ; ++y)
      for(size_t x = 0 ; x < s.x() ; ++x, ++k) {
        ushort label = data[k];
        if(label > 0) {
          positions[label].push_back(Point3f(x, y, z) * dir);
        }
      }
  double half_percentile = percentile / 200;
  //Information::out << "Finished scanning stack. ½ percentile = " << half_percentile << endl;
  std::vector<float> sizes(65536);
  for(size_t i = 0 ; i < 65536 ; ++i) {
    if(not positions[i].empty()) {
      auto& pos = positions[i];
      std::sort(begin(pos), end(pos));
      double start_index = double(pos.size()) * (0.5 - half_percentile);
      double end_index = double(pos.size()) * (0.5 + half_percentile);
      size_t si_low = floor(start_index);
      double si_frac = start_index - si_low;
      size_t si_high = (si_low < pos.size()-1 ? si_low+1 : si_low);
      size_t ei_low = floor(end_index);
      double ei_frac = end_index - ei_low;
      size_t ei_high = (ei_low < pos.size()-1 ? ei_low+1 : ei_low);

      float low_end = pos[si_low] * (1-si_frac) + pos[si_high] * si_frac;
      float high_end = pos[ei_low] * (1-ei_frac) + pos[ei_high] * ei_frac;
      sizes[i] = fabs(high_end - low_end);

      //Information::out << "Label " << i << " # positions: " << pos.size() << " - low = " << start_index << " - high = " << end_index << endl;
    }
  }

  {
    QFile file(filename);
    if(not file.open(QIODevice::WriteOnly))
      return setErrorMessage(QString("Cannot open file '%1' for writing").arg(filename));

    QTextStream ts(&file);

    ts << QString("Label,Length (%1)\r\n").arg(UM);
    for(size_t i = 0 ; i < 65536 ; ++i) {
      if(not positions[i].empty())
        ts << i << "," << sizes[i] << "\r\n";
    }
  }
  return true;
}

REGISTER_STACK_PROCESS(ComputeCellLength);

} // namespace process
} // namespace lgx
