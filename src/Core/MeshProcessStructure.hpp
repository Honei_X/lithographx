/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MESH_PROCESS_STRUCTURE_HPP
#define MESH_PROCESS_STRUCTURE_HPP

#include <Process.hpp>

#include <Misc.hpp>

namespace lgx {
namespace process {
/**
 *  Subdivide triangle by bi-sections
 *
 * \param S Graph to subdivide
 * \param v1 First vertex of the triangle to divide
 * \param v2 Second vertex of the triangle to divide
 * \param v3 Third vertex of the triangle to divide
 * \param selected If true, new vertices will be selected
 * \param vs If non-NULL, vertices inserted are appended
 *
 * \ingroup ProcessUtils
 */
LGXCORE_EXPORT bool subdivideBisect(vvgraph& S, const vertex& v1, const vertex& v2, const vertex& v3, bool selected,
                                std::vector<vertex>* vs = 0);
/**
 * Mark the cells and tissue margin vertices.
 *
 * \param M subset of the graph to consider
 * \param S full tissue
 * \param remborders If true, all vertices with -1 as label and not at the
 * border between cells will be marked as 0.
 *
 * \ingroup ProcessUtils
 */
LGXCORE_EXPORT void markMargin(vvgraph& M, vvgraph& S, bool remborders);

///\addtogroup MeshProcess
///@{

/**
 * \class TransformMesh MeshProcessStructure.hpp <MeshProcessStructure.hpp>
 *
 * Apply an affine transformation to the vertices of a mesh (all of them or just the selected ones).
 */
class LGXCORE_EXPORT TransformMesh : public MeshProcess {
public:
  TransformMesh(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    Point3f trans, rot;
    {
      QString txt = parms[0];
      QTextStream ts(&txt);
      ts >> trans;
    }
    {
      QString txt = parms[1];
      QTextStream ts(&txt);
      ts >> rot;
    }
    bool ok;
    float angle = parms[2].toFloat(&ok) * M_PI / 180.f;
    if(!ok)
      return setErrorMessage("Error, the 'angle' parameter must be a number");
    float scale = parms[3].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, the 'scale' parameter must be a number");
    return (*this)(currentMesh(), trans, rot, angle, scale);
  }

  bool operator()(Mesh* mesh, Point3f translation, Point3f rotation_axis, float rotation, float scale);

  QString folder() const {
    return "Structure";
  }
  QString name() const {
    return "Transform Mesh";
  }
  QString description() const {
    return "Apply an affine transformation to all vertices of a mesh";
  }
  QStringList parmNames() const
  {
    return QStringList() << QString("translation (%1)").arg(UM) << "rotation axis"
                         << "angle (degree)"
                         << "scale";
  }
  QStringList parmDesc() const
  {
    return QStringList() << "Translation in micrometers. Enter x y z separated by spaces"
                         << "Vector representing the axis of rotation. Enter x y z separated by spaces"
                         << "Angle of rotation in degrees"
                         << "Global scaling factor";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "0.0 0.0 0.0"
                         << "0.0 0.0 1.0"
                         << "0.0"
                         << "1.0";
  }
  QIcon icon() const {
    return QIcon(":/images/Resize.png");
  }
};

/**
 * \class ReverseMesh MeshProcessStructure.hpp <MeshProcessStructure.hpp>
 *
 * Reverse the orientation of the mesh.
 */
class LGXCORE_EXPORT ReverseMesh : public MeshProcess {
public:
  ReverseMesh(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& )
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh());
  }
  bool operator()(Mesh* mesh);

  QString folder() const {
    return "Structure";
  }
  QString name() const {
    return "Reverse Mesh";
  }
  QString description() const {
    return "Reverse orientation of the mesh";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QStringList parmDefaults() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/Invert.png");
  }
};

/**
 * \class SmoothMesh MeshProcessStructure.hpp <MeshProcessStructure.hpp>
 *
 * Average each vertex position based on its neighbors.
 */
class LGXCORE_EXPORT SmoothMesh : public MeshProcess {
public:
  SmoothMesh(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0].toUInt());
  }

  bool operator()(Mesh* mesh, uint passes);

  QString folder() const {
    return "Structure";
  }
  QString name() const {
    return "Smooth Mesh";
  }
  QString description() const {
    return "Average each vertex position based on its neighbors.";
  }
  QStringList parmNames() const {
    return QStringList() << "Passes";
  }
  QStringList parmDescs() const {
    return QStringList() << "Passes";
  }
  QStringList parmDefaults() const {
    return QStringList() << "1";
  }
  QIcon icon() const {
    return QIcon(":/images/SmoothMesh.png");
  }
};

/**
 * \class ShrinkMesh MeshProcessStructure.hpp <MeshProcessStructure.hpp>
 *
 * Displace each vertex towards the mesh center, perpendicular to the surface.
 */
class LGXCORE_EXPORT ShrinkMesh : public MeshProcess {
public:
  ShrinkMesh(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0].toFloat());
  }

  bool operator()(Mesh* mesh, float distance);

  QString folder() const {
    return "Structure";
  }
  QString name() const {
    return "Shrink Mesh";
  }
  QString description() const
  {
    return "Displace each vertex towards the mesh center, perpendicular to the surface.";
  }
  QStringList parmNames() const {
    return QStringList() << QString("Distance(%1)").arg(UM);
  }
  QStringList parmDescs() const {
    return QStringList() << "Vertex displacement. If negtive, the mesh will expand.";
  }
  QStringList parmDefaults() const {
    return QStringList() << "1.0";
  }
  QIcon icon() const {
    return QIcon(":/images/ShrinkMesh.png");
  }
};

/**
 * \class LoopSubdivisionMesh MeshProcessStructure.hpp <MeshProcessStructure.hpp>
 *
 * Subdivide the mesh uniformly using Loop subdivision.
 */
class LGXCORE_EXPORT LoopSubdivisionMesh : public MeshProcess {
public:
  LoopSubdivisionMesh(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& )
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh());
  }

  bool operator()(Mesh* mesh);

  QString folder() const {
    return "Structure";
  }
  QString name() const {
    return "Loop Subdivision";
  }
  QString description() const {
    return "Subdivide the mesh uniformly using Loop subdivision.";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/SubdivideTri.png");
  }
};

/**
 * \class SubdivideMesh MeshProcessStructure.hpp <MeshProcessStructure.hpp>
 *
 * Subdivide the mesh uniformely, without displacing existing vertices.
 */
class LGXCORE_EXPORT SubdivideMesh : public MeshProcess {
public:
  SubdivideMesh(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& )
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh());
  }

  bool operator()(Mesh* mesh);

  QString folder() const {
    return "Structure";
  }
  QString name() const {
    return "Subdivide";
  }
  QString description() const {
    return "Subdivide the mesh unifromly";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/SubdivideTri.png");
  }
};

/**
 * \class AdaptiveSubdivideBorderMesh MeshProcessStructure.hpp <MeshProcessStructure.hpp>
 *
 * Subdivide triangles around cell borders.
 */
class LGXCORE_EXPORT AdaptiveSubdivideBorderMesh : public MeshProcess {
public:
  AdaptiveSubdivideBorderMesh(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0].toFloat(), parms[1].toFloat());
  }

  bool operator()(Mesh* mesh, float cellMaxArea, float borderDist);

  QString folder() const {
    return "Structure";
  }
  QString name() const {
    return "Subdivide Adaptive Near Borders";
  }
  QString description() const {
    return "Subdivide triangles around cell borders";
  }
  QStringList parmNames() const
  {
    return QStringList() << QString("Max Area(%1)").arg(UM2) << QString("Border Dist(%1)").arg(UM);
  }
  QStringList parmDescs() const
  {
    return QStringList()
           << "Area threshold (in square microns) for subdivision, triangles smaller than this won't be subdivided"
           << "Distance (in microns) from cell borders that triangles will be subdivided";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "0.1"
                         << "1.0";
  }
  QIcon icon() const {
    return QIcon(":/images/SubdivideTriAdapt.png");
  }
};

/**
 * \class AdaptiveSubdivideSignalMesh MeshProcessStructure.hpp <MeshProcessStructure.hpp>
 *
 * Subdivide triangles depending on mesh signal. Triangle size is determined
 * by a high and low area, which is interpolated based on the minimum and
 * maximum signals
 */
class LGXCORE_EXPORT AdaptiveSubdivideSignalMesh : public MeshProcess {
public:
  AdaptiveSubdivideSignalMesh(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0].toFloat(), parms[1].toFloat());
  }

  bool operator()(Mesh* mesh, float cellMaxAreaLow, float cellMaxAreaHigh);

  QString folder() const {
    return "Structure";
  }
  QString name() const {
    return "Subdivide Adaptive by Signal";
  }
  QString description() const
  {
    return "Subdivide triangles depending on mesh signal. Triangle size\n"
           "is determined by a high and low area, which is interpolated\n"
           "based on the minimum and maximum signals";
  }
  QStringList parmNames() const
  {
    return QStringList() << QString("Low Max Area(%1)").arg(UM2) << QString("High Max Area(%1)").arg(UM2);
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Maximum area (square microns) for low instentity voxels"
                         << "Maximum area (square microns) for high instentity voxels";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "1.0"
                         << "0.1";
  }
  QIcon icon() const {
    return QIcon(":/images/SubdivideTriAdapt.png");
  }
};

/**
 * \class SubdivideBisectMesh MeshProcessStructure.hpp <MeshProcessStructure.hpp>
 *
 * Subdivide triangles area with bisection.
 */
class LGXCORE_EXPORT SubdivideBisectMesh : public MeshProcess {
public:
  SubdivideBisectMesh(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0].toFloat());
  }

  bool operator()(Mesh* mesh, float cellMaxArea);

  QString folder() const {
    return "Structure";
  }
  QString name() const {
    return "Subdivide with Bisection";
  }
  QString description() const {
    return "Subdivide triangles area with bisection";
  }
  QStringList parmNames() const {
    return QStringList() << QString("Max Area(%1)").arg(UM2);
  }
  QStringList parmDescs() const {
    return QStringList() << QString("Max Area(%1)").arg(UM2);
  }
  QStringList parmDefaults() const {
    return QStringList() << "1.0";
  }
  QIcon icon() const {
    return QIcon(":/images/SubdivideTriAdapt.png");
  }
};

/**
 * \class ScaleMesh MeshProcessStructure.hpp <MeshProcessStructure.hpp>
 *
 * Scale the mesh. It is possible to specify a negative number, in which case the dimension will be mirrored.
 */
class LGXCORE_EXPORT ScaleMesh : public MeshProcess {
public:
  ScaleMesh(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0].toFloat(), parms[1].toFloat(), parms[2].toFloat());
  }

  bool operator()(Mesh* mesh, float scaleX, float scaleY, float scaleZ);

  QString folder() const {
    return "Structure";
  }
  QString name() const {
    return "Scale Mesh";
  }
  QString description() const
  {
    return "Scale Mesh, or a selected part of it.\n"
           "It is possible to specify a negative number, in which case the dimension will be mirrored.\n"
           "If either 1 or 3 axis are mirrored, then the whole mesh needs to be scaled, "
           "as these triangles will change orientation";
  }
  QStringList parmNames() const
  {
    return QStringList() << "X Scale"
                         << "Y Scale"
                         << "Z Scale";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "X Scale"
                         << "Y Scale"
                         << "Z Scale";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "1.0"
                         << "1.0"
                         << "1.0";
  }
  QIcon icon() const {
    return QIcon(":/images/Scale.png");
  }
};

/**
 * \class MeshDeleteValence MeshProcessStructure.hpp <MeshProcessStructure.hpp>
 *
 * Delete vertices whose valence is wihin a given range.
 */
class LGXCORE_EXPORT MeshDeleteValence : public MeshProcess {
public:
  MeshDeleteValence(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0].toInt(), parms[1].toInt());
  }

  bool operator()(Mesh* mesh, int startValence, int endValence);

  QString folder() const {
    return "Structure";
  }
  QString name() const {
    return "Delete Mesh Vertices by Valence";
  }
  QString description() const {
    return "Delete mesh vertices that have valence within the specified range";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Start Valence"
                         << "End Valence";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Start Valence"
                         << "End Valence";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "0"
                         << "2";
  }
  QIcon icon() const {
    return QIcon(":/images/DeleteValence");
  }
};

/**
 * \class MergeVertices MeshProcessStructure.hpp <MeshProcessStructure.hpp>
 *
 * Merge selected vertices into one.
 */
class LGXCORE_EXPORT MergeVertices : public MeshProcess {
public:
  MergeVertices(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& )
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return operator()(currentMesh());
  }

  bool operator()(Mesh* mesh);

  QString folder() const {
    return "Structure";
  }
  QString name() const {
    return "Merge Vertices";
  }
  QString description() const {
    return "Merge selected vertices into one.";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QStringList parmDefaults() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/MergeVertices");
  }
};

/**
 * \class DeleteEdge MeshProcessStructure.hpp <MeshProcessStructure.hpp>
 *
 * Delete the edge between 2 selected vertices.
 */
class LGXCORE_EXPORT DeleteEdge : public MeshProcess {
public:
  DeleteEdge(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& )
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return operator()(currentMesh());
  }

  bool operator()(Mesh* mesh);

  QString folder() const {
    return "Structure";
  }
  QString name() const {
    return "Delete Edge";
  }
  QString description() const {
    return "Delete edge between 2 selected vertices.";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QStringList parmDefaults() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/DeleteLabel.png");
  }
};

/**
 * \class MeshDeleteSelection MeshProcessStructure.hpp <MeshProcessStructure.hpp>
 *
 * Delete vertices selected in the current mesh, preserving cells.
 */
class LGXCORE_EXPORT MeshDeleteSelection : public MeshProcess {
public:
  MeshDeleteSelection(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& )
  {
    if(!checkState().mesh(MESH_NON_EMPTY | MESH_SHOW_MESH))
      return false;
    return (*this)(currentMesh());
  }

  bool operator()(Mesh* m);

  QString folder() const {
    return "Structure";
  }
  QString name() const {
    return "Delete Selection";
  }
  QString description() const {
    return "Delete vertices selected in the current mesh, preserving cells.";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/DeleteLabel.png");
  }
};

/**
 * \class MeshKeepVertices MeshProcessStructure.hpp <MeshProcessStructure.hpp>
 *
 * Mark vertices so that they can survive as lines and points. Also prevents labels changing.
 */
class LGXCORE_EXPORT MeshKeepVertices : public MeshProcess {
public:
  MeshKeepVertices(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_NON_EMPTY | MESH_SHOW_MESH))
      return false;
    bool keep = stringToBool(parms[0]);
    return (*this)(currentMesh(), keep);
  }

  bool operator()(Mesh* m, bool keep);

  QString folder() const {
    return "Structure";
  }
  QString name() const {
    return "Keep Vertices";
  }
  QString description() const
  {
    return "Mark vertices so that they can survive as lines and points. Also prevents labels changing.";
  }
  QStringList parmNames() const {
    return QStringList() << "Keep";
  }
  QStringList parmDescs() const {
    return QStringList() << "Keep";
  }
  QStringList parmDefaults() const {
    return QStringList() << "No";
  }
  ParmChoiceMap parmChoice() const
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    return map;
  }
  QIcon icon() const {
    return QIcon(":/images/KeepVertices.png");
  }
};
///@}
} // namespace process
} // namespace lgx
#endif
