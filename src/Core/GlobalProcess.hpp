/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef GLOBALPROCESS_HPP
#define GLOBALPROCESS_HPP

#include <Process.hpp>

#include <Stack.hpp>
#include <Store.hpp>

namespace lgx {
namespace process {

///\addtogroup GlobalProcess
///@{
/**
 * \class SetCurrentStack GlobalProcess.hpp <GlobalProcess.hpp>
 *
 * Set the current stack and store. This process is meant to be used when scripting.
 */
class LGXCORE_EXPORT SetCurrentStack : public GlobalProcess {
public:
  SetCurrentStack(const GlobalProcess& process)
    : Process(process)
    , GlobalProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    STORE which_store = stringToStore(parms[0]);
    int id = parms[1].toInt();
    if(!checkState().store(which_store, id))
      return false;
    return (*this)(which_store, id);
  }

  bool operator()(STORE which, int id);

  QString name() const {
    return "SetCurrentStack";
  }
  QString folder() const {
    return "System";
  }
  QString description() const {
    return "Change the current stack and mesh. Needed for scripts.";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Store"
                         << "Stack id";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Store"
                         << "Stack id";
  }
  ParmChoiceMap parmChoice() const
  {
    ParmChoiceMap map;
    map[0] = storeChoice();
    return map;
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "Main"
                         << "0";
  }
  QIcon icon() const {
    return QIcon(":/images/Relabel.png");
  }
};

/**
 * \class SaveGlobalTransform GlobalProcess.hpp <GlobalProcess.hpp>
 *
 * Save the transformation from stack 1 to stack 2 in a file. The file contains 16 numbers:
 *
 * ``m11 m21 m31 m41 m21 m22 m23 m24 m31 m32 m33 m34 m41 m42 m43 m44``
 *
 * The transformation is then defined by the matrix:
 *
 * \f$ M = \left[\begin{array}{cccc} m11 & m12 & m13 & m14 \\
 *                                  m21 & m22 & m23 & m24 \\
 *                                  m31 & m32 & m33 & m34 \\
 *                                  m41 & m42 & m43 & m44 \end{array} \right] \f$
 */
class LGXCORE_EXPORT SaveGlobalTransform : public QObject, public GlobalProcess {
  Q_OBJECT

public:
  SaveGlobalTransform(const GlobalProcess& process)
    : Process(process)
    , QObject()
    , GlobalProcess(process)
  {
  }

  bool initialize(QStringList& parms, QWidget* parent);

  bool operator()(const QStringList& parms) {
    return (*this)(parms[0]);
  }

  bool operator()(const QString& filename);

  QString folder() const {
    return "Transform";
  }
  QString name() const {
    return "Save Global Transform";
  }
  QString description() const
  {
    return "Save the global alignment (transform) matrix from one stack to the other into a file";
  }
  QStringList parmNames() const {
    return QStringList() << "Filename";
  }
  QStringList parmDescs() const {
    return QStringList() << "Filename";
  }
  QStringList parmDefaults() const {
    return QStringList() << "";
  }
  QIcon icon() const {
    return QIcon(":/images/save.png");
  }
};

/**
 * \class JoinRegionsSegment GlobalProcess.hpp <GlobalProcess.hpp>
 *
 * Join regions, using both the segmented stack and the extracted 3D cell mesh.
 *
 * Starting from a segmented stack, and the 3D cell mesh created from it, the
 * user can select a set of cells that needs to be merged. This process will
 * create a new cell corresponding to the union of all the selected cells and
 * rerun the Marching Cube process with the provided cube size.
 */
class LGXCORE_EXPORT JoinRegionsSegment : public GlobalProcess {
public:
  JoinRegionsSegment(const GlobalProcess& process)
    : Process(process)
    , GlobalProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_WORK | STORE_LABEL).mesh(MESH_NON_EMPTY))
      return false;
    Store* work = currentStack()->work();
    Mesh* mesh = currentMesh();

    bool ok;
    float cubeSize = parms[0].toFloat(&ok);
    if(not ok)
      return setErrorMessage("Error, the parameter 'Cube Size' must be a number.");

    bool res = (*this)(work, mesh, cubeSize);
    if(res)
      work->show();
    return res;
  }

  bool operator()(Store* work, Mesh* mesh, float cubeSize);

  QString name() const {
    return "JoinRegions Segmentation";
  }
  QString description() const
  {
    return "Join Regions after 3D segmentation.\n"
           "Cells selected in the 3D cell mesh extracted from the stack will be merged and re-extracted.";
  }
  QString folder() const {
    return "Segmentation";
  }
  QStringList parmNames() const {
    return QStringList() << "Cube Size";
  }
  QStringList parmDescs() const {
    return QStringList() << "Cube Size for the Marching Cube process.";
  }
  QStringList parmDefaults() const {
    return QStringList() << "0";
  }
  QIcon icon() const {
    return QIcon(":/images/JoinRegions.png");
  }
};
///@}
} // namespace process
} // namespace lgx

#endif
