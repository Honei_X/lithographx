/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "MeshProcessStructure.hpp"

#include "Progress.hpp"
#include "Insert.hpp"
#include "Mesh.hpp"

#include <QTextStream>

namespace lgx {
namespace process {

bool TransformMesh::operator()(Mesh* mesh, Point3f translation, Point3f rotation, float angle, float scale)
{
  Point4f dir = homogeneous(rotation);
  Matrix4f M = Matrix4f::rotation(dir, angle);
  M(0, 3) = translation.x();
  M(1, 3) = translation.y();
  M(2, 3) = translation.z();
  M(3, 3) = 1.f / scale;
  const std::vector<vertex>& vs = mesh->activeVertices();
  if(scale < 0 and vs.size() != mesh->graph().size())
    return setErrorMessage(
      "Error, the scaling can be negative only if all the vertices are transformed at the same time.");
  forall(const vertex& v, vs) {
    v->pos = cartesian(M * homogeneous(v->pos));
  }
  if(scale < 0) {
    mesh->graph().reverse();
    mesh->updateTriangles();
  }
  mesh->setNormals();
  mesh->updatePositions();
  return true;
}
REGISTER_MESH_PROCESS(TransformMesh);

bool ReverseMesh::operator()(Mesh* mesh)
{
  if(mesh->activeVertices().size() != mesh->graph().size())
    return setErrorMessage("All vertices must be reversed at the same.");

  mesh->graph().reverse();
  mesh->setNormals();
  mesh->updateAll();
  return true;
}
REGISTER_MESH_PROCESS(ReverseMesh);

bool SmoothMesh::operator()(Mesh* mesh, uint passes)
{
  const std::vector<vertex>& vs = mesh->activeVertices();
  vvgraph& S = mesh->graph();
  Progress progress(QString("Smoothing mesh %1").arg(mesh->userId()), passes);
  for(uint i = 0; i < passes; ++i) {
    if(!progress.advance(i))
      userCancel();
    // We'll use the normal as a temp position to save space in the vertex struct
    forall(const vertex& v, vs) {
      float wt = 0;
      v->nrml = Point3f(0, 0, 0);
      forall(const vertex& n, S.neighbors(v)) {
        if(!v->margin) {
          vertex m = S.nextTo(v, n);
          float a = triangleArea(v->pos, n->pos, m->pos);
          wt += a;
          v->nrml += Point3f(a * (n->pos + m->pos) / 2.0);
        } else if(n->margin) {
          float a = (n->pos - v->pos).norm();
          v->nrml += Point3f(a * n->pos);
          wt += a;
        }
      }
      if(wt > 0)
        v->nrml /= wt;
      else
        v->nrml = v->pos;
    }
    forall(const vertex& v, vs)
      v->pos = v->pos * .5 + v->nrml * .5;
  }

  mesh->setNormals();
  mesh->clearImgTex();
  mesh->updateAll();
  return true;
}
REGISTER_MESH_PROCESS(SmoothMesh);

bool ShrinkMesh::operator()(Mesh* mesh, float distance)
{
  const std::vector<vertex>& vs = mesh->activeVertices();

  forall(const vertex& v, vs)
    v->pos -= distance * v->nrml;

  mesh->setNormals();
  mesh->clearImgTex();
  mesh->updateAll();
  return true;
}
REGISTER_MESH_PROCESS(ShrinkMesh);

bool SubdivideMesh::operator()(Mesh* mesh)
{
  vvgraph& S = mesh->graph();
  const std::vector<vertex>& vs = mesh->activeVertices();

  bool selection = vs.size() != S.size();

  Progress progress(QString("Subdividing mesh %1").arg(mesh->userId()), 0);
  vvgraph SS = S.subgraph(vs);
  vvgraph T, T1;
  T = S;
  util::Insert<vvgraph> insert;
  std::vector<vertex> new_vertices;

  int cnt = 0;

  // Add vertex in middle of all existing edges
  forall(const vertex& n, SS) {
    if((++cnt % 1000 == 0) and progress.canceled()) {
      S = T;
      userCancel();
    }
    forall(const vertex& m, T.neighbors(n)) {
      if(SS.contains(m) and m < n)
        continue;
      vertex v = insert(n, m, S);
      new_vertices.push_back(v);
      v->selected = selection;
      v->margin = false;
      v->signal = (m->signal + n->signal) / 2.0;
      v->pos = (m->pos + n->pos) / 2.0;
      v->txpos = (m->txpos + n->txpos) / 2.0;
      if(m->label == n->label)
        v->label = m->label;
    }
  }
  progress.setMaximum(new_vertices.size() + 1);
  T1 = S;
  // Now add new edges
  int i = 0;
  forall(const vertex& v, new_vertices) {
    ++i;
    if((i % 1000 == 0)and !progress.advance(i)) {
      S = T;
      userCancel();
    }
    forall(const vertex& n, T1.neighbors(v)) {
      vertex m = T1.nextTo(v, n);
      vertex nv = T1.nextTo(n, v);
      vertex mv = T1.prevTo(m, v);
      vertex nm = T1.nextTo(nv, n);
      vertex mn = T1.prevTo(mv, m);
      if(nm != mn) {
        if(mn == nv)         // it's a border!
        {
          if(norm(mv->pos - v->pos) < norm(m->pos - nv->pos)) {
            S.spliceAfter(v, m, mv);
            S.spliceBefore(mv, m, v);
          } else {
            S.spliceBefore(nv, mv, m);
            S.spliceAfter(m, mv, nv);
          }
        }
        continue;
      }
      S.spliceBefore(nv, nm, mv);
      S.spliceAfter(mv, mn, nv);
    }
  }

  mesh->setCells(false);
  SETSTATUS("Mesh " << mesh->userId() << " - Subdivide triangles, total vertices:" << S.size());
  mesh->updateAll();
  return true;
}

REGISTER_MESH_PROCESS(SubdivideMesh);

bool LoopSubdivisionMesh::operator()(Mesh* mesh)
{
  vvgraph& S = mesh->graph();
  const std::vector<vertex>& vs = mesh->activeVertices();

  bool selection = vs.size() != S.size();
  vvgraph OldS;

  if(selection)
    OldS = S.subgraph(S);
  else
    OldS = S;

  size_t ss = OldS.size();
  size_t disp = ss / 50;

  Progress progress(QString("Subdividing mesh %1").arg(mesh->userId()), 2 * ss);

  util::Insert<vvgraph> insert;
  std::vector<vertex> new_vertices;
  std::vector<Point3f> old_pos;
  std::unordered_map<vertex, size_t> vertex_index;
  old_pos.reserve(OldS.size());

  for(size_t i = 0; i < OldS.size(); ++i) {
    vertex v = OldS[i];
    vertex_index[v] = i;
    old_pos[i] = v->pos;
  }

  // Add vertex in middle of all existing edges
  for(size_t i = 0; i < OldS.size(); ++i) {
    if((i % disp == 0)and not progress.advance(i))
      userCancel();
    bool surface_v = false;     // The vertex is on the border of the surface
    const vertex& v = OldS[i];
    size_t n = OldS.valence(v);
    Point3f q;

    float w = 0.375 + 0.25 * std::cos(2.0 * M_PI / float(n));
    w *= w;
    w = 0.625 - w;

    forall(const vertex& u, OldS.neighbors(v)) {
      size_t j = vertex_index[u];
      bool boundary = true;
      if((OldS.nextTo(u, v) == OldS.prevTo(v, u))and (OldS.prevTo(u, v) == OldS.nextTo(v, u)))
        boundary = false;
      else {
        if(surface_v)
          q = 0.0;
        surface_v = true;
      }
      if((surface_v and boundary) or not surface_v)
        q += old_pos[j];

      // Only sub-divide every undirected edge once
      if(u < v)
        continue;

      vertex x = insert(u, v, S);
      new_vertices.push_back(x);

      // Set properties of the new vertex
      x->selected = selection;
      x->margin = boundary;
      if(u->label == v->label)
        x->label = u->label;
      if(boundary) {
        x->pos = (old_pos[i] + old_pos[j]) / 2.0;
        x->txpos = (u->txpos + v->txpos) / 2.0;
        x->signal = (u->signal + v->signal) / 2.0;
      } else {
        const vertex& nv = OldS.nextTo(v, u);
        const vertex& pv = OldS.prevTo(v, u);
        size_t kn = vertex_index[nv];
        size_t kp = vertex_index[pv];
        x->pos = (old_pos[i] * 3.0 + old_pos[j] * 3.0 + old_pos[kn] + old_pos[kp]) / 8.0;
        x->txpos = (u->txpos * 3.0 + v->txpos * 3.0 + pv->txpos + nv->txpos) / 8.0;
        x->signal = (u->signal * 3.0 + v->signal * 3.0 + pv->signal + nv->signal) / 8.0;
      }
    }
    if(surface_v) {
      v->pos = 0.75 * v->pos + 0.125 * q;
    } else {
      v->pos = (1 - w) * v->pos + (w / n) * q;
    }
  }

  progress.setMaximum(ss + new_vertices.size());

  // Connect the inside triangles
  size_t i = 0;
  disp = new_vertices.size() / 50;
  forall(const vertex& v, new_vertices) {
    if((i % disp == 0)and not progress.advance(ss + i))
      userCancel();
    ++i;
    const vertex& a = S.anyIn(v);
    const vertex& b = S.nextTo(v, a);
    if(OldS.prevTo(a, b) == OldS.nextTo(b, a)) {
      S.spliceAfter(v, a, S.prevTo(a, v));
      S.spliceBefore(v, b, S.nextTo(b, v));
    }
    if(OldS.nextTo(a, b) == OldS.prevTo(b, a)) {
      S.spliceAfter(v, b, S.prevTo(b, v));
      S.spliceBefore(v, a, S.nextTo(a, v));
    }
  }

  mesh->setCells(false);
  SETSTATUS("Mesh " << mesh->userId() << " - Subdivide triangles, total vertices:" << S.size());
  mesh->updateAll();
  return true;
}

REGISTER_MESH_PROCESS(LoopSubdivisionMesh);

bool subdivideBisect(vvgraph& S, const vertex& v1, const vertex& v2, const vertex& v3, bool selected,
                     std::vector<vertex>* vs)
{
  // v1, v2, v3 must be in order
  vertex a(0), b(0), c(0);

  // Find longest edge and divide
  a = v1;
  b = v2;
  c = v3;
  if((v2->pos - v3->pos).norm() > (a->pos - b->pos).norm()) {
    a = v2;
    b = v3;
    c = v1;
  } else if((v3->pos - v1->pos).norm() > (a->pos - b->pos).norm()) {
    a = v3;
    b = v1;
    c = v2;
  }

  // Insert vertex in long edge if not already there
  if(S.edge(a, b)) {
    vertex d;
    d->type = 'j';
    d->selected = selected;
    if(vs)
      vs->push_back(d);
    S.insert(d);
    S.insertEdge(d, a);
    S.insertEdge(d, b);
    d->pos = (a->pos + b->pos) / 2.0;
    d->signal = (a->signal + b->signal) / 2.0;
    // d->txpos = clipTex((a->txpos + b->txpos)/2.0);
    d->txpos = (a->txpos + b->txpos) / 2.0;
    if(a->label == b->label)
      d->label = a->label;
    S.replace(a, b, d);
    S.replace(b, a, d);
  } else {
    // Already there, must be the last triangle
    const vertex& d = S.prevTo(a, c);
    S.spliceAfter(c, a, d);
    S.spliceBefore(d, a, c);
    return true;
  }

  // Create dividing edge
  vertex d(0);
  if(S.edge(a, c)) {
    d = S.prevTo(a, c);
    S.spliceAfter(c, a, d);
    S.spliceBefore(d, a, c);
    const vertex& v = S.nextTo(c, d);
    // If edge c,b split by v, connect d to v
    if(v != b) {
      S.spliceBefore(d, c, v);
      S.spliceAfter(v, c, d);
    }
  } else if(S.edge(b, c)) {
    d = S.nextTo(b, c);
    S.spliceBefore(c, b, d);
    S.spliceAfter(d, b, c);
    const vertex& v = S.prevTo(c, d);
    // If edge c,a split by v, connect d to v
    if(v != a) {
      S.spliceAfter(d, c, v);
      S.spliceBefore(v, c, d);
    }
  } else {
    SETSTATUS("subdivideBisect::Error:Both incoming short edges divided");
    return false;
  }

  // Find neighbor
  vertex n(0);
  if(S.prevTo(a, d) == S.nextTo(b, d)) {
    n = S.prevTo(a, d);
    // Neighbor already divided
    if(S.edge(d, n))
      return true;
    return subdivideBisect(S, b, a, n, selected, vs);
  }
  return true;
}

bool AdaptiveSubdivideBorderMesh::operator()(Mesh* mesh, float cellMaxArea, float borderDist)
{
  vvgraph& S = mesh->graph();
  const std::vector<vertex>& vs = mesh->activeVertices();
  bool selected = vs.size() != S.size();

  // Find triangles on borders
  mesh->markBorder(borderDist);

  // Copy to temp graph
  vvgraph T = S;
  forall(vertex v, vs)
    forall(vertex n, T.neighbors(v)) {
      vertex m = T.nextTo(v, n);
      if(!mesh->uniqueTri(v, n, m))
        continue;

      // Subdivide triangles on border and clear label
      if(v->minb == 0 or m->minb == 0 or n->minb == 0)
        continue;
      v->label = n->label = m->label = 0;
      float area = triangleArea(v->pos, n->pos, m->pos);
      if(area < cellMaxArea)
        continue;

      if(!subdivideBisect(S, v, n, m, selected)) {
        setErrorMessage("Failed bisecting triangle");
        return false;
      }
    }
  SETSTATUS(S.size() << " total vertices");
  mesh->updateAll();
  mesh->setCells(false);
  return true;
}

REGISTER_MESH_PROCESS(AdaptiveSubdivideBorderMesh);

bool AdaptiveSubdivideSignalMesh::operator()(Mesh* mesh, float cellMaxAreaLow, float cellMaxAreaHigh)
{
  vvgraph& S = mesh->graph();
  const std::vector<vertex>& vs = mesh->activeVertices();

  bool selected = vs.size() != S.size();

  // Find max/min colors
  float maxc = 0, minc = HUGE_VAL;
  forall(const vertex& v, vs) {
    if(v->signal > maxc)
      maxc = v->signal;
    if(v->signal < minc)
      minc = v->signal;
  }
  float rangec = maxc - minc;
  // Return if no real signal difference
  if(fabs(rangec) < 1e-5) {
    setErrorMessage("subdivideTriangles::Error:No signal difference");
    return false;
  }
  // Copy to temp graph
  vvgraph T = S;
  forall(vertex v, vs)
    forall(vertex n, T.neighbors(v)) {
      vertex m = T.nextTo(v, n);
      if(!mesh->uniqueTri(v, n, m))
        continue;

      // Find max signal on triangle
      float maxcol = v->signal;
      if(n->signal > maxcol)
        maxcol = n->signal;
      if(m->signal > maxcol)
        maxcol = m->signal;

      float val = ((maxcol - minc) / rangec);
      float area = triangleArea(v->pos, n->pos, m->pos);
      if(area < interpolate(cellMaxAreaLow, cellMaxAreaHigh, val))
        continue;

      if(!subdivideBisect(S, v, n, m, selected)) {
        setErrorMessage("Failed bisecting triangle");
        return false;
      }
    }
  SETSTATUS(S.size() << " total vertices");
  mesh->updateAll();
  mesh->setCells(false);
  return true;
}

REGISTER_MESH_PROCESS(AdaptiveSubdivideSignalMesh);

bool SubdivideBisectMesh::operator()(Mesh* mesh, float cellMaxArea)
{
  vvgraph& S = mesh->graph();
  const std::vector<vertex>& vs = mesh->activeVertices();

  bool selected = vs.size() != S.size();

  // Copy to temp graph
  vvgraph T = S;
  forall(vertex v, vs)
    forall(vertex n, T.neighbors(v)) {
      vertex m = T.nextTo(v, n);
      if(!mesh->uniqueTri(v, n, m))
        continue;

      float area = triangleArea(v->pos, n->pos, m->pos);
      if(area < cellMaxArea)
        continue;

      if(!subdivideBisect(S, v, n, m, selected)) {
        setErrorMessage("Failed bisecting triangle");
        return false;
      }
    }
  SETSTATUS(S.size() << " total vertices");
  mesh->updateAll();
  mesh->setCells(false);
  return true;
}
REGISTER_MESH_PROCESS(SubdivideBisectMesh);

bool ScaleMesh::operator()(Mesh* mesh, float scaleX, float scaleY, float scaleZ)
{
  vvgraph& S = mesh->graph();
  const std::vector<vertex>& vs = mesh->activeVertices();

  if(scaleX * scaleY * scaleZ < 0) {
    if(vs.size() != S.size())
      return setErrorMessage(
        "Error, symmetries have to be applied globally. Make sure the product of the scalings are positive.");
    S.reverse();
  }

  // Copy to temp graph
  forall(vertex v, vs) {
    if(scaleX != 1.0)
      v->pos.x() *= scaleX;
    if(scaleY != 1.0)
      v->pos.y() *= scaleY;
    if(scaleZ != 1.0)
      v->pos.z() *= scaleZ;
  }
  mesh->updateAll();
  return true;
}
REGISTER_MESH_PROCESS(ScaleMesh);

void markMargin(vvgraph& M, vvgraph& S, bool remborders)
{
  if(M.empty() or S.empty())
    return;

  // Mark margin and borders
  forall(const vertex& v, M) {
    v->margin = false;
    // All vertices with valence <= 2 are border and margin
    if(S.valence(v) <= 2) {
      v->margin = true;
      v->label = -1;
      continue;
    }

    int label = 0, labcount = 0, zcount = 0;
    forall(const vertex& n, S.neighbors(v)) {
      // If there is no edge between a pair of vertices next to each other
      // in a neighborhood, they muct be on the margin
      vertex m = S.nextTo(v, n);
      if(!S.edge(n, m)) {
        v->margin = true;
        v->label = -1;
        break;
      }
      if(n->label == 0)
        zcount++;
      else if(n->label > 0 and n->label != label) {
        labcount++;
        label = n->label;
      }
    }
    // Margin vertices always have border label
    if(v->margin)
      continue;
    // Adjacent to more than one label means a cell boundary
    if(labcount > 1)
      v->label = -1;
    // Clear hanging border lines,
    if(v->label == -1 and remborders) {
      // On zero labeled area, only clear if remborders set (so we can draw)
      if(labcount == 0)
        v->label = 0;
      else if(labcount == 1 and zcount == 0)
        v->label = label;
    }
  }
}

bool MeshDeleteValence::operator()(Mesh* mesh, int startValence, int endValence)
{
  if(startValence < 0 or endValence < 0)
    return setErrorMessage("Start and end valence must be >= 0.");
  vvgraph& S = mesh->graph();
  std::vector<vertex> D;
  forall(const vertex& v, S)
    if(S.valence(v) >= (size_t)startValence and S.valence(v) <= (size_t)endValence)
      D.push_back(v);
  forall(const vertex& v, D)
    S.erase(v);

  SETSTATUS(D.size() << " vertex(es) deleted, total vertices:" << S.size());
  mesh->updateAll();
  mesh->setCells(false);
  return true;
}
REGISTER_MESH_PROCESS(MeshDeleteValence);

bool spliceNhbs(vvgraph& S, vertex& a, vertex& b)
{
  vertex n = S.nextTo(b, a);
  vertex m = S.prevTo(b, a);
  vertex pv = n;

  while(n != m) {
    if(!S.edge(a, n)) {
      S.spliceAfter(a, pv, n);
      S.spliceAfter(n, b, a);
    }
    pv = n;
    n = S.nextTo(b, n);
  }
  return true;
}

bool MergeVertices::operator()(Mesh* mesh)
{
  typedef std::map<vertex, int> VtxIntMap;

  vvgraph& S = mesh->graph();
  vvgraph vs = mesh->activeVertices();
  if(vs.size() < 2)
    throw QString("Error, Must select at least two connected vertices.");

  VtxIntMap weight;
  int count = 0;
  bool vertexDeleted;
  do {
    vertexDeleted = false;
    forall(vertex a, vs) {
      forall(vertex b, S.neighbors(a))
        if(vs.contains(b) and spliceNhbs(S, a, b)) {
          if(weight.count(a) == 0)
            weight[a] = 1;
          if(weight.count(b) == 0)
            weight[b] = 1;
          a->pos = (a->pos * weight[a] + b->pos * weight[b]) / float(weight[a] + weight[b]);
          weight[a] += weight[b];
          S.erase(b);
          count++;
          vertexDeleted = true;
          break;
        }
      if(vertexDeleted)
        break;
    }
  } while(vertexDeleted);

  if(count > 0)
    mesh->updateAll();

  Information::out << count << " vertices deleted from mesh" << endl;
  return true;
}
REGISTER_MESH_PROCESS(MergeVertices);

bool DeleteEdge::operator()(Mesh* mesh)
{
  vvgraph& S = mesh->graph();
  vvgraph vs = mesh->activeVertices();
  if(vs.size() != 2)
    throw QString("Error, Must have exactly two vertices selected.");

  vertex v1 = *(vs.begin()), v2 = *(vs.begin() + 1);
  if(!S.edge(v1, v2))
    throw QString("Error, Vertices have no edge between them.");

  S.eraseEdge(v1, v2);
  S.eraseEdge(v2, v1);

  mesh->updateAll();

  Information::out << "Edge deleted from mesh" << endl;
  return true;
}
REGISTER_MESH_PROCESS(DeleteEdge);

bool MeshDeleteSelection::operator()(Mesh* m)
{
  m->correctSelection(false);
  vvgraph& S = m->graph();

  std::vector<size_t> to_erase;

  size_t n = S.size();
  for(size_t i = 0; i < n; ++i) {
    if(S[i]->selected) to_erase.push_back(i);
  }

  S.erase_sorted_indices(to_erase.begin(), to_erase.end());

  m->updateAll();
  return true;
}
REGISTER_MESH_PROCESS(MeshDeleteSelection);

bool MeshKeepVertices::operator()(Mesh* mesh, bool keep)
{
  const std::vector<vertex>& vs = mesh->activeVertices();

  forall(const vertex& v, vs)
    if(keep)
      v->type = 'l';
    else if(v->type == 'l')
      v->type = 'j';

  mesh->updateAll();
  return true;
}
REGISTER_MESH_PROCESS(MeshKeepVertices);
} // namespace process
} // namespace lgx
