/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PRINCIPAL_ORIENTATIONS_HPP
#define PRINCIPAL_ORIENTATIONS_HPP

#include <Process.hpp>
#include <MeshProcessCellAxis.hpp>

namespace lgx {
namespace process {
///\addtogroup MeshProcess
///@{
/**
 * \class FibrilOrientations <MeshProcessFibril.hpp>
 *
 * Compute principle orientations of lines in the mesh signal. Based on
 * Boudaoud et al., 'FibrilTool, an ImageJ plug-in to quantify fibrillar
 * structures in raw microscopy images', Nature Protocols 2014
 */
class LGXCORE_EXPORT FibrilOrientations : public MeshProcess {
public:
  FibrilOrientations(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_NON_EMPTY | MESH_SIGNAL))
      return false;
    Mesh* mesh = currentMesh();
    bool res = operator()(mesh, parms[0].toFloat(), parms[1].toFloat());
    if(res)
      showResult();
    return res;
  }

  bool operator()(Mesh* mesh, float border, float minAreaRatio);

  void showResult();

  QString folder() const {
    return "Cell Axis/Fibril Orientations";
  }
  QString name() const {
    return "Compute Fibril Orientations";
  }
  QString description() const
  {
    return "Compute principle orientations of lines in the mesh signal.\n"
           "Based on Boudaoud et al., "
           "'FibrilTool, an ImageJ plug-in to quantify fibrillar structures in raw microscopy images'. "
           "Nature Protocols 2014";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Border Size"
                         << "Minimum inner area ratio";
  }
  QStringList parmDescs() const
  {
    return QStringList()
           << "Width of cell border that is not taken into account for the computation."
           << "Minimum ratio of inner area (whole cell - border) vs. total area needed for compuation.";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "1.0"
                         << "0.25";
  }
  QIcon icon() const {
    return QIcon(":/images/PrincipalOrientations.png");
  }
};

/**
 * \class DisplayFibrilOrientations <MeshProcessFibril.hpp>
 *
 * Change the representation of the fibril orientation after it has been
 * computed.
 */
class LGXCORE_EXPORT DisplayFibrilOrientations : public MeshProcess {
public:
  DisplayFibrilOrientations(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;

    bool ok;
    float axisLineWidth = parms[2].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, argument 'Line Color' must be a number");
    float scaleAxisLength = parms[3].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, argument 'Line Scale' must be a number");
    float axisOffset = parms[4].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, argument 'Line Offset' must be a number");
    float threshold = parms[5].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, argument 'Threshold' must be a number");

    return (*this)(currentMesh(), parms[0], QColor(parms[1]), axisLineWidth, scaleAxisLength, axisOffset,
                   threshold);
  }

  bool operator()(Mesh* mesh, const QString& displayHeatMap, const QColor& colorMax, float axisLineWidth,
                  float scaleAxisLength, float axisOffset, float threshold);

  QString folder() const {
    return "Cell Axis/Fibril Orientations";
  }
  QString name() const {
    return "Display Fibril Orientations";
  }
  QString description() const
  {
    return "Display the orientations of fibrils on the image.\n"
           "Only the maximal direction (main orientation) is displayed as a vector.";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Heatmap"
                         << "Line Color"
                         << "Line Width"
                         << "Line Scale"
                         << "Line Offset"
                         << "Threshold";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Display orientation strength (= MaxDirection/MinDirection - 1) as a colormap."
                         << "Line Color"
                         << "Line Width"
                         << "Length of the vectors = Scale * orientation strength."
                         << "Draw the vector ends a bit tilted up for proper display on surfaces."
                         << "Minimal value of orientation strength required for drawing main direction.";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "none"
                         << "red"
                         << "5.0"
                         << "10.0"
                         << "0.1"
                         << "0.0";
  }
  ParmChoiceMap parmChoice() const
  {
    ParmChoiceMap map;
    map[0] = QStringList() << "none"
                           << "Score";
    map[1] = QColor::colorNames();
    return map;
  }
  QIcon icon() const {
    return QIcon(":/images/PrincipalOrientations.png");
  }
};

class LGXCORE_EXPORT LoadFibrilOrientation : public LoadCellAxis {
public:
  LoadFibrilOrientation(const MeshProcess& process)
    : Process(process)
    , LoadCellAxis(process)
  {
  }

  bool operator()(Mesh* mesh, const QString& filename);

  QString folder() const {
    return "Cell Axis/Fibril Orientations";
  }
  QString name() const {
    return "Load Fibril Orientations";
  }
  QString description() const {
    return "Load the orientations of fibrils from a CSV file";
  }
};

class LGXCORE_EXPORT SaveFibrilOrientation : public SaveCellAxis {
public:
  SaveFibrilOrientation(const MeshProcess& process)
    : Process(process)
    , SaveCellAxis(process)
  {
  }

  bool operator()(Mesh* mesh, const QString& filename);

  QString folder() const {
    return "Cell Axis/Fibril Orientations";
  }
  QString name() const {
    return "Save Fibril Orientations";
  }
  QString description() const {
    return "Save the orientations of fibrils to a CSV file";
  }
};
///@}
} // namespace process
} // namespace lgx

#endif
