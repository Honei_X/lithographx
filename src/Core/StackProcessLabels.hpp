/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef STACKPROCESSLABELS_HPP
#define STACKPROCESSLABELS_HPP

#include <Process.hpp>

#include <Misc.hpp>
#include <Stack.hpp>

namespace lgx {
namespace process {
///\addtogroup StackProcess
///@{
/**
 * \class WatershedStack StackProcessLabels.hpp <StackProcessLabels.hpp>
 *
 * Compute the seeded watershed of the current stack using the CImg library.
 */
class LGXCORE_EXPORT WatershedStack : public StackProcess {
public:
  WatershedStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList&)
  {
    if(!checkState().store(STORE_MAIN | STORE_VISIBLE | STORE_NON_LABEL).store(STORE_WORK | STORE_VISIBLE
                                                                               | STORE_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* main = stack->main();
    Store* labels = stack->work();
    return (*this)(stack, main, labels);
  }

  bool operator()(Stack* stack, Store* main, Store* labels);

  QString folder() const {
    return "Segmentation";
  }
  QString name() const {
    return "Watershed3D";
  }
  QString description() const {
    return "3D Watershed on the current labeled stack.";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/SegmentMesh.png");
  }
};

/**
 * \class ConsolidateRegions StackProcessLabels.hpp <StackProcessLabels.hpp>
 *
 * Try to merge segmented regions based on their contact area and the total voxel
 * intensity at their boundary.
 */
class LGXCORE_EXPORT ConsolidateRegions : public StackProcess {
public:
  ConsolidateRegions(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_MAIN | STORE_VISIBLE | STORE_NON_LABEL).store(STORE_WORK | STORE_VISIBLE
                                                                               | STORE_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* main = stack->main();
    Store* labels = stack->work();
    return (*this)(main, labels, parms[0].toUInt(), parms[1].toUInt());
  }

  bool operator()(Store* data, Store* labels, uint threshold, uint minvoxels);

  QString folder() const {
    return "Segmentation";
  }
  QString name() const {
    return "Consolidate Regions";
  }
  QString description() const {
    return "Consilodate regions after watershed overseeding";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Threshold"
                         << "Min Voxels";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Threshold"
                         << "Min Voxels";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "5000"
                         << "100";
  }
  QIcon icon() const {
    return QIcon(":/images/Consolidate.png");
  }
};

/**
 * \class ConsolidateRegionsNormalized StackProcessLabels.hpp <StackProcessLabels.hpp>
 *
 * Variation on the ConsolidateRegions process.
 */
class LGXCORE_EXPORT ConsolidateRegionsNormalized : public StackProcess {
public:
  ConsolidateRegionsNormalized(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_MAIN | STORE_VISIBLE | STORE_NON_LABEL).store(STORE_WORK | STORE_VISIBLE
                                                                               | STORE_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* main = stack->main();
    Store* labels = stack->work();
    return (*this)(main, labels, parms[0].toFloat());
  }

  bool operator()(Store* data, Store* labels, float tolerance);

  QString folder() const {
    return "Segmentation";
  }
  QString name() const {
    return "Consolidate Regions Normalized";
  }
  QString description() const {
    return "Consolidate regions with normalization (slower)";
  }
  QStringList parmNames() const {
    return QStringList() << "Tolerance";
  }
  QStringList parmDescs() const {
    return QStringList() << "Tolerance";
  }
  QStringList parmDefaults() const {
    return QStringList() << "0.3";
  }
  QIcon icon() const {
    return QIcon(":/images/Consolidate.png");
  }
};

/**
 * \class ThresholdLabelDelete StackProcessLabels.hpp <StackProcessLabels.hpp>
 *
 * Delete labels that have too few or too many voxels.
 */
class LGXCORE_EXPORT ThresholdLabelDelete : public StackProcess {
public:
  ThresholdLabelDelete(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* input = stack->currentStore();
    Store* output = stack->work();
    if(!input)
      return false;
    bool res = (*this)(input, output, parms[0].toInt(), parms[1].toInt());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* labels, uint minVoxels, uint maxVoxels);

  QString folder() const {
    return "Segmentation";
  }
  QString name() const {
    return "Delete Labels by Threshold";
  }
  QString description() const {
    return "Delete Labels above/below voxel thresholds";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Min voxels"
                         << "Max voxels";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Min voxels"
                         << "Max voxels";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "1000"
                         << "0";
  }
  QIcon icon() const {
    return QIcon(":/images/DeleteLabel.png");
  }
};

/**
 * \class LocalMaximaStack StackProcessLabels.hpp <StackProcessLabels.hpp>
 *
 * Find the local maxima of the current image
 *
 * \note If the algorithm finds more than 65535 maxima, it will fail as it
 * cannot number them all.
 */
class LGXCORE_EXPORT LocalMaximaStack : public StackProcess {
public:
  LocalMaximaStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_NON_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* input = stack->currentStore();
    Store* labels = stack->work();
    if(!input)
      return false;
    Point3f radius = Point3u(parms[0].toFloat(), parms[1].toFloat(), parms[2].toFloat());
    uint lab = parms[3].toUInt();
    uint minColor = parms[4].toUInt();
    bool res = (*this)(input, labels, radius, lab, minColor);
    if(res) {
      input->hide();
      labels->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* labels, Point3f radius, uint startLabel, uint minColor);

  QString folder() const {
    return "Segmentation";
  }
  QString name() const {
    return "Local Maxima";
  }
  QString description() const {
    return "Find local maxima and possibly number them";
  }
  QStringList parmNames() const
  {
    return QStringList() << QString("X Radius (%1)").arg(UM) << QString("Y Radius (%1)").arg(UM)
                         << QString("Z Radius (%1)").arg(UM) << "Start Label"
                         << "Min Color";
  }
  QStringList parmDescs() const
  {
    return QStringList() << QString("X Radius (%1)").arg(UM) << QString("Y Radius (%1)").arg(UM)
                         << QString("Z Radius (%1)").arg(UM) << "Start Label"
                         << "Min Color";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "5.0"
                         << "5.0"
                         << "5.0"
                         << "2"
                         << "10000";
  }
  QIcon icon() const {
    return QIcon(":/images/LocalMaxima.png");
  }
};

/**
 * \class FillLabelStack StackProcessLabels.hpp <StackProcessLabels.hpp>
 *
 * Replace one label by another one on the stack
 */
class LGXCORE_EXPORT FillLabelStack : public StackProcess {
public:
  FillLabelStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* input = stack->currentStore();
    Store* output = stack->work();
    if(!input)
      return false;
    bool res = (*this)(input, output, parms[0].toUShort(), parms[1].toUShort());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, ushort filledLabel, ushort newLabel);

  QString folder() const {
    return "Segmentation";
  }
  QString name() const {
    return "Fill Label";
  }
  QString description() const {
    return "Replace a label with another one";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Filled label"
                         << "New label";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Filled label"
                         << "New label";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "1000"
                         << "0";
  }
  QIcon icon() const {
    return QIcon(":/images/FillLabel.png");
  }
};

/**
 * \class EraseAtBorderStack StackProcessLabels.hpp <StackProcessLabels.hpp>
 *
 * Erase any label touching the border of the image.
 */
class LGXCORE_EXPORT EraseAtBorderStack : public StackProcess {
public:
  EraseAtBorderStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList&)
  {
    if(!checkState().store(STORE_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* input = stack->currentStore();
    Store* output = stack->work();
    if(!input)
      return false;
    bool res = (*this)(input, output);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output);

  QString folder() const {
    return "Segmentation";
  }
  QString name() const {
    return "Erase at Border";
  }
  QString description() const {
    return "Erase any labelled region touching the border of the image";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QStringList parmDefaults() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/EraseLabel.png");
  }
};

class LGXCORE_EXPORT MajorityFilter : public StackProcess {
public:
  MajorityFilter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* input = stack->currentStore();
    Store* output = stack->work();
    if(!input)
      return false;

    bool ok;
    int nb_steps = parms[0].toInt(&ok);
    if(!ok)
      return setErrorMessage("Error the 'Steps' argument must be an integer number");
    bool res = (*this)(input, output, nb_steps);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, int nb_steps);

  QString folder() const {
    return "Segmentation";
  }
  QString name() const {
    return "Majority Filter";
  }
  QString description() const
  {
    return "Replace all labels with the label that is the most present in its neighborhood.\n"
           "In case of tie with the original color, it doesn't change. Otherwise, the smaller label is chosen.";
  }
  QStringList parmNames() const {
    return QStringList() << "Steps";
  }
  QStringList parmDescs() const {
    return QStringList() << "Number of iteration of the algorithm.";
  }
  QStringList parmDefaults() const {
    return QStringList() << "1";
  }
  QIcon icon() const {
    return QIcon(":/images/FixCorners.png");
  }
};

///@}
} // namespace process
} // namespace lgx

#endif
