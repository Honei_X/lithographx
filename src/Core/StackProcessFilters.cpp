/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "StackProcessFilters.hpp"
#include "SystemProcessLoad.hpp"
#include "Progress.hpp"
#include "Forall.hpp"
#include "Geometry.hpp"
#include "Information.hpp"
#include "cuda/CudaExport.hpp"
#include <thrust/transform.h>

#include "CImg.h"
using namespace cimg_library;

typedef CImg<ushort> CImgUS;

namespace lgx {
namespace process {

REGISTER_STACK_PROCESS(StackSwapBytes);

bool AverageStack::operator()(const Store* input, Store* output, Point3i radius, uint steps)
{
  if(steps <= 0)
    throw std::string("Average voxels, steps must be > 0");
  const Stack* stack = output->stack();
  SETSTATUS("Average( " << radius << ", " << steps << ")");
  Progress progress(QString("Average pixels for Stack %1").arg(stack->userId()), 100);
  const HVecUS& data = input->data();
  HVecUS& outputData = output->data();
  Point3u imgSize = stack->size();
  if(averageGPU(imgSize, radius, data, outputData)) {
    setErrorMessage("Error while running CUDA process");
    return false;
  }
  for(uint i = 1; i < steps; i++) {
    if(!progress.advance(1))
      userCancel();
    if(averageGPU(imgSize, radius, outputData, outputData)) {
      setErrorMessage("Error while running CUDA process");
      return false;
    }
  }
  output->changed();
  output->copyMetaData(input);
  return true;
}

REGISTER_STACK_PROCESS(AverageStack);

bool BinarizeStack::operator()(const Store* input, Store* output, ushort threshold)
{
  const HVecUS& idata = input->data();
  HVecUS& data = output->data();
  Progress progress("Binarize stack", 100);
  for(uint idx = 0; idx < data.size(); idx++) {
    data[idx] = (idata[idx] > threshold) ? 65535 : 0;
    if(idx % 100 == 0 and !progress.advance(idx / 100))
      userCancel();
  }
  output->changed();
  output->copyMetaData(input);
  return true;
}

REGISTER_STACK_PROCESS(BinarizeStack);

bool BrightenStack::operator()(const Store* input, Store* output, float brightness)
{
  if(input != output)
    output->data() = input->data();
  HVecUS& data = output->data();
  Progress progress("Brighten/Darken stack", 100);
  for(uint idx = 0; idx < data.size(); idx++) {
    float pix = float(data[idx]) * brightness;
    if(pix < 0)
      pix = 0;
    else if(pix > 0xFFFF)
      pix = 0xFFFF;
    data[idx] = ushort(pix);
    if(idx % 100 == 0 and !progress.advance(idx / 100))
      userCancel();
  }
  output->changed();
  output->copyMetaData(input);
  return true;
}

REGISTER_STACK_PROCESS(BrightenStack);

bool ColorGradient::operator()(const Store* input, Store* output, float colorGradDiv)
{
  Point3u imgSize = input->stack()->size();
  const HVecUS& data = input->data();
  HVecUS& workData = output->data();
  if(colorGradGPU(imgSize, colorGradDiv, data, workData)) {
    setErrorMessage("Error while running CUDA process");
    return false;
  }
  output->changed();
  output->copyMetaData(input);
  return true;
}

REGISTER_STACK_PROCESS(ColorGradient);

bool FilterStack::operator()(const Store* input, Store* output, uint lowFilter, uint highFilter)
{
  const Stack* stack = output->stack();
  const HVecUS& data = input->data();
  HVecUS& workData = output->data();
  int highCnt = 0, lowCnt = 0;

  lowFilter = trim(lowFilter, uint(0), uint(0xFFFF));
  highFilter = trim(highFilter, uint(0), uint(0xFFFF));

  for(uint idx = 0; idx < data.size(); idx++) {
    if(highFilter > 0 and data[idx] > highFilter) {
      highCnt++;
      workData[idx] = highFilter;
    } else if(data[idx] < lowFilter) {
      lowCnt++;
      workData[idx] = 0;
    } else
      workData[idx] = data[idx];
  }
  SETSTATUS("Stack " << stack->userId() << " Filtered out " << lowCnt << " low pixels, and " << highCnt
                     << " high pixels");
  output->changed();
  output->copyMetaData(input);
  return true;
}

REGISTER_STACK_PROCESS(FilterStack);

bool InvertStack::operator()(const Store* input, Store* output)
{
  const HVecUS& data = input->data();
  HVecUS& workData = output->data();
  for(uint i = 0; i < data.size(); i++)
    workData[i] = 0xFFFF - data[i];
  output->changed();
  output->copyMetaData(input);
  return true;
}

REGISTER_STACK_PROCESS(InvertStack);

bool GaussianBlurStack::operator()(const QStringList& parms)
{
    if(!checkState().store(STORE_NON_EMPTY))
        return false;
#ifndef THRUST_BACKEND_CUDA
    if(validProcessName("Stack", "ITK Smoothing Recursive Gaussian Image Filter"))
        return runProcess("Stack", "ITK Smoothing Recursive Gaussian Image Filter", parms);
#endif
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool ok;
    float x = parms[0].toFloat(&ok);
    if(!ok)
        return setErrorMessage("X parameter must be a number");
    float y = parms[1].toFloat(&ok);
    if(!ok)
        return setErrorMessage("Y parameter must be a number");
    float z = parms[2].toFloat(&ok);
    if(!ok)
        return setErrorMessage("Z parameter must be a number");
    Point3f sigma(x, y, z);
    bool res = (*this)(input, output, sigma);
    if(res) {
        input->hide();
        output->show();
    }
    return res;
}

bool GaussianBlurStack::operator()(const Store* input, Store* output, Point3f sigma)
{
  const Stack* stack = input->stack();
  gaussianBlurGPU(stack->size(), stack->step(), sigma, input->data(), output->data());
  output->copyMetaData(input);
  output->changed();
  return true;
}

REGISTER_STACK_PROCESS(GaussianBlurStack);

bool SharpenStack::operator()(const Store* input, Store* output, const Point3f sigma, const float amount)
{
  const Stack* stack = input->stack();
  sharpenGPU(stack->size(), stack->step(), sigma, amount, input->data(), output->data());
  output->copyMetaData(input);
  output->changed();
  return true;
}

REGISTER_STACK_PROCESS(SharpenStack);

bool ApplyKernelStack::operator()(const Store* input, Store* output, const HVecF& kernelX, const HVecF& kernelY,
                                  const HVecF& kernelZ)
{
  const Stack* stack = input->stack();

  if((kernelX.size() > 0 and kernelX.size() < 3)or (kernelY.size() > 0 and kernelY.size() < 3)
     or (kernelZ.size() > 0 and kernelZ.size() < 3)) {
    setErrorMessage("Kernel must be empty or at least 3 numbers long");
    return false;
  }
  if((kernelX.size() > 0 and (kernelX.size() % 2) == 0)or (kernelY.size() > 0 and (kernelY.size() % 2) == 0)
     or (kernelZ.size() > 0 and (kernelZ.size() % 2) == 0)) {
    setErrorMessage("Kernel size must be odd");
    return false;
  }
  applyKernelGPU(stack->size(), kernelX, kernelY, kernelZ, input->data(), output->data());
  output->copyMetaData(input);
  output->changed();
  return true;
}

REGISTER_STACK_PROCESS(ApplyKernelStack);

bool NormalizeStack::operator()(const Store* input, Store* output, Point3f radius, Point3f sigma, uint thresh,
                                float blurFactor)
{
  if(blurFactor < 0 or blurFactor > 1)
    throw(QString("Blur Factor must be between 0 and 1"));
  if(thresh > 65535)
    throw(QString("Threshold must be between 0 and 65535"));
  if(radius.x() < 0 or radius.y() < 0 or radius.z() < 0)
    throw(QString("Radii for blurring must all be >= 0"));

  const Stack* stack = input->stack();
  const HVecUS& data = input->data();

  Point3u voxRadius = toVoxelsCeil(radius, stack->step());

  HVecUS bDilate(input->data().size());
  HVecUS tDilate(input->data().size());
  HVecUS& workData = output->data();

  dilateGPU(stack->size(), voxRadius, input->data(), tDilate);
  if(norm(sigma) == 0) {
    bDilate = tDilate;
    blurFactor = 0;
  }
  else {
#ifndef THRUST_BACKEND_CUDA
    using std::swap;
    bool useITK = validProcessName("Stack", "ITK Smoothing Recursive Gaussian Image Filter");
    if(useITK) {
      QStringList itk_params
          = { QString::number(radius.x()), QString::number(radius.y()), QString::number(radius.z()) };

      HVecUS save;
      if(input == output)
        save = workData;
      if(not runProcess("Stack", "ITK Smoothing Recursive Gaussian Image Filter", itk_params))
        return false;
      swap(workData, bDilate);
      if(input == output)
        swap(save, workData);
    }
    else
#endif
      gaussianBlurGPU(stack->size(), stack->step(), sigma, input->data(), bDilate);
    dilateGPU(stack->size(), voxRadius, bDilate, bDilate);
  }
  if(blurFactor > 1.0)
    blurFactor = 1.0;

  // Blend the value based on the range and the blurred range
  for(uint idx = 0; idx < workData.size(); idx++)
    if(tDilate[idx] > thresh)
      workData[idx] = trim(int((blurFactor * trim(float(data[idx]) / bDilate[idx], 0.0f, 1.0f)
                                   + (1.0 - blurFactor) * data[idx] / tDilate[idx]) * 0xFFFF),
                           0, 0xFFFF);
    else
      workData[idx] = 0;

  output->copyMetaData(input);
  output->changed();
  return true;
}

REGISTER_STACK_PROCESS(NormalizeStack);

bool NormalizeStackVariance::operator()(const Store* input, Store* output, Point3f radius, bool norm_mean,
                                        float min_mean, float min_var)
{
  const HVecUS& indata = input->data();
  const Stack* stack = input->stack();
  HVecUS& outdata = output->data();

  HVecUS sqdata(indata.size());
  HVecUS vardata(indata.size());
  HVecUS meandata(indata.size());

#ifndef THRUST_BACKEND_CUDA
  using std::swap;
  bool useITK = validProcessName("Stack", "ITK Smoothing Recursive Gaussian Image Filter");
  QStringList itk_params;
  if(useITK) {
    itk_params << QString::number(radius.x())
               << QString::number(radius.y())
               << QString::number(radius.z());

    if(input == output)
      sqdata = indata;
    if(not runProcess("Stack", "ITK Smoothing Recursive Gaussian Image Filter", itk_params))
      return false;
    std::swap(outdata, meandata);
    if(input == output)
      std::swap(sqdata, outdata);
  }
  else
#endif
    gaussianBlurGPU(stack->size(), stack->step(), radius, indata, meandata);

  float global_mean = 0.;

#pragma omp parallel for reduction(+ : global_mean)
  for(size_t k = 0; k < meandata.size(); ++k) {
    int value = indata[k];
    global_mean += value;
    value -= meandata[k];
    value *= value;
    sqdata[k] = value >> 16;
  }

  global_mean /= meandata.size();
  int gmean = (norm_mean ? global_mean : 0);

#ifndef THRUST_BACKEND_CUDA
  if(useITK) {
    HVecUS save;
    if(input == output)
      save = indata;
    outdata = sqdata;
    if(not runProcess("Stack", "ITK Smoothing Recursive Gaussian Image Filter", itk_params))
      return false;
    std::swap(outdata, vardata);
    if(input == output)
      std::swap(save, outdata);
  }
  else
#endif
    gaussianBlurGPU(stack->size(), stack->step(), radius, sqdata, vardata);

  // Find max variance
  ushort max_var = 0;
#pragma omp parallel for reduction(max : max_var)
  for(size_t k = 0; k < vardata.size(); ++k)
    if(vardata[k] > max_var)
      max_var = vardata[k];

  float maxv = sqrt(max_var);

#pragma omp parallel for
  for(size_t k = 0; k < vardata.size(); ++k) {
    float val = sqrt(vardata[k]) / maxv;
    if(val <= min_var or (norm_mean and meandata[k] <= min_mean))
      outdata[k] = indata[k];
    else {
      int d = (norm_mean ? int(indata[k]) - int(meandata[k]) + gmean : indata[k]);
      d = d / val;
      if(d >= (1 << 16))
        outdata[k] = 0xFFFF;
      else if(d < 0)
        outdata[k] = 0;
      else
        outdata[k] = ushort(d);
    }
  }

  output->copyMetaData(input);
  output->changed();
  return true;
}

REGISTER_STACK_PROCESS(NormalizeStackVariance);

// CImg filters
bool CImgGaussianBlurStack::operator()(const Store* input, Store* output, uint radius)
{
  if(input != output)
    output->data() = input->data();
  Point3u size = input->stack()->size();
  CImgUS image(output->data().data(), size.x(), size.y(), size.z(), 1, true);
  image.blur(radius);
  output->changed();
  output->copyMetaData(input);
  return true;
}

REGISTER_STACK_PROCESS(CImgGaussianBlurStack);

bool CImgLaplaceStack::operator()(const Store* input, Store* output)
{
  if(input != output)
    memcpy(output->data().data(), input->data().data(), input->data().size() * 2);
  Point3u size = input->stack()->size();
  CImgUS image(output->data().data(), size.x(), size.y(), size.z(), 1, true);
  image.laplacian();
  output->changed();
  output->copyMetaData(input);
  return true;
}

REGISTER_STACK_PROCESS(CImgLaplaceStack);
} // namespace process
} // namespace lgx
