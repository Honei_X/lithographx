/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef STACKPROCESS_HPP
#define STACKPROCESS_HPP

/**
 * \file StackProcess.hpp
 * Contains various stack processes
 */

#include <Process.hpp>

#include <Misc.hpp>
#include <Stack.hpp>
#include <Store.hpp>

namespace lgx {
namespace process {

///\addtogroup StackProcess
///@{
/**
 * \class Annihilate StackProcess.hpp <StackProcess.hpp>
 *
 * Delete all but a layour of the stack just "below" the mesh.
 */
class LGXCORE_EXPORT Annihilate : public StackProcess {
public:
  Annihilate(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_NON_EMPTY).mesh(MESH_NON_EMPTY))
      return false;
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();

    bool fill = stringToBool(parms[0]);
    bool res
      = (*this)(input, output, currentMesh(), parms[2].toFloat(), parms[3].toFloat(), fill, parms[1].toUInt());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  /**
   * Annihilate the stack
   * \param input Input stack
   * \param output Stack storing the result
   * \param mesh Mesh used
   * \param minDist Distance from the mesh, taken "below" (i.e. against the normal)
   * \param maxDist Distance from the mesh, taken "below" (i.e. against the normal)
   */
  bool operator()(const Store* input, Store* output, const Mesh* mesh, float minDist, float maxDist,
                  bool fill, uint fillval);

  QString name() const {
    return "Annihilate";
  }
  QString description() const {
    return "Keep or fill a layer near the mesh";
  }
  QString folder() const {
    return "Mesh Interaction";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Fill"
                         << "Fill Val" << QString("Min Dist(%1)").arg(UM)
                         << QString("Max Dist(%1)").arg(UM);
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Fill the layer with specified value, or keep the original data."
                         << "Value to fill the volume with."
                         << "Minimal distance from layer to mesh."
                         << "Maximal distance from layre to mesh";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "No"
                         << "30000"
                         << "1.0"
                         << "5.0";
  }
  ParmChoiceMap parmChoice() const
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    return map;
  }
  QIcon icon() const {
    return QIcon(":/images/Annihilate.png");
  }
};

/**
 * \class AutoScaleStack StackProcess.hpp <StackProcess.hpp>
 *
 * Scale the stack intensity to fill exactly the whole range.
 */
class LGXCORE_EXPORT AutoScaleStack : public StackProcess {
public:
  AutoScaleStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& )
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    bool res = (*this)(input, output);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  /**
   * Scale the stack values, so as to use the whole range.
   */
  bool operator()(const Store* store, Store* output);

  QString name() const {
    return "Autoscale Stack";
  }
  QString description() const {
    return "Scale the stack intensity to fill exactly the whole range.";
  }
  QString folder() const {
    return "Filters";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/Palette.png");
  }
};

/**
 * \class ApplyTransferFunction StackProcess.hpp <StackProcess.hpp>
 *
 * Transform the stack to reflect the transfer function in use.
 */
class LGXCORE_EXPORT ApplyTransferFunction : public StackProcess {
public:
  ApplyTransferFunction(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    bool res
      = (*this)(input, output, parms[0].toFloat(), parms[1].toFloat(), parms[2].toFloat(), parms[3].toFloat());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  /**
   * Apply the transfer function. The final value is computed as a linear combination of the red, green, blue and
   * alpha channels.
   * \param store Input store
   * \param output Output store
   * \param red Coefficient for the red channel
   * \param green Coefficient for the green channel
   * \param blue Coefficient for the blue channel
   * \param alpha Coefficient for the alpha channel
   * \note If the sum of the coefficient is not 1, values can globally increase/decrease
   */
  bool operator()(Store* store, Store* output, float red, float green, float blue, float alpha);

  QString name() const {
    return "Apply Transfer Function";
  }
  QString description() const {
    return "Apply the transfer function to the stack (modifies voxel values).";
  }
  QString folder() const {
    return "Filters";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Red"
                         << "Green"
                         << "Blue"
                         << "Alpha";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Red"
                         << "Green"
                         << "Blue"
                         << "Alpha";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "0"
                         << "0"
                         << "0"
                         << "1";
  }
  QIcon icon() const {
    return QIcon(":/images/Palette.png");
  }
};

/**
 * \class AverageStores StackProcess.hpp <StackProcess.hpp>
 *
 * Average the values of both stores.
 */
class LGXCORE_EXPORT AverageStores : public StackProcess {
public:
  AverageStores(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList&)
  {
    if(!checkState().store(STORE_MAIN | STORE_NON_EMPTY | STORE_NON_LABEL).store(STORE_WORK | STORE_NON_EMPTY
                                                                                 | STORE_NON_LABEL))
      return false;
    Stack* stack = currentStack();
    bool res = (*this)(stack);
    if(res) {
      stack->main()->hide();
      stack->work()->show();
    }
    return res;
  }
  /**
   * Average the stores
   * \param stack Stack containing the stores to average. The result will be placed in the work store.
   */
  bool operator()(Stack* stack);

  QString folder() const {
    return "Multi-stack";
  }
  QString name() const {
    return "Average Main and Work Stacks";
  }
  QString description() const {
    return "Compute average of main and work stacks";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/AverageStacks.png");
  }
};

/**
 * \class BlobDetect StackProcess.hpp <StackProcess.hpp>
 *
 * Find and label blobs in an image.
 */
class LGXCORE_EXPORT BlobDetect : public StackProcess {
public:
  BlobDetect(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_NON_EMPTY | STORE_NON_LABEL))
      return false;
    bool wat = stringToBool(parms[0]);
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    bool res = (*this)(input, output, wat, parms[1].toUInt());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, bool watershed, uint startlabel);

  QString folder() const {
    return "Segmentation";
  }
  QString name() const {
    return "Blob Detect";
  }
  QString description() const {
    return "Find and label blobs in an image";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Use watershed"
                         << "Start Label";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Use watershed"
                         << "Start Label";
  }
  ParmChoiceMap parmChoice() const
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    return map;
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "No"
                         << "1";
  }
  QIcon icon() const {
    return QIcon(":/images/BlobDetect.png");
  }
};

/**
 * \class ClearWorkStack StackProcess.hpp <StackProcess.hpp>
 *
 * Erase the content of the work stack
 */
class LGXCORE_EXPORT ClearWorkStack : public StackProcess {
public:
  ClearWorkStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_WORK))
      return false;
    return (*this)(currentStack(), parms[0].toUInt());
  }

  bool operator()(Stack* stack, uint fillValue);

  QString folder() const {
    return "System";
  }
  QString name() const {
    return "Clear Work Stack";
  }
  QString description() const {
    return "Clear the work stack";
  }
  QStringList parmNames() const {
    return QStringList() << "Fill value";
  }
  QStringList parmDescs() const {
    return QStringList() << "Fill value";
  }
  QStringList parmDefaults() const {
    return QStringList() << "0";
  }
  QIcon icon() const {
    return QIcon(":/images/ClearStack.png");
  }
};

/**
 * \class ClearMainStack StackProcess.hpp <StackProcess.hpp>
 *
 * Erase the content of the main stack
 */
class LGXCORE_EXPORT ClearMainStack : public StackProcess {
public:
  ClearMainStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_MAIN))
      return false;
    return (*this)(currentStack(), parms[0].toUInt());
  }

  bool operator()(Stack* stack, uint fillValue);

  QString folder() const {
    return "System";
  }
  QString name() const {
    return "Clear Main Stack";
  }
  QString description() const {
    return "Clear the main stack";
  }
  QStringList parmNames() const {
    return QStringList() << "Fill value";
  }
  QStringList parmDescs() const {
    return QStringList() << "Fill value";
  }
  QStringList parmDefaults() const {
    return QStringList() << "0";
  }
  QIcon icon() const {
    return QIcon(":/images/ClearStack.png");
  }
};

/**
 * \class ClipStack StackProcess.hpp <StackProcess.hpp>
 *
 * Apply the active clipping planes to the current stack
 */
class LGXCORE_EXPORT ClipStack : public StackProcess {
public:
  ClipStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList&)
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    bool res = (*this)(input, output);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output);

  QString folder() const {
    return "Mesh Interaction";
  }
  QString name() const {
    return "Clip Stack";
  }
  QString description() const {
    return "Trim stack to clipping planes";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/ClipStack.png");
  }
};

/**
 * \class CopyMainToWork StackProcess.hpp <StackProcess.hpp>
 *
 * Copy the content of the main stack into the work stack
 */
class LGXCORE_EXPORT CopyMainToWork : public StackProcess {
public:
  CopyMainToWork(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList&)
  {
    if(!checkState().store(STORE_MAIN | STORE_NON_EMPTY))
      return false;
    Stack* stack = currentStack();
    bool res = (*this)(stack);
    if(res) {
      stack->main()->hide();
      stack->work()->show();
    }
    return res;
  }

  bool operator()(Stack* stack);

  QString folder() const {
    return "Multi-stack";
  }
  QString name() const {
    return "Copy Main to Work Stack";
  }
  QString description() const {
    return "Copy Main to Work Stack";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/CopyMainToWork.png");
  }
};

/**
 * \class CopyWorkToMain StackProcess.hpp <StackProcess.hpp>
 *
 * Copy the content of the work stack into the main stack
 */
class LGXCORE_EXPORT CopyWorkToMain : public StackProcess {
public:
  CopyWorkToMain(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList&)
  {
    if(!checkState().store(STORE_WORK | STORE_NON_EMPTY))
      return false;
    Stack* stack = currentStack();
    bool res = (*this)(stack);
    if(res) {
      stack->work()->hide();
      stack->main()->show();
    }
    return res;
  }

  bool operator()(Stack* stack);

  QString folder() const {
    return "Multi-stack";
  }
  QString name() const {
    return "Copy Work to Main Stack";
  }
  QString description() const {
    return "Copy Work to Main Stack";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/CopyWorkToMain.png");
  }
};

/**
 * \class CopySwapStacks <StackProcess.hpp>
 *
 * Copy or swap stacks between stack 1 and 2.
 */
class LGXCORE_EXPORT CopySwapStacks : public StackProcess {
public:
  CopySwapStacks(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    // Stack *stack = currentStack();
    bool res = (*this)(parms[0], parms[1]);
    // if(res)
    //{
    //  stack->main()->hide();
    //  stack->work()->show();
    //}
    return res;
  }

  bool operator()(const QString& storeStr, const QString& actionStr);

  QString folder() const {
    return "Multi-stack";
  }
  QString name() const {
    return "Swap or Copy Stack 1 and 2";
  }
  QString description() const {
    return "Copy or Swap Stack 1 and 2";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Store"
                         << "Action";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Store"
                         << "Action";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "Main"
                         << "1 -> 2";
  }
  ParmChoiceMap parmChoice() const
  {
    ParmChoiceMap map;
    map[0] = storeChoice();
    map[1] = QStringList() << "1 -> 2"
                           << "1 <- 2"
                           << "1 <-> 2";
    return map;
  }

  QIcon icon() const {
    return QIcon(":/images/CopySwapStacks.png");
  }
};

/**
 * \class StackMeshProcess StackProcess.hpp <StackProcess.hpp>
 *
 * Base class for a process that either fill or erase the inside part of a mesh in a stack.
 */
class LGXCORE_EXPORT StackMeshProcess : public StackProcess {
public:
  StackMeshProcess(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms, bool fill)
  {
    if(!checkState().store(STORE_NON_EMPTY).mesh(MESH_NON_EMPTY))
      return false;
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    const Mesh* mesh = currentMesh();
    uint fillValue = 0;
    if(fill)
      fillValue = parms[0].toUInt();
    bool res = (*this)(input, output, mesh, fill, fillValue);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, const Mesh* mesh, bool fill, uint fillValue);
};

/**
 * \class FillStackToMesh StackProcess.hpp <StackProcess.hpp>
 *
 * Fill the volume contained by a closed mesh with a pre-defined intensity.
 */
class LGXCORE_EXPORT FillStackToMesh : public StackMeshProcess {
public:
  FillStackToMesh(const StackProcess& process)
    : Process(process)
    , StackMeshProcess(process)
  {
  }

  using StackMeshProcess::operator();

  bool operator()(const QStringList& parms) {
    return (*this)(parms, true);
  }

  QString name() const {
    return "Fill Stack from Mesh";
  }
  QString folder() const {
    return "Mesh Interaction";
  }
  QString description() const {
    return "Fill volume contained by closed mesh";
  }
  QStringList parmNames() const {
    return QStringList() << "Fill Value";
  }
  QStringList parmDescs() const {
    return QStringList() << "Fill Value";
  }
  QStringList parmDefaults() const {
    return QStringList() << "32000";
  }
  QIcon icon() const {
    return QIcon(":/images/TrimStack.png");
  }
};

/**
 * \class TrimStackProcess StackProcess.hpp <StackProcess.hpp>
 *
 * Set to 0 any voxel not contained within the closed mesh.
 */
class LGXCORE_EXPORT TrimStackProcess : public StackMeshProcess {
public:
  TrimStackProcess(const StackProcess& process)
    : Process(process)
    , StackMeshProcess(process)
  {
  }

  using StackMeshProcess::operator();

  bool operator()(const QStringList& parms) {
    return (*this)(parms, false);
  }

  QString folder() const {
    return "Mesh Interaction";
  }
  QString name() const {
    return "Trim Stack";
  }
  QString description() const {
    return "Trim parts of stack which are not contained within closed mesh.";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/TrimStack.png");
  }
};

/**
 * \class FillStack3D StackProcess.hpp <StackProcess.hpp>
 *
 * Fill the stack with labels from a labeled 3D mesh.
 */
class LGXCORE_EXPORT FillStack3D : public StackProcess {
public:
  FillStack3D(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& )
  {
    if(!checkState().store(STORE_NON_EMPTY).mesh(MESH_NON_EMPTY))
      return false;
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    const Mesh* mesh = currentMesh();
    bool res = (*this)(input, output, mesh);
    if(res) {
      input->hide();
      output->show();
      output->setLabels(true);
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, const Mesh* mesh);

  QString name() const {
    return "Fill Stack from 3D Mesh";
  }
  QString folder() const {
    return "Mesh Interaction";
  }
  QString description() const {
    return "Fill stack contained by labeled 3D mesh";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QList<float> default_values() const {
    return QList<float>();
  }
  QIcon icon() const {
    return QIcon(":/images/FillStack3D.png");
  }
};

/**
 * \class SwapStacks StackProcess.hpp <StackProcess.hpp>
 *
 * Swap the main and work stores of a stack
 */
class LGXCORE_EXPORT SwapStacks : public StackProcess {
public:
  SwapStacks(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList&)
  {
    if(!checkState().stack(STACK_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    if(!s) {
      setErrorMessage("You need to select a stack to launch this process.");
      return false;
    }
    return (*this)(s);
  }

  bool operator()(Stack* stack);

  QString folder() const {
    return "Multi-stack";
  }
  QString name() const {
    return "Swap Main and Work Stacks";
  }
  QString description() const {
    return "Swap the main and work data of the current stack.";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/SwapStacks.png");
  }
};

/**
 * \class ReverseStack StackProcess.hpp <StackProcess.hpp>
 *
 * Reverse the direction of the selected axes.
 */
class LGXCORE_EXPORT ReverseStack : public StackProcess {
public:
  ReverseStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool reverse_x = stringToBool(parms[0]);
    bool reverse_y = stringToBool(parms[1]);
    bool reverse_z = stringToBool(parms[2]);
    if((*this)(output, input, reverse_x, reverse_y, reverse_z)) {
      input->hide();
      output->show();
      return true;
    }
    return false;
  }

  bool operator()(Store* output, const Store* input, bool x, bool y, bool z);

  QString folder() const {
    return "Canvas";
  }
  QString name() const {
    return "Reverse Axes";
  }
  QString description() const
  {
    return "Reverse the direction of the selected axes.\n"
           "Press A-key to display the axis.";
  }
  QStringList parmNames() const
  {
    return QStringList() << "X"
                         << "Y"
                         << "Z";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "X"
                         << "Y"
                         << "Z";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "No"
                         << "No"
                         << "Yes";
  }
  ParmChoiceMap parmChoice() const
  {
    ParmChoiceMap map;
    map[0] = map[1] = map[2] = booleanChoice();
    return map;
  }
  QIcon icon() const {
    return QIcon(":/images/Resize.png");
  }
};

/**
 * \class ChangeVoxelSize StackProcess.hpp <StackProcess.hpp>
 *
 * Change the size of the stack's voxel, without changing the data itself.
 */
class LGXCORE_EXPORT ChangeVoxelSize : public StackProcess {
public:
  ChangeVoxelSize(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().stack(STACK_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    bool ok;
    float x, y, z;
    if(parms[0].isEmpty()) x = 0.f;
    else {
      x = parms[0].toFloat(&ok);
      if(not ok) return setErrorMessage("Error, bad value for x parameter");
    }
    if(parms[1].isEmpty()) y = 0.f;
    else {
      y = parms[1].toFloat(&ok);
      if(not ok) return setErrorMessage("Error, bad value for y parameter");
    }
    if(parms[2].isEmpty()) z = 0.f;
    else {
      z = parms[2].toFloat(&ok);
      if(not ok) return setErrorMessage("Error, bad value for z parameter");
    }
    Point3f nv(x, y, z);
    return (*this)(s, nv);
  }

  bool operator()(Stack* stack, Point3f nv);

  QString folder() const {
    return "Canvas";
  }
  QString name() const {
    return "Change Voxel Size";
  }
  QString description() const {
    return "Change the size of a voxel (i.e. doesn't change the data)";
  }
  QStringList parmNames() const
  {
    return QStringList() << QString::fromWCharArray(L"X (\xb5m)") << QString::fromWCharArray(L"Y (\xb5m)")
                         << QString::fromWCharArray(L"Z (\xb5m)");
  }
  QStringList parmDescs() const
  {
    return QStringList() << QString::fromWCharArray(L"X (\xb5m)") << QString::fromWCharArray(L"Y (\xb5m)")
                         << QString::fromWCharArray(L"Z (\xb5m)");
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "1.0"
                         << "1.0"
                         << "1.0";
  }
  QIcon icon() const {
    return QIcon(":/images/Resize.png");
  }
};

/**
 * \class ResizeCanvas StackProcess.hpp <StackProcess.hpp>
 *
 * Resize the stack to add or remove voxels.
 */
class LGXCORE_EXPORT ResizeCanvas : public StackProcess {
public:
  ResizeCanvas(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().stack(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Point3i ds(parms[2].toInt(), parms[3].toInt(), parms[4].toInt());
    return (*this)(s, stringToBool(parms[0]), stringToBool(parms[1]), ds);
  }

  bool operator()(Stack* stack, bool isRelative, bool center, Point3i ds);

  QString folder() const {
    return "Canvas";
  }
  QString name() const {
    return "Resize Canvas";
  }
  QString description() const
  {
    return "Resize the stack to add or remove voxels.\n"
           "Make sure BBox is checked on before running.";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Relative"
                         << "Center"
                         << "X"
                         << "Y"
                         << "Z";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "If true, X, Y and Z are given in percentage, if false in voxels."
                         << "New canvas centered as the old one, or else use the bottom left corner as reference."
                         << "Canvas size for X direction, in percentage or voxels."
                         << "Canvas size for Y direction, in percentage or voxels."
                         << "Canvas size for Z direction, in percentage or voxels.";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "Yes"
                         << "Yes"
                         << "0"
                         << "0"
                         << "0";
  }
  ParmChoiceMap parmChoice() const
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    map[1] = booleanChoice();
    return map;
  }
  QIcon icon() const {
    return QIcon(":/images/Resize.png");
  }
};

/**
 * \class ScaleStack StackProcess.hpp <StackProcess.hpp>
 *
 * Scale the stack.
 */
class LGXCORE_EXPORT ScaleStack : public StackProcess {
public:
  ScaleStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().stack(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Point3f newsize(parms[1].toFloat(), parms[2].toFloat(), parms[3].toFloat());
    return (*this)(s, newsize, stringToBool(parms[0]));
  }

  bool operator()(Stack* stack, Point3f newsize, bool percent);

  QString folder() const {
    return "Canvas";
  }
  QString name() const {
    return "Scale Stack";
  }
  QString description() const {
    return "Scale the stack.";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Percent"
                         << "X"
                         << "Y"
                         << "Z";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Percent"
                         << "X"
                         << "Y"
                         << "Z";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "Yes"
                         << "0.0"
                         << "0.0"
                         << "0.0";
  }
  ParmChoiceMap parmChoice() const
  {
    ParmChoiceMap map;
    map[0] = map[1] = booleanChoice();
    return map;
  }
  QIcon icon() const {
    return QIcon(":/images/Scale.png");
  }
};

/**
 * \class ShiftStack StackProcess.hpp <StackProcess.hpp>
 *
 * Shift main and work stores within the canvas (e.g. the voxels' values are
 * moved within the image)
 */
class LGXCORE_EXPORT ShiftStack : public StackProcess {
public:
  ShiftStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Point3i ds(parms[1].toInt(), parms[2].toInt(), parms[3].toInt());
    return (*this)(s, stringToBool(parms[0]), ds);
  }

  bool operator()(Stack* stack, bool origin, Point3i ds);

  QString folder() const {
    return "Canvas";
  }
  QString name() const {
    return "Shift Stack";
  }
  QString description() const {
    return "Shift both stores of the stack to within the canvas.";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Origin"
                         << "X"
                         << "Y"
                         << "Z";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Origin"
                         << "X"
                         << "Y"
                         << "Z";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "No"
                         << "0"
                         << "0"
                         << "0";
  }
  ParmChoiceMap parmChoice() const
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    return map;
  }
  QIcon icon() const {
    return QIcon(":/images/Shift.png");
  }
};

/**
 * \class StackRelabel StackProcess.hpp <StackProcess.hpp>
 *
 * Relabel a 3D stack to use consecutive labels. The list of cells is
 * randomized before relabeling to ensure a different labeling at each run.
 */
class LGXCORE_EXPORT StackRelabel : public StackProcess {
public:
  StackRelabel(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_NON_EMPTY | STORE_LABEL))
      return false;
    Stack* s = currentStack();
    Store* store = s->currentStore();
    Store* output = s->work();
    int start = parms[0].toInt();
    int step = parms[1].toInt();
    if((*this)(s, store, output, start, step)) {
      store->hide();
      output->show();
      return true;
    }
    return false;
  }

  bool operator()(Stack* stack, const Store* store, Store* output, int start, int step);

  QString folder() const {
    return "Segmentation";
  }
  QString name() const {
    return "Relabel";
  }
  QString description() const
  {
    return "Relabel a 3D stack to use consecutive labels.\n"
           "The cells are shuffled so each relabling will be different.";
  }
  QStringList parmNames() const
  {
    return QStringList() << "StartLabel"
                         << "Step";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "StartLabel"
                         << "Step";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "1"
                         << "1";
  }
  QIcon icon() const {
    return QIcon(":/images/Relabel.png");
  }
};

/**
 * \class StackRelabelFromMesh StackProcess.hpp <StackProcess.hpp>
 *
 * Change the labels of a stack to match the ones of a labeled 3D cell mesh.
 * Unknown cells (i.e. cells in the stack, not in the mesh), can be either
 * kept or deleted. If kept, they will be relabeled to not conflict with
 * existing cells.
 */
class LGXCORE_EXPORT StackRelabelFromMesh : public StackProcess {
public:
  StackRelabelFromMesh(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_NON_EMPTY | STORE_LABEL).mesh(MESH_NON_EMPTY | MESH_LABEL))
      return false;
    Stack* s = currentStack();
    Store* store = s->currentStore();
    Store* output = s->work();
    const Mesh* m = currentMesh();
    if((*this)(s, store, output, m, stringToBool(parms[0]))) {
      store->hide();
      output->show();
      return true;
    }
    return false;
  }

  bool operator()(Stack* stack, const Store* store, Store* output, const Mesh* mesh,
                  bool delete_unknown);

  QString folder() const {
    return "Mesh Interaction";
  }
  QString name() const {
    return "Relabel From Mesh";
  }
  QString description() const
  {
    return "Relabel a 3D stack reusing the same labels as in the stack.\n"
           "Unknown cells (i.e. cells in the stack, not in the mesh), \n"
           "can be either kept or deleted. If kept, they will be relabeled\n"
           "to not conflict with existing cells.";
  }
  QStringList parmNames() const {
    return QStringList() << "Delete unknown";
  }
  QStringList parmDescs() const {
    return QStringList() << "Delete unknown";
  }
  QStringList parmDefaults() const {
    return QStringList() << "Yes";
  }
  ParmChoiceMap parmChoice() const
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    return map;
  }
  QIcon icon() const {
    return QIcon(":/images/Relabel.png");
  }
};

/**
 * \class SaveTransform StackProcess.hpp <StackProcess.hpp>
 *
 * Save the frame (or transform) matrix to a file, as a list of values in column-major.
 */
class LGXCORE_EXPORT SaveTransform : public QObject, public StackProcess {
  Q_OBJECT

public:
  SaveTransform(const StackProcess& process)
    : Process(process)
    , QObject()
    , StackProcess(process)
  {
  }

  bool initialize(QStringList& parms, QWidget* parent);

  bool operator()(const QStringList& parms) {
    return (*this)(currentStack(), parms[0]);
  }

  bool operator()(Stack* stack, const QString& filename);

  QString folder() const {
    return "Transform";
  }
  QString name() const {
    return "Save Transform";
  }
  QString description() const {
    return "Save the frame matrix (or transform if trans checked) to a file";
  }
  QStringList parmNames() const {
    return QStringList() << "Filename";
  }
  QStringList parmDescs() const {
    return QStringList() << "Filename";
  }
  QStringList parmDefaults() const {
    return QStringList() << "";
  }
  QIcon icon() const {
    return QIcon(":/images/save.png");
  }
};

/**
 * \class LoadTransform StackProcess.hpp <StackProcess.hpp>
 *
 * Load the frame (or transform) matrix from a file containing a list of values in column-major.
 */
class LGXCORE_EXPORT LoadTransform : public QObject, public StackProcess {
  Q_OBJECT

public:
  LoadTransform(const StackProcess& process)
    : Process(process)
    , QObject()
    , StackProcess(process)
  {
  }

  bool initialize(QStringList& parms, QWidget* parent);

  bool operator()(const QStringList& parms) {
    return (*this)(currentStack(), parms[0]);
  }

  bool operator()(Stack* stack, const QString& filename);

  QString folder() const {
    return "Transform";
  }
  QString name() const {
    return "Load Transform";
  }
  QString description() const {
    return "Save the frame matrix (or transform if trans checked) from a file";
  }
  QStringList parmNames() const {
    return QStringList() << "Filename";
  }
  QStringList parmDescs() const {
    return QStringList() << "Filename";
  }
  QStringList parmDefaults() const {
    return QStringList() << "";
  }
  QIcon icon() const {
    return QIcon(":/images/open.png");
  }
};

///@}
} // namespace process
} // namespace lgx

#endif
