/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "MeshProcessSelection.hpp"

#include <Clip.hpp>
#include <Mesh.hpp>

namespace lgx {
namespace process {

bool MeshSelectByHeat::operator()(Mesh* m, float minHeat, float maxHeat, bool replaceSelection)
{
  const IntFloatMap& labHeat = m->labelHeat();
  const IntIntFloatMap& wallHeat = m->wallHeat();
  const vvgraph& S = m->graph();
  if(not labHeat.empty()) {
    // label heat
    std::unordered_set<int> selected_labels;
    forall(const IntFloatPair& p, labHeat) {
      if(p.second >= minHeat and p.second <= maxHeat)
        selected_labels.insert(p.first);
    }
    forall(const vertex& v, S) {
      if(selected_labels.find(v->label) != selected_labels.end())
        v->selected = true;
      else if(replaceSelection)
        v->selected = false;
    }
    m->updateSelection();
  } else if(not wallHeat.empty()) {
    // wall heat
    if(replaceSelection) {
      forall(const vertex& v, S)
        v->selected = false;
    }
    const IntIntVIdSetMap& walls = m->wallVId();
    forall(const IntIntFloatPair& p, wallHeat) {
      if(p.second >= minHeat and p.second <= maxHeat) {
        IntIntVIdSetMap::const_iterator found = walls.find(p.first);
        if(found != walls.end()) {
          const VIdSet& vids = found->second;
          forall(const long& vp, vids) {
            vertex v(vp);
            v->selected = true;
          }
        }
      }
    }
    m->updateSelection();
  } else
    setWarningMessage(QString("No heat was available for selection"));
  return true;
}

REGISTER_MESH_PROCESS(MeshSelectByHeat);

bool MeshSelectAll::operator()(Mesh* m)
{
  forall(const vertex& v, m->graph())
    v->selected = true;
  m->updateSelection();
  return true;
}
REGISTER_MESH_PROCESS(MeshSelectAll);

bool MeshSelectBadNormals::operator()(Mesh* m)
{
  int nb_bad_normals = 0;
  forall(const vertex& v, m->graph()) {
    v->selected = !m->setNormal(v);
    if(v->selected)
      ++nb_bad_normals;
  }
  SETSTATUS("Selected " << nb_bad_normals << " vertices with bad normals");
  m->updateSelection();
  return true;
}
REGISTER_MESH_PROCESS(MeshSelectBadNormals);

bool MeshUnselect::operator()(Mesh* m)
{
  forall(const vertex& v, m->graph())
    v->selected = false;
  m->updateSelection();
  return true;
}
REGISTER_MESH_PROCESS(MeshUnselect);

bool MeshInvertSelection::operator()(Mesh* m)
{
  forall(const vertex& v, m->graph())
    v->selected ^= true;
  m->correctSelection(true);
  m->updateSelection();
  return true;
}

REGISTER_MESH_PROCESS(MeshInvertSelection);

bool MeshSelectUnlabeled::operator()(Mesh* m, bool replace)
{
  forall(const vertex& v, m->graph()) {
    if(v->label == 0)
      v->selected = true;
    else if(replace and v->label != 0)
      v->selected = false;
  }
  m->correctSelection(true);
  m->updateSelection();
  return true;
}
REGISTER_MESH_PROCESS(MeshSelectUnlabeled);

bool MeshSelectLabeled::operator()(Mesh* m, bool replace)
{
  forall(const vertex& v, m->graph()) {
    if(v->label > 0)
      v->selected = true;
    else if(replace and v->label == 0)
      v->selected = false;
  }
  m->correctSelection(true);
  m->updateSelection();
  return true;
}
REGISTER_MESH_PROCESS(MeshSelectLabeled);

bool MeshSelectLabel::operator()(Mesh* m, bool replace, int label)
{
  if(label <= 0)
    label = selectedLabel();
  if(label <= 0)
    return (setErrorMessage("Cannot select label, no current label is defined"));

  SETSTATUS(QString("Selecting label %1.").arg(label));

  forall(const vertex& v, m->graph()) {
    if(v->label == label)
      v->selected = true;
    else if(replace and v->label != label)
      v->selected = false;
  }

  m->correctSelection(true);

  m->updateSelection();
  return true;
}
REGISTER_MESH_PROCESS(MeshSelectLabel);

bool MeshUnselectLabel::operator()(Mesh* m, int label)
{
  if(label <= 0)
    label = selectedLabel();
  if(label <= 0) {
    setErrorMessage("Cannot unselect label, no current label is defined");
    return false;
  }

  SETSTATUS(QString("Unselecting label %1.").arg(label));

  forall(const vertex& v, m->graph()) {
    if(v->label == label)
      v->selected = false;
  }

  m->correctSelection(true);

  m->updateSelection();
  return true;
}
REGISTER_MESH_PROCESS(MeshUnselectLabel);

bool MeshSelectClip::operator()(Mesh* m)
{
  SETSTATUS(QString("Selecting vertices within clipping region."));

  forall(const vertex& v, m->graph()) {
    const Stack* s = m->stack();
    bool clipped = false;
    Point3f p = Point3f(s->frame().inverseCoordinatesOf(qglviewer::Vec(v->pos)));
    if(clip1()->isClipped(p))
      clipped = true;
    if(clip2()->isClipped(p))
      clipped = true;
    if(clip3()->isClipped(p))
      clipped = true;
    if(!clipped)
      v->selected = true;
  }
  m->updateSelection();
  return true;
}
REGISTER_MESH_PROCESS(MeshSelectClip);

bool MeshSelectWholeLabelExtend::operator()(Mesh* m)
{
  SETSTATUS(QString("Extending vertex selection to whole labels."));

  std::set<int> labels;

  forall(const vertex& v, m->graph())
    if(v->selected and v->label > 0)
      labels.insert(v->label);

  forall(const vertex& v, m->graph())
    if(labels.find(v->label) != labels.end())
      v->selected = true;

  m->correctSelection(true);
  m->updateSelection();
  return true;
}
REGISTER_MESH_PROCESS(MeshSelectWholeLabelExtend);

bool MeshSelectDuplicateCells::operator()(Mesh* m)
{
  SETSTATUS(QString("Finding vertices with more than one contig."));

  // Labels to select
  std::unordered_map<int, int> LabCount;
  std::unordered_map<int, vertex> Labels;
  std::unordered_set<vertex> Vertices;

  // Grab one vertex for each label
  vvgraph& S = m->graph();
  forall(const vertex& v, S) {
    Vertices.insert(v);
    Labels[v->label] = v;
  }

  // Find contigs
  while(!Vertices.empty()) {
    // Grab any vertex and save the label
    vertex v = *Vertices.begin();
    Vertices.erase(v);
    int label = v->label;

    // Start growing neighbor set
    std::set<vertex> Nbs;
    std::set<vertex> NewNbs;
    Nbs.insert(v);
    do {
      forall(const vertex& u, Nbs)
        forall(const vertex& n, S.neighbors(u))
          if(Vertices.count(n) > 0 and n->label == label) {
            Vertices.erase(n);
            NewNbs.insert(n);
          }
      Nbs = NewNbs;
      NewNbs.clear();
    } while(!Nbs.empty());

    // One more region for this label
    LabCount[label]++;
  }

  // Mark labels with more than one region selected
  forall(const vertex& v, S)
    if(LabCount[v->label] > 1)
      v->selected = true;
    else
      v->selected = false;

  m->correctSelection(true);
  m->updateSelection();
  return true;
}
REGISTER_MESH_PROCESS(MeshSelectDuplicateCells);

bool ExtendByConnectivity::operator()(Mesh* m)
{
  vvgraph& S = m->graph();
  std::vector<vertex> vs = m->activeVertices();
  // Either all vertices are selected, or none
  if(vs.size() == S.size())
    return true;
  std::unordered_set<vertex> selected(vs.begin(), vs.end());
  // Tabular approach
  for(size_t i = 0; i < vs.size(); ++i) {
    vertex v = vs[i];
    forall(const vertex& n, S.neighbors(v)) {
      if(selected.count(n))
        continue;
      n->selected = true;
      selected.insert(n);
      vs.push_back(n);
    }
  }
  m->updateSelection();
  return true;
}
REGISTER_MESH_PROCESS(ExtendByConnectivity);
} // namespace process
} // namespace lgx
