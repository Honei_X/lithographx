/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "MeshProcessSegmentation.hpp"
#include "MeshProcessSignal.hpp"

#include "Progress.hpp"

#include "ui_LoadHeatMap.h"

namespace lgx {
namespace process {
bool SegmentMesh::operator()(Mesh* mesh, uint maxsteps, const std::vector<vertex>& to_segment)
{
  typedef std::pair<float, vertex> cvpair;
  typedef std::multimap<float, vertex> cvmap;

  // Start progress bar
  vvgraph& S = mesh->graph();
  const std::vector<vertex>& vs = (to_segment.empty() ? mesh->activeVertices() : to_segment);

  bool all_active = vs.size() == S.size();

  vvgraph SS;
  size_t unlabeled = 0;
  if(all_active) {
    SS = S;
    forall(vertex v, S)
      if(v->label == 0)
        unlabeled++;
  } else {
    std::unordered_set<vertex> vset(vs.begin(), vs.end());
    // Add labeled neighbors of non-labeled vertices
    forall(vertex v, vs)
      if(v->label == 0) {
        unlabeled++;
        forall(vertex n, S.neighbors(v))
          if(n->label > 0 and vset.count(n) == 0)
            vset.insert(n);
      }
    SS = S.subgraph(vset);
  }

  Progress progress(QString("Segmenting mesh-%1").arg(mesh->userId()), unlabeled);
  int count = 0;
  mesh->updateTriangles();
  mesh->updateLines();

  cvmap Q;
  do {
    Q.clear();
    // Mark all vertices as not in queue
    forall(const vertex& v, SS)
      v->inqueue = false;

    // Put all neighbors of labelled vertices into queue
    forall(const vertex& v, SS)
      if(v->label > 0) {
        forall(const vertex& n, SS.neighbors(v)) {
          if(n->label == 0 and !n->inqueue) {
            n->inqueue = true;
            Q.insert(cvpair(n->signal, n));
          }
        }
      }

    // Process queue
    long steps = (maxsteps > 0 ? maxsteps : LONG_MAX);
    if(Q.size() > 0)
      mesh->updateTriangles();

    while(Q.size() > 0 and steps-- > 0) {
      if(!progress.advance(count))
        userCancel();

      cvmap::iterator cvmax = Q.begin();
      vertex vmax = cvmax->second;

      int label = 0;
      bool foundlabel = false, difflabels = false;
      forall(const vertex& n, SS.neighbors(vmax)) {
        if(n->label > 0) {
          if(!foundlabel) {
            label = n->label;
            foundlabel = true;
          } else if(label != n->label)
            difflabels = true;
        }
        // RSS not sure why this is here
        // else if(n->label == 0 and !n->inqueue) {
        //  n->inqueue = true;
        //  Q.insert(cvpair(n->signal, n));
        //}
      }
      if(foundlabel) {
        if(difflabels)
          vmax->label = -1;
        else
          vmax->label = label;
        count++;

        forall(const vertex& n, SS.neighbors(vmax))
          if(n->label == 0 and !n->inqueue) {
            n->inqueue = true;
            Q.insert(cvpair(n->signal, n));
          }
      }

      vmax->inqueue = false;
      Q.erase(cvmax);
    }

    if(count > 0) {
      mesh->updateTriangles();
      mesh->updateLines();
    }
    if(Q.size() > 0) {
      updateState();
      updateViewer();
    }

    SETSTATUS("Segmenting mesh " << mesh->userId() << ", vertices left in queue:" << Q.size());
  } while(Q.size() > 0);

  return true;
}
REGISTER_MESH_PROCESS(SegmentMesh);

bool SegmentClear::operator()(Mesh* mesh)
{
  const std::vector<vertex>& vs = mesh->activeVertices();
  forall(const vertex& v, vs)
    v->label = 0;
  mesh->updateTriangles();
  mesh->updateLines();
  return true;
}
REGISTER_MESH_PROCESS(SegmentClear);

bool MeshRelabel::operator()(Mesh* m, int start, int step)
{
  if(step < 1)
    step = 1;
  if(start < 1)
    start = 1;
  const std::vector<vertex>& vs = m->activeVertices();
  Progress progress(QString("Relabeling current mesh.").arg(start).arg(step), vs.size());
  std::unordered_map<int, int> relabel_map;
  int prog = vs.size() / 100;
  int i = 0;
  forall(const vertex& v, vs) {
    if(v->label > 0) {
      std::unordered_map<int, int>::iterator found = relabel_map.find(v->label);
      if(found != relabel_map.end())
        v->label = found->second;
      else {
        relabel_map[v->label] = start;
        v->label = start;
        start += step;
      }
    }
    if((i % prog) == 0 and !progress.advance(i))
      userCancel();
    ++i;
  }
  m->setLabel(start);
  if(!progress.advance(vs.size()))
    userCancel();
  m->updateTriangles();
  SETSTATUS(QString("Total number of labels created = %1. Last label = %2").arg(relabel_map.size()).arg(start));
  return true;
}
REGISTER_MESH_PROCESS(MeshRelabel);

bool GrabLabelsSegment::operator()(Mesh* mesh1, Mesh* mesh2, float tolerence)
{
  const vvgraph& S1 = mesh1->graph();
  const vvgraph& S2 = mesh2->graph();

  // Find center of all cells (should be closed surfaces)
  typedef std::pair<int, Point3f> IntPoint4fPair;
  std::unordered_map<int, Point4f> posMap1;
  forall(const vertex& v, S1)
    if(v->label > 0)
      posMap1[v->label] += Point4f(v->pos.x(), v->pos.y(), v->pos.z(), 1.0);
  for(const auto& p: posMap1)
    posMap1[p.first] /= posMap1[p.first].t();

  std::unordered_map<int, Point4f> posMap2;
  forall(const vertex& v, S2)
    if(v->label > 0)
      posMap2[v->label] += Point4f(v->pos.x(), v->pos.y(), v->pos.z(), 1.0);
  for(const auto& p: posMap2)
    posMap2[p.first] /= posMap2[p.first].t();

  // For each label in mesh 1, find closest in mesh 2.
  std::unordered_map<int, int> labelMap;
  for(const auto& p1: posMap1) {
    float minDist = 1e37;
    int minLabel = 0;
    for(const auto& p2: posMap2) {
      float dist = (p1.second - p2.second).norm();
      if(dist < minDist and dist < tolerence) {
        minDist = dist;
        minLabel = p2.first;
      }
    }
    if(minLabel > 0)
      labelMap[p1.first] = minLabel;
  }
  SETSTATUS(QString("Updating %1 matching cells.").arg(labelMap.size()));

  // Update mesh labels
  for(const vertex& v: S1)
    if(labelMap[v->label] > 0)
      v->label = labelMap[v->label];
    else
      v->label = 0;

  mesh1->updateTriangles();
  return true;
}
REGISTER_MESH_PROCESS(GrabLabelsSegment);

bool LabelSelected::operator()(Mesh* mesh, int label)
{
  const std::vector<vertex>& vs = mesh->activeVertices();
  if(label <= 0) {
    forall(const vertex& v, vs)
      if(v->label > 0) {
        if(label == 0)
          label = v->label;
        else if(label != v->label)
          return setErrorMessage("At most one label can be contained in selected vertices");
      }
    if(label <= 0)
      label = mesh->nextLabel();
  }
  forall(const vertex& v, vs)
    v->label = label;
  mesh->updateTriangles();
  mesh->updateLines();
  return true;
}
REGISTER_MESH_PROCESS(LabelSelected);

// Relabel 3D cells
bool RelabelCells3D::operator()(Mesh* mesh, int label_start, int label_step)
{
  // Get the vertices
  vvgraph V(mesh->activeVertices());

  // Get cells by connected regions
  VVGraphVec cellVertex;
  VIntMap vertexCell;
  mesh->getConnectedRegions(V, cellVertex, vertexCell);

  if(label_start == -1)
    label_start = mesh->nextLabel();

  // Relabel vertices
  forall(const vvgraph& S, cellVertex) {
    forall(const vertex& v, S)
      v->label = label_start;
    label_start += label_step;
  }

  if(label_start >= mesh->viewLabel())
    mesh->setLabel(label_start + 1);

  // Tell the system the mesh color has changed
  mesh->updateTriangles();
  mesh->updateSelection();

  return true;
}
REGISTER_MESH_PROCESS(RelabelCells3D);

bool MeshCombineRegions::operator()(Mesh* mesh, float borderDist, float threshold)
{
  vvgraph& S = mesh->graph();
  const std::vector<vertex>& vs = mesh->activeVertices();
  Progress progress(QString("Combining Regions on Mesh %1").arg(mesh->userId()), S.size());
  typedef std::set<vertex> Set;
  Set VNew;
  std::set<int> VContain;
  typedef std::pair<int, int> SigPair;
  SigPair pr;
  std::set<SigPair> pair, PairUpdated;
  std::map<int, int> VCont, labelCount;
  std::map<int, double> labelAvg;
  typedef std::pair<SigPair, double> to_loop;
  std::map<SigPair, double> mysig, mywt, PairSet;
  mesh->markBorder(borderDist);
  typedef std::map<int, Set> junctionVertices;
  junctionVertices junction;
  std::set<vertex> VJunction;

  // Removing the vertices at the junctions
  forall(const vertex& v, vs) {
    if(v->label == -1) {
      VContain.clear();
      forall(const vertex& n, S.neighbors(v)) {
        if(n->label == -1) {
          vertex m = S.nextTo(v, n);
          VContain.insert(m->label);
        }
      }
      if(VContain.size() < 3)
        VNew.insert(v);
      else {
        // VJunction.insert(v);
        forall(int m, VContain) {
          junction[m].insert(v);
        }
      }
    }
  }

  // Average of v->signal for each cell (respective to each label)
  int cnt = 0;
  forall(const vertex& v, vs) {
    if(!progress.advance(cnt++))
      userCancel();
    if(v->label <= 0 or v->minb != 0)
      continue;
    labelAvg[v->label] += v->signal;
    labelCount[v->label]++;
  }
  int c = 0;
  forall(SigPair pr, labelCount) {
    c++;
    if(labelCount[pr.first] != 0) {
      labelAvg[pr.first] /= labelCount[pr.first];
    }
  }

  // Calculation of the average signal at the border
  forall(const vertex& v, VNew) {
    if(v->label == -1) {
      forall(const vertex& n, S.neighbors(v)) {
        if(n->label == -1) {
          vertex m = S.nextTo(v, n);
          vertex k = S.prevTo(v, n);
          if(m->label > 0 and k->label > 0 and m->label < k->label and labelAvg.count(m->label) == 1
             and labelAvg.count(k->label) == 1) {
            pr = std::make_pair(m->label, k->label);
            Point3f vn_avg = (v->pos + n->pos) / 2.0;
            bool flag = true;
            forall(const vertex k, junction[m->label]) {
              if(norm(k->pos - vn_avg) < borderDist) {
                flag = false;
                break;
              }
            }
            if(flag == true) {
              mywt[pr] += norm(v->pos - n->pos);
              mysig[pr] += v->signal * norm(v->pos - n->pos);
            }
          }
        }
      }
    }
  }

  // Set of pairs of labels to be updated if the avg. border signal is not too much higher
  // than the average signals of the two adjacent cells
  forall(to_loop loop, mysig) {
    if(float((loop.second / mywt[loop.first]) / ((labelAvg[loop.first.first] + labelAvg[loop.first.second]) / 2))
       < threshold) {
      pr = std::make_pair(loop.first.first, loop.first.second);
      pair.insert(pr);
      PairSet[pr] = 1;
    }
  }
  int count = 0;
  // Updating the mesh with the changed labels and updating those changed labels in
  // respective pairs further on in the set
  do {
    if(!progress.advance(count++))
      userCancel();
    pr = *(pair.begin());
    forall(const vertex& n, vs) {
      if(n->label == pr.first) {
        n->label = pr.second;
      }
    }
    PairUpdated.clear();
    forall(SigPair pr2, pair) {
      if(pr.first == pr2.first and pr.second == pr2.second) {
        PairSet[pr2]++;
      }
      if(PairSet[pr2] == 1) {
        if(pr.first == pr2.first and pr.second < pr2.second) {
          pr2.first = pr.second;
          PairUpdated.insert(std::make_pair(pr2.first, pr2.second));
          PairSet[pr2] = 1;
        } else if(pr.first == pr2.second) {
          pr2.second = pr.second;
          PairUpdated.insert(std::make_pair(pr2.first, pr2.second));
          PairSet[pr2] = 1;
        } else {
          PairUpdated.insert(std::make_pair(pr2.first, pr2.second));
        }
      }
    }
    pair = PairUpdated;
  } while(pair.size() > 0);
  mesh->updateAll();
  return true;
}
REGISTER_MESH_PROCESS(MeshCombineRegions);

bool MeshAutoSegment::operator()(Mesh* mesh, bool updateView, bool normalize, float gaussianRadiusCell,
                                 float localMinimaRadius, float gaussianRadiusWall, float normalizeRadius,
                                 float borderDist, float threshold)
{
  const std::vector<vertex>& vs = mesh->activeVertices();
  std::vector<float> MeshSignal(vs.size());

  for(size_t i = 0; i < vs.size(); ++i)
    MeshSignal[i] = vs[i]->signal;

  // Blur the mesh, normally with same radius as local minima
  MeshGaussianBlur blurCells(*this);
  blurCells(mesh, gaussianRadiusCell);
  if(updateView) {
    updateState();
    updateViewer();
  }

  // Find local minima
  MeshLocalMinima minima(*this);
  minima(mesh, localMinimaRadius);
  if(updateView) {
    updateState();
    updateViewer();
  }

  // Restore mesh signal
  for(size_t i = 0; i < vs.size(); ++i)
    vs[i]->signal = MeshSignal[i];

  // Blur the mesh for segmentation and merging over-segmented regions
  MeshGaussianBlur blurWalls(*this);
  blurWalls(mesh, gaussianRadiusWall);
  if(updateView) {
    mesh->showNormal();
    mesh->showMeshLines();
    mesh->showCellMesh();
    updateState();
    updateViewer();
  }

  // Normalize Mesh Signal if required
  if(normalize) {
    MeshNormalize normal(*this);
    normal(mesh, normalizeRadius);
    if(updateView) {
      updateState();
      updateViewer();
    }
  }

  // Segment Mesh
  SegmentMesh segMesh(*this);
  segMesh(mesh, 0, vs);
  if(updateView) {
    updateState();
    updateViewer();
  }

  // Merge over-segmented regions
  MeshCombineRegions combine(*this);
  combine(mesh, borderDist, threshold);

  return true;
}
REGISTER_MESH_PROCESS(MeshAutoSegment);
} // namespace process
} // namespace lgx
