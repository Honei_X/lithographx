/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MESH_PROCESS_CELL_MESH_HPP
#define MESH_PROCESS_CELL_MESH_HPP

#include <Process.hpp>

#include <MeshProcessSegmentation.hpp>

namespace lgx {
namespace process {
///\addtogroup MeshProcess
///@{
/**
 * \class FixMeshCorners MeshProcessCellMesh.hpp <MeshProcessCellMesh.hpp>
 *
 * Fix labelling of cell corners. Depending on the topology, it may be that a
 * triangle between three segmented cells cannot be labeled correctly. This
 * process identifies such triangle and sub-divide them in the hope of
 * changing the topology and solve the problem. This process may need to be
 * called many times to solve all the issues as this is only a heuristic that
 * tends to improve the situation.
 */
class LGXCORE_EXPORT FixMeshCorners : public MeshProcess {
public:
  FixMeshCorners(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    if(parms[2].toInt() < 1) {
      setErrorMessage("Number of runs must be at least one.");
      return false;
    }
    return (*this)(currentMesh(), stringToBool(parms[0]), stringToBool(parms[1]), parms[2].toInt());
  }

  bool operator()(Mesh* mesh, bool autoSegment, bool selectBadVertices, int maxRun);

  QString folder() const {
    return "Cell Mesh";
  }
  QString name() const {
    return "Fix Corners";
  }
  QString description() const {
    return "Fix labelling of cell corners.";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Auto segmentation?"
                         << "Select bad vertices"
                         << "Max iterations";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Re-run watershed automatically at each turn."
                         << "Select remaining bad vertices at the end.\n"
              "Helps in deleting bad vertices that cannot be segmented properly."
                         << "Maximal number of turns.";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "Yes"
                         << "Yes"
                         << "5";
  }
  ParmChoiceMap parmChoice() const
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    map[1] = booleanChoice();
    return map;
  }
  QIcon icon() const {
    return QIcon(":/images/FixCorners.png");
  }
};

/**
 * \class MakeCellMesh MeshProcessCellMesh.hpp <MeshProcessCellMesh.hpp>
 *
 * Convert a segmented mesh into a 2D cellular mesh. The final mesh only
 * contains a vertex per cell and vertices for their border.
 */
class LGXCORE_EXPORT MakeCellMesh : public MeshProcess {
public:
  MakeCellMesh(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_NON_EMPTY) or checkState().mesh(MESH_CELLS))
      return false;
    return (*this)(currentMesh(), parms[1].toFloat(), currentMesh()->graph(), stringToBool(parms[0]));
  }

  bool operator()(Mesh* mesh, float minWall, vvgraph& S, bool eraseMarginCells);

  QString folder() const {
    return "Cell Mesh";
  }
  QString name() const {
    return "Make Cells";
  }
  QString description() const
  {
    return "Convert the segmented mesh into a 2D cellular mesh.\n"
           "The simplified mesh contains only vertices for cell centers and cell outlines";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Erase Margin Cells" << QString("Min Wall Length (%1)").arg(UM);
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Delete cells that touch the edge of the mesh."
                         << "Min length in between vertices within cell outlines.";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "No"
                         << "1";
  }
  ParmChoiceMap parmChoice() const
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    return map;
  }
  QIcon icon() const {
    return QIcon(":/images/MakeCells.png");
  }
};
///@}
} // namespace process
} // namespace lgx
#endif
