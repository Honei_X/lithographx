/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MESH_PROCESS_HEAT_MAP_HPP
#define MESH_PROCESS_HEAT_MAP_HPP

#include <Process.hpp>

#include <Mesh.hpp>
#include <Misc.hpp>

#include <limits>

class Ui_LoadHeatMap;

namespace lgx {
namespace process {
LGXCORE_EXPORT void getBBox(const HVec3F& pts, Point3f* bBox);

///\addtogroup MeshProcess
///@{

/**
 * \class ComputeHeatMap MeshProcessHeatMap.hpp <MeshProcessHeatMap.hpp>
 *
 * Compute heat map on the current mesh. For details, look at the description of the various parameters.
 */
class LGXCORE_EXPORT ComputeHeatMap : public QObject, public MeshProcess {
  Q_OBJECT
public:
  ComputeHeatMap(const MeshProcess& process)
    : Process(process)
    , QObject()
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& parms);

  enum MapType { AREA, VOLUME, WALL };

  enum SignalType {
    GEOMETRY = 0x1,
    BORDER = 0x2,
    INTERIOR = 0x4,
    TOTAL = 0x8,
    BORDER_TOTAL = 0x10,
    INTERIOR_TOTAL = 0x20
  };

  enum PolarityType { NONE, CELL_AVERAGE, WALL_MIN };

  enum MultiMapType {
    SINGLE,
    INCREASING_RATIO,
    INCREASING_DIFF,
    DECREASING_RATIO,
    DECREASING_DIFF,
    INCREASING_GROWTH,
    DECREASING_GROWTH
  };

  struct ImageHeatMaps {
    IntFloatMap LabelGeom;         // Label Areas
    IntFloatMap LabelBordGeom;     // Area of label borders
    IntFloatMap LabelIntGeom;      // Area of label interiors
    IntFloatMap LabelTotalSig;     // Label total signal
    IntFloatMap LabelBordSig;      // Label border signal
    IntFloatMap LabelIntSig;       // Label interior signal
    IntFloatMap LabelHeatAmt;      // Label HeatMap temp for change map
    IntHVec3uMap LabelTris;        // Label triangle lists
    IntVIdSetMap LabelPts;         // Label triangle list points

    IntIntFloatMap WallBordGeom;     // Wall border areas
    IntIntFloatMap WallBordSig;      // Wall border signal
    IntIntFloatMap WallHeatAmt;      // Wall border signal amt for change map
  };

  bool operator()(Mesh* mesh1, Mesh* mesh2, MapType map, SignalType signal, const QString& reportFile, int report,
                  bool manualRange, float rangeMin, float rangeMax, bool signalAverage, bool globalCoordinates,
                  PolarityType polarity, MultiMapType multiMapType, float growthTime, float borderSize);

  bool initialize(QStringList& parms, QWidget* parent);

  QString folder() const {
    return "Heat Map";
  }
  QString name() const {
    return "Heat Map";
  }
  QString description() const {
    return "Generate heat map for the current mesh";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Type"
                         << "Visualize"
                         << "FileName"
                         << "ReportFields"
                         << "Man. Range"
                         << "Range Min"
                         << "Range Max"
                         << "Signal Avg"
                         << "Global Coord"
                         << "Polarity Type"
                         << "Change Map"
                         << "Increasing"
                         << "Diff Type"
                         << "Growth Time" << QString("Border Size(%1)").arg(UM);
  }
  QStringList parmDescs() const
  {
    return QStringList()
           << "Area: signal/geometry on curved surfaces,\n"
              "Volume: for 3D only,\n"
              "Walls: quantify signal or geometry at cell borders."
           << "Geometry: cell areas or volume,\n"
              "Border signal: signal associated with cell borders, within a given distance (Border Size)\n"
              "Interior signal: total signal of a cell - border signal.\n"
           << "Path to output file."
           << "Options to report the following fields in spreadsheet: Geometry, Signal, Border-Interior"
           << "Manually define the range of the color map."
           << "Color map lower bound."
           << "Color map upper bound."
           << "Option to normalize the signal by cell area or volume"
           << "Apply the rotation/translation of the stack to the cell center coordinates."
           << "Experimental option, attempt to determine cell signal polarity based on strength of signal on "
              "different walls.\n"
              "Cell Average: compare each wall signal to signal average,\n"
              "Wall/Min: compare wall signal to the weakest wall signal in the cell."
           << "Compare two meshes with each other (deformation or change in signal) .\n"
           << "Increasing: the current mesh is the reference (T0), the other mesh is the changed state (T1),\n"
              "Decreasing: the other mesh is the reference (T0), the current mesh is the changed state (T1)."
           << "Ratio: area or signal in changed state(T1) / area or signal in reference (T0),\n"
              "Difference: area or signal in changed state(T1) - area or signal in reference (T0),\n"
              "Growth: (Ratio -1)  / growth time.\n"
           << "Time interval between the two samples." << QString("Border Size(%1)").arg(UM);
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "Area"
                         << "Geometry"
                         << ""
                         << "Geometry"
                         << "No"
                         << "0"
                         << "65535"
                         << "Yes"
                         << "No"
                         << "None"
                         << "No"
                         << "Yes"
                         << "Ratio"
                         << ".001"
                         << "1.0";
  }
  QIcon icon() const {
    return QIcon(":/images/MakeHeatMap.png");
  }

  QDialog* dlg;

protected slots:
  void changeMapType(const QString& type);
  void selectSpreadsheetFile();

protected:
  bool getLabelMaps(ImageHeatMaps& map, Mesh* mesh);
  MapType mapType;
  SignalType signal;
  int report;
  bool manualRange;
  float rangeMin;
  float rangeMax;
  bool signalAverage;
  bool globalCoordinates;
  PolarityType polarity;
  bool changeMap;
  MultiMapType multiMapType;
  float growthTime;
  float borderSize;
};

/**
 * \class DeleteHeatRangeLabels MeshProcessHeatMap.hpp <MeshProcessHeatMap.hpp>
 *
 * Delete labeling, and possibly cells, if their heat falls within a defined range.
 */
class LGXCORE_EXPORT DeleteHeatRangeLabels : public MeshProcess {
public:
  DeleteHeatRangeLabels(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    float min, max;
    bool ok = true;
    if(parms[2].isEmpty())
      min = std::numeric_limits<float>::quiet_NaN();
    else
      min = parms[2].toFloat(&ok);
    if(not ok)
      return setErrorMessage("Error, parameter 'Min Heat' must be either empty, or a number");
    if(parms[3].isEmpty())
      max = std::numeric_limits<float>::quiet_NaN();
    else
      max = parms[3].toFloat(&ok);
    if(not ok)
      return setErrorMessage("Error, parameter 'Min Heat' must be either empty, or a number");
    return (*this)(currentMesh(), stringToBool(parms[0]), stringToBool(parms[1]), min, max);
  }

  bool operator()(Mesh* mesh, bool rescale, bool deleteCells, float min, float max);

  QString folder() const {
    return "Heat Map";
  }
  QString name() const {
    return "Delete Heat Range Labels";
  }
  QString description() const
  {
    return "Delete labels with heat within a given range. The heat is given relative to the total range.";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Rescale"
                         << "Delete cells"
                         << "Min Heat"
                         << "Max Heat";
  }
  QStringList parmDescs() const
  {
    return QStringList()
           << "Redefine the lower/upper bounds of colormap to fit the range of values."
           << "Delete vertices within a given range of the color map (Min Heat-Max Heat)."
           << "Lower bound of color value for which the cells will be deleted (empty for current min)."
           << "Upper bound of color value for which the cells will be deleted (empty for current max).";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "Yes"
                         << "No"
                         << ""
                         << "";
  }
  ParmChoiceMap parmChoice() const
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    map[1] = booleanChoice();
    return map;
  }
  QIcon icon() const {
    return QIcon(":/images/ClearStack.png");
  }
};

/**
 * \class RescaleHeatMap MeshProcessHeatMap.hpp <MeshProcessHeatMap.hpp>
 *
 * Rescale the heat map, specifying new lower and upper bound.
 */
class LGXCORE_EXPORT RescaleHeatMap : public MeshProcess {
public:
  RescaleHeatMap(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_HEAT))
      return false;
    Mesh* m = currentMesh();
    bool ok = true;
    float min, max;
    if(parms[0].isEmpty())
      min = std::numeric_limits<float>::quiet_NaN();
    else
      min = parms[0].toFloat(&ok);
    if(not ok)
      return setErrorMessage("Error, parameter 'Min' must be either empty, or a number");
    if(parms[1].isEmpty())
      max = std::numeric_limits<float>::quiet_NaN();
    else
      max = parms[1].toFloat(&ok);
    if(not ok)
      return setErrorMessage("Error, parameter 'Max' must be either empty, or a number");
    return (*this)(m, min, max);
  }

  bool operator()(Mesh* m, float min, float max);

  QString folder() const {
    return "Heat Map";
  }
  QString name() const {
    return "Rescale Heat Map";
  }
  QString description() const {
    return "Change the range of the current heat map for display.";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Min"
                         << "Max";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Lower bound (empty for current)"
                         << "Upper bound (empty for current)";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << ""
                         << "";
  }
  QIcon icon() const {
    return QIcon(":/images/MakeHeatMap.png");
  }
};

/**
 * \class SaveHeatMap MeshProcessHeatMap.hpp <MeshProcessHeatMap.hpp>
 *
 * Save the current heat map in a CSV file.
 */
class LGXCORE_EXPORT SaveHeatMap : public QObject, public MeshProcess {
  Q_OBJECT
public:
  SaveHeatMap(const MeshProcess& process)
    : Process(process)
    , QObject()
    , MeshProcess(process)
  {
  }

  bool initialize(QStringList& parms, QWidget* parent);

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_ANY))
      return false;
    Mesh* m = currentMesh();
    return (*this)(m, parms[0]);
  }

  bool operator()(Mesh* mesh, const QString& filename);

  QString folder() const {
    return "Heat Map";
  }
  QString name() const {
    return "Save Heat Map";
  }
  QString description() const {
    return "Save heat map to a file";
  }
  QStringList parmNames() const {
    return QStringList() << "Filename";
  }
  QStringList parmDescs() const {
    return QStringList() << "Path to spreadsheat file.";
  }
  QStringList parmDefaults() const {
    return QStringList() << "";
  }
  QIcon icon() const {
    return QIcon(":/images/MakeHeatMap.png");
  }
};

/**
 * \class LoadHeatMap MeshProcessHeatMap.hpp <MeshProcessHeatMap.hpp>
 *
 * Load the heat map from a CSV file. The CSV file must have the following structure:
 *
 *  - The first column must be cell labels (the name doesn't matter)
 *  - If the heat map is on walls, the second column must be called "Neighbor" and also be cell labels.
 *  - Each other column is a potential heat. The column name is structured
 *    either as "name" or "name (unit)". In the second case, the unit will be
 *    extracted for display.
 */
class LGXCORE_EXPORT LoadHeatMap : public QObject, public MeshProcess {
  Q_OBJECT
public:
  LoadHeatMap(const MeshProcess& process)
    : Process(process)
    , QObject()
    , MeshProcess(process)
  {
  }

  bool initialize(QStringList& parms, QWidget* parent);

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_ANY))
      return false;
    Mesh* m = currentMesh();
    return (*this)(m, parms[0], parms[1].toInt(), parms[2].toFloat());
  }

  bool operator()(Mesh* mesh, const QString& filename, int column, float border_size);

  QString folder() const {
    return "Heat Map";
  }
  QString name() const {
    return "Load Heat Map";
  }
  QString description() const {
    return "Load a heat map file and set the corresponding heat for each label";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Filename"
                         << "Column" << QString("Border size (%1)").arg(UM);
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Path to spreadsheat file."
                         << "Column of the csv file to load for display.\n"
              "Label: cell labels, Value: original computed values (e.g. growth or signal), Heat: "
              "values scaled for visualization (always between 0-1)."
                         << "Width of cell outline used for vizualization of 'wall' colormaps";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << ""
                         << "2"
                         << "1.0";
  }
  QIcon icon() const {
    return QIcon(":/images/MakeHeatMap.png");
  }

public slots:
  void selectFile(const QString& filename);
  void selectFile();
  void selectColumn(int c);

protected:
  QDialog* dlg;
  Ui_LoadHeatMap* ui;
  QList<QString> column_unit;
};

/**
 * \class SaveParents MeshProcessHeatMap.hpp <MeshProcessHeatMap.hpp>
 *
 * Store mapping from each label to a label in a different mesh (parent).
 * \author Gerardo Tauriello
 */
class LGXCORE_EXPORT SaveParents : public MeshProcess {
public:
  SaveParents(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool initialize(QStringList& parms, QWidget* parent);

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_ANY))
      return false;
    Mesh* m = currentMesh();
    return (*this)(m, parms[0]);
  }

  bool operator()(Mesh* mesh, const QString& filename);

  QString folder() const {
    return "Lineage Tracking";
  }
  QString name() const {
    return "Save Parents";
  }
  QString description() const {
    return "Save map of labels to parents labels to a file";
  }
  QStringList parmNames() const {
    return QStringList() << "Filename";
  }
  QStringList parmDescs() const {
    return QStringList() << "Path to label parents file.";
  }
  QStringList parmDefaults() const {
    return QStringList() << "";
  }
  QIcon icon() const {
    return QIcon(":/images/Parents.png");
  }
};

/**
 * \class LoadParents MeshProcessHeatMap.hpp <MeshProcessHeatMap.hpp>
 *
 * Load mapping from each label to a label in a different mesh (parent).
 * \author Gerardo Tauriello
 */
class LGXCORE_EXPORT LoadParents : public MeshProcess {
public:
  LoadParents(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool initialize(QStringList& parms, QWidget* parent);

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_ANY))
      return false;
    Mesh* m = currentMesh();
    return (*this)(m, parms[0]);
  }

  bool operator()(Mesh* mesh, const QString& filename);

  QString folder() const {
    return "Lineage Tracking";
  }
  QString name() const {
    return "Load Parents";
  }
  QString description() const {
    return "Load map of labels to parents from a file";
  }
  QStringList parmNames() const {
    return QStringList() << "Filename";
  }
  QStringList parmDescs() const {
    return QStringList() << "Path to label parents file.";
  }
  QStringList parmDefaults() const {
    return QStringList() << "";
  }
  QIcon icon() const {
    return QIcon(":/images/Parents.png");
  }
};

/**
 * Clear label to parent mapping
 * \author Gerardo Tauriello
 */
class LGXCORE_EXPORT ClearParents : public MeshProcess {
public:
  ClearParents(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList&)
  {
    if(!checkState().mesh(MESH_ANY))
      return false;
    Mesh* m = currentMesh();
    return (*this)(m);
  }

  bool operator()(Mesh* mesh);

  QString folder() const {
    return "Lineage Tracking";
  }
  QString name() const {
    return "Clear Parents";
  }
  QString description() const {
    return "Clear mapping from parents to labels";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/Parents.png");
  }
};
///@}
} // namespace process
} // namespace lgx

#endif
