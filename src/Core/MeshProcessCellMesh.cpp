/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "MeshProcessCellMesh.hpp"

#include "MeshProcessStructure.hpp"
#include "Progress.hpp"
#include "SetVector.hpp"

namespace lgx {
namespace process {
using std::swap;

bool FixMeshCorners::operator()(Mesh* mesh, bool autoSegment, bool selectBadVertices, int maxRun)
{
  vvgraph& S = mesh->graph();
  if(S.empty()) {
    setErrorMessage("You need a non-empty mesh to fix its labelling problems.");
    return false;
  }

  // Do not try to fix corners on cellular mesh (there should not be any)
  if(mesh->cells())
    return true;

  // select unlabeled/troublesome vertices and put them in a list during the first run of fix corner.
  // the subsequent re-segmentation will run only on them, so it goes faster.
  // Afterwards we take them out of the list if they are labeled.
  // new vertices resulting from subdivision are also in the list.
  util::set_vector<vertex> vCheckSwap;

  // Important for consistency among the mesh processes
  const std::vector<vertex>& avs = mesh->activeVertices();
  util::set_vector<vertex> vCheck;

  // Starts by checking border and unlabeled vertices
  forall(vertex v, avs)
    if(v->label <= 0)
      vCheck.insert(v);

  Progress progress(QString("Fixing border triangles for mesh %1").arg(mesh->userId()), 0, false);

  int tricount = 0, prevtricount = INT_MAX, totaltricount = 0;
  int vdelcnt = 0;
  int nRun;

  for(nRun = 0; not vCheck.empty() and prevtricount != 0 and nRun < maxRun; ++nRun) {
    if(nRun == 0)
      prevtricount = 0;
    std::vector<Point3ul> tris;
    // In first run we check all vertices, after re-segmentation we check only vertices which caused trouble in
    // previous runs.
    forall(vertex v, vCheck) {
      // Check for triangles with all vertices on the margin
      if(v->label == -1 and v->margin) {
        forall(vertex n, S.neighbors(v)) {
          vertex m = S.nextTo(v, n);
          if(!mesh->uniqueTri(v, n, m) or !n->margin or n->label != -1 or !m->margin or m->label != -1)
            continue;
          tris.push_back(Point3ul(v.id(), n.id(), m.id()));
        }
      }
    }

    // Margin triangles must have a vertex with valence 2, delete it
    vvgraph N;
    forall(const Point3ul& tri, tris) {
      for(int i = 0; i < 2; i++) {
        vertex v(tri[i]);
        if(S.valence(v) <= 2) {
          // Add the neighbor, who might now be on the margin
          forall(const vertex& n, S.neighbors(v))
            N.insert(n);
          vdelcnt++;
          S.erase(v);
          // Remove in neighbor set if there
          if(N.contains(v))
            N.erase(v);
        }
      }
    }
    // Check if neighbors of deleted vertices now on the margin
    markMargin(N, S, true);

    tris.clear();
    forall(vertex v, vCheck) {
      if(v->label == -1) {
        forall(vertex n, S.neighbors(v)) {         // Check for triangles with all vertices on the border
          vertex m = S.nextTo(v, n);
          if(!mesh->uniqueTri(v, n, m) or n->label != -1 or m->label != -1)
            continue;
          if(S.edge(n, m))
            tris.push_back(Point3ul(v.id(), n.id(), m.id()));
        }
      } else if(v->label == 0) {       // Check for unlabeled vertices that have only border neighbors
        bool allborder = true;
        forall(vertex n, S.neighbors(v))
          if(n->label != -1)
            allborder = false;

        if(allborder) {
          forall(vertex n, S.neighbors(v)) {
            vertex m = S.nextTo(v, n);
            if(S.edge(n, m))
              tris.push_back(Point3ul(v.id(), n.id(), m.id()));
          }
        }
      }

      // Subdivide internal borders ("islands") within skinny cells
      if(v->label == -1) {
        forall(vertex n, S.neighbors(v)) {
          if(n->label != -1)
            continue;
          if(S.valence(v) <= 1)
            Information::out << "Problem:with valence" << S.valence(v) << endl;

          vertex nn = S.nextTo(v, n);
          vertex pn = S.prevTo(v, n);

          if(nn->label > 0 and nn->label == pn->label and S.edge(n, nn))
            tris.push_back(Point3ul(v.id(), n.id(), nn.id()));
        }
      }
    }

    // Subdivide triangles and accumulate new vertices to check on next iteration
    tricount = 0;
    forall(const Point3ul& tri, tris) {
      vertex v(tri.x()), n(tri.y()), m(tri.z());
      v->label = m->label = n->label = 0;
      std::vector<vertex> newCheck;
      newCheck.push_back(v);
      newCheck.push_back(m);
      newCheck.push_back(n);
      if(m == S.nextTo(v, n)) {
        tricount++;
        if(S.edge(v, n) and S.edge(n, m) and S.edge(m, v))
          subdivideBisect(S, v, n, m, false, &newCheck);
        else
          Information::out << "Error:FixMeshCorners:Attempting to subdivide bad triangle" << endl;
      }
      vCheckSwap.insert(newCheck.begin(), newCheck.end());
    }
    markMargin(S, S, true);

    // re-segment mesh only for troublesome vertices (unlabeled, subdivided etc.)
    if(autoSegment and not vCheckSwap.empty()) {
      SegmentMesh sm(*this);
      bool result;
      result = sm(mesh, 0, vCheckSwap.vector());
      if(!result)
        return false;
    }

    totaltricount += tricount;

    Information::out << "nRun " << nRun << " tricount: " << tricount << " prevtricount: " << prevtricount
                     << " totaltricount: " << totaltricount << " size vCheck: " << vCheck.size() << endl;

    prevtricount = tricount;

    // Put in vCheck the list of vertices to check on the next round
    swap(vCheck, vCheckSwap);
    vCheckSwap.clear();
  }

  mesh->updateAll();

  SETSTATUS(vdelcnt << " margin vertices deleted and " << totaltricount << " border triangles subdivided for Mesh "
                    << mesh->userId() << " in " << nRun << " runs.");

  // un-select fixed vertices after last run, keep unlabeled and bad normals selected
  forall(vertex v, S)
    if(v->label == 0)
      vCheck.insert(v);
  if(not vCheck.empty()) {
    QString are_selected;
    if(selectBadVertices) {
      forall(vertex v, S)
        v->selected = false;
      forall(vertex v, vCheck)
        v->selected = true;
      mesh->showSelectedMesh();
      mesh->showMesh();
      are_selected = " All bad vertices are selected.";
    }
    QString errMsg = "Remaining bad vertices after fix corners. "
                     "%1 unlabeled vertices found.%2";
    return setErrorMessage(errMsg.arg(vCheck.size()).arg(are_selected));
  }

  return true;
}
REGISTER_MESH_PROCESS(FixMeshCorners);

namespace {
bool relabelCellMesh(Mesh* mesh)
{
  std::unordered_map<int, vertex> cell_map;
  vvgraph& S = mesh->graph();
  forall(const vertex& v, S) {
    int lab = v->label;
    if(lab >= 0) {
      if(cell_map.find(lab) != cell_map.end())
        return false;
      cell_map[lab] = v;
      // Then check that neighbors are all labeled -1
      forall(const vertex& n, S.neighbors(v))
        if(n->label >= 0)
          return false;
    } else if(v->type != 'j')
      v->type = 'j';
  }
  std::unordered_map<int, vertex>::iterator it;
  for(it = cell_map.begin(); it != cell_map.end(); ++it)
    it->second->type = 'c';
  mesh->setCells(true);
  return true;
}
} // namespace

bool MakeCellMesh::operator()(Mesh* mesh, float minWall, vvgraph& V, bool eraseMarginCells)
{
  vvgraph& S = mesh->graph();

  // Check if we want to change the original graph.
  bool changeCurrentMesh = (&S == &V);

  if(S.empty()) {
    setErrorMessage("You need a non-empty mesh to fix its labelling problems.");
    return false;
  }
  // First, check if it's not already a cell-mesh badly labeled
  if(relabelCellMesh(mesh))
    return true;
  // Then check for bad corners
  forall(vertex v, S) {
    if(v->label == 0) {
      setErrorMessage(QString("Cannot make cells, unlabeled vertex found on Mesh %1").arg(mesh->userId()));
      return false;
    }
    forall(vertex n, S.neighbors(v)) {
      // get unique triangles
      vertex m = S.nextTo(v, n);
      if(!mesh->uniqueTri(v, n, m))
        continue;
      if(v->label < 0 and n->label < 0 and m->label < 0) {
        setErrorMessage(QString("Cannot make cells, border triangles found for Mesh %1 (use fix corners)")
                        .arg(mesh->userId()));
        return false;
      }
    }
    // Check for extra borders ("islands") within skiny cells
    if(v->label == -1) {
      vertex start = S.anyIn(v);
      vertex n = start;
      do {
        if(n->label == -1) {
          vertex nn = S.nextTo(v, n);
          vertex pn = S.prevTo(v, n);
          if(nn->label > 0 and nn->label == pn->label) {
            setErrorMessage(
              QString(
                "Cannot make cells, island of label found in Mesh %1 for label nr %2 (use fix corners)")
              .arg(mesh->userId())
              .arg(nn->label));
            return false;
          }
        }
        n = S.nextTo(v, n);
      } while(n != start);
    }
  }

  // Create new graph
  vvgraph T;

  // Make centers
  typedef std::unordered_map<int, vertex> VMap;
  typedef std::pair<int, vertex> VMPair;
  VMap C;
  forall(vertex v, S) {
    forall(vertex n, S.neighbors(v)) {
      vertex m = S.nextTo(v, n);
      // get unique triangles
      if(!mesh->uniqueTri(v, n, m))
        continue;
      int label = mesh->getLabel(v, n, m, false);       // don't use parent label.
      if(label <= 0)
        continue;
      // If new label add to map
      VMap::iterator it = C.find(label);
      if(it == C.end()) {
        vertex c;
        c->label = label;
        c->pos = 0;
        c->area = 0;
        c->type = 'c';
        T.insert(c);
        it = C.insert(VMPair(label, c)).first;
      }
      vertex c = it->second;
      float area = triangleArea(v->pos, n->pos, m->pos);
      c->pos += (v->pos + n->pos + m->pos) / 3.0 * area;
      c->area += area;
    }
  }

  // Go through graph and count number of unique labels in neighborhood of
  // each border vertex
  util::set_vector<int> marginCellsLabel;
  forall(vertex v, S) {
    v->type = ' ';
    if(v->label == -1) {
      std::set<int> L;
      forall(vertex n, S.neighbors(v))
        if(n->label > 0)
          L.insert(n->label);
      v->labcount = L.size();
      // cells connected to margin vertices are considered as giant cells
      if(L.size() == 1)
        marginCellsLabel.insert(*L.begin());
    } else
      v->labcount = 0;
  }

  // Find all junctions
  for(VMap::iterator it = C.begin(); it != C.end(); it++) {
    vertex c = it->second;

    // Find a vertex labeled the same as c connected to an edge
    vertex v(0), n(0);
    bool found = false;
    forall(vertex w, S) {
      if(w->label == -1)
        forall(vertex m, S.neighbors(w))
          if(m->label == c->label) {
            found = true;
            v = w;
            n = m;
            break;
          }
      if(found)
        break;
    }
    if(!found) {
      setErrorMessage("processConvertCells::Error:Label not found finding junctions");
      return false;
    }

    // Walk along edge until junction found
    vertex start = v;
    double length = 0;
    do {
      if(v->labcount > 2 or v->type == 'j' or (minWall >= 0 and length > minWall)) {       // at a junction
        length = 0;
        if(v->type != 'j') {
          T.insert(v);
          v->type = 'j';
        }
      }
      n = S.prevTo(v, n);
      while(n->label != -1)
        n = S.prevTo(v, n);
      vertex m = n;
      length += (v->pos - n->pos).norm();
      n = v;
      v = m;
    } while(v != start);
  }

  // Connect centers to junctions (> 2 labels or margin and > 1 label)
  for(VMap::iterator it = C.begin(); it != C.end(); it++) {
    vertex c = it->second;
    c->pos /= c->area;

    // Find a vertex labeled the same as c connected to an edge
    vertex v(0), n(0);
    bool found = false;
    forall(const vertex& w, S) {
      if(w->label == -1)
        forall(const vertex& m, S.neighbors(w))
          if(m->label == c->label) {
            found = true;
            v = w;
            n = m;
            break;
          }
      if(found)
        break;
    }
    if(!found) {
      SETSTATUS("processConvertCells::Error:Label not found connecting centers");
      continue;
    }

    // Walk along edge until junction found
    vertex start = v;
    vertex pv(0);
    do {
      if(v->type == 'j') {       // at a junction
        // Join centers to junctions
        if(pv)
          T.spliceAfter(c, pv, v);
        else
          T.insertEdge(c, v);
        pv = v;
      }
      n = S.prevTo(v, n);
      while(n->label != -1)
        n = S.prevTo(v, n);
      vertex m = n;
      n = v;
      v = m;
    } while(v != start);
  }

  // Join junctions to centers
  forall(vertex v, T) {
    if(v->type == 'j') {     // at a junction
      int labcount = v->labcount;
      vertex n = S.anyIn(v);
      vertex pn(0);
      while(labcount--) {
        while(n->label == -1)
          n = S.nextTo(v, n);
        int label = n->label;
        VMap::iterator it = C.find(label);
        if(it == C.end()) {
          SETSTATUS("processConvertCells::Error:Label not found connecting junctions, label:" << label);
          continue;
        }
        if(pn)
          T.spliceAfter(v, pn, it->second);
        else
          T.insertEdge(v, it->second);
        pn = it->second;
        while(n->label == label)
          n = S.nextTo(v, n);
      }
    }
  }

  // Remove points that are too close to each other
  std::set<vertex> D;
  forall(vertex c, T) {
    if(c->type != 'c')
      continue;
    forall(vertex n, T.neighbors(c)) {
      vertex m = T.nextTo(c, n);
      if((m->pos - n->pos).norm() < minWall / 2.0) {
        if(D.find(n) != D.end() or D.find(m) != D.end())
          continue;
        if((n->margin and n->labcount == 1)or (!n->margin and n->labcount == 2))
          D.insert(n);
        else if((m->margin and m->labcount == 1)or (!m->margin and m->labcount == 2))
          D.insert(m);
      }
    }
  }
  forall(vertex v, D)
    if(T.contains(v))
      T.erase(v);

  // Connect junctions to each other
  forall(vertex c, T) {
    if(c->type != 'c')
      continue;
    forall(vertex n, T.neighbors(c)) {
      vertex m = T.nextTo(c, n);
      if(!T.edge(n, m))
        T.spliceBefore(n, c, m);
      if(!T.edge(m, n))
        T.spliceAfter(m, c, n);
    }
  }

  // take out giant cell
  if(eraseMarginCells) {
    forall(int label, marginCellsLabel) {
      std::cout << "label giant cell: " << label << std::endl;
      vertex c = C.find(label)->second;
      if(T.contains(c))
        T.erase(c);
    }
  }

  // set normals in the cellular mesh and fill in the labeld positions and normals
  IntPoint3fMap& cellPos = mesh->labelCenter();
  IntPoint3fMap& cellNorm = mesh->labelNormal();

  if(changeCurrentMesh) {
    cellPos.clear();
    cellNorm.clear();
  }

  forall(vertex v, T) {
    Mesh::setNormal(T, v);
    if(changeCurrentMesh and v->type == 'c') {
      cellPos[v->label] = v->pos;
      cellNorm[v->label] = v->nrml;
    }
  }

  V.swap(T);

  markMargin(V, V, true);

  SETSTATUS("Mesh " << mesh->userId() << " turned into cellular mesh with " << C.size() << " cells, " << V.size()
                    << " vertices in mesh. Erased " << marginCellsLabel.size() << " cells at the margin.");

  // if the current mesh is changed, set it to cellular mesh and update normals and everything
  if(changeCurrentMesh) {
    mesh->setCells(true);
    mesh->setNormals();
    mesh->updateAll();
  }
  return true;
}
REGISTER_MESH_PROCESS(MakeCellMesh);
} // namespace process
} // namespace lgx
