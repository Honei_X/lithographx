/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef TISSUECURVATURE_HPP
#define TISSUECURVATURE_HPP

#include <Process.hpp>

#include <MeshProcessCellMesh.hpp>
#include <MeshProcessCellAxis.hpp>

namespace lgx {
namespace process {
///\addtogroup MeshProcess
///@{
/**
 * \class TissueCurvature MeshProcessCurv.hpp <MeshProcessCurv.hpp>
 *
 * Compute the curvature tensor for each cell of a cell mesh. If the tissue
 * is a full mesh, it will first be simplified into a cell mesh.
 * */
class LGXCORE_EXPORT TissueCurvature : public MeshProcess {
public:
  TissueCurvature(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    bool ok;
    float neighborhood = parms[0].toFloat(&ok);
    if(not ok)
      return setErrorMessage("Error, parameter 'Radius' must be a number");
    return operator()(currentMesh(), neighborhood, stringToBool(parms[1]));
  }

  bool operator()(Mesh* mesh, float neighborhood, bool checkLabel);

  void showResult();

  QString folder() const {
    return "Cell Axis/Curvature";
  }
  QString name() const {
    return "Compute Tissue Curvature";
  }
  QString description() const
  {
    return "Compute curvature based on simplified mesh (from Make Cells) for a neighborhood of given radius.";
  }
  QStringList parmNames() const {
    return QStringList() << QString("Radius (%1)").arg(UM) << "Selected cell";
  }
  QStringList parmDescs() const {
    return QStringList() << QString("Radius (%1)").arg(UM) << "Selected cell";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "10.0"
                         << "No";
  }
  ParmChoiceMap parmChoice() const
  {
    ParmChoiceMap map;
    map[1] = booleanChoice();
    return map;
  }
  QIcon icon() const {
    return QIcon(":/images/Curvature.png");
  }
};

/**
 * \class DisplayTissueCurvature MeshProcessCurv.hpp <MeshProcessCurv.hpp>
 *
 * Once the curvature tensor has been computed, this process allows to change
 * how it is displayed.
 */
class LGXCORE_EXPORT DisplayTissueCurvature : public MeshProcess {
public:
  DisplayTissueCurvature(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;

    bool ok;
    float heatmapPercentile = parms[2].toFloat(&ok);
    if(not ok)
      return setErrorMessage("Error, argument 'Heatmap percentile' must be a number");
    float axisLineWidth = parms[6].toFloat(&ok);
    if(not ok)
      return setErrorMessage("Error, argument 'Line Width' must be a number");
    float axisLineScale = parms[7].toFloat(&ok);
    if(not ok)
      return setErrorMessage("Error, argument 'Line Scale' must be a number");
    float axisOffset = parms[8].toFloat(&ok);
    if(not ok)
      return setErrorMessage("Error, argument 'Line Offset' must be a number");
    float threshold = parms[9].toFloat(&ok);
    if(not ok)
      return setErrorMessage("Error, argument 'Threshold' must be a number");

    return (*this)(currentMesh(), parms[0], stringToBool(parms[1]), heatmapPercentile, parms[3], QColor(parms[4]),
                   QColor(parms[5]), axisLineWidth, axisLineScale, axisOffset, threshold);
  }

  bool operator()(Mesh* mesh, QString DisplayHeatMap, bool compareZero, float heatmapPercentile, QString DisplayCurv,
                  const QColor& ColorExpansion, const QColor& ColorShrinkage, float AxisLineWidth,
                  float AxisLineScale, float AxisOffset, float CurvThreshold);

  QString folder() const {
    return "Cell Axis/Curvature";
  }
  QString name() const {
    return "Display Tissue Curvature";
  }
  QString description() const {
    return "Display curvature based on cellular mesh";
  }
  QStringList parmNames() const
  {
    return QStringList() << "Heatmap"
                         << "Compare to 0"
                         << "Heatmap percentile"
                         << "Show Axis"
                         << "Color +"
                         << "Color -"
                         << "Line Width"
                         << "Line Scale"
                         << "Line Offset"
                         << "Threshold";
  }

  QStringList parmDescs() const
  {
    return QStringList() << "Display curvature values in max or min direction as a color map.\n"
                            "Average: (CurvMax + CurvMin)/2,  SignedAverageAbs: sign(CurvMax or CurvMin) x "
                            "(abs(CurvMax) + abs(CurvMin))/2, \n"
                            "Gaussian:(CurvMax x CurvMin), RootSumSquare: sqrt(CurvMax^2 + CurvMin^2), Anisotropy: "
                            "(CurvMax / CurvMin)."
                         << "Center the color map on zero, if negtive values exist."
                         << "Percentile of values used for color map upper-lower bounds."
                         << "Draw main curvature directions as vectors, scaled by 1/(curvature radius)."
                         << "Color used for convex (curvature > 0)"
                         << "Color used for concave (curvature < 0)"
                         << "Line width"
                         << "Length of the vectors = Scale * 1/(curvature radius)."
                         << "Draw the vector ends a bit tilted up for proper display on surfaces."
                         << "Minimal value of curvature required for drawing the directions.";
  }
  QStringList parmDefaults() const
  {
    return QStringList() << "SignedAverageAbs"
                         << "Yes"
                         << "85.0"
                         << "Both"
                         << "white"
                         << "red"
                         << "2.0"
                         << "100.0"
                         << "0.1"
                         << "0.0";
  }
  ParmChoiceMap parmChoice() const
  {
    ParmChoiceMap map;
    map[0] = QStringList() << "none"
                           << "CurvMax"
                           << "CurvMin"
                           << "Average"
                           << "SignedAverageAbs"
                           << "Gaussian"
                           << "RootSumSquare"
                           << "Anisotropy";
    map[1] = booleanChoice();
    map[3] = QStringList() << "Both"
                           << "CurvMax"
                           << "CurvMin"
                           << "none";
    map[4] = QColor::colorNames();
    map[5] = QColor::colorNames();
    return map;
  }
  QIcon icon() const {
    return QIcon(":/images/Curvature.png");
  }
};

class LGXCORE_EXPORT LoadTissueCurvature : public LoadCellAxis {
public:
  LoadTissueCurvature(const MeshProcess& process)
    : Process(process)
    , LoadCellAxis(process)
  {
  }

  bool operator()(Mesh* mesh, const QString& filename);

  QString folder() const {
    return "Cell Axis/Curvature";
  }
  QString name() const {
    return "Load Tissue Curvature";
  }
  QString description() const {
    return "Load the tissue curvature from a CSV file";
  }
};

class LGXCORE_EXPORT SaveTissueCurvature : public SaveCellAxis {
public:
  SaveTissueCurvature(const MeshProcess& process)
    : Process(process)
    , SaveCellAxis(process)
  {
  }

  bool operator()(Mesh* mesh, const QString& filename);

  QString folder() const {
    return "Cell Axis/Curvature";
  }
  QString name() const {
    return "Save Tissue Curvature";
  }
  QString description() const {
    return "Save the tissue curvature to a CSV file";
  }
};
///@}
} // namespace process
} // namespace lgx

#endif
