/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "MeshProcessHeatMap.hpp"

#include "Progress.hpp"

#include "ui_HeatMap.h"
#include "ui_LoadHeatMap.h"
#include <cmath>
#include <QFileDialog>
#include <QMessageBox>

namespace lgx {
namespace process {
bool ComputeHeatMap::initialize(QStringList& parms, QWidget* parent)
{
  dlg = new QDialog(parent);
  Ui_HeatMapDialog ui;
  ui.setupUi(dlg);

  connect(ui.HeatMapType, SIGNAL(currentIndexChanged(const QString &)),
          this, SLOT(changeMapType(const QString &)));
  connect(ui.SelectSpreadsheetFile, &QAbstractButton::clicked,
          this, &ComputeHeatMap::selectSpreadsheetFile);

  for(int i = 0; i < ui.HeatMapType->count(); ++i) {
    if(ui.HeatMapType->itemText(i) == parms[0]) {
      ui.HeatMapType->setCurrentIndex(i);
      break;
    }
  }
  for(int i = 0; i < ui.HeatMapSignal->count(); ++i) {
    if(ui.HeatMapSignal->itemText(i) == parms[1]) {
      ui.HeatMapSignal->setCurrentIndex(i);
      break;
    }
  }
  ui.SpreadsheetFile->setText(parms[2]);
  if(parms[3].isEmpty()) {
    ui.CreateSpreadsheet->setChecked(false);
  } else {
    QStringList spread = parms[3].split(",");
    ui.HeatMapRepGeom->setChecked(spread.contains("Geometry"));
    ui.HeatMapRepSig->setChecked(spread.contains("Signal"));
    ui.HeatMapRepBord->setChecked(spread.contains("Border"));
  }
  ui.HeatMapRange->setChecked(stringToBool(parms[4]));
  ui.RangeMin->setValue(parms[5].toFloat());
  ui.RangeMax->setValue(parms[6].toFloat());
  ui.HeatMapAvg->setChecked(stringToBool(parms[7]));
  ui.GlobalCoordinates->setChecked(stringToBool(parms[8]));
  ui.HeatMapPolarity->setChecked(parms[9] != "None");
  if(ui.HeatMapPolarity->isChecked()) {
    for(int i = 0; i < ui.HeatMapPolarityType->count(); ++i) {
      if(ui.HeatMapPolarityType->itemText(i) == parms[9]) {
        ui.HeatMapPolarityType->setCurrentIndex(i);
        break;
      }
    }
  }

  ui.HeatMapChange->setChecked(stringToBool(parms[10]));
  if(parms[11] == "Increasing")
    ui.HeatMapIncr->setCurrentIndex(0);
  else
    ui.HeatMapIncr->setCurrentIndex(1);
  if(parms[12] == "Ratio")
    ui.HeatMapDiffType->setCurrentIndex(0);
  else if(parms[12] == "Difference")
    ui.HeatMapDiffType->setCurrentIndex(1);
  else if(parms[12] == "Growth")
    ui.HeatMapDiffType->setCurrentIndex(2);

  ui.HeatMapGrowthTime->setValue(parms[13].toFloat());
  borderSize = parms[14].toFloat();

  bool go = dlg->exec() == QDialog::Accepted;
  if(go) {
    parms.clear();
    QStringList spread;
    if(ui.CreateSpreadsheet->isChecked()) {
      if(ui.HeatMapRepGeom->isChecked())
        spread << "Geometry";
      if(ui.HeatMapRepSig->isChecked())
        spread << "Signal";
      if(ui.HeatMapRepBord->isChecked())
        spread << "Border";
    }
    parms << ui.HeatMapType->currentText() << ui.HeatMapSignal->currentText() << ui.SpreadsheetFile->text()
          << spread.join(",") << (ui.HeatMapRange->isChecked() ? "Yes" : "No")
          << QString::number(ui.RangeMin->value()) << QString::number(ui.RangeMax->value())
          << (ui.HeatMapAvg->isChecked() ? "Yes" : "No") << (ui.GlobalCoordinates->isChecked() ? "Yes" : "No")
          << (ui.HeatMapPolarity->isChecked() ? ui.HeatMapPolarityType->currentText() : QString("None"))
          << (ui.HeatMapChange->isChecked() ? "Yes" : "No") << ui.HeatMapIncr->currentText()
          << ui.HeatMapDiffType->currentText() << QString::number(ui.HeatMapGrowthTime->value())
          << QString::number(borderSize);
  }
  delete dlg;
  dlg = 0;
  return go;
}

bool ComputeHeatMap::operator()(const QStringList& parms)
{
  MapType map;
  if(parms[0] == "Area")
    map = AREA;
  else if(parms[0] == "Volume")
    map = VOLUME;
  else if(parms[0] == "Walls")
    map = WALL;
  else {
    setErrorMessage("Error, first string must be one of 'Area', 'Volume' or 'Walls'");
    return false;
  }
  SignalType signal;
  if(parms[1] == "Geometry")
    signal = GEOMETRY;
  else if(parms[1] == "Border signal")
    signal = BORDER;
  else if(parms[1] == "Interior signal")
    signal = INTERIOR;
  else if(parms[1] == "Border/total")
    signal = BORDER_TOTAL;
  else if(parms[1] == "Interior/total")
    signal = INTERIOR_TOTAL;
  else if(parms[1] == "Total signal")
    signal = TOTAL;
  else {
    setErrorMessage("Error, second string must be one of 'Geometry', 'Border signal',"
                    " 'Interior signal', 'Border/total', 'Interior/total' or 'Total signal'");
    return false;
  }
  QString reportFile = parms[2];
  if(!reportFile.isEmpty() and !reportFile.endsWith(".csv", Qt::CaseInsensitive))
    reportFile += ".csv";
  QStringList spread = parms[3].split(",");
  int report = 0;
  if(spread.contains("Geometry"))
    report |= GEOMETRY;
  if(spread.contains("Signal"))
    report |= TOTAL;
  if(spread.contains("Border"))
    report |= BORDER;
  bool manualRange = stringToBool(parms[4]);
  float rangeMin = parms[5].toFloat();
  float rangeMax = parms[6].toFloat();
  if(manualRange and rangeMin >= rangeMax) {
    setErrorMessage("If range is manual, then rangeMin (2nd value) must be less than rangeMax (3rd value)");
    return false;
  }
  bool signalAverage = stringToBool(parms[7]);
  bool globalCoordinates = stringToBool(parms[8]);
  PolarityType polarity = NONE;
  if(parms[9] == "None")
    polarity = NONE;
  else if(parms[9] == "Cell Average")
    polarity = CELL_AVERAGE;
  else if(parms[9] == "Wall/Min")
    polarity = WALL_MIN;
  else {
    setErrorMessage("Error, 9th string must be one of 'None', 'Cell Average or 'Wall/Min'");
    return false;
  }

  MultiMapType multiMapType;
  float growthTime = parms[13].toFloat();
  bool hasChange = stringToBool(parms[10]);
  if(!hasChange)
    multiMapType = SINGLE;
  else {
    bool incr = parms[11] == "Increasing";
    bool ratio = parms[12] == "Ratio";
    bool diff = parms[12] == "Difference";
    bool growth = parms[12] == "Growth";
    if(growth and growthTime <= 0) {
      setErrorMessage("Time for growth rate cannot be <= 0)");
      return false;
    }
    if(incr) {
      if(ratio)
        multiMapType = INCREASING_RATIO;
      else if(diff)
        multiMapType = INCREASING_DIFF;
      else
        multiMapType = INCREASING_GROWTH;
    } else {
      if(ratio)
        multiMapType = DECREASING_RATIO;
      else if(diff)
        multiMapType = DECREASING_DIFF;
      else
        multiMapType = DECREASING_GROWTH;
    }
  }
  float borderSize = parms[14].toFloat();

  Mesh* mesh1 = 0, *mesh2 = 0;
  if(multiMapType == SINGLE) {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    mesh1 = currentMesh();
  } else {
    if(!checkState().mesh(MESH_NON_EMPTY, 0).mesh(MESH_NON_EMPTY, 1))
      return false;
    if(currentMesh() == mesh(0)) {
      mesh1 = mesh(0);
      mesh2 = mesh(1);
    } else if(currentMesh() == mesh(1)) {
      mesh2 = mesh(0);
      mesh1 = mesh(1);
    } else
      return false;
  }

  bool res = (*this)(mesh1, mesh2, map, signal, reportFile, report, manualRange, rangeMin, rangeMax, signalAverage,
                     globalCoordinates, polarity, multiMapType, growthTime, borderSize);

  if(res)
    currentMesh()->showHeat();

  return res;
}

bool ComputeHeatMap::operator()(Mesh* mesh1, Mesh* mesh2, MapType _map, SignalType _signal, const QString& reportFile,
                                int _report, bool _manualRange, float _rangeMin, float _rangeMax, bool _signalAverage,
                                bool _globalCoordinates, PolarityType _polarity, MultiMapType _multiMapType,
                                float _growthTime, float _borderSize)
{
  mapType = _map;
  signal = _signal;
  report = _report;
  manualRange = _manualRange;
  rangeMin = _rangeMin;
  rangeMax = _rangeMax;
  signalAverage = _signalAverage;
  globalCoordinates = _globalCoordinates;
  polarity = _polarity;
  multiMapType = _multiMapType;
  growthTime = _growthTime;
  borderSize = _borderSize;

  bool multiMap = multiMapType != SINGLE;
  if(multiMap and (!mesh2 or mesh2->graph().empty())) {
    setErrorMessage("Error, to compute a heat map based on two maps, you must specify two meshes");
    return false;
  }

  // Check if the report/signal can actually be computed

  if(mapType == VOLUME) {
    int anysignal = (BORDER | INTERIOR | TOTAL | BORDER_TOTAL | INTERIOR_TOTAL);
    if((mesh1 and (mesh1->stack()->empty() or mesh1->stack()->currentStore()->labels()))
       or (mesh2 and (mesh2->stack()->empty() or mesh2->stack()->currentStore()->labels()))) {
      if((signal & anysignal)or (report & anysignal))
        return setErrorMessage("Error, trying to measure 3D signal without a stack being loaded");
    }
  }

  QString sigStr = "";
  if(signalAverage and signal != GEOMETRY)
    sigStr += "Average ";

  // Heat map signal type
  switch(signal) {
  case GEOMETRY:
    switch(mapType) {
    case AREA:
      sigStr += "Area";
      break;
    case VOLUME:
      sigStr += "Volume";
      break;
    case WALL:
      sigStr += "Wall";
    }
    break;
  case BORDER:
    sigStr += "Border Signal";
    break;
  case INTERIOR:
    sigStr += "Interior Signal";
    break;
  case BORDER_TOTAL:
    sigStr += "Border/Total Signal";
    break;
  case INTERIOR_TOTAL:
    sigStr += "Interior/Total Signal";
    break;
  case TOTAL:
    sigStr += "Total Signal";
  }

  ImageHeatMaps map1, map2;
  if(!getLabelMaps(map1, mesh1))
    return false;

  IntFloatMap& LabelHeat = mesh1->labelHeat();
  IntIntFloatMap& WallHeat = mesh1->wallHeat();

  // If we are doing a change map, get it from the other mesh and take the ratio as the signal
  // Otherwise just copy the info
  LabelHeat.clear();
  WallHeat.clear();
  if(!multiMap) {
    if(mapType == AREA or mapType == VOLUME)
      LabelHeat = map1.LabelHeatAmt;
    else if(mapType == WALL)
      WallHeat = map1.WallHeatAmt;
  } else {
    switch(multiMapType) {
    case INCREASING_RATIO:
      sigStr += " increase ratio";
      break;
    case INCREASING_DIFF:
      sigStr += " increase difference";
      break;
    case INCREASING_GROWTH:
      sigStr += QString(" increase growth(%1)").arg(growthTime);
      break;
    case DECREASING_RATIO:
      sigStr += " decrease ratio";
      break;
    case DECREASING_DIFF:
      sigStr += " decrease difference";
      break;
    case DECREASING_GROWTH:
      sigStr += QString(" decrease growth(%1)").arg(growthTime);
      break;
    case SINGLE:
      break;
    }

    // Get values from other mesh
    if(!getLabelMaps(map2, mesh2))
      return false;

    // Change map for area and volume
    switch(mapType) {
    case AREA:
    case VOLUME: {
      std::vector<int> LabelDel;
      forall(const IntFloatPair& p, map1.LabelHeatAmt) {
        int i = p.first;
        // Calculate change
        if(map2.LabelHeatAmt.find(i) != map2.LabelHeatAmt.end()) {
          switch(multiMapType) {
          case INCREASING_RATIO:
            LabelHeat[i] = (map1.LabelHeatAmt[i] == 0 ? 0 : map2.LabelHeatAmt[i] / map1.LabelHeatAmt[i]);
            break;
          case DECREASING_RATIO:
            LabelHeat[i] = (map2.LabelHeatAmt[i] == 0 ? 0 : map1.LabelHeatAmt[i] / map2.LabelHeatAmt[i]);
            break;
          case INCREASING_DIFF:
            LabelHeat[i] = map2.LabelHeatAmt[i] - map1.LabelHeatAmt[i];
            break;
          case DECREASING_DIFF:
            LabelHeat[i] = map1.LabelHeatAmt[i] - map2.LabelHeatAmt[i];
            break;
          case INCREASING_GROWTH:
            if(growthTime == 0 or map1.LabelHeatAmt[i] == 0)
              LabelHeat[i] = 0;
            else
              LabelHeat[i] = (map2.LabelHeatAmt[i] / map1.LabelHeatAmt[i] - 1) / growthTime;
            break;
          case DECREASING_GROWTH:
            if(growthTime == 0 or map2.LabelHeatAmt[i] == 0)
              LabelHeat[i] = 0;
            else
              LabelHeat[i] = (map1.LabelHeatAmt[i] / map2.LabelHeatAmt[i] - 1) / growthTime;
            break;
          case SINGLE:
            break;
          }
        } else
          LabelDel.push_back(i);
      }
      // Delete labels not in both meshes
      forall(int i, LabelDel) {
        LabelHeat.erase(i);
        SETSTATUS("MakeHeatMap Warning mesh " << mesh1->userId() << " erasing label " << i);
      }
    } break;
    case WALL: {     // Calculate wall change map
      std::vector<IntIntPair> WallDel;
      forall(const IntIntFloatPair& p, map1.WallHeatAmt) {
        IntIntPair i = p.first;
        // Calculate change
        if(map2.WallHeatAmt.find(i) != map2.WallHeatAmt.end()) {
          switch(multiMapType) {
          case INCREASING_RATIO:
            WallHeat[i] = (map1.WallHeatAmt[i] == 0 ? 0 : map2.WallHeatAmt[i] / map1.WallHeatAmt[i]);
            break;
          case DECREASING_RATIO:
            WallHeat[i] = (map2.WallHeatAmt[i] == 0 ? 0 : map1.WallHeatAmt[i] / map2.WallHeatAmt[i]);
            break;
          case INCREASING_DIFF:
            WallHeat[i] = map2.WallHeatAmt[i] - map1.WallHeatAmt[i];
            break;
          case DECREASING_DIFF:
            WallHeat[i] = map1.WallHeatAmt[i] - map2.WallHeatAmt[i];
          case INCREASING_GROWTH:
            if(growthTime == 0 or map1.WallHeatAmt[i] == 0)
              WallHeat[i] = 0;
            else
              WallHeat[i] = (map2.WallHeatAmt[i] / map1.WallHeatAmt[i] - 1) / growthTime;
            break;
          case DECREASING_GROWTH:
            if(growthTime == 0 or map2.WallHeatAmt[i] == 0)
              WallHeat[i] = 0;
            else
              WallHeat[i] = (map1.WallHeatAmt[i] / map2.WallHeatAmt[i] - 1) / growthTime;
            break;
          case SINGLE:
            break;
          }
        } else
          WallDel.push_back(i);
      }
      // Delete labels not in both meshes
      forall(IntIntPair i, WallDel) {
        WallHeat.erase(i);
        SETSTATUS("MakeHeatMap::Warning:Mesh" << mesh1->userId() << " erasing wall " << i.first << "->"
                                              << i.second);
      }
    }
    }
  }

  // Adjust heat map if run on parents
  if(mesh1->toShow() == Mesh::PARENT) {
    Information::out << "Heat map table" << endl;
    forall(const IntFloatPair& p, LabelHeat)
      Information::out << p.first << " " << p.second << endl;
    IntFloatMap OldHeat = LabelHeat;
    LabelHeat.clear();
    IntIntMap& parentMap = mesh1->parentLabelMap();
    forall(const IntIntPair& p, parentMap) {
      if(p.second > 0)
        LabelHeat[p.first] = OldHeat[p.second];
      Information::out << "cell " << p.first << " has label " << p.second << " old heat:" << OldHeat[p.second]
                       << endl;
    }
  }

  // Calculate ranges if not using user defined range
  float heatMinAmt = rangeMin;
  float heatMaxAmt = rangeMax;
  if(!manualRange) {
    heatMinAmt = HUGE_VAL, heatMaxAmt = -HUGE_VAL;

    // Find max and min
    switch(mapType) {
    case AREA:
    case VOLUME:
      forall(const IntFloatPair& p, LabelHeat) {
        float amt = LabelHeat[p.first];
        if(amt < heatMinAmt)
          heatMinAmt = amt;
        if(amt > heatMaxAmt)
          heatMaxAmt = amt;
      }
      break;
    case WALL:
      forall(const IntIntFloatPair& p, WallHeat) {
        float amt = WallHeat[p.first];
        if(amt < heatMinAmt)
          heatMinAmt = amt;
        if(amt > heatMaxAmt)
          heatMaxAmt = amt;
      }
    }
  }
  mesh1->heatMapBounds() = Point2f(heatMinAmt, heatMaxAmt);

  // Find units
  QString gunits(""), hunits("");
  switch(mapType) {
  case WALL:
    gunits = UM;
    break;
  case AREA:
    gunits = UM2;
    break;
  case VOLUME:
    gunits = UM3;
    break;
  }
  if(!multiMap and signal == GEOMETRY)
    hunits = gunits;

  mesh1->heatMapUnit() = hunits;

  // Write to file
  if(report != 0) {
    QFile file(reportFile);
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
      SETSTATUS("makeHeatMap::Error:Cannot open output file:" << reportFile);
    } else {
      QTextStream out(&file);

      // Area heat map
      switch(mapType) {
      case AREA:
      case VOLUME: {
        // Write header
        out << "Label,Value";
        if(mapType == AREA) {
          out << QString(",Area (%1)").arg(UM2);
          if(report & BORDER) {
            out << QString(",Border Area (%1),Interior Area (%1)").arg(UM2);
          }
        } else {
          out << QString(",Volume (%1)").arg(UM3);
          if(report & BORDER) {
            out << QString(",Border Volume (%1),Interior Volume (%1)").arg(UM3);
          }
        }
        if((report & BORDER)or (report & TOTAL)) {
          out << ",Total Signal";
          if(report & BORDER) {
            out << ",Border Signal,Interior Signal";
          }
        }
        out << ",Center_X,Center_Y,Center_Z";
        out << endl;

        // Write data
        IntPoint3fMap& LabelCenter = mesh1->labelCenter();
        const qglviewer::Frame& frame = mesh1->stack()->getFrame();
        forall(const IntFloatPair& p, LabelHeat) {
          int label = p.first;
          out << label << "," << LabelHeat[label];
          out << "," << map1.LabelGeom[label];
          if(report & BORDER) {
            out << "," << map1.LabelBordGeom[label];
            out << "," << map1.LabelIntGeom[label];
          }
          if((report & SINGLE)or (report & TOTAL)) {
            out << "," << map1.LabelTotalSig[label];
            if(report & BORDER) {
              out << "," << map1.LabelBordSig[label];
              out << "," << map1.LabelIntSig[label];
            }
          }
          // Write cell centers
          if(globalCoordinates) {
            Point3f tr = Point3f(frame.inverseCoordinatesOf(qglviewer::Vec(LabelCenter[label])));
            out << "," << tr.x();
            out << "," << tr.y();
            out << "," << tr.z();
          } else {
            out << "," << LabelCenter[label].x();
            out << "," << LabelCenter[label].y();
            out << "," << LabelCenter[label].z();
          }

          out << endl;
        }
      } break;
      case WALL: {
        IntIntFloatMap& WallGeom = mesh1->wallGeom();
        // Write header
        out << QString("Label,Neighbor,Value,Wall Length (%1)").arg(UM);
        if(signal == TOTAL or (report & TOTAL))
          out << QString(",Wall Border Area (%1),Wall Signal").arg(UM2);
        out << endl;

        // Write data
        forall(const IntIntFloatPair& p, WallHeat) {
          IntIntPair wall = p.first;
          out << wall.first << "," << wall.second << ",";
          out << WallHeat[wall] << "," << WallGeom[wall];
          if(signal == TOTAL or (report & TOTAL))
            out << "," << map1.WallBordGeom[wall] << "," << map1.WallBordSig[wall];
          out << endl;
        }
      }
      }
      // Footer same for all heat maps
      QString msg = QString("%1 HeatMap results for mesh %2").arg(sigStr).arg(mesh1->userId());
      out << endl << endl << qPrintable(msg) << endl;

      out << "Signal range: " << heatMinAmt << "-" << heatMaxAmt << " " << hunits << endl;
      file.close();
    }
  }

  // Print out stats
  int ncells = map1.LabelGeom.size();
  int acells = map1.LabelTotalSig.size();
  double totageom = 0, totgeom = 0;
  if(mapType == AREA or mapType == VOLUME) {
    forall(const IntFloatPair& p, map1.LabelGeom) {
      totgeom += map1.LabelGeom[p.first];
      if(map1.LabelTotalSig.find(p.first) != map1.LabelTotalSig.end())
        totageom += map1.LabelGeom[p.first];
    }
    SETSTATUS("Mesh " << mesh1->userId() << " " << sigStr << " analysis, Cells: " << acells << "/" << ncells << ", "
                      << totageom << "/" << totgeom << " " << gunits);
  }
  SETSTATUS("Range min: " << heatMinAmt << " max: " << heatMaxAmt << " " << hunits);

  mesh1->updateTriangles();
  return true;
}

void ComputeHeatMap::selectSpreadsheetFile()
{
  QLineEdit* edit = dlg->findChild<QLineEdit*>("SpreadsheetFile");
  if(edit) {
    QString filename = edit->text();
    if(filename.isEmpty())
      filename = "spreadsheet.csv";
    filename = QFileDialog::getSaveFileName(dlg, "Select file to save report in", filename, "CSV files (*.csv)");
    if(!filename.isEmpty()) {
      if(!filename.endsWith(".csv", Qt::CaseInsensitive))
        filename += ".csv";
      edit->setText(filename);
    }
  } else
    SETSTATUS("SpreadsheetFile not found ...");
}

void ComputeHeatMap::changeMapType(const QString& type)
{
  QCheckBox* pol = dlg->findChild<QCheckBox*>("HeatMapPolarity");
  if(pol)
    pol->setEnabled(type == "Walls");
  else
    SETSTATUS("HeatMapPolarity not found");
  QComboBox* signal = dlg->findChild<QComboBox*>("HeatMapSignal");
  if(signal) {
    QString curtext = signal->currentText();
    signal->clear();
    if(type == "Area") {
      signal->addItem("Geometry");
      signal->addItem("Border signal");
      signal->addItem("Interior signal");
      signal->addItem("Border/total");
      signal->addItem("Interior/total");
      signal->addItem("Total signal");
    } else if(type == "Volume") {
      signal->addItem("Geometry");
      signal->addItem("Border signal");
      signal->addItem("Interior signal");
      signal->addItem("Border/total");
      signal->addItem("Interior/total");
      signal->addItem("Total signal");
    } else {
      signal->addItem("Geometry");
      signal->addItem("Total signal");
    }
    for(int i = 0; i < signal->count(); ++i) {
      if(signal->itemText(i) == curtext) {
        signal->setCurrentIndex(i);
        break;
      }
    }
  } else
    SETSTATUS("HeatMapSignal not found");
  QCheckBox* border = dlg->findChild<QCheckBox*>("HeatMapRepBord");
  if(border)
    border->setEnabled(type == "Area" || type == "Volume");
  else
    SETSTATUS("HeatMapRepBord not found");
}

void getBBox(const HVec3F& pts, Point3f* bBox)
{
  bBox[0] = Point3f(1e10, 1e10, 1e10);
  bBox[1] = -bBox[0];
  forall(const Point3f& pt, pts)
    for(int i = 0; i < 3; i++) {
      if(pt[i] < bBox[0][i])
        bBox[0][i] = pt[i];
      if(pt[i] > bBox[1][i])
        bBox[1][i] = pt[i];
    }
}

bool ComputeHeatMap::getLabelMaps(ImageHeatMaps& map, Mesh* mesh)
{
  const Stack* stack = mesh->stack();
  int label;

  IntFloatMap& LabelGeom = map.LabelGeom;
  IntFloatMap& LabelBordGeom = map.LabelBordGeom;
  IntFloatMap& LabelIntGeom = map.LabelIntGeom;
  IntFloatMap& LabelTotalSig = map.LabelTotalSig;
  IntFloatMap& LabelBordSig = map.LabelBordSig;
  IntFloatMap& LabelIntSig = map.LabelIntSig;
  IntFloatMap& LabelHeatAmt = map.LabelHeatAmt;
  IntHVec3uMap& LabelTris = map.LabelTris;
  IntVIdSetMap& LabelPts = map.LabelPts;

  IntIntFloatMap& WallBordGeom = map.WallBordGeom;
  IntIntFloatMap& WallBordSig = map.WallBordSig;
  IntIntFloatMap& WallHeatAmt = map.WallHeatAmt;

  IntPoint3fMap& LabelCenter = mesh->labelCenter();

  LabelGeom.clear();
  LabelBordGeom.clear();
  LabelIntGeom.clear();
  LabelTotalSig.clear();
  LabelBordSig.clear();
  LabelIntSig.clear();
  LabelTris.clear();
  LabelPts.clear();
  LabelHeatAmt.clear();
  LabelCenter.clear();

  WallBordGeom.clear();
  WallBordSig.clear();
  WallHeatAmt.clear();

  vvgraph& S = mesh->graph();

  // Find border vertices for wall maps or area maps that require it
  bool doborder = false;
  QList<SignalType> needBorder = QList<SignalType>() << BORDER << BORDER_TOTAL << INTERIOR << INTERIOR_TOTAL;
  if((mapType == AREA or mapType == VOLUME)and ((report & BORDER) or needBorder.contains(signal)))
    doborder = true;

  // Find all vertices within a limited distance from the walls
  if(doborder or mapType == WALL)
    mesh->updateWallGeometry(borderSize);

  if(mapType == AREA or mapType == VOLUME) {
    // For volumes, delete labels with open meshes
    std::set<int> LabelDel;

    // Loop over triangles
    forall(const vertex& v, S) {
      forall(const vertex& n, S.neighbors(v)) {
        vertex m = S.nextTo(v, n);
        if(!mesh->uniqueTri(v, n, m))
          continue;
        label = mesh->getLabel(v, n, m);
        if(label <= 0)
          continue;

        if(mapType == AREA) {
          // Total area, signal, and label center
          float area = triangleArea(v->pos, n->pos, m->pos);
          LabelGeom[label] += area;
          LabelCenter[label] += area * Point3f(v->pos + n->pos + m->pos) / 3.0;
          LabelTotalSig[label] += area * (v->signal + n->signal + m->signal) / 3.0;

          // Calculate how much of triangle is in border area
          if(doborder) {
            int border = 0;
            if(v->minb != 0)
              border++;
            if(n->minb != 0)
              border++;
            if(m->minb != 0)
              border++;
            // Border area and signal
            float bamt = float(border) / 3.0;
            LabelBordSig[label] += area * bamt * (v->signal + n->signal + m->signal) / 3.0;
            LabelBordGeom[label] += area * bamt;

            // Interior area and signal
            float iamt = float(3 - border) / 3.0;
            LabelIntSig[label] += area * iamt * (v->signal + n->signal + m->signal) / 3.0;
            LabelIntGeom[label] += area * iamt;
          }
        } else if(mapType == VOLUME) {
          // For volume just grab the triangle list
          // If any vertices of triangle are not the same as the label, then the label
          // does not define a closed volume, add it to deletion list
          // RSS: Fix this for parents
          if(v->label != label or n->label != label or m->label != label)
            LabelDel.insert(label);

          // Calculate volume enclosed by mesh and save triangles
          float volume = signedTetraVolume(v->pos, n->pos, m->pos);
          LabelGeom[label] += volume;
          LabelCenter[label] += volume * Point3f(v->pos + n->pos + m->pos) / 4.0;

          // Save triangles and points if processing volume signal
          if(signal != GEOMETRY or (report & TOTAL) or (report & BORDER)) {
            LabelTris[label].push_back(Point3ul(v.id(), n.id(), m.id()));
            LabelPts[label].insert(v.id());
            LabelPts[label].insert(n.id());
            LabelPts[label].insert(m.id());
          }
        }
      }
    }

    // Do the volume heat map signal calculation
    if(mapType == VOLUME) {
      // Remove labels which define volume calculations on open meshes
      forall(int i, LabelDel) {
        LabelTotalSig.erase(i);
        SETSTATUS("MakeHeatMap::Warning: Mesh " << mesh->userId() << " open mesh for Vol calc, erasing label "
                                                << i);
      }

      // Get mesh volume fluorescence
      if(signal != GEOMETRY or (report & TOTAL) or (report & BORDER)) {
        // Get current store
        const Store* store = mesh->stack()->currentStore();
        if(!store) {
          setErrorMessage("Volume signal calculation requires associated store");
          return false;
        }
        const HVecUS& data = store->data();

        // Clear previous geometries and use pixel based volumes, keep the centers
        LabelGeom.clear();
        LabelBordGeom.clear();
        LabelIntGeom.clear();

        // Loop over labels
        uint step = 0;
        Progress progress(QString("Finding signal inside mesh %1").arg(mesh->userId()), LabelGeom.size());

        forall(const IntPoint3fPair& p, LabelCenter) {
          int label = p.first;

          // Process triangle and point lists
          IntIntMap vmap;
          HVec3F pts(LabelPts[label].size());
          int idx = 0;
          forall(const ulong u, LabelPts[label]) {
            vertex v(u);
            pts[idx] = v->pos;
            vmap[u] = idx++;
          }
          // Calculate triangle normals.
          idx = 0;
          HVec3F nrmls(LabelTris[label].size());
          // Convert triangle index list to new vertex list
          forall(Point3u& tri, LabelTris[label]) {
            tri = Point3u(vmap[tri.x()], vmap[tri.y()], vmap[tri.z()]);
            if(tri.x() > pts.size() or tri.y() > pts.size() or tri.z() > pts.size()) {
              setErrorMessage("Error, bad triangle index");
              return false;
            }
            Point3f a = pts[tri.x()], b = pts[tri.y()], c = pts[tri.z()];
            nrmls[idx++] = ((b - a) ^ (c - a)).normalize();
          }

          // Get bounding box
          Point3f bBoxW[2];
          getBBox(pts, bBoxW);
          BoundingBox3i bBoxI(stack->worldToImagei(bBoxW[0]),
                              stack->worldToImagei(bBoxW[1]) + Point3i(1, 1, 1));
          Point3i imgSize = stack->size();
          bBoxI &= BoundingBox3i(Point3i(0, 0, 0), imgSize);
          Point3i size = bBoxI.size();

          // call cuda
          if(!progress.advance(step++))
            userCancel();
          HVecUS totMask(size_t(size.x()) * size.y() * size.z(), 0),
          bordMask(size.x() * size.y() * size.z(), 0);
          if(insideMeshGPU(bBoxI[0], size, stack->step(), stack->origin(), pts, LabelTris[label], nrmls,
                           totMask))
            return false;
          if(!progress.advance(0))
            userCancel();

          if(doborder) {
            if(nearMeshGPU(bBoxI[0], size, stack->step(), stack->origin(), pts, nrmls, 0, borderSize,
                           bordMask))
              return false;
            if(!progress.advance(0))
              userCancel();
          }

          // Count interior, border, and total pixels
          uint borderPix = 0, interiorPix = 0, totalPix = 0;

          // sum results
          for(int z = bBoxI[0].z(); z < bBoxI[1].z(); ++z)
            for(int y = bBoxI[0].y(); y < bBoxI[1].y(); ++y) {
              int xIdx = stack->offset(bBoxI[0].x(), y, z);
              for(int x = bBoxI[0].x(); x < bBoxI[1].x(); ++x, ++xIdx) {
                size_t mIdx = ((size_t(z) - bBoxI[0].z()) * size.y() + y - bBoxI[0].y()) * size.x() + x
                  - bBoxI[0].x();
                if(totMask[mIdx]) {
                  LabelTotalSig[label] += data[xIdx];
                  totalPix++;
                  if(doborder and bordMask[mIdx]) {
                    borderPix++;
                    LabelBordSig[label] += data[xIdx];
                  } else {
                    interiorPix++;
                    LabelIntSig[label] += data[xIdx];
                  }
                }
              }
            }

          // Calculate volume and average signal per label
          float pixVolume = stack->step().x() * stack->step().y() * stack->step().z();

          LabelGeom[label] = totalPix * pixVolume;
          LabelBordGeom[label] = borderPix * pixVolume;
          LabelIntGeom[label] = interiorPix * pixVolume;

          LabelTotalSig[label] *= pixVolume;
          LabelBordSig[label] *= pixVolume;
          LabelIntSig[label] *= pixVolume;
        }
      }
    }

    // Fill in heatmap color
    forall(const IntFloatPair& p, LabelGeom) {
      int label = p.first;

      // Calculate label center
      LabelCenter[label] = (LabelGeom[label] == 0 ? Point3f(0, 0, 0) : LabelCenter[label] / LabelGeom[label]);

      float totalSignal, bordSignal = 0, intSignal = 0;

      // Set signal amounts, average over area if required
      totalSignal = LabelTotalSig[label];
      if(doborder) {
        bordSignal = LabelBordSig[label];
        intSignal = LabelIntSig[label];
      }
      if(signalAverage) {
        totalSignal = (LabelGeom[label] == 0 ? 0 : totalSignal / LabelGeom[label]);
        if(doborder) {
          bordSignal = (LabelBordGeom[label] == 0 ? 0 : bordSignal / LabelBordGeom[label]);
          intSignal = (LabelIntGeom[label] == 0 ? 0 : intSignal / LabelIntGeom[label]);
        }
      }

      // Fill in heat depending on user choice
      switch(signal) {
      case GEOMETRY:
        LabelHeatAmt[label] = LabelGeom[label];
        break;
      case BORDER:
        LabelHeatAmt[label] = bordSignal;
        break;
      case BORDER_TOTAL:
        LabelHeatAmt[label] = (totalSignal == 0 ? 0 : bordSignal / totalSignal);
        break;
      case INTERIOR:
        LabelHeatAmt[label] = intSignal;
        break;
      case INTERIOR_TOTAL:
        LabelHeatAmt[label] = (totalSignal == 0 ? 0 : intSignal / totalSignal);
        break;
      case TOTAL:
        LabelHeatAmt[label] = totalSignal;
        break;
      }
    }
  } else if(mapType == WALL) {   // Do Wall heatmap
    IntIntFloatMap& WallGeom = mesh->wallGeom();

    forall(const IntIntFloatPair& wall, WallGeom) {
      LabelGeom[wall.first.first] += wall.second;
    }

    // Find area, and signal on each wall
    if(signal == TOTAL or (report & TOTAL)) {
      forall(const vertex& v, S) {
        forall(const vertex& n, S.neighbors(v)) {
          vertex m = S.nextTo(v, n);
          if(!mesh->uniqueTri(v, n, m))
            continue;

          // Sum signal and area
          IntIntPair wall;
          int label = mesh->getLabel(v, n, m);
          if(mesh->isBordTriangle(label, v, n, m, wall)) {
            float area = triangleArea(v->pos, n->pos, m->pos);
            WallBordGeom[wall] += area;
            WallBordSig[wall] += (v->signal + n->signal + m->signal) / 3.0 * area;
            if(polarity != NONE) {
              LabelBordGeom[wall.first] += area;
              LabelBordSig[wall.first] += (v->signal + n->signal + m->signal) / 3.0 * area;
            }
          }
        }
      }
    }

    forall(const IntIntFloatPair& wall, WallGeom) {
      // Fill in heat depending on user choice
      switch(signal) {
      case GEOMETRY:
        // Heat based on wall length
        if(polarity == CELL_AVERAGE)
          WallHeatAmt[wall.first]
            = (LabelGeom[wall.first.first] == 0 ? 0 : WallGeom[wall.first] / LabelGeom[wall.first.first]);
        else
          WallHeatAmt[wall.first] = WallGeom[wall.first];
        break;
      case TOTAL:
        // Heat based on wall signal
        WallHeatAmt[wall.first] = WallBordSig[wall.first];
        // Divide by area if required
        if(signalAverage)
          WallHeatAmt[wall.first]
            = (WallBordGeom[wall.first] == 0 ? 0 : WallHeatAmt[wall.first] / WallBordGeom[wall.first]);
        // Do relative to cell average
        if(polarity == CELL_AVERAGE) {
          if(signalAverage)
            WallHeatAmt[wall.first]
              = (LabelBordSig[wall.first.first] == 0 or LabelBordGeom[wall.first.first] == 0
                 ? 0
                 : WallHeatAmt[wall.first] / (LabelBordSig[wall.first.first] / LabelBordGeom[wall.first.first]));
          else
            WallHeatAmt[wall.first]
              = (LabelBordSig[wall.first.first] == 0
                 ? 0
                 : WallHeatAmt[wall.first] / LabelBordSig[wall.first.first]);
        }
        break;
      case BORDER:
      case INTERIOR:
      case BORDER_TOTAL:
      case INTERIOR_TOTAL:
        break;
      }
    }

    // Maps for polarity calc relative to minimum wall
    if(polarity == WALL_MIN) {
      IntFloatMap LabelMin;
      // Find min heat per cell
      forall(const IntIntFloatPair& wall, WallHeatAmt)
        if(LabelMin.find(wall.first.first) == LabelMin.end())
          LabelMin[wall.first.first] = wall.second;
        else if(LabelMin[wall.first.first] > wall.second)
          LabelMin[wall.first.first] = wall.second;
      // Divide heat amounts
      forall(const IntIntFloatPair& wall, WallHeatAmt)
        WallHeatAmt[wall.first]
          = (LabelMin[wall.first.first] == 0 ? 0 : WallHeatAmt[wall.first] / LabelMin[wall.first.first]);
    }
  }
  return true;
}
REGISTER_MESH_PROCESS(ComputeHeatMap);

bool DeleteHeatRangeLabels::operator()(Mesh* mesh, bool rescale, bool deleteCells, float min, float max)
{
  using std::isnan;
  Point2f& heatMapBounds = mesh->heatMapBounds();
  if(isnan(min))
    min = heatMapBounds[0];
  if(isnan(max))
    max = heatMapBounds[1];
  if(min >= max) {
    setErrorMessage("Error, the minimum must be strictly less than the maximum");
    return false;
  }
  IntFloatMap& labelHeat = mesh->labelHeat();
  if(labelHeat.empty())
    return true;
  vvgraph& S = mesh->graph();
  std::vector<vertex> T;

  float vmin = 1, vmax = 0;

  IntSet deleteLabel;
  for(IntFloatMap::iterator it = labelHeat.begin(); it != labelHeat.end(); ++it) {
    float v = it->second;
    if(v >= min and v <= max)
      deleteLabel.insert(it->first);
    else if(rescale) {
      if(v < vmin)
        vmin = v;
      if(v > vmax)
        vmax = v;
    }
  }
  if(deleteLabel.empty())
    return true;

  if(rescale and vmin < vmax)
    heatMapBounds = Point2f(vmin, vmax);

  forall(const vertex& v, S) {
    if(deleteLabel.count(v->label)) {
      if(deleteCells)
        T.push_back(v);
    }
  }
  forall(const vertex& v, T)
    S.erase(v);
  forall(int label, deleteLabel)
    labelHeat.erase(label);

  if(deleteCells) {
    // At last, erase all vertices not part of a triangle
    do {
      T.clear();
      forall(const vertex& v, S) {
        if(S.valence(v) < 2)
          T.push_back(v);
        else if(S.valence(v) == 2) {
          const vertex& n = S.anyIn(v);
          const vertex& m = S.nextTo(v, n);
          if(!S.edge(n, m))
            T.push_back(v);
        }
      }
      forall(const vertex& v, T)
        S.erase(v);
    } while(!T.empty());
    mesh->updateLines();
  }
  mesh->updateAll();
  return true;
}
REGISTER_MESH_PROCESS(DeleteHeatRangeLabels);

bool RescaleHeatMap::operator()(Mesh* m, float minValue, float maxValue)
{
  using std::isnan;

  if(isnan(minValue))
    minValue = m->heatMapBounds()[0];
  if(isnan(maxValue))
    maxValue = m->heatMapBounds()[1];

  if(maxValue <= minValue)
    maxValue = minValue + 1;

  m->heatMapBounds() = Point2f(minValue, maxValue);

  return true;
}
REGISTER_MESH_PROCESS(RescaleHeatMap);

bool SaveHeatMap::initialize(QStringList& parms, QWidget* parent)
{
  QString& filename = parms[0];
  if(filename.isEmpty())
    filename = QFileDialog::getSaveFileName(parent, "Choose spreadsheet file to save", QDir::currentPath(),
                                            "CSV files (*.csv)");
  if(filename.isEmpty())
    return false;
  if(!filename.endsWith(".csv", Qt::CaseInsensitive))
    filename += ".csv";
  parms[0] = filename;
  return true;
}

bool SaveHeatMap::operator()(Mesh* mesh, const QString& filename)
{
  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly)) {
    setErrorMessage(QString("File '%1' cannot be opened for writing").arg(filename));
    return false;
  }
  QTextStream out(&file);

  out << "Label,Value" << endl;
  forall(const IntFloatPair& p, mesh->labelHeat())
    out << p.first << "," << p.second << endl;

  SETSTATUS(QString("Wrote heat map file with %1 labels").arg(mesh->labelHeat().size()));
  return true;
}
REGISTER_MESH_PROCESS(SaveHeatMap);

bool LoadHeatMap::initialize(QStringList& parms, QWidget* parent)
{
  Ui_LoadHeatMap ui;
  dlg = new QDialog(parent);
  ui.setupUi(dlg);
  connect(ui.SelectHeatMapFile, &QAbstractButton::clicked,
          this, (void (LoadHeatMap::*)())&LoadHeatMap::selectFile);
  connect(ui.HeatMapColumn, SIGNAL(currentIndexChanged(int)), this, SLOT(selectColumn(int)));
  this->ui = &ui;
  ui.HeatMapColumn->setSizeAdjustPolicy(QComboBox::AdjustToContents);
  if(!parms[0].isEmpty())
    selectFile(parms[0]);
  if(dlg->exec() == QDialog::Accepted) {
    parms[0] = ui.HeatMapFile->text();
    parms[1] = QString::number(ui.HeatMapColumn->currentIndex());
    return true;
  }
  return false;
}

void LoadHeatMap::selectFile(const QString& filename)
{
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly)) {
    QMessageBox::critical(dlg, "Error opening file",
                          QString("File '%1' cannot be opened for reading").arg(filename));
    return;
  }
  QTextStream ss(&file);
  QString line = ss.readLine();
  QStringList fields = line.split(",");
  if(fields.size() < 2) {
    QMessageBox::critical(dlg, "Error reading file",
                          QString("File '%1' should be a CSV file with at least 3 columns").arg(filename));
    return;
  }
  QRegExp unit(".*\\((.*)\\)\\s*");
  ui->HeatMapColumn->clear();
  column_unit.clear();
  for(int i = 0; i < fields.size(); ++i) {
    QString txt = fields[i].trimmed();
    if(txt[0] == '"' and txt[txt.size() - 1] == '"')
      txt = txt.mid(1, txt.size() - 2).trimmed();
    if(unit.indexIn(txt) == 0)
      column_unit << unit.cap(1);
    else
      column_unit << "";
    ui->HeatMapColumn->addItem(txt);
  }
  if(fields[1].trimmed() == "Neighbor")
    ui->HeatMapColumn->setCurrentIndex(3);
  else
    ui->HeatMapColumn->setCurrentIndex(2);
  ui->HeatMapFile->setText(filename);
}

void LoadHeatMap::selectFile()
{
  QString filename = QFileDialog::getOpenFileName(dlg, "Choose spreadsheet to load", ui->HeatMapFile->text(),
                                                  "CSV files (*.csv);;All files (*.*)");
  if(!filename.isEmpty())
    selectFile(filename);
}

void LoadHeatMap::selectColumn(int i)
{
  if(i >= 0 and i < column_unit.size())
    ui->unitLabel->setText(column_unit[i]);
}

bool LoadHeatMap::operator()(Mesh* mesh, const QString& filename, int column, float border_size)
{
  if(column < 0) {
    setErrorMessage(QString("Invalid column '%1'").arg(column));
    return false;
  }

  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly)) {
    setErrorMessage(QString("File '%1' cannot be opened for reading").arg(filename));
    return false;
  }
  QTextStream ss(&file);
  QString line = ss.readLine();
  QStringList fields = line.split(",");
  if(fields.size() < column + 1) {
    setErrorMessage(
      QString("File '%1' should be a CSV file with at least %2 columns").arg(filename).arg(column + 1));
    return false;
  }
  QRegExp re_unit(".*\\((.*)\\)\\s*");
  QString unit;
  QString txt = fields[column].trimmed();
  int nb_fields = fields.size();
  if(re_unit.indexIn(txt) == 0)
    unit = re_unit.cap(1);
  QString col2 = fields[1];
  if(col2[0] == '"' and col2[col2.size() - 1] == '"')
    col2 = col2.mid(1, col2.size() - 2).trimmed();
  bool is_wall = col2.trimmed() == "Neighbor";
  if(is_wall)
    mesh->updateWallGeometry(border_size);
  float minValue = HUGE_VAL, maxValue = -HUGE_VAL;
  int line_nb = 1;
  IntFloatMap& labelMap = mesh->labelHeat();
  IntIntFloatMap& wallMap = mesh->wallHeat();
  while(ss.status() == QTextStream::Ok) {
    ++line_nb;
    fields = ss.readLine().split(",");
    if(fields.size() != nb_fields or fields[0].isEmpty())
      break;
    bool ok;
    int neighbor = 0;
    int label = fields[0].toInt(&ok);
    if(!ok) {
      setErrorMessage(QString("Error line %1: the first column is not an integer number but '%2'")
                      .arg(line_nb)
                      .arg(fields[0].trimmed()));
      return false;
    }
    if(is_wall) {
      neighbor = fields[1].toInt(&ok);
      if(!ok) {
        setErrorMessage(QString("Error line %1: the second column is not an integer number but '%2'")
                        .arg(line_nb)
                        .arg(fields[1].trimmed()));
        return false;
      }
    }
    float value = fields[column].toFloat(&ok);
    if(!ok) {
      setErrorMessage(QString("Error line %1: the column %2 is not an floating point number but '%3'")
                      .arg(line_nb)
                      .arg(column)
                      .arg(fields[column].trimmed()));
      return false;
    }
    if(is_wall)
      wallMap[std::make_pair(label, neighbor)] = value;
    else
      labelMap[label] = value;
    if(value < minValue)
      minValue = value;
    if(value > maxValue)
      maxValue = value;
  }
  if(minValue >= maxValue)
    return setErrorMessage("The file contains 0 or 1 value only. We cannot create a heat map from that.");
  mesh->heatMapUnit() = unit;
  mesh->heatMapBounds() = Point2f(minValue, maxValue);
  mesh->updateTriangles();
  mesh->showHeat();
  SETSTATUS(QString("Loaded heat map with values ranging from %1%3 to %2%3").arg(minValue).arg(maxValue).arg(unit));
  return true;
}
REGISTER_MESH_PROCESS(LoadHeatMap);
} // namespace process
} // namespace lgx
