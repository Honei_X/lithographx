/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef STACKPROCESSANALYSES_HPP
#define STACKPROCESSANALYSES_HPP

#include <Process.hpp>

#include <Stack.hpp>
#include <Store.hpp>

namespace lgx {
namespace process {

///\addtogroup StackProcess
///@{
/**
 * \class ComputeVolume StackProcessAnalyses.hpp <StackProcessAnalyses.hpp>
 *
 * Compute the volume of each label and write the result in a CSV file.
 *
 * The volume is computed as the product between the number of voxel for each
 * label, and the volume of a single voxel.
 */
class LGXCORE_EXPORT ComputeVolume : public StackProcess {
public:
  ComputeVolume(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const QStringList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY | STORE_LABEL))
      return false;
    Store* input = currentStack()->currentStore();
    return (*this)(input, parms[0]);
  }

  bool operator()(const Store* input, QString filename);

  QString name() const override {
    return "Compute Volumes";
  }
  QString description() const override
  {
    return "Compute the volumes of the labels, i.e. the number of voxels multiplied by the vocel size.";
  }
  QString folder() const override {
    return "Analyses";
  }
  QStringList parmNames() const override {
    return QStringList() << "Filename";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Filename";
  }
  QStringList parmDefaults() const override {
    return QStringList() << "output.csv";
  }
  QIcon icon() const override {
    return QIcon(":/images/CellFiles3D.png");
  }
};

class LGXCORE_EXPORT ComputeCellLength : public StackProcess {
public:
  ComputeCellLength(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  { }

  bool initialize(QStringList& parms, QWidget* parent) override;

  bool operator()(const QStringList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY | STORE_LABEL))
      return false;
    QString axis_str = parms[1].toLower();
    bool ok = true;
    Point3f axis;
    if(axis_str == "x")
      axis = Point3f(1,0,0);
    else if(axis_str == "y")
      axis = Point3f(0,1,0);
    else if(axis_str == "z")
      axis = Point3f(0,0,1);
    else {
      QStringList fields = axis_str.split(" ", QString::SkipEmptyParts);
      if(fields.size() == 3) {
        float x, y, z;
        x = fields[0].toFloat(&ok);
        if(ok)
          y = fields[1].toFloat(&ok);
        if(ok)
          z = fields[1].toFloat(&ok);
        if(ok)
          axis = normalized(Point3f(x,y,z));
      }
    }
    if(not ok)
      return setErrorMessage("The axis must be 'X', 'Y', 'Z' or three numbers separated by a space.");
    float percentile = parms[2].toFloat(&ok);
    if(not ok or percentile < 0 or percentile > 100)
      return setErrorMessage("The percentile must be a number between 0 and 100");
    return (*this)(currentStack()->currentStore(), axis, percentile, parms[0]);
  }

  bool operator()(Store* store, const Point3f axis, float percentile, const QString filename);

  QString name() const override {
    return "Compute Cell Length";
  }
  QString description() const override
  {
    return "Compute the length of the segmented cells along a given direction\n"
           " The current stack must contained a labeled and a non-labeled store.";
  }
  QString folder() const override {
    return "Analyses";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Filename"
                         << "Axis"
                         << "Percentile";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "The result will be saved in this file. Cannot be empty."
                         << "Axis along which the length of the cell is. This is a global axis.\n"
                            "This can be 'X', 'Y', 'Z' or three numbers separated by a space and defining a 3D vector"
                         << "How many of the voxels to take into account. Must be a number between 0 and 100.";
  }
  QStringList parmDefaults() const override
  {
    return QStringList() << "output.csv"
                         << "X"
                         << "95";
  }
  QIcon icon() const override {
    return QIcon(":/images/ruler.png");
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] << "X" << "Y" << "Z";
    return map;
  }

};

class LGXCORE_EXPORT ComputeStackSignal : public StackProcess {
public:
  ComputeStackSignal(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool initialize(QStringList& parms, QWidget* parent) override;

  bool operator()(const QStringList& parms) override
  {
    using std::swap;
    if(currentStack()->empty())
      return setErrorMessage("Error, current stack is empty");
    Store* labels = currentStack()->main();
    Store* signal = currentStack()->work();
    if(not labels->labels())
      swap(labels, signal);
    if(not labels->labels() or signal->labels())
      return setErrorMessage("Error, you must have one labeled and one non-labeled store in the current stack");
    return (*this)(labels, signal, stringToBool(parms[1]), parms[0]);
  }

  bool operator()(const Store* labels, const Store* signal, bool volumetric, const QString& filename);

  QString name() const override {
    return "Compute Signal";
  }
  QString description() const override
  {
    return "Compute the amount of signal per label on a stack."
           " The current stack must contained a labeled and a non-labeled store.";
  }
  QString folder() const override {
    return "Analyses";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Filename"
                         << "Per unit volume";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "If not empty, the result will be saved in this file."
                         << "If true, the stack signal is normalize w.r.t. the cell volume";
  }
  QStringList parmDefaults() const override
  {
    return QStringList() << "output.csv"
                         << "false";
  }
  QIcon icon() const override {
    return QIcon(":/images/CellFiles3D.png");
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] << booleanChoice();
    return map;
  }
};

///@}
} // namespace process
} // namespace lgx

#endif
