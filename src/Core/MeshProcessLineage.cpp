/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "MeshProcessLineage.hpp"

#include "Progress.hpp"

#include <QFileDialog>
#include <QMessageBox>

namespace lgx {
namespace process {
bool SaveParents::initialize(QStringList& parms, QWidget* parent)
{
  QString filename
    = QFileDialog::getSaveFileName(parent, "Select file to save labels in", parms[0], "CSV files (*.csv)");
  // check filename
  if(filename.isEmpty())
    return false;
  // check ending, add suffix if required
  QString suffix = ".csv";
  if(filename.right(suffix.size()) != suffix)
    filename.append(suffix);

  parms[0] = filename;
  return true;
}

bool SaveParents::operator()(Mesh* mesh, const QString& filename)
{
  const IntIntMap& parentLabel = mesh->parentLabelMap();
  if(parentLabel.size() == 0) {
    setErrorMessage(QString("There are no parent labels").arg(filename));
    return false;
  }

  QFile file(filename);

  if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
    setErrorMessage(QString("File '%1' cannot be opened for writing").arg(filename));
    return false;
  }
  QTextStream out(&file);
  out << "Label, Parent Label" << endl;

  // store to file
  SETSTATUS("Storing " << parentLabel.size() << " parent labels to file " << filename);
  forall(const IntIntPair& p, parentLabel) {
    int label = p.first;
    int parent = p.second;
    out << label << "," << parent << endl;
  }

  file.close();
  return true;
}
REGISTER_MESH_PROCESS(SaveParents);

bool LoadParents::initialize(QStringList& parms, QWidget* parent)
{
  QString filename
    = QFileDialog::getOpenFileName(parent, "Choose parent label file to load", parms[0], "CSV files (*.csv)");

  // check file
  if(filename.isEmpty())
    return false;

  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly)) {
    QMessageBox::critical(parent, "Error opening file",
                          QString("File '%1' cannot be opened for reading").arg(filename));
    return false;
  }
  QTextStream ss(&file);
  QString line = ss.readLine();
  QStringList fields = line.split(",");
  if(fields.size() != 2) {
    QMessageBox::critical(parent, "Error reading file",
                          QString("File '%1' should be a CSV file with 2 columns").arg(filename));
    return false;
  }

  // ok now we're happy
  parms[0] = filename;
  return true;
}

bool LoadParents::operator()(Mesh* mesh, const QString& filename)
{
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly)) {
    setErrorMessage(QString("File '%1' cannot be opened for reading").arg(filename));
    return false;
  }
  QTextStream ss(&file);
  QString line = ss.readLine();
  QStringList fields = line.split(",");
  if(fields.size() != 2) {
    setErrorMessage(QString("File '%1' should be a CSV file with 2 columns").arg(filename));
    return false;
  }

  // read file
  IntIntMap parentLabel;
  IntFloatMap labelHeat;
  int line_nb = 1;
  while(ss.status() == QTextStream::Ok) {
    ++line_nb;
    fields = ss.readLine().split(",");
    if(fields.size() != 2)
      break;
    bool ok;
    int label = fields[0].toInt(&ok);
    if(!ok) {
      setErrorMessage(QString("Error line %1: the first column is not an integer number but '%2'")
                      .arg(line_nb)
                      .arg(fields[0].trimmed()));
      return false;
    }
    int parent = fields[1].toInt(&ok);
    if(!ok) {
      setErrorMessage(QString("Error line %1: the second column is not an integer number but '%2'")
                      .arg(line_nb)
                      .arg(fields[1].trimmed()));
      return false;
    }
    parentLabel[label] = parent;
    labelHeat[label] = 1;
    // SETSTATUS("Seed " << parent << " to be mapped to local label " << label);
  }
  // done
  SETSTATUS(parentLabel.size() << " parent labels loaded from file " << filename);

  // clear tables related to parents
  mesh->parentCenter().clear();
  mesh->parentCenterVis().clear();
  mesh->parentNormal().clear();
  mesh->parentNormalVis().clear();

  // store it
  mesh->parentLabelMap() = parentLabel;
  mesh->labelHeat() = labelHeat;
  mesh->heatMapUnit() = "ON/OFF";
  mesh->heatMapBounds() = Point2f(0, 1);
  mesh->updateTriangles();
  return true;
}
REGISTER_MESH_PROCESS(LoadParents);

bool ClearParents::operator()(Mesh* mesh)
{
  mesh->parentLabelMap().clear();
  mesh->parentCenter().clear();
  mesh->parentCenterVis().clear();
  mesh->parentNormal().clear();
  mesh->parentNormalVis().clear();
  mesh->labelHeat().clear();
  mesh->updateTriangles();
  return true;
}
REGISTER_MESH_PROCESS(ClearParents);

// Eliminate from parentLabelMap labels which are not in mesh1 or mesh2
bool CorrectParents::operator()(Mesh* mesh1, Mesh* mesh2)
{
  IntIntMap& daughterParentMap = mesh2->parentLabelMap();
  if(daughterParentMap.size() == 0) {
    setErrorMessage(QString("No parent labels on second mesh"));
    return false;
  }

  vvgraph S1 = mesh1->graph();
  vvgraph S2 = mesh2->graph();

  // Check the parentLabelMap for cells which are not in the mesh1 or mesh2
  IntIntMap daughterParentMaptemp;
  std::set<int> labelsS1;
  std::set<int> labelsS2;
  std::set<int> problemsS1;
  std::set<int> problemsS2;
  forall(vertex c, S1)
    if(c->label > 0)
      labelsS1.insert(c->label);
  forall(vertex c, S2)
    if(c->label > 0)
      labelsS2.insert(c->label);
  for(IntIntMap::iterator it = daughterParentMap.begin(); it != daughterParentMap.end(); it++) {
    int lParent = it->second;
    int lDaughter = it->first;
    if(labelsS1.count(lParent) == 0)
      problemsS1.insert(lParent);
    if(labelsS2.count(lDaughter) == 0)
      problemsS2.insert(lDaughter);
    if(labelsS1.count(lParent) != 0 and labelsS2.count(lDaughter) != 0)
      daughterParentMaptemp[lDaughter] = lParent;
  }
  daughterParentMap = daughterParentMaptemp;

  if(problemsS1.size() > 0 or problemsS2.size() > 0) {
    Information::out << "problems with parentLabelMap. Some labels in the map are not present in the mesh:\n"
                     << "in mesh1, the following labels do not exist:\n";
    forall(int l, problemsS1)
      Information::out << l << ", ";
    Information::out << "\nin mesh2, the following labels do not exist:\n";
    forall(int l, problemsS2)
      Information::out << l << ", ";
    Information::out << endl;
  }
  return true;
}
REGISTER_MESH_PROCESS(CorrectParents);

// Make a heat map based on how many daughter cells a parent cell has
bool HeatMapDaughterCells::operator()(Mesh* mesh)
{
  vvgraph& S = mesh->graph();

  IntIntMap parents = mesh->parentLabelMap();
  IntSet labels;

  // get list of labels
  forall(const vertex& v, S)
    if(v->label > 0)
      labels.insert(v->label);

  IntFloatMap daughters;

  // find how daughters each parent has
  forall(IntIntPair p, parents)
    if(p.first > 0 and p.second > 0)
      daughters[p.second] += 1;

  // Build heat map
  IntFloatMap labelDaughters;
  forall(IntIntPair p, parents)
    if(p.first > 0 and p.second > 0 and daughters[p.second] > 0)
      labelDaughters[p.first] = daughters[p.second];

  // Go over all the volumes to find the min and max
  float dmin = 1, dmax = 1;

  forall(const IntFloatPair& v, labelDaughters) {
    if(v.second < dmin)
      dmin = v.second;
    if(v.second > dmax)
      dmax = v.second;
  }

  if(dmax <= dmin)
    dmax = dmin + 1;

  // Set the map bounds. A heat of 0 will be interpreted has vol_min, and a heat of 1 as vol_max.
  mesh->heatMapBounds() = Point2f(dmin, dmax);
  // Unit of the heat map
  mesh->heatMapUnit() = QString("#");
  // Set the values
  mesh->labelHeat() = labelDaughters;
  // Tell the system the triangles have changed (here, the heat map)
  mesh->updateTriangles();

  return true;
}
REGISTER_MESH_PROCESS(HeatMapDaughterCells);

// Copy parent labels overtop of labels
bool CopyParentsToLabels::operator()(Mesh* mesh)
{
  vvgraph& S = mesh->graph();
  IntIntMap& parents = mesh->parentLabelMap();
  forall(const vertex& v, S)
    if(v->label > 0) {
      if(parents[v->label] > 0)
        v->label = parents[v->label];
      else
        v->label = 0;
    }

  parents.clear();

  mesh->updateAll();
  return true;
}
REGISTER_MESH_PROCESS(CopyParentsToLabels);
} // namespace process
} // namespace lgx
