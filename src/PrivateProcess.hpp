/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PRIVATEPROCESS_HPP
#define PRIVATEPROCESS_HPP

#include <Process.hpp>

#include <CuttingSurface.hpp>
#include <Clip.hpp>
#include <Mesh.hpp>
#include <Stack.hpp>
#include <Store.hpp>

#include <QMutex>
#include <QWaitCondition>
#include <functional>

namespace lgx {
namespace process {

typedef StackRegistration::processFactory stackProcessFactory;
typedef MeshRegistration::processFactory meshProcessFactory;
typedef GlobalRegistration::processFactory globalProcessFactory;

inline QList<stackProcessFactory>& stackProcessFactories() {
  return StackRegistration::processFactories();
}
inline QList<meshProcessFactory>& meshProcessFactories() {
  return MeshRegistration::processFactories();
}
inline QList<globalProcessFactory>& globalProcessFactories() {
  return GlobalRegistration::processFactories();
}

typedef QMap<QString, ProcessDefinition<StackProcess> > stackProcessesMap_t;
typedef QMap<QString, ProcessDefinition<MeshProcess> > meshProcessesMap_t;
typedef QMap<QString, ProcessDefinition<GlobalProcess> > globalProcessesMap_t;

LGX_EXPORT extern stackProcessesMap_t stackProcesses;
LGX_EXPORT extern meshProcessesMap_t meshProcesses;
LGX_EXPORT extern globalProcessesMap_t globalProcesses;

typedef std::pair<MacroRegistrarDefinition::fct_t, MacroRegistrarDefinition::fct_t> macroRegistrar_t;

struct lgx_process_QStringHash
{
  size_t operator()(const QString& s) const
  {
    return qHash(s);
  }
};

typedef std::unordered_map<QString, macroRegistrar_t, lgx_process_QStringHash> macroRegistrarsMap_t;
LGX_EXPORT extern macroRegistrarsMap_t _macroRegistrars;

class PrivateProcess {
public:
  PrivateProcess(QObject* p)
    : current_stack(-1)
    , current_mesh(-1)
    , global_brightness(0.0)
    , global_contrast(1.0)
    , selected_label(0)
    , mesh_selection(false)
    , line_border_selection(false)
    , clip1(0, p)
    , clip2(1, p)
    , clip3(2, p)
    , cuttingSurface()
    , error()
    , warning()
    , parent(p)
    , success(false)
    , changedFolder(false)
  {
  }

  std::vector<Stack*> _stacks;
  std::vector<Mesh*> _meshes;
  int current_stack, current_mesh;
  float global_brightness, global_contrast;
  int selected_label;
  bool mesh_selection, line_border_selection;
  Clip clip1, clip2, clip3;
  CuttingSurface cuttingSurface;
  QString error, warning, filename;
  QString currentPythonCall;
  QString actingFile;
  QObject* parent;

  bool success, changedFolder;
  QMutex lock;
  QWaitCondition updated;
};

class SetupProcess : public GlobalProcess {
public:
  SetupProcess(QObject* parent = 0)
    : Process()
    , GlobalProcess()
  {
    p = new PrivateProcess(parent);
  }

  void clear_private()
  {
    if(p) {
      for(size_t i = 0; i < p->_stacks.size(); ++i) {
        Stack* s = p->_stacks[i];
        if(s)
          delete s;
        p->_stacks[i] = 0;
      }
      for(size_t i = 0; i < p->_meshes.size(); ++i) {
        Mesh* m = p->_meshes[i];
        if(m)
          delete m;
        p->_meshes[i] = 0;
      }
    }
  }

  ~SetupProcess()
  {
    clear_private();
    if(p)
      delete p;
    p = 0;
  }

  bool operator()(const QStringList&) {
    return true;
  }
  QString name() const {
    return "Global";
  }
  QString description() const {
    return "Global Process";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  // QList<float> default_values() const { return QList<float>(); }
  void setFile(const QString& s) {
    p->filename = s;
  }

  void resetModified()
  {
    if(p) {
      p->error = QString();
      p->warning = QString();
      for(size_t i = 0; i < p->_stacks.size(); ++i) {
        Stack* s = p->_stacks[i];
        if(s) {
          s->resetModified();
          if(s->main())
            s->main()->resetModified();
          if(s->work())
            s->work()->resetModified();
        }
      }
      for(size_t i = 0; i < p->_meshes.size(); ++i) {
        Mesh* m = p->_meshes[i];
        if(m)
          m->resetModified();
      }
      p->cuttingSurface.resetModified();
    }
  }
};
} // namespace process
} // namespace lgx

#endif
