/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef SETTINGSDLG_HPP
#define SETTINGSDLG_HPP

#include <LGXConfig.hpp>

#include <QDialog>

#include <ui_SettingsDlg.h>

class LithoViewer;
class LithoGraphX;

namespace lgx {
class ColorBar;
class ScaleBar;
}

class SettingsDlg : public QDialog
{
  Q_OBJECT
public:
  SettingsDlg(LithoViewer* lgx);

  static void loadDefaults(LithoViewer* lgx);
  static void loadDefaults(LithoGraphX* lgx);

protected slots:
  void on_buttonBox_accepted();
  void on_buttonBox_clicked(QAbstractButton* btn);
  void on_generalSetDefaults_clicked();
  void on_renderingSetDefaults_clicked();
  void on_scaleBarSetDefaults_clicked();
  void on_colorBarSetDefaults_clicked();
  void on_scaleBarMinSize_valueChanged(int value);
  void on_scaleBarMaxSize_valueChanged(int value);
  void on_scaleBarChooseFont_clicked();
  void on_colorBarChooseFont_clicked();
  void on_checkVersionInterval_valueChanged(int value);

  void applyGeneral();
  void applyRendering();
  void applyScaleBar();
  void applyColorBar();
  void saveGeneral();
  void saveRendering();
  void saveScaleBar();
  void saveColorBar();

  void applyAll();

protected:
  void loadGeneral();
  void loadRendering();
  void loadScaleBar();
  void loadColorBar();

  LithoViewer *viewer;
  Ui::SettingsDlg ui;

  QFont scaleBarFont, colorBarFont;

signals:
  void update2DDrawing();
  void update3DDrawing();
};

#endif // SETTINGSDLG_HPP
