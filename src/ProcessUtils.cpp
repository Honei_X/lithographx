/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ProcessUtils.hpp"

#include "Information.hpp"

#include <QRegularExpression>
#include <QTreeWidget>
#include <QWidget>
#include <QString>

namespace process_util {
QTreeWidgetItem* getFolder(QString name, QHash<QString, QTreeWidgetItem*>& folders, QTreeWidget* tree)
{
  if(folders.contains(name))
    return folders[name];
  int idx = name.lastIndexOf('/');
  QTreeWidgetItem* item = 0;
  if(idx == -1) {
    item = new QTreeWidgetItem(QStringList() << name);
    item->setExpanded(false);
    item->setFlags(Qt::ItemIsEnabled);
    tree->addTopLevelItem(item);
  } else {
    QString newname = name.left(idx);
    QTreeWidgetItem* parent = getFolder(newname, folders, tree);
    QString fn = name.mid(idx + 1);
    item = new QTreeWidgetItem(QStringList() << fn);
    item->setExpanded(true);
    item->setFlags(Qt::ItemIsEnabled);
    parent->addChild(item);
  }
  folders[name] = item;
  return item;
}

// Returns true if the item is visible
bool filterItem(QTreeWidgetItem* item, const QRegularExpression& filter)
{
  if(item->flags() & Qt::ItemNeverHasChildren) {
    bool selected = item->text(0).contains(filter);
    item->setHidden(not selected);
    return selected;
  } else {
    bool any_selected = false;
    for(int id = 0 ; id < item->childCount() ; ++id) {
      auto child = item->child(id);
      any_selected |= filterItem(child, filter);
    }
    item->setHidden(not any_selected);
    return any_selected;
  }
}

void filterProcesses(QTreeWidget* tree, const QString& filter_text)
{
  QRegularExpression filter(filter_text, QRegularExpression::CaseInsensitiveOption);
  if(not filter.isValid() or filter_text.isEmpty())
    filter.setPattern(".");
  for(int top = 0 ; top < tree->topLevelItemCount() ; ++top) {
    auto topItem = tree->topLevelItem(top);
    filterItem(topItem, filter);
  }
}

}
