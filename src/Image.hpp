/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <LGXConfig.hpp>

#include <cuda/CudaExport.hpp>
#include <Vector.hpp>

#include <array>
#include <QString>
#include <QStringList>

namespace lgx {

class Progress;

typedef util::Vector<3, unsigned int> Point3u;

LGX_EXPORT QStringList supportedImageReadFormats();
LGX_EXPORT QStringList supportedImageWriteFormats();

struct LGX_EXPORT ImageInfo {
  enum Axis {
    X = 0,
    Y,
    Z,
    C,
    T
  };
  explicit ImageInfo(const QString& fn)
    : filename(fn)
  { }
  ImageInfo(const QString& fn,
            const Point3u& si,
            const Point3f& st)
    : filename(fn)
    , size(si)
    , step(st)
  { }
  ImageInfo(const ImageInfo&) = default;
  ImageInfo& operator=(const ImageInfo&) = default;
#ifndef _MSC_VER
  ImageInfo() { }
  ImageInfo(ImageInfo&&) = default;
  ImageInfo& operator=(ImageInfo&&) = default;
#else
  ImageInfo()
	  : axisOrder({{ X, Y, C, Z, T }})
  { }
#endif

  virtual ~ImageInfo() { }

  QString filename;
  Point3u size;
  Point3f step;
  Point3f origin;
  size_t nb_channels = 1;   ///< Number of channels in the image
  size_t nb_timepoints = 1; ///< Number of time points in the image
  int plane = -1;           ///< To load a 2D image in a 3D stack, -1 to load the whole stack
  bool labels = false;      ///< If the image contains labels
#ifndef _MSC_VER
  std::array<Axis,5> axisOrder = {{X, Y, C, Z, T}}; ///< Vector defining the order in which the dimensions are encoded
#else
  std::array<Axis, 5> axisOrder;
#endif
};

struct LGX_EXPORT Image3D : public ImageInfo {
  Image3D();
  Image3D(const Image3D&) = default;
  explicit Image3D(const QString& filename);
  Image3D(const QString& filename, HVecUS& data, const Point3u& size, const Point3f& step = Point3f(1, 1, 1));

#ifndef _MSC_VER
  Image3D(Image3D&&) = default;
  Image3D& operator=(Image3D&&) = default;
#endif

  virtual ~Image3D();

  void allocate(const Point3u& size);
  void allocate();

  void setPlane(int i)
  {
    if(i >= 0 and size_t(i) < info.size.z())
      plane = i;
    else
      plane = -1;
  }

  const ushort& operator[](int i) const {
    return (*data)[i];
  }

  ushort& operator[](int i) {
    return (*data)[i];
  }

  HVecUS* data = nullptr;
  ImageInfo info;
  uint minc = 0, maxc = 0;
  float brightness = 1.0f;
  uint8_t compression_level = 9;

protected:
  bool allocated = false;
};

// In case of an error, this function will throw a QString with the description of the error
LGX_EXPORT bool saveImage(QString filename, const Image3D& data, QString type = "CImg Auto",
                          unsigned int nb_digits = 0);

// Save a TIFF image as a single file
LGX_EXPORT bool saveTIFFImage(QString filename, const Image3D& data);

/// Load a sample of each timepoint / channel
LGX_EXPORT bool loadTIFFSamples(QString filename, Image3D& data, Progress* progress = 0);

LGX_EXPORT bool loadTIFFImage(QString filename, Image3D& data,
                              size_t channel = 0, size_t timepoint = 0,
                              bool allocate_data = false,
                              Progress* progress = 0);
LGX_EXPORT bool loadImage(QString filename, Image3D& data, bool allocate_data = false);

LGX_EXPORT HVecUS resize(const HVecUS& data, const Point3u& before, const Point3u& after, bool center);

LGX_EXPORT ImageInfo getTIFFInfo(QString filename);
} // namespace lgx

#endif // IMAGE_HPP
