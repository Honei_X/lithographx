uniform bool blending;

uniform bool use_colormap;

varying vec3 texCoord;
/*
 *varying vec3 normal;
 *varying vec3 lightDir[4], halfVector[4];
 */

void setColor()
{
  if((texCoord.x < 0.0) || (texCoord.x > 1.0) ||
     (texCoord.y < 0.0) || (texCoord.y > 1.0) ||
     (texCoord.z < 0.0) || (texCoord.z > 1.0))
  {
    discard;
    return;
  }
  vec4 col = color(texCoord);

/*
 *  vec4 light = vec4(0,0,0,0);
 *  vec3 n = normalize(normal);
 *
 *  for(int i = 0 ; i < 4 ; ++i)
 *  {
 *    vec4 diffuseLight, specularLight;	
 *    float shine = 8.0;//32.0;
 *
 *    float lightIntensity = max(dot(n, normalize(lightDir[i])), 0.0);
 *
 *    diffuseLight = lightIntensity * gl_LightSource[i].diffuse;
 *    specularLight = pow(max(dot(n, normalize(halfVector[i])), 0.0), shine) * gl_LightSource[i].specular;
 *
 *    light += (diffuseLight + specularLight + gl_LightSource[i].ambient);
 *  }
 */

  col.a = 1-col.a;
  col = light(col);
  gl_FragColor.rgb = col.rgb;//light.rgb*(col.rgb/col.a);
  if(blending)
    gl_FragColor.a = opacity*col.a;
  else
    gl_FragColor.a = 1.0;

}

