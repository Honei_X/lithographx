/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef THRUST_HPP
#define THRUST_HPP

#include <LGXConfig.hpp>
#include <thrust/version.h>

#cmakedefine USE_OPENMP

#if THRUST_VERSION >= 100600
#    if defined(USE_TBB)
#        define USING_TBB
#        include <thrust/system/tbb/vector.h>
#        include <thrust/iterator/retag.h>
#        define THRUST_RETAG(x) thrust::reinterpret_tag<thrust::tbb::tag>(x)
#        define THRUST_TAG "Threading Building Blocks"
#    elif defined(USE_OPENMP)
#        define USING_OPENMP
#        include <thrust/system/omp/vector.h>
#        include <thrust/iterator/retag.h>
#        define THRUST_RETAG(x) thrust::reinterpret_tag<thrust::omp::tag>(x)
#        define THRUST_TAG "OpenMP"
#    else
#        include <thrust/system/cpp/vector.h>
#        include <thrust/iterator/retag.h>
#        define THRUST_RETAG(x) thrust::reinterpret_tag<thrust::cpp::tag>(x)
#        define THRUST_TAG "Plain C++"
#    endif
#else
#    define THRUST_RETAG(x) x
#    define THRUST_TAG "Plain C++"
#endif

#endif // THRUST_HPP

