uniform sampler2D backDepth;
uniform sampler2D frontDepth;
uniform bool peeling;

varying vec4 realPos;
varying float depthProj;

#define EPSILON 1e-7

void main()
{
  if(peeling)
  {
    vec3 screenPos = (realPos.xyz / realPos.w + 1.0) / 2.0;
    if(float(texture2D(backDepth, screenPos.xy).a-EPSILON) <= float(screenPos.z)) // Small delta to avoid calculation errors
      discard;
    else if(float(texture2D(frontDepth, screenPos.xy).a+EPSILON) >= float(screenPos.z)) // Small delta to avoid calculation errors
      discard;
    else
    {
      setColor();
      if(gl_FragColor.a == 0) discard;
      //else gl_FragDepth = gl_FragCoord.z/gl_FragCoord.w;
    }
  }
  else
  {
    setColor();
    if(gl_FragColor.a == 0) discard;
    //else gl_FragDepth = (depthProj + 1.0)/2.0;//gl_FragCoord.z/gl_FragCoord.w;
  }
}

