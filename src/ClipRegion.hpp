/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef CLIP_REGION_H
#define CLIP_REGION_H

/**
 * \file ClipRegion.hpp
 * This file contains the definition of a clipping region
 */

#include <LGXConfig.hpp>
#include <GL.hpp>

#include <Clip.hpp>
#include <Colors.hpp>
#include <Geometry.hpp>
#include <LGXViewer/qglviewer.h>
#include <Parms.hpp>

#include <iostream>

namespace lgx {
typedef util::Color<float> Color4f;

/**
 * \class ClipRegion ClipRegion.hpp <ClipRegion.hpp>
 *
 * Class to handle rotatable pair of clipping planes.
 */
class LGX_EXPORT ClipRegion {
public:
  ClipRegion();
  ~ClipRegion(){
  };

  Clip* clip;

  void setClip(Clip* c);

  /**
   * OpenGL identifier for the first clipping plane
   */
  GLenum clip0;
  /**
   * OpenGL identifier for the second clipping plane
   */
  GLenum clip1;

  Colors::ColorType GridColor;   ///< Color used to draw the grid

  /// Read clipping plane parameters
  void readParms(util::Parms& parms, QString section);

  /// write parms to file
  void writeParms(QTextStream& pout, QString section);

  /// Draw (use) clipping place (ax + by + cz + d = 0)
  void disable()
  {
    glDisable(clip0);
    glDisable(clip1);
  }

  /// Draw (use) clipping place (ax + by + cz + d = 0)
  void drawClip();

  /// Draw (use) clipping place (ax + by + cz + d = 0)
  void drawGrid(float width);
};
} // namespace lgx
#endif /*CLIP_REGION_H*/
