#include "QPower2SpinBox.h"

#include <QString>

QPower2SpinBox::QPower2SpinBox(QWidget* parent)
  : QSpinBox(parent)
{
}

void QPower2SpinBox::stepBy(int i)
{
  auto val = value();
  if(i > 0)
    val <<= i;
  else
    val >>= -i;
  setValue(val);
}

void QPower2SpinBox::fixup(QString& input) const
{
  bool ok;
  auto val = input.toUInt(&ok);
  if(not ok)
    val = value();
  else {
    size_t i = 0;
    bool cur = false, prev = false;
    while(val > 0) {
      ++i;
      prev = cur;
      cur = val & 0x1;
      val >>= 1;
    }
    if(prev)
      val = 1 << i;
    else
      val = 1 << (i-1);
  }
  input = QString::number(val);
}

QValidator::State QPower2SpinBox::validate(QString& input, int& pos) const
{
  bool ok;
  auto val = input.toUInt(&ok);
  if(not ok)
    return QValidator::Invalid;
  while(val > 0) {
    bool cur = val & 0x1;
    val >>= 1;
    if(val > 0 and cur)
      return QValidator::Intermediate;
  }
  return QValidator::Acceptable;
}
