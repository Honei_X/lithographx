/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PROCESSDOCDIALOG_HPP
#define PROCESSDOCDIALOG_HPP

#include <LGXConfig.hpp>

#include <QDialog>

#include <memory>

class QTreeWidget;
class QTreeWidgetItem;

namespace Ui {
class ProcessDocDialog;
} // namespace Ui

namespace lgx {
namespace process {
struct BaseProcessDefinition;
} // namespace process
} // namespace lgx

class ProcessDocDialog : public QDialog
{
  Q_OBJECT
public:
  ProcessDocDialog(QWidget *parent);
  virtual ~ProcessDocDialog();

protected slots:
  void on_StackTreeWidget_currentItemChanged(QTreeWidgetItem* item, QTreeWidgetItem* previous);
  void on_MeshTreeWidget_currentItemChanged(QTreeWidgetItem* item, QTreeWidgetItem* previous);
  void on_GlobalTreeWidget_currentItemChanged(QTreeWidgetItem* item, QTreeWidgetItem* previous);

  void on_StackFilter_textChanged(const QString& text);
  void on_MeshFilter_textChanged(const QString& text);
  void on_GlobalFilter_textChanged(const QString& text);

private:
  void updateDocView(const QString& type, lgx::process::BaseProcessDefinition* def);
  void findProcesses(const QString& type, QTreeWidget* tree);

  std::unique_ptr<Ui::ProcessDocDialog> ui;
};

#endif // PROCESSDOCDIALOG_HPP

