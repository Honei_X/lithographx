/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "SystemProcessLoad.hpp"

#include "Dir.hpp"
#include "Forall.hpp"
#include "Image.hpp"
#include "Information.hpp"
#include "LGXViewer/qglviewer.h"
#include "Misc.hpp"
#include "Parms.hpp"
#include "PlyFile.hpp"
#include "Progress.hpp"
#include "Shapes.hpp"
#include "Thrust.hpp"
#include "UnorderedMap.hpp"

#include <CImg.h>
#include <QCheckBox>
#include <QDialog>
#include <QDoubleSpinBox>
#include <QInputDialog>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QHash>
#include <QListWidget>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QRegExp>

#include "ui_ImportMesh.h"
#include "ui_LoadMesh.h"
#include "ui_LoadStack.h"
#include "ui_TIFFOpen.h"

#ifdef WIN32
#  define FileDialogOptions QFileDialog::DontUseNativeDialog
#else
#  define FileDialogOptions 0
#endif

using namespace qglviewer;
using namespace cimg_library;

#include <QTextStream>
#include <stdio.h>

static QTextStream err(stderr);

namespace lgx {
namespace process {

typedef std::unordered_map<int, vertex> vertex_map;

typedef CImg<ushort> CImgUS;

bool StackSwapBytes::operator()(const Store* input, Store* output)
{
  const HVecUS& idata = input->data();
  HVecUS& odata = output->data();
#pragma omp parallel for
  for(size_t i = 0; i < idata.size(); ++i) {
    ushort us = idata[i];
    odata[i] = ((us & 0x00ff) << 8) + ((us & 0xff00) >> 8);
  }

  output->copyMetaData(input);
  output->changed();
  return true;
}

struct StackHdr {
  uint hdrsz;
  uint xsz, ysz, zsz;
  float xum, yum, zum;
  uint datasz;

  StackHdr()
    : hdrsz(1024)
  {
  }
};

StackImportDlg::StackImportDlg(QWidget* p, Qt::WindowFlags f)
  : QDialog(p, f)
  , ui(new Ui_LoadStackDialog)
{
  ui->setupUi(this);
}

StackImportDlg::~StackImportDlg()
{ }

void StackImportDlg::setBrightness(float val)
{
  ui->Brightness->setValue(val);
}

float StackImportDlg::brightness() const
{
  return ui->Brightness->value();
}

void StackImportDlg::clearImageFiles()
{
  ui->ImageFiles->clear();
}

QStringList StackImportDlg::imageFiles() const
{
  QStringList result;
  for(int i = 0; i < ui->ImageFiles->count(); ++i)
    result << ui->ImageFiles->item(i)->text();
  return result;
}

void StackImportDlg::on_LoadProfile_clicked()
{
  LoadProfile();
}

void StackImportDlg::LoadProfile(QString filename)
{
  if(filename.isEmpty()) {
    filename = QFileDialog::getOpenFileName(this, QString("Select profile file to open"), filename,
                                            QString("Text files (*.txt)"), 0, FileDialogOptions);
    if(filename.isEmpty())
      return;
  }

  QStringList imageFiles;
  Point3u size;
  Point3f step;
  float brightness;
  if(StackImport::LoadProfile(filename, size, step, brightness, imageFiles)) {
    setImageSize(size.x(), size.y(), size.z());
    ui->StepX->setValue(step.x());
    ui->StepY->setValue(step.y());
    ui->StepZ->setValue(step.z());
    ui->Brightness->setValue(brightness);
    QListWidget* lst = ui->ImageFiles;
    lst->clear();
    forall(const QString& s, imageFiles)
      lst->addItem(s);

    _loadedFile = filename;
  }
}

void StackImportDlg::on_SaveProfile_clicked()
{
  // Write to file
  QString filename = QFileDialog::getSaveFileName(this, QString("Select file to save profile for "), _loadedFile,
                                                  QString("Text files (*.txt)"), 0, FileDialogOptions);
  if(filename.isEmpty())
    return;

  // Check ending, add suffix if required
  if(filename.right(4) != ".txt")
    filename.append(".txt");

  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
    SETSTATUS("loadStackSaveProfile::Error:Cannot open output file:" << filename);
    return;
  }
  QString StackFile = util::stripCurrentDir(filename);
  //actingFile(filename);

  QTextStream out(&file);
  out << "[Stack]" << endl;
  out << "SizeX: " << imageSize.x() << endl;
  out << "SizeY: " << imageSize.y() << endl;
  out << "StepX: " << ui->StepX->value() << endl;
  out << "StepY: " << ui->StepY->value() << endl;
  out << "StepZ: " << ui->StepZ->value() << endl;
  out << "Brightness: " << ui->Brightness->value() << endl;
  QListWidget* lst = ui->ImageFiles;
  for(int i = 0; i < lst->count(); i++) {
    QListWidgetItem* item = lst->item(i);
    out << "ImageFile: " << util::stripCurrentDir(item->text()) << endl;
  }
  file.close();
  _loadedFile = filename;
  SETSTATUS("Saving profile, file:" << filename);
}

void StackImportDlg::on_AddFiles_clicked()
{
  // RSS How to get a complete list from CImg? (there are over 100)
  QStringList files = QFileDialog::getOpenFileNames(this, QString("Select stack image files "), util::currentPath(),
                                                    QString("Any files (*.*)"), 0);
  if(files.empty())
    return;

  on_ImageFiles_filesDropped(files);
}

void StackImportDlg::on_ImageFiles_filesDropped(const QStringList& files)
{
  QListWidget* lst = ui->ImageFiles;
  if(lst->count() == 0) {
    QString first = files[0];
    Image3D image;
    image.step = imageResolution();
    if(!loadImage(first, image)) {
      QMessageBox::critical(this, "Error reading image", QString("Error, cannot read image from '%1'").arg(first));
      return;
    }
    SETSTATUS(QString("Loaded first image: size = %1x%2").arg(image.size.x()).arg(image.size.y()));
    // CImgUS image(qPrintable(first));
    setImageSize(image.size.x(), image.size.y(), 0);
    setImageResolution(image.step);
  }
  lst->addItems(files);
  setImageSize(imageSize.x(), imageSize.y(), lst->count());
}

Point3f StackImportDlg::imageResolution() {
  return Point3f(ui->StepX->value(), ui->StepY->value(), ui->StepZ->value());
}

void StackImportDlg::setImageResolution(Point3f step)
{
  ui->StepX->setValue(step.x());
  ui->StepY->setValue(step.y());
  ui->StepZ->setValue(step.z());
}

void StackImportDlg::setImageSize(Point3u size)
{
  imageSize = size;
  ui->SizeXY->setText(QString("(%1,%2,%3)").arg(imageSize.x()).arg(imageSize.y()).arg(imageSize.z()));
}

void StackImportDlg::on_RemoveFiles_clicked()
{
  QListWidget* lst = ui->ImageFiles;
  QList<QListWidgetItem*> items = lst->selectedItems();
  for(QList<QListWidgetItem*>::iterator i = items.begin(); i != items.end(); i++) {
    QListWidgetItem* item = lst->takeItem(lst->row(*i));
    delete item;
  }
  setImageSize(imageSize.x(), imageSize.y(), lst->count());
}

void StackImportDlg::on_FilterFiles_clicked()
{
  QListWidget* lst = ui->ImageFiles;
  QString text = ui->FilterText->text();

  QList<QListWidgetItem*> items = lst->findItems(".*", Qt::MatchRegExp);
  for(QList<QListWidgetItem*>::iterator i = items.begin(); i != items.end(); i++) {
    QString itemtext = (*i)->text();
    if(itemtext.indexOf(text) < 0) {
      QListWidgetItem* item = lst->takeItem(lst->row(*i));
      delete item;
    }
  }
}

void StackImportDlg::on_SortAscending_clicked()
{
  QListWidget* lst = ui->ImageFiles;
  lst->sortItems(Qt::AscendingOrder);
}

void StackImportDlg::on_SortDescending_clicked()
{
  QListWidget* lst = ui->ImageFiles;
  lst->sortItems(Qt::DescendingOrder);
}

bool StackImport::LoadProfile(QString filename, Point3u& size, Point3f& step, float& brightness, QStringList& files)
{
  if(QFile::exists(filename)) {
    util::Parms parms(filename);

    bool ok = true;
    ok &= parms("Stack", "SizeX", size.x());
    ok &= parms("Stack", "SizeY", size.y());
    // New version: StepX and StepY and separate
    if(parms("Stack", "StepX", step.x())) {
      parms("Stack", "StepY", step.y());
      parms("Stack", "StepZ", step.z());
    } else {
      // This is for backward compatibility
      if(!parms("Stack", "StepXY", step.x()) or !parms("Stack", "StepZ", step.z())) {
        float zScale;
        if(parms("Stack", "ZScale", zScale)) {
          step.z() = .5;
          step.x() = step.z() / zScale;
        } else
          ok = false;
      }
      step.y() = step.x();
    }
    ok &= parms("Stack", "Brightness", brightness);
    if(!ok) {
      SETSTATUS("loadStack::Error:Cannot read parameter file:" << filename);
      return false;
    }
    parms.all("Stack", "ImageFile", files);
    size.z() = files.size();
    return true;
  }
  files = filename.split(";;");
  if(files.size() > 1) {
    CImgUS first_image(qPrintable(files.at(0)));
    size = Point3u(first_image.width(), first_image.height(), files.size());
    return true;
  }
  SETSTATUS("loadStack::Error:Cannot interpret filename: '" << filename << "'");
  files.clear();
  return false;
}


bool StackImport::initialize(QStringList& parms, QWidget* parent)
{
  Point3f step(parms[2].toFloat(), parms[3].toFloat(), parms[4].toFloat());
  float brightness(parms[5].toFloat());
  QString filename(parms[6]);

  // Get stack and store
  int stackId = -1;
  Stack* stk = currentStack();
  if(not parms[0].isEmpty()) {
    bool ok;
    stackId = parms[0].toInt(&ok);
    if(not ok)
      return setErrorMessage("Stack parameter must be a valid integer or empty.");
    stk = stack(parms[0].toInt());
  }
  if(not stk)
    return setErrorMessage("Invalid stack selected");
  if(!checkState().stack(STACK_ANY, stackId))
    return false;

  QString& storeName = parms[1];
  Store* store = stk->currentStore();
  if(storeName == "Main")
    store = stk->main();
  else if(storeName == "Work")
    store = stk->work();
  else {
    storeName = "";
    if(not store)
      store = stk->main();
  }

  // Set up dialog
  StackImportDlg dlg(parent);

  bool res = false;
  dlg.setImageSize(512, 512, 0);
  dlg.setImageResolution(step);
  dlg.setBrightness(brightness);
  dlg.clearImageFiles();

  QString currentFile(store->file());

  if(not filename.isEmpty()) {
    // If file list contains one
    dlg.LoadProfile(filename);
  } else if(not currentFile.isEmpty()) {
    // If current stack file is a .txt file, load it as a profile
    dlg.LoadProfile(currentFile);
  }

  if(dlg.exec() == QDialog::Accepted) {
    // Change parms
    Point3f step = dlg.imageResolution();
    parms[2] = QString::number(step.x());
    parms[3] = QString::number(step.y());
    parms[4] = QString::number(step.z());
    parms[5] = QString::number(dlg.brightness());
    filename = dlg.loadedFile();
    if(filename.isEmpty()) {
      filename = dlg.imageFiles().join(";;");
    }
    parms[6] = filename;

    res = true;
  }

  return res;
}

bool StackImport::operator()(Stack* stack, Store* store, Point3f step, float brightness, QString filename)
{
  Point3u size;
  QString txtFile;
  QStringList imageFiles;

  if(filename.size() > 0) {
    if(not LoadProfile(filename, size, step, brightness, imageFiles))
      return setErrorMessage(QString("Couldn't read profile file '%1'. See terminal for details.").arg(filename));
    if(filename.endsWith(".txt"))
      actingFile(filename);
    else
      actingFile(imageFiles[0]);
  } else
    return setErrorMessage("You need to specify a valid filename.");

  HVecUS& data = store->data();
  stack->setSize(size);
  stack->setStep(step);
  size_t SizeXYZ = size_t(size.x()) * size.y() * size.z();
  store->setFile(filename);
  uint filecount = 0;
  SETSTATUS("Loading " << ((store == stack->main()) ? "main" : "work") << " stack " << stack->userId());
  Progress progress(QString("Loading %1 Stack %2").arg((store == stack->main()) ? "main" : "work").arg(stack->userId()),
                    size.z());
  memset(data.data(), 0, SizeXYZ * sizeof(ushort));

  Image3D image(filename, data, size, step);
  image.brightness = brightness;
  image.minc = 65535;
  image.maxc = 0;
  for(uint z = 0; z < size.z(); z++) {
    image.plane = filecount++;
    if(!loadImage(imageFiles.at(z), image)) {
      setErrorMessage(QString("Cannot read image from file '%1'").arg(imageFiles.at(z)));
      return false;
    }
    SETSTATUS("Read File:" << imageFiles.at(z));
    if(!progress.advance(z + 1))
      userCancel();
  }
  if(image.maxc > 0xFFFF)
    SETSTATUS("WARNING: max intensity:" << image.maxc << ", clipped at:" << 0xFFFF);
  else
    SETSTATUS(filecount << " of " << size.z() << " files,read intensity range:" << image.minc << "-" << image.maxc);
  {
    // Autoscale the brightness
    const auto& data = store->data();
    ushort low = 65525u, high = 0u;
#pragma omp parallel for reduction(max : high) reduction(min : low)
    for(size_t i = 0; i < data.size(); ++i) {
      if(low > data[i])
        low = data[i];
      if(high < data[i])
        high = data[i];
    }
    if(low < high) {
      auto fct = store->transferFct();
      fct.adjust(low, high);
      store->setTransferFct(fct);
    }
  }
  stack->center();
  store->changed();
  return true;
}

// REGISTER_STACK_PROCESS(StackImport);

TIFFOpenDlg::TIFFOpenDlg(const ImageInfo& info, QWidget* p, Qt::WindowFlags f)
  : QDialog(p, f)
  , ui(new Ui_TIFFOpenDlg)
{
  ui->setupUi(this);

  nb_channels = info.nb_channels;
  nb_timepoints = info.nb_timepoints;

  Image3D img;
  if(not loadTIFFSamples(info.filename, img)) {
    nb_channels = 0;
    return;
  }

  auto nb_pixels = img.size.x()*img.size.y();
  std::vector<uint8_t> sample_data(3*nb_pixels, 0xff);
  for(size_t i = 0, k = 0 ; i < nb_timepoints ; ++i)
    for(size_t j = 0 ; j < nb_channels ; ++j, ++k) {
      size_t shift = k*nb_pixels;
      ushort low = 65535, high = 0;
#pragma omp parallel for reduction(min: low) reduction(max: high)
      for(size_t d = 0 ; d < nb_pixels ; ++d) {
        ushort v = img[shift+d];
        if(v < low) low = v;
        if(v > high) high = v;
      }
#pragma omp parallel for
      for(size_t d = 0 ; d < nb_pixels ; ++d) {
        uchar val = uchar(255*double(img[shift+d] - low) / (high - low));
        sample_data[3*d  ] = val;
        sample_data[3*d+1] = val;
        sample_data[3*d+2] = val;
      }
      QImage sample_image(sample_data.data(), img.size.x(), img.size.y(), QImage::Format_RGB888);
      QPixmap sample(256, 256);
      {
        QPainter paint(&sample);
        paint.setRenderHint(QPainter::SmoothPixmapTransform);
        paint.drawImage(QRect(0, 0, 256, 256), sample_image, sample_image.rect());
      }
      samples.push_back(sample);
    }

  Information::out << "# samples: " << samples.size() << endl;

  ui->sample->setPixmap(samples[0]);
  ui->channel->setMaximum(nb_channels);
  ui->timePoint->setMaximum(nb_timepoints);
  ui->stackSize->setText(QString("(%1,%2,%3)").arg(img.size.x()).arg(img.size.y()).arg(img.size.z()));
  ui->stackStep->setText(QString("%1%4 x %2%4 x %3%4").arg(img.step.x()).arg(img.step.y()).arg(img.step.z()).arg(UM));
  ui->nbChannels->setText(QString::number(nb_channels));
  ui->nbTimePoints->setText(QString::number(nb_timepoints));
}

TIFFOpenDlg::~TIFFOpenDlg()
{ }

int TIFFOpenDlg::channel() const
{
  return ui->channel->value()-1;
}

int TIFFOpenDlg::timepoint() const
{
  return ui->timePoint->value()-1;
}

void TIFFOpenDlg::setChannel(int value)
{
  ui->channel->setValue(value);
}

void TIFFOpenDlg::setTimepoint(int value)
{
  ui->timePoint->setValue(value);
}

void TIFFOpenDlg::on_channel_valueChanged(int value)
{
  auto pos = value-1 + timepoint()*nb_channels;
  Information::out << "Changed channel to " << value << " showing image " << pos << endl;
  ui->sample->setPixmap(samples[pos]);
}

void TIFFOpenDlg::on_timePoint_valueChanged(int value)
{
  auto pos = channel() + (value-1)*nb_channels;
  Information::out << "Changed timePoint to " << value << " showing image " << pos << endl;
  ui->sample->setPixmap(samples[pos]);
}

bool StackOpen::parseFilename(QString& filename, size_t& channel, size_t& timepoint)
{
  if(not filename.contains('?')) {
    channel = 0;
    timepoint = 0;
    return true;
  }
  auto idx = filename.indexOf('?');
  QString params = filename.mid(idx+1);
  filename = filename.left(idx);
  QStringList fields = params.split('&', QString::SkipEmptyParts);
  for(const auto& field: fields) {
    auto idx_eq = field.indexOf('=');
    if(idx_eq >= 0) {
      QString name = field.left(idx_eq);
      QString value = field.mid(idx_eq+1);
      if(name == "C") {
        bool ok;
        channel = value.toUInt(&ok);
        if(not ok)
          return setErrorMessage("Channel ('C') field in filename must be an unsigned integer");
      } else if(name == "T") {
        bool ok;
        timepoint = value.toUInt(&ok);
        if(not ok)
          return setErrorMessage("Time Point ('T') field in filename must be an unsigned integer");
      } else {
        return setErrorMessage(QString("Unknown field in filename: '%1'").arg(name));
      }
    }
  }
  return true;
}

bool StackOpen::initialize(QStringList& parms, QWidget* parent)
{
  STORE which_store = stringToStore(parms[1]);
  int stackId = -1;
  if(not parms[2].isEmpty())
  {
    bool ok;
    stackId = parms[2].toInt(&ok);
    if(not ok)
      return setErrorMessage("Stack parameter must be a valid integer or empty");
  }
  if(!checkState().stack(STACK_ANY, stackId))
    return false;
  bool choose_file = stringToBool(parms[3]);
  parms[3] = "Yes";
  QString allFilters("All Stack files (*.mgxs *.inr *.tif *.tiff)");
  QString mgxsFilter("LithoGraphX Stack files (*.mgxs)"), inrFilter("Inria Stack files (*.inr)"),
  tifFilter("TIFF Image Directory (*.tif *.tiff)"), filter;
  QString filename = parms[0];

  size_t channel = 0;
  size_t timepoint = 0;
  if(not parseFilename(filename, channel, timepoint))
    return false;

  if(filename.isEmpty() or choose_file) {
    auto store = stack(stackId)->store(which_store);
    QString currentFile;
    if(store)
      currentFile = store->file();
    if(!currentFile.isEmpty())
      filename = currentFile;

    // If we loaded a list of images before, strip of the ending and default type
    if(filename.right(4) == ".txt") {
      filename = filename.left(filename.length() - 4);
      filename += ".mgxs";
    }

    if(filename.right(4) == ".inr")
      filter = inrFilter;
    else if(filename.endsWith(".tif", Qt::CaseInsensitive) or filename.endsWith(".tiff", Qt::CaseInsensitive))
      filter = tifFilter;
    else if(filename.endsWith(".lgx", Qt::CaseInsensitive))
      filter = mgxsFilter;
    else
      filter = allFilters;

    // Get the file name
    filename = QFileDialog::getOpenFileName(parent, QString("Select stack file"), filename,
                                            QString(allFilters + ";;" + mgxsFilter + ";;" + inrFilter + ";;" + tifFilter),
                                            &filter, FileDialogOptions);
    if(filename.isEmpty())
      return false;
  }

  parms[0] = filename;

  if(filename.endsWith(".tif", Qt::CaseInsensitive) or filename.endsWith(".tiff", Qt::CaseInsensitive)) {
    ImageInfo info = getTIFFInfo(filename);
    if(info.nb_timepoints > 1 or info.nb_channels > 1) {
      TIFFOpenDlg dlg(info, parent);

      dlg.setTimepoint(timepoint);
      dlg.setChannel(channel);

      if(not dlg.valid())
        return setErrorMessage("Couldn't load TIFF samples");
      if(dlg.exec() == QDialog::Accepted) {
        filename += QString("?C=%1&T=%2").arg(dlg.channel()).arg(dlg.timepoint());
        parms[0] = filename;
        Information::out << "channel: " << dlg.channel() << endl;
        Information::out << "time point: " << dlg.timepoint() << endl;
        Information::out << "Full filename: " << filename << endl;
        return true;
      }
      return false;
    }
  }

  return true;
}

bool StackOpen::operator()(const QStringList& parms)
{
  int stackId = parms[2].toInt();
  if(!checkState().stack(STACK_ANY, stackId))
    return false;
  Stack* stack = this->stack(stackId);
  Store* store = stack->store(stringToStore(parms[1]));
  if(not store)
    store = stack->main();
  QString filename = parms[0];
  bool res = (*this)(stack, store, filename);
  if(res) {
    store->show();
  }
  return res;
}

bool StackOpen::loadMGXS_0(QIODevice& file, Stack* stack, Store* store)
{
  HVecUS& data = store->data();
  QByteArray stkdata = file.readAll();
  file.close();
  stkdata = qUncompress(stkdata);

  struct StackHdr stkhdr;
  memcpy(&stkhdr, stkdata.constData(), sizeof(stkhdr));
  Point3u size(stkhdr.xsz, stkhdr.ysz, stkhdr.zsz);
  Point3f step(stkhdr.xum, stkhdr.yum, stkhdr.zum);
  stack->setSize(size);
  stack->setStep(step);
  if(stkhdr.datasz / 2 != stack->storeSize())
    throw QString("Datasize does not match dimensions in .mgxs file");
  memcpy(data.data(), stkdata.constData() + stkhdr.hdrsz, stkhdr.datasz);
  return true;
}

bool StackOpen::loadMGXS_1_3(QIODevice& file, Stack* stack, Store* store)
{
  bool isLabel;
  float sxum, syum, szum;
  file.read((char*)&isLabel, sizeof(bool));
  file.read((char*)&sxum, sizeof(float));
  file.read((char*)&syum, sizeof(float));
  file.read((char*)&szum, sizeof(float));
  bool result = loadMGXS_1_0(file, stack, store);
  if(result) {
    store->setLabels(isLabel);
    stack->setOrigin(Point3f(sxum, syum, szum));
    SETSTATUS("Set stack step to " << stack->origin());
  }
  return result;
}

bool StackOpen::loadMGXS_1_2(QIODevice& file, Stack* stack, Store* store)
{
  bool result = loadMGXS_1_3(file, stack, store);
  if(result and store->labels()) {
    StackSwapBytes swap(*this);
    return swap(store, store);
  }
  return result;
}

bool StackOpen::loadMGXS_1_1(QIODevice& file, Stack* stack, Store* store)
{
  bool isLabel;
  file.read((char*)&isLabel, sizeof(bool));
  bool result = loadMGXS_1_0(file, stack, store);
  if(result) {
    store->setLabels(isLabel);
    if(store->labels()) {
      StackSwapBytes swap(*this);
      return swap(store, store);
    }
  }
  return result;
}

bool StackOpen::loadMGXS_1_0(QIODevice& file, Stack* stack, Store* store)
{
  HVecUS& data = store->data();
  quint32 xsz;
  quint32 ysz;
  quint32 zsz;
  float xum;
  float yum;
  float zum;
  quint64 datasz;
  quint8 cl;
  file.read((char*)&xsz, sizeof(quint32));
  file.read((char*)&ysz, sizeof(quint32));
  file.read((char*)&zsz, sizeof(quint32));
  Information::out << "Size: " << xsz << " " << ysz << " " << zsz << endl;
  file.read((char*)&xum, sizeof(float));
  file.read((char*)&yum, sizeof(float));
  file.read((char*)&zum, sizeof(float));
  file.read((char*)&datasz, sizeof(quint64));
  file.read((char*)&cl, sizeof(quint8));
  quint64 expected_size = 2ul * (quint64(xsz) * quint64(ysz) * quint64(zsz));
  if(datasz != expected_size)
    throw QString("Datasize does not match dimensions in MGXS file (size = %1, expected from dimension = %2)")
          .arg(datasz)
          .arg(expected_size);
  Point3u size(xsz, ysz, zsz);
  Point3f step(xum, yum, zum);
  stack->setSize(size);
  stack->setStep(step);
  if(cl > 0) {
    Progress progress(QString("Loading stack %1").arg(stack->id()), expected_size);
    quint64 cur_size = 0;
    char* d = (char*)data.data();
    while(cur_size < 2 * data.size()) {
      if(!progress.advance(cur_size))
        userCancel();
      // Information::out << "advance to " << cur_size << endl;
      quint32 sz;
      qint64 qty = file.read((char*)&sz, sizeof(quint32));
      if(qty != sizeof(quint32)) {
        throw QString("Could not read enough data from MGXS file (read = %1, expected = %2)").arg(qty).arg(
                sizeof(quint32));
      }
      QByteArray stkdata = file.read(sz);
      if(quint32(stkdata.size()) != sz) {
        throw QString("Could not read enough data from MGXS file (read = %1, expected = %2)")
              .arg(stkdata.size())
              .arg(sz);
      }
      stkdata = qUncompress(stkdata);
      if(stkdata.isEmpty()) {
        throw QString("Compressed data in MGXS file is corrupted");
      }
      memcpy(d, stkdata.data(), stkdata.size());
      d += stkdata.size();
      cur_size += stkdata.size();
    }
    if(!progress.advance(cur_size))
      userCancel();
  } else {
    // Ok, if greater than 64MB, read in bits
    quint64 nb_slices = expected_size >> 26;
    quint64 slice_size = 1ul << 26;
    Progress progress(QString("Loading stack %1").arg(stack->id()), nb_slices);
    Information::out << "Nb of slices = " << nb_slices << endl;
    for(quint64 i = 0; i < nb_slices; ++i) {
      qint64 qty = file.read((char*)data.data() + i * slice_size, slice_size);
      if(qty != (int)slice_size) {
        throw QString("Could not read enough data from MGXS file (read = %1, expected = %2)").arg(qty).arg(
                slice_size);
      }
      if(!progress.advance(i))
        userCancel();
      // Information::out << "advance to " << i << endl;
    }
    quint64 left_size = expected_size - (nb_slices << 26);
    qint64 qty = file.read((char*)data.data() + (nb_slices << 26), left_size);
    if(qty != (int)left_size) {
      throw QString("Could not read enough data from MGXS file (read = %1, expected = %2)").arg(qty).arg(
              left_size);
    }
    if(!progress.advance(nb_slices))
      userCancel();
  }
  return true;
}

QList<int> extractVersion(QIODevice& file)
{
  QByteArray version;
  version.reserve(10);
  do {
    char c;
    qint64 qty = file.read(&c, 1);
    if(qty == 0) {
      throw QString("extractVersion:Could not read enough data from file (read = %1, expected = %2)")
            .arg(version.size())
            .arg(version.size() + 1);
    }
    version.push_back(c);
  } while(version[version.size() - 1] != ' ');
  version.chop(1);
  QList<QByteArray> values = version.split('.');
  QList<int> result;
  foreach(const QByteArray &a, values) {
    bool ok;
    result << a.toInt(&ok);
    if(!ok)
      throw QString("extractVersion:Version number must contain only integers (read = '%1')").arg(QString(a));
  }
  return result;
}

bool StackOpen::operator()(Stack* stack, Store* store, QString filename)
{
  if(filename.isEmpty())
    throw QString("StackOpen::Error:Error, trying to load a stack from an empty filename.");

  actingFile(filename);
  store->setFile(filename);

  size_t channel, timepoint;
  if(not parseFilename(filename, channel, timepoint))
    return false;

  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly))
    throw QString("StackOpen::Error:Cannot open input file - '%1'").arg(filename);

  Progress progress(QString("Loading %1 Stack %2 from File '%3'")
                    .arg((store == stack->main()) ? "main" : "work")
                    .arg(stack->userId())
                    .arg(filename),
                    0, false);
  // Progress progress(QString("Reading stack file %1").arg(filename), 0, false);
  if(filename.endsWith(".mgxs", Qt::CaseInsensitive)) {
    static const char version[] = "MGXS ";
    static const int size_version = sizeof(version) - 1;
    char header_version[size_version];
    file.read(header_version, size_version);
    if(memcmp(header_version, version, size_version) == 0) {
      QList<int> version = extractVersion(file);
      if(version.size() != 2) {
        QStringList str;
        foreach(int i, version)
        str << QString::number(i);
        throw QString("StackOpen::Error:Invalid file version format (%1), expected: MAJOR.MINOR")
              .arg(str.join("."));
      }
      bool success = false;
      switch(version[0]) {
      case 1:
        switch(version[1]) {
        case 0:
          success = loadMGXS_1_0(file, stack, store);
          stack->center();
          break;
        case 1:
          success = loadMGXS_1_1(file, stack, store);
          stack->center();
          break;
        case 2:
          success = loadMGXS_1_2(file, stack, store);
          break;
        case 3:
          success = loadMGXS_1_3(file, stack, store);
          break;
        default:
          throw QString(
                  "StackOpen::Error:Unknown file version %1.%2, this software recognizes up to version 1.1")
                .arg(version[0])
                .arg(version[1]);
        }
        break;
      default:
        throw QString("StackOpen::Error:Unknown file version %1.%2, this software recognizes up to version 1.0")
              .arg(version[0])
              .arg(version[1]);
      }
      if(!success)
        return false;
    } else {
      file.seek(0);
      if(!loadMGXS_0(file, stack, store))
        return false;
      stack->center();
    }
  } else if(filename.endsWith(".inr", Qt::CaseInsensitive)) {
    HVecUS& data = store->data();
    QByteArray stkdata = file.readAll();
    file.close();
    CImgUS image;
    Point3f voxelsz;
    image.load_inr(filename.toLocal8Bit(), voxelsz.data());
    Point3u size(image.width(), image.height(), image.depth());
    Point3f step(voxelsz.x(), voxelsz.y(), voxelsz.z());
    stack->setSize(size);
    stack->setStep(step);
    memcpy(data.data(), image.data(), stack->storeSize() * 2);
    //stack->center();
  } else if(filename.endsWith(".tif", Qt::CaseInsensitive) or filename.endsWith(".tiff", Qt::CaseInsensitive)) {
    Image3D data(filename, store->data(), stack->size(), stack->step());
    if(!loadTIFFImage(filename, data, channel, timepoint, true, &progress)) {
      stack->setSize(Point3u(0, 0, 0));
      stack->main()->changed();
      stack->work()->changed();
      setErrorMessage(
        QString("Error, cannot load TIFF file %1. Check standard output for exact error.").arg(filename));
      return false;
    }
    stack->setSize(data.size);
    stack->setStep(data.step);
    store->setLabels(data.labels);
    stack->center();
  } else
    throw QString("StackOpen::Error:Invalid file type - %1").arg(filename);

  if(store->labels()) {
    ushort max_label = 0;
    const HVecUS& data = store->data();
#pragma omp parallel for reduction(max: max_label)
    for(size_t i = 0 ; i < data.size() ; ++i) {
      auto label = data[i];
      if(label > max_label)
        max_label = label;
    }
    stack->setLabel(max_label);
  } else {
    // Autoscale the brightness
    const auto& data = store->data();
    ushort low = 65525u, high = 0u;
#pragma omp parallel for reduction(max: high) reduction(min: low)
    for(size_t i = 0 ; i < data.size() ; ++i) {
      auto value = data[i];
      if(low > value) low = value;
      if(high < value) high = value;
    }
    Information::out << "low = " << low << " - high = " << high << endl;
    if(low < high) {
      auto fct = store->transferFct();
      fct.adjust(double(low)/65535., double(high)/65535.);
      store->setTransferFct(fct);
    }
  }

  store->changed();
  SETSTATUS("Loaded stack " << stack->userId() << ", file:" << store->file() << " size: " << stack->size());
  return true;
}

// REGISTER_STACK_PROCESS(StackOpen);

bool MeshImport::initialize(QStringList& parms, QWidget* parent)
{
  int meshId = parms[5].toInt();
  if(!checkState().mesh(MESH_ANY, int(meshId)))
    return false;

  bool choose_file = stringToBool(parms[6]);
  parms[6] = "Yes";
  QString filename = parms[0];
  if(filename.isEmpty() or choose_file) {
    QDialog dlg(parent);
    Ui_ImportMeshDialog ui;
    ui.setupUi(&dlg);

    ui.meshId->clear();

    for(int i = 0 ; i < meshCount() ; ++i)
      ui.meshId->addItem(QString::number(i+1));

    ui.meshId->setCurrentIndex(meshId);

    this->ui = &ui;
    this->dlg = &dlg;

    QObject::connect(ui.SelectMeshFile, &QAbstractButton::clicked, this, &MeshImport::selectMeshFile);
    QObject::connect(ui.MeshType, SIGNAL(currentIndexChanged(const QString &)), this,
                     SLOT(selectMeshType(const QString &)));

    Mesh* mesh = this->mesh(meshId);
    if(not mesh->file().isEmpty())
      filename = parms[0];

    setMeshFile(properFile(filename, parms[1]), parms[1]);
    ui.ScaleUm->setChecked(stringToBool(parms[2]));
    ui.Transform->setChecked(stringToBool(parms[3]));
    ui.Add->setChecked(stringToBool(parms[4]));

    bool res = false;
    if(dlg.exec() == QDialog::Accepted) {
      parms[0] = ui.MeshFile->text();
      parms[1] = ui.MeshType->currentText();
      parms[2] = (ui.ScaleUm->isChecked() ? "yes" : "no");
      parms[3] = (ui.Transform->isChecked() ? "yes" : "no");
      parms[4] = (ui.Add->isChecked() ? "yes" : "no");
      parms[5] = QString::number(ui.meshId->currentIndex());
      res = true;
    }
    this->ui = 0;
    this->dlg = 0;
    return res;
  }
  return true;
}

bool MeshImport::operator()(const QStringList& parms)
{
  int meshId = parms[5].toInt();
  if(!checkState().mesh(MESH_ANY, meshId))
    return false;
  Mesh* mesh = this->mesh(meshId);
  QString type = parms[1];
  bool scale = stringToBool(parms[2]);
  bool transform = stringToBool(parms[3]);
  bool add = stringToBool(parms[4]);
  bool res = (*this)(mesh, parms[0], type, scale, transform, add);
  if(res) {
    // Show mesh if nothing visible
    if(!(mesh->isMeshVisible() or mesh->isSurfaceVisible())) {
      mesh->showSurface();
      mesh->showMesh();
    }
    if(mesh->hasImgTex())
      mesh->showImage();
    else
      mesh->showSignal();
    mesh->setNormals();
  }
  return res;
}

bool MeshImport::operator()(Mesh* mesh, QString filename, QString type, bool scale, bool transform, bool add)
{
  Progress progress(QString("Loading Mesh %1 from File '%2'").arg(mesh->userId()).arg(filename), 0, false);
  // Progress progress(QString("Loading Mesh File - %1").arg(filename), 0, false);
  actingFile(filename);
  QFileInfo fi(filename);
  QString suf = fi.suffix();
  bool success = false;
  if(type.isEmpty() or type.toLower() == "from extension") {
    type = extToType(suf);
    if(type == "From extension") {
      setErrorMessage(QString("Cannot find the type of the file '%1'").arg(filename));
      return false;
    }
  }
  if(type == "PLY")
    success = loadMeshPLY(mesh, filename, scale, transform, add);
  else if(type == "VTK Mesh")
    success = loadMeshVTK(mesh, filename, scale, transform, add);
  else if(type == "Text")
    success = loadText(mesh, filename, scale, transform, add);
  else if(type == "Cells")
    success = loadCells(mesh, filename, scale, transform, add);
  else if(type == "Keyence")
    success = loadKeyence(mesh, filename, scale, transform, add);
  else if(type == "MeshEdit")
    success = loadMeshEdit(mesh, filename, scale, transform, add);
  else if(type == "OBJ")
    success = loadMeshOBJ(mesh, filename, scale, transform, add);
  else {
    setErrorMessage(QString("Unknown file type '%1'").arg(type));
    return false;
  }
  if(success) {
    mesh->setFile(properFile(filename, "Mesh"));
    if(mesh->stack()->empty())
      mesh->setScaled(scale);
    else
      mesh->setScaled(true);
    mesh->setTransformed(transform);
    mesh->updateAll();
  }
  return success;
}

static Point3f realPos(Point3f pos, bool scale, bool transform, const Mesh* mesh)
{
  if(!scale)
    pos = mesh->stack()->abstractToWorld(pos);
  if(transform)
    pos = Point3f(mesh->stack()->frame().coordinatesOf(Vec(pos)));
  return pos;
}

bool MeshImport::loadText(Mesh* mesh, const QString& filename, bool scale, bool transform, bool add)
{
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    setErrorMessage(QString("loadText::Error:Cannot open input file: %1").arg(filename));
    return false;
  }

  vvgraph& S = mesh->graph();

  if(!S.empty() and !add)
    mesh->reset();

  vertex_map vmap;

  QTextStream in(&file);
  int vCnt;
  in >> vCnt;
  Progress progress(QString("Loading Mesh %1 from File '%2'").arg(mesh->userId()).arg(mesh->file()), vCnt * 2);
  // Progress progress(QString("Loading Mesh-%1 - %2").arg(mesh->userId()).arg(mesh->file()), vCnt*2);
  int interval = vCnt / 100;
  if(interval < 1)
    interval = 1;
  S.reserve(vCnt);
  for(int i = 0; i < vCnt; i++) {
    if(in.atEnd())
      throw QString("Premature end of file reading vertices");
    vertex v;
    S.insert(v);
    in >> v->saveId >> v->pos >> v->label;
    v->pos = realPos(v->pos, scale, transform, mesh);
    vmap[v->saveId] = v;
    if(v->label > mesh->viewLabel())
      mesh->setLabel(v->label);
    if((i % interval == 0)and !progress.advance(i))
      userCancel();
  }
  for(int i = 0; i < vCnt; i++) {
    if(in.atEnd())
      throw QString("Premature end of file reading neighborhoods");
    uint vId, nCnt;
    in >> vId >> nCnt;
    vertex_map::const_iterator vit = vmap.find(vId);
    if(vit == vmap.end())
      throw QString("Invalid vertex id: %1").arg(vId);
    vertex v = vit->second;
    vertex pn(0);
    for(uint j = 0; j < nCnt; j++) {
      uint nId;
      in >> nId;
      vertex_map::const_iterator nit = vmap.find(nId);
      if(nit == vmap.end())
        throw QString("Invalid vertex id: %1").arg(nId);
      vertex n = nit->second;
      if(S.valence(v) == 0)
        S.insertEdge(v, n);
      else
        S.spliceAfter(v, pn, n);
      pn = n;
    }
    if((i % interval == 0)and !progress.advance(vCnt + i))
      userCancel();
  }
  if(!progress.advance(2 * vCnt))
    userCancel();
  file.close();
  mesh->setCells(false);
  mesh->clearImgTex();
  mesh->setNormals();
  SETSTATUS("Loaded mesh, file:" << mesh->file() << ", vertices:" << S.size());
  return true;
}

using util::PlyFile;

void showHeader(PlyFile& ply)
{
  Information::out << "PLY file content";
  Information::out << "format = " << PlyFile::formatNames[ply.format()] << endl;
  Information::out << "version = " << ply.version() << endl;
  Information::out << "content position = " << ply.contentPosition() << endl;
  Information::out << endl;
  Information::out << "# elements = " << ply.nbElements() << endl;
  for(size_t i = 0; i < ply.nbElements(); ++i) {
    const PlyFile::Element& element = *ply.element(i);
    Information::out << "Element '" << element.name() << "' - " << element.size()
                     << " # properties = " << element.nbProperties() << endl;
    for(size_t j = 0; j < element.nbProperties(); ++j) {
      const PlyFile::Property& prop = *element.property(j);
      Information::out << "Property '" << prop.name() << "'";
      if(prop.kind() == PlyFile::Property::LIST)
        Information::out << " list " << PlyFile::typeNames[prop.sizeType()];
      Information::out << " " << PlyFile::typeNames[prop.fileType()] << " / "
                       << PlyFile::typeNames[prop.memType()] << endl;
    }
  }
}

bool MeshImport::loadMeshPLY(Mesh* mesh, const QString& filename, bool scale, bool transform, bool add)
{
  vvgraph& S = mesh->graph();

  Progress progress(QString("Loading PLY Mesh %1 from File '%2'").arg(mesh->userId()).arg(filename), 0);

  if(!S.empty() and !add)
    mesh->reset();

  // Define the reader and read header
  PlyFile ply;
  if(!ply.parseHeader(filename))
    throw(QString("Cannot parse PLY file header"));

  // Get vertices and properties
  PlyFile::Element* vtx = ply.element("vertex");
  if(!vtx)
    throw(QString("No vertex element in PLY file"));

  PlyFile::Property* vx = vtx->property("x");   // vertex positions
  PlyFile::Property* vy = vtx->property("y");
  PlyFile::Property* vz = vtx->property("z");
  if(!vx or !vy or !vz)
    throw(QString("x, y or z missing from vertex element in PLY file"));

  vx->setMemType(PlyFile::FLOAT);
  vy->setMemType(PlyFile::FLOAT);
  vz->setMemType(PlyFile::FLOAT);

  PlyFile::Property* vlabel = vtx->property("label");   // vertex labels
  if(vlabel)
    vlabel->setMemType(PlyFile::INT);

  // Vertex signal, if not signal, use the color
  bool hasSignal = false;
  bool hasRGB = false;
  PlyFile::Property* vSignal = vtx->property("signal");
  PlyFile::Property* vRed = vtx->property("red");
  PlyFile::Property* vGreen = vtx->property("green");
  PlyFile::Property* vBlue = vtx->property("blue");
  if(vSignal) {
    hasSignal = true;
    vSignal->setMemType(PlyFile::FLOAT);
  } else if(vRed and vGreen and vBlue) {
    hasRGB = true;
    vRed->setMemType(PlyFile::UCHAR);
    vGreen->setMemType(PlyFile::UCHAR);
    vBlue->setMemType(PlyFile::UCHAR);
  }

  // Get faces and properties
  PlyFile::Element* faces = ply.element("face");   // faces
  if(!faces)
    throw(QString("No face element in PLY file"));

  PlyFile::Property* vi = faces->property("vertex_index");
  if(!vi) {
    vi = faces->property("vertex_indices");
    if(!vi)
      throw(QString("Neither vertex_index or vertex_indices are present in face element"));
  }
  vi->setMemType(PlyFile::INT);

  PlyFile::Property* flabel = faces->property("label");   // face labels
  if(flabel)
    flabel->setMemType(PlyFile::INT);

  // Parse file
  if(!ply.parseContent())
    throw(QString("Unable to parse contents of PLY file"));

  // Get vertex data
  std::vector<vertex> vertices(vtx->size(), vertex(0));   // vertex positions
  const std::vector<float>& px = *vx->value<float>();
  const std::vector<float>& py = *vy->value<float>();
  const std::vector<float>& pz = *vz->value<float>();

  std::vector<int>* vlabelp = 0;   // vertex labels
  if(vlabel)
    vlabelp = vlabel->value<int>();

  std::vector<uchar>* vRedp = 0;   // vertex colors
  std::vector<uchar>* vGreenp = 0;
  std::vector<uchar>* vBluep = 0;
  std::vector<float>* vSignalp = 0;
  if(hasSignal) {
    Information::out << "Has signal" << endl;
    vSignalp = vSignal->value<float>();
  } else if(hasRGB) {
    Information::out << "Has RGB information" << endl;
    vRedp = vRed->value<uchar>();
    vGreenp = vGreen->value<uchar>();
    vBluep = vBlue->value<uchar>();
  }

  // Get face data
  std::vector<Point3i> triangles;   // faces
  const std::vector<std::vector<int32_t> >& vertex_index = *vi->list<int32_t>();

  std::vector<int>* flabelp = 0;   // face labels
  if(flabel)
    flabelp = flabel->value<int>();

  // See if cellular mesh
  bool cells = false;
  for(size_t i = 0; i < faces->size(); ++i)
    if(vertex_index[i].size() > 3)
      cells = true;
  float minSignal = std::numeric_limits<float>::max();
  float maxSignal = -std::numeric_limits<float>::max();
  int maxLabel = 0;
  // Process vertices
#pragma omp parallel for reduction(max: maxLabel)
  for(uint i = 0; i < vtx->size(); ++i) {
    vertex v;
    v->pos = Point3f(px[i], py[i], pz[i]);
    v->pos = realPos(v->pos, scale, transform, mesh);
    v->saveId = i;
    v->type = 'j';
    vertices[i] = v;
    //S.insert(v);
    if(cells)
      v->label = -1;
    else if(vlabel)
      v->label = (*vlabelp)[i];

    if(hasSignal)
      v->signal = (*vSignalp)[i];
    else if(hasRGB)
      v->signal = (0.21 * (*vRedp)[i] + 0.71 * (*vGreenp)[i] + 0.07 * (*vBluep)[i]) / 256.0;
    else
      v->signal = 0;

    if(minSignal < v->signal)
      minSignal = v->signal;
    if(maxSignal > v->signal)
      maxSignal = v->signal;

    if(v->label > maxLabel)
      maxLabel = v->label;
  }
  mesh->setLabel(maxLabel);
  if(hasSignal or hasRGB)
    mesh->signalBounds() = Point2f(minSignal, maxSignal);
  else
    mesh->signalBounds() = Point2f(0, 1);

  // Process faces
  int nextLabel = mesh->viewLabel();
  if(cells) {
    for(size_t i = 0; i < faces->size(); ++i) {
      // If a cellular mesh, make a triangle fan.
      vertex c;

      // Find center vertex position
      c->pos = Point3f(0, 0, 0);
      forall(int j, vertex_index[i])
        c->pos += vertices[j]->pos;
      c->pos /= vertex_index[i].size();
      c->type = 'c';

      // Put label on center vertex
      if(flabel)
        c->label = (*flabelp)[i];
      else
        c->label = ++nextLabel;

      int centerv = vertices.size();
      for(size_t j = 0; j < vertex_index[i].size(); ++j) {
        size_t prev = j - 1;
        if(j == 0)
          prev = vertex_index[i].size() - 1;

        triangles.push_back(Point3i(centerv, vertex_index[i][prev], vertex_index[i][j]));
      }
      vertices.push_back(c);

      if(c->label > mesh->viewLabel())
        mesh->setLabel(c->label);
    }
  } else {
    triangles.resize(faces->size());
#pragma omp parallel for
    for(size_t i = 0; i < faces->size(); ++i)
      triangles[i] = Point3i(vertex_index[i][0], vertex_index[i][1], vertex_index[i][2]);
  }

  // Create connectivity from triangle list
  if(!shape::meshFromTriangles(S, vertices, triangles))
    throw(QString("Cannot add all the triangles"));

  mesh->setCells(cells);
  mesh->clearImgTex();
  mesh->setNormals();
  if(cells)
    SETSTATUS("Loaded cellular mesh, file:" << mesh->file() << ", vertices:" << S.size());
  else
    SETSTATUS("Loaded triangle mesh, file:" << mesh->file() << ", vertices:" << S.size());
  return true;
}

bool MeshImport::loadCells(Mesh* mesh, const QString& filename, bool scale, bool transform, bool add)
{
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    setErrorMessage(QString("loadMesh::Error:Cannot open input file: %1").arg(filename));
    return false;
  }

  vvgraph& S = mesh->graph();

  if(!S.empty() and !add)
    mesh->reset();

  vertex_map vmap;

  QTextStream in(&file);
  int vCnt;
  in >> vCnt;
  Progress progress(QString("Loading Cellular Mesh %1 from File '%2'").arg(mesh->userId()).arg(mesh->file()),
                    vCnt * 2);
  int interval = vCnt / 100;
  if(interval < 1)
    interval = 1;
  S.reserve(vCnt);
  for(int i = 0; i < vCnt; i++) {
    if(in.atEnd())
      throw QString("Premature end of file reading vertices");
    vertex v;
    S.insert(v);
    char tchar;
    in >> v->saveId >> v->pos >> v->label >> tchar >> v->type;
    v->pos = realPos(v->pos, scale, transform, mesh);
    vmap[v->saveId] = v;
    if(v->label > mesh->viewLabel())
      mesh->setLabel(v->label);
    if((i % interval == 0)and !progress.advance(i))
      userCancel();
  }
  for(int i = 0; i < vCnt; i++) {
    if(in.atEnd())
      throw QString("Premature end of file reading neighborhoods");
    uint vId, nCnt;
    in >> vId >> nCnt;
    vertex_map::const_iterator vit = vmap.find(vId);
    if(vit == vmap.end())
      throw QString("Invalid vertex id: %1").arg(vId);
    vertex v = vit->second;
    vertex pn(0);
    for(uint j = 0; j < nCnt; j++) {
      uint nId;
      in >> nId;
      vertex_map::const_iterator nit = vmap.find(nId);
      if(nit == vmap.end())
        throw QString("Invalid vertex id: %1").arg(nId);
      vertex n = nit->second;
      if(S.valence(v) == 0)
        S.insertEdge(v, n);
      else
        S.spliceAfter(v, pn, n);
      pn = n;
    }
    if((i % interval == 0)and !progress.advance(vCnt + i))
      userCancel();
  }
  if(!progress.advance(2 * vCnt))
    userCancel();
  file.close();
  mesh->setCells(true);
  mesh->clearImgTex();
  mesh->setNormals();
  SETSTATUS("Loaded cell mesh, file:" << mesh->file() << ", vertices:" << S.size());
  return true;
}

bool MeshImport::loadKeyence(Mesh* mesh, const QString& filename, bool scale, bool transform, bool /*add*/)
{
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly)) {
    setErrorMessage(QString("loadMesh::Error:Cannot open input file: %1").arg(filename));
    return false;
  }

  vvgraph& S = mesh->graph();

  mesh->reset();

  // First read in the entire file
  QByteArray buff = file.readAll();
  if(buff.size() != file.size())
    throw QString("Could not read the whole file");
  file.close();
  // Grab the image size
  int xsize = *(short*)(buff.data() + 47);
  int ysize = *(short*)(buff.data() + 49);
  // Height map uses every 4th pixel
  int xsz = xsize / 4;
  int ysz = ysize / 4;
  Progress progress(QString("Loading Height Map %1 from File '%2'").arg(mesh->userId()).arg(mesh->file()), ysz);

  // Find second image file
  int imgpos = buff.indexOf(QByteArray("JFIF"), 16);
  if(imgpos < 0)
    throw QString("Can't find color image");
  imgpos -= 6;
  QTemporaryFile imgfile;

  // Write to temp file
  imgfile.open();
  imgfile.write(buff.data() + imgpos, buff.size() - imgpos - xsz * ysz * 4);
  imgfile.close();

  // Load surface texture image
  QImage image(imgfile.fileName());
  // image.invertPixels();
  if(xsize != image.width() or ysize != image.height())
    throw QString("Invalid image size");
  mesh->setImgTex(image);

  // Find max/min z values to center image in z
  float maxz = -1e10, minz = 1e10;
  float* htp = (float*)(buff.data() + buff.size() - xsz * ysz * 4);
  S.reserve(xsz * ysz);
  for(int i = 0; i < xsz * ysz; i++) {
    float z = *htp++;
    if(z > maxz)
      maxz = z;
    if(z < minz)
      minz = z;
  }
  float zshift = (minz + maxz) / 2.0;

  // Save the vertex ids for connecting
  // vertex vtx[xsz][ysz];
  std::vector<std::vector<vertex> > vtx(xsz, std::vector<vertex>(ysz, vertex(0)));

  // Create the vertices and fill in height and texture coords
  htp = (float*)(buff.data() + buff.size() - xsz * ysz * 4);
  for(int y = ysz - 1; y >= 0; y--) {
    for(int x = xsz - 1; x >= 0; x--) {
      float z = *htp++;
      vertex v;
      S.insert(v);
      vtx[x][y] = v;
      v->pos.x() = (double(x) / double(xsz) - .5) * 4 / 3;
      v->pos.y() = double(y) / double(ysz) - .5;
      v->pos.z() = (z - zshift) * .03;
      v->pos = realPos(v->pos, scale, transform, mesh);
      v->txpos.x() = (float(x * 4) / float(xsize));
      v->txpos.y() = (float(y * 4) / float(ysize));
    }
  }

  // Connect neighborhoods
  for(int y = 0; y < ysz; y++) {
    if(!progress.advance(y + 1))
      throw(QString("Keyence load cancelled"));
    for(int x = 0; x < xsz; x++) {
      std::vector<vertex> nhbs;
      nhbs.push_back(vtx[x][y]);
      if(x == 0 and y == ysz - 1) {       // top left corner;
        nhbs.push_back(vtx[x][y - 1]);
        nhbs.push_back(vtx[x + 1][y - 1]);
        nhbs.push_back(vtx[x + 1][y]);
      } else if(x == 0 and y == 0) {       // bottom left corner
        nhbs.push_back(vtx[x + 1][y]);
        nhbs.push_back(vtx[x][y + 1]);
      } else if(x == xsz - 1 and y == 0) {       // bottom right corner
        nhbs.push_back(vtx[x][y + 1]);
        nhbs.push_back(vtx[x - 1][y + 1]);
        nhbs.push_back(vtx[x - 1][y]);
      } else if(x == xsz - 1 and y == ysz - 1) {       // top right corner
        nhbs.push_back(vtx[x - 1][y]);
        nhbs.push_back(vtx[x][y - 1]);
      } else if(y == ysz - 1) {       // top edge
        nhbs.push_back(vtx[x - 1][y]);
        nhbs.push_back(vtx[x][y - 1]);
        nhbs.push_back(vtx[x + 1][y - 1]);
        nhbs.push_back(vtx[x + 1][y]);
      } else if(x == 0) {       // left edge
        nhbs.push_back(vtx[x][y - 1]);
        nhbs.push_back(vtx[x + 1][y - 1]);
        nhbs.push_back(vtx[x + 1][y]);
        nhbs.push_back(vtx[x][y + 1]);
      } else if(y == 0) {       // bottom edge
        nhbs.push_back(vtx[x + 1][y]);
        nhbs.push_back(vtx[x][y + 1]);
        nhbs.push_back(vtx[x - 1][y + 1]);
        nhbs.push_back(vtx[x - 1][y]);
      } else if(x == xsz - 1) {       // right edge
        nhbs.push_back(vtx[x][y - 1]);
        nhbs.push_back(vtx[x - 1][y + 1]);
        nhbs.push_back(vtx[x - 1][y]);
        nhbs.push_back(vtx[x][y + 1]);
      } else {       // Interior vertex
        nhbs.push_back(vtx[x + 1][y - 1]);
        nhbs.push_back(vtx[x + 1][y]);
        nhbs.push_back(vtx[x][y + 1]);
        nhbs.push_back(vtx[x - 1][y + 1]);
        nhbs.push_back(vtx[x - 1][y]);
        nhbs.push_back(vtx[x][y - 1]);
      }
      vertex v = nhbs[0];
      vertex pn(0);
      for(uint i = 1; i < nhbs.size(); i++) {
        vertex n = nhbs[i];
        if(i == 1)
          S.insertEdge(v, n);
        else
          S.spliceAfter(v, pn, n);
        pn = n;
      }
    }
  }
  mesh->setCells(false);
  mesh->setNormals();
  SETSTATUS("Loaded Keyence mesh, file:" << mesh->file() << ", vertices:" << S.size());
  return true;
}

bool MeshImport::loadMeshOBJ(Mesh* mesh, const QString& filename, bool scale, bool transform, bool add)
{
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    setErrorMessage(QString("loadMesh::Error:Cannot open input file: %1").arg(filename));
    return false;
  }

  std::vector<Point3f> points;
  std::vector<float> signal;
  std::vector<int> labels;
  IntFloatMap& label_heat = mesh->labelHeat();
  label_heat.clear();
  std::vector<Point3i> triangles;
  float minSignal = FLT_MAX, maxSignal = -FLT_MAX;
  float minHeat = FLT_MAX, maxHeat = -FLT_MAX;

  QTextStream ts(&file);
  int line_count = 0;
  bool fixedBound = false;

  Information::out << "Reading the file" << endl;
  QString signalDescription;
  QString heatDescription;

  while(!ts.atEnd()) {
    ++line_count;
    QString line = ts.readLine().trimmed();
    int idx = line.indexOf('#');
    if(idx != -1)
      line = line.left(idx).trimmed();
    if(!line.isEmpty()) {
      QStringList fields = line.split(QRegExp("[ \t]"), QString::SkipEmptyParts);
      if(fields[0] == "v") {
        if(fields.size() != 4 and fields.size() != 5) {
          setErrorMessage(
            QString("Error reading file '%1' on line %2: vertex needs 3 or 4 values (x y z [w]), got %3")
            .arg(filename)
            .arg(line_count)
            .arg(fields.size() - 1));
          return false;
        }
        Point3f pos;
        bool ok;
        pos.x() = fields[1].toFloat(&ok);
        if(!ok) {
          setErrorMessage(QString("Error reading file '%1' on line %2: invalid value for vertex x: '%3'")
                          .arg(filename)
                          .arg(line_count)
                          .arg(fields[1]));
          return false;
        }
        pos.y() = fields[2].toFloat(&ok);
        if(!ok) {
          setErrorMessage(QString("Error reading file '%1' on line %2: invalid value for vertex y: '%3'")
                          .arg(filename)
                          .arg(line_count)
                          .arg(fields[2]));
          return false;
        }
        pos.z() = fields[3].toFloat(&ok);
        if(!ok) {
          setErrorMessage(QString("Error reading file '%1' on line %2: invalid value for vertex z: '%3'")
                          .arg(filename)
                          .arg(line_count)
                          .arg(fields[3]));
          return false;
        }
        points.push_back(pos);

        // If there is a 5th field, this is w, so we need to normalize the position with this
        if(fields.size() == 5) {
          float w = fields[4].toFloat(&ok);
          if(!ok) {
            setErrorMessage(QString("Error reading file '%1' on line %2: invalid value for vertex w: '%3'")
                            .arg(filename)
                            .arg(line_count)
                            .arg(fields[4]));
            return false;
          }
          if(w != 0)
            pos /= w;
          else {
            setErrorMessage(
              QString("Error reading file '%1' on line %2: LithoGraphX cannot interpret a vertex w of 0")
              .arg(filename)
              .arg(line_count));
            return false;
          }
        }
      } else if(fields[0] == "f") {
        std::vector<int> poly(fields.size() - 1);
        bool ok;
        int psz = points.size();
        for(int i = 0; i < fields.size() - 1; ++i) {
          poly[i] = fields[i + 1].toInt(&ok) - 1;
          if(!ok or poly[i] < 0 or poly[i] >= psz) {
            setErrorMessage(
              QString("Error reading file '%1' on line %2: the first vertex id is not valid: '%3'")
              .arg(filename)
              .arg(line_count)
              .arg(fields[i + 1]));
            return false;
          }
        }
        // If a triangle, just add it
        if(poly.size() == 3)
          triangles.push_back(Point3i(poly[0], poly[1], poly[2]));
        else {         // Otherwise make a triangle fan
                       // Find center
          Point3f c(0, 0, 0);
          for(size_t i = 0; i < poly.size(); ++i)
            c += points[poly[i]];
          c /= poly.size();
          points.push_back(c);

          // Add tris
          for(size_t i = 0; i < poly.size(); ++i)
            if(i == poly.size() - 1)
              triangles.push_back(Point3i(psz, poly[i], poly[0]));
            else
              triangles.push_back(Point3i(psz, poly[i], poly[i + 1]));
        }
      } else if(fields[0] == "label") {
        if(fields.size() != 2) {
          setErrorMessage(
            QString("Error reading file '%1' on line %2: the label field must have 1 value, got %3")
            .arg(filename)
            .arg(line_count)
            .arg(fields.size() - 1));
          return false;
        }
        int lab;
        bool ok;
        lab = fields[1].toInt(&ok);
        if(!ok) {
          setErrorMessage(
            QString("Error reading file '%1' on line %2: the vertex label has invalid value: '%3'")
            .arg(filename)
            .arg(line_count)
            .arg(fields[1]));
          return false;
        }
        labels.push_back(lab);
      } else if(fields[0] == "vv") {
        if(fields.size() != 2) {
          setErrorMessage(
            QString("Error reading file '%1' on line %2: the vv field must have 1 value, got %3")
            .arg(filename)
            .arg(line_count)
            .arg(fields.size() - 1));
          return false;
        }
        float value;
        bool ok;
        value = fields[1].toFloat(&ok);
        if(!ok) {
          setErrorMessage(
            QString("Error reading file '%1' on line %2: the vertex value has invalid value: '%3'")
            .arg(filename)
            .arg(line_count)
            .arg(fields[1]));
          return false;
        }
        signal.push_back(value);
        if(!fixedBound) {
          if(value > maxSignal)
            maxSignal = value;
          if(value < minSignal)
            minSignal = value;
        }
      } else if(fields[0] == "vv_desc") {
        fields.pop_front();
        signalDescription = fields.join(" ");
      } else if(fields[0] == "vv_range") {
        if(fields.size() != 3) {
          return setErrorMessage(
            QString("Error reading file '%1' on line %2: the vv_range needs 2 values, not %3")
            .arg(filename)
            .arg(line_count)
            .arg(fields.size() - 1));
        }
        float vmin, vmax;
        bool ok;
        vmin = fields[1].toFloat(&ok);
        if(!ok) {
          return setErrorMessage(
            QString("Error reading file '%1' on line %2: min value for vv_range is not a value float: %3")
            .arg(filename)
            .arg(line_count)
            .arg(fields[1]));
        }
        vmax = fields[2].toFloat(&ok);
        if(!ok) {
          return setErrorMessage(
            QString("Error reading file '%1' on line %2: max value for vv_range is not a value float: %3")
            .arg(filename)
            .arg(line_count)
            .arg(fields[2]));
        }
        minSignal = vmin;
        maxSignal = vmax;
        Information::out << "vv_range = " << minSignal << " - " << maxSignal << endl;
        fixedBound = true;
      } else if(fields[0] == "heat") {
        if(fields.size() != 3) {
          setErrorMessage(
            QString("Error reading file '%1' on line %2: the vv field must have 2 value, got %3")
            .arg(filename)
            .arg(line_count)
            .arg(fields.size() - 1));
          return false;
        }
        int lab;
        float value;
        bool ok;
        lab = fields[1].toInt(&ok);
        if(!ok) {
          setErrorMessage(
            QString("Error reading file '%1' on line %2: the heat label has invalid value: '%3'")
            .arg(filename)
            .arg(line_count)
            .arg(fields[1]));
          return false;
        }
        value = fields[2].toFloat(&ok);
        if(!ok) {
          setErrorMessage(
            QString("Error reading file '%1' on line %2: the heat value has invalid value: '%3'")
            .arg(filename)
            .arg(line_count)
            .arg(fields[2]));
          return false;
        }
        label_heat[lab] = value;
        if(value < minHeat)
          minHeat = value;
        if(value > maxHeat)
          maxHeat = value;
      } else if(fields[0] == "heat_desc") {
        fields.pop_front();
        heatDescription = fields.join(" ");
      }
      // Ignore anything else
    }
  }

  if(!signal.empty() and signal.size() != points.size()) {
    setErrorMessage(
      QString(
        "Error reading file '%1', signal array size mismatch. Current file has %2 vs fields and %3 vertices")
      .arg(filename)
      .arg(signal.size())
      .arg(points.size()));
    return false;
  }
  if(!labels.empty() and labels.size() != points.size()) {
    setErrorMessage(QString("Error reading file '%1', labels array size mismatch. Current file has %2 labels "
                            "fields and %3 vertices")
                    .arg(filename)
                    .arg(labels.size())
                    .arg(points.size()));
    return false;
  }

  Information::out << "Extracted " << points.size() << " points and " << triangles.size() << " triangles" << endl;

  vvgraph& S = mesh->graph();

  if(!add)
    mesh->reset();
  else
    forall(const vertex& v, S)
      v->saveId = -1;

  if(signal.empty() and not fixedBound) {
    minSignal = 0.0;
    maxSignal = 1.0;
  } else if(maxSignal == minSignal)
    minSignal -= 1.0f;

  // First add vertices
  std::vector<vertex> vtx(points.size(), vertex(0));
  for(size_t i = 0; i < points.size(); ++i) {
    vertex v;
    v->pos = realPos(points[i], scale, transform, mesh);
    vtx[i] = v;
    if(signal.empty())
      v->signal = 1.0;
    else {
      v->signal = signal[i];
    }
    if(!labels.empty())
      v->label = labels[i];
    // Check for NaN
    v->signal = v->signal == v->signal ? v->signal : 0;
  }

  if(!shape::meshFromTriangles(S, vtx, triangles)) {
    setErrorMessage("Error, cannot add all the triangles");
    return false;
  }

  std::cout << "MinSignal:" << minSignal << ", MaxSignal:" << maxSignal << std::endl;

  if(!label_heat.empty()) {
    if(minHeat >= maxHeat)
      maxHeat += 1;
    mesh->heatMapBounds() = Point2f(minHeat, maxHeat);
    Information::out << "heatMapBounds = " << mesh->heatMapBounds() << endl;
    mesh->showHeat();
  }
  if(!heatDescription.isEmpty())
    mesh->heatMapUnit() = heatDescription;

  mesh->signalBounds() = Point2f(minSignal, maxSignal);
  mesh->signalUnit() = signalDescription;
  mesh->setCells(false);
  mesh->clearImgTex();
  mesh->setNormals();
  mesh->updateBBox();
  mesh->setCulling(false);
  mesh->updateAll();

  return true;
}

bool MeshImport::loadMeshEdit(Mesh* mesh, const QString& filename, bool scale, bool transform, bool add)
{
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    setErrorMessage(QString("loadMesh::Error:Cannot open input file: %1").arg(filename));
    return false;
  }

  vvgraph& S = mesh->graph();

  if(!add)
    mesh->reset();
  else
    forall(const vertex& v, S)
      v->saveId = -1;

  std::vector<vertex> vertices;
  std::vector<Point3i> triangles;

  QTextStream in(&file);
  QString tmp;
  tmp = in.readLine();
  tmp = in.readLine();
  tmp = in.readLine();
  int vCnt;
  in >> vCnt;
  Progress progress(QString("Loading Triangle Mesh Vertices %1 from File '%2'").arg(mesh->userId()).arg(mesh->file()),
                    vCnt);
  int prog = 1;
  uint saveId = 0;
  S.reserve(vCnt);
  for(int i = 0; i < vCnt; i++) {
    if(in.atEnd())
      throw(QString("Premature end of file reading vertices"));
    vertex v;
    S.insert(v);
    in >> v->pos >> v->label;
    v->pos = realPos(v->pos, scale, transform, mesh);
    v->saveId = saveId++;
    vertices.push_back(v);
    if(v->label > mesh->viewLabel())
      mesh->setLabel(v->label);
    if(!progress.advance(prog++))
      userCancel();
  }
  tmp = in.readLine();
  tmp = in.readLine();
  int tCnt, t;

  in >> tCnt;
  progress.restart(QString("Loading triangles-%1 - %2").arg(mesh->userId()).arg(mesh->file()), tCnt);
  for(int i = 0; i < tCnt; i++) {
    if(in.atEnd())
      throw QString("Premature end of file reading triangles");
    Point3i tri;
    in >> tri.x() >> tri.y() >> tri.z() >> t;
    tri -= Point3i(1, 1, 1);
    triangles.push_back(tri);
    if(!progress.advance(prog++))
      userCancel();
  }
  file.close();

  if(!shape::meshFromTriangles(S, vertices, triangles)) {
    setErrorMessage("Error, cannot add all the triangles");
    return false;
  }
  mesh->setCells(false);
  mesh->clearImgTex();
  mesh->setNormals();
  SETSTATUS("Loaded mesh, file:" << mesh->file() << ", vertices:" << S.size());
  return true;
}

void MeshImport::selectMeshType(const QString& type)
{
  QString filename = ui->MeshFile->text();
  if(filename.isEmpty() or type.toLower() == "from extension")
    return;
  QFileInfo fi(filename);
  QString suf = fi.suffix();
  if(!suf.isEmpty())
    filename = filename.left(filename.size() - suf.size() - 1);
  if(type == "Mesh")
    filename += ".mgxm";
  else if(type == "PLY")
    filename += ".ply";
  else if(type == "Text" or type == "Cells")
    filename += ".txt";
  else if(type == "Keyence")
    filename += ".jpg";
  else if(type == "MeshEdit")
    filename += ".mesh";
  else if(type == "OBJ")
    filename += ".obj";
  ui->MeshFile->setText(filename);
}

void MeshImport::selectMeshFile()
{
  QString filename = ui->MeshFile->text();
  QStringList filters;
  filters << "Stanford Polygon files (*.ply)"
          << "VTK Mesh files (*.vtu)"
          << "Text or cell files (*.txt)"
          << "Keyence files (*.jpg)"
          << "MeshEdit files (*.mesh)"
          << "OBJ files (*.obj)"
          << "All Mesh Files (*.ply *.vtu *.txt *.jpg *.mesh *.obj)";
  QString filter;
  {
    QFileInfo fi(filename);
    QString suf = fi.suffix();
    if(suf == "vtu")
      filter = filters[0];
    else if(suf == "vtu")
      filter = filters[1];
    else if(suf == "txt")
      filter = filters[2];
    else if(suf == "jpg")
      filter = filters[3];
    else if(suf == "mesh")
      filter = filters[4];
    else if(suf == "obj")
      filter = filters[5];
    else
      filter = filters[6];
  }
  filename = QFileDialog::getOpenFileName(dlg, QString("Select mesh file"), filename, filters.join(";;"), &filter,
                                          FileDialogOptions);
  if(!filename.isEmpty()) {
    if(!QFile::exists(filename)) {
      QMessageBox::information(dlg, "Error selecting file",
                               "You must select an existing file. Your selection is ignored.");
      return;
    }
    setMeshFile(filename);
  }
}

QString MeshImport::typeToExt(QString type) const
{
  if(type == "PLY")
    return "ply";
  else if(type == "VTK Mesh")
    return "vtu";
  else if(type == "Text" or type == "Cells")
    return "txt";
  else if(type == "MeshEdit")
    return "mesh";
  else if(type == "Keyence")
    return "jpg";
  else if(type == "OBJ")
    return "obj";
  return "";
}

int MeshImport::typeToTypeId(QString type) const
{
  if(type == "PLY")
    return 0;
  else if(type == "VTK Mesh")
    return 1;
  else if(type == "Text")
    return 2;
  else if(type == "Cells")
    return 3;
  else if(type == "Keyence")
    return 4;
  else if(type == "MeshEdit")
    return 5;
  else if(type == "OBJ")
    return 6;
  return 7;
}

QString MeshImport::extToType(QString ext) const
{
  if(ext == "ply")
    return "PLY";
  else if(ext == "vtu")
    return "VTK Mesh";
  else if(ext == "txt")
    return "Text";
  else if(ext == "jpg")
    return "Keyence";
  else if(ext == "mesh")
    return "MeshEdit";
  else if(ext == "obj")
    return "OBJ";
  return "From extension";
}

int MeshImport::extToTypeId(QString ext) const
{
  if(ext == "ply")
    return 0;
  else if(ext == "vtu")
    return 1;
  else if(ext == "txt")
    return 2;
  else if(ext == "jpg")
    return 4;
  else if(ext == "mesh")
    return 5;
  else if(ext == "obj")
    return 6;
  return 7;
}

QString MeshImport::properFile(QString filename, const QString& type) const
{
  QFileInfo fi(filename);
  QString suf = fi.suffix();
  QString ext = typeToExt(type);
  if(ext.isEmpty())
    return filename;
  if(!suf.isEmpty())
    filename = filename.left(filename.size() - suf.size());
  return filename + ext;
}

void MeshImport::setMeshFile(const QString& filename, const QString& type)
{
  if(type == "From extension") {
    ui->MeshFile->setText(filename);
    ui->MeshType->setCurrentIndex(7);
    return;
  }
  QFileInfo fi(filename);
  int tid = 0;
  // Note: these ids need to match those in importmesh.ui
  if(type.isEmpty()) {
    QString suf = fi.suffix();
    tid = extToTypeId(suf);
    ui->MeshFile->setText(filename);
  } else {
    tid = typeToTypeId(type);
    ui->MeshFile->setText(properFile(filename, type));
  }
  ui->MeshType->setCurrentIndex(tid);
}

// REGISTER_MESH_PROCESS(MeshImport);

bool MeshLoad::initialize(QStringList& parms, QWidget* parent)
{
  int meshId = parms[3].toInt();
  if(!checkState().mesh(MESH_ANY, meshId))
    return false;

  bool choose_file = stringToBool(parms[4]);
  parms[4] = "Yes";
  QString filename = parms[0];
  if(choose_file or filename.isEmpty()) {
    QDialog dlg(parent);
    Ui_LoadMeshDialog ui;
    ui.setupUi(&dlg);

    this->ui = &ui;
    this->dlg = &dlg;

    QObject::connect(ui.SelectMeshFile, &QAbstractButton::clicked, this, &MeshLoad::selectMeshFile);

    Mesh* mesh = this->mesh(meshId);
    if(not mesh->file().isEmpty())
      filename = mesh->file();

    setMeshFile(filename);
    ui.Transform->setChecked(stringToBool(parms[1]));
    ui.Add->setChecked(stringToBool(parms[2]));

    bool res = false;
    if(dlg.exec() == QDialog::Accepted) {
      parms[0] = ui.MeshFile->text();
      parms[1] = (ui.Transform->isChecked() ? "yes" : "no");
      parms[2] = (ui.Add->isChecked() ? "yes" : "no");
      res = true;
    }
    this->ui = 0;
    this->dlg = 0;
    return res;
  }
  return true;
}

void MeshLoad::selectMeshFile()
{
  QString filename = ui->MeshFile->text();
  filename = QFileDialog::getOpenFileName(dlg, QString("Select mesh file"), filename,
                                          "Mesh files (*.mgxm);;All files (*.*)", 0, FileDialogOptions);
  if(!filename.isEmpty()) {
    if(!QFile::exists(filename)) {
      QMessageBox::information(dlg, "Error selecting file",
                               "You must select an existing file. Your selection is ignored.");
      return;
    }
    setMeshFile(filename);
  }
}

void MeshLoad::setMeshFile(const QString& filename) {
  ui->MeshFile->setText(filename);
}

bool MeshLoad::operator()(const QStringList& parms)
{
  int meshId = parms[3].toInt();
  if(!checkState().mesh(MESH_ANY, meshId))
    return false;
  Mesh* mesh = this->mesh(meshId);
  bool transform = stringToBool(parms[1]);
  bool add = stringToBool(parms[2]);
  bool res = (*this)(mesh, parms[0], transform, add);
  if(res) {
    // Show mesh if nothing visible
    if(!(mesh->isMeshVisible() or mesh->isSurfaceVisible())) {
      mesh->showSurface();
      mesh->showMesh();
    }
    if(mesh->hasImgTex())
      mesh->showImage();
    else
      mesh->showSignal();
    mesh->setNormals();
  }
  return res;
}

bool MeshLoad::loadMGXM_1_3(QIODevice& file, Mesh* mesh, bool& scale, bool& transform, bool add)
{
  return loadMGXM_1_2(file, mesh, scale, transform, add, false);
}

bool MeshLoad::loadMGXM_1_2(QIODevice& file, Mesh* mesh, bool& scale, bool& transform, bool add, bool has_color)
{
  bool is_cells;
  file.read((char*)&is_cells, sizeof(bool));
  mesh->setCells(is_cells);
  return loadMGXM_1_1(file, mesh, scale, transform, add, has_color);
}

bool MeshLoad::loadMGXM_1_1(QIODevice& file, Mesh* mesh, bool& scale, bool& transform, bool add, bool has_color)
{
  file.read((char*)&scale, sizeof(bool));
  file.read((char*)&transform, sizeof(bool));
  Point2f signalBounds;
  file.read((char*)&signalBounds[0], sizeof(float));
  file.read((char*)&signalBounds[1], sizeof(float));
  if(signalBounds[0] == signalBounds[1]) {
    signalBounds[0] = 0;
    signalBounds[1] = 65535;
  }
  mesh->signalBounds() = signalBounds;
  uint stringSize;
  file.read((char*)&stringSize, sizeof(uint));
  QByteArray string(stringSize, 0);
  file.read(string.data(), stringSize);
  mesh->signalUnit() = QString::fromUtf8(string);
  bool res = loadMGXM_0(file, mesh, scale, transform, add, has_color);
  if(res and has_color) {
    float deltaSignal = signalBounds[1] - signalBounds[0];
    forall(const vertex& v, mesh->graph())
      v->signal = signalBounds[0] + v->signal * deltaSignal;
  }
  return res;
}

bool MeshLoad::loadMGXM_1_0(QIODevice& file, Mesh* mesh, bool& scale, bool& transform, bool add, bool )
{
  file.read((char*)&scale, sizeof(bool));
  file.read((char*)&transform, sizeof(bool));
  return loadMGXM_0(file, mesh, scale, transform, add);
}

bool MeshLoad::loadMGXM_0(QIODevice& file, Mesh* mesh, bool scale, bool transform, bool add, bool has_color)
{
  vvgraph& S = mesh->graph();
  vertex_map vmap;

  if(!S.empty() and !add)
    mesh->reset();

  uint hSz, vCnt, vSz, eSz;
  file.read((char*)&hSz, 4);
  file.read((char*)&vCnt, 4);
  file.read((char*)&vSz, 4);
  file.read((char*)&eSz, 4);
  bool has_label = vSz >= 4;
  if(has_label)
    vSz -= 4;
  if(has_color) {
    has_color = vSz >= 4;
    if(has_color)
      vSz -= 4;
  }
  bool has_signal = vSz >= 4;
  if(has_signal)
    vSz -= 4;
  bool has_type = vSz >= 1;
  if(has_type)
    vSz -= 1;
  bool has_selected = vSz >= 1;
  if(has_selected)
    vSz -= 1;

  if(hSz > 0) {
    std::vector<char> t(hSz);
    file.read(t.data(), hSz);
  }
  float tmp;

  Progress progress(QString("Loading Mesh %1 from File '%2'").arg(mesh->userId()).arg(mesh->file()), vCnt * 2);
  // Progress progress(QString("Loading Mesh-%1 - %2").arg(mesh->userId()).arg(mesh->file()), vCnt * 2);
  int interval = vCnt / 100;
  if(interval < 1)
    interval = 1;
  S.reserve(vCnt);
  for(uint i = 0; i < vCnt; i++) {
    if(file.atEnd())
      throw(QString("Premature end of file reading vertices"));
    vertex v;
    S.insert(v);
    file.read((char*)&v->saveId, 4);
    file.read((char*)&v->pos, 12);
    v->pos = realPos(v->pos, scale, transform, mesh);
    if(has_label)
      file.read((char*)&v->label, 4);
    if(has_color or has_signal)
      file.read((char*)&v->signal, 4);
    if(has_color and has_signal)
      file.read((char*)&tmp, 4);
    if(has_type)
      file.read((char*)&v->type, 1);
    if(has_selected)
      file.read((char*)&v->selected, 1);
    if(vSz > 0) {
      std::vector<char> t(vSz);
      file.read(t.data(), vSz);
    }
    vmap[v->saveId] = v;
    if(v->label > mesh->viewLabel())
      mesh->setLabel(v->label);
    if((i % interval == 0)and !progress.advance(i))
      userCancel();
  }
  for(uint i = 0; i < vCnt; i++) {
    if(file.atEnd())
      throw(QString("Premature end of file reading neighborhoods"));
    uint vId, nCnt;
    file.read((char*)&vId, 4);
    file.read((char*)&nCnt, 4);
    if(eSz > 0) {
      std::vector<char> t(eSz);
      file.read(t.data(), eSz);
    }
    vertex_map::const_iterator vit = vmap.find(vId);
    if(vit == vmap.end())
      throw QString("Invalid vertex id: %1").arg(vId);
    vertex v = vit->second;
    vertex pn(0);
    for(uint j = 0; j < nCnt; j++) {
      uint nId;
      file.read((char*)&nId, 4);
      vertex_map::const_iterator nit = vmap.find(nId);
      if(vit == vmap.end())
        throw QString("Invalid vertex id: %1").arg(nId);
      vertex n = nit->second;
      if(S.valence(v) == 0)
        S.insertEdge(v, n);
      else
        S.spliceAfter(v, pn, n);
      pn = n;
    }
    if((i % interval) == 0 and !progress.advance(vCnt + i))
      userCancel();
  }
  if(!progress.advance(2 * vCnt))
    userCancel();
  return true;
}

namespace {
// Returns true if the mesh is a cell mesh
bool checkCells(Mesh* mesh)
{
  const vvgraph& S = mesh->graph();
  forall(const vertex& v, S) {
    if(v->type == 'j') {
      char prev = S.prevTo(v, S.anyIn(v))->type;
      bool has_cell = false;
      forall(const vertex& n, S.neighbors(v)) {
        if(n->type == 'c') {
          if(prev == 'c')
            return false;
          has_cell = true;
        }
        prev = n->type;
      }
      if(not has_cell)
        return false;
    } else if(v->type == 'c') {
      forall(const vertex& n, S.neighbors(v))
        if(n->type != 'j')
          return false;
    } else
      return false;
  }
  return true;
}
} // namespace

void MeshLoad::findSignalBounds(Mesh* mesh)
{
  vvgraph& S = mesh->graph();
  float minSignal = FLT_MAX, maxSignal = -FLT_MAX;
  forall(const vertex& v, S) {
    float s = v->signal;
    if(s > maxSignal)
      maxSignal = s;
    if(s < minSignal)
      minSignal = s;
  }
  if(minSignal == maxSignal)
    minSignal -= 1;
  mesh->signalBounds() = Point2f(minSignal, maxSignal);
}

bool MeshLoad::operator()(Mesh* mesh, QString filename, bool transform, bool add)
{
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly)) {
    setErrorMessage(QString("loadMesh::Error:Cannot open input file: %1").arg(filename));
    return false;
  }

  char magic[9];
  char magic_check[9] = "MGX MESH";
  file.read(magic, 8);
  magic[8] = 0;
  bool scale;
  if(strcmp(magic, magic_check) == 0) {
    scale = false;
    if(!loadMGXM_0(file, mesh, scale, transform, add))
      return false;
    findSignalBounds(mesh);
  } else {
    file.seek(0);
    file.read(magic, 5);
    if(magic[4] == ' ')
      magic[4] = 0;
    if(strcmp(magic, "MGXM") == 0) {
      QList<int> version = extractVersion(file);
      if(version.size() != 2) {
        QStringList str;
        foreach(int i, version)
        str << QString::number(i);
        throw QString("MeshLoad::Error:Invalid file version format (%1), expected: MAJOR.MINOR")
              .arg(str.join("."));
      }
      bool success = false;
      switch(version[0]) {
      case 1:
        switch(version[1]) {
        case 0:
          success = loadMGXM_1_0(file, mesh, scale, transform, add);
          mesh->setCells(checkCells(mesh));
          findSignalBounds(mesh);
          break;
        case 1:
          success = loadMGXM_1_1(file, mesh, scale, transform, add);
          mesh->setCells(checkCells(mesh));
          break;
        case 2:
          success = loadMGXM_1_2(file, mesh, scale, transform, add);
          break;
        case 3:
          success = loadMGXM_1_3(file, mesh, scale, transform, add);
          break;
        default:
          throw QString(
                  "MeshLoad::Error:Unknown file version %1.%2, this software recognises up to version 1.0")
                .arg(version[0])
                .arg(version[1]);
        }
        break;
      default:
        throw QString("MeshLoad::Error:Unknown file version %1.%2, this software recognises up to version 1.0")
              .arg(version[0])
              .arg(version[1]);
      }
      if(!success)
        return false;
    } else {
      setErrorMessage(QString("The file '%1' is not a lgx mesh file").arg(filename));
      return false;
    }
  }
  file.close();
  actingFile(filename);
  mesh->setFile(filename);
  mesh->clearImgTex();
  if(mesh->stack()->empty())
    mesh->setScaled(scale);
  else
    mesh->setScaled(true);
  mesh->setTransformed(transform);
  mesh->updateAll();
  SETSTATUS("Loaded mesh, file:" << mesh->file() << ", vertices:" << mesh->graph().size());
  return true;
}

// REGISTER_MESH_PROCESS(MeshLoad);

bool LoadAllData::operator()(const QStringList&)
{
  if(!(*this)())
    return false;
  for(int i = 0; i < stackCount(); ++i) {
    Stack* s = stack(i);
    if(s->empty()) {
      s->main()->hide();
      s->work()->hide();
    }
  }
  for(int i = 0; i < meshCount(); ++i) {
    Mesh* m = mesh(i);
    if(m->graph().empty()) {
      m->hideSurface();
      m->hideMesh();
    } else {
      if(m->toShow() == Mesh::HEAT)
        m->showNormal();
    }
  }
  return true;
}

bool LoadAllData::loadStore(Stack* stack, Store* store, QStringList& errors)
{
  if(!store->file().isEmpty()) {
    if(store->file().endsWith(".txt", Qt::CaseInsensitive)) {
      StackImport importStack(*this);
      Point3f step;
      float brightness = 1.f;
      if(!importStack(stack, store, step, brightness, store->file())) {
        errors << importStack.errorMessage();
        return false;
      }
    } else {
      StackOpen openStack(*this);
      if(!openStack(stack, store, store->file())) {
        errors << openStack.errorMessage();
        return false;
      }
    }
  } else
    store->reset();
  return true;
}

bool LoadAllData::operator()()
{
  QStringList errors;
  for(int i = 0; i < stackCount(); ++i) {
    Stack* s = stack(i);
    Store* main = s->main();
    QString mainFilename = main->file();
    Store* work = s->work();
    QString workFilename = work->file();
    try {
      main->setFile(mainFilename);
      if(!loadStore(s, main, errors))
        main->reset();
    }
    catch(UserCancelException&) {
      main->reset();
      return true;
    }
    try {
      work->setFile(workFilename);
      if(!loadStore(s, work, errors))
        work->reset();
    }
    catch(UserCancelException&) {
      work->reset();
      return true;
    }
    if(main->file().isEmpty() and work->file().isEmpty()) {
      s->setSize(Point3u(0, 0, 0));
      main->reset();
      work->reset();
    }
  }
  MeshLoad loadMesh(*this);
  MeshImport importMesh(*this);
  for(int i = 0; i < meshCount(); ++i) {
    Mesh* m = mesh(i);
    if(!m->file().isEmpty()) {
      try {
        QString low_fn = m->file().toLower();
        if(low_fn.endsWith(".mgxm", Qt::CaseInsensitive)) {
          if(!loadMesh(m, m->file(), m->transformed(), false)) {
            m->reset();
            errors << loadMesh.errorMessage();
          }
        } else if(!importMesh(m, m->file(), "", m->scaled(), m->transformed(), false)) {
          m->reset();
          errors << importMesh.errorMessage();
        }
      }
      catch(UserCancelException&) {
        m->graph().clear();
        return true;
      }
    }
  }
  if(!errors.empty())
    setWarningMessage(QString("Warnings:\n  *%1").arg(errors.join("\n  *")));
  return true;
}

// REGISTER_GLOBAL_PROCESS(LoadAllData);

bool LoadProjectFile::operator()(const QStringList& parms) {
  return (*this)(parms[0]);
}

bool LoadProjectFile::operator()(QString filename)
{
  actingFile(filename, true);
  bool res = systemCommand(LOAD_VIEW, QStringList() << filename);
  if(!res) {
    setErrorMessage("Error while loading project file.");
    return false;
  }
  LoadAllData loaddata(*this);
  res = loaddata();
  if(!res)
    setErrorMessage(loaddata.errorMessage());
  return res;
}

bool LoadProjectFile::initialize(QStringList& parms, QWidget* parent)
{
  QString filename = parms[0];
  bool choose_file = stringToBool(parms[1]);
  parms[1] = "Yes";

  // Get the file name
  if(choose_file or filename.isEmpty()) {
    if(filename.isEmpty())
      filename = file();
    filename = QFileDialog::getOpenFileName(parent, QString("Select project file"), filename,
                                            "LithoGraphX Project files (*.lgxp);;MorphoGraphX View files (*.mgxv);;All files (*.*)", 0, FileDialogOptions);

    if(filename.isEmpty())
      return false;
  }

  parms[0] = filename;
  return true;
}

// REGISTER_GLOBAL_PROCESS(LoadProjectFile);

bool ResetMeshProcess::operator()(const QStringList& parms)
{
  int meshId = parms[0].toInt();
  if(!checkState().mesh(MESH_ANY, meshId))
    return false;
  return operator()(this->mesh(meshId));
}

bool ResetMeshProcess::operator()(Mesh* m)
{
  m->reset();
  return true;
}
} // namespace process
} // namespace lgx
