/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MISC_H
#define MISC_H
/**
 * \file Misc.hpp
 *
 * Misc. definitions and utilities
 */
#include <LGXConfig.hpp>
#include <GL.hpp>

#include <Color.hpp>
#include <EdgeData.hpp>
#include <LGXViewer/qglviewer.h>
#include <Vector.hpp>
#include <VertexData.hpp>
#include <VVGraph.hpp>

#include <QDir>
#include <QList>
#include <QtGui>

namespace lgx {

static const QString UM(QString::fromWCharArray(L"\xb5m"));             // um
static const QString UM2(QString::fromWCharArray(L"\xb5m\xb2"));        // um^2
static const QString UM3(QString::fromWCharArray(L"\xb5m\xb3"));        // um^3
static const QString UM_1(QString::fromWCharArray(L"\xb5m\x207B\xb9")); // um^-1
static const QString UM_2(QString::fromWCharArray(L"\xb5m\x207B\xb2")); // um^-2

#ifdef _MSC_VER
template class LGX_EXPORT graph::Vertex<VertexData, std::allocator<VertexData> >;
template class LGX_EXPORT graph::Edge<EdgeData>;
template class LGX_EXPORT graph::VVGraph<VertexData, EdgeData>;
#endif

// Type of the VV graph
typedef graph::VVGraph<VertexData, EdgeData> vvgraph;

// Type of a vertex
typedef vvgraph::vertex_t vertex;

// Type of an edge
typedef vvgraph::edge_t edge;

using qglviewer::Quaternion;
typedef util::Vector<2, int> Point2i;
typedef util::Vector<3, GLuint> Point3GLui;
typedef util::Vector<4, GLuint> Point4GLui;
typedef util::Vector<3, GLubyte> Point3GLub;
typedef util::Vector<4, GLubyte> Point4GLub;
typedef util::Color<float> Colorf;
typedef util::Vector<3, float> Point3f;

namespace util {
/// Returns the base dir for configuration
LGX_EXPORT QDir configurationDir(bool create=false);
/// Returns resource directory
LGX_EXPORT QDir resourcesDir();
/// Returns user processes directory
LGX_EXPORT QDir userProcessesDir(bool create=false);
/// Returns processes directories
LGX_EXPORT QList<QDir> processesDirs();
/// Returns the includes directory
LGX_EXPORT QDir includesDir();
/// Returns the libs directory
LGX_EXPORT QDir libsDir();
/// Returns the documentation directory
LGX_EXPORT QDir docsDir();
/// Return the macro directories
LGX_EXPORT QList<QDir> macroDirs();
/// Return the user macro directory
LGX_EXPORT QDir userMacroDir(bool create=false);

/**
 * Extract a Poin3f from a string.
 *
 * If the string is not a valid 3D point, returns NaN in all values of the
 * point.
 */
LGX_EXPORT Point3f stringToPoint3f(QString s);

/// Map unique colors to indices
inline Point3GLub vMapColor(uint u)
{
  u++;
  return (Point3GLub(u / (256 * 256), u / 256 % 256, u % 256));
}

/// Map unique colors to indices
inline uint vMapColor(Point3GLub& p) {
  return ((int(p.x()) * 256 * 256 + int(p.y()) * 256 + int(p.z())) - 1);
}

/// Create representation of a string that can be written in a single line, without spaces
inline QString shield(QString s)
{
  s.replace("\\", "\\\\");
  s.replace(" ", "\\s");
  s.replace("\n", "\\n");
  s.replace("\t", "\\t");
  s.replace("\"", "\\\"");
  s.replace("\'", "\\\'");
  return s;
}

/// Retrieve a string that has been shielded with shield
inline QString unshield(QString s)
{
  s.replace("\\\'", "\'");
  s.replace("\\\"", "\"");
  s.replace("\\s", " ");
  s.replace("\\n", "\n");
  s.replace("\\t", "\t");
  s.replace("\\\\", "\\");
  return s;
}

/// Shield a string to send it to the python interpreter
inline QString shield_python(QString s)
{
  s.replace("\\", "\\\\");
  s.replace("\n", "\\n");
  s.replace("\t", "\\t");
  s.replace("\"", "\\\"");
  s.replace("\'", "\\\'");
  return s;
}

/// Retrieve a string that is retrieved from the python representation
inline QString unshield_python(QString s)
{
  s.replace("\\\'", "\'");
  s.replace("\\\"", "\"");
  s.replace("\\n", "\n");
  s.replace("\\t", "\t");
  s.replace("\\\\", "\\");
  return s;
}

} // namespace util
} // namespace lgx
#endif
