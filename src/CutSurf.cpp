/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "CutSurf.hpp"

#include "ImageData.hpp"
#include "Information.hpp"
#include "Shader.hpp"
#include "Stack.hpp"
#include "Vector.hpp"

#ifndef GLAPIENTRY
#  define GLAPIENTRY
#endif

namespace lgx {
using Information::err;
using qglviewer::Vec;

typedef util::Vector<16, double> Point16d;

CutSurf::CutSurf()
  : QObject(0)
  , cut(0)
  , SceneRadius(-1)
{
}

CutSurf::~CutSurf() {
}

// Read cutting plane parameters
void CutSurf::readParms(util::Parms& parms, QString section)
{
  bool DrawCutSurf, CutSurfGrid;
  int Mode;
  float Brightness, Opacity;
  bool ShowPoints, Blending;
  Point3f Size;
  Point2i SurfSize;
  uint BezPoints;
  parms(section, "DrawCutSurf", DrawCutSurf, false);
  if(DrawCutSurf)
    cut->show();
  else
    cut->hide();
  parms(section, "Mode", Mode, (int)CuttingSurface::PLANE);
  cut->setMode((CuttingSurface::Mode)Mode);
  parms(section, "CutSurfGrid", CutSurfGrid, false);
  if(CutSurfGrid)
    cut->showGrid();
  else
    cut->hideGrid();
  parms(section, "Material", Material, 0u);
  parms(section, "Size", Size, Point3f(1.0f, 1.0f, 1.0f));
  cut->setSize(Size);
  parms(section, "ShowPoints", ShowPoints, false);
  cut->setShowPoints(ShowPoints);
  parms(section, "Blending", Blending, false);
  cut->setBlending(Blending);
  parms(section, "Brightness", Brightness, 1.0f);
  cut->setBrightness(Brightness);
  parms(section, "Opacity", Opacity, 1.0f);
  cut->setOpacity(Opacity);
  parms(section, "SurfSize", SurfSize, Point2i(15, 15));
  cut->setSurfSize(SurfSize);
  parms(section, "LineWidth", LineWidth, 1.0f);
  parms(section, "BezPoints", BezPoints, uint(5));
  if(BezPoints < 4)
    BezPoints = 4;
  cut->setBezPoints(BezPoints);

  Matrix4d m = 1;
  parms(section, "Frame", m, m);
  cut->frame().setFromMatrix(m.c_data());
  cut->hasChanged();

  initBez();
  std::vector<Point3f> bezierT;
  parms.all(section, "BezPointList", bezierT);
  cut->setBezierV(bezierT);
}

// Write cutting plane parameters
void CutSurf::writeParms(QTextStream& pout, QString section)
{
  pout << endl;
  pout << "[" << section << "]" << endl;
  pout << "DrawCutSurf: " << (cut->isVisible() ? "true" : "false") << endl;
  pout << "Mode: " << (int)cut->mode() << endl;
  pout << "CutSurfGrid: " << (cut->drawGrid() ? "true" : "false") << endl;
  pout << "Material: " << Material << endl;
  pout << "Size: " << cut->size() << endl;
  pout << "ShowPoints: " << (cut->showPoints() ? "true" : "false") << endl;
  pout << "Blending: " << (cut->blending() ? "true" : "false") << endl;
  pout << "Brightness: " << cut->brightness() << endl;
  pout << "Opacity: " << cut->opacity() << endl;
  pout << "SurfSize: " << cut->surfSize() << endl;
  pout << "LineWidth: " << LineWidth << endl;
  pout << "BezPoints: " << cut->bezPoints() << endl;
  pout << "Frame: " << Point16d(cut->frame().matrix()) << endl;
  const std::vector<Point3f>& bezierV = cut->bezierV();
  for(size_t i = 0; i < cut->bezPoints() * cut->bezPoints(); i++)
    pout << "BezPointList: " << bezierV[i] << endl;
}

void CutSurf::initBez()
{
  cut->initBez();
  selectV.clear();
}

// Draw cutting surface slot
void CutSurf::setVisible(bool val)
{
  if(val)
    cut->show();
  else
    cut->hide();
  emit modified();
}

// Three axis slot
void CutSurf::selectThreeAxis(bool val)
{
  if(val)
    cut->setMode(CuttingSurface::THREE_AXIS);
  emit modified();
}

// Draw surface grid
void CutSurf::showGrid(bool val)
{
  if(val)
    cut->showGrid();
  else
    cut->hideGrid();
  emit modified();
}

// Draw cutting plane slot
void CutSurf::selectPlane(bool val)
{
  if(val)
    cut->setMode(CuttingSurface::PLANE);
  emit modified();
}

// Draw cutting Bezier slot
void CutSurf::selectBezier(bool val)
{
  if(val)
    cut->setMode(CuttingSurface::BEZIER);
  emit modified();
}

void CutSurf::setBrightness(int val)
{
  float v = float(val) / 10000.0;
  if(cut->brightness() != v) {
    cut->setBrightness(v);
    emit modified();
  }
}

void CutSurf::setOpacity(int val)
{
  float v = float(val) / 10000.0;
  if(cut->opacity() != v) {
    Information::out << "New CurSurf opacity: " << v << endl;
    cut->setOpacity(v);
    emit modified();
  }
}

void CutSurf::setBlend(bool val)
{
  if(cut->blending() != val) {
    cut->setBlending(val);
    emit modified();
  }
}
void CutSurf::showPoints(bool val)
{
  if(cut->showPoints() != val) {
    cut->setShowPoints(val);
    emit modified();
  }
}

float CutSurf::getSize(int val) {
  return 0.5f * SceneRadius * (float)exp(double(val) / 2000.0);
}

// Set sizes:
void CutSurf::setSizeX(int val)
{
  Point3f s = cut->size();
  s.x() = getSize(val);
  cut->setSize(s);
  emit modified();
}

void CutSurf::setSizeY(int val)
{
  Point3f s = cut->size();
  s.y() = getSize(val);
  cut->setSize(s);
  emit modified();
}

void CutSurf::setSizeZ(int val)
{
  Point3f s = cut->size();
  s.z() = getSize(val);
  cut->setSize(s);
  emit modified();
}

void CutSurf::drawSurface(ImgData& stk, bool select)
{
  std::vector<Point3f> points;
  std::vector<Point3f> normals;
  std::vector<Point3f> coords;

  switch(cut->mode()) {
  case CuttingSurface::PLANE:
  case CuttingSurface::THREE_AXIS: {
    // Draw surface with texture
    int axis = (cut->threeAxis() ? 3 : 1);

    const Point3f& Size = cut->size();

    points.reserve(4*axis);
    normals.reserve(4*axis);
    coords.reserve(4*axis);

    for(int i = 0; i < axis; i++) {
      Point3f x(0, 0, 0), y(0, 0, 0), z(0, 0, 0);
      x[i] = Size[i];
      y[(i + 1) % 3] = Size[(i + 1) % 3];
      z[(i + 2) % 3] = Size[(i + 2) % 3];

      Point3f tr = x + y;
      Point3f tl = -x + y;
      Point3f bl = -x - y;
      Point3f br = x - y;

      normals.push_back(z);
      coords.push_back(Point3f(stk.getFrame().coordinatesOf(cut->frame().inverseCoordinatesOf(Vec(tr)))));
      points.push_back(tr);

      normals.push_back(z);
      coords.push_back(Point3f(stk.getFrame().coordinatesOf(cut->frame().inverseCoordinatesOf(Vec(tl)))));
      points.push_back(tl);

      normals.push_back(z);
      coords.push_back(Point3f(stk.getFrame().coordinatesOf(cut->frame().inverseCoordinatesOf(Vec(bl)))));
      points.push_back(bl);

      normals.push_back(z);
      coords.push_back(Point3f(stk.getFrame().coordinatesOf(cut->frame().inverseCoordinatesOf(Vec(br)))));
      points.push_back(br);
    }

    opengl->glEnableClientState(GL_NORMAL_ARRAY);
    opengl->glNormalPointer(GL_FLOAT, 0, normals.data());
    if(select) {
      opengl->glEnableClientState(GL_COLOR_ARRAY);
      opengl->glColorPointer(3, GL_FLOAT, 0, coords.data());
    } else {
      opengl->glEnableClientState(GL_TEXTURE_COORD_ARRAY);
      opengl->glTexCoordPointer(3, GL_FLOAT, 0, coords.data());
    }
    opengl->glEnableClientState(GL_VERTEX_ARRAY);
    opengl->glVertexPointer(3, GL_FLOAT, 0, points.data());
    opengl->glDrawArrays(GL_QUADS, 0, points.size());
    if(select)
      opengl->glDisableClientState(GL_COLOR_ARRAY);
    else
      opengl->glDisableClientState(GL_COLOR_ARRAY);
    opengl->glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    opengl->glDisableClientState(GL_NORMAL_ARRAY); // That doesn't hurt!

  } break;
  case CuttingSurface::BEZIER: {
    const Point2i& SurfSize = cut->surfSize();
    int uSize = SurfSize.x(), vSize = SurfSize.y();

    auto to_reserve = 2*(uSize-1)*(vSize-1);
    points.reserve(to_reserve);
    coords.reserve(to_reserve);

    for(int u = 0; u < uSize - 1; u++) {
      for(int v = 0; v < vSize - 1; v++) {
        Point3f v0 = cut->evalCoord(float(u) / float(uSize - 1), float(v) / float(vSize - 1));
        Point3f v1 = cut->evalCoord(float(u + 1) / float(uSize - 1), float(v) / float(vSize - 1));

        if(select) {
          // For select we'll use the tex coords for the color
          Point3f col;
          col = (Point3f(stk.getFrame().coordinatesOf(cut->frame().inverseCoordinatesOf(Vec(v0))))
                 + Point3f(.5f, .5f, .5f));
          coords.push_back(col);
          points.push_back(v0);

          col = (Point3f(stk.getFrame().coordinatesOf(cut->frame().inverseCoordinatesOf(Vec(v1))))
                 + Point3f(.5f, .5f, .5f));
          coords.push_back(col);
          points.push_back(v1);
        } else {
          Point3d tex = Point3f(stk.getFrame().coordinatesOf(cut->frame().inverseCoordinatesOf(Vec(v0))));
          coords.push_back(tex);
          points.push_back(v0);

          tex = Point3f(stk.getFrame().coordinatesOf(cut->frame().inverseCoordinatesOf(Vec(v1))));
          coords.push_back(tex);
          points.push_back(v1);
        }
      }
    }
    opengl->glEnable(GL_AUTO_NORMAL);
    if(select) {
      opengl->glEnableClientState(GL_COLOR_ARRAY);
      opengl->glColorPointer(3, GL_FLOAT, 0, coords.data());
    } else {
      opengl->glEnableClientState(GL_TEXTURE_COORD_ARRAY);
      opengl->glTexCoordPointer(3, GL_FLOAT, 0, coords.data());
    }
    opengl->glEnableClientState(GL_VERTEX_ARRAY);
    opengl->glVertexPointer(3, GL_FLOAT, 0, points.data());
    auto size = 2*(vSize-1);
    for(int u = 0 ; u < uSize - 1 ; ++u)
      opengl->glDrawArrays(GL_TRIANGLE_STRIP, u*size, size);
    opengl->glDisableClientState(GL_VERTEX_ARRAY);
    if(select)
      opengl->glDisableClientState(GL_COLOR_ARRAY);
    else
      opengl->glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  }
  }

}

// Draw cutting plane
void CutSurf::drawCutSurf(ImgData& stk, bool select, Shader* shader)
{
  if(!cut->isVisible() or !stk.isVisible())
    return;

  opengl->glDisable(GL_BLEND);
  opengl->glDisable(GL_TEXTURE_1D);
  opengl->glDisable(GL_TEXTURE_2D);
  opengl->glDisable(GL_TEXTURE_3D);
  opengl->glDisable(GL_CULL_FACE);
  opengl->glEnable(GL_DEPTH_TEST);
  opengl->glDisable(GL_LIGHTING);

  opengl->glMatrixMode(GL_MODELVIEW);
  opengl->glPushMatrix();
  opengl->glMultMatrixd(cut->frame().matrix());

  if(select) {
    opengl->glRenderMode(GL_RENDER);
    opengl->glDrawBuffer(GL_AUX0);
    opengl->glClearColor(0, 0, 0, 1.0);
    opengl->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    drawSurface(stk, true);
  } else if(stk.isVisible()) {

    opengl->glEnable(GL_LIGHTING);
    //opengl->glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    opengl->glFrontFace(GL_CCW);
    opengl->glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    stk.setup3DRenderingData(shader);

    shader->setUniform("brightness", GLSLValue(powf(cut->brightness(), 2.0f)));
    shader->setUniform("opacity", GLSLValue(cut->opacity()));
    shader->setUniform("second_brightness", GLSLValue(powf(cut->brightness(), 2.0f)));
    shader->setUniform("second_opacity", GLSLValue(cut->opacity()));
    shader->setUniform("blending", GLSLValue(cut->blending()));

    // First, draw main
    shader->useShaders();
    shader->setupUniforms();
    drawSurface(stk, false);
    shader->stopUsingShaders();

    stk.unbind3DTex();
  }

  if(select)
    opengl->glDrawBuffer(GL_BACK);
  opengl->glPopMatrix();
}

void CutSurf::drawCutSurfGrid(ImgData& stk)
{
  if(!cut->drawGrid())
    return;
  opengl->glMatrixMode(GL_MODELVIEW);
  opengl->glPushMatrix();
  opengl->glMultMatrixd(cut->frame().matrix());
  opengl->glDisable(GL_LIGHTING);
  opengl->glDisable(GL_TEXTURE_3D);
  opengl->glDisable(GL_CULL_FACE);

  opengl->glColor4fv(Colors::getColor(Colors::CuttingPlaneGridColor).c_data());

  std::vector<Point3f> points;

  // Draw the grid
  switch(cut->mode()) {
  case CuttingSurface::PLANE:
  case CuttingSurface::THREE_AXIS: {
    const Point3f& Size = cut->size();
    int axis = (cut->threeAxis() ? 3 : 1);
    points.reserve(4*axis);
    for(int i = 0; i < axis; i++) {
      Point3f x(0, 0, 0), y(0, 0, 0), z(0, 0, 0);
      x[i] = Size[i];
      y[(i + 1) % 3] = Size[(i + 1) % 3];
      z[(i + 2) % 3] = Size[(i + 2) % 3];

      //opengl->glNormal3fv(z.c_data());
      Point3f tr = x + y;
      Point3f tl = -x + y;
      Point3f bl = -x - y;
      Point3f br = x - y;

      points.push_back(tl);
      points.push_back(bl);
      points.push_back(br);
      points.push_back(tr);
    }
  } break;
  case CuttingSurface::BEZIER: {
    int uSize = cut->surfSize().x(), vSize = cut->surfSize().y();
    points.reserve(4*(vSize-1)*(uSize-1));
    for(int u = 0; u < uSize - 1; u++) {
      for(int v = 0; v < vSize - 1; v++) {
        Point3f tl = cut->evalCoord(float(u) / float(uSize - 1), float(v) / float(vSize - 1));
        Point3f bl = cut->evalCoord(float(u) / float(uSize - 1), float(v + 1) / float(vSize - 1));
        Point3f br = cut->evalCoord(float(u + 1) / float(uSize - 1), float(v + 1) / float(vSize - 1));
        Point3f tr = cut->evalCoord(float(u + 1) / float(uSize - 1), float(v) / float(vSize - 1));

        points.push_back(tl);
        points.push_back(bl);
        points.push_back(br);
        points.push_back(tr);
      }
    }
  }
  }

  opengl->glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  opengl->glLineWidth(LineWidth);
  opengl->glEnableClientState(GL_VERTEX_ARRAY);
  opengl->glVertexPointer(3, GL_FLOAT, 0, points.data());
  opengl->glDrawArrays(GL_QUADS, 0, points.size());
  opengl->glDisableClientState(GL_VERTEX_ARRAY);

  // Draw the points if required
  if(cut->bezier() and cut->showPoints() and cut->bezPoints()) {
    Color4f meshcol = Colors::getColor(stk.MeshColor);
    Color4f selectcol = Colors::getColor(stk.MeshSelectColor);

    points.clear();
    std::vector<Color4f> colors;
    auto to_reserve = cut->bezPoints()*cut->bezPoints();
    points.reserve(to_reserve);
    colors.reserve(to_reserve);

    for(uint v = 0; v < cut->bezPoints(); v++)
      for(uint u = 0; u < cut->bezPoints(); u++) {
        if(selectV.find(cut->idx(u, v)) == selectV.end())
          colors.push_back(meshcol);
        else
          colors.push_back(selectcol);
        points.push_back(cut->bezierV(u, v));
      }

    opengl->glPointSize(ImgData::MeshPointSize);
    opengl->glEnableClientState(GL_VERTEX_ARRAY);
    opengl->glEnableClientState(GL_COLOR_ARRAY);
    opengl->glColorPointer(4, GL_FLOAT, 0, colors.data());
    opengl->glVertexPointer(3, GL_FLOAT, 0, points.data());
    opengl->glDrawArrays(GL_POINTS, 0, points.size());
    opengl->glDisableClientState(GL_VERTEX_ARRAY);
    opengl->glDisableClientState(GL_COLOR_ARRAY);
  }
  opengl->glPopMatrix();
}

void CutSurf::drawSelect()
{
  if(!cut->drawGrid() or !cut->bezier() or !ImgData::meshSelect() or !cut->showPoints())
    return;

  // For Bezier draw the control points
  opengl->glDisable(GL_LIGHTING);
  opengl->glDisable(GL_TEXTURE_1D);
  opengl->glDisable(GL_TEXTURE_2D);
  opengl->glDisable(GL_TEXTURE_3D);
  opengl->glDisable(GL_CULL_FACE);
  opengl->glDisable(GL_DEPTH_TEST);

  opengl->glMatrixMode(GL_MODELVIEW);
  opengl->glPushMatrix();
  opengl->glMultMatrixd(cut->frame().matrix());

  opengl->glRenderMode(GL_RENDER);
  opengl->glDrawBuffer(GL_AUX0);
  opengl->glClearColor(0.0, 0.0, 0.0, 1.0);
  opengl->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  opengl->glPointSize(ImgData::MeshPointSize * 2);

  opengl->glBegin(GL_POINTS);
  for(uint v = 0; v < cut->bezPoints(); v++)
    for(uint u = 0; u < cut->bezPoints(); u++) {
      opengl->glColor3ubv(util::vMapColor(cut->idx(u, v)).c_data());
      opengl->glVertex3fv(cut->bezierV(u, v).c_data());
    }
  opengl->glEnd();

  opengl->glDrawBuffer(GL_BACK);
  opengl->glPopMatrix();
}

// Clear list of selected points
void CutSurf::clearSelect() {
  selectV.clear();
}

// Find selected point in surface
int CutSurf::findSelectPoint(uint x, uint y)
{
  // Draw unique colors in tha BACK buffer
  drawSelect();

  // Find the color of the pixels drawn in AUX0 buffer
  Point3GLub pix;
  GLint viewport[4];
  opengl->glGetIntegerv(GL_VIEWPORT, viewport);
  opengl->glReadBuffer(GL_AUX0);
  opengl->glReadPixels(x, viewport[3] - y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, &pix);
  opengl->glReadBuffer(GL_FRONT);
  int i = util::vMapColor(pix);
  if(i >= int(cut->bezPoints() * cut->bezPoints()))
    return -1;
  return (i);
}

bool CutSurf::showOpaqueSurface(const ImgData& stk)
{
  return stk.isVisible() and cut->isVisible() and not cut->blending();
}

bool CutSurf::showTransparentSurface(const ImgData& stk)
{
  return stk.isVisible() and cut->isVisible() and cut->blending();
}

void CutSurf::reset(double sceneRadius)
{
  SceneRadius = sceneRadius;
  cut->setSize(Point3f(SceneRadius, SceneRadius, SceneRadius));

  if(SceneRadius > 0.0f) {
    switch(cut->mode()) {
    case CuttingSurface::BEZIER: {
      initBez();
      forall(Point3f& p, cut->bezierV()) {
        p *= SceneRadius;
      }
    }
    case CuttingSurface::PLANE:
    case CuttingSurface::THREE_AXIS:
      break;
    }
  }
  emit modified();
}

void CutSurf::setSceneBoundingBox(const Point3f& bbox)
{
  if(SceneRadius <= 0)
    SceneRadius = norm(bbox);
}
} // namespace lgx
