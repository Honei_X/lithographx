/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "TransferFunction.hpp"

#include <QTextStream>
#include <QStringList>
#include <algorithm>

#include <QTextStream>
#include <stdio.h>

namespace lgx {
const double TransferFunction::epsilon = 1e-6;

TransferFunction::TransferFunction(Interpolation i)
  : _interpolation(i)
  , clamp(true)
  , exteriorColor(0, 0, 0, 0)
{
}

TransferFunction::TransferFunction(const TransferFunction& copy)
  : _interpolation(copy._interpolation)
  , values(copy.values)
  , keys(copy.keys)
  , clamp(copy.clamp)
  , exteriorColor(copy.exteriorColor)
{
}

TransferFunction TransferFunction::load(QString spec)
{
  TransferFunction fct;
  QStringList all = spec.split("\n");
  for(int line_number = 0; line_number < all.size(); ++line_number) {
    QString line = all[line_number].trimmed();
    if(line.isEmpty() or line[0] == '#')
      continue;
    QStringList fields = line.split(":");
    QString header = fields[0].trimmed();
    fields.pop_front();
    QString value = fields.join(":").trimmed().toLower();
    if(header == "Interpolation") {
      if(value == "rgb")
        fct._interpolation = RGB;
      else if(value == "hsv")
        fct._interpolation = HSV;
      else if(value == "cyclic_hsv")
        fct._interpolation = CYCLIC_HSV;
    } else if(header == "Clamp") {
      fct.clamp = value == "true";
    } else if(header == "ExteriorColor") {
      QStringList cs = value.split(",");
      Colorf col;
      for(int i = 0; i < 4; ++i)
        col[i] = cs[i].toFloat();
      fct.exteriorColor = col;
    } else if(header == "ColoredPos") {
      QStringList vs = value.split("-");
      double pos = vs[0].toDouble();
      vs.pop_front();
      QString cols = vs.join("-");
      Colorf col;
      QStringList cs = cols.split(",");
      for(int i = 0; i < 4; ++i)
        col[i] = cs[i].toFloat();
      fct.values.push_back(std::make_pair(pos, col));
    }
  }
  fct.clamp = true;
  if(fct.size() < 2)
    fct.clear();
  else
    fct.update_keys();
  return fct;
}

QString TransferFunction::dump() const
{
  QString result;
  QTextStream ts(&result, QIODevice::WriteOnly);
  ts << "Interpolation: ";
  switch(_interpolation) {
  case RGB:
    ts << "rgb";
    break;
  case HSV:
    ts << "hsv";
    break;
  case CYCLIC_HSV:
    ts << "cyclic_hsv";
    break;
  }
  ts << endl;
  ts << "Clamp: " << (clamp ? "true" : "false") << endl;
  ts << "ExteriorColor: "
     << QString("%1,%2,%3,%4").arg(exteriorColor[0]).arg(exteriorColor[1]).arg(exteriorColor[2]).arg(exteriorColor[3])
     << endl;
  for(size_t i = 0; i < values.size(); ++i) {
    Colorf col = values[i].second;
    if(_interpolation == RGB) {
      if(col[0] < 0)
        col[0] = 0;
      else if(col[0] > 1)
        col[0] = 1;
    } else {
      if(col[0] < 0)
        col[0] += 360;
      else if(col[0] >= 360)
        col[0] -= 360;
    }
    for(int j = 1; j < 4; ++j) {
      if(col[j] < 0)
        col[j] = 0;
      else if(col[j] > 1)
        col[j] = 1;
    }
    ts << "ColoredPos: " << values[i].first << "-"
       << QString("%1,%2,%3,%4").arg(values[i].second[0]).arg(values[i].second[1]).arg(values[i].second[2]).arg(
      values[i].second[3]) << endl;
  }
  return result;
}

template <typename T1, typename T2> struct ComparePairFirst {
  bool operator()(const std::pair<T1, T2>& p1, const std::pair<T1, T2>& p2) {
    return p1.first < p2.first;
  }
};

void TransferFunction::update_keys()
{
  std::sort(values.begin(), values.end(), ComparePairFirst<double, Colorf>());
  keys.clear();
  for(size_t i = 0; i < values.size(); ++i) {
    keys[values[i].first] = i;
  }
}

void TransferFunction::adjust(double minValue, double maxValue)
{
  if(values.size() < 2)
    return;
  double lowest = values.front().first;
  double highest = values.back().first;
  double ratio = (maxValue - minValue) / (highest - lowest);
  for(size_t i = 0; i < values.size(); ++i) {
    values[i].first = (values[i].first - lowest) * ratio + minValue;
  }
  update_keys();
}

TransferFunction::Colorf TransferFunction::interpolate(double position, double p1, Colorf col1, double p2,
                                                       Colorf col2) const
{
  double delta = p2 - p1;
  double dp1 = (p2 - position) / delta;
  double dp2 = 1.0 - dp1;
  if(_interpolation == CYCLIC_HSV) {
    if(col2[0] - col1[0] > 180)
      col1[0] += 360;
    else if(col1[0] - col2[0] > 180)
      col2[0] += 360;
  }
  Colorf col = dp1 * col1 + dp2 * col2;
  if(_interpolation == CYCLIC_HSV) {
    if(col[0] < 0)
      col[0] += 360;
    else if(col[0] > 360)
      col[0] -= 360;
  }
  return col;
}

void TransferFunction::add_rgba_point(double pos, Colorf col)
{
  if(_interpolation != RGB)
    col = convertRGBtoHSV(col);
  add_point(pos, col);
}

void TransferFunction::add_point(double pos, Colorf col)
{
  key_map::iterator it_found = keys.find(pos);
  if(it_found != keys.end()) {
    values[it_found.value()] = std::make_pair(pos, col);
  } else {
    values.push_back(std::make_pair(pos, col));
    update_keys();
  }
}

void TransferFunction::add_hsva_point(double pos, Colorf col)
{
  if(_interpolation == RGB)
    col = convertHSVtoRGB(col);
  add_point(pos, col);
}

void TransferFunction::remove_point(double pos)
{
  key_map::iterator it_found = keys.find(pos);
  if(it_found != keys.end()) {
    values.erase(values.begin() + it_found.value());
    keys.erase(it_found);
  }
}

TransferFunction::Colorf TransferFunction::rgba(double pos) const
{
  Colorf col = color(pos);
  if(_interpolation != RGB)
    return convertHSVtoRGB(col);
  return col;
}

TransferFunction::Colorf TransferFunction::color(double pos) const
{
  Colorf prev_col;
  double prev_pos = -1;
  for(size_t i = 0; i < size(); ++i) {
    const double& p = values[i].first;
    const Colorf& col = values[i].second;
    if((p == 0 and fabs(pos) < epsilon)or (fabs(pos - p) / fabs(pos + p) < epsilon)) {
      return col;
    }
    if(pos < p) {
      if(prev_pos < 0) {
        if(clamp)
          return col;
        else
          return exteriorColor;
      }
      return interpolate(pos, prev_pos, prev_col, p, col);
    }
    prev_col = col;
    prev_pos = p;
  }
  if(clamp)
    return prev_col;
  else
    return exteriorColor;
}

double TransferFunction::alpha(double pos) const
{
  Colorf prev_col;
  double prev_pos = -1;
  for(size_t i = 0; i < size(); ++i) {
    const double& p = values[i].first;
    const Colorf& col = values[i].second;
    if((p == 0 and fabs(pos) < epsilon)or (fabs(pos - p) / fabs(pos + p) < epsilon)) {
      return col.a();
    }
    if(pos < p) {
      if(prev_pos < 0) {
        if(clamp)
          return col.a();
        else
          return exteriorColor.a();
      }
      double delta = p - prev_pos;
      double dp1 = fabs((pos - prev_pos) / delta);
      double dp2 = fabs((pos - p) / delta);
      return dp1 * col.a() + dp2 * prev_col.a();
    }
    prev_col = col;
    prev_pos = p;
  }
  if(clamp)
    return prev_col.a();
  else
    return exteriorColor.a();
}

TransferFunction::Colorf TransferFunction::hsva(double pos) const
{
  Colorf col = color(pos);
  if(_interpolation != RGB)
    return col;
  return convertRGBtoHSV(col);
}

void TransferFunction::setInterpolation(Interpolation i)
{
  if(i != _interpolation) {
    if(i * _interpolation == 0)     // Change from HSV to RGB
    {
      if(i == RGB) {
        for(size_t i = 0; i < size(); ++i)
          values[i].second = convertHSVtoRGB(values[i].second);
      } else {
        for(size_t i = 0; i < size(); ++i)
          values[i].second = convertRGBtoHSV(values[i].second);
      }
    }
    _interpolation = i;
  }
}

void TransferFunction::reverse()
{
  for(size_t i = 0; i < values.size(); ++i)
    values[i].first = 1 - values[i].first;
  update_keys();
}

void TransferFunction::addOpacityScale()
{
  double first = values.front().first;
  double last = values.back().first;
  double delta = last - first;
  size_t n = values.size();
  for(size_t i = 0 ; i < n ; ++i) {
    double rel_pos = values[i].first - first / delta;
    values[i].second.a() = rel_pos;
  }
}

void TransferFunction::makeOpaque()
{
  for(auto& value: values) {
    value.second.a() = 1.;
  }
}

void TransferFunction::clear()
{
  values.clear();
  keys.clear();
}

void TransferFunction::move_point(double old_pos, double new_pos)
{
  key_map::iterator it_found = keys.find(old_pos);
  if(it_found != keys.end()) {
    values[it_found.value()].first = new_pos;
    update_keys();
  }
}

double TransferFunction::next_pos(double old_pos) const
{
  key_map::const_iterator it_found = keys.find(old_pos);
  if(it_found != keys.end()) {
    int p = it_found.value();
    if(p < int(size() - 1))
      return values[p + 1].first;
  }
  return -1;
}

double TransferFunction::prev_pos(double old_pos) const
{
  key_map::const_iterator it_found = keys.find(old_pos);
  if(it_found != keys.end()) {
    int p = it_found.value();
    if(p > 0)
      return values[p - 1].first;
  }
  return -1;
}

TransferFunction TransferFunction::scale(Colorf min, Colorf max, Interpolation interpolation)
{
  TransferFunction fct(interpolation);
  if(interpolation == RGB) {
    fct.add_rgba_point(0, min);
    fct.add_rgba_point(1, max);
  } else {
    fct.add_hsva_point(0, min);
    fct.add_hsva_point(1, max);
  }
  return fct;
}

TransferFunction TransferFunction::hue_scale()
{
  TransferFunction fct(CYCLIC_HSV);
  fct.add_hsva_point(0, Colorf(0, 1, 1, 0));
  fct.add_hsva_point(0.3, Colorf(0.3 * 360, 1, 1, 0.3));
  fct.add_hsva_point(0.7, Colorf(0.7 * 360, 1, 1, 0.7));
  fct.add_hsva_point(1, Colorf(0, 1, 1, 1));
  return fct;
}

TransferFunction TransferFunction::french_flag()
{
  TransferFunction fct(RGB);
  fct.add_rgba_point(0, Colorf(0, 0, 1, 1));
  fct.add_rgba_point(0.5, Colorf(1, 1, 1, 0));
  fct.add_rgba_point(1, Colorf(1, 0, 0, 1));
  return fct;
}

TransferFunction TransferFunction::viridis()
{
  std::vector<Colorf> data =
  {{ 0.26700401,  0.00487433,  0.32941519, 1.},
    { 0.26851048,  0.00960483,  0.33542652, 1.},
    { 0.26994384,  0.01462494,  0.34137895, 1.},
    { 0.27130489,  0.01994186,  0.34726862, 1.},
    { 0.27259384,  0.02556309,  0.35309303, 1.},
    { 0.27380934,  0.03149748,  0.35885256, 1.},
    { 0.27495242,  0.03775181,  0.36454323, 1.},
    { 0.27602238,  0.04416723,  0.37016418, 1.},
    { 0.2770184 ,  0.05034437,  0.37571452, 1.},
    { 0.27794143,  0.05632444,  0.38119074, 1.},
    { 0.27879067,  0.06214536,  0.38659204, 1.},
    { 0.2795655 ,  0.06783587,  0.39191723, 1.},
    { 0.28026658,  0.07341724,  0.39716349, 1.},
    { 0.28089358,  0.07890703,  0.40232944, 1.},
    { 0.28144581,  0.0843197 ,  0.40741404, 1.},
    { 0.28192358,  0.08966622,  0.41241521, 1.},
    { 0.28232739,  0.09495545,  0.41733086, 1.},
    { 0.28265633,  0.10019576,  0.42216032, 1.},
    { 0.28291049,  0.10539345,  0.42690202, 1.},
    { 0.28309095,  0.11055307,  0.43155375, 1.},
    { 0.28319704,  0.11567966,  0.43611482, 1.},
    { 0.28322882,  0.12077701,  0.44058404, 1.},
    { 0.28318684,  0.12584799,  0.44496   , 1.},
    { 0.283072  ,  0.13089477,  0.44924127, 1.},
    { 0.28288389,  0.13592005,  0.45342734, 1.},
    { 0.28262297,  0.14092556,  0.45751726, 1.},
    { 0.28229037,  0.14591233,  0.46150995, 1.},
    { 0.28188676,  0.15088147,  0.46540474, 1.},
    { 0.28141228,  0.15583425,  0.46920128, 1.},
    { 0.28086773,  0.16077132,  0.47289909, 1.},
    { 0.28025468,  0.16569272,  0.47649762, 1.},
    { 0.27957399,  0.17059884,  0.47999675, 1.},
    { 0.27882618,  0.1754902 ,  0.48339654, 1.},
    { 0.27801236,  0.18036684,  0.48669702, 1.},
    { 0.27713437,  0.18522836,  0.48989831, 1.},
    { 0.27619376,  0.19007447,  0.49300074, 1.},
    { 0.27519116,  0.1949054 ,  0.49600488, 1.},
    { 0.27412802,  0.19972086,  0.49891131, 1.},
    { 0.27300596,  0.20452049,  0.50172076, 1.},
    { 0.27182812,  0.20930306,  0.50443413, 1.},
    { 0.27059473,  0.21406899,  0.50705243, 1.},
    { 0.26930756,  0.21881782,  0.50957678, 1.},
    { 0.26796846,  0.22354911,  0.5120084 , 1.},
    { 0.26657984,  0.2282621 ,  0.5143487 , 1.},
    { 0.2651445 ,  0.23295593,  0.5165993 , 1.},
    { 0.2636632 ,  0.23763078,  0.51876163, 1.},
    { 0.26213801,  0.24228619,  0.52083736, 1.},
    { 0.26057103,  0.2469217 ,  0.52282822, 1.},
    { 0.25896451,  0.25153685,  0.52473609, 1.},
    { 0.25732244,  0.2561304 ,  0.52656332, 1.},
    { 0.25564519,  0.26070284,  0.52831152, 1.},
    { 0.25393498,  0.26525384,  0.52998273, 1.},
    { 0.25219404,  0.26978306,  0.53157905, 1.},
    { 0.25042462,  0.27429024,  0.53310261, 1.},
    { 0.24862899,  0.27877509,  0.53455561, 1.},
    { 0.2468114 ,  0.28323662,  0.53594093, 1.},
    { 0.24497208,  0.28767547,  0.53726018, 1.},
    { 0.24311324,  0.29209154,  0.53851561, 1.},
    { 0.24123708,  0.29648471,  0.53970946, 1.},
    { 0.23934575,  0.30085494,  0.54084398, 1.},
    { 0.23744138,  0.30520222,  0.5419214 , 1.},
    { 0.23552606,  0.30952657,  0.54294396, 1.},
    { 0.23360277,  0.31382773,  0.54391424, 1.},
    { 0.2316735 ,  0.3181058 ,  0.54483444, 1.},
    { 0.22973926,  0.32236127,  0.54570633, 1.},
    { 0.22780192,  0.32659432,  0.546532  , 1.},
    { 0.2258633 ,  0.33080515,  0.54731353, 1.},
    { 0.22392515,  0.334994  ,  0.54805291, 1.},
    { 0.22198915,  0.33916114,  0.54875211, 1.},
    { 0.22005691,  0.34330688,  0.54941304, 1.},
    { 0.21812995,  0.34743154,  0.55003755, 1.},
    { 0.21620971,  0.35153548,  0.55062743, 1.},
    { 0.21429757,  0.35561907,  0.5511844 , 1.},
    { 0.21239477,  0.35968273,  0.55171011, 1.},
    { 0.2105031 ,  0.36372671,  0.55220646, 1.},
    { 0.20862342,  0.36775151,  0.55267486, 1.},
    { 0.20675628,  0.37175775,  0.55311653, 1.},
    { 0.20490257,  0.37574589,  0.55353282, 1.},
    { 0.20306309,  0.37971644,  0.55392505, 1.},
    { 0.20123854,  0.38366989,  0.55429441, 1.},
    { 0.1994295 ,  0.38760678,  0.55464205, 1.},
    { 0.1976365 ,  0.39152762,  0.55496905, 1.},
    { 0.19585993,  0.39543297,  0.55527637, 1.},
    { 0.19410009,  0.39932336,  0.55556494, 1.},
    { 0.19235719,  0.40319934,  0.55583559, 1.},
    { 0.19063135,  0.40706148,  0.55608907, 1.},
    { 0.18892259,  0.41091033,  0.55632606, 1.},
    { 0.18723083,  0.41474645,  0.55654717, 1.},
    { 0.18555593,  0.4185704 ,  0.55675292, 1.},
    { 0.18389763,  0.42238275,  0.55694377, 1.},
    { 0.18225561,  0.42618405,  0.5571201 , 1.},
    { 0.18062949,  0.42997486,  0.55728221, 1.},
    { 0.17901879,  0.43375572,  0.55743035, 1.},
    { 0.17742298,  0.4375272 ,  0.55756466, 1.},
    { 0.17584148,  0.44128981,  0.55768526, 1.},
    { 0.17427363,  0.4450441 ,  0.55779216, 1.},
    { 0.17271876,  0.4487906 ,  0.55788532, 1.},
    { 0.17117615,  0.4525298 ,  0.55796464, 1.},
    { 0.16964573,  0.45626209,  0.55803034, 1.},
    { 0.16812641,  0.45998802,  0.55808199, 1.},
    { 0.1666171 ,  0.46370813,  0.55811913, 1.},
    { 0.16511703,  0.4674229 ,  0.55814141, 1.},
    { 0.16362543,  0.47113278,  0.55814842, 1.},
    { 0.16214155,  0.47483821,  0.55813967, 1.},
    { 0.16066467,  0.47853961,  0.55811466, 1.},
    { 0.15919413,  0.4822374 ,  0.5580728 , 1.},
    { 0.15772933,  0.48593197,  0.55801347, 1.},
    { 0.15626973,  0.4896237 ,  0.557936  , 1.},
    { 0.15481488,  0.49331293,  0.55783967, 1.},
    { 0.15336445,  0.49700003,  0.55772371, 1.},
    { 0.1519182 ,  0.50068529,  0.55758733, 1.},
    { 0.15047605,  0.50436904,  0.55742968, 1.},
    { 0.14903918,  0.50805136,  0.5572505 , 1.},
    { 0.14760731,  0.51173263,  0.55704861, 1.},
    { 0.14618026,  0.51541316,  0.55682271, 1.},
    { 0.14475863,  0.51909319,  0.55657181, 1.},
    { 0.14334327,  0.52277292,  0.55629491, 1.},
    { 0.14193527,  0.52645254,  0.55599097, 1.},
    { 0.14053599,  0.53013219,  0.55565893, 1.},
    { 0.13914708,  0.53381201,  0.55529773, 1.},
    { 0.13777048,  0.53749213,  0.55490625, 1.},
    { 0.1364085 ,  0.54117264,  0.55448339, 1.},
    { 0.13506561,  0.54485335,  0.55402906, 1.},
    { 0.13374299,  0.54853458,  0.55354108, 1.},
    { 0.13244401,  0.55221637,  0.55301828, 1.},
    { 0.13117249,  0.55589872,  0.55245948, 1.},
    { 0.1299327 ,  0.55958162,  0.55186354, 1.},
    { 0.12872938,  0.56326503,  0.55122927, 1.},
    { 0.12756771,  0.56694891,  0.55055551, 1.},
    { 0.12645338,  0.57063316,  0.5498411 , 1.},
    { 0.12539383,  0.57431754,  0.54908564, 1.},
    { 0.12439474,  0.57800205,  0.5482874 , 1.},
    { 0.12346281,  0.58168661,  0.54744498, 1.},
    { 0.12260562,  0.58537105,  0.54655722, 1.},
    { 0.12183122,  0.58905521,  0.54562298, 1.},
    { 0.12114807,  0.59273889,  0.54464114, 1.},
    { 0.12056501,  0.59642187,  0.54361058, 1.},
    { 0.12009154,  0.60010387,  0.54253043, 1.},
    { 0.11973756,  0.60378459,  0.54139999, 1.},
    { 0.11951163,  0.60746388,  0.54021751, 1.},
    { 0.11942341,  0.61114146,  0.53898192, 1.},
    { 0.11948255,  0.61481702,  0.53769219, 1.},
    { 0.11969858,  0.61849025,  0.53634733, 1.},
    { 0.12008079,  0.62216081,  0.53494633, 1.},
    { 0.12063824,  0.62582833,  0.53348834, 1.},
    { 0.12137972,  0.62949242,  0.53197275, 1.},
    { 0.12231244,  0.63315277,  0.53039808, 1.},
    { 0.12344358,  0.63680899,  0.52876343, 1.},
    { 0.12477953,  0.64046069,  0.52706792, 1.},
    { 0.12632581,  0.64410744,  0.52531069, 1.},
    { 0.12808703,  0.64774881,  0.52349092, 1.},
    { 0.13006688,  0.65138436,  0.52160791, 1.},
    { 0.13226797,  0.65501363,  0.51966086, 1.},
    { 0.13469183,  0.65863619,  0.5176488 , 1.},
    { 0.13733921,  0.66225157,  0.51557101, 1.},
    { 0.14020991,  0.66585927,  0.5134268 , 1.},
    { 0.14330291,  0.66945881,  0.51121549, 1.},
    { 0.1466164 ,  0.67304968,  0.50893644, 1.},
    { 0.15014782,  0.67663139,  0.5065889 , 1.},
    { 0.15389405,  0.68020343,  0.50417217, 1.},
    { 0.15785146,  0.68376525,  0.50168574, 1.},
    { 0.16201598,  0.68731632,  0.49912906, 1.},
    { 0.1663832 ,  0.69085611,  0.49650163, 1.},
    { 0.1709484 ,  0.69438405,  0.49380294, 1.},
    { 0.17570671,  0.6978996 ,  0.49103252, 1.},
    { 0.18065314,  0.70140222,  0.48818938, 1.},
    { 0.18578266,  0.70489133,  0.48527326, 1.},
    { 0.19109018,  0.70836635,  0.48228395, 1.},
    { 0.19657063,  0.71182668,  0.47922108, 1.},
    { 0.20221902,  0.71527175,  0.47608431, 1.},
    { 0.20803045,  0.71870095,  0.4728733 , 1.},
    { 0.21400015,  0.72211371,  0.46958774, 1.},
    { 0.22012381,  0.72550945,  0.46622638, 1.},
    { 0.2263969 ,  0.72888753,  0.46278934, 1.},
    { 0.23281498,  0.73224735,  0.45927675, 1.},
    { 0.2393739 ,  0.73558828,  0.45568838, 1.},
    { 0.24606968,  0.73890972,  0.45202405, 1.},
    { 0.25289851,  0.74221104,  0.44828355, 1.},
    { 0.25985676,  0.74549162,  0.44446673, 1.},
    { 0.26694127,  0.74875084,  0.44057284, 1.},
    { 0.27414922,  0.75198807,  0.4366009 , 1.},
    { 0.28147681,  0.75520266,  0.43255207, 1.},
    { 0.28892102,  0.75839399,  0.42842626, 1.},
    { 0.29647899,  0.76156142,  0.42422341, 1.},
    { 0.30414796,  0.76470433,  0.41994346, 1.},
    { 0.31192534,  0.76782207,  0.41558638, 1.},
    { 0.3198086 ,  0.77091403,  0.41115215, 1.},
    { 0.3277958 ,  0.77397953,  0.40664011, 1.},
    { 0.33588539,  0.7770179 ,  0.40204917, 1.},
    { 0.34407411,  0.78002855,  0.39738103, 1.},
    { 0.35235985,  0.78301086,  0.39263579, 1.},
    { 0.36074053,  0.78596419,  0.38781353, 1.},
    { 0.3692142 ,  0.78888793,  0.38291438, 1.},
    { 0.37777892,  0.79178146,  0.3779385 , 1.},
    { 0.38643282,  0.79464415,  0.37288606, 1.},
    { 0.39517408,  0.79747541,  0.36775726, 1.},
    { 0.40400101,  0.80027461,  0.36255223, 1.},
    { 0.4129135 ,  0.80304099,  0.35726893, 1.},
    { 0.42190813,  0.80577412,  0.35191009, 1.},
    { 0.43098317,  0.80847343,  0.34647607, 1.},
    { 0.44013691,  0.81113836,  0.3409673 , 1.},
    { 0.44936763,  0.81376835,  0.33538426, 1.},
    { 0.45867362,  0.81636288,  0.32972749, 1.},
    { 0.46805314,  0.81892143,  0.32399761, 1.},
    { 0.47750446,  0.82144351,  0.31819529, 1.},
    { 0.4870258 ,  0.82392862,  0.31232133, 1.},
    { 0.49661536,  0.82637633,  0.30637661, 1.},
    { 0.5062713 ,  0.82878621,  0.30036211, 1.},
    { 0.51599182,  0.83115784,  0.29427888, 1.},
    { 0.52577622,  0.83349064,  0.2881265 , 1.},
    { 0.5356211 ,  0.83578452,  0.28190832, 1.},
    { 0.5455244 ,  0.83803918,  0.27562602, 1.},
    { 0.55548397,  0.84025437,  0.26928147, 1.},
    { 0.5654976 ,  0.8424299 ,  0.26287683, 1.},
    { 0.57556297,  0.84456561,  0.25641457, 1.},
    { 0.58567772,  0.84666139,  0.24989748, 1.},
    { 0.59583934,  0.84871722,  0.24332878, 1.},
    { 0.60604528,  0.8507331 ,  0.23671214, 1.},
    { 0.61629283,  0.85270912,  0.23005179, 1.},
    { 0.62657923,  0.85464543,  0.22335258, 1.},
    { 0.63690157,  0.85654226,  0.21662012, 1.},
    { 0.64725685,  0.85839991,  0.20986086, 1.},
    { 0.65764197,  0.86021878,  0.20308229, 1.},
    { 0.66805369,  0.86199932,  0.19629307, 1.},
    { 0.67848868,  0.86374211,  0.18950326, 1.},
    { 0.68894351,  0.86544779,  0.18272455, 1.},
    { 0.69941463,  0.86711711,  0.17597055, 1.},
    { 0.70989842,  0.86875092,  0.16925712, 1.},
    { 0.72039115,  0.87035015,  0.16260273, 1.},
    { 0.73088902,  0.87191584,  0.15602894, 1.},
    { 0.74138803,  0.87344918,  0.14956101, 1.},
    { 0.75188414,  0.87495143,  0.14322828, 1.},
    { 0.76237342,  0.87642392,  0.13706449, 1.},
    { 0.77285183,  0.87786808,  0.13110864, 1.},
    { 0.78331535,  0.87928545,  0.12540538, 1.},
    { 0.79375994,  0.88067763,  0.12000532, 1.},
    { 0.80418159,  0.88204632,  0.11496505, 1.},
    { 0.81457634,  0.88339329,  0.11034678, 1.},
    { 0.82494028,  0.88472036,  0.10621724, 1.},
    { 0.83526959,  0.88602943,  0.1026459 , 1.},
    { 0.84556056,  0.88732243,  0.09970219, 1.},
    { 0.8558096 ,  0.88860134,  0.09745186, 1.},
    { 0.86601325,  0.88986815,  0.09595277, 1.},
    { 0.87616824,  0.89112487,  0.09525046, 1.},
    { 0.88627146,  0.89237353,  0.09537439, 1.},
    { 0.89632002,  0.89361614,  0.09633538, 1.},
    { 0.90631121,  0.89485467,  0.09812496, 1.},
    { 0.91624212,  0.89609127,  0.1007168 , 1.},
    { 0.92610579,  0.89732977,  0.10407067, 1.},
    { 0.93590444,  0.8985704 ,  0.10813094, 1.},
    { 0.94563626,  0.899815  ,  0.11283773, 1.},
    { 0.95529972,  0.90106534,  0.11812832, 1.},
    { 0.96489353,  0.90232311,  0.12394051, 1.},
    { 0.97441665,  0.90358991,  0.13021494, 1.},
    { 0.98386829,  0.90486726,  0.13689671, 1.},
    { 0.99324789,  0.90615657,  0.1439362 , 1.}};

  TransferFunction fct(RGB);
  double dx = 1. / (data.size() - 1);
  for(size_t i = 0 ; i < data.size() ; ++i) {
    fct.add_rgba_point(i*dx, data[i]);
  }
  return fct;
}

TransferFunction TransferFunction::jet()
{
  TransferFunction fct(RGB);
  fct.add_rgba_point(1, Colorf(0.5, 0.0, 0.0, 1));
  fct.add_rgba_point(7. / 8, Colorf(1.0, 0.0, 0.0, 1));
  fct.add_rgba_point(5. / 8, Colorf(1.0, 1.0, 0.0, 1));
  fct.add_rgba_point(3. / 8, Colorf(0.0, 1.0, 1.0, 1));
  fct.add_rgba_point(1. / 8, Colorf(0.0, 0.0, 1.0, 1));
  fct.add_rgba_point(0, Colorf(0.0, 0.0, 0.5, 1));
  return fct;
}

TransferFunction& TransferFunction::operator=(const TransferFunction& other)
{
  clamp = other.clamp;
  _interpolation = other._interpolation;
  exteriorColor = other.exteriorColor;
  values = other.values;
  keys = other.keys;
  return *this;
}

double TransferFunction::operator[](int n) const {
  return values[n].first;
}

TransferFunction::Colorf TransferFunction::hsva_point(double pos) const
{
  key_map::const_iterator it_found = keys.find(pos);
  if(it_found != keys.end()) {
    Colorf col = values[it_found.value()].second;
    if(_interpolation == RGB)
      col = convertRGBtoHSV(col);
    return col;
  }
  return Colorf(-1.0);
}

TransferFunction::Colorf TransferFunction::rgba_point(double pos) const
{
  key_map::const_iterator it_found = keys.find(pos);
  if(it_found != keys.end()) {
    Colorf col = values[it_found.value()].second;
    if(_interpolation != RGB)
      col = convertHSVtoRGB(col);
    return col;
  }
  return Colorf(-1.0);
}

void TransferFunction::setPointList(const value_list& lst)
{
  values = lst;
  update_keys();
}

bool TransferFunction::operator==(const TransferFunction& other) const {
  return values == other.values;
}

bool TransferFunction::operator!=(const TransferFunction& other) const {
  return values != other.values;
}
} // namespace lgx
