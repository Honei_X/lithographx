/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ProcessDocDialog.hpp"

#include "Process.hpp"
#include "Information.hpp"
#include "ProcessUtils.hpp"

#include "ui_ProcessDocs.h"

#include <QHash>
#include <QRegularExpression>
#include <QTreeWidget>
#include <QWidget>

ProcessDocDialog::ProcessDocDialog(QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::ProcessDocDialog)
{
  ui->setupUi(this);
  findProcesses("Stack", ui->StackTreeWidget);
  findProcesses("Mesh", ui->MeshTreeWidget);
  findProcesses("Global", ui->GlobalTreeWidget);
}

ProcessDocDialog::~ProcessDocDialog() {}

void ProcessDocDialog::findProcesses(const QString& type, QTreeWidget* tree)
{
  QHash<QString, QTreeWidgetItem*> folders;
  auto processes = lgx::process::listProcesses(type);
  for(const auto& name : processes) {
    auto def = lgx::process::getBaseProcessDefinition(type, name);
    if(not def) {
      lgx::Information::err << QString("Couldn't find definition of process %1.%2").arg(type).arg(name) << endl;
      continue;
    }
    auto item = new QTreeWidgetItem(QStringList() << def->name);
    item->setFlags(Qt::ItemIsEnabled | Qt::ItemNeverHasChildren | Qt::ItemIsSelectable);
    item->setToolTip(0, def->description);
    item->setIcon(0, def->icon);
    if(def->folder.isEmpty())
      tree->addTopLevelItem(item);
    else {
      QString folder_s = def->folder;
      auto folder = process_util::getFolder(folder_s, folders, tree);
      folder->addChild(item);
    }
  }
  tree->sortItems(0, Qt::AscendingOrder);
  tree->resizeColumnToContents(0);
}

void ProcessDocDialog::on_StackTreeWidget_currentItemChanged(QTreeWidgetItem* item, QTreeWidgetItem*)
{
  if(item->flags() & Qt::ItemNeverHasChildren) {
    auto def = lgx::process::getBaseProcessDefinition("Stack", item->text(0));
    if(def)
      updateDocView("Stack", def);
  }
}

void ProcessDocDialog::on_MeshTreeWidget_currentItemChanged(QTreeWidgetItem* item, QTreeWidgetItem*)
{
  if(item->flags() & Qt::ItemNeverHasChildren) {
    auto def = lgx::process::getBaseProcessDefinition("Mesh", item->text(0));
    if(def)
      updateDocView("Mesh", def);
  }
}

void ProcessDocDialog::on_GlobalTreeWidget_currentItemChanged(QTreeWidgetItem* item, QTreeWidgetItem*)
{
  if(item->flags() & Qt::ItemNeverHasChildren) {
    auto def = lgx::process::getBaseProcessDefinition("Global", item->text(0));
    if(def)
      updateDocView("Global", def);
  }
}

void ProcessDocDialog::updateDocView(const QString& type, lgx::process::BaseProcessDefinition* def)
{
  QStringList html;
  QString desc = def->description;
  QString icon_name = QString("icon://%1_%2").arg(type).arg(def->name);
  icon_name.replace(" ", "_");
  auto doc = ui->Doc->document();
  doc->addResource(QTextDocument::ImageResource, QUrl(icon_name), def->icon.pixmap(QSize(64, 64)));
  desc.replace("\n", "<br/>");
  html << QString("<h1><img src=\"%3\"/> %1.%2</h1>").arg(type).arg(def->name).arg(icon_name)
       << QString("<h2>Description</h2><p>%1</p>").arg(desc);
  if(not def->parmNames.empty()) {
    html << "<h2>Parameters</h2>"
         << "<table width='100%' border=1 cell-spacing='1' style='border-width: 1px;border-style: solid'>";
    for(int i = 0; i < def->parmNames.size(); ++i) {
      html << "<tr><td width='20%'><b>" << def->parmNames[i] << "</b></td>";
      QString desc;
      if(i < def->parmDescs.size())
        desc = def->parmDescs[i].trimmed();
      else
        desc = def->parmNames[i];
      desc.replace("\n", "<br/>");
      html << "<td> " << desc << " </td>";
      html << "</tr>";
    }
    html << "</table></frame><br/><br/>";
  }

  ui->Doc->setHtml(html.join("\n"));
}

void ProcessDocDialog::on_StackFilter_textChanged(const QString& text)
{
  process_util::filterProcesses(ui->StackTreeWidget, text);
}

void ProcessDocDialog::on_MeshFilter_textChanged(const QString& text)
{
  process_util::filterProcesses(ui->MeshTreeWidget, text);
}

void ProcessDocDialog::on_GlobalFilter_textChanged(const QString& text)
{
  process_util::filterProcesses(ui->GlobalTreeWidget, text);
}
