/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Stack.hpp"
#include "Process.hpp"

#include "Information.hpp"

namespace lgx {

// template <typename T>
// static const T& max(const T& v1, const T& v2)
//{
// if(v1 > v2) return v1;
// return v2;
//}

Stack::Stack(int id)
  : _main(new Store(this))
  , _work(new Store(this))
  , _scale(Point3f(1.0, 1.0, 1.0))
  //, _scale(1.0f)
  , _origin()
  , _texScale(1.0)
  , _size(512, 512, 100)
  , _step(1, 1, 1)
  , _frame()
  , _trans()
  , changed_frame(false)
  , changed_trans(false)
  , changed_label(false)
  , _id(id)
  , _CurrLabel(0)
  , _showScale(false)
  , _showTrans(false)
  , _showBBox(false)
  , _tieScales(true)
{
  updateSizes();
}

Stack::Stack(const Stack& copy)
  : _main(new Store(*copy._main))
  , _work(new Store(*copy._work))
  , _scale(copy._scale)
  , _origin(copy._origin)
  , _texScale(copy._texScale)
  , _size(copy._size)
  , _step(copy._step)
  , _frame(copy._frame)
  , _trans(copy._trans)
  , changed_frame(copy.changed_frame)
  , changed_trans(copy.changed_trans)
  , changed_label(copy.changed_label)
  , _id(-1)
  , _CurrLabel(copy._CurrLabel)
  , _showScale(copy._showScale)
  , _showTrans(copy._showTrans)
  , _showBBox(copy._showBBox)
  , _tieScales(copy._tieScales)
{
  _main->setStack(this);
  _work->setStack(this);
  updateSizes();
}

void Stack::reset() {
  setSize(Point3u());
}

Stack::~Stack()
{
  if(_main)
    delete _main;
  _main = 0;
  if(_work)
    delete _work;
  _work = 0;
}

void Stack::setMain(Store* m)
{
  _main = m;
}

void Stack::setWork(Store* w)
{
  _work = w;
}

void Stack::updateSizes()
{
  size_t SizeXYZ = size_t(_size.x()) * _size.y() * _size.z();
  if(SizeXYZ > 0) {
    _texScale = max(_size.x() * _step.x(), max(_size.y() * _step.y(), _size.z() * _step.z()));

    if(main() and main()->size() != SizeXYZ)
      main()->allocate();
    if(work() and work()->size() != SizeXYZ)
      work()->allocate();
  } else {
    _texScale = 1.0f;
    if(main())
      main()->reset();
    if(work())
      work()->reset();
  }
}

void Stack::setSize(const Point3u& size)
{
  _size = size;
  updateSizes();
}

void Stack::setStep(const Point3f& step)
{
  Point3f s = step;
  if(s.x() <= 0)
    s.x() = 1;
  if(s.y() <= 0)
    s.y() = 1;
  if(s.z() <= 0)
    s.z() = 1;
  _step = divide(s, scale());
  //_step = s / scale();
  updateSizes();
}

void Stack::setOrigin(const Point3f& s)
{
  _origin = divide(s, scale());
  //_origin = s / scale();
}

void Stack::center() {
  _origin = -multiply(_step, Point3f(_size)) / 2;
}

void Stack::resetModified()
{
  changed_frame = false;
  changed_trans = false;
  changed_label = false;
}

Matrix4f Stack::worldToImage() const
{
  Matrix4f m = Matrix4f::identity();
  m(0, 3) = -origin().x() / step().x();
  m(1, 3) = -origin().y() / step().y();
  m(2, 3) = -origin().z() / step().z();
  m(0, 0) = 1. / step().x();
  m(1, 1) = 1. / step().y();
  m(2, 2) = 1. / step().z();
  Matrix4f m2 = Matrix4f::identity();
  m2(0, 3) = m2(1, 3) = m2(2, 3) = -0.5;
  return m;
}

Matrix4f Stack::imageToWorld() const
{
  Matrix4f m = Matrix4f::identity();
  m(0, 3) = 0.5 * step().x();
  m(1, 3) = 0.5 * step().y();
  m(2, 3) = 0.5 * step().z();
  m(0, 0) = step().x();
  m(1, 1) = step().y();
  m(2, 2) = step().z();
  Matrix4f m2 = Matrix4f::identity();
  m2(0, 3) = origin().x();
  m2(1, 3) = origin().y();
  m2(2, 3) = origin().z();
  return m2 * m;
}

Matrix4f Stack::worldToImageVector() const
{
  Matrix4f m = Matrix4f::identity();
  m(3, 0) = 1. / step().x();
  m(3, 1) = 1. / step().y();
  m(3, 2) = 1. / step().z();
  return m;
}

Matrix4f Stack::imageToWorldVector() const
{
  Matrix4f m = Matrix4f::identity();
  m(3, 0) = step().x();
  m(3, 1) = step().y();
  m(3, 2) = step().z();
  return m;
}

Store* Stack::currentStore()
{
  if(_work and _work->isVisible())
    return _work;
  if(_main and _main->isVisible())
     return _main;
  return nullptr;
}

const Store* Stack::currentStore() const
{
  if(_work and _work->isVisible())
    return _work;
  if(_main and _main->isVisible())
     return _main;
  return nullptr;
}

void Stack::setMainAsCurrent()
{
  _main->show();
  _work->hide();
}

void Stack::setWorkAsCurrent()
{
  _work->show();
}

void Stack::setCurrent(STORE which)
{
  switch(which) {
    case MAIN_STORE:
      setMainAsCurrent();
      break;
    case WORK_STORE:
      setWorkAsCurrent();
    case CURRENT_STORE:
      break;
  }
}

Store* Stack::store(STORE store) {
  switch(store) {
    case MAIN_STORE:
      return main();
    case WORK_STORE:
      return work();
    case CURRENT_STORE:
      return currentStore();
  }
  return nullptr;
}

const Store* Stack::store(STORE store) const {
  switch(store) {
    case MAIN_STORE:
      return main();
    case WORK_STORE:
      return work();
    case CURRENT_STORE:
      return currentStore();
  }
  return nullptr;
}

} // namespace lgx
