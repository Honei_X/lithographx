/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef DEBUGDLG_HPP
#define DEBUGDLG_HPP

#include <LGXConfig.hpp>

#include <LithoViewer.hpp>

#include <ui_DebugDlg.h>

class DebugDlg : public QDialog {
  Q_OBJECT
public:
  DebugDlg(LithoViewer* viewer, QWidget* parent = 0, Qt::WindowFlags f = 0);

protected slots:
  void on_peeling_toggled(bool on);
  void on_peelingSlice_valueChanged(int val);
  void on_peelingType_currentIndexChanged(int val);

protected:
  LithoViewer* viewer;
  Ui::DebugDlg ui;
};

#endif
