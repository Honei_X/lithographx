/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef QHULL_LGXQHULL_HPP
#define QHULL_LGXQHULL_HPP

#include <LGXConfig.hpp>

#ifdef WIN32
#  ifndef lgxQHull_EXPORTS
#    define qh_QHpointer_dllimport 1
#  endif
#endif

extern "C" {
#include <qhull/qhull_a.h>
}

#endif // QHULL_LGXQHULL_HPP

