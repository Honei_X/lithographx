/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef EDITMARKERSDLG_HPP
#define EDITMARKERSDLG_HPP

#include <LGXConfig.hpp>

#include <TransferFunction.hpp>
#include <TransferMarkers.hpp>

#include <QDialog>
#include <vector>

#include <ui_EditMarkersDlg.h>

namespace lgx {
namespace gui {
class LGX_EXPORT EditMarkersDlg : public QDialog {
  Q_OBJECT
public:
  typedef TransferFunction::Colorf Colorf;
  typedef TransferFunction::value_list value_list;
  typedef TransferFunction::Interpolation Interpolation;
  EditMarkersDlg(const TransferFunction& fct, QWidget* parent = 0, Qt::WindowFlags f = 0);

  TransferFunction getFunction() const {
    return function;
  }

  value_list pointList() const;

public slots:
  void on_addMarker_clicked();
  void on_removeMarker_clicked();
  void on_rgbaMode_toggled(bool on);
  void on_hsvaMode_toggled(bool on);
  void spreadMarkers();

protected:
  TransferFunction function;
  Ui::EditMarkersDlg ui;
  TransferMarkerModel* model;
  MarkerColorDelegate* delegate;
  QPushButton* spread_button;
};
} // namespace gui // namespace gui
} // namespace lgx // namespace lgx
#endif
