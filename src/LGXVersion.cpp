/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include <LGXVersion.hpp>

#include <QStringList>

bool LGXVersion::valid() const
{
  return major > 0 and minor > 0 and patch > 0;
}

LGXVersion::operator bool() const
{
  return valid();
}

LGXVersion::LGXVersion(int maj, int min, int p)
  : major(maj)
  , minor(min)
  , patch(p)
{ }

LGXVersion::LGXVersion(const QString& s)
{
  QStringList fields = s.split(".");
  if(fields.size() != 3)
    return;
  int maj, min, pat;
  bool ok;
  maj = fields[0].toUInt(&ok);
  if(not ok) return;
  min = fields[1].toUInt(&ok);
  if(not ok) return;
  pat = fields[2].toUInt(&ok);
  if(not ok) return;
  major = maj;
  minor = min;
  patch = pat;
}

LGXVersion lgxVersion = { VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH };

QString LGXVersion::toString() const
{
  return QString("%1.%2.%3").arg(major).arg(minor).arg(patch);
}

bool operator==(const LGXVersion& v1, const LGXVersion& v2)
{
  return v1.major == v2.major and v1.minor == v2.minor and v1.patch == v2.patch;
}

bool operator!=(const LGXVersion& v1, const LGXVersion& v2)
{
  return v1.major != v2.major or v1.minor != v2.minor or v1.patch != v2.patch;
}

bool operator>(const LGXVersion& v1, const LGXVersion& v2)
{
  return (v1.major > v2.major or
          (v1.major == v2.major and
           (v1.minor > v2.minor or
            (v1.minor == v2.minor and v1.patch > v2.patch))));
}

bool operator<(const LGXVersion& v1, const LGXVersion& v2)
{
  return (v1.major < v2.major or
          (v1.major == v2.major and
           (v1.minor < v2.minor or
            (v1.minor == v2.minor and v1.patch < v2.patch))));
}

bool operator>=(const LGXVersion& v1, const LGXVersion& v2)
{
  return not (v1 < v2);
}

bool operator<=(const LGXVersion& v1, const LGXVersion& v2)
{
  return not (v1 > v2);
}

