varying vec3 nor;
varying vec3 lightPos;

void light()
{
  nor = gl_NormalMatrix * gl_Normal;
  lightPos = gl_Vertex.xyz / gl_Vertex.w;
}
