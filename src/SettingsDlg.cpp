/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "SettingsDlg.hpp"

#include <LithoViewer.hpp>
#include <ImageData.hpp>
#include <Information.hpp>
#include <QFontDialog>
#include <cuda/CudaExport.hpp>

SettingsDlg::SettingsDlg(LithoViewer* l)
  : QDialog(l)
  , viewer(l)
{
  ui.setupUi(this);
  loadGeneral();
  loadRendering();
  loadScaleBar();
  loadColorBar();
}

void SettingsDlg::loadRendering()
{
  ui.renderingMaxTexX->setValue(lgx::ImgData::MaxTexSize.x());
  ui.renderingMaxTexY->setValue(lgx::ImgData::MaxTexSize.y());
  ui.renderingMaxTexZ->setValue(lgx::ImgData::MaxTexSize.z());
  ui.maxCudaMem->setValue(getHoldMemGPU());
  ui.renderingNormalSize->setValue(lgx::ImgData::DrawNormals);

  ui.rendering16Bits->setChecked(lgx::ImgData::defaultTex16Bits);
  ui.rendering8Bits->setChecked(not lgx::ImgData::defaultTex16Bits);

  ui.renderingMaxPeel->setValue(viewer->property("maxNbPeels").toInt());
  auto unsharp = viewer->property("unsharpening").toFloat();
  lgx::Information::out << "unsharp = " << unsharp << endl;
  ui.renderingUnsharp->setValue(unsharp);
  auto shininess = viewer->property("shininess").toFloat();
  lgx::Information::out << "shininess = " << shininess << endl;
  ui.renderingShininess->setValue(shininess);
  auto specular = viewer->property("specular").toFloat();
  lgx::Information::out << "specular = " << specular << endl;
  ui.renderingSpecular->setValue(specular);
}

void SettingsDlg::loadGeneral()
{
#ifdef LithoGraphX_CHECK_UPDATES
  QSettings settings;
  auto autoUpdate = settings.value("AutoUpdate/CheckUpdate", true).toBool();
  bool ok;
  auto intervalTime = settings.value("AutoUpdate/CheckInterval", 1).toUInt(&ok);
  if(not ok) intervalTime = 1;
  ui.checkNewVersion->setChecked(autoUpdate);
  ui.checkVersionInterval->setValue(intervalTime);
#else
  auto idx = ui.tabWidget->indexOf(ui.tabGeneral);
  ui.tabWidget->removeTab(idx);
#endif
}

void SettingsDlg::loadScaleBar()
{
  auto& scaleBar = lgx::ImgData::scaleBar;

  ui.scaleBarPosition->setCurrentIndex((int)scaleBar.position);
  ui.scaleBarOrientation->setCurrentIndex((int)scaleBar.orientation);
  ui.scaleBarTextPosition->setCurrentIndex((int)scaleBar.textPosition);
  ui.scaleBarMinSize->setValue((int)scaleBar.minimumSize());
  ui.scaleBarMaxSize->setValue((int)scaleBar.maximumSize());
  ui.scaleBarThickness->setValue((int)scaleBar.thickness());

  scaleBarFont = scaleBar.font();
}

void SettingsDlg::loadColorBar()
{
  auto& colorBar = lgx::ImgData::colorBar;

  ui.colorBarPosition->setCurrentIndex((int)colorBar.position);
  ui.colorBarOrientation->setCurrentIndex((int)colorBar.orientation);
  ui.colorBarLength->setValue(100*colorBar.scale_length);
  ui.colorBarWidth->setValue((int)colorBar.width);
  ui.colorBarDistText->setValue((int)colorBar.text_to_bar);
  ui.colorBarDistBorder->setValue((int)colorBar.distance_to_border);
  ui.colorBarTickSize->setValue((int)colorBar.tick_size);
  ui.colorBarLineWidth->setValue((int)colorBar.line_width);

  colorBarFont = colorBar.font;
}

void SettingsDlg::loadDefaults(LithoGraphX* lgx)
{
  QSettings settings;

  {
    settings.beginGroup("Rendering");
    lgx::Point3u maxTex;
    bool ok;
    maxTex.x() = settings.value("MaxTexX", 1024).toInt(&ok);
    if(not ok)
      maxTex.x() = lgx::ImgData::MaxTexSize.x();
    maxTex.y() = settings.value("MaxTexY", 1024).toInt(&ok);
    if(not ok)
      maxTex.y() = lgx::ImgData::MaxTexSize.y();
    maxTex.z() = settings.value("MaxTexZ", 1024).toInt(&ok);
    if(not ok)
      maxTex.z() = lgx::ImgData::MaxTexSize.z();
    lgx::ImgData::MaxTexSize = maxTex;

    lgx::ImgData::defaultTex16Bits = settings.value("Default16Bits", true).toBool();

    auto cudaMem = settings.value("CudaMem", 0).toUInt();
    if(ok) setHoldMemGPU(cudaMem);

    double normalSize = settings.value("MeshNormalSize", 0.).toDouble(&ok);
    if(ok) lgx::ImgData::DrawNormals = normalSize;

    settings.endGroup();
  }


}

void SettingsDlg::loadDefaults(LithoViewer* viewer)
{
  QSettings settings;

  {
    settings.beginGroup("Rendering");
    bool ok;

    auto maxPeels = settings.value("MaxPeels", 10).toUInt(&ok);
    if(ok and maxPeels > 0) viewer->setProperty("maxNbPeels", maxPeels);

    auto unsharpening = settings.value("Unsharpening", 1.f).toFloat(&ok);
    if(ok) viewer->setProperty("unsharpening", unsharpening);

    auto shininess = settings.value("Shininess", 32.f).toFloat(&ok);
    if(ok) viewer->setProperty("shininess", shininess);

    auto specular = settings.value("Specular", 0.2f).toFloat(&ok);
    if(ok) viewer->setProperty("specular", specular);
    settings.endGroup();
  }

  {
    lgx::ImgData::scaleBar.loadDefaults();
  }
  {
    lgx::ImgData::colorBar.loadDefaults();
  }
}

void SettingsDlg::applyGeneral()
{
  QSettings settings;
  settings.setValue("AutoUpdate/CheckUpdate", ui.checkNewVersion->isChecked());
  settings.setValue("AutoUpdate/CheckInterval", ui.checkVersionInterval->value());
}

void SettingsDlg::saveGeneral()
{
  applyGeneral();
}

void SettingsDlg::applyRendering()
{
  lgx::ImgData::MaxTexSize.x() = (uint)ui.renderingMaxTexX->value();
  lgx::ImgData::MaxTexSize.y() = (uint)ui.renderingMaxTexY->value();
  lgx::ImgData::MaxTexSize.z() = (uint)ui.renderingMaxTexZ->value();

  lgx::ImgData::defaultTex16Bits = ui.rendering16Bits->isChecked();

  ui.maxCudaMem->setValue(setHoldMemGPU(ui.maxCudaMem->value()));

  lgx::ImgData::DrawNormals = ui.renderingNormalSize->value();

  viewer->setProperty("maxNbPeels", ui.renderingMaxPeel->value());
  viewer->setProperty("unsharpening", ui.renderingUnsharp->value());
  viewer->setProperty("shininess", ui.renderingShininess->value());
  viewer->setProperty("specular", ui.renderingSpecular->value());

  emit update3DDrawing();
}

void SettingsDlg::saveRendering()
{
  applyRendering();

  QSettings settings;

  settings.beginGroup("Rendering");

  settings.setValue("MaxTexX", lgx::ImgData::MaxTexSize.x());
  settings.setValue("MaxTexY", lgx::ImgData::MaxTexSize.y());
  settings.setValue("MaxTexZ", lgx::ImgData::MaxTexSize.z());
  settings.setValue("Default16Bits", lgx::ImgData::defaultTex16Bits);
  settings.setValue("CudaMem", getHoldMemGPU());
  settings.setValue("MeshNormalSize", lgx::ImgData::DrawNormals);
  settings.setValue("MaxPeels", viewer->property("maxNbPeels").toUInt());
  settings.setValue("Unsharpening", viewer->property("unsharpening").toFloat());
  settings.setValue("Shininess", viewer->property("shininess").toFloat());
  settings.setValue("Specular", viewer->property("specular").toFloat());

  settings.endGroup();
}

void SettingsDlg::applyScaleBar()
{
  auto& scaleBar = lgx::ImgData::scaleBar;

  scaleBar.position = (lgx::ScaleBar::Position) ui.scaleBarPosition->currentIndex();
  scaleBar.orientation = (lgx::ScaleBar::Orientation) ui.scaleBarOrientation->currentIndex();
  scaleBar.textPosition = (lgx::ScaleBar::TextPosition) ui.scaleBarTextPosition->currentIndex();
  scaleBar.setSizeRange(ui.scaleBarMinSize->value(), ui.scaleBarMaxSize->value());
  scaleBar.setThickness(ui.scaleBarThickness->value());
  scaleBar.setFont(scaleBarFont);

  emit update2DDrawing();
}

void SettingsDlg::saveScaleBar()
{
  applyScaleBar();

  auto& scaleBar = lgx::ImgData::scaleBar;
  QSettings settings;

  settings.beginGroup("ScaleBar");

  settings.setValue("Position", (int)scaleBar.position);
  settings.setValue("Orientation", (int)scaleBar.orientation);
  settings.setValue("TextPosition", (int)scaleBar.textPosition);
  settings.setValue("Font", scaleBar.font());
  settings.setValue("MinimumSize", scaleBar.minimumSize());
  settings.setValue("MaximumSize", scaleBar.maximumSize());
  settings.setValue("Thickness", scaleBar.thickness());

  settings.endGroup();
}

void SettingsDlg::applyColorBar()
{
  auto& colorBar = lgx::ImgData::colorBar;

  colorBar.position = (lgx::ColorBar::Position) ui.colorBarPosition->currentIndex();
  colorBar.orientation = (lgx::ColorBar::Orientation) ui.colorBarOrientation->currentIndex();
  colorBar.scale_length = ui.colorBarLength->value() / 100.;
  colorBar.width = ui.colorBarWidth->value();
  colorBar.text_to_bar = ui.colorBarDistText->value();
  colorBar.distance_to_border = ui.colorBarDistBorder->value();
  colorBar.tick_size = ui.colorBarTickSize->value();
  colorBar.font = colorBarFont;
  colorBar.line_width = ui.colorBarLineWidth->value();

  emit update2DDrawing();
}

void SettingsDlg::saveColorBar()
{
  applyColorBar();
  auto& colorBar = lgx::ImgData::colorBar;

  QSettings settings;
  settings.beginGroup("ColorBar");

  settings.setValue("Position", (int)colorBar.position);
  settings.setValue("Orientation", (int)colorBar.orientation);
  settings.setValue("Length", colorBar.scale_length);
  settings.setValue("Width", colorBar.width);
  settings.setValue("DistanceToText", colorBar.text_to_bar);
  settings.setValue("DistanceToBorder", colorBar.distance_to_border);
  settings.setValue("TickSize", colorBar.tick_size);
  settings.setValue("Font", colorBar.font);
  settings.setValue("LineWidth", colorBar.line_width);

  settings.endGroup();
}

void SettingsDlg::on_buttonBox_accepted()
{
  applyAll();
}

void SettingsDlg::applyAll()
{
  applyGeneral();
  applyRendering();
  applyScaleBar();
  applyColorBar();
}

void SettingsDlg::on_buttonBox_clicked(QAbstractButton* btn)
{
  switch(ui.buttonBox->buttonRole(btn)) {
    case QDialogButtonBox::ApplyRole:
      applyAll();
      break;
    default:
      break;
  }
}

void SettingsDlg::on_generalSetDefaults_clicked()
{
  saveGeneral();
}

void SettingsDlg::on_renderingSetDefaults_clicked()
{
  saveRendering();
}

void SettingsDlg::on_scaleBarSetDefaults_clicked()
{
  saveScaleBar();
}

void SettingsDlg::on_colorBarSetDefaults_clicked()
{
  saveColorBar();
}

void SettingsDlg::on_scaleBarMinSize_valueChanged(int value)
{
  int maxValue = ui.scaleBarMaxSize->value();
  if(maxValue < 2.5*value) {
    maxValue = (int)std::ceil(2.5*value);
    ui.scaleBarMaxSize->setValue(maxValue);
  }
}

void SettingsDlg::on_scaleBarMaxSize_valueChanged(int value)
{
  int minValue = ui.scaleBarMinSize->value();
  if(value > value - 2.5) {
    minValue = (int)std::floor(value/2.5);
    ui.scaleBarMinSize->setValue(minValue);
  }
}

void SettingsDlg::on_scaleBarChooseFont_clicked()
{
  QFontDialog dlg(scaleBarFont, this);
  if(dlg.exec() == QDialog::Accepted) {
    scaleBarFont = dlg.currentFont();
  }
}

void SettingsDlg::on_colorBarChooseFont_clicked()
{
  QFontDialog dlg(colorBarFont, this);
  if(dlg.exec() == QDialog::Accepted) {
    colorBarFont = dlg.currentFont();
  }
}

void SettingsDlg::on_checkVersionInterval_valueChanged(int value)
{
  if(value > 1)
    ui.checkVersionInterval->setSuffix(" days");
  else
    ui.checkVersionInterval->setSuffix(" day");
}
