/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef IMAGE_DATA_H

#define IMAGE_DATA_H

#include <LGXConfig.hpp>

#include <ClipRegion.hpp>
#include <ColorBar.hpp>
#include <Colors.hpp>
#include <Geometry.hpp>
#include <Mesh.hpp>
#include <LGXViewer/qglviewer.h>
#include <Misc.hpp>
#include <Parms.hpp>
#include <Process.hpp>
#include <ScaleBar.hpp>
#include <Shader.hpp>
#include <StackInfoDlg.hpp>
#include <TransferFunctionDlg.hpp>
#include <TransferFunction.hpp>

#include <CImg.h>
#include <QtGui>
#include <string>
#include <string.h>

namespace lgx {

class CutSurf;

typedef util::Vector<4, vertex> Point4v;
typedef util::Vector<3, GLubyte> Point3GLub;

class LGX_EXPORT ImgData : public QObject
{
  Q_OBJECT
public:
  typedef Mesh::MeshView MeshViewModeEnum;

  enum ReloadData {
    RELOAD_NONE      = 0x0,
    RELOAD_MAIN      = 0x1,
    RELOAD_WORK      = 0x2,
    RELOAD_TRIS      = 0x4,
    RELOAD_LINES     = 0x8,
    RELOAD_POS       = 0x10,
    RELOAD_VBO       = 0x20,
    UPDATE_SELECTION = 0x40
  };

  enum SelectedTool {
    ST_PIXEL_EDIT,       ///< 3D pixel edit tool
    ST_PICK_3D_LABEL,    ///< Select 3D labels
    ST_DELETE_3D_LABEL,  ///< Delete 3D labels
    ST_FILL_3D_LABEL,    ///< Replace label by another one in 3D
    ST_MESH_SELECT,      ///< Select part of a mesh
    ST_ADD_NEW_SEED,     ///< Label triangles, creating a new seed on each click
    ST_ADD_CURRENT_SEED, ///< Use the current seed to label triangles
    ST_PICK_LABEL,       ///< Pick a label on the mesh
    ST_SELECT_LABEL,     ///< Select part of the mesh, based on label
    ST_SELECT_CONNECTED, ///< Select a connected part of the mesh
    ST_GRAB_SEED,        ///< Grab seed from the other mesh
    ST_FILL_LABEL,       ///< Replace a label by another one, in 2D
    ST_DELETE_LABEL      ///< Delete 3D labels
  };


  int StackId;

  Stack* stack;
  Mesh* mesh;

  Point3u displayStep;

  // Main and stack paramters
  bool Main16Bit, Work16Bit;

  // General
  bool needUpdateLines;
  bool needUpdateTris;

  gui::TransferFunctionDlg* workTransferDlg, *mainTransferDlg, *surfTransferDlg, *heatTransferDlg;
  // Function defining the color of the pixels
  std::vector<TransferFunction::Colorf> MainColorMap;   // Color map for the main data
  std::vector<TransferFunction::Colorf> WorkColorMap;   // Color map for the work data
  std::vector<TransferFunction::Colorf> SurfColorMap;   // Color map for the surface
  std::vector<TransferFunction::Colorf> HeatColorMap;   // Color map for the heatmap
  // If true, a new texture need to be sent for the colormap
  bool newMainColorMap, newWorkColorMap, newSurfColorMap, newHeatColorMap;
  std::vector<double> MainHist;   // Histogram of the main data
  std::vector<double> WorkHist;   // Histogram of the working data
  std::vector<double> SurfHist;   // Histogram of the surface data
  std::vector<double> HeatHist;   // Histogram of the surface data
  std::pair<double, double> mainBounds;
  std::pair<double, double> workBounds;

  GLuint mainDataTexId;              // 3D texture Id for main stack
  GLuint workDataTexId;              // 3D texture Id for work stack
  GLuint dataTexColor;               // 3D texture color
  GLuint surfTexId;                  // 1D texture map for surface colors.
  GLuint heatTexId;                  // 1D texture map for heatmap colors.
  GLuint imgTexId;                   // 2D texture map for surface from an image
  GLuint mcmapTexId, wcmapTexId;     // 1D texture map for volume colors.
  GLuint labelTexId;                 // 1D texture map to color indexed texture for labels
  graph::vertex_identity_t lineId;   // Last id when drawing lines
  std::set<uint> selectV;            // List of selected vertices

  Colors::ColorType MeshColor;
  Colors::ColorType MeshBorderColor;
  // Colors::ColorType MeshPointColor;
  Colors::ColorType MeshSelectColor;

  int TCount;   // Triangle count
  int LCount;   // Line count
  int VCount;   // Vertex count

  // Helper arrays for vertex buffer obejects
  std::vector<vertex> idVA;                // Vertex ids, used to find selection in triangle mode (!meshSelect)
  std::vector<vertex> pidVA;               // Point vtx ids, used to find selection in point mode (meshSelect)
  std::unordered_map<vertex, uint> vMap;   // Pointer into idVA by vertex

  // VBO arrays
  std::vector<Point3f> posVA;      // Vertex positions
  std::vector<Point3f> nrmlVA;     // Vertex normals
  std::vector<Point3GLub> selVA;   // Unique colors for triangle selection
  std::vector<Point3f> texVA;      // Mesh labels, signal, and heat
  std::vector<Point2f> imgVA;      // Image texture coordinates

  std::vector<uint> lineVA;    // Indices for drawing lines
  std::vector<uint> lbrdVA;    // Indices for drawing border lines
  std::vector<uint> lcellVA;   // Indices for drawing cell border lines
  std::vector<uint> lselVA;    // Indices for drawing selected lines

  std::vector<uint> pbrdVA;            // Indices for drawing border points
  std::vector<uint> pcellVA;           // Indices for drawing cell border points
  std::vector<uint> pselVA;            // Indices for drawing selected points
  std::vector<Point3f> pntsVA;         // Vertices for drawing points
  std::vector<Point3GLub> pcolVA;      // Colors for points
  std::vector<Point3GLub> pselcolVA;   // Selection colors for points

  GLuint posVAid;   // VBO object ids
  GLuint nrmlVAid;
  GLuint selVAid;
  GLuint texVAid;
  GLuint imgVAid;

  GLuint pntsVAid;
  GLuint pcolVAid;
  GLuint pselVAid;

  GLubyte* texMap;   // Texture map to memory

  // Parameters from stack file
  QString Section;
  std::vector<QString> ImageFiles;

  float min, max;

  // Vectors to make heat maps
  float labelWallBordMin;
  float labelWallBordMax;
  QString sigStr;          // Description of heat map
  IntPoint3fMap LabC;      // Label Centers
  Point3u clipDo;          // Which clipping planes are selected
  Point4f pn[6];           // Clip planes tranformed into frame coordinates
  HVec4F Hpn;              // Host vector for planes for cuda
  Matrix4d frm;            // Frame matrix
  Matrix4d clm[3];         // Clip plane matrices
  float pixelRadius;       // Radius in frame coordinates of pixel edit tool
  BoundingBox3f bBox;      // Bounding box
  BoundingBox3i bBoxTex;   // Bounding box for texture update
  int marchLabel;          // Label for maching cubes on segmented stack

  bool LoadMeshAdd;
  float meshShift;   // Shift for the mesh, relative to the scene radius

  // Variables to marching cubes (for eval)
  HVecUS* marchData;

  bool EnableRotate;

  bool readOnce;   // Used for parameter loading

  // Parameters shared by all instances
  static int ClearColor;
  static uint Slices;
  static uint TileCount;
  static Point3u MaxTexSize;
  static bool defaultTex16Bits;
  static float ZOffset;

  static float DrawNormals;
  static float DrawOffset;
  static float DrawZeroLabels;
  static float DrawNhbds;
  static bool DeleteBadVertex;
  static bool FillWorkData;
  static bool SeedStack;
  static ushort FillWorkValue;
  static int PixelEditRadius;
  static int PixelEditMaxPix;
  static std::vector<Colorf> LabelColors;
  bool LabelColorsChanged;
  static SelectedTool tool;

  static ScaleBar scaleBar;
  static ColorBar colorBar;

  static float MeshPointSize;
  static float MeshLineWidth;

  bool changed;         // Has the stack (mesh) changed
  bool pixelsChanged;   // Changed flag for pixel editing

  QPointer<gui::StackInfoDlg> info_dlg;

public:
  ImgData(int id, QWidget* _parent = 0);

  ~ImgData();

  void init(Stack* s, Mesh* m);

  // Set parameters for function
  void readParms(util::Parms& parms, QString section);

  // Set parameters for function
  void writeParms(QTextStream& pout, QString section);

  // Get the frame with or without additional transform
  qglviewer::ManipulatedFrame& getFrame() {
    return stack->getFrame();
  }

  // Get the main frame
  qglviewer::ManipulatedFrame& getMainFrame() {
    return stack->frame();
  }

  // Get the transform frame
  qglviewer::ManipulatedFrame& getTransFrame() {
    return stack->trans();
  }

  // Marching cubes evaluation function
  int eval(Point3f p);

  // Draw stack data
  void drawStack(Shader* shader);

  // Set the file containing the current color map
  void setColorMap(const QString& pth, bool work);

  // Update heat histogram
  void updateHeatHistogram();
  // Update surface histogram
  void updateSurfHistogram();
  // Update work histogram
  void updateWorkHistogram();
  // Update main histogram
  void updateMainHistogram();

  // Update the color map texture
  void updateSurfColorMap() {
    updateColorMap(newSurfColorMap, SurfColorMap, mesh->surfFct());
  }
  void updateHeatColorMap() {
    updateColorMap(newHeatColorMap, HeatColorMap, mesh->heatFct());
  }
  void updateWorkColorMap() {
    updateColorMap(newWorkColorMap, WorkColorMap, stack->work()->transferFct());
  }
  void updateMainColorMap() {
    updateColorMap(newMainColorMap, MainColorMap, stack->main()->transferFct());
  }
  void updateColorMap(bool& newColorMap, std::vector<TransferFunction::Colorf>& ColorMap,
                      const TransferFunction& transferFct);

  // Setup color map to use in draw
  void setupSurfColorMap() {
    setupColorMap(newSurfColorMap, surfTexId, SurfColorMap, Shader::AT_SURF_TEX);
  }
  void setupHeatColorMap() {
    setupColorMap(newHeatColorMap, heatTexId, HeatColorMap, Shader::AT_HEAT_TEX);
  }
  void setupMainColorMap() {
    setupColorMap(newMainColorMap, mcmapTexId, MainColorMap, Shader::AT_CMAP_TEX);
  }
  void setupMainColorMap2() {
    setupColorMap(newMainColorMap, mcmapTexId, MainColorMap, Shader::AT_SECOND_CMAP_TEX);
  }
  void setupWorkColorMap() {
    setupColorMap(newWorkColorMap, wcmapTexId, WorkColorMap, Shader::AT_CMAP_TEX);
  }
  void setupWorkColorMap2() {
    setupColorMap(newWorkColorMap, wcmapTexId, WorkColorMap, Shader::AT_SECOND_CMAP_TEX);
  }
  void setupLabelColorMap()
  {
    std::vector<TransferFunction::Colorf> empty;
    bool nonew = false;
    setupColorMap(nonew, labelTexId, empty, Shader::AT_LABEL_TEX);
  }
  void setupColorMap(bool& newColorMap, GLuint& cmapTexId, const std::vector<TransferFunction::Colorf>& ColorMap,
                     Shader::ActiveTextures activeTex);

  // Draw mesh wireframe, select renders point only in AUX buffer
  void drawMesh(bool select);

  // Draw lines joining corresponding vertices
  void drawVertexVertexLine(ImgData* stack);

  // Draw cell axis
  void drawCellAxis();

  // Draw Bounding Box around the stack
  void drawBBox();

  // Setup the uniforms and textures to render 3D data
  void setup3DRenderingData(Shader* shader);

  // Draw mesh shaded, select renders unique colors in AUX buffer
  void drawSurf(bool select, Shader* texture_shader = 0, Shader* label_shader = 0, Shader* volume_shader = 0);

  bool showOpaqueSurface() {
    return mesh->isSurfaceVisible() and !mesh->blending();
  }

  bool showTransparentSurface() {
    return mesh->isSurfaceVisible() && mesh->blending() && (mesh->opacity() > 0.0);
  }

  void setupVolumeShader(Shader& shader, int pos);

  // Mark margin vertices and border labels
  void markMargin(std::set<vertex>& M, bool remborders);

  // Check if the graph has matching in-out edges
  void chkGraph();

  // Subdivide bisect, recursively divide to prevent degenerate triangles
  void subdivideBisect(vertex v1, vertex v2, vertex v3);

  // Get all vertices close to a set of border vertices
  void markBorder(float borderSize);

  // Get signal and areas by label
  void getLabelMaps(float borderSize);

  // Test if border triangle
  bool isBordTriangle(int label, vertex v, vertex n, vertex m, IntIntPair& rp);

  // Setup data for clipping test
  void getClipTestData(ClipRegion& clip1, ClipRegion& clip2, ClipRegion& clip3);

  // Test if a point (in image coordinates) is inside clipping planes
  bool clipTest(const Point3i& ip);

  // Routine to connect neighborhoods when loading keyence data
  void makeNhbd(std::vector<vertex>& vtxs);
  void makeNhbd(vvgraph& S, vertex v, vertex n1, vertex n2, vertex n3, vertex n4);
  void makeNhbd(vvgraph& S, vertex v, vertex n1, vertex n2, vertex n3, vertex n4, vertex n5, vertex n6);

  // Save surface for ICP
  int getPointsICP(std::vector<Point3f>& pts, qglviewer::Frame& frame);

  // Reset stack data
  void resetStack();

  // Reset mesh data
  void resetMesh();

  // Initialize controls
  void initControls(QWidget* viewer);

  // Determine if triangle is labeled, all vertices labelled and the same (or -1 = boundary)
  int getLabel(vertex v1, vertex v2, vertex v3, bool useParentLabel = true) const;

  // Texture management
  void unloadTex();
  void loadTex();
  void reloadLabelTex();
  void reloadMainTex(const BoundingBox3i& bbox = BoundingBox3i());
  void reloadWorkTex(const BoundingBox3i& bbox = BoundingBox3i());
  void reloadTex(GLuint texId);
  void updateTex(GLuint texId, BoundingBox3i bBox);
  void initTex();
  void bind3DTex(GLuint texId, Shader::ActiveTextures atexId = Shader::AT_TEX3D);
  void bind2DTex(GLuint texId);
  void unbind3DTex();
  void unbind2DTex();
  void loadImgTex(const QImage& image);

  // Fill selected color with current label (bucket icon)
  void fillLabel(int label, int currlabel);

  // Set parent label
  void setParent(int label, int parentLabel);

  // Select the vertices for given label
  void selectLabel(int label, int repeat = 0);

  // Unselect the vertices for given label
  void unselectLabel(int label);

  // Select the vertices for given parent
  void selectParent(int label, int repeat = 0);

  // Unselect the vertices for given parent
  void unselectParent(int label);

  // Select connected vertices
  void selectConnected(std::vector<uint>& vlist, bool unselect);

  // Put label on a list of vertices (a triangle)
  void addSeed(int label, std::vector<uint>& vlist);

  // Fill selection with current
  void fillSelect(int label);

  // Delete selected vertices
  void deleteLabel(int label);

  // void markLine(const vertex &s, const vertex &d);

  // Set margins and normals and count triangles for VBOs
  void marginNormalsVBOs();

  // Update triangle texture coords
  void updateTris();
  void updateTris(vvgraph& T);

  // Update points and lines
  void updateLines();

  // Update positions
  void updatePos();

  // Fill the vertex array
  void fillVBOs();

  // Correct the selection
  void correctSelection(bool inclusive);

  // Update the selection array
  void updateSelection();

  // Update VBO texture coordinate values
  // RSS: Is this used?
  // void updTexCoord();

  // Update a list of vertex positions in the VBO
  void updPos(std::set<uint>& vlist, bool points);

  // Find selected triangle in triangle select mode (!meshSelect)
  // label is set to -1 if no triangle is clicked
  void findSelectTriangle(uint x, uint y, std::vector<uint>& vlist, int& label, bool useParentLabel = true);

  // Find selected point in mesh select mode (meshSelect)
  int findSelectPoint(uint x, uint y);

  // Average a list of vertices to find select point
  bool findSeedPoint(uint x, uint y, CutSurf& cutSurf, Point3f& p);

  // Clear mesh selection
  void clearMeshSelect();

  // Check if there is a selection
  bool hasSelectedVertices();

  // Add point to selection
  void addSelect(const std::vector<uint>& vlist);

  // Remove points from selection
  void removeSelect(const std::vector<uint>& vlist);

  // Update selection colors
  void updateVertexesColors(const std::vector<uint>& vlist);

  // Apply a single clip plane to the bounding box
  void bBoxClip(BoundingBox3f& bBox, Point3f p, Point3f n);

  // Make a bounding box from the clipping planes
  void bBoxFromClip();

  // Start pixel editing
  void pixelEditStart(ClipRegion& clip1, ClipRegion& clip2, ClipRegion& clip3);

  // Pixel editing operations
  // p, px, py and pz should be in world coordinates
  void pixelEdit(float pixelRadius, const Point3f& p, const Point3f& px, const Point3f& py, const Point3f& pz,
                 bool doCut, int currentLabel);

  // Stop pixel editing
  void pixelEditStop();

  bool isMainVisible() const {
    return stack->main()->isVisible() and mainDataTexId;
  }
  bool isWorkVisible() const {
    return stack->work()->isVisible() and workDataTexId;
  }
  bool isVisible() const {
    return isMainVisible() or isWorkVisible();
  }

  HVecUS& currentData()
  {
    if(stack->work()->isVisible())
      return stack->work()->data();
    return stack->main()->data();
  }
  const HVecUS& currentData() const
  {
    if(stack->work()->isVisible())
      return stack->work()->data();
    return stack->main()->data();
  }
  Point3f imageGradientW(Point3d worldpos);
  Point3f imageGradientI(Point3i ipos);
  uint imageLevel(Point3d worldpos);
  Point2i imageMinMax();

  GLenum interpolation(GLuint texId)
  {
    if(texId == workDataTexId)
      return stack->work()->labels() ? GL_NEAREST : GL_LINEAR;
    else
      return stack->main()->labels() ? GL_NEAREST : GL_LINEAR;
  }

  GLenum internalFormat(GLuint texId)
  {
    if(texId == workDataTexId)
      return Work16Bit ? GL_ALPHA16 : GL_ALPHA8;
    else
      return Main16Bit ? GL_ALPHA16 : GL_ALPHA8;
  }

  GLenum swapTextureBytes(GLuint texId)
  {
    if(texId == workDataTexId)
      return stack->work()->labels();
    else
      return stack->main()->labels();
  }

  // Set stack sizes
  void updateStackSize();

  // Change the texture scale of the stack
  void setTexScale(float s);

  bool valid() const {
    return stack->storeSize() > 0 and (mainDataTexId or workDataTexId);
  }

  /// Check if the current tool is meshSelect
  static bool meshSelect();

  /// Checks if 3D textures need to be reloaded
  void check3DTexture();

protected:
  // Update a list of vertex labels
  void updLabel(int label, std::vector<uint>& vlist);

  // Add a triangle to the VV mesh when converting from unoriented mesh (triangles must be oriented)
  bool addTriangle(std::vector<vertex>& vertices, Point3i tri);

  // Load 3D texture data, possibly downsampling.
  void load3DTexData(const GLuint texId, const Point3u size, const ushort* data);

  // Get step for texture decimation
  Point3u getTexStep();

public:
  // Clip texture coordinates
  float trimTex(const float val) {
    return (trim(val, 0.0f, 1.0f));
  }

  Point2f trimTex(const Point2f& val) {
    return (Point2f(trimTex(val.x()), trimTex(val.y())));
  }

protected:
  // Return unique triangles
  static bool uniqueTri(const vvgraph& S, const vertex& v, const vertex& n, const vertex& m)
  {
    if(v.id() <= n.id() or v.id() <= m.id() or n.id() == m.id() or !S.edge(v, n) or !S.edge(v, m) or !S.edge(n, m))
      return false;
    else
      return true;
  }

  // Return unique lines
  static bool uniqueLine(const vvgraph& S, const vertex& v, const vertex& n)
  {
    if(v.id() <= n.id() or !S.edge(v, n))
      return false;
    else
      return true;
  }

  // Test if quad out of bounds, used for clipping 3D data
  bool testQuad(float a, float b, float c, float d, float x)
  {
    if(a > x and b > x and c > x and d > x)
      return (true);
    if(a < -x and b < -x and c < -x and d < -x)
      return (true);
    return (false);
  }

  // See if point should be drawn
  //  bool pDraw(const vertex &v)
  //  {
  //    // Choose which vertices to draw
  //    if(MeshBorder and !v->margin)
  //      return false;
  //    if(MeshCell and v->label != -1)
  //      return false;
  //    return true;
  //  }

  // Get point color
  Point3GLub pColor(const vertex& v)
  {
    // Get the color
    Color4f c;
    if(v->selected)
      c = Colors::getColor(MeshSelectColor);
    else if(v->minb != 0 or v->label < 0)
      c = Colors::getColor(MeshBorderColor);
    else
      c = Colors::getColor(MeshColor);

    return (Point3GLub(255 * c.x(), 255 * c.y(), 255 * c.z()));
  }

  // Get line color, and if it should be drawn
  bool lDraw(const vertex& a, const vertex& b)
  {
    // Choose which line to draw
    if((mesh->meshView() == Mesh::BORDER_MESH)and !(a->margin and b->margin))
      return false;
    if((mesh->meshView() == Mesh::CELL_MESH)and !(a->label == -1 and b->label == -1))
      return false;
    return true;
  }

  // Compute offset for image data including border
  size_t offset(uint x, uint y, uint z) {
    return stack->offset(x, y, z);
  }

  size_t offset(Point3i ipos) {
    return stack->offset(ipos);
  }

  // Check if in bounds
  bool boundsOK(int x, int y, int z)
  {
    if(x < 0 or y < 0 or z < 0 or x >= int(stack->size().x()) or y >= int(stack->size().y())
       or z >= int(stack->size().z()))
      return (false);
    else
      return (true);
  }

public:
  /// Go from image coordinates to world coordinates
  template <typename T> Point3f imageToWorld(const util::Vector<3, T>& img) const
  {
    return stack->imageToWorld<T>(img);
  }

  /// Go from world coordinates to image coordinates
  template <typename T> util::Vector<3, T> worldToImage(Point3f wrld) const {
    return stack->worldToImage<T>(wrld);
  }

  Point3f worldToImagef(const Point3f& a) const {
    return worldToImage<float>(a);
  }
  Point3i worldToImagei(const Point3f& a) const {
    return worldToImage<int>(a);
  }
  Point3u worldToImageu(const Point3f& a) const {
    return worldToImage<uint>(a);
  }

  // Map scale slider values
  int toSliderScale(float s);
  float fromSliderScale(int i);

protected:
  void updateHistogram(std::vector<double>& hist, const HVecUS& data, std::pair<double, double>& minMaxValues,
                       int max_data = 1 << 16, int size = 512);
  void invalidateHistogram(std::vector<double>& hist) {
    hist.clear();
  }

  // Clear 3D stack texture
  void clearData(HVecUS& data)
  {
    data.resize(stack->storeSize());
    memset(data.data(), 0, data.size() * sizeof(ushort));
  }

  // Return texture info for a vertex (this version used to update color when drawing on mesh)
  Point3f texCoord(int label, vertex v);

  // Return texture info for a vertex (this version used to fill VBOs and respects wall heatmap)
  Point3f texCoord(int label, vertex v, vertex a, vertex b, vertex c);

  // Set label on triangle
  bool setLabel(vertex v1, vertex v2, vertex v3, int label)
  {
    bool ok = true;
    if(v1->label > 0 and v1->label != label)
      ok = false;
    if(v2->label > 0 and v2->label != label)
      ok = false;
    if(v3->label > 0 and v3->label != label)
      ok = false;

    if(ok) {
      if(v1->label == 0)
        v1->label = label;
      if(v2->label == 0)
        v2->label = label;
      if(v3->label == 0)
        v3->label = label;
    }
    return (ok);
  }

public slots:

  // Edit the transfer function
  void editMainTransferFunction();
  void editWorkTransferFunction();
  void editSurfTransferFunction();
  void editHeatTransferFunction();

  // Update the colors of all points in the mesh
  void updateColors();

  // Slots for controls
  void showMain(bool val = true);
  void setMainBrightness(int val);
  void setMainOpacity(int val);
  void setMainLabel(bool val = true);
  void setMain16Bits(bool val = true);

  void showWork(bool val = true);
  void setWorkBrightness(int val);
  void setWorkOpacity(int val);
  void setWorkLabels(bool val = true);
  void setWork16Bits(bool val = true);

  void showSurf(bool val = true);
  void setSurfBrightness(int val);
  void setSurfOpacity(int val);
  void setSurfBlend(bool val = true);
  void setSurfCull(bool val = true);

  void setSurfView(int item);

  void setSurfSignal(bool val);
  void setSurfTexture(bool val);
  void setSurfImage(bool val);

  void showMesh(bool val);
  void setChangeMeshViewMode(int mode);
  void setMeshLines(bool val);
  void setMeshPoints(bool val);

  void setCellMap(bool val);
  void setShowTrans(bool val);
  void setShowScale(bool val);
  void setShowBBox(bool val);
  void setTieScales(bool val);
  void setScaleX(int val);
  void setScaleY(int val);
  void setScaleZ(int val);

  // Slots to change transfer functions
  void setSurfColorMap(const TransferFunction& fct);
  void setHeatColorMap(const TransferFunction& fct);
  void setWorkColorMap(const TransferFunction& fct);
  void setMainColorMap(const TransferFunction& fct);

  // Slots to detect changes in transfer fct dialog boxes
  void changeSurfColorMap(const TransferFunction& fct);
  void changeHeatColorMap(const TransferFunction& fct);
  void changeWorkColorMap(const TransferFunction& fct);
  void changeMainColorMap(const TransferFunction& fct);

  // Perform the updates of lines or tris
  // This function must be called from the main thread of the application!
  void updateImgData();
  // Called when the lines need updating
  void needsLinesUpdate();
  // Called when the traignales need updating
  void needsTriangleUpdate();

  void showInfo();

signals:
  void needsUpdate();
  void viewerUpdate();
  void updateSliderScale();
  void changedInterface();
  // void changeSize(uint x, uint y, uint z, float xstep, float ystep, float zstep);
  void changeSize(const Point3u& size, const Point3f& step, const Point3f& origin);
  void stackUnloaded();
  void updatedImgData();
};
} // namespace lgx
#endif /*IMAGE_DATA_H*/
