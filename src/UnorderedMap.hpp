/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef UNORDEREDMAP_HPP
#define UNORDEREDMAP_HPP

#include <LGXConfig.hpp>

#if (__cplusplus >= 201103L) || (defined(_MSC_VER) && _MSC_VER >= 1700)
#  include <unordered_map>
#elif defined(__GNUC__)
#  include <Features.hpp>
#  if __GNUC_PREREQ(4, 0)
#    include <tr1/unordered_map>
namespace std {
using namespace std::tr1;
}
#    ifndef HASH_NEED_TR1
#      define HASH_NEED_TR1
#    endif
#  else
#    error "GCC do not have unordered_set before the version 4.0"
#  endif
#else
#  pragma message ("warning: unordered_map is not identified for your compiler. Assumes it's defined as in C++11.")
#  include <unordered_map>
#endif

#endif // UNORDEREDMAP_HPP
