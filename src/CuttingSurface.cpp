/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "CuttingSurface.hpp"
#include "Information.hpp"

namespace lgx {

using qglviewer::Vec;

CuttingSurface::CuttingSurface()
{
}

CuttingSurface::~CuttingSurface() {
}

void CuttingSurface::getSurfPoints(const qglviewer::Frame* stk_frame, std::vector<Point3f>& points, int& uSize,
                                   int& vSize)
{
  uSize = _surfSize.x();
  vSize = _surfSize.y();

  for(int u = 0; u < uSize; u++)
    for(int v = 0; v < vSize; v++) {
      Point3f p = evalCoord(float(u) / float(uSize - 1), float(v) / float(vSize - 1));
      if(stk_frame)
        points.push_back(Point3f(stk_frame->coordinatesOf(_frame.inverseCoordinatesOf(Vec(p)))));
      else
        points.push_back(p);
    }
}

int CuttingSurface::choose(int n, int k) const
{
  if(n > int(_bezPoints) || k > n) {
    SETSTATUS("CutSurf::Choose:Error Can't compute " << n << " choose " << k);
    return 0;
  }

  // Just grab value from the table
  return _chooseV[idx(n, k)];
}

Point3f CuttingSurface::evalCoord(float u, float v) const
{
  // Make sure u and v between 0.0 and 1.0
  u = trim(u, 0.0f, 1.0f);
  v = trim(v, 0.0f, 1.0f);
  // Set Pointer to bezier points
  // Evaluate
  Point3f pos;
  if(bezier()) {
    for(size_t i = 0, k = 0; i < _bezPoints; i++)
      for(size_t j = 0; j < _bezPoints; j++, k++) {
        float s = (float(choose(_bezPoints - 1, i)) * pow(u, (float)i)
                   * pow(1.0f - u, (float)(_bezPoints - 1 - i)) * float(choose(_bezPoints - 1, j))
                   * pow(v, (float)j) * pow(1.0f - v, (float)(_bezPoints - 1 - j)));
        pos += s * _bezierV[k];
      }
  } else {
    pos.x() = 2 * (u - .5) * _size.x();
    pos.y() = 2 * (v - .5) * _size.y();
  }
  return pos;
}

Point3f CuttingSurface::evalNormal(float u, float v) const
{
  // Make sure u and v between 0.0 and 1.0
  u = trim(u, 0.0f, 1.0f);
  v = trim(v, 0.0f, 1.0f);
  Point3f du;
  Point3f dv;
  if(u < 0.05)
    du = (evalCoord(u + 0.05, v) - evalCoord(u, v)) / 0.05;
  else if(u > 0.95)
    du = (evalCoord(u, v) - evalCoord(u - 0.05, v)) / 0.05;
  else
    du = (evalCoord(u + 0.05, v) - evalCoord(u - 0.05, v)) / 0.1;
  if(v < 0.05)
    dv = (evalCoord(u, v + 0.05) - evalCoord(u, v)) / 0.05;
  else if(v > 0.95)
    dv = (evalCoord(u, v) - evalCoord(u, v - 0.05)) / 0.05;
  else
    dv = (evalCoord(u, v + 0.05) - evalCoord(u, v - 0.05)) / 0.1;
  return normalized(du ^ dv);
}

void CuttingSurface::initBez()
{
  _chooseV.resize(_bezPoints * _bezPoints);
  _bezierV.resize(_bezPoints * _bezPoints);

  // Fill in tables for binomial coefficients and patch points
  for(uint v = 0; v < _bezPoints; v++)
    for(uint u = 0; u < _bezPoints; u++) {
      if(u < v)
        _chooseV[idx(u, v)] = 0.0f;
      else if(v == 0 || v == u)
        _chooseV[idx(u, v)] = 1.0f;
      else
        _chooseV[idx(u, v)] = _chooseV[idx(u - 1, v)] + _chooseV[idx(u - 1, v - 1)];
      _bezierV[idx(u, v)]
        = Point3f(-0.5f + float(u) / float(_bezPoints - 1), -0.5f + float(v) / float(_bezPoints - 1), 0.0f);
    }
}
} // namespace lgx
