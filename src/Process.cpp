/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Process.hpp"

#include "Dir.hpp"
#include "Forall.hpp"
#include "ImageData.hpp"
#include "Information.hpp"
#include "PrivateProcess.hpp"
#include "ProcessThread.hpp"
#include "Progress.hpp"
#include "LGXVersion.hpp"

#include <exception>
#include <memory>
#include <QMutexLocker>
#include <QTextStream>
#include <stdio.h>

static QTextStream err(stderr);

namespace lgx {

using namespace util;

namespace process {
LGX_EXPORT unsigned int Process::processVersion = PROCESS_VERSION;

Process::Process()
  : p(0)
{
}

Process::Process(const Process& p)
  : p(p.p)
{
}

QString Process::pythonCall(const QStringList& parms) const
{
  QStringList all_params;
  forall(QString s, parms)
    all_params << QString("'%1'").arg(shield_python(s));
  QString name = this->name();
  name.replace(' ', "_");
  return QString("%1.%2(%3)").arg(type()).arg(name).arg(all_params.join(", "));
}

QString Process::actingFile() const {
  return p->actingFile;
}

void Process::actingFile(const QString& filename, bool project_file)
{
  if(!filename.isEmpty() and (project_file or file().isEmpty())) {
    DEBUG_OUTPUT("Entering actingFile (" << filename << ")- project_file = " << project_file << endl);
    QString dir = getDir(filename);
    bool changed_dir = (dir != currentPath());
    setCurrentPath(dir);
    p->actingFile = filename;
    DEBUG_OUTPUT("## Change dir to = '" << dir << "'\n"
                                        << "## Current dir = '" << currentPath() << "'" << endl);
    if((project_file and file().isEmpty()) or changed_dir) {
      DEBUG_OUTPUT("Writing LithoGraphX.py" << endl);
      QFile f("LithoGraphX.py");
      if(f.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text)) {
        QTextStream ss(&f);
        QDateTime now = QDateTime::currentDateTime();
        ss << "######## New LithoGraphX session v" << VERSION << " r" << REVISION << ": "
           << now.toString("yyyy-MM-dd hh:mm:ss") << endl;
        ss << p->currentPythonCall << endl;
        p->changedFolder = true;
        f.close();
      }
      DEBUG_OUTPUT("Written LithoGraphX.py");
    }
    DEBUG_OUTPUT("Leaving actingFile");
  }
}

int Process::stackCount() const {
  return p->_stacks.size();
}

std::pair<Process::stack_iterator, Process::stack_iterator> Process::stacks()
{
  return std::make_pair(p->_stacks.begin(), p->_stacks.end());
}

std::pair<Process::const_stack_iterator, Process::const_stack_iterator> Process::stacks() const
{
  return std::make_pair(p->_stacks.begin(), p->_stacks.end());
}

Stack* Process::stack(int i)
{
  if(i == -1)
    return currentStack();
  if(i < 0 or i >= (int)p->_stacks.size())
    return 0;
  return p->_stacks[i];
}

Stack* Process::currentStack()
{
  if(currentStackId() == -1)
    return 0;
  return p->_stacks[currentStackId()];
}

int Process::currentStackId() const {
  return p->current_stack;
}

void Process::setCurrentStackId(int i)
{
  if(i >= -1 and i < (int)p->_stacks.size())
    p->current_stack = i;
}

Stack* Process::addStack()
{
  Stack* s = new Stack((int)p->_stacks.size());
  p->_stacks.push_back(s);
  return s;
}

bool Process::deleteStack(int i)
{
  if(i >= 0 and i < (int)p->_stacks.size()) {
    delete p->_stacks[i];
    p->_stacks[i] = 0;
    p->_stacks.erase(p->_stacks.begin() + i);
    for(int j = 0; j < (int)p->_stacks.size(); ++j)
      p->_stacks[j]->setId(j);
    return true;
  }
  return false;
}

int Process::meshCount() const {
  return p->_meshes.size();
}

std::pair<Process::mesh_iterator, Process::mesh_iterator> Process::meshes()
{
  return std::make_pair(p->_meshes.begin(), p->_meshes.end());
}

std::pair<Process::const_mesh_iterator, Process::const_mesh_iterator> Process::meshes() const
{
  return std::make_pair(p->_meshes.begin(), p->_meshes.end());
}

Mesh* Process::mesh(int i)
{
  if(i == -1)
    return currentMesh();
  return p->_meshes[i];
}

Mesh* Process::currentMesh()
{
  if(currentMeshId() == -1)
    return 0;
  return p->_meshes[currentMeshId()];
}

int Process::currentMeshId() const {
  return p->current_mesh;
}

void Process::setCurrentMeshId(int i)
{
  if(i >= -1 and i < (int)p->_meshes.size())
    p->current_mesh = i;
}

Mesh* Process::addMesh(const Stack* stack)
{
  Mesh* m = new Mesh((int)p->_meshes.size(), stack);
  p->_meshes.push_back(m);
  return m;
}

bool Process::deleteMesh(int i)
{
  if(i >= 0 and i < (int)p->_meshes.size()) {
    delete p->_meshes[i];
    p->_meshes[i] = 0;
    p->_meshes.erase(p->_meshes.begin() + i);
    for(int j = 0; j < (int)p->_meshes.size(); ++j)
      p->_meshes[j]->setId(j);
    return true;
  }
  return false;
}

int Process::selectedLabel() const {
  return p->selected_label;
}

void Process::setSelectedLabel(int label) {
  p->selected_label = label;
}

float Process::globalBrightness() {
  return p->global_brightness;
}

float Process::globalContrast() {
  return p->global_contrast;
}

void Process::setGlobalBrightness(float value)
{
  if(value > 1)
    p->global_brightness = 1.0f;
  else if(value < -1)
    p->global_brightness = -1.0f;
  else
    p->global_brightness = value;
}

void Process::setGlobalContrast(float value)
{
  if(value > 2)
    p->global_contrast = 2.0f;
  else if(value < 0)
    p->global_contrast = 0.0f;
  else
    p->global_contrast = value;
}

bool Process::meshSelection() const {
  return p->mesh_selection;
}

bool Process::lineBorderSelection() const {
  return p->line_border_selection;
}

bool Process::setErrorMessage(const QString& str)
{
  p->error = str;
  return false;
}

QString Process::errorMessage() const {
  return p->error;
}

void Process::setWarningMessage(const QString& str) {
  p->warning = str;
}

QString Process::warningMessage() const {
  return p->warning;
}

void Process::updateState()
{
  // Hold cuda mem to protect from GL
  systemCommand(UPDATE_STRUCTURE, QStringList());
}

bool Process::systemCommand(SystemCommand cmd, const QStringList& parms)
{
  QMutexLocker locker(&p->lock);
  p->success = false;
  QCoreApplication::postEvent(p->parent, new ProcessSystemCommand(this, cmd, parms));
  p->updated.wait(&p->lock);
  return p->success;
}

bool GlobalProcess::setCurrentStack(int id, STORE which)
{
  if(id >= stackCount()) {
    setErrorMessage(
      QString("There are only %1 stacks, cannot made stack %2 the current one").arg(stackCount()).arg(id));
    return false;
  }
  return systemCommand(SET_CURRENT_STACK, QStringList() << storeToString(which) << QString::number(id));
}

bool GlobalProcess::resetProject() {
  return systemCommand(RESET_PROJECT, QStringList());
}

bool GlobalProcess::takeSnapshot(QString filename, float overSampling, int width, int height, int quality,
                                 bool expand_frustum)
{
  QStringList parms;
  parms << filename << boolToString(expand_frustum) << QString::number(width) << QString::number(height)
        << QString::number(quality) << QString::number(overSampling);
  return systemCommand(TAKE_SNAPSHOT, parms);
}

void Process::updateViewer() {
  QCoreApplication::postEvent(p->parent, new ProcessUpdateViewEvent());
}

Clip* Process::clip1() {
  return &p->clip1;
}

Clip* Process::clip2() {
  return &p->clip2;
}

Clip* Process::clip3() {
  return &p->clip3;
}

const Clip* Process::clip1() const {
  return &p->clip1;
}

const Clip* Process::clip2() const {
  return &p->clip2;
}

const Clip* Process::clip3() const {
  return &p->clip3;
}

CuttingSurface* Process::cuttingSurface() {
  return &p->cuttingSurface;
}

const CuttingSurface* Process::cuttingSurface() const {
  return &p->cuttingSurface;
}

const QString& Process::file() const {
  return p->filename;
}

QMap<QString, ProcessDefinition<StackProcess> > stackProcesses;
QMap<QString, ProcessDefinition<MeshProcess> > meshProcesses;
QMap<QString, ProcessDefinition<GlobalProcess> > globalProcesses;

template <typename P>
static const BaseProcessDefinition* getBaseProcessDefinition(const QMap<QString, ProcessDefinition<P> >& processes,
                                                             const QString& processName)
{
  if(!processes.contains(processName))
    return 0;
  BaseProcessDefinition& def = processes[processName];
  return &def;
}

template <typename P>
static BaseProcessDefinition* getBaseProcessDefinition(QMap<QString, ProcessDefinition<P> >& processes,
                                                       const QString& processName)
{
  if(!processes.contains(processName))
    return 0;
  BaseProcessDefinition& def = processes[processName];
  return &def;
}

BaseProcessDefinition* getBaseProcessDefinition(const QString& processType, const QString& processName)
{
  if(processType == "Stack")
    return getBaseProcessDefinition(stackProcesses, processName);
  if(processType == "Mesh")
    return getBaseProcessDefinition(meshProcesses, processName);
  if(processType == "Global")
    return getBaseProcessDefinition(globalProcesses, processName);
  return 0;
}

bool getLastParms(const Process& proc, QStringList& parms) {
  return getLastParms(proc.type(), proc.name(), parms);
}

bool getLastParms(const QString& processType, const QString& processName, QStringList& parms)
{
  BaseProcessDefinition* def = getBaseProcessDefinition(processType, processName);
  if(!def)
    return false;
  parms = def->parms;
  return true;
}

bool getDefaultParms(const Process& proc, QStringList& parms)
{
  return getDefaultParms(proc.type(), proc.name(), parms);
}

bool getDefaultParms(const QString& processType, const QString& processName, QStringList& parms)
{
  SetupProcess setup;
  Process* p = setup.makeProcess(processType, processName);
  if(!p)
    return false;
  parms = p->parmDefaults();
  delete p;
  return true;
}

bool saveDefaultParms(const Process& proc, const QStringList& parms)
{
  return saveDefaultParms(proc.type(), proc.name(), parms);
}

bool saveDefaultParms(const QString& processType, const QString& processName, const QStringList& parms)
{
  BaseProcessDefinition* def = getBaseProcessDefinition(processType, processName);
  if(!def)
    return false;
  if(parms.size() != def->parms.size())
    return false;
  def->parms = parms;
  return true;
}

bool checkProcessParms(const Process& proc, const QStringList& parms, size_t* nbParms)
{
  return checkProcessParms(proc.type(), proc.name(), parms, nbParms);
}

bool checkProcessParms(const QString& processType, const QString& processName, const QStringList& parms,
                       size_t* nbParms)
{
  BaseProcessDefinition* def = getBaseProcessDefinition(processType, processName);
  if(!def)
    return false;
  if(nbParms)
    *nbParms = def->parmNames.size();
  // if(nbNamedStrings) *nbStringsValues = def->nbStringsValues;
  // if(nbNamedValues) *nbNamedValues = def->nbNamedValues;
  if(parms.size() != def->parms.size())
    return false;
  return true;
}

template <typename P, typename P1>
static P* makeProcess(const QMap<QString, ProcessDefinition<P> >& processes,
                      const QString& processName, const QString& processType,
                      const P1& process)
{
  if(!processes.contains(processName))
    throw NoSuchProcess(processName, processType);
  P* proc = processes[processName].factory(process);
  return proc;
}

Process* Process::makeProcess(const QString& processType, const QString& processName)
{
  if(processType == "Stack") {
    const StackProcess* real_process = dynamic_cast<const StackProcess*>(this);
    if(real_process)
      return process::makeProcess(stackProcesses, processName, processType, *real_process);
  }
  if(processType == "Mesh") {
    const MeshProcess* real_process = dynamic_cast<const MeshProcess*>(this);
    if(real_process)
      return process::makeProcess(meshProcesses, processName, processType, *real_process);
  }
  if(processType == "Global") {
    const GlobalProcess* real_process = dynamic_cast<const GlobalProcess*>(this);
    if(real_process)
      return process::makeProcess(globalProcesses, processName, processType, *real_process);
  }
  throw BadProcessType(type(), processType);
}

QStringList listProcesses(const QString& processType)
{
  QStringList res;

  if(processType == "Stack") {
    forall(BaseProcessDefinition& def, stackProcesses)
      res << def.name;
  } else if(processType == "Mesh") {
    forall(BaseProcessDefinition& def, meshProcesses)
      res << def.name;
  } else if(processType == "Global") {
    forall(BaseProcessDefinition& def, globalProcesses)
      res << def.name;
  }
  return res;
}

bool validProcessType(const QString& processType)
{
  return ((processType == "Stack")or (processType == "Mesh") or (processType == "Global"));
}

bool validProcessName(const QString& processType, const QString& processName)
{
  if(processType == "Stack") {
    return stackProcesses.contains(processName);
  } else if(processType == "Mesh") {
    return meshProcesses.contains(processName);
  } else if(processType == "Global") {
    return globalProcesses.contains(processName);
  }
  return false;
}

bool stringToBool(const QString& string)
{
  static QStringList t = QStringList() << "t"
                                       << "true"
                                       << "on"
                                       << "yes"
                                       << "y"
                                       << "1";
  return t.contains(string.toLower());
}

STORE stringToStore(const QString& str)
{
  auto low = str.toLower();
  if(low == "main")
    return MAIN_STORE;
  else if(low == "work")
    return WORK_STORE;
  return CURRENT_STORE;
}

QString storeToString(STORE which)
{
  switch(which) {
    case MAIN_STORE:
      return "Main";
    case WORK_STORE:
      return "Work";
    case CURRENT_STORE:
      return "";
  }
  return "";
}

bool Process::runProcess(const QString& processType, const QString& processName, const QStringList& parms) throw()
{
  try {
    std::unique_ptr<Process> proc(makeProcess(processType, processName));
    if(not proc.get())
      return setErrorMessage(QString("Error, cannot create process %1.%2").arg(processType).arg(processName));
    if(!checkProcessParms(processType, processName, parms))
      return setErrorMessage(
        QString("Error, incorrect parameters for process %1.%2").arg(processType).arg(processName));
    return runProcess(*proc, parms);
  }
  catch(std::exception& ex) {
    return setErrorMessage(
      QString("Exception caught while creating process %1.%2: %3").arg(processType).arg(processName).arg(
        ex.what()));
  }
  catch(...) {
    return setErrorMessage(
      QString("Unknown exception caught while creating process %1.%2.").arg(processType).arg(processName));
  }
}

bool Process::runProcess(Process& proc, const QStringList& parms) throw()
{
  try {
    if(!proc(parms))
      return setErrorMessage(proc.errorMessage());
  }
  catch(QString& err) {
    return setErrorMessage(QString("Error in %1.%2:\n%3").arg(proc.type()).arg(proc.name()).arg(err));
  }
  catch(std::string& err) {
    return setErrorMessage(QString("Error in %1.%2:\n%3").arg(proc.type()).arg(proc.name()).arg(err.c_str()));
  }
  catch(std::exception& ex) {
    return setErrorMessage(QString("Error in %1.%2:\n%3").arg(proc.type()).arg(proc.name()).arg(ex.what()));
  }
  catch(...) {
    setErrorMessage(QString("Error in %1.%2:\nUnknown C++ exception").arg(proc.type()).arg(proc.name()));
    return false;
  }
  return true;
}

QString Process::stackError(int type, int i)
{
  QString msg;
  if(i == -1)
    msg = "the selected stack to be %1";
  else
    msg = QString("the stack %1 to be %2").arg(i);
  QStringList what;
  if(type == STORE_CURRENT)
    what << "valid";
  else {
    if(type & STACK_NON_EMPTY)
      what << "non-empty";
    if(type & STACK_VISIBLE)
      what << "visible";
    if(type & STACK_EMPTY)
      what << "empty";
    if(type & STACK_SCALED)
      what << "scaled";
    if(type & STACK_TRANSFORMED)
      what << "transformed";
    if(type & STACK_NON_SCALED)
      what << "not scaled";
    if(type & STACK_NON_TRANSFORMED)
      what << "not transformed";
  }
  return msg.arg(what.join(", "));
}

QString Process::storeError(int type, int i)
{
  QString msg;
  if(i == -1)
    msg = "the current%1 stack to be %2";
  else
    msg = QString("the%2 stack %1 to be %3").arg(i);
  QString which;
  QStringList what;
  if(type & STORE_WORK)
    which = " work";
  else if(type & STORE_MAIN)
    which = " main";
  else
    which = "";
  if(type == STORE_CURRENT)
    what << "valid";
  else {
    if(type & STORE_VISIBLE)
      what << "visible";
    if(type & STORE_NON_EMPTY)
      what << "non-empty";
    if(type & STORE_EMPTY)
      what << "empty";
    if(type & STORE_LABEL)
      what << "labeled";
    else if(type & STORE_NON_LABEL)
      what << "non-labeled";
    if(type & STORE_SCALED)
      what << "scaled";
    if(type & STORE_TRANSFORMED)
      what << "transformed";
    if(type & STORE_NON_SCALED)
      what << "not scaled";
    if(type & STORE_NON_TRANSFORMED)
      what << "not transformed";
  }
  return msg.arg(which).arg(what.join(", "));
}

QString Process::meshError(int type, int i)
{
  QString msg;
  if(i == -1)
    msg = "the selected mesh to be %1";
  else
    msg = QString("the mesh %1 to be %2").arg(i);
  QStringList what;
  if(type == MESH_ANY)
    what << "valid";
  else {
    if(type & MESH_VISIBLE)
      what << "visible";
    if(type & MESH_NON_EMPTY)
      what << "non-empty";

    if(type & MESH_HEAT)
      what << "showing heat";
    if(type & MESH_LABEL)
      what << "showing label";
    if(type & MESH_PARENT)
      what << "showing parents";
    if(type & MESH_LABEL_PARENT)
      what << "showing labels or parents";
    if(type & MESH_NORMAL)
      what << "showing normal";
    if(type & MESH_SIGNAL)
      what << "showing signal";
    if(type & MESH_TEXTURE)
      what << "showing texture";
    if(type & MESH_IMAGE)
      what << "showing image";
    if(type & MESH_SHOW_MESH)
      what << "showing mesh";
    if(type & MESH_SHOW_SURF)
      what << "showing surface";
    if(type & MESH_ALL)
      what << "showing the whole mesh";
    if(type & MESH_BORDER)
      what << "showing the border";
    if(type & MESH_CELLMAP)
      what << "showing the cell mapping";
    if(type & MESH_CELLS)
      what << "a cell mesh";
    if(type & MESH_IMG_TEX)
      what << "haveing an image texture";
    if(type & MESH_SCALED)
      what << "scaled";
    if(type & MESH_TRANSFORMED)
      what << "transformed";
    if(type & MESH_EMPTY)
      what << "empty";
    if(type & MESH_NON_CELLS)
      what << "not a cell mesh";
    if(type & MESH_NON_IMG_TEX)
      what << "nat having an image texture";
    if(type & MESH_NON_SCALED)
      what << "not scaled";
    if(type & MESH_NON_TRANSFORMED)
      what << "not transformed";
  }
  return msg.arg(what.join(", "));
}

bool Process::stackCheck(int checks, int which)
{
  Stack* s = (which == CHECK_CURRENT) ? currentStack() : stack(which);
  return (s and not (checks & STACK_NON_EMPTY and s->empty())
          and not (checks & STACK_VISIBLE and not (s->main()->isVisible() or s->work()->isVisible()))
          and not (checks & STACK_EMPTY and not s->empty())and not (checks & STACK_SCALED and not s->showScale())
          and not (checks & STACK_TRANSFORMED and not s->showTrans())
          and not (checks & STACK_NON_SCALED and s->showScale())
          and not (checks & STACK_NON_TRANSFORMED and s->showTrans()));
}

bool Process::storeCheck(int checks, int which)
{
  Stack* s = (which == CHECK_CURRENT) ? currentStack() : stack(which);
  if(!s)
    return false;
  Store* store = 0;
  if(checks & STORE_MAIN)
    store = s->main();
  else if(checks & STORE_WORK)
    store = s->work();
  else
    store = s->currentStore();
  return (store and not (checks & STORE_NON_EMPTY and store->empty())
          and not (checks & STORE_VISIBLE and not store->isVisible())
          and not (checks & STORE_EMPTY and not store->empty())
          and not (checks & STORE_LABEL and not store->labels())
          and not (checks & STORE_NON_LABEL and store->labels())
          and not (checks & STORE_SCALED and not store->stack()->showScale())
          and not (checks & STORE_TRANSFORMED and not store->stack()->showTrans())
          and not (checks & STORE_NON_SCALED and store->stack()->showScale())
          and not (checks & STORE_NON_TRANSFORMED and store->stack()->showTrans()));
}

bool Process::meshCheck(int checks, int which)
{
  Mesh* m = (which == CHECK_CURRENT) ? currentMesh() : mesh(which);
  return (m and not (checks & MESH_NON_EMPTY and m->empty())
          and not (checks & MESH_VISIBLE and not (m->isSurfaceVisible() or m->isMeshVisible()))
          and not (checks & MESH_HEAT and not (m->toShow() == Mesh::HEAT))
          and not (checks & MESH_LABEL and not (m->toShow() == Mesh::LABEL))
          and not (checks & MESH_PARENT and not (m->toShow() == Mesh::PARENT))
          and not (checks & MESH_LABEL_PARENT and not (m->toShow() == Mesh::LABEL or m->toShow() == Mesh::PARENT))
          and not (checks & MESH_NORMAL and not (m->toShow() == Mesh::NORMAL))
          and not (checks & MESH_SIGNAL and not (m->coloring() == Mesh::SIGNAL))
          and not (checks & MESH_TEXTURE and not (m->coloring() == Mesh::TEXTURE))
          and not (checks & MESH_IMAGE and not (m->coloring() == Mesh::IMAGE))
          and not (checks & MESH_SHOW_MESH and not m->isMeshVisible())
          and not (checks & MESH_SHOW_SURF and not m->isSurfaceVisible())
          and not (checks & MESH_ALL and not (m->meshView() == Mesh::ALL_MESH))
          and not (checks & MESH_BORDER and not (m->meshView() == Mesh::BORDER_MESH))
          and not (checks & MESH_CELLMAP and not (m->meshView() == Mesh::CELL_MESH))
          and not (checks & MESH_CELLS and not m->cells())
          and not (checks & MESH_IMG_TEX and not m->hasImgTex())
          and not (checks & MESH_SCALED and not m->scaled())
          and not (checks & MESH_TRANSFORMED and not m->transformed())
          and not (checks & MESH_EMPTY and not m->empty())
          and not (checks & MESH_NON_CELLS and m->cells())
          and not (checks & MESH_NON_IMG_TEX and m->hasImgTex())
          and not (checks & MESH_NON_SCALED and m->scaled())
          and not (checks & MESH_NON_TRANSFORMED and m->transformed()));
}

Process::CheckState Process::checkState() {
  return CheckState(this);
}

Process::CheckState::CheckState(const CheckState& copy)
  : reqs(copy.reqs)
  , process(copy.process)
{
}

Process::CheckState::CheckState(Process* proc)
  : process(proc)
{
}

Process::CheckState& Process::CheckState::store(int checks, int which)
{
  ProcessReqs r = { CHECK_STORE, checks, which };
  reqs << r;
  return *this;
}

Process::CheckState& Process::CheckState::stack(int checks, int which)
{
  ProcessReqs r = { CHECK_STACK, checks, which };
  reqs << r;
  return *this;
}

Process::CheckState& Process::CheckState::mesh(int checks, int which)
{
  ProcessReqs r = { CHECK_MESH, checks, which };
  reqs << r;
  return *this;
}

void Process::CheckState::setError()
{
  QStringList msgs;
  forall(const ProcessReqs& r, reqs) {
    switch(r.type) {
    case CHECK_STACK:
      msgs << process->stackError(r.checks, r.which);
      break;
    case CHECK_STORE:
      msgs << process->storeError(r.checks, r.which);
      break;
    case CHECK_MESH:
      msgs << process->meshError(r.checks, r.which);
    default:
      break;
    }
  }
  QString msg = QString("This process requires %1").arg(msgs.join(" ; "));
  process->setErrorMessage(msg);
}

Process::CheckState::operator bool()
{
  forall(const ProcessReqs& r, reqs) {
    switch(r.type) {
    case CHECK_STACK:
      if(!process->stackCheck(r.checks, r.which)) {
        setError();
        return false;
      }
      break;
    case CHECK_STORE:
      if(!process->storeCheck(r.checks, r.which)) {
        setError();
        return false;
      }
      break;
    case CHECK_MESH:
      if(!process->meshCheck(r.checks, r.which)) {
        setError();
        return false;
      }
    default:
      break;
    }
  }
  return true;
}

macroRegistrarsMap_t _macroRegistrars;

MacroRegistrarDefinition::MacroRegistrarDefinition(const QString& name, fct_t loader, fct_t unloader)
{
  if(name.isEmpty()) {
    Information::err << "Error, you need to specify a name for the macro registrar." << endl;
    return;
  }
  auto found = _macroRegistrars.find(name);
  if(found != _macroRegistrars.end()) {
    Information::err << "Error, there is already a macro registrar called '" << name << "'."
                        " The second one will not be used." << endl;
    return;
  }
  _name = name;
  _macroRegistrars[name] = { loader, unloader };
}

MacroRegistrarDefinition::~MacroRegistrarDefinition()
{
  if(valid()) {
    _macroRegistrars.erase(_name);
    _name = "";
  }
}

QStringList macroRegistrars()
{
  QStringList result;
  for(const auto& p: _macroRegistrars)
    result << p.first;
  return result;
}

std::function<bool(void)> macroLoader(const QString& name)
{
  auto found = _macroRegistrars.find(name);
  if(found == _macroRegistrars.end()) {
    return [name]() -> bool {
      Information::out << "Error, no such macro registrar: '" << name << "'" << endl;
      return false;
    };
  }
  return found->second.first;
}

std::function<bool(void)> macroUnloader(const QString& name)
{
  auto found = _macroRegistrars.find(name);
  if(found == _macroRegistrars.end()) {
    return [name]() -> bool {
      Information::out << "Error, no such macro registrar: '" << name << "'" << endl;
      return false;
    };
  }
  return found->second.second;
}

template <typename P>
Registration<P>::Registration(Registration&& other)
  : factory(other.factory)
  , classname(other.classname)
{
  other.factory = nullptr;
}

template <typename P>
Registration<P>::Registration(processFactory f, QString class_name, unsigned int compiledVersion)
  : factory()
  , classname(class_name)
{
  if(DEBUG) {
    QByteArray name_ba = classname.toLocal8Bit();
    Information::out << "Registration of process " << util::qdemangle(class_name.toStdString()) << " at address 0x"
                     << QString::number(f.id(), 16) << endl
                     << "    Type of data: " << util::qdemangle(typeid(*f).name()) << endl;
  }
  if(Process::processVersion != compiledVersion) {
    QTextStream err(stderr);
    err << "Error registering factory " << util::qdemangle(class_name.toStdString()) << endl
        << " It has been compiled against a different version of LithoGraphX:" << QString::number(compiledVersion)
        << endl << " and LithoGraphX is running processes version:" << QString::number(Process::processVersion)
        << endl;
  } else {
    factory = f;
    if(!factory and DEBUG)
      Information::out << "Storing null factory!" << endl;
    processFactories() << factory;
  }
}

template <typename P>
Registration<P>::~Registration()
{
  if(factory) {
    if(DEBUG) {
      Information::out << "Unregistration of process " << util::qdemangle(classname.toStdString()) << " at address 0x"
                       << QString::number(factory.id(), 16) << endl
                       << "    Type of data: " << util::qdemangle(typeid(*factory).name()) << endl;
    }
    QList<processFactory>& list = processFactories();
    int size_before = list.size();
    for(int i = 0; i < list.size(); ++i) {
      if(list[i] == factory) {
        list.removeAt(i);
        break;
      }
    }
    if(DEBUG and list.size() == size_before) {
      Information::out << "Error, could not find processFactory to remove" << endl;
    }
  }
}

template <typename P>
QList<typename Registration<P>::processFactory>& Registration<P>::processFactories()
{
  return factories;
}

#ifdef __MINGW32__
template LGX_EXPORT QList<Registration<StackProcess>::processFactory>& Registration<StackProcess>::processFactories();
template LGX_EXPORT QList<Registration<MeshProcess>::processFactory>& Registration<MeshProcess>::processFactories();
template LGX_EXPORT QList<Registration<GlobalProcess>::processFactory>& Registration<GlobalProcess>::processFactories();
#endif

template <typename P>
typename Registration<P>::factoryList Registration<P>::factories;

template struct LGX_EXPORT Registration<StackProcess>;
template struct LGX_EXPORT Registration<MeshProcess>;
template struct LGX_EXPORT Registration<GlobalProcess>;

} // namespace process
} // namespace lgx
