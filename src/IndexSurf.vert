varying vec3 texCoord;

void setTexCoord()
{
  texCoord = vec3(gl_MultiTexCoord0.x, gl_MultiTexCoord0.y, gl_MultiTexCoord0.z);
}
