/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PYTHON_LGX_MODULE_HPP
#define PYTHON_LGX_MODULE_HPP

#include <LGXConfig.hpp>

#include <python/Python.hpp>

#include <exception>

#include <QString>

class QWidget;

namespace lgx {
namespace process {

class Process;

namespace python {

struct LGXPYTHONSHARED_EXPORT SafePyObject
{
  SafePyObject(PyObject *obj = nullptr);
  SafePyObject(SafePyObject&& other);
  ~SafePyObject();

  SafePyObject& operator=(SafePyObject&& other);

  operator PyObject*() { return _ptr; }
  operator bool() const { return (bool)_ptr; }

  PyObject* get() { return _ptr; }
  PyObject* operator->() { return _ptr; }
  PyObject& operator*() { return *_ptr; }
  const PyObject* get() const { return _ptr; }
  const PyObject* operator->() const { return _ptr; }
  const PyObject& operator*() const { return *_ptr; }

  // Increate reference count
  void acquire();
  void free();
  PyObject* release();

private:
  PyObject* _ptr;
};

template <typename T>
struct LGXPYTHONSHARED_EXPORT SafePy : public SafePyObject
{
  SafePy(T* p=nullptr)
    : SafePyObject((PyObject*)p)
  { }

  explicit SafePy(SafePyObject&& other)
    : SafePyObject(std::move(other))
  { }

  SafePy(SafePy&& other)
    : SafePyObject(std::move(other))
  { }

  SafePy& operator=(SafePy&& other)
  {
    SafePyObject::operator=(std::move(other));
    return *this;
  }

  T* release() {
    return reinterpret_cast<T*>(SafePyObject::release());
  }

  const T* get() const { return reinterpret_cast<T*>(SafePyObject::get()); }
  const T* operator->() const { return get(); }
  const T& operator*() const { return *get(); }

  T* get() { return reinterpret_cast<T*>(SafePyObject::get()); }
  T* operator->() { return get(); }
  T& operator*() { return *get(); }

  operator T*() { return get(); }
  explicit operator PyObject*() { return SafePyObject::get(); }
};

LGXPYTHONSHARED_EXPORT QString getString(PyObject *obj);
LGXPYTHONSHARED_EXPORT SafePyObject makeString(const QString& str);

LGXPYTHONSHARED_EXPORT QString getError();
LGXPYTHONSHARED_EXPORT QString getTraceBack(PyObject* tb, int limit);

class LGXPYTHONSHARED_EXPORT ProcessRunner
{
public:
  ProcessRunner();
  ProcessRunner(const QString& type, const QString& name);
  ~ProcessRunner();

  bool operator()(const QStringList& arguments);

  static void registerBaseProcess(Process *proc);
  static void popBaseProcess();

  QString errorMessage();

protected:
  QString _type;
  QString _name;
  QString _message;
};

class LGXPYTHONSHARED_EXPORT ProcessMaker
{
public:
  ProcessMaker();
  ProcessMaker(const QString& type);
  ~ProcessMaker();

  QStringList processes();

  ProcessRunner __getattr__(const QString& name);

protected:
  QString _type;
};

LGXPYTHONSHARED_EXPORT PyObject* mapQWidgetToSelf(QWidget* w);

} // namespace python

LGX_EXPORT bool stringToBool(const QString& str);

} // namespace process
} // namespace lgx

#endif // PYTHON_LGX_MODULE_HPP
