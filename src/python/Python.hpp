/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PYTHON_PYTHON_HPP
#define PYTHON_PYTHON_HPP

#include <LGXConfig.hpp>

#include <Python.h>

#if PY_MAJOR_VERSION >= 3
#  define IS_PY3K
#endif

#ifdef WIN32
#    ifdef lgxPython_EXPORTS
#        define LGXPYTHON_EXPORT __declspec(dllexport)
#    else
#        define LGXPYTHON_EXPORT __declspec(dllimport)
#    endif
#    ifdef lgxPy_shared_EXPORTS
#        define LGXPYTHONSHARED_EXPORT __declspec(dllexport)
#    else
#        define LGXPYTHONSHARED_EXPORT __declspec(dllimport)
#    endif
#else
#  define LGXPYTHON_EXPORT
#  define LGXPYTHONSHARED_EXPORT
#endif

#endif // PYTHON_PYTHON_HPP

