/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Python.hpp"

#include "lgx_module.hpp"

#include "Process.hpp"
#include "Defer.hpp"

#include <structmember.h>
#include <frameobject.h>
#include <sip.h>

#include <stack>
#include <cassert>
#include <memory>

namespace lgx {
namespace process {
namespace python {

namespace {
const sipAPIDef *get_sip_api()
{
#if defined(SIP_USE_PYCAPSULE)
    return (const sipAPIDef *)PyCapsule_Import("sip._C_API", 0);
#else
    PyObject *sip_module;
    PyObject *sip_module_dict;
    PyObject *c_api;

    /* Import the SIP module. */
    sip_module = PyImport_ImportModule("sip");

    if (sip_module == NULL)
        return NULL;

    /* Get the module's dictionary. */
    sip_module_dict = PyModule_GetDict(sip_module);

    /* Get the "_C_API" attribute. */
    c_api = PyDict_GetItemString(sip_module_dict, "_C_API");

    if (c_api == NULL)
        return NULL;

    /* Sanity check that it is the right type. */
    if (!PyCObject_Check(c_api))
        return NULL;

    /* Get the actual pointer from the object. */
    return (const sipAPIDef *)PyCObject_AsVoidPtr(c_api);
#endif
}
} // namespace

SafePyObject::SafePyObject(PyObject *obj)
  : _ptr(obj)
{ }

SafePyObject::SafePyObject(SafePyObject&& other)
  : _ptr(other._ptr)
{
  other._ptr = nullptr;
}

SafePyObject::~SafePyObject()
{
  Py_XDECREF(_ptr);
  _ptr = nullptr;
}

SafePyObject& SafePyObject::operator=(SafePyObject&& other)
{
  Py_XDECREF(_ptr);
  _ptr = other._ptr;
  other._ptr = nullptr;
  return *this;
}

void SafePyObject::acquire() {
  if(_ptr)
    Py_INCREF(_ptr);
}

void SafePyObject::free() {
  Py_XDECREF(_ptr);
  _ptr = nullptr;
}

PyObject* SafePyObject::release() {
  PyObject* value = _ptr;
  _ptr = nullptr;
  return value;
}

QString getString(PyObject* obj)
{
#ifdef IS_PY3K
  if(PyUnicode_Check(obj))
    return QString::fromUtf8(PyUnicode_AsUTF8(obj));
  SafePyObject pystr = PyObject_Str(obj);
  return QString::fromUtf8(PyUnicode_AsUTF8(pystr));
#else
  if(PyString_Check(obj))
    return QString::fromLocal8Bit(PyString_AsString(obj));
  else if(PyUnicode_Check(obj)) {
    SafePyObject pystr = PyUnicode_AsUTF8String(obj);
    if(not pystr) {
      Information::err << "Error, couldn't convert unicode string to UTF-8 string" << endl;
      return QString();
    }
    return QString::fromUtf8(PyString_AsString(pystr));
  } else {
    SafePyObject pystr = PyObject_Str(obj);
    return QString::fromUtf8(PyString_AsString(pystr));
  }
#endif
}

SafePyObject makeString(const QString& s)
{
#ifdef IS_PY3K
  QByteArray ba = s.toUtf8();
  const char err[] = "ignore";
  return PyUnicode_DecodeUTF8(ba.data(), ba.size(), err);
#else
  QByteArray ba = s.toLocal8Bit();
  return PyString_FromString(ba.data());
#endif
}

QString getTraceBack(PyObject* tb, int limit)
{
  QString err;
  PyTracebackObject* tb1 = (PyTracebackObject*)tb;
  int depth = 0;
  while(tb1) {
    depth++;
    tb1 = tb1->tb_next;
  }
  tb1 = (PyTracebackObject*)tb;
  while(tb1 != NULL) {
    if(depth <= limit) {
#ifdef IS_PY3K
      QString fn = QString::fromUtf8(PyUnicode_AsUTF8(tb1->tb_frame->f_code->co_filename));
      QString co = QString::fromUtf8(PyUnicode_AsUTF8(tb1->tb_frame->f_code->co_name));
#else
      const char* fn = PyString_AsString(tb1->tb_frame->f_code->co_filename);
      const char* co = PyString_AsString(tb1->tb_frame->f_code->co_name);
#endif
      int lo = tb1->tb_lineno;
      err += QString("  File \"%1\", line %2, in %3\n").arg(fn).arg(lo).arg(co);
    }
    depth--;
    tb1 = tb1->tb_next;
  }
  return err;
}

QString getError()
{
  PyObject* exc, *val, *tb;
  PyErr_Fetch(&exc, &val, &tb);
  if(!exc)
    return "Unknown error in Python code";
  else {
    // Now, get the exception string
    QString err;
    PyErr_NormalizeException(&exc, &val, &tb);
    if(tb and tb != Py_None) {
      err += "Traceback (most recent call last):\n";
      err += getTraceBack(tb, 10);
    }
    SafePyObject str = PyObject_Str(val);
    if(str) {
      SafePyObject exc_str = PyObject_GetAttrString(exc, "__name__");
      QString s_exc = python::getString(exc_str);
      QString s_val = python::getString(str);
      QString msg = err + QString("%1: %2").arg(s_exc).arg(s_val);
      return msg;
    } else
      return "Unknown exception in Python code";
  }
}

std::stack<Process*> active_process;

ProcessRunner::ProcessRunner()
{ }

ProcessRunner::ProcessRunner(const QString& type, const QString& name)
  : _type(type)
  , _name(name)
{ }

ProcessRunner::~ProcessRunner()
{ }

bool ProcessRunner::operator()(const QStringList& args)
{
  if(active_process.empty())
    throw ProcessError("No active process.");
  Process *base_proc = active_process.top();
  auto new_proc = std::unique_ptr<Process>(base_proc->makeProcess(_type, _name));
  try {
    if(not (*new_proc)(args)) {
      throw ProcessError(new_proc->errorMessage());
    }
  } catch(std::string s) {
    throw ProcessError(s.data());
  } catch(QString s) {
    throw ProcessError(s.toUtf8().data());
  } catch(std::exception&) {
    throw;
  } catch(...) {
    throw ProcessError("Unknown C++ exception.");
  }
  return true;
}

QString ProcessRunner::errorMessage()
{
  if(active_process.empty())
    return "No active process";
  return active_process.top()->errorMessage();
}

void ProcessRunner::registerBaseProcess(Process *proc)
{
  active_process.push(proc);
}

void ProcessRunner::popBaseProcess()
{
  assert(not active_process.empty());
  active_process.pop();
}

ProcessMaker::ProcessMaker()
{ }

ProcessMaker::ProcessMaker(const QString& type)
  : _type(type)
{ }

ProcessMaker::~ProcessMaker() { }

QStringList ProcessMaker::processes()
{
  auto result = listProcesses(_type);
  result.replaceInStrings(" ", "_").sort(Qt::CaseInsensitive);
  return result;
}

ProcessRunner ProcessMaker::__getattr__(const QString& name)
{
  QString real_name = name;
  real_name.replace("_", " ");
  auto* proc = getBaseProcessDefinition(_type, real_name);
  if(not proc)
    throw NoSuchProcess(_type, real_name);
  return ProcessRunner(_type, real_name);
}

PyObject* mapQWidgetToSelf(QWidget* w)
{
  auto api = get_sip_api();
  const sipTypeDef* qwidget_typedef = api->api_find_type("QWidget");
  return api->api_convert_from_type(w, qwidget_typedef, nullptr);
}

} // namespace python
} // namespace process
} // namespace lgx
