/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Python.hpp"

#include "PythonScript.hpp"

#include <Defer.hpp>
#include "Dir.hpp"
#include <Forall.hpp>
#include <Information.hpp>
#include "lgx_module.hpp"

#include <QFileDialog>
#include <QTextStream>
#include <stddef.h>
#include <stdio.h>

namespace lgx {
namespace process {

bool PythonScript::initialize(QStringList& parms, QWidget* parent)
{
  QString filename = parms[0];
  bool choose_file = stringToBool(parms[1]);

  if(choose_file or filename.isEmpty()) {
    filename = QFileDialog::getOpenFileName(parent, "Choose python script to execute", filename,
                                            "Python scripts (*.py);;All files (*.*)");
    // check file
    if(filename.isEmpty())
      return false;
  }
  // ok all good
  parms[0] = filename;
  parms[1] = "Yes"; // Always reset to Yes!

  return true;
}

bool PythonScript::operator()(QString filename)
{
  bool success = false;
  PythonInterpreter interpreter;
  filename = util::resolvePath(filename);
  FILE* file = fopen(filename.toLocal8Bit().data(), "rt");
  if(not file) {
    setErrorMessage(QString("Cannot open file '%1' for reading").arg(filename));
    return false;
  }
  python::ProcessRunner::registerBaseProcess(this);
  auto defered = util::defer([file]() -> void {
                               PyErr_Clear();
                               fclose(file);
                               python::ProcessRunner::popBaseProcess();
                             });
  do {
    // First, add the __main__ module and set its file name to the file loaded
    QString initScript = "__file__ = '%1'\n"
                         "from lgxPy import Stack, Mesh, Global";

    initScript = initScript.arg(filename);

    if(PyRun_SimpleString(initScript.toUtf8().data()) != 0)
      return setErrorMessage("Couldn't initialize python's script.");

    auto res = PyRun_SimpleFile(file, filename.toLocal8Bit().data());
    if(not res) {
      return setErrorMessage(python::getError());
    } else {
      Py_DECREF(res);
      success = true;
    }
  } while(false);
  return success;
}

REGISTER_GLOBAL_PROCESS(PythonScript);
} // namespace process
} // namespace lgx
