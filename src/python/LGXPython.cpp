/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Python.hpp"

#include <LGXPython.hpp>

#include "Misc.hpp"
#include "lgx_module.hpp"

#include <QString>

namespace lgx {
namespace process {

template <typename ProcessType>
PythonProcess<ProcessType>::PythonProcess(const process_t& process)
  : Process(process)
  , ProcessType(process)
{ }

template class LGXPYTHON_EXPORT PythonProcess<StackProcess>;
template class LGXPYTHON_EXPORT PythonProcess<MeshProcess>;
template class LGXPYTHON_EXPORT PythonProcess<GlobalProcess>;

PythonInterpreter::PythonInterpreter(bool global_interpreter)
  : is_global(global_interpreter)
{
  bool need_initializing = not (global_interpreter or Py_IsInitialized());
  if(need_initializing) {
#ifdef IS_PY3K
    wchar_t pn[] = L"LithoGraphX";
#else
    char pn[] = "LithoGraphX";
#endif
    Py_SetProgramName(pn);
    Information::out << "Initialize Python interpreter" << endl;

    Py_Initialize();

    PyObject* sys_path = PySys_GetObject("path");

    for(const QDir& folder: util::macroDirs()) {
      QString path = folder.canonicalPath();
      python::SafePyObject pypath = python::makeString(path);
      PyList_Insert(sys_path, 0, pypath);
    }

    // To start with, import lgxPy and PyQt5 modules (will save time later and we make sure everything is alright)
    python::SafePyObject lgxPy = PyImport_ImportModule("lgxPy");
    if(PyErr_Occurred())
      Information::err << "Error loading lgxPy: " << python::getError() << endl;
    python::SafePyObject core = PyImport_ImportModule("PyQt5.QtCore");
    if(PyErr_Occurred())
      Information::err << "Error loading PyQt5.QtCore: " << python::getError() << endl;
    python::SafePyObject gui = PyImport_ImportModule("PyQt5.QtGui");
    if(PyErr_Occurred())
      Information::err << "Error loading PyQt5.QtGui: " << python::getError() << endl;
    python::SafePyObject widgets = PyImport_ImportModule("PyQt5.QtWidgets");
    if(PyErr_Occurred())
      Information::err << "Error loading PyQt5.QtWidgets: " << python::getError() << endl;
  }
}

PythonInterpreter::~PythonInterpreter()
{
  if(is_global)
    Py_Finalize();
}

namespace {
PythonInterpreter interpreter(true); // Global interpreter
}

} // namespace process
} // namespace lgx
