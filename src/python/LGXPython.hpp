/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef LGXPYTHON_HPP
#define LGXPYTHON_HPP

#include <Process.hpp>

#include <vector>

#ifndef PyObject_HEAD
struct _object;
typedef _object PyObject;
#endif

namespace lgx {
namespace process {
/**
 * \class PythonProcess python/LGXPython.h <python/LGXPython.h>
 * Define a python process for a process of type P
 *
 * P must be one of StackProcess, MeshProcess or GlobalProcess
 */
template <typename ProcessType>
class LGXPYTHON_EXPORT PythonProcess : public ProcessType
{
public:
  typedef ProcessType process_t;

  PythonProcess(const process_t& process);
};

/// Start and create a python interpreter, if it is not already started
struct LGXPYTHON_EXPORT PythonInterpreter
{
  PythonInterpreter(bool global_interpreter = false);
  ~PythonInterpreter();

private:
  bool is_global;
};

#ifndef WIN32
extern template class PythonProcess<StackProcess>;
extern template class PythonProcess<MeshProcess>;
extern template class PythonProcess<GlobalProcess>;
#endif

} // namespace process
} // namespace lgx

#endif // LGXPYTHON_HPP

