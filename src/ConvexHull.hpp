/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef QHULL_CONVEXHULL_HPP
#define QHULL_CONVEXHULL_HPP

#include <qhull/LGXQHull.hpp>

#include <Geometry.hpp>

#include <vector>

namespace lgx {
namespace qhull {

std::vector<Point3i> convexHull(std::vector<Point3f> positions, QString* errorString = nullptr);

} // namespace qhull
} // namespace lgx

#endif // QHULL_CONVEXHULL_HPP

