/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef DIR_HPP
#define DIR_HPP

#include <LGXConfig.hpp>

#include <QDir>

namespace lgx {
namespace util {

/// Returns the path relative to the current directory
LGX_EXPORT QString stripCurrentDir(QString file);

/// Remove a directory from the file path
LGX_EXPORT QString stripDir(const QString& dir, QString file);

/// Get the directory containing a file (base of the name only)
LGX_EXPORT QString getDir(QString file);

/// Set the current path
LGX_EXPORT bool setCurrentPath(const QString& path);

/// Get the current path
LGX_EXPORT QString currentPath();

/// Get the current path as a directory
LGX_EXPORT QDir currentDir();

/// Return the absolute path to a file
LGX_EXPORT QString absoluteFilePath(QString filename);

/// Return the absolute file path, resolving things like environement variables and '~' for $HOME
LGX_EXPORT QString resolvePath(QString path);

/// Create a directory path
LGX_EXPORT bool createPath(QString path);

} // namespace util
} // namespace lgx

#endif
