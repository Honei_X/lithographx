/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Shader.hpp"

#include "Forall.hpp"
#include "Information.hpp"

#include <algorithm>
#include <iostream>
#include <QFile>
#include <QSet>
#include <QString>
#include <QTextStream>

namespace lgx {
using Information::err;

namespace {
void glUniform1fv_(GLint id, GLsizei s, const GLfloat* val)
{
  opengl->glUniform1fv(id, s, val);
}
void glUniform2fv_(GLint id, GLsizei s, const GLfloat* val)
{
  opengl->glUniform2fv(id, s, val);
}
void glUniform3fv_(GLint id, GLsizei s, const GLfloat* val)
{
  opengl->glUniform3fv(id, s, val);
}
void glUniform4fv_(GLint id, GLsizei s, const GLfloat* val)
{
  opengl->glUniform4fv(id, s, val);
}
void glUniform1iv_(GLint id, GLsizei s, const GLint* val)
{
  opengl->glUniform1iv(id, s, val);
}
void glUniform2iv_(GLint id, GLsizei s, const GLint* val)
{
  opengl->glUniform2iv(id, s, val);
}
void glUniform3iv_(GLint id, GLsizei s, const GLint* val)
{
  opengl->glUniform3iv(id, s, val);
}
void glUniformTransposedMatrix2fv_(GLint location, GLsizei count, const GLfloat* v)
{
  opengl->glUniformMatrix2fv(location, count, 1, v);
}
void glUniformTransposedMatrix3fv_(GLint location, GLsizei count, const GLfloat* v)
{
  opengl->glUniformMatrix3fv(location, count, 1, v);
}
void glUniformTransposedMatrix4fv_(GLint location, GLsizei count, const GLfloat* v)
{
  opengl->glUniformMatrix4fv(location, count, 1, v);
}
void glUniform4iv_(GLint id, GLsizei s, const GLint* val)
{
  opengl->glUniform4iv(id, s, val);
}
void glVertexAttrib1fv_(GLuint id, const GLfloat* val)
{
  opengl->glVertexAttrib1fv(id, val);
}
void glVertexAttrib2fv_(GLuint id, const GLfloat* val)
{
  opengl->glVertexAttrib2fv(id, val);
}
void glVertexAttrib3fv_(GLuint id, const GLfloat* val)
{
  opengl->glVertexAttrib3fv(id, val);
}
void glVertexAttrib4fv_(GLuint id, const GLfloat* val)
{
  opengl->glVertexAttrib4fv(id, val);
}
} // namespace


Shader::Shader(int _verbosity)
  : verbosity(_verbosity)
  , _program(0)
  , _initialized(false)
{
}

bool Shader::init()
{
  has_shaders = true;
  return true;
}

void Shader::addVertexShader(const QString& filename)
{
  std::vector<code_t>::iterator it_found
    = std::find(vertex_shaders_code.begin(), vertex_shaders_code.end(), std::make_pair(filename, false));
  if(it_found == vertex_shaders_code.end()) {
    vertex_shaders_code.push_back(std::make_pair(filename, false));
    invalidate();
  }
}

void Shader::removeVertexShader(const QString& filename)
{
  std::vector<code_t>::iterator it_found
    = std::find(vertex_shaders_code.begin(), vertex_shaders_code.end(), std::make_pair(filename, false));
  if(it_found != vertex_shaders_code.end()) {
    vertex_shaders_code.erase(it_found);
    invalidate();
  }
}

void Shader::addFragmentShader(const QString& filename)
{
  std::vector<code_t>::iterator it_found
    = std::find(fragment_shaders_code.begin(), fragment_shaders_code.end(), std::make_pair(filename, false));
  if(it_found == fragment_shaders_code.end()) {
    fragment_shaders_code.push_back(std::make_pair(filename, false));
    invalidate();
  }
}

bool Shader::changeFragmentShader(int pos, const QString& filename)
{
  if(pos < 0) pos += (int)fragment_shaders_code.size();
  if(pos < 0) return false;
  if(fragment_shaders_code.size() > (size_t)pos) {
    code_t c = std::make_pair(filename, false);
    if(c != fragment_shaders_code[pos]) {
      invalidate();
      fragment_shaders_code[pos] = std::make_pair(filename, false);
    }
    return true;
  }
  return false;
}

bool Shader::changeVertexShader(int pos, const QString& filename)
{
  if(pos < 0) pos += (int)vertex_shaders_code.size();
  if(pos < 0) return false;
  if(vertex_shaders_code.size() > (size_t)pos) {
    code_t c = std::make_pair(filename, false);
    if(c != vertex_shaders_code[pos]) {
      invalidate();
      vertex_shaders_code[pos] = std::make_pair(filename, false);
    }
    return true;
  }
  return false;
}

bool Shader::changeFragmentShaderCode(int pos, const QString& filename)
{
  if(pos < 0) pos += fragment_shaders_code.size();
  if(pos < 0) return false;
  if(fragment_shaders_code.size() > (size_t)pos) {
    code_t c = std::make_pair(filename, true);
    if(c != fragment_shaders_code[pos]) {
      invalidate();
      fragment_shaders_code[pos] = std::make_pair(filename, true);
    }
    return true;
  }
  return false;
}

bool Shader::changeVertexShaderCode(int pos, const QString& filename)
{
  if(pos < 0) pos += vertex_shaders_code.size();
  if(pos < 0) return false;
  if(vertex_shaders_code.size() > (size_t)pos) {
    code_t c = std::make_pair(filename, true);
    if(c != vertex_shaders_code[pos]) {
      invalidate();
      vertex_shaders_code[pos] = std::make_pair(filename, true);
    }
    return true;
  }
  return false;
}

void Shader::removeFragmentShader(const QString& filename)
{
  std::vector<code_t>::iterator it_found
    = std::find(fragment_shaders_code.begin(), fragment_shaders_code.end(), std::make_pair(filename, false));
  if(it_found != fragment_shaders_code.end()) {
    fragment_shaders_code.erase(it_found);
    invalidate();
  }
}

void Shader::addVertexShaderCode(const QString& code)
{
  std::vector<code_t>::iterator it_found
    = std::find(vertex_shaders_code.begin(), vertex_shaders_code.end(), std::make_pair(code, true));
  if(it_found == vertex_shaders_code.end()) {
    vertex_shaders_code.push_back(std::make_pair(code, true));
    invalidate();
  }
}

void Shader::removeVertexShaderCode(const QString& code)
{
  std::vector<code_t>::iterator it_found
    = std::find(vertex_shaders_code.begin(), vertex_shaders_code.end(), std::make_pair(code, true));
  if(it_found != vertex_shaders_code.end()) {
    vertex_shaders_code.erase(it_found);
    invalidate();
  }
}

void Shader::addFragmentShaderCode(const QString& code)
{
  std::vector<code_t>::iterator it_found
    = std::find(fragment_shaders_code.begin(), fragment_shaders_code.end(), std::make_pair(code, true));
  if(it_found == fragment_shaders_code.end()) {
    fragment_shaders_code.push_back(std::make_pair(code, true));
    invalidate();
  }
}

void Shader::removeFragmentShaderCode(const QString& code)
{
  std::vector<code_t>::iterator it_found
    = std::find(fragment_shaders_code.begin(), fragment_shaders_code.end(), std::make_pair(code, true));
  if(it_found != fragment_shaders_code.end()) {
    fragment_shaders_code.erase(it_found);
    invalidate();
  }
}

void Shader::cleanShaders()
{
  if(!hasShaders())
    return;
  for(const GLuint& id: vertex_shaders) {
    if(_program)
      opengl->glDetachShader(_program, id);
    opengl->glDeleteShader(id);
  }
  for(const GLuint& id: fragment_shaders) {
    if(_program)
      opengl->glDetachShader(_program, id);
    opengl->glDeleteShader(id);
  }
  vertex_shaders.clear();
  fragment_shaders.clear();
  if(_program)
    opengl->glDeleteProgram(_program);
  _program = 0;
}

bool Shader::setupShaders()
{
  if(!hasShaders())
    return false;
  cleanShaders();
  // First, compile the shaders
  // For that, concatenate all the files, and add the version 120 to the beginning
  {
    QString content("#version 120\n\n");
    forall(const code_t& code, vertex_shaders_code) {
      if(code.second) {
        content += code.first;
        content += "\n";
      } else {
        const QString& filename = code.first;
        QFile f(filename);
        if(f.open(QIODevice::ReadOnly)) {
          QTextStream ts(&f);
          content += ts.readAll();
          content += "\n";
        } else {
          err << "Error, cannot open shader file " << filename << endl;
          cleanShaders();
          return false;
        }
      }
    }
    GLuint s = compileShader(GL_VERTEX_SHADER, content);
    if(verbosity > 3)
      err << "Compiling vertex shader" << endl;
    if(s == 0) {
      cleanShaders();
      return false;
    }
    vertex_shaders.push_back(s);
  }

  {
    QString content("#version 120\n\n");
    forall(const code_t& code, fragment_shaders_code) {
      if(code.second) {
        content += code.first;
        content += "\n";
      } else {
        const QString& filename = code.first;
        QFile f(filename);
        if(f.open(QIODevice::ReadOnly)) {
          QTextStream ts(&f);
          content += ts.readAll();
          content += "\n";
        } else {
          err << "Error, cannot open shader file " << filename << endl;
          cleanShaders();
          return false;
        }
      }
    }
    GLuint s = compileShader(GL_FRAGMENT_SHADER, content);
    if(verbosity > 3)
      err << "Compiling fragment shader" << endl;
    if(s == 0) {
      cleanShaders();
      return false;
    }
    fragment_shaders.push_back(s);
  }

  if(fragment_shaders.empty() and vertex_shaders.empty()) {
    err << "Warning, no shader to compile" << endl;
    return false;
  }
  // Create the program
  _program = opengl->glCreateProgram();
  REPORT_GL_ERROR("_program = opengl->glCreateProgramObject()");
  // Add shader
  forall(const GLuint& id, vertex_shaders) {
    opengl->glAttachShader(_program, id);
    REPORT_GL_ERROR("opengl->glAttachObject(_program, id)");
  }
  forall(const GLuint& id, fragment_shaders) {
    opengl->glAttachShader(_program, id);
    REPORT_GL_ERROR("opengl->glAttachObject(_program, id)");
  }
  // Link shader
  opengl->glLinkProgram(_program);
  REPORT_GL_ERROR("opengl->glLinkProgram(_program)");

  int status;
  opengl->glGetProgramiv(_program, GL_LINK_STATUS, &status);
  if(!status) {
    err << "Error while linking program:" << endl;
    printProgramInfoLog(_program);
    return false;
  }
  _initialized = true;
  return true;
}

bool Shader::reportGLError(const QString& id, const char* file, int line)
{
  GLenum error = opengl->glGetError();
  if(error) {
    err << "OpenGL error in file " << file << " on line " << line << " for command :\n - " << id << ":" << endl
        << (char*)gluErrorString(error) << endl;
    return true;
  }
  return false;
}

bool Shader::reportGLError(const char* id, const char* file, int line)
{
  return reportGLError(QString::fromLatin1(id), file, line);
}

QString Shader::shaderTypeName(GLenum shader_type)
{
  switch(shader_type) {
  case GL_VERTEX_SHADER:
    return "vertex";
  case GL_FRAGMENT_SHADER:
    return "fragment";
  default:
    return "unknown";
  }
}

GLuint Shader::compileShaderFile(GLenum shader_type, QString filename)
{
  if(!hasShaders())
    return 0;
  QFile f(filename);
  if(!f.open(QIODevice::ReadOnly)) {
    err << "Cannot open file " << f.fileName() << endl;
    return 0;
  }
  QTextStream ts(&f);
  QString content = ts.readAll();
  return compileShader(shader_type, content);
}

void Shader::printProgramInfoLog(GLuint object)
{
  if(!hasShaders())
    return;

  GLsizei log_length;
  opengl->glGetProgramiv(object, GL_INFO_LOG_LENGTH, &log_length);
  if(log_length > 0) {
    std::vector<char> cs(log_length + 1);
    GLsizei l;
    opengl->glGetProgramInfoLog(object, log_length, &l, cs.data());
    err << "*** START LOGS ***\n";
    err << cs.data() << "\n*** END LOGS ***" << endl;
  } else
    err << "*** EMPTY LOG ***" << endl;
}

void Shader::printShaderInfoLog(GLuint object)
{
  if(!hasShaders())
    return;

  GLsizei log_length;
  opengl->glGetShaderiv(object, GL_INFO_LOG_LENGTH, &log_length);
  if(log_length > 0) {
    std::vector<char> cs(log_length + 1);
    GLsizei l;
    opengl->glGetShaderInfoLog(object, log_length, &l, cs.data());
    err << "*** START LOGS ***\n";
    err << cs.data() << "\n*** END LOGS ***" << endl;
  } else
    err << "*** EMPTY LOG ***" << endl;
}

GLuint Shader::compileShader(GLenum shader_type, QString content)
{
  if(!hasShaders())
    return 0;
  int shader = opengl->glCreateShader(shader_type);
  QByteArray ba = content.toLatin1();
  const char* src = ba;
  // int size = content.size();
  opengl->glShaderSource(shader, 1, (const GLchar**)(&src), NULL);
  opengl->glCompileShader(shader);
  int status;
  opengl->glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  if(!status) {
    err << "Error while compiling this " << shaderTypeName(shader_type) << " shader: " << endl
        << "***** Beginning of shader *****" << endl << src << endl << "***** End of shader *****" << endl;
    printShaderInfoLog(shader);
    return 0;
  }
  return shader;
}

bool Shader::useShaders()
{
  if(hasShaders() and _program) {
    // err << "opengl->glUseProgram(" << _program << ")" << endl;
    opengl->glUseProgram(_program);
    return true;
  }
  return false;
}

bool Shader::stopUsingShaders()
{
  // err << "opengl->glUseProgram(0) (0x" << QString::number((uintptr_t)opengl->glUseProgram, 16) << endl;
  opengl->glUseProgram(0);
  return true;
}

QTextStream& GLSLValue::write(QTextStream& s) const
{
  if(!value) {
    s << "invalid value";
    return s;
  }
  switch(type) {
  case UNIFORM_INT:
    s << "int ";
    break;
  case UNIFORM_INT2:
    s << "ivec2 ";
    break;
  case UNIFORM_INT3:
    s << "ivec3 ";
    break;
  case UNIFORM_INT4:
    s << "ivec4 ";
    break;
  case UNIFORM_FLOAT:
    s << "float ";
    break;
  case UNIFORM_FLOAT2:
    s << "vec2 ";
    break;
  case UNIFORM_FLOAT3:
    s << "vec3 ";
    break;
  case UNIFORM_FLOAT4:
    s << "vec4 ";
    break;
  case UNIFORM_MATRIX2:
    s << "mat2 ";
    break;
  case UNIFORM_MATRIX3:
    s << "mat3 ";
    break;
  case UNIFORM_MATRIX4:
    s << "mat4 ";
    break;
  default:
    s << "Unkown type: " << type;
  }
  return value->write(s);
}

std::ostream& GLSLValue::write(std::ostream& s) const
{
  if(!value) {
    s << "invalid value";
    return s;
  }
  switch(type) {
  case UNIFORM_INT:
    s << "int ";
    break;
  case UNIFORM_INT2:
    s << "ivec2 ";
    break;
  case UNIFORM_INT3:
    s << "ivec3 ";
    break;
  case UNIFORM_INT4:
    s << "ivec4 ";
    break;
  case UNIFORM_FLOAT:
    s << "float ";
    break;
  case UNIFORM_FLOAT2:
    s << "vec2 ";
    break;
  case UNIFORM_FLOAT3:
    s << "vec3 ";
    break;
  case UNIFORM_FLOAT4:
    s << "vec4 ";
    break;
  case UNIFORM_MATRIX2:
    s << "mat2 ";
    break;
  case UNIFORM_MATRIX3:
    s << "mat3 ";
    break;
  case UNIFORM_MATRIX4:
    s << "mat4 ";
    break;
  default:
    s << "Unkown type: " << type;
  }
  return value->write(s);
}

QTextStream& GLSLValue::read(QTextStream& s)
{
  QString str;
  s >> str;
  delete value;
  if(str == "int") {
    type = UNIFORM_INT;
    setValue(ivec1());
  } else if(str == "ivec2") {
    type = UNIFORM_INT2;
    setValue(ivec2());
  } else if(str == "ivec3") {
    type = UNIFORM_INT3;
    setValue(ivec3());
  } else if(str == "ivec4") {
    type = UNIFORM_INT4;
    setValue(ivec4());
  } else if(str == "float") {
    type = UNIFORM_FLOAT;
    setValue(vec1());
  } else if(str == "vec2") {
    type = UNIFORM_FLOAT2;
    setValue(vec2());
  } else if(str == "vec3") {
    type = UNIFORM_FLOAT3;
    setValue(vec3());
  } else if(str == "vec4") {
    type = UNIFORM_FLOAT4;
    setValue(vec4());
  } else if(str == "mat2") {
    type = UNIFORM_MATRIX2;
    setValue(mat2());
  } else if(str == "mat3") {
    type = UNIFORM_MATRIX3;
    setValue(mat3());
  } else if(str == "mat4") {
    type = UNIFORM_MATRIX4;
    setValue(mat4());
  } else {
    value = 0;
    err << "Error, uniform type '" << str << "' not recognised" << endl;
    return s;
  }
  return value->read(s);
}

std::istream& GLSLValue::read(std::istream& s)
{
  std::string str;
  s >> str;
  delete value;
  if(str == "int") {
    type = UNIFORM_INT;
    setValue(ivec1());
  } else if(str == "ivec2") {
    type = UNIFORM_INT2;
    setValue(ivec2());
  } else if(str == "ivec3") {
    type = UNIFORM_INT3;
    setValue(ivec3());
  } else if(str == "ivec4") {
    type = UNIFORM_INT4;
    setValue(ivec4());
  } else if(str == "float") {
    type = UNIFORM_FLOAT;
    setValue(vec1());
  } else if(str == "vec2") {
    type = UNIFORM_FLOAT2;
    setValue(vec2());
  } else if(str == "vec3") {
    type = UNIFORM_FLOAT3;
    setValue(vec3());
  } else if(str == "vec4") {
    type = UNIFORM_FLOAT4;
    setValue(vec4());
  } else if(str == "mat2") {
    type = UNIFORM_MATRIX2;
    setValue(mat2());
  } else if(str == "mat3") {
    type = UNIFORM_MATRIX3;
    setValue(mat3());
  } else if(str == "mat4") {
    type = UNIFORM_MATRIX4;
    setValue(mat4());
  } else {
    value = 0;
    err << "Error, uniform type '" << QString::fromStdString(str) << "' not recognised" << endl;
    return s;
  }
  return value->read(s);
}

void GLSLValue::setValue(const GLint* val, int count)
{
  type = UNIFORM_INT;
  value = new ValueImpl<ivec1>(glUniform1iv_, 0, (const ivec1*)val, count);
}

void GLSLValue::setValue(const ivec1* val, int count)
{
  type = UNIFORM_INT;
  value = new ValueImpl<ivec1>(glUniform1iv_, 0, val, count);
}

void GLSLValue::setValue(const ivec2* val, int count)
{
  type = UNIFORM_INT2;
  value = new ValueImpl<ivec2>(glUniform2iv_, 0, val, count);
}

void GLSLValue::setValue(const ivec3* val, int count)
{
  type = UNIFORM_INT3;
  value = new ValueImpl<ivec3>(glUniform3iv_, 0, val, count);
}

void GLSLValue::setValue(const ivec4* val, int count)
{
  type = UNIFORM_INT4;
  value = new ValueImpl<ivec4>(glUniform4iv_, 0, val, count);
}

void GLSLValue::setValue(const GLfloat* val, int count)
{
  type = UNIFORM_FLOAT;
  value = new ValueImpl<vec1>(glUniform1fv_, glVertexAttrib1fv_, (const vec1*)val, count);
}

void GLSLValue::setValue(const vec1* val, int count)
{
  type = UNIFORM_FLOAT;
  value = new ValueImpl<vec1>(glUniform1fv_, glVertexAttrib1fv_, val, count);
}

void GLSLValue::setValue(const vec2* val, int count)
{
  type = UNIFORM_FLOAT2;
  value = new ValueImpl<vec2>(glUniform2fv_, glVertexAttrib2fv_, val, count);
}

void GLSLValue::setValue(const vec3* val, int count)
{
  type = UNIFORM_FLOAT3;
  value = new ValueImpl<vec3>(glUniform3fv_, glVertexAttrib3fv_, val, count);
}

void GLSLValue::setValue(const vec4* val, int count)
{
  type = UNIFORM_FLOAT4;
  value = new ValueImpl<vec4>(glUniform4fv_, glVertexAttrib4fv_, val, count);
}

void GLSLValue::setValue(const mat2* val, int count)
{
  type = UNIFORM_MATRIX2;
  value = new ValueImpl<mat2>(glUniformTransposedMatrix2fv_, 0, val, count);
}

void GLSLValue::setValue(const mat3* val, int count)
{
  type = UNIFORM_MATRIX3;
  value = new ValueImpl<mat3>(glUniformTransposedMatrix3fv_, 0, val, count);
}

void GLSLValue::setValue(const mat4* val, int count)
{
  type = UNIFORM_MATRIX4;
  value = new ValueImpl<mat4>(glUniformTransposedMatrix4fv_, 0, val, count);
}

bool Shader::setUniform(const QString& name, const GLSLValue& value)
{
  bool found = false;
  for(size_t i = 0; i < model_uniform_names.size(); ++i) {
    if(model_uniform_names[i] == name) {
      model_uniform_values[i] = value;
      found = true;
    }
  }
  if(!found) {
    model_uniform_names.push_back(name);
    model_uniform_values.push_back(value);
  }
  return found;
}

void Shader::setUniform_instant(const QString& name, const GLSLValue& value) {
  loadUniform(_program, name, value);
}

void Shader::loadUniform(GLint program, const QString& name, const GLSLValue& value)
{
  // err << "loadUniform(" << program << "," << name << "," << value << ")" << endl;
  // err << "opengl->glGetUniformLocation = 0x" << QString::number((uintptr_t)opengl->glGetUniformLocation, 16) << endl;
  QByteArray name_ba = name.toLatin1();
  GLint loc = opengl->glGetUniformLocation(program, name_ba.constData());
  // err << "location: " << loc << endl;
  if(loc != -1) {
    // err << "Setting uniform to " << loc << endl;
    value.setUniform(loc);
  } else if(verbosity > 0) {
    err << "Error, uniform '" << name << "' was not found in the program" << endl;
  }
}

GLuint Shader::attribLocation(const QString& name)
{
  GLuint loc = opengl->glGetAttribLocation(_program, name.toLatin1());
  return loc;
}

void Shader::setAttrib(const QString& name, const GLSLValue& value)
{
  GLuint loc = attribLocation(name);
  setAttrib(loc, value);
}

void Shader::setAttrib(GLuint loc, const GLSLValue& value) {
  value.setAttrib(loc);
}

void Shader::setupUniforms()
{
  if(hasShaders() and _program) {
    QSet<QString> done_uniforms;
    for(size_t i = 0; i < model_uniform_names.size(); ++i) {
      const QString& name = model_uniform_names[i];
      if(!done_uniforms.contains(name)) {
        loadUniform(_program, name, model_uniform_values[i]);
        if(verbosity > 2)
          err << "Loading uniform '" << name << "' with value " << model_uniform_values[i] << endl;
        done_uniforms.insert(name);
      } else if(verbosity > 1) {
        err << "Warning: uniform " << name << " defined more than once" << endl;
      }
    }
    for(size_t i = 0; i < uniform_names.size(); ++i) {
      const QString& name = uniform_names[i];
      if(!done_uniforms.contains(name)) {
        loadUniform(_program, name, uniform_values[i]);
        if(verbosity > 2)
          err << "Loading uniform '" << name << "' with value " << uniform_values[i] << endl;
        done_uniforms.insert(name);
      } else if(verbosity > 1) {
        err << "Warning: uniform '" << name << "' overriden by model or defined more than once" << endl;
      }
    }
  } else if(verbosity > 2) {
    err << "Warning: trying to setup uniforms without having a running program" << endl;
  }
}
} // namespace lgx
