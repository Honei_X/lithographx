#!/bin/bash
INPUT="$1"
shift
SIZES="$@"
if [ -z "$INPUT" ]; then
    echo "Usage: $0 file.svg" >&2
    exit 2
fi
BASE=$(basename "$INPUT" .svg)
OUTPUT="$BASE".ico
declare -a PNG_FILES
for s in $SIZES; do
    PNG="$BASE$s.png"
    inkscape -z -w $s -h $s $INPUT -e "$PNG"
    PNG_FILES+=("$PNG")
done
icotool -c "${PNG_FILES[@]}" -o "$OUTPUT"
