/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Color.hpp"

namespace lgx {
namespace util {
QColor convertToQColor(const Color<float>& c) {
  return QColor::fromRgbF(c.r(), c.g(), c.b(), c.a());
}

QColor convertToQColor(const Color<double>& c) {
  return QColor::fromRgbF(c.r(), c.g(), c.b(), c.a());
}

QColor convertToQColor(const Color<long double>& c) {
  return QColor::fromRgbF(c.r(), c.g(), c.b(), c.a());
}

QColor convertToQColor(const Color<unsigned char>& c) {
  return QColor(c.r(), c.g(), c.b(), c.a());
}

QColor convertToQColor(const Color<unsigned short>& c) {
  return QColor(c.r(), c.g(), c.b(), c.a());
}

QColor convertToQColor(const Color<unsigned int>& c) {
  return QColor(c.r(), c.g(), c.b(), c.a());
}

QColor convertToQColor(const Color<unsigned long>& c) {
  return QColor(c.r(), c.g(), c.b(), c.a());
}

QColor convertToQColor(const Color<unsigned long long>& c) {
  return QColor(c.r(), c.g(), c.b(), c.a());
}

QColor convertToQColor(const Color<char>& c) {
  return QColor(c.r(), c.g(), c.b(), c.a());
}

QColor convertToQColor(const Color<short>& c) {
  return QColor(c.r(), c.g(), c.b(), c.a());
}

QColor convertToQColor(const Color<int>& c) {
  return QColor(c.r(), c.g(), c.b(), c.a());
}

QColor convertToQColor(const Color<long>& c) {
  return QColor(c.r(), c.g(), c.b(), c.a());
}

QColor convertToQColor(const Color<long long>& c) {
  return QColor(c.r(), c.g(), c.b(), c.a());
}

void convertFromQColor(Color<float>& c, const QColor& col)
{
  c.r() = col.redF();
  c.g() = col.greenF();
  c.b() = col.blueF();
  c.a() = col.alphaF();
}

void convertFromQColor(Color<double>& c, const QColor& col)
{
  c.r() = col.redF();
  c.g() = col.greenF();
  c.b() = col.blueF();
  c.a() = col.alphaF();
}

void convertFromQColor(Color<long double>& c, const QColor& col)
{
  c.r() = col.redF();
  c.g() = col.greenF();
  c.b() = col.blueF();
  c.a() = col.alphaF();
}

void convertFromQColor(Color<unsigned char>& c, const QColor& col)
{
  c.r() = col.red();
  c.g() = col.green();
  c.b() = col.blue();
  c.a() = col.alpha();
}

void convertFromQColor(Color<unsigned short>& c, const QColor& col)
{
  c.r() = col.red();
  c.g() = col.green();
  c.b() = col.blue();
  c.a() = col.alpha();
}

void convertFromQColor(Color<unsigned int>& c, const QColor& col)
{
  c.r() = col.red();
  c.g() = col.green();
  c.b() = col.blue();
  c.a() = col.alpha();
}

void convertFromQColor(Color<unsigned long>& c, const QColor& col)
{
  c.r() = col.red();
  c.g() = col.green();
  c.b() = col.blue();
  c.a() = col.alpha();
}

void convertFromQColor(Color<unsigned long long>& c, const QColor& col)
{
  c.r() = col.red();
  c.g() = col.green();
  c.b() = col.blue();
  c.a() = col.alpha();
}

void convertFromQColor(Color<char>& c, const QColor& col)
{
  c.r() = col.red();
  c.g() = col.green();
  c.b() = col.blue();
  c.a() = col.alpha();
}

void convertFromQColor(Color<short>& c, const QColor& col)
{
  c.r() = col.red();
  c.g() = col.green();
  c.b() = col.blue();
  c.a() = col.alpha();
}

void convertFromQColor(Color<int>& c, const QColor& col)
{
  c.r() = col.red();
  c.g() = col.green();
  c.b() = col.blue();
  c.a() = col.alpha();
}

void convertFromQColor(Color<long>& c, const QColor& col)
{
  c.r() = col.red();
  c.g() = col.green();
  c.b() = col.blue();
  c.a() = col.alpha();
}

void convertFromQColor(Color<long long>& c, const QColor& col)
{
  c.r() = col.red();
  c.g() = col.green();
  c.b() = col.blue();
  c.a() = col.alpha();
}
} // namespace util
} // namespace lgx
