/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef SHAPES_HPP
#define SHAPES_HPP

#include <LGXConfig.hpp>

#include <Geometry.hpp>
#include <Mesh.hpp>

#include <vector>

namespace lgx {
namespace shape {

/**
 * Add a single triangle to an existing mesh
 */
LGXCORE_EXPORT bool addTriangle(vvgraph& S, std::vector<vertex>& vertices, Point3i tri);

/**
 * This function creates a graph from a triangle soup.
 *
 * \note The graph will not be cleared, the triangles will be added to the
 * graph.
 *
 * \param S Graph that will be updated.
 * \param vertices List of vertices to insert in S
 * \param triangles List of triplet of indices in \c vertices defining
 * triangles
 *
 * \returns true on success, false on failure
 *
 * \ingroup ProcessUtils
 */
LGXCORE_EXPORT bool meshFromTriangles(vvgraph& S, std::vector<vertex>& vertices, std::vector<Point3i>& triangles);

class LGX_EXPORT AbstractShape {
public:
  AbstractShape();
  AbstractShape(const AbstractShape&) = default;
  AbstractShape(AbstractShape&&) = default;

  virtual ~AbstractShape();

  /**
   * Add the shape to an existing mesh
   *
   * \param mesh Mesh to modify
   * \param center Center of the shape
   * \param sx Size and orientation of the x axis of the shape
   * \param sy Size and orientation of the y axis of the shape
   * \param sz Size and orientation of the z axis of the shape
   * \param label Which label to give to the new vertices
   *
   * \note No check is done to ensure rx, ry and rz are orthogonal to each other.
   */
  virtual bool addTo(Mesh* mesh,
                     const Point3f& center,
                     const Point3f& rx, const Point3f& ry, const Point3f& rz,
                     int label = 0) = 0;

  /**
   * Add the shape to an existing mesh
   *
   * \param mesh Mesh to modify
   * \param center Center of the sphere
   * \param radius Radius of the sphere
   * \param label Which label to give to the new vertices
   */
  virtual bool addTo(Mesh *mesh,
                     const Point3f& center,
                     double radius,
                     int label = 0)
  {
    return addTo(mesh, center, Point3f(radius, 0, 0), Point3f(0, radius, 0), Point3f(0, 0, radius), label);
  }

  /**
   * If the last operation failed, this will hold a string describing the error
   */
  QString errorString() const { return _errorString; }

  /**
   * True only if initialization succeeded
   */
  bool valid() const { return _valid; }

  /**
   * True only if initialization succeeded
   */
  explicit operator bool() const { return _valid; }

  /// Call this function after you checked the error string to clear it
  void clearErrorString() { _errorString = QString(); }

protected:
  QString _errorString;
  bool _valid = false;
};

class LGX_EXPORT AbstractSphere : public AbstractShape {
public:
  AbstractSphere();
  AbstractSphere(const AbstractSphere&) = default;
  AbstractSphere(AbstractSphere&&) = default;
  ~AbstractSphere();

  AbstractSphere& operator=(const AbstractSphere&) = default;
  AbstractSphere& operator=(AbstractSphere&&) = default;
  /**
   * Add the sphere or ellipsoids to an existing mesh
   *
   * \param mesh Mesh to modify
   * \param center Center of the sphere / ellipsoid
   * \param rx Size and orientation of the x axis of the sphere
   * \param ry Size and orientation of the y axis of the sphere
   * \param rz Size and orientation of the z axis of the sphere
   * \param label Which label to give to the new vertices
   *
   * \note No check is done to ensure rx, ry and rz are orthogonal to each other.
   */
  bool addTo(Mesh* mesh,
             const Point3f& center,
             const Point3f& rx, const Point3f& ry, const Point3f& rz,
             int label = 0) override;
protected:
  std::vector<Point3f> positions;
  std::vector<Point3i> triangles;
};

/**
 * \class UniformSphere Shapes.hpp <Shapes.hpp>
 *
 * Creates a sphere with n points placed uniformely on the surface of a unit sphere.
 */
class LGX_EXPORT UniformSphere : public AbstractSphere {
public:
  UniformSphere(size_t nb_points);
  UniformSphere(const UniformSphere&) = default;
  UniformSphere(UniformSphere&) = default;

  UniformSphere& operator=(const UniformSphere&) = default;
  UniformSphere& operator=(UniformSphere&&) = default;
};

/**
 * \class UniformSphere Shapes.hpp <Shapes.hpp>
 *
 * Creates a sphere based on a regular dodecagon subdivided n times.
 */
class LGX_EXPORT RegularSphere : public AbstractSphere {
public:
  RegularSphere(size_t nb_subdivisions);
  RegularSphere(const RegularSphere&) = default;
  RegularSphere(RegularSphere&) = default;

  RegularSphere& operator=(const RegularSphere&) = default;
  RegularSphere& operator=(RegularSphere&&) = default;
};

class LGX_EXPORT Cube : public AbstractShape
{
public:
  Cube();
  virtual ~Cube() {}

  Cube(const Cube&) = default;
  Cube(Cube&&) = default;

  Cube& operator=(const Cube&) = default;
  Cube& operator=(Cube&&) = default;

  /**
   * Add the cube to an existing mesh
   *
   * \param mesh Mesh to modify
   * \param center Center of the shape
   * \param sx Size and orientation of the x axis of the shape
   * \param sy Size and orientation of the y axis of the shape
   * \param sz Size and orientation of the z axis of the shape
   * \param label Which label to give to the new vertices
   *
   * \note No check is done to ensure rx, ry and rz are orthogonal to each other.
   */
  bool addTo(Mesh* mesh,
             const Point3f& center,
             const Point3f& rx, const Point3f& ry, const Point3f& rz,
             int label = 0) override;

};

class Cylinder : public AbstractShape
{
public:
  Cylinder(size_t nb_slices);
  virtual ~Cylinder() { }

  Cylinder(const Cylinder&) = default;
  Cylinder(Cylinder&&) = default;

  Cylinder& operator=(const Cylinder&) = default;
  Cylinder& operator=(Cylinder&&) = default;

  /**
   * Add the cylinder to an existing mesh
   *
   * \param mesh Mesh to modify
   * \param center Center of the shape
   * \param sx Size and orientation of the x axis of the shape
   * \param sy Size and orientation of the y axis of the shape
   * \param sz Size and orientation of the z axis of the shape
   * \param label Which label to give to the new vertices
   *
   * \note No check is done to ensure rx, ry and rz are orthogonal to each other.
   */
  bool addTo(Mesh* mesh,
             const Point3f& center,
             const Point3f& rx, const Point3f& ry, const Point3f& rz,
             int label = 0) override;

protected:
  size_t _slices;
};

} // namespace shape
} // namespace lgx

#endif // SHAPES_HPP

