/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef CUTTINGSURFACE_HPP
#define CUTTINGSURFACE_HPP

#include <LGXConfig.hpp>

#include <Geometry.hpp>
#include <LGXViewer/qglviewer.h>

namespace lgx {

namespace process {
class SetupProcess;
} // namespace process

typedef util::Vector<2, int> Point2i;
typedef util::Vector<3, float> Point3f;

class LGX_EXPORT CuttingSurface : public QObject {
  Q_OBJECT
  friend class process::SetupProcess;

public:
  CuttingSurface();
  ~CuttingSurface();

  enum Mode { PLANE, THREE_AXIS, BEZIER };

  bool threeAxis() const {
    return _mode == THREE_AXIS;
  }
  bool plane() const {
    return _mode == PLANE;
  }
  bool bezier() const {
    return _mode == BEZIER;
  }
  Mode mode() const {
    return _mode;
  }
  void setMode(Mode m)
  {
    if(m != _mode) {
      _mode = m;
      hasChanged();
    }
  }

  bool drawGrid() const {
    return _drawGrid;
  }
  void showGrid()
  {
    if(!_drawGrid) {
      _drawGrid = true;
      hasChanged();
    }
  }
  void hideGrid()
  {
    if(_drawGrid) {
      _drawGrid = false;
      hasChanged();
    }
  }

  void show()
  {
    if(!_draw) {
      _draw = true;
      hasChanged();
    }
  }
  void hide()
  {
    if(_draw) {
      _draw = false;
      hasChanged();
    }
  }
  bool isVisible() const {
    return _draw;
  }

  bool showPoints() const {
    return _showPoints;
  }
  void setShowPoints(bool show) {
    _showPoints = show;
  }

  bool blending() const {
    return _blend;
  }
  void setBlending(bool on) {
    _blend = on;
  }

  /**
   * Get the current opacity level of the surface
   */
  float opacity() const {
    return _opacity;
  }
  /**
   * Change the current opactity level of the surface
   */
  void setOpacity(float f)
  {
    if(f < 0)
      _opacity = 0;
    else if(f > 1)
      _opacity = 1;
    else
      _opacity = f;
  }
  /**
   * Get the current brightness of the surface
   */
  float brightness() const {
    return _brightness;
  }
  /**
   * Change the current brightness of the surface
   */
  void setBrightness(float f)
  {
    if(f < 0)
      _brightness = 0;
    else if(f > 1)
      _brightness = 1;
    else
      _brightness = f;
  }


  const Point3f& size() const {
    return _size;
  }
  void setSize(const Point3f& s)
  {
    if(s != _size) {
      _size = s;
      hasChanged();
    }
  }

  const Point2i& surfSize() const {
    return _surfSize;
  }
  void setSurfSize(const Point2i& s)
  {
    if(s != _surfSize) {
      _surfSize = s;
      hasChanged();
    }
  }

  uint bezPoints() const {
    return _bezPoints;
  }
  void setBezPoints(uint n)
  {
    if(n != _bezPoints) {
      _bezPoints = n;
      hasChanged();
    }
  }

  void setBezierV(const std::vector<Point3f>& points)
  {
    if(points.size() == _bezierV.size()) {
      _bezierV = points;
      hasChanged();
    }
  }
  const std::vector<Point3f>& bezierV() const {
    return _bezierV;
  }
  std::vector<Point3f>& bezierV() {
    return _bezierV;
  }

  Point3f& bezierV(uint u, uint v) {
    return _bezierV[idx(u, v)];
  }
  const Point3f& bezierV(uint u, uint v) const {
    return _bezierV[idx(u, v)];
  }

  // Get frame
  qglviewer::ManipulatedFrame& frame() {
    return _frame;
  }
  const qglviewer::ManipulatedFrame& frame() const {
    return _frame;
  }

  // Get cutting plane rectangle
  void getSurfPoints(const qglviewer::Frame* stk_frame, std::vector<Point3f>& points, int& uSize, int& vSize);

  // Evaluate bezier
  Point3f evalCoord(float u, float v) const;
  Point3f evalNormal(float u, float v) const;

  void hasChanged() {
    _changed = true;
  }
  bool changed() const {
    return _changed;
  }

  void initBez();

  uint idx(uint u, uint v) const {
    return (u * _bezPoints + v);
  }

  // Binomial cooef
  int choose(int i, int j) const;

protected:
  void resetModified() {
    _changed = false;
  }

  Mode _mode = PLANE;
  bool _drawGrid = false;
  Point3f _size = Point3f(1.f, 1.f, 1.f);

  bool _draw = false, _showPoints = false, _blend = false;
  float _brightness = 1.f, _opacity = 1.f;
  std::vector<Point3f> _bezierV;
  Point2i _surfSize = Point2i(15, 15);
  uint _bezPoints = 0;
  std::vector<uint> _chooseV;

  bool _changed = false;

  // Clipping plane frame
  qglviewer::ManipulatedFrame _frame;
};
} // namespace lgx

#endif // CUTTINGSURFACE_HPP
