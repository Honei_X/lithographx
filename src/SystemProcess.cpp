/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "SystemProcess.hpp"
#include "SystemProcessLoad.hpp"
#include "SystemProcessSave.hpp"
#include "Information.hpp"

namespace lgx {
namespace process {

#define INIT_PROCESS(varName, ClassName, ProcessClass) \
  varName(new ClassProcessFactory<ProcessClass, ClassName>(), typeid(ClassName).name(), PROCESS_VERSION)

struct SystemRegistration {
  SystemRegistration()
    : INIT_PROCESS(stackImport, StackImport, StackProcess)
    , INIT_PROCESS(stackOpen, StackOpen, StackProcess)
    , INIT_PROCESS(stackExport, StackExport, StackProcess)
    , INIT_PROCESS(stackSave, StackSave, StackProcess)
    , INIT_PROCESS(meshImport, MeshImport, MeshProcess)
    , INIT_PROCESS(meshLoad, MeshLoad, MeshProcess)
    , INIT_PROCESS(meshExport, MeshExport, MeshProcess)
    , INIT_PROCESS(meshSave, MeshSave, MeshProcess)
    , INIT_PROCESS(meshReset, ResetMeshProcess, MeshProcess)
    , INIT_PROCESS(loadAllData, LoadAllData, GlobalProcess)
    , INIT_PROCESS(loadProjectFile, LoadProjectFile, GlobalProcess)
    , INIT_PROCESS(saveProjectFile, SaveProjectFile, GlobalProcess)
    , INIT_PROCESS(saveAll, SaveAll, GlobalProcess)
    , INIT_PROCESS(resetAll, ResetAll, GlobalProcess)
    , INIT_PROCESS(takeSnapshot, TakeSnapshot, GlobalProcess)
  {
  }

  StackRegistration stackImport, stackOpen, stackExport, stackSave;
  MeshRegistration meshImport, meshLoad, meshExport, meshSave, meshReset;
  GlobalRegistration loadAllData, loadProjectFile, saveProjectFile, saveAll, resetAll, takeSnapshot;
};

static SystemRegistration* systemRegistration = 0;

void registerSystemProcesses()
{
  if(systemRegistration == 0)
    systemRegistration = new SystemRegistration();
}

void unregisterSystemProcesses()
{
  if(lgx::DEBUG)
    Information::out << "Trying to unregister system processes" << endl;
  if(systemRegistration)
    delete systemRegistration;
  systemRegistration = 0;
}
} // namespace process
} // namespace lgx
