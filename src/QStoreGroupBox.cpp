/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "QStoreGroupBox.hpp"
#include "LithoGraphX.hpp"
#include "Information.hpp"

#include <QRegularExpression>

QStoreGroupBox::QStoreGroupBox(QWidget* parent)
  : QGroupBox(parent)
{
  setAcceptDrops(true);
}

QStoreGroupBox::QStoreGroupBox(const QString& title, QWidget* parent)
  : QGroupBox(title, parent)
{
  setAcceptDrops(true);
}

LithoGraphX* QStoreGroupBox::mainWindow()
{
  QWidget *p = parentWidget();
  while(p != nullptr) {
    LithoGraphX* lgx = dynamic_cast<LithoGraphX*>(p);
    if(lgx) return lgx;
    p = p->parentWidget();
  }
  return nullptr;
}

void QStoreGroupBox::dragEnterEvent(QDragEnterEvent* event)
{
  auto *lgx = mainWindow();
  auto mime = event->mimeData();
  if(mime->hasUrls()) {
    auto urls = mime->urls();
    if(urls.size() == 1) {
      auto url = urls[0];
      if(lgx->canAutoOpen(url.toLocalFile())) {
        event->accept();
        return;
      } else
        DEBUG_OUTPUT("Wrong type of files" << endl);
    } else
      DEBUG_OUTPUT("Too many files" << endl);
  } else
    DEBUG_OUTPUT("No urls" << endl);
  event->ignore();
}

void QStoreGroupBox::dropEvent(QDropEvent* event)
{
  auto *lgx = mainWindow();
  auto pth = event->mimeData()->urls()[0].toLocalFile();
  auto re = QRegularExpression("Stack(\\d)(Work|Main)Show");
  lgx::Information::out << "Regular expression '" << re.pattern() << "' for title '" << objectName() << "'" << endl;
  auto m = re.match(objectName());
  if(m.hasMatch()) {
    int stackid = m.captured(1).toInt() - 1;
    bool is_main = m.captured(2) == "Main";
    lgx->autoOpen(pth, stackid, is_main);
  } else {
    lgx::Information::out << "No match!" << endl;
  }
}
