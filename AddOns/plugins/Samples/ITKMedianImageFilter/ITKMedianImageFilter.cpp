/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ITKMedianImageFilter.hpp"

#include <ITKProgress.hpp>
#include <itkMedianImageFilter.h>

namespace lgx {
namespace process {
bool ITKMedianImageFilter::operator()(const Store* input, Store* output, Point3u radius)
{
  UImageConverter::Pointer converter = UImageConverter::New();

  converter->SetStore(input);

  typedef itk::MedianImageFilter<UImageType, UImageType> FilterType;
  typedef itk::MedianImageFilter<UImageType, UImageType>::InputSizeType RadiusType;
  FilterType::Pointer filter = FilterType::New();
  filter->SetInput(converter->GetOutput());
  RadiusType r = { radius.x(), radius.y(), radius.z() };
  filter->SetRadius(r);

  ITKProgress progress("Median Image Filter");
  progress.setFilter(filter);

  filter->Update();

  UImageType::ConstPointer result = filter->GetOutput();

  if(!UImageConverter::TransferImage(output->stack(), output, result))
    throw(QString("ITK Process tried to change the size of the stack"));

  output->changed();
  return true;
}
REGISTER_STACK_PROCESS(ITKMedianImageFilter);
}
}
