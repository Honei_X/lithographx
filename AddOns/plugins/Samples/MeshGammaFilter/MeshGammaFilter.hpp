/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MESH_GAMMA_FILTER_HPP
#define MESH_GAMMA_FILTER_HPP

#include <Process.hpp>

namespace lgx {
namespace process {
// Gamma filter on mesh signal, inherit from mesh process
class MeshGammaFilter : public MeshProcess {
public:
  MeshGammaFilter(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  // Standard call interface for all processes
  bool operator()(const QStringList& parms)
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0].toFloat());
  }

  // Call interface with parameters used
  bool operator()(Mesh* mesh, float gamma);

  // Plug-in folder
  QString folder() const {
    return "Signal";
  }
  // Plug-in name
  QString name() const {
    return "Mesh Gamma Filter";
  }
  // Plug-in long description
  QString description() const {
    return "Run a gamma correction filter on the mesh signal";
  }
  // List of parameter names
  QStringList parmNames() const {
    return QStringList() << "Gamma";
  }
  // List of parameter long descriptions
  QStringList parmDescs() const {
    return QStringList() << "Gamma value, must be positive.";
  }
  // List of parameter default values
  QStringList parmDefaults() const {
    return QStringList() << "0.5";
  }
  // Plug-in icon
  QIcon icon() const {
    return QIcon(":/images/GammaFilter.png");
  }
};
}
}
#endif
