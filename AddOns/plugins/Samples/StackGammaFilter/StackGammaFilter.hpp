/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef STACK_GAMMA_FILTER_HPP
#define STACK_GAMMA_FILTER_HPP

#include <Process.hpp>
#include <Stack.hpp>
#include <Store.hpp>

namespace lgx {
namespace process {
// Gamma filter on mesh signal, inherit from mesh process
class StackGammaFilter : public StackProcess {
public:
  StackGammaFilter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  // Standard call interface for all processes
  bool operator()(const QStringList& parms)
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    bool res = (*this)(input, output, parms[0].toFloat());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  // Call interface with parameters used
  bool operator()(const Store* input, Store* output, float gamma);

  // Plug-in folder
  QString folder() const {
    return "Filters";
  }
  // Plug-in name
  QString name() const {
    return "Gamma Filter";
  }
  // Plug-in long description
  QString description() const {
    return "Run a gamma correction filter";
  }
  // List of parameter names
  QStringList parmNames() const {
    return QStringList() << "Gamma";
  }
  // List of parameter long descriptions
  QStringList parmDescs() const {
    return QStringList() << "Gamma value, must be positive.";
  }
  // List of parameter default values
  QStringList parmDefaults() const {
    return QStringList() << "0.5";
  }
  // Plug-in icon
  QIcon icon() const {
    return QIcon(":/images/GammaFilter.png");
  }
};
}
}
#endif
