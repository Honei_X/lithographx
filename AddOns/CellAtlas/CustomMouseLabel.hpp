/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef CUSTOMMOUSELABEL_HPP
#define CUSTOMMOUSELABEL_HPP
  
#include <QLabel>
#include <QMouseEvent>
#include <QEvent>
#include <QDebug>

class QMouseEvent;

class CustomMouseLabel : public QLabel
{
Q_OBJECT
signals:
    void mousePressed( const QPoint& );
    void mouseReleased( const QPoint& );
    void mouseDouble( const QPoint& );
    void keyPressed( const QString& );
    void mouseMove( const QPoint& );
public:
    CustomMouseLabel( QWidget* parent = 0, Qt::WindowFlags f = 0 );
    CustomMouseLabel( const QString& text, QWidget* parent = 0, Qt::WindowFlags f = 0 );
    virtual ~CustomMouseLabel() {}

    void mousePressEvent( QMouseEvent* event );
    void mouseReleaseEvent( QMouseEvent* event );
    void mouseDoubleClickEvent( QMouseEvent* event );
    void mouseMoveEvent( QMouseEvent* event );
    void keyPressEvent( QKeyEvent* event );

};

#endif
