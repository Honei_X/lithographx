/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef RootCellAnalyzing_H
#define RootCellAnalyzing_H

#include "RootCellProcessing.hpp"
#include <math.h>
#include "Progress.hpp"

#define PI 3.14159265

using namespace std;

namespace mgx {
namespace process {

class RootCellAnalyzing 
{
public:
  // estimates the size of the cell in a given direction
  double estimateCellLength(int currentLabel, Point3f coordCellCentroid, Point3f& dirVec, 
                                                                   triVector& cellTriangles);

  // analyzes the root based on: Bezier, segmeneted mesh and surface mesh
  // creates multiple outputs:
  // cellTriangles: map of label -> unique triangles
  // cellCentroids: center of all cells
  bool analyzeEmbryo(std::vector<int>& uniqueLabels, const vvgraph& segmentedMesh, 
    const vvgraph& surfaceMesh, bezierMap& bMap, bezierMap& diffbMap, labelVertexMap& lvMap, 
    RootCellProcessing& rootDataBox, int firstLabel, double minVolume);

private:
  labelBoolMap badCells;
};
}
}

#endif
