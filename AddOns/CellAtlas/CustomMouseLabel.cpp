/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "CustomMouseLabel.hpp"

CustomMouseLabel::CustomMouseLabel( QWidget * parent, Qt::WindowFlags f )
    : QLabel( parent, f ) {}

CustomMouseLabel::CustomMouseLabel( const QString& text, QWidget* parent, Qt::WindowFlags f )
    : QLabel( text, parent, f ) {}

void CustomMouseLabel::mousePressEvent( QMouseEvent* event )
{
    const QPoint p = event->pos();
    emit mousePressed( p );
}

void CustomMouseLabel::mouseReleaseEvent( QMouseEvent* event )
{
    const QPoint p = event->pos();
    emit mouseReleased( p );
}

void CustomMouseLabel::mouseDoubleClickEvent( QMouseEvent* event )
{
    const QPoint p = event->pos();
    emit mouseDouble( p );
}

void CustomMouseLabel::mouseMoveEvent( QMouseEvent* event )
{
    const QPoint p = event->pos();
    emit mouseMove( p );
}

void CustomMouseLabel::keyPressEvent( QKeyEvent* event )
{
    const QString s = event->text();
    emit keyPressed( event->text() );
}

