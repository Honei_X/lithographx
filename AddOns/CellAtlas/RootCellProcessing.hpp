/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef RootCellProcessing_H
#define RootCellProcessing_H

#include "MGXCompat.hpp"
#include "Geometry.hpp"
#include "Mesh.hpp"

namespace mgx {
namespace process {

using util::Vector;

typedef Vector<2, int> Vec2i;
typedef std::vector<Vec2i> labelVec;
typedef Vector<2, double> Vec2d;
typedef Vector<4, double> Vec4d;
typedef std::vector<Vec2d> clusterVec;

typedef std::vector<vertex> vMap;
typedef std::map<int, vMap> labelVertexMap;

typedef std::map<int, Point3f> bezierMap;

typedef std::map<int, Point3f> labelPosMap;
typedef std::map<int, double> labelDataMap;
typedef std::map<int, bool> labelBoolMap;

typedef Vector<3, vertex> tri;
typedef std::vector<tri> triVector;
typedef std::map<int, triVector> labelTriMap;

typedef std::map<int,Vec2d > maximaVector;
typedef std::map<int,int > IntIntMap;
typedef std::map<int,std::map<int,double> > cellMorphLandscape;

typedef std::map <int, std::vector<int> > intToVec;

typedef std::map<Vec2i, float> Vec2iFloatMap;
typedef std::pair<Vec2i, float> Vec2iFloatPair;

using namespace std;

// structure that saves general root cell information
struct rootDataStructure
{
  ////// general overall data /////

  bool hasRadicle;

  int rootType; // 1 = embryo, 2 = hypocotyl, 3 = root

  // first and last root cap 
  int labelFirstCell, labelLastRootCapCell;

  // vector with all labels
  std::vector<int> uniqueLabels;

  int numCells; // number of cells

  //intToVec cellConnections;
  //intToVec cellWallAreas;
  Vec2iFloatMap wallArea;
  labelDataMap cellWallArea;

  labelDataMap correctedLengths;

  bezierMap bez, diffBez;
  labelPosMap diffBezInterp, bezInterp;

  IntIntMap associatedCorticalCell;

  labelBoolMap mislabelled;

  // currently not needed
  // map of: label -> all vertices
  //labelVertexMap lvMap;

  // currently not needed
  // map of: label -> all unique triangles
  //labelTriMap cellTriangles;

  ////// cell data /////

  // currently not needed
  // label -> center of cell
  //labelPosMap cellCentroids;

  // label -> volume of cell
  labelDataMap cellVolumes;

  // label -> booelan that is true when cell is bad (too small or no ray intercept)
  labelBoolMap cellBad;

  labelDataMap radialDis;
  labelDataMap scaledRadialDis;
  labelDataMap azimuthal;

  labelPosMap cellCentroids;

  // label -> location on Bezier (0...1)
  labelDataMap arclengths;

  // label -> radial/longitudinal/circumferential vector
  labelPosMap dirRad, dirLong, dirCirc;

  // label -> radial/longitudinal/circumferential cell length
  labelDataMap lengthRad, lengthLong, lengthCirc;

};

// structure that saves heatmap specific data
struct heatMapDataStructure
{
  // labeldata maps for the generation of the landscape
  labelDataMap x, y;
  int chosenY;
  int chosenX;

  int gridSize;
  double sigma;
  Vec4d heatMapMinMax;
  Vec4d heatMapStatistics;

  // label -> boolean mainBody, rootCap, columella
  labelBoolMap cells;
  int cellCounter; // cell counter columella

  // landscape function of 2 characteristics with gaussian bell for each cell
  cellMorphLandscape heatMap;
  double high;

  // location of all cells within the heatmap
  cellMorphLandscape points;

  maximaVector maximaHeatMapAll;
  maximaVector maximaHeatMapSelected;
  IntIntMap maximaHeatMapSelectedLabel;

  // map that relates each cell to the nearest maximum
  labelDataMap nearestMaximumOfCell;

  int maximaHeatMapSelectedCounter;

  int numberOfClusters;
  int activatedMaximum;

  labelBoolMap preselect;    

  labelDataMap cellLabel;

  heatMapDataStructure() : maximaHeatMapSelectedCounter(0) {}
};

class RootCellProcessing
{

public:
  void initHeatMapDataStructure(heatMapDataStructure& body, double sig);

  // calculates the value of a two dimensional gaussian function at location (x,y)
  double gauss1D(double x, double muX, double sigmaX);

  // calculates the value of a two dimensional gaussian function at location (x,y)
  double gauss2D(double x, double y, double muX, double muY, double sigmaX, double sigmaY);

  // interpolates the appropriate array index for the landscape function which has a 
  // resolution of newRange+1 
  double interpolateArrayIndex(double value, double min, double max, double newRange);

  // finds min and max of a labelData map
  void calcMinMax(labelDataMap& map, double& min, double& max);

  // finds min, max, avg and std of the cells, needed for scaling the heatmap size
  void findHeatStatistics(heatMapDataStructure& body);

  // generates the heatmap (=cellMorphLandscape) of the cell set (=body)
  cellMorphLandscape generateCellMorphologyLandscape(heatMapDataStructure& body);

  // find all local maxima in a cellMorphLandscape (=heatmap) and save their positions
  maximaVector findMaxima(cellMorphLandscape& landsc);

  // label all cells according to a label-to-maximum and a maximum-to-referenceMaximum map
  labelDataMap labelCells(labelDataMap& labelToMax, labelDataMap& maxToRef,
                                                       heatMapDataStructure& body);

  // reverse the arclengths if the root has the wrong orientation
  void correctDirections();

  // assign the root regions according the selected cells and arclengths
  void assignRootRegions();

  // relate each cell in the root to a maxima in the heatmap by following the steepest 
  // gradient upwards until a maximum is reached
  labelDataMap relateCellstoMaxima(heatMapDataStructure& body, cellMorphLandscape& landsc, 
                                         labelDataMap& x, labelDataMap& y, maximaVector maxima);

  // calculates the mininmum distance to a maximum from a given maximum
  double minDisToMax(maximaVector& maxima, labelDataMap& usedMax, int currentIdx);

  // function to assign cluster automatically by selecting the highest maxima
  void findAutomaticCluster(heatMapDataStructure& body, int nrOfClusters);

  void setAutoCluster(int nrCluster, bool ismainBody);

  // deletes a cluster from the current selected ones
  void delIdx(heatMapDataStructure& body, int idx);

  // big procedure to find the correct labels, calls earlier functions
  labelDataMap clusterCells(heatMapDataStructure& body);

  // sets parameters that come from MGX
  void setParameter(bool outputType, int label1, int label2);

  // finds highest heatmap value for scaling the colours in the heatmap
  double findHighMax(heatMapDataStructure& body);

  // sets the X coord of the heatmap
  void setHeatmapX(heatMapDataStructure& body, QString stringX);

  // sets the Y coord of the heatmap
  void setHeatmapY(heatMapDataStructure& body, QString stringY);

  // big procedure to generate the heatmap and label the cells
  void geometryBasedLabelling();

  // reset preselection
  void resetHeat(heatMapDataStructure& body);

  // take all currently selected clusters, look for nearest maximum and handle 
  // them as preselected and remove them from the heatmap
  void preselectCells(heatMapDataStructure& body);

  // label columella cells
  // cells with small wall area to neighbours become root cap
  // remaining cells: small become vasc, big col
  void reassignColumella(int labelRootCap, int labelCol);//, int labelVasc)

  // assign cortical cells subroutine to create the correctedlengths and to dffind 
  // the min and max of the arclengths
  void assignCortCellsGetMinMax(int labelCort, double& minS, double& maxS);

  // assign the cortical cell number to each cell in the root
  void assignCortCells(int labelCort, std::vector<double>& sSmooth, bezierMap& bMapS, int length);

  rootDataStructure rootData;
  heatMapDataStructure mainBody;
  heatMapDataStructure radicle;
  heatMapDataStructure columella;
  labelDataMap cellLabel;
};
}}

#endif
