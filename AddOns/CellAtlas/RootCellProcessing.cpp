/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "RootCellProcessing.hpp"

#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>

#include <gsl/gsl_bspline.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_statistics.h>
namespace mgx
{
namespace process
{

void RootCellProcessing::initHeatMapDataStructure(heatMapDataStructure& body, double sig)
{
  body.gridSize = 160;
  body.sigma = sig;

  if(body.chosenX == 2){
    body.x = rootData.azimuthal;
  } else if(body.chosenX == 3) {
    body.x = rootData.arclengths;
  } else if(body.chosenX == 4) {
    body.x = rootData.lengthRad;
  } else if(body.chosenX == 5) {
    body.x = rootData.lengthCirc;
  } else if(body.chosenX == 6) {
    body.x = rootData.lengthLong;
  } else {
    body.x = rootData.scaledRadialDis;
    body.chosenX = 1;
  }



  if(body.chosenY == 1){
    body.y = rootData.scaledRadialDis;
  } else if(body.chosenY == 2) {
    body.y = rootData.azimuthal;
  } else if(body.chosenY == 3) {
    body.y = rootData.arclengths;
  } else if(body.chosenY == 4) {
    body.y = rootData.lengthRad;
  } else if(body.chosenY == 6) {
    body.y = rootData.lengthLong;
  } else {
    body.y = rootData.lengthCirc;
    body.chosenY = 5;
  }
}

// calculates the value of a two dimensional gaussian function at location (x,y)
double RootCellProcessing::gauss1D(double x, double muX, double sigmaX)
{
  return (exp(-0.5*(x-muX)*(x-muX)/sigmaX/sigmaX));
}

// calculates the value of a two dimensional gaussian function at location (x,y)
double RootCellProcessing::gauss2D(double x, double y, double muX, double muY, double sigmaX, double sigmaY)
{
  //return (1.0/(2.0*3.14159*sigmaX*sigmaY)*exp(-0.5*((x-muX)*(x-muX)/sigmaX/sigmaX+(y-muY)*(y-muY)/sigmaY/sigmaY)));
  return (exp(-0.5*((x-muX)*(x-muX)/sigmaX/sigmaX+(y-muY)*(y-muY)/sigmaY/sigmaY)));
}

// interpolates the apropriate array index for the landscape function which has a resolution of newRange+1 
double RootCellProcessing::interpolateArrayIndex(double value, double min, double max, double newRange)
{
  if(value >= max){
    return newRange;
  } else if(value <= min){
    return 0;
  } else {
    return ((value - min)/(max - min) * newRange);
  }
}

// finds min and max of a labelData map
void RootCellProcessing::calcMinMax(labelDataMap& map, double& min, double& max)
{
  min = 1E20;
  max = -1E20;

  for(int i = 0; i<rootData.numCells; i++){
    int currentLabel = rootData.uniqueLabels[i];
    if(map[currentLabel] > max)
      max = map[currentLabel];
    if(map[currentLabel] < min)
      min = map[currentLabel];
  }
}

// finds min, max, avg and std of the cells, needed for scaling the heatmap size
void RootCellProcessing::findHeatStatistics(heatMapDataStructure& body)
{

  double minX = 1E20, minY = 1E20, maxX = -1E20, maxY = -1E20;
  double avgX = 0, avgY = 0, stdX = 0, stdY = 0;

  for(int i = 0; i<rootData.numCells; i++){
    int currentLabel = rootData.uniqueLabels[i];
    if(!body.preselect[currentLabel] && body.cells[currentLabel]){
      // NAN check
      if(body.x[currentLabel]!=body.x[currentLabel] || body.y[currentLabel]!=body.y[currentLabel]){
        body.x[currentLabel] = 0;
        body.y[currentLabel] = 0;
        cout << " * * * Warning: Cell with label " << currentLabel << " was detected with invalid cell sizes and set to size zero" << endl; 
      }

      avgX += body.x[currentLabel];
      avgY += body.y[currentLabel];
      if(body.cells[currentLabel]){
      //cout << "lab  " << currentLabel << "  " << x[currentLabel] << "  Y " << y[currentLabel] << endl;
        if(minX > body.x[currentLabel]){
          minX = body.x[currentLabel];}
        if(minY > body.y[currentLabel]){
          minY = body.y[currentLabel];}
        if(maxX < body.x[currentLabel]){
          maxX = body.x[currentLabel];}
        if(maxY < body.y[currentLabel]){
//cout << "newMaxY  " << currentLabel << "  " << body.x[currentLabel] << "  Y " << body.y[currentLabel] << endl;
          maxY = body.y[currentLabel];}
      }
    }
  }

  avgX /= body.cellCounter;
  avgY /= body.cellCounter;

  for(int i = 0; i<rootData.numCells; i++){
    int currentLabel = rootData.uniqueLabels[i];
    if(!body.preselect[currentLabel] && body.cells[currentLabel]){
      stdX += std::abs(avgX - body.x[currentLabel]);
      stdY += std::abs(avgY - body.y[currentLabel]);
    }
  }

  stdX /= body.cellCounter;
  stdY /= body.cellCounter;

  body.heatMapMinMax[0] = minX;
  body.heatMapMinMax[1] = maxX;
  body.heatMapMinMax[2] = minY;
  body.heatMapMinMax[3] = maxY;

  body.heatMapStatistics[0] = avgX;
  body.heatMapStatistics[1] = avgY;
  body.heatMapStatistics[2] = stdX;
  body.heatMapStatistics[3] = stdY;

}

// generates the heatmap (=cellMorphLandscape) of the cell set (=body)
cellMorphLandscape RootCellProcessing::generateCellMorphologyLandscape(heatMapDataStructure& body)
{

  cellMorphLandscape xyLandscape;

  findHeatStatistics(body);

  double minX = body.heatMapMinMax[0];
  double minY = body.heatMapMinMax[2];
  double maxX = body.heatMapMinMax[1];
  double maxY = body.heatMapMinMax[3];

  double avgX = body.heatMapStatistics[0];
  double avgY = body.heatMapStatistics[1];
  double stdX = body.heatMapStatistics[2];
  double stdY = body.heatMapStatistics[3];

  //cout << " mm     " << minX << "  " << maxX << "  " << minY << "  " << maxY << endl;
  //cout << " stat   " << avgX << "  " << stdX << "  " << avgY << "  " << stdY << endl;

  double threshold = 20; // threshold for outlier detection

  // take care of outliers
  if(avgX + threshold*stdX < maxX || avgX - threshold*stdX > minX || avgY + threshold*stdY < maxY || avgY - threshold*stdY > minY){
    // remove outlier cells and rerun statistics
    for(int i = 0; i<rootData.numCells; i++){
      int currentLabel = rootData.uniqueLabels[i];
      if(!body.preselect[currentLabel] && body.cells[currentLabel]){
        if(avgX + threshold*stdX < body.x[currentLabel] || avgX - threshold*stdX > body.x[currentLabel] || avgY + threshold*stdY < body.y[currentLabel] || avgY - threshold*stdY > body.y[currentLabel]){
          // this might cause problems with the preselect feature!
          body.x[currentLabel] = 0;
          body.y[currentLabel] = 0;
          cout << " * * * Warning: Cell with label " << currentLabel << " was detected as an outlier with abnormal cell sizes and set to size zero" << endl; 
        }
      }
    }
    findHeatStatistics(body);
    minX = body.heatMapMinMax[0];
    minY = body.heatMapMinMax[2];
    maxX = body.heatMapMinMax[1];
    maxY = body.heatMapMinMax[3];
  }

  // step size of the heatmap
  double stepX = (double)(maxX-minX)/(double)body.gridSize;
  double stepY = (double)(maxY-minY)/(double)body.gridSize;

  for(int i = 0; i<body.gridSize; i++){
    for(int j = 0; j<body.gridSize; j++){
      xyLandscape[i][j] = 0;
      body.points[i][j] = 0;
    }
  }

  for(int i = 0; i<rootData.numCells; i++){
    // calc center of gaussian
    int currentLabel = rootData.uniqueLabels[i];
    if(!body.preselect[currentLabel] && body.cells[currentLabel]){
      int binX = interpolateArrayIndex(body.x[currentLabel],minX,maxX,body.gridSize-1);
      int binY = interpolateArrayIndex(body.y[currentLabel],minY,maxY,body.gridSize-1);

      body.points[binX][binY]++;

      // cut off radius around the center
      int radius = std::max(10.0,body.sigma*2);
      for(int i = -radius; i<=radius; i++){
        for(int j = -radius; j<=radius; j++){
          // calc gaussian
          if(binX+i>=0 && binX+i<body.gridSize && binY+j>=0 && binY+j<body.gridSize){
            double xValue = interpolateArrayIndex(body.x[currentLabel]+i*stepX,minX,maxX,body.gridSize-1);
            double yValue = interpolateArrayIndex(body.y[currentLabel]+j*stepY,minY,maxY,body.gridSize-1);
            xyLandscape[binX+i][binY+j] += gauss2D(xValue, yValue, binX, binY, body.sigma, body.sigma);
          }
        }
      }
    }
  }
  //cout << " generated     " << endl;
  return xyLandscape;
}

// find all local maxima in a cellMorphLandscape (=heatmap) and save their positions
maximaVector RootCellProcessing::findMaxima(cellMorphLandscape& landsc)
{
  maximaVector maxima;

  int counter = 0;
  int size = landsc.size();

  // inner area of landscape
  for(int i=1; i<size-1; i++){
    for(int j=1; j<size-1; j++){
      // if maximum landsc[i][j] -> add i,j to maxima
      if(landsc[i][j] > landsc[i-1][j] &&
         landsc[i][j] > landsc[i][j-1] &&
         landsc[i][j] > landsc[i-1][j-1] &&
         landsc[i][j] > landsc[i+1][j] &&
         landsc[i][j] > landsc[i][j+1] &&
         landsc[i][j] > landsc[i+1][j+1] &&
         landsc[i][j] > landsc[i+1][j-1] &&
         landsc[i][j] > landsc[i-1][j+1])
      {
         Vec2d maxPos(i,j);
         maxima[counter] = maxPos;
         counter++;
      }

    }
  }
  // edges of landscape
  for(int i=1; i<size-1; i++){
    int j = 0;
    if(landsc[i][j] > landsc[i-1][j] &&
       landsc[i][j] > landsc[i+1][j] &&
       landsc[i][j] > landsc[i][j+1] &&
       landsc[i][j] > landsc[i+1][j+1] &&
       landsc[i][j] > landsc[i-1][j+1])
    {
       Vec2d maxPos(i,j);
       maxima[counter] = maxPos;
       counter++;
    }
  }

  for(int i=1; i<size-1; i++){
    int j = size-1;
    if(landsc[i][j] > landsc[i-1][j] &&
       landsc[i][j] > landsc[i+1][j] &&
       landsc[i][j] > landsc[i][j-1] &&
       landsc[i][j] > landsc[i+1][j-1] &&
       landsc[i][j] > landsc[i-1][j-1])
    {
       Vec2d maxPos(i,j);
       maxima[counter] = maxPos;
       counter++;
    }
  }

  for(int j=1; j<size-1; j++){
    int i = size-1;
    if(landsc[i][j] > landsc[i-1][j] &&
       landsc[i][j] > landsc[i][j-1] &&
       landsc[i][j] > landsc[i-1][j-1] &&
       landsc[i][j] > landsc[i][j+1] &&
       landsc[i][j] > landsc[i-1][j+1])
    {
       Vec2d maxPos(i,j);
       maxima[counter] = maxPos;
       counter++;
    }
  }

  for(int j=1; j<size-1; j++){
    int i = 0;
    if(landsc[i][j] > landsc[i+1][j] &&
       landsc[i][j] > landsc[i][j-1] &&
       landsc[i][j] > landsc[i+1][j-1] &&
       landsc[i][j] > landsc[i][j+1] &&
       landsc[i][j] > landsc[i+1][j+1])
    {
       Vec2d maxPos(i,j);
       maxima[counter] = maxPos;
       counter++;
    }
  }

  // corners of landscape
  int i = 0;
  int j = 0;
  if(landsc[i][j] > landsc[i+1][j] &&
     landsc[i][j] > landsc[i][j+1] &&
     landsc[i][j] > landsc[i+1][j+1])
  {
     Vec2d maxPos(i,j);
     maxima[counter] = maxPos;
     counter++;
  }

  i = size-1;
  j = 0;
  if(landsc[i][j] > landsc[i-1][j] &&
     landsc[i][j] > landsc[i][j+1] &&
     landsc[i][j] > landsc[i-1][j+1])
  {
     Vec2d maxPos(i,j);
     maxima[counter] = maxPos;
     counter++;
  }

  i = size-1;
  j = size-1;
  if(landsc[i][j] > landsc[i-1][j] &&
     landsc[i][j] > landsc[i][j-1] &&
     landsc[i][j] > landsc[i-1][j-1])
  {
     Vec2d maxPos(i,j);
     maxima[counter] = maxPos;
     counter++;
  }
  i = 0;
  j = size-1;
  if(landsc[i][j] > landsc[i+1][j] &&
     landsc[i][j] > landsc[i][j-1] &&
     landsc[i][j] > landsc[i+1][j-1])
  {
     Vec2d maxPos(i,j);
     maxima[counter] = maxPos;
     counter++;
  }

  return maxima;
  }

// label all cells according to a label-to-maximum and a maximum-to-referenceMaximum map
labelDataMap RootCellProcessing::labelCells(labelDataMap& labelToMax, labelDataMap& maxToRef, heatMapDataStructure& body)
{
  labelBoolMap rootArea = body.cells;
  IntIntMap labels = body.maximaHeatMapSelectedLabel;


  labelDataMap cellLabel;
  // generate data for the graphs

  for(int i = 0; i<rootData.numCells; i++){
    int currentLabel = rootData.uniqueLabels[i];
    if(!body.preselect[currentLabel] && body.cells[currentLabel]){
      cellLabel[currentLabel] = labels[maxToRef[labelToMax[currentLabel]]];
    }
  }
  return cellLabel;
  }

// reverse the arclengths if the root has the wrong orientation
void RootCellProcessing::correctDirections()
  {

  // check azimut of first cell

  if(rootData.arclengths[rootData.labelFirstCell] < 0.5) {
  // if low -> right direction
  // do nothing
  } else {
  // if high -> wrong direction
  // reverse arclengths
    for(int i = 0; i<rootData.numCells; i++){
      int currentLabel = rootData.uniqueLabels[i];
      rootData.arclengths[currentLabel] = 1.0 - rootData.arclengths[currentLabel];
    }

    int save = rootData.labelLastRootCapCell;
    rootData.labelLastRootCapCell = rootData.labelFirstCell;
    rootData.labelFirstCell = save;

    assignRootRegions();
  }

  }

// assign the root regions according the selected cells and arclengths
void RootCellProcessing::assignRootRegions()
  {

  mainBody.cellCounter = 0;
  columella.cellCounter = 0;
  radicle.cellCounter = 0;
    if(!rootData.hasRadicle){
    // no root cap -> all cells are in the main body
    for(int i=0; i<rootData.numCells; i++){
      int currentLabel = rootData.uniqueLabels[i];
      mainBody.cells[currentLabel] = true;
      radicle.cells[currentLabel] = false;
      columella.cells[currentLabel] = false;
      mainBody.cellCounter++;
    }
  } else {
    // with root cap -> 
    double arcFirst = rootData.arclengths[rootData.labelFirstCell];
    double arcLastRootCap = rootData.arclengths[rootData.labelLastRootCapCell];

    for(int i=0; i<rootData.numCells; i++){
      int currentLabel = rootData.uniqueLabels[i];
      if(rootData.arclengths[currentLabel] <= arcFirst){
        mainBody.cells[currentLabel] = false;
        radicle.cells[currentLabel] = false;
        columella.cells[currentLabel] = true;
        columella.cellCounter++;
      } else if(rootData.arclengths[currentLabel] <= arcLastRootCap){
        mainBody.cells[currentLabel] = false;
        radicle.cells[currentLabel] = true;
        columella.cells[currentLabel] = false;
        radicle.cellCounter++;
      } else {
        mainBody.cells[currentLabel] = true;
        radicle.cells[currentLabel] = false;
        columella.cells[currentLabel] = false;
        mainBody.cellCounter++;
      }
    }
  }

}

// relate each cell in the root to a maxima in the heatmap by following the steepest gradient upwards until a maximum is reached
labelDataMap RootCellProcessing::relateCellstoMaxima(heatMapDataStructure& body, cellMorphLandscape& landsc, labelDataMap& x, labelDataMap& y, maximaVector maxima)
{
  labelDataMap nearestMaximum;
  labelBoolMap rootArea = body.cells;
  int numMax = maxima.size();
  double minX = body.heatMapMinMax[0];
  double maxX = body.heatMapMinMax[1];
  double minY = body.heatMapMinMax[2];
  double maxY = body.heatMapMinMax[3];

  for(int i = 0; i<rootData.numCells; i++){
    int currentLabel = rootData.uniqueLabels[i];
    if(rootArea[currentLabel]){
      Vec2d currentPos;
      double minDis = 1E20;
      int minNum = 0;
      currentPos[0] = interpolateArrayIndex(x[currentLabel],minX,maxX,body.gridSize-1);
      currentPos[1] = interpolateArrayIndex(y[currentLabel],minY,maxY,body.gridSize-1);
      for(int j = 0; j<numMax; j++){ // find nearest maximum
        if(minDis > norm(currentPos - maxima[j])){
          minDis = norm(currentPos - maxima[j]);
          minNum = j;
        }
      }

      int stopCounter = 0;

      // if nearest maximum too far, then follow gradient
      while(minDis > body.sigma && stopCounter<100){
        stopCounter++;
        double maxValue = -1E20;
        int changeX = 0;
        int changeY = 0;
        // find highest value in neighbourhood and go there
        for(int i = -1; i<=1; i++){
          for(int j = -1; j<=1; j++){
            if(currentPos[0]+i >= 0 && currentPos[0]+i < body.gridSize && currentPos[1]+j >=0 && currentPos[1]+j < body.gridSize){
              if(maxValue < landsc[currentPos[0]+i][currentPos[1]+j]){
                maxValue = landsc[currentPos[0]+i][currentPos[1]+j];
                changeX = i;
                changeY = j;
              }
            }
          }
        }
        // find nearest max again
        minDis = 1E20;
        minNum = 0;
        currentPos[0] += changeX;
        currentPos[1] += changeY;
        for(int j = 0; j<numMax; j++){ // find nearest maximum
          if(minDis > norm(currentPos - maxima[j])){
            minDis = norm(currentPos - maxima[j]);
            minNum = j;
          }
        }
      }
      nearestMaximum[currentLabel] = minNum;
    }
  }


  return nearestMaximum;
}

// calculates the mininmum distance to a maximum from a given maximum
double RootCellProcessing::minDisToMax(maximaVector& maxima, labelDataMap& usedMax, int currentIdx)
{
  int numMax = maxima.size();
  double minDistance = 1E20;
  for(int j = 0; j<numMax; j++){
    if(usedMax[j] == 1){
      double distance = norm(maxima[j] - maxima[currentIdx]);
      if(distance < minDistance){
        minDistance = distance;
      }
    }
  }
  return minDistance;
}

// function to assign cluster automatically by selecting the highest maxima
void RootCellProcessing::findAutomaticCluster(heatMapDataStructure& body, int nrOfClusters)
{
  cellMorphLandscape landsc = body.heatMap;
  maximaVector maxima = body.maximaHeatMapAll;

  maximaVector automaticCluster;
  labelDataMap valueOfMax;
  labelDataMap usedMax;
  int numMax = maxima.size();
  IntIntMap labels;

  for(int j = 0; j<numMax; j++){
    Vec2d currentPoint = maxima[j];
    valueOfMax[j] = landsc[currentPoint[0]][currentPoint[1]];
    usedMax[j] = 0;
  }


  for(int i = 0; i<nrOfClusters; i++){
    int maxValue = 0;
    int maxPos = -1;
    for(int j = 0; j<numMax; j++){
      if(usedMax[j] == 0){
        if(maxValue < valueOfMax[j] && minDisToMax(maxima,usedMax,j) > 10.0){
          maxValue = valueOfMax[j];
          maxPos = j;
        }
      }
    }
    usedMax[maxPos] = 1;
    automaticCluster[i] = maxima[maxPos];
    labels[i] = i;
  }

  body.maximaHeatMapSelected = automaticCluster;
  body.maximaHeatMapSelectedLabel = labels;

}

void RootCellProcessing::setAutoCluster(int nrCluster, bool ismainBody)
{
  if(ismainBody)
    findAutomaticCluster(mainBody, nrCluster);
  else
    findAutomaticCluster(radicle, nrCluster);

}

// deletes a cluster from the current selected ones
void RootCellProcessing::delIdx(heatMapDataStructure& body, int idx)
{

  maximaVector max = body.maximaHeatMapSelected;
  IntIntMap labels = body.maximaHeatMapSelectedLabel;

  int sizeMax = max.size();
  maximaVector newMax;
  IntIntMap newLabels;
  int correct = 0;

  for(int i = 0; i<sizeMax; i++){
    if(i!=idx){
      newMax[i-correct] = max[i];
      newLabels[i-correct] = labels[i];
    } else {
      correct++;
    }
  }
  body.maximaHeatMapSelected = newMax;
  body.maximaHeatMapSelectedLabel = newLabels;
}


// big procedure to find the correct labels, calls earlier functions
labelDataMap RootCellProcessing::clusterCells(heatMapDataStructure& body)
{

  cellMorphLandscape landsc = body.heatMap;
  labelDataMap x = body.x;
  labelDataMap y = body.y;
  maximaVector maxima = body.maximaHeatMapAll;
  maximaVector customMax = body.maximaHeatMapSelected;

  // relate each cell to the nearest maximum
  labelDataMap nearestMaximum = relateCellstoMaxima(body, landsc, x, y, maxima);

  int numMax = maxima.size();
  int nrOfCustomMax = customMax.size();

  labelDataMap maximaClustMap;

  for(int i = 0; i<numMax; i++){
    // calc distance to reference maximum and find nearest ref max
    double minDis = 1E20;
    int idxRef = 0;
    for(int j = 0; j<nrOfCustomMax; j++){
      double distance = norm(customMax[j]-maxima[i]);
      if(minDis > distance){
        minDis = distance;
        idxRef = j;
      }
    }
    maximaClustMap[i] = idxRef;
    //cout << "m i " << i << "  " << idxRef << "  " <<  refMainBody[idxRef] << "   " << maxima[i] << endl;

  }

  return labelCells(nearestMaximum, maximaClustMap, body);
}

// sets parameters that come from MGX
void RootCellProcessing::setParameter(bool outputType, int label1, int label2)
{
  /*if(not outputType.isEmpty())
    {
      if(outputType == "Cell Type"){
        //m->showNormal();
      } else if(outputType == "Embryo"){
        rootData.rootType = 1;
      } else if(outputType == "Hypocotyl"){
        rootData.rootType = 2;
      } else if(outputType == "Root"){
        rootData.rootType = 3;
      } else {
        rootData.rootType = -1;
        //warning << QString("Invalid type '%1'").arg(outputType);
      }
    }*/
    //if(!outputType){
    rootData.hasRadicle = outputType; // hypocotyl
    //} else {
    //  rootData.hasRadicle = true; // embryo/root
    //}

  if(rootData.arclengths[label1] < rootData.arclengths[label2]){
    rootData.labelLastRootCapCell = label2;
    rootData.labelFirstCell = label1;
  } else {
    rootData.labelLastRootCapCell = label1;
    rootData.labelFirstCell = label2;
  }

  initHeatMapDataStructure(mainBody, 4.0);
  initHeatMapDataStructure(radicle, 6.0);

}

// finds highest heatmap value for scaling the colours in the heatmap
double RootCellProcessing::findHighMax(heatMapDataStructure& body)
{

  maximaVector maxVec = body.maximaHeatMapAll;
  cellMorphLandscape landsc = body.heatMap;

  double high1 = 0, high2 = 0, high3 = 0;

  int maxVecSize = maxVec.size();

  for(int i=0; i<maxVecSize; i++)
    {
    Vec2d currentPos = maxVec[i];
    int x = currentPos[0];
    int y = currentPos[1];
    double maxima = landsc[x][y];
      if(high1 < maxima)
      {
        high3 = high2; 
        high2 = high1;
        high1 = maxima;
      } else if(high2 < maxima)
      {
        high3 = high2; 
        high2 = maxima;
      } else if(high3 < maxima)
      {
        high3 = maxima; 
      }
    }
    //cout << "highs3  " << high1 << "  " << high2 << "  " << high3 << endl;
  return high3;

}

// sets the X coord of the heatmap
void RootCellProcessing::setHeatmapX(heatMapDataStructure& body, QString stringX)
{
  if(stringX == "Radial Cell Length") {
    body.x = rootData.lengthRad;
    body.chosenX = 4;
  } else if(stringX == "Circumferential Cell Length") {
    body.x = rootData.lengthCirc;
    body.chosenX = 5;
  } else if(stringX == "Longitudinal Cell Length") {
    body.x = rootData.lengthLong;
    body.chosenX = 6;
  } else if(stringX == "Radial Coordinate") {
    body.x = rootData.scaledRadialDis;
    body.chosenX = 1;
  } else if(stringX == "Circumferential Coordinate") {
    body.x = rootData.azimuthal;
    body.chosenX = 2;
  } else if(stringX == "Longitudinal Coordinate") {
    body.x = rootData.arclengths;
    body.chosenX = 3;
  }
}

// sets the Y coord of the heatmap
void RootCellProcessing::setHeatmapY(heatMapDataStructure& body, QString stringY)
{
  if(stringY == "Radial Cell Length") {
    body.y = rootData.lengthRad;
    body.chosenY = 4;
  } else if(stringY == "Circumferential Cell Length") {
    body.y = rootData.lengthCirc;
    body.chosenY = 5;
  } else if(stringY == "Longitudinal Cell Length") {
    body.y = rootData.lengthLong;
    body.chosenY = 6;
  } else if(stringY == "Radial Coordinate") {
    body.y = rootData.scaledRadialDis;
    body.chosenY = 1;
  } else if(stringY == "Circumferential Coordinate") {
    body.y = rootData.azimuthal;
    body.chosenY = 2;
  } else if(stringY == "Longitudinal Coordinate") {
    body.y = rootData.arclengths;
    body.chosenY = 3;
  }
}

// big procedure to generate the heatmap and label the cells
void RootCellProcessing::geometryBasedLabelling()
{
  // Function to label cell data based on steepest ascent on fuzzy histogram of
  // cell morphology data, matched to a learning set. The input plot_out gives
  // you the option to see the segmentation.

  // check where is the start and where the end of the root
  correctDirections();

  labelDataMap cellLabelMain, cellLabelRoot;

  if(rootData.hasRadicle){
  // embryo OR root cap

    radicle.heatMap = generateCellMorphologyLandscape(radicle);
    radicle.maximaHeatMapAll = findMaxima(radicle.heatMap);
    radicle.high = findHighMax(radicle);
    cellLabelRoot = clusterCells(radicle);

  }
  // main body for all root types
  mainBody.heatMap = generateCellMorphologyLandscape(mainBody);
  mainBody.maximaHeatMapAll = findMaxima(mainBody.heatMap);
  mainBody.high = findHighMax(mainBody);
  cellLabelMain = clusterCells(mainBody);

  
  // now fill the data structure to save all labels
  for(int i = 0; i<rootData.numCells; i++){
    int currentLabel = rootData.uniqueLabels[i];
    if(mainBody.cells[currentLabel] && !mainBody.preselect[currentLabel]){
      cellLabel[currentLabel] = cellLabelMain[currentLabel]+1;
    } else if(radicle.cells[currentLabel] && !radicle.preselect[currentLabel]){
      cellLabel[currentLabel] = cellLabelRoot[currentLabel]+1;
    } else{
    }
  }

}

// reset preselection
void RootCellProcessing::resetHeat(heatMapDataStructure& body)
{
  for(int i = 0; i<rootData.numCells; i++){
    int currentLabel = rootData.uniqueLabels[i];
    if(body.cells[currentLabel]){
      body.preselect[currentLabel] = false;
    }
  }

}

// take all currently selected clusters, look for nearest maximum and handle them as preselected and remove them from the heatmap
void RootCellProcessing::preselectCells(heatMapDataStructure& body)
{

  maximaVector maxPre = body.maximaHeatMapSelected;
  IntIntMap maxPreLabel = body.maximaHeatMapSelectedLabel;

  cellMorphLandscape landsc = body.heatMap;
  labelBoolMap rootArea = body.cells;
  labelDataMap x = body.x;
  labelDataMap y = body.y;
  maximaVector maxima = body.maximaHeatMapAll;
  //maximaVector customMax = body.maximaHeatMapSelected;
  //IntIntMap labels = body.maximaHeatMapSelectedLabel;

  // relate each cell to the nearest maximum
  labelDataMap nearestMaximum = relateCellstoMaxima(body, landsc, x, y, maxima);

  // find the nearest maxima to the selected postions

  int numMaxPre = maxPre.size();
  int numMax = maxima.size();

  IntIntMap preToMax;

  for(int i = 0; i<numMaxPre; i++){
    double minDis = 1E20;
    int idxRef = 0;
    for(int j = 0; j<numMax; j++){
      double distance = norm(maxPre[i]-maxima[j]);
      if(minDis > distance){
        minDis = distance;
        idxRef = j;
      }
    }
    preToMax[i] = idxRef;
  }

  for(int i = 0; i<rootData.numCells; i++){
    int currentLabel = rootData.uniqueLabels[i];
    if(body.cells[currentLabel]){
      for(int j = 0; j<preToMax.size(); j++){
        if(nearestMaximum[currentLabel] == preToMax[j]){
          cellLabel[currentLabel] = maxPreLabel[j]+1;
          body.preselect[currentLabel] = true;
        }
      }
    }
  }

  maximaVector newMax;
  body.maximaHeatMapSelected = newMax;


}

// label columella cells
// cells with small wall area to neighbours become root cap
// remaining cells: small become vasc, big col
void RootCellProcessing::reassignColumella(int labelRootCap, int labelCol)//, int labelVasc)
{
  intToVec cellConnections;// = rcp.rootData.cellConnections;
  intToVec cellWallAreaPaired;// = rcp.rootData.cellWallAreas;

  // create a connection list
  forall(const Vec2iFloatPair &pr, rootData.wallArea){
    //if(pr.first.x() < pr.first.y()){
    //if(pr.second > threshWallArea && rcp.rootData.cellVolumes[pr.first.x()] > threshVol && rcp.rootData.cellVolumes[pr.first.y()] > threshVol){
      cellConnections[pr.first.x()].push_back(pr.first.y());
      cellWallAreaPaired[pr.first.x()].push_back(pr.second);
    //}
    //}
  }


  double maxVol = 0;
  //double avgVolVasc = 0, vascCount = 0;
  

  // calc max volume of the columella cells
  for(int i = 0; i<rootData.numCells; i++){
    int currentLabel = rootData.uniqueLabels[i];
    if(columella.cells[currentLabel] && rootData.cellVolumes[currentLabel] > maxVol)
      maxVol = rootData.cellVolumes[currentLabel];
    //if(cellLabel[currentLabel] == labelVasc){
    //  vascCount++;
    //  avgVolVasc+=rootData.cellVolumes[currentLabel];
    //}
  }
  //avgVolVasc/=vascCount;
//cout << "avg  " << avgVolVasc << endl;

  labelDataMap wallShared;
  labelDataMap ratio;

  for(int i = 0; i<rootData.numCells; i++){
    int currentLabel = rootData.uniqueLabels[i];
    if(columella.cells[currentLabel]){
      for(int j = 0; j<cellConnections[currentLabel].size(); j++){
        wallShared[currentLabel]+=cellWallAreaPaired[currentLabel][j];
      }

      ratio[currentLabel] = wallShared[currentLabel] / rootData.cellWallArea[currentLabel];

    }
  }

  // find root cap cells
  labelBoolMap rootCapCells;
  double rat_val = 0.4;
  double sca_val = 0.48;

  // find vasculature cells
  labelBoolMap vasc;
  //double volThreshold = 0.2;

  // all other cells
  labelBoolMap nonRootCap;

  for(int i = 0; i<rootData.numCells; i++){
    int currentLabel = rootData.uniqueLabels[i];
    if(columella.cells[currentLabel]){
      if(ratio[currentLabel] < rat_val && rootData.scaledRadialDis[currentLabel] > sca_val){
      // if small wallarea to neighbours and radial distance big enough -> root cap
        rootCapCells[currentLabel] = true;
      //} else if(rootData.cellVolumes[currentLabel]/maxVol < volThreshold){
      // if volume too small -> vasc
      // kicked out on request
//cout << "vol  " << currentLabel << "  " << rootData.cellVolumes[currentLabel] << "   "  << maxVol << endl;
      //  vasc[currentLabel] = true;
      } else {//if(!vasc[currentLabel] && !rootCapCells[currentLabel]){
        nonRootCap[currentLabel] = true;
      }
      // "last ditch" in the matlab files
      if(columella.cells[currentLabel] && nonRootCap[currentLabel] && rootData.scaledRadialDis[currentLabel] > 0.6){
        rootCapCells[currentLabel] = true;
        nonRootCap[currentLabel] = false;
      }

      if(rootCapCells[currentLabel]){
        cellLabel[currentLabel] = labelRootCap;
      //} else if(vasc[currentLabel]){
      //  cellLabel[currentLabel] = labelVasc;
      } else {
        cellLabel[currentLabel] = labelCol;
      }

    }
  }

}

// assign cortical cells subroutine to create the correctedlengths and to dffind the min and max of the arclengths
void RootCellProcessing::assignCortCellsGetMinMax(int labelCort, double& minS, double& maxS)
{

  int sizeBez = rootData.bez.size();
  double lengthBez = 0;

  for(int i = 1; i<sizeBez; i++){
    lengthBez+=norm(rootData.bez[i]-rootData.bez[i-1]);
  }

  labelPosMap longDir;

  labelDataMap correctedLengths;

  minS = 1E20;
  maxS = -1E20;

  for(int i = 0; i<rootData.numCells; i++){
    int currentLabel = rootData.uniqueLabels[i];
    if(cellLabel[currentLabel] == labelCort){
      Point3f bezInterpPoint = rootData.diffBezInterp[currentLabel];
      bezInterpPoint = -bezInterpPoint/norm(bezInterpPoint);
      correctedLengths[currentLabel] = -rootData.lengthLong[currentLabel]*(rootData.dirLong[currentLabel]*bezInterpPoint);

      if(minS > rootData.arclengths[currentLabel]) minS = rootData.arclengths[currentLabel];
      if(maxS < rootData.arclengths[currentLabel]) maxS = rootData.arclengths[currentLabel];

    }
  }

  rootData.correctedLengths = correctedLengths;
}

// assign the cortical cell number to each cell in the root
void RootCellProcessing::assignCortCells(int labelCort, std::vector<double>& sSmooth, bezierMap& bMapS, int length)
{

  // now spline fit x = arclengths / y = correctedLengths of labelCort cells
  int cortCells = 0;
  for(int i = 0; i<rootData.numCells; i++){
    int currentLabel = rootData.uniqueLabels[i];
    if(cellLabel[currentLabel] == labelCort){
      cortCells++;
    }
  }  
  //cout << "spline" << endl;
  double x[cortCells], y[cortCells];
  cortCells = 0;
  for(int i = 0; i<rootData.numCells; i++){
    int currentLabel = rootData.uniqueLabels[i];
    if(cellLabel[currentLabel] == labelCort){
      x[cortCells] = rootData.arclengths[currentLabel];
      y[cortCells] = rootData.correctedLengths[currentLabel];
      cortCells++;
    }
  }  

  double eps = 0.000001;

  // sort the x values from lowest to highest (bubble sort implementation)
  // x vector has to be increasing for the gsl spline
  for(int i = 0; i<cortCells; i++){
    for(int j = 0; j<cortCells; j++){
      if(i>j && x[i] < x[j]){
        double a = x[i];
        x[i] = x[j];
        x[j] = a;
        a = y[i];
        y[i] = y[j];
        y[j] = a;
      }
      // equal x values have to be eliminated // not sure if still necessary
      if(x[i] == x[j] && i!=j){
        if(i<j) x[i]-=eps;
        if(i>j) x[j]-=eps;
      }
    }
  }


  const size_t ncoeffs = 12; // number of fit coef
  const size_t nbreak = 10; // ncoeff-2
  gsl_bspline_workspace *bw;
  gsl_vector *B;
  gsl_vector *c, *w;
  gsl_vector *xGsl, *yGsl;
  gsl_matrix *X, *cov;
  gsl_multifit_linear_workspace *mw;
  double chisq;

  // allocate cubic bspline workspace (k = 4)
  bw = gsl_bspline_alloc(4, nbreak);
  B = gsl_vector_alloc(ncoeffs);

  xGsl = gsl_vector_alloc(cortCells);
  yGsl = gsl_vector_alloc(cortCells);
  X = gsl_matrix_alloc(cortCells, ncoeffs);
  c = gsl_vector_alloc(ncoeffs);
  w = gsl_vector_alloc(cortCells);
  cov = gsl_matrix_alloc(ncoeffs, ncoeffs);
  mw = gsl_multifit_linear_alloc(cortCells, ncoeffs);

  // this is the data to be fitted
  for (int i = 0; i < cortCells; i++)
    {
      gsl_vector_set(xGsl, i, x[i]);
      gsl_vector_set(yGsl, i, y[i]);
      gsl_vector_set(w, i, 1.0);
    }

  // uniform breakpoints
  gsl_bspline_knots_uniform(x[0], x[cortCells-1], bw);

  // construct the fit matrix X
  for (int i = 0; i < cortCells; i++)
    {
      double xi = gsl_vector_get(xGsl, i);

      // compute B_j(xi) for all j
      gsl_bspline_eval(xi, B, bw);

      // fill in row i of X
      for (int j = 0; j < ncoeffs; j++)
        {
          double Bj = gsl_vector_get(B, j);
          gsl_matrix_set(X, i, j, Bj);
        }
    }

  // do the fit
  gsl_multifit_wlinear(X, w, yGsl, c, cov, &chisq, mw);

  double totalLength = 0;

  std::vector<double> sSmooth2;

  for(int i = 1; i<length; i++){
    totalLength+=norm(bMapS[i]-bMapS[i-1]);
  }

  double divider = sSmooth[length-1]-sSmooth[0];
  if(divider<0) divider*= -1.0;

  double sMin = 1E20;
  double sMax = -1E20;

  for(int i = 0; i<length; i++){
    sSmooth2.push_back(sSmooth[i]*totalLength/divider);
    if(sSmooth2[i]<sMin) sMin = sSmooth2[i];
    if(sSmooth2[i]>sMax) sMax = sSmooth2[i];
  } 

  double sTest = sMin;
  int round = 0;
  std::vector<double> sBin;

  // go through whole root and create bins of the size of the average cortical cell size
  while(sTest <= sMax){
    double splineVal, yerr;
    gsl_bspline_eval(sTest*divider/totalLength, B, bw);
    gsl_multifit_linear_est(B, c, cov, &splineVal, &yerr);
    sTest+=splineVal;
    sBin.push_back(sTest);
    round++;
  }

  // assign the right bin to each cell
  for(int k = 0; k<rootData.numCells; k++){
    int currentLabel = rootData.uniqueLabels[k];
    double currentCompare = rootData.arclengths[currentLabel]*totalLength/divider;
    for(int j = 2; j<round; j++){
      if(currentCompare<sBin[j] && currentCompare>=sBin[j-1]){
        rootData.associatedCorticalCell[currentLabel] = j-1;
      }
    }
    if(currentCompare>=sBin[round-1]){
      rootData.associatedCorticalCell[currentLabel] = round-1;
    }
    if(currentCompare<sBin[0]){
      rootData.associatedCorticalCell[currentLabel] = 0;
    }

  }
  gsl_bspline_free(bw);
  gsl_vector_free(B);
  gsl_vector_free(xGsl);
  gsl_vector_free(yGsl);
  gsl_matrix_free(X);
  gsl_vector_free(c);
  gsl_vector_free(w);
  gsl_matrix_free(cov);
  gsl_multifit_linear_free(mw);

}
}
}
