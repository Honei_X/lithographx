/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef CELL_ATLAS_HPP
#define CELL_ATLAS_HPP

#include <Process.hpp>
#include "RootCellAnalyzing.hpp"
#include "RootCellProcessing.hpp"
#include "DataAnalyzer.hpp"
#include "ui_CellAtlasGUI.h"


namespace mgx { 
namespace process {

  using util::Vector;

  static RootCellProcessing rcp;
  static Ui_CellAtlasDialog ui;
  static QDialog* dlg;
  static bool analyzed=false, loaded=false;

  class AnalyzeCells : public MeshProcess
  {
    public:
      AnalyzeCells(const MeshProcess& process) : Process(process), MeshProcess(process) { }

      bool operator()(const QStringList& parms)
      {
        const Stack *s1 = stack(0);
        const Stack *s2 = stack(1);
        Mesh *m1 = mesh(0);
        Mesh *m2 = mesh(1);
	return (*this)(s1, s2, m1, m2, rcp, parms[0].toDouble());
      }
      bool operator()(const Stack *s1, const Stack *s2, Mesh *m1, Mesh *m2, RootCellProcessing& rcp, double minVolume);

      QString name() const { return "A - Analyze Cells 3D"; }
      QString folder() const { return "Cell Atlas 3D"; }
      QString description() const { return "Analyze Cells 3D\n"
        "\n"
        "Analyze the 3D properties of the cells according to the Bezier-coordinate system.\n"
        "\n"
        "This process is needed for most of the other processes as it provides the raw data.\n"
        "\n"
        "Requirements:\n"
        " - Bezier line through the root body\n"
        " - A selected first cell for defining the orientation"; }
      QStringList parmNames() const { return QStringList() << "Volume Threshold"; }
      QStringList parmDefaults() const { return QStringList() << "0"; }

      //QList<float> default_values() const { return QList<float>() << 0.0; }
      
      QIcon icon() const { return QIcon(":/images/CellGraph3D.png"); }

  };

  class SelectBadCells : public MeshProcess
  {
    public:
      SelectBadCells(const MeshProcess& process) : Process(process), MeshProcess(process) { }

      bool operator()(const QStringList& parms)
      {
        Mesh *m1 = mesh(0);
	return (*this)(m1);
      }
      bool operator()(Mesh *m1);

      QStringList parmNames() const { return QStringList(); }
      QStringList parmDefaults() const { return QStringList(); }
      QString name() const { return "Select Bad Cells"; }
      QString folder() const { return "Cell Atlas 3D/Tools"; }
      QString description() const { return "Select Bad Cells\n"
        "\n"
        "Select cells that were labelled as bad by the Analyze Cells 3D process."
        "\n"
        "Bad cells are:\n"
        " - Cells that have a small value (see threshold Anaylze Cells 3D)\n"
        " - Cells that have their centroid outside of the cell body"; }
      QIcon icon() const { return QIcon(":/images/CellAtlas.png"); }

  };

class SaveCellData : public MeshProcess
  {
    public:
      SaveCellData(const MeshProcess& process) : Process(process), MeshProcess(process) { }

      bool initialize(QStringList& parms, QWidget* parent);

      bool operator()(const QStringList& parms)
      {
	return (*this)(parms[0]);
      }
      bool operator()(QString filename);

      QStringList parmNames() const { return QStringList() << "Filename"; }
      QStringList parmDefaults() const { return QStringList() << ""; }
      QString name() const { return "Save Cell Data"; }
      QString folder() const { return "Cell Atlas 3D/Tools"; }
      QString description() const { return "Save Cell Data\n"
        "\n"
        "Save current data to csv-files"; }
     
      QIcon icon() const { return QIcon(":/images/CellAtlasSave.png"); }

  };

class DisplayCellData : public MeshProcess
  {
    public:
      DisplayCellData(const MeshProcess& process) : Process(process), MeshProcess(process) { }

      bool operator()(const QStringList& parms)
      {
	return (*this)(parms[0]);
      }
      bool operator()(QString choice);

      QStringList parmNames() const { return QStringList() << "Data"; }

      QStringList parmDefaults() const { return QStringList() << "Arclengths"; }

      ParmChoiceMap parmChoice() const
      {
        ParmChoiceMap map;
        map[0] = QStringList() << "Associated Cortical Cell" << "Arclengths" << "Radial" << "Longitudinal Cell Length" << "Radial Cell Length" << "Circumferential Cell Length" << "Volume" << "Circumferential Angle" << "Cell Wall Area";
        return map;
      }

      QString name() const { return "Display Cell Data"; }
      QString folder() const { return "Cell Atlas 3D/Tools"; }
      QString description() const { return "Display Cell Data\n"
        "\n"
        "Displays cell properties as a heatmap.\n"
        "\n"
        "Requires analyzed or loaded data in the memory."; }
     
      QIcon icon() const { return QIcon(":/images/CellAtlas.png"); }

  };

class LoadCellData : public MeshProcess
  {
    public:
      LoadCellData(const MeshProcess& process) : Process(process), MeshProcess(process) { }

      bool initialize(QStringList& parms, QWidget *parent);

      bool operator()(const QStringList& parms)
      {
	return (*this)(parms[0]);
      }
      bool operator()(QString filename);

      QStringList parmNames() const { return QStringList() << "Filename"; }
      QStringList parmDefaults() const { return QStringList() << ""; }
      QString name() const { return "Load Cell Data"; }
      QString folder() const { return "Cell Atlas 3D/Tools"; }
      QString description() const { return "Load Cell Data\n"\
        "\n"
        "Loads data from an analyzed root from csv files"; }
     
      QIcon icon() const { return QIcon(":/images/CellAtlasLoad.png"); }

  };


class TopologicalCheck : public MeshProcess
  {
    public:
      TopologicalCheck(const MeshProcess& process) : Process(process), MeshProcess(process) { }

      bool operator()(const QStringList& parms){
	return (*this)(parms[0], parms[1].toDouble(), parms[2].toDouble(), parms[3].toInt(), parms[4].toInt(), parms[5].toInt(), parms[6].toInt());
      }
      bool operator()(QString selection, double threshVol, double threshWallArea, int rootCapLabel, int airBubbleLabel, int vascLabel, int errors);

      QStringList parmNames() const { return QStringList() /*<< "Has Radicle" << "Tolerance" */<< "Work on Selection" << "Threshold Volume" << "Threshold Wall Area" << "Root Cap Label" << "Air Bubble Label" << "Vasculature Label" << "Error Limit"; }

      QStringList parmDefaults() const { return QStringList() << "No" << "10" << "1" << "7" << "6" << "5" << "2"; }
      //QList<float> default_values() const { return QList<float>() /*<< 0 << .0001 */<< 10 << 1 << 7 << 6 << 5 << 2; }

      ParmChoiceMap parmChoice() const
      {
        ParmChoiceMap map;
        map[0] = booleanChoice();
        return map;
      }



      QString name() const { return "E - Topological Check"; }
      QString folder() const { return "Cell Atlas 3D"; }
      QString description() const { return "Topological Check\n"
        "\n"
        "Selects potentially mislabelled cells\n"
        "\n"
        "This process checks the neighbourhood relation of the parent labels of the cells.\n"
        "Every parent label cell layer only allows connections to two other parent labels (the layer above and below).\n"
        "\n"
        "Exceptions are the columella parent label (=10) and the airbubble parent label which can be specified as a parameter.\n"
        "Those two cell types are ignored in the neighbourhood graph.\n"
        "\n"
        "Further exceptions are the rootcap and the vasculature tissue.\n"
        "Those two cell types allow connections only to one further cell type and also have to be specified as parameters.\n "
        "\n"
        "The process works either on the whole root or on a selection.\n"
        "Potentially mislabelled cells will be selected according to the Error Count parameter"; }
     
      QIcon icon() const { return QIcon(":/images/SubdivideTriAdapt.png"); }

  };

  class CellAtlas : public QObject, public MeshProcess
  {
    Q_OBJECT
    public:
      CellAtlas(const MeshProcess& process) : Process(process), QObject(), MeshProcess(process) { }

      virtual ~CellAtlas() { }

      bool initialize(QStringList& strings, QWidget *parent);

      bool operator()(const QStringList& parms){
	 return (*this)(parms[0]);
      }
      bool operator()(QString rootCap);

      QString name() const { return "B - Assign Cell Types"; }
      QString folder() const { return "Cell Atlas 3D"; }
      QString description() const { return "Assign Cell Types\n"
        "\n"
        "View the cell property heat map and assign parent labels to the different cell types.\n"
        "\n"
        "Requires a selected first cell and a last root cap cell (if required). Also requires root cell data in the memory (either loaded or from the plugin Analyze Cells 3D\n"
        "\n"
        "GUI options:\n"
        " - Mouseclick: creates a new cluster if no existing one is nearby\n"
        " - Drag and Drop on cluster: moves the cluster\n"
        " - Double Click on cluster: deletes the cluster"; }

      ParmChoiceMap parmChoice() const
      {
        ParmChoiceMap map;
        map[0] = booleanChoice();
        return map;
      }


      QStringList parmNames() const { return QStringList() << "Has multiple segments";}

      QStringList parmDefaults() const 
      {
        return QStringList() << "Yes";
      }

      //QList<float> default_values() const { return QList<float>() << 1; }

      QIcon icon() const { return QIcon(":/images/Cluster.png"); }


      protected slots:

      void setImage();
      void setDeletePosition(const QPoint& p);
      void setPosition(const QPoint& p);
      void setReleasePosition(const QPoint& p);
      void setAutoCluster();
      void changeHeatmapX(QString stringX);
      void changeHeatmap(QString stringY);
      void setClusterLabel(QString label);
      void setMousePosition(const QPoint& p);
      void changeSigma(double sigma);
      void changeRootPart();
      void setPreCluster();
      void resetCluster();

      protected:
      QDialog *dlg;
      Vec2d mousePos;

  };

class CollapseBezier : public MeshProcess
  {
    public:
      CollapseBezier(const MeshProcess& process) : Process(process), MeshProcess(process) { }

      bool operator()(const QStringList& parms){
	 return (*this)();
      }
      bool operator()();

      QString name() const { return "Collapse Bezier Points"; }
      QString folder() const { return "Cell Atlas 3D/Tools"; }
      QString description() const { return "Collapse Bezier Points\n"
        "\n"
        "Collapse the 2D Bezier gridpoints into a line (needed for Analyze Cells 3D)"; }
      QStringList parmNames() const { return QStringList();}
    QStringList parmDefaults() const { return QStringList(); }

      QIcon icon() const { return QIcon(":/images/Bezier.png"); }
  };

class AssignColumella : public MeshProcess
  {
    public:
      AssignColumella(const MeshProcess& process) : Process(process), MeshProcess(process) { }

      bool operator()(const QStringList& parms){
	 return (*this)(parms[0].toInt(), parms[1].toInt());//, values[2]);
      }
      bool operator()(int labelRootCap, int labelCol);//, int labelVasc);

      QString name() const { return "C - Assign Columella"; }
      QString folder() const { return "Cell Atlas 3D"; }
      QString description() const { return "Assign Columella\n"
        "\n"
        "Assign the Columella Cells according to the given labels"; }
      QStringList parmNames() const { return QStringList() << "Root Cap Label" << "Columella Label";}// << "Vasculature Label";}
      QStringList parmDefaults() const { return QStringList() << "7" << "10"; }
      //QList<float> default_values() const { return QList<float>() << 7 << 10;}// << 5; }

      QIcon icon() const { return QIcon(":/images/CellAtlasCol.png"); }
  };

class AssignCorticalCells : public MeshProcess
  {
    public:
      AssignCorticalCells(const MeshProcess& process) : Process(process), MeshProcess(process) { }

      bool operator()(const QStringList& parms){
	 return (*this)(parms[0].toInt());
      }
      bool operator()(int labelCort);

      QString name() const { return "D - Assign Cortical Cells"; }
      QString folder() const { return "Cell Atlas 3D"; }
      QString description() const { return "Assign Cortical Cells\n"
        "\n"
        "Assign the associated cortical cells to all cells\n"
        "\n"
        "Requires Analyze Cells 3D, loading files is not enough!"; }
      QStringList parmNames() const { return QStringList() << "Cortical Cell Label";}
      QStringList parmDefaults() const { return QStringList() << "2"; }
      //QList<float> default_values() const { return QList<float>() << 2; }

      QIcon icon() const { return QIcon(":/images/CellAtlasCort.png"); }
  };

class DataAnalysis : public MeshProcess
  {
    public:
      DataAnalysis(const MeshProcess& process) : Process(process), MeshProcess(process) { }

      bool initialize(QStringList& parms, QWidget* parent);

      bool operator()(const QStringList& parms){
	 return (*this)(parms[0], parms[1], parms[2], parms[3], parms[4], parms[5].toDouble());
      }
      bool operator()(QString folderControl, QString folderTreatment,  QString folderOutput, QString outputFileType, QString avg, double window);

      QString name() const { return "Cell growth analysis 3D"; }
      QString folder() const { return "Cell Atlas 3D/Statistics"; }
      QString description() const { return "3D cell growth analysis\n"
        "\n"
        "Data Analysis tool.\n"
        "\n"
        "Requires input data from two folders: control and treatment.\n"
        "\n"
        "Merges the output with the chosen files"; }
      QStringList parmNames() const { return QStringList() 
        << "Control folder" << "Treatment folder" << "Output folder" << "Output Type" << "Sliding Avg" << "Window";}
      QStringList parmDefaults() const 
      {
        return QStringList() << "" << "" << "" << "Treatment" << "No" << "3";
      }
      //QList<float> default_values() const { return QList<float>() << 3; }

      ParmChoiceMap parmChoice() const
      {
        ParmChoiceMap map;
        map[3] = QStringList() << "Treatment" << "Control";
        map[4] = booleanChoice();
        map[5] = booleanChoice();
        return map;
      }

      QIcon icon() const { return QIcon(":/images/CellAtlas.png"); }
  };

class DataAnalysisGUS : public MeshProcess
  {
    public:
      DataAnalysisGUS(const MeshProcess& process) : Process(process), MeshProcess(process) { }

      bool initialize(QStringList& parms, QWidget* parent);

      bool operator()(const QStringList& parms){
        Mesh *m1 = mesh(0);
	 return (*this)(m1, parms[0], parms[1], parms[2], parms[3], parms[4], parms[5].toDouble(), parms[6], parms[7].toDouble(), parms[8],  parms[9].toDouble() /*,parms[10].toDouble()*/);
      }
      bool operator()(Mesh *m1, QString folderInput, QString gusFileExt,/* QString folderOutput, */QString mergeWithFile, QString fileToMerge, QString avg, double window, QString upperFilter, double upperFilterValue, QString lowerFilter, double lowerFilterValue);

      QString name() const { return "Reporter abundance analysis 3D"; }
      QString folder() const { return "Cell Atlas 3D/Statistics"; }
      QString description() const { return "3D reporter abundance analysis\n"
        "\n"
        "Data Analysis tool.\n"
        "\n"
        "Requires input data a folder. Note that input files come in pairs:\n"
        "One file for the cell data and another for the reporter data. Files have to named in the following way for the plugin to find the file pairs:\n"
        "cell data: filename.csv\n"
        "reporter data: filename''reporter file string''.csv, where ''reporter file string'' can be spcified."
        "\n"
        "Output files will be saved in the input folder"; }
      QStringList parmNames() const { return QStringList() << "Input folder" << "Reporter file string" << /*"Output folder" <<*/ "Merge With File" << "Output File" << "Sliding Avg" << "Sliding Avg Window" << "Upper Filter Type" << "Upper Filter Limit" << "Lower Filter Type"  << "Lower Filter Limit";}
      QStringList parmDefaults() const 
      {
        return QStringList() 
          << "" << "_reporter" << /*"" <<*/ "Yes" << "" << "No" << "3" << "No Filter" << "100" << "No Filter" << "0";
      }
      //QList<float> default_values() const { return QList<float>() << 3 << 100 << 100; }

      ParmChoiceMap parmChoice() const
      {
        ParmChoiceMap map;
        map[2] = booleanChoice();
        map[4] = booleanChoice();
        map[6] = QStringList() << "No Filter" << "Percentage" << "Value";
        map[8] = QStringList() << "No Filter" << "Percentage" << "Value";
        return map;
      }

      QIcon icon() const { return QIcon(":/images/CellAtlas.png"); }
  };

class ExamineBadVasculature : public MeshProcess
  {
    public:
      ExamineBadVasculature(const MeshProcess& process) : Process(process), MeshProcess(process) { }

      bool operator()(const QStringList& parms){
	 return (*this)(parms[0].toInt(), parms[1].toInt(), parms[2].toInt(), parms[3].toInt());
      }
      bool operator()(int labelVasc, int labelAirBubble, int labelEndo, int methodAvg);

      QString name() const { return "F - Examine Vasculature"; }
      QString folder() const { return "Cell Atlas 3D"; }
      QString description() const { return "Examine Vasculature\n"
        "\n"
        "Examine all cells that are selected as wrong (use Topological check) and marked as vasculature\n"
        "in order to split them into air bubble and endoderm\n"
        "\n"
        "Requires selected cells from Topological Check in order to work. Hereby note the Topological Check parameters:\n"
        "If a cell/air bubble is not selected it can't be fixed with this plugin! (It might be necessary to lower the thresholds there)\n"
        "\n"
        "The matlab function didn't work very well (creating the function with Gaussians, looking for min to find a separation point\n"
        "between air bubbles and endoderm), so another function was implemented which bases the separation point on the cell lengths\n"
        "of correct endoderm cells (the Average Separation parameter 1 choses this, 0 will use the matlab function)"; }
      QStringList parmNames() const { return QStringList() << "Vasculature Label" << "Air Bubble Label" << "Endo Label" << "Average Separation";}
      QStringList parmDefaults() const { return QStringList() << "5" << "6" << "4" << "1"; }
      //QList<float> default_values() const { return QList<float>() << 5 << 6 << 4 << 1; }

      QIcon icon() const { return QIcon(":/images/CellAtlasVasc.png"); }
  };
}
}

#endif

