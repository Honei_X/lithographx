/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef DataAnalyzer_H
#define DataAnalyzer_H

#include <MGXCompat.hpp>
#include <math.h>
#include <Progress.hpp>

#include <Geometry.hpp>

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <string.h>
#include <fstream>
#include <dirent.h>

using namespace std;

namespace mgx {

namespace process {

using mgx::util::Vector;

struct SortResultData
{
  int cellType;
  int asocCortCell;
  double ratio, upper, lower;

  int measure;

};


struct FileDataType
{

  int cellNumber;
  int cellType;
  int asocCortCell;
  double radial;
  double lenLong, lenRad, lenCirc;
  double wallArea, cellVol;

  double GUS;
  double GUSUpper;
  double GUSLower;

  QString file;

};

struct FileEFactorDataType
{

  int cellType;
  int asocCortCell;
  double facLong, facRad, facCirc;

};

typedef std::vector<SortResultData> SortResultVector;
typedef std::vector<SortResultVector> SortResultVectorAll;
typedef std::vector<FileDataType> FileDataVector;
typedef std::vector<FileEFactorDataType> FileEFactorDataVector;
typedef Vector<2, int> Vec2i;
typedef std::vector<Vec2i> tagTypeVector;
typedef std::unordered_map<int, FileDataVector> tagDataMap;
typedef std::vector<QString> FilenameVector;
typedef std::map<int, int> IntIntMap;
typedef std::map<int, double> IntDoubleMap;
typedef std::map<int, Point3f> IntPoint3fMap;

class DataAnalyzer
{

public:
  // global variables
  // X is always treatment and Y control

  // possible tags (cell type, assoc cort cell)
  tagTypeVector tagTypesX, tagTypesY;

  // maps tags (celltype*100 + acc) to the data from the file data = [cell#, type, acc, [data columns], file]
  tagDataMap dataX, dataY;

  // vector with all filenames
  FilenameVector filenamesX, filenamesY;

  // data structure for the efactors
  FileEFactorDataVector efData;

  // data structure for the sorted results
  SortResultVectorAll resVecAll;

  bool saveEFactors(FileEFactorDataVector eFData, tagDataMap data, tagTypeVector tagTypes, FilenameVector filenames, QString folderOut);

  bool calcEFactors(SortResultVectorAll sortResVec, bool avg, int window, tagTypeVector tagTypes, FileEFactorDataVector& eFData);

  bool logRatio(double& ratio, double& upper, double& lower);

  bool sortData(tagDataMap data1, tagDataMap data2, tagTypeVector tagTypes);

  bool logRatio(double& ratio, double& upper, double& lower, std::vector<double> dataX, std::vector<double> dataY);

  bool analyze(QString folderX, QString folderY, QString folderOut, bool treatment, bool avg, int window);

  bool readCellData(QString folder, tagDataMap& tagData, tagTypeVector& tagTypes, FilenameVector& filenames);

  double t005(double df);

  bool smoothAnalyzed(SortResultVectorAll& sortResVec, double window, tagTypeVector tagTypes);

  bool joinData(SortResultVectorAll sortResVec, tagDataMap data, tagTypeVector tagTypes, FilenameVector& filenames, QString folderOut);

  double tIntervalB(double t, std::vector<double> x, std::vector<double> y);

  bool analyzeGUS(QString folderInput, /*QString folderOut, */QString gusFileExt, bool mergeBool, QString fileToMerge, bool avg, int window, QString upperFilter, double upperFilterValue, QString lowerFilter, double lowerFilterValue, IntDoubleMap& GUSInfo);

private:

};
}
}

#endif

