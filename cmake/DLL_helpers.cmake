set(DLL_HELPER_SCRIPT ${CMAKE_CURRENT_LIST_DIR}/build_cmake_install.py)
function(DLL_DEPENDENCIES output_cmakefile destination inout search_paths)
    if(NOT PYTHONINTERP_FOUND)
        message(FATAL "Cannot find DLL dependencies without a valid Python interpreter")
    endif()
    if("${inout}" STREQUAL "INCLUDE")
        set(dllopt "dllpath")
    elseif("${inout}" STREQUAL "EXCLUDE")
        set(dllopt "exclude-dllpath")
    else()
        message(FATAL "Error, third argument to DLL_DEPENDENCIES macro must be INCLUDE or EXCLUDE")
    endif()
    #message(STATUS "DLL_DEPENDENCIES of ${ARGN}")
    find_file(MSVC_DUMPBIN dumpbin.exe)
    foreach(pth ${search_paths})
        list(APPEND dll_paths "--${dllopt}=${pth}")
    endforeach()
    set(targets ${ARGN})
    foreach(target ${targets})
        list(APPEND target_paths "--target=$<TARGET_FILE:${target}>")
    endforeach()
    add_custom_command(OUTPUT ${output_cmakefile}
        COMMAND ${PYTHON_EXECUTABLE} "${DLL_HELPER_SCRIPT}"
            --output=${CMAKE_CURRENT_BINARY_DIR}/${output_cmakefile}
            --dumpbin="${MSVC_DUMPBIN}"
            --destination="${destination}"
            ${target_paths} ${dll_paths}
        DEPENDS ${targets} "${DLL_HELPER_SCRIPT}")
    add_custom_target(generate_${output_cmakefile} ALL
        DEPENDS ${output_cmakefile})
    install(SCRIPT ${CMAKE_CURRENT_BINARY_DIR}/${output_cmakefile})
endfunction(DLL_DEPENDENCIES)

macro(FIND_LIB_PATHS output)
    foreach(lib ${ARGN})
        if(TARGET ${lib})
            list(APPEND ${output} "$<TARGET_FILE_DIR:${lib}>")
        else()
            list(APPEND ${output} "${lib}")
        endif()
    endforeach()
    list(REMOVE_DUPLICATES ${output})
endmacro(FIND_LIB_PATHS)
