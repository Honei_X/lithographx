# Finder for thrust library
# thrust is template only, so only includes are searched
# 
# THRUST_INCLUDE_DIR Toplevel include dir containing thrust, includes have to be defined like <thrust/header.h>


if (THRUST_INCLUDE_DIR)
  # Already in cache, be silent
  set(THRUST_FIND_QUIETLY TRUE)
endif (THRUST_INCLUDE_DIR)

option(THRUST_BACKEND_CUDA "Compile for CUDA thrust backend." ON)
option(THRUST_BACKEND_OMP "Compile for OpenMP thrust backend." OFF)
if(THRUST_BACKEND_CUDA)
  find_package(CUDA)
  if(NOT CUDA_FOUND)
    set(THRUST_BACKEND_OMP TRUE)
    set(THRUST_BACKEND_CUDA FALSE)
  endif()
endif()

# Set the backend for thrust
#SET(THRUST_BACKEND_TBB FALSE CACHE BOOL "Compile for TBB thrust backend.")
if(THRUST_BACKEND_TBB AND THRUST_BACKEND_OMP)
  message(SEND_ERROR "Only one of THRUST_BACKEND_TBB and THRUST_BACKEND_OMP can be true.")
elseif(THRUST_BACKEND_TBB)
  list(APPEND CUDA_OPTIONS -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_TBB)
elseif(THRUST_BACKEND_OMP)
  list(APPEND CUDA_OPTIONS -Xcompiler ${OpenMP_CXX_FLAGS} -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_OMP)
endif()
if(THRUST_BACKEND_OMP OR THRUST_BACKEND_TBB)
  set(THRUST_BACKEND_CUDA FALSE)
else()
  set(THRUST_BACKEND_CUDA TRUE)
endif()

find_path( THRUST_INCLUDE_DIR
  HINTS /usr/include /usr/local/include
  $ENV{CUDA_INC_PATH}
  ${CUDA_TOOLKIT_INCLUDE}
  ${CUDA_TOOLKIT_ROOT_DIR}
  NAMES thrust/version.h
  DOC "Lib Thrust Include dir"
  )

# ensure that we only get one
if( THRUST_INCLUDE_DIR )
  list( REMOVE_DUPLICATES THRUST_INCLUDE_DIR )
endif( THRUST_INCLUDE_DIR )

# Check for required components
if ( THRUST_INCLUDE_DIR )

  # Parse version file to get the thrust version numbers

  file( STRINGS ${THRUST_INCLUDE_DIR}/thrust/version.h
    version
    REGEX "#define[ \t]+THRUST_VERSION[ \t]+([0-9x]+)"
    )

  string( REGEX REPLACE "#define[ \t]+THRUST_VERSION[ \t]+" "" version ${version} )
  string( REGEX MATCH "^[0-9]" major ${version} )
  string( REGEX REPLACE "^${major}00" "" version ${version} )
  string( REGEX MATCH "^[0-9]" minor ${version} )
  string( REGEX REPLACE "^${minor}0" "" version ${version} )
  set( THRUST_VERSION "${major}.${minor}.${version}")

  set( THRUST_FOUND TRUE )

  include( FindPackageHandleStandardArgs )
  find_package_handle_standard_args( Thrust
    REQUIRED_VARS THRUST_INCLUDE_DIR
    VERSION_VAR THRUST_VERSION
    )
endif()

