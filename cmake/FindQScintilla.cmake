#
# Try to find the QScintilla library and include path
#
# QSCINTILLA_FOUND
# QSCINTILLA_LIBRARY
# QSCINTILLA_INCLUDE_PATH
#

if(WIN32)
  get_target_property(QtCore_includes Qt5::Core INCLUDE_DIRS)
  find_library(QSCINTILLA_LIBRARY qt5scintilla2.dll)
  find_path(QSCINTILLA_INCLUDE_PATH
            names QSci/qsciglobal.h
            paths "${QtCore_includes}/..")
else(WIN32)
  set(test a b c d)
  find_library(QSCINTILLA_LIBRARY libqt5scintilla2.so)
  message(STATUS "Path: ${Qt5Core_INCLUDE_DIRS}")
  find_path(QSCINTILLA_INCLUDE_PATH
            Qsci/qsciglobal.h
            paths /usr/include
                  /usr/local/include
                  ${Qt5Core_INCLUDE_DIRS})
endif()
