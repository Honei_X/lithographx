# Try to find SIP and define options
# Once done, this will define:
#
# SIP_FOUND
# SIP_INCLUDE_PATH - Path to add to compile generated source files
# SIP_EXECUTABLE   - Full path to the sip executable
# SIP_FLAGS        - Flags used for the sip executable
# SIP_PYQT5_PATH   - Folder containing the PyQt5 sip files
#
# New python modules can be defined with:
#
# add_sip_module(MODULE OUTPUT_FILES SIP_FILES)
#

find_package(PythonInterp REQUIRED)

if(NOT (PYTHON_VERSION_STRING VERSION_EQUAL PYTHONLIBS_VERSION_STRING))
  message(FATAL_ERROR "The python interpreter and libraries must have the same version.")
endif()

if(WIN32)
  find_file(SIP_EXECUTABLE sip
            PATHS ENV PATH
            DOC "sip executable file" CACHE)
  find_path(SIP_PYQT5_CORE_PATH QtCoremod.sip
            PATHS /usr/include/sip
                  /usr/local/include/sip
                  /usr/share/sip
                  ${PYTHON_INCLUDE_PATH}
            PATH_SUFFIXES PyQt5/QtCore)
  get_filename_component(SIP_PYQT5_PATH ${SIP_PYQT5_CORE_PATH}
                         PATH CACHE)
  find_path(SIP_INCLUDE_PATH sip.h
    PATHS ${PYTHON_INCLUDE_PATH}
          /usr/include
          /usr/local/include
    DOC "Directory containing sip.h")
  mark_as_advanced(SIP_PYQT5_CORE_PATH)
else(WIN32)
  find_file(SIP_EXECUTABLE sip
            PATHS ENV PATH
            DOC "sip executable file" CACHE)
  find_path(SIP_PYQT5_CORE_PATH QtCoremod.sip
            PATHS /usr/include/sip
                  /usr/local/include/sip
                  /usr/share/sip
                  ${PYTHON_INCLUDE_PATH}
            PATH_SUFFIXES PyQt5/QtCore)
  get_filename_component(SIP_PYQT5_PATH ${SIP_PYQT5_CORE_PATH}
                         PATH CACHE)
  find_path(SIP_INCLUDE_PATH sip.h
    PATHS ${PYTHON_INCLUDE_PATH}
          /usr/include
          /usr/local/include
    DOC "Directory containing sip.h")
  mark_as_advanced(SIP_PYQT5_CORE_PATH)
endif(WIN32)

if(SIP_EXECUTABLE)
  set(SIP_FOUND TRUE)

  execute_process(COMMAND ${PYTHON_EXECUTABLE} -c "from PyQt5 import Qt; print(Qt.PYQT_CONFIGURATION.get('sip_flags', ''))"
                  OUTPUT_VARIABLE SIP_MIN_FLAGS
                  RESULT_VARIABLE SIP_HAS_PYQT5
                  OUTPUT_STRIP_TRAILING_WHITESPACE)
  if(SIP_HAS_PYQT5 GREATER 0)
    message(SEND_ERROR "Couldn't find PyQt5 as a module for python.")
  endif()

  set(SIP_FLAGS "${SIP_MIN_FLAGS} -I ${SIP_PYQT5_PATH}")

  # add_sip_module requires an output, a single source file and a list of classes in the source file
  function(add_sip_module OUTPUT SOURCE)
    get_filename_component(module_name ${SOURCE} NAME_WE)
    get_filename_component(source ${SOURCE} ABSOLUTE)
    set(base_folder "${CMAKE_CURRENT_BINARY_DIR}/sip${module_name}")
    file(MAKE_DIRECTORY ${base_folder})
    set(cpp_files "${base_folder}/sip${module_name}cmodule.cpp")
    #message(STATUS "cpp_files: ${cpp_files}")
    foreach(class IN ITEMS ${ARGN})
      list(APPEND cpp_files "${base_folder}/sip${module_name}${class}.cpp")
      #message(STATUS "cpp_files: ${cpp_files}")
    endforeach()
    #message(STATUS "cpp_files: ${cpp_files}")
    separate_arguments(sip_flags UNIX_COMMAND "${SIP_FLAGS}")
    #set(sip_qsci_output "${base_folder}/${module_name}.qsci")
    add_custom_command(OUTPUT ${cpp_files}
      COMMAND ${SIP_EXECUTABLE} -e -g ${sip_flags} -c ${base_folder} ${source}
      DEPENDS ${source}
      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
    set(${OUTPUT} ${cpp_files} PARENT_SCOPE)
    #set(${QSCI_OUTPUT} ${qsci_files} PARENT_SCOPE)
  endfunction()
else()
  set(SIP_FOUND FALSE)
endif()

