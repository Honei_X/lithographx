# Include Add-ons in build
# 
# Each Add-on must have its own directory in AddOns/ and have CMakeLists.txt file
# in that directory
# 
macro(SUBDIRLIST result curdir)
  file(GLOB dirs ${curdir}/*)
  set(dirlist "")
  foreach(dir ${dirs})
    if(IS_DIRECTORY ${dir} AND EXISTS ${dir}/CMakeLists.txt)
        LIST(APPEND dirlist ${dir})
    endif()
  endforeach()
  set(${result} ${dirlist})
endmacro()

subdirlist(ADDON_DIRS "${LithoGraphX_SOURCE_DIR}/AddOns")

foreach(ADDON_DIR ${ADDON_DIRS})
  get_filename_component(ADDON ${ADDON_DIR} NAME)
  message(STATUS "Found Add-on:${ADDON} at ${ADDON_DIR}")
  set(BUILD_${ADDON} FALSE CACHE BOOL "If true, add-on ${ADDON} will be built")
  if(BUILD_${ADDON})
    add_subdirectory(${ADDON_DIR})
    list(APPEND LGX_ADDONS ${ADDON})
  endif()
endforeach()

STRING(REPLACE ";" "-" LGX_ADDONS "${LGX_ADDONS}")
