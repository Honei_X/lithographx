from __future__ import print_function
import argparse
from subprocess import check_output, CalledProcessError
import os.path
import sys

DEBUG=False

parser = argparse.ArgumentParser()
parser.add_argument("--dumpbin", default="", type=str, help="Path to dumpbin.exe")
parser.add_argument("--target", action='append', dest="targets", help="File to examine")
parser.add_argument("--dllpath", action='append', default=[], dest="dllpaths", help="Path in which the DLL are found.")
parser.add_argument("--exclude-dllpath", action='append', default=[], dest="nodllpaths", help="Path in which the DLL are *not* found.")
parser.add_argument("--output", help="Output file name")
parser.add_argument("--destination", help="Where the DLLs should be copied")
args = parser.parse_args()

if DEBUG:
    print("Processing of ", args.targets, file=sys.stderr)

if not args.dumpbin:
    args.dumpbin = "dumpbin.exe"

if args.dllpaths and args.nodllpaths:
    print("Error, only one of 'dllpath' and 'exclude-dllpath' can be used", file=sys.stderr)
    sys.exit(1)

if args.dllpaths:
    args.dllpaths = list(set(args.dllpaths))
    if DEBUG:
        print("Including only DLLs from ", args.dllpaths, file=sys.stderr)
elif args.nodllpaths:
    args.nodllpaths = list(set(args.nodllpaths))
    if DEBUG:
        print("Excluding all DLLs from ", args.nodllpaths, file=sys.stderr)

def find_dll(dll_name):
    if args.dllpaths:
        for pth in args.dllpaths:
            full_pth = os.path.join(pth, dll_name)
            if os.path.isfile(full_pth):
                return full_pth
    else:
        try:
            full_pths = [p.strip() for p in check_output(['where', dll_name]).split("\n")]
            for full_pth in full_pths:
                if os.path.isfile(full_pth):
                    if DEBUG:
                        print("  Full path: ", full_pth, file=sys.stderr)
                    low_pth = full_pth.lower()
                    if args.nodllpaths:
                        for pth in args.nodllpaths:
                            if low_pth.startswith(pth.lower()):
                                return
                    return full_pth
                elif DEBUG:
                    print("  Path doesn't exist: '{}'".format(full_pth), file=sys.stderr)
        except CalledProcessError:
            pass

def extract_dlls(target):
    if DEBUG:
        print("Processing", target, file=sys.stderr)
    dlls = []
    try:
        output = check_output([args.dumpbin, '/dependents', target])
    except CalledProcessError:
        print("Warning, cannot examine file: ", target, file=sys.stderr)
        return []
    lines = [l.strip() for l in output.split("\n")]
    begin = False
    for l in lines:
        if begin:
            if l.endswith(".dll"):
                if DEBUG:
                    print("   Found dll:", l, file=sys.stderr)
                real_dll = find_dll(l)
                if real_dll:
                    if DEBUG:
                        print("      -> keep ", real_dll, file=sys.stderr)
                    dlls.append(real_dll)
        elif l == 'Image has the following dependencies:':
            begin = True
        elif l == 'Summary':
            break
    return dlls

dlls = set()
to_check = args.targets
while to_check:
    new_dlls = set()
    for target in to_check:
        new_dlls.update(extract_dlls(target))
    new_dlls -= dlls
    dlls.update(new_dlls)
    to_check = list(new_dlls)

# CMake wants only forward slashes
dlls = ['"{}"'.format(d.replace('\\', '/')) for d in dlls]

with open(args.output, "wt") as f:
    print("file(INSTALL {0} DESTINATION ${{CMAKE_INSTALL_PREFIX}}/{1})".format(" ".join(dlls), args.destination), file=f)
