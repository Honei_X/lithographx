set(LithoGraphX_VERSION_STRING @LGX_VERSION@)
set(LithoGraphX_LIBRARIES LithoGraphX)
set(LithoGraphX_HAS_ITK @BUILD_ITK@)
set(LithoGraphX_HAS_VTK @BUILD_VTK@)

foreach(module ${LithoGraphX_FIND_COMPONENTS})
  if(NOT LithoGraphX_HAS_${module})
    message(FATAL_ERROR "This version of LithoGraphX doesn't have any component called ${module}")
  endif()
  set(LithoGraphX_REQUESTED_${module} TRUE)
  #message(STATUS "set LithoGraphX_REQUESTED_${module} to TRUE")
endforeach()

get_filename_component(_LOCAL_PATH "${CMAKE_CURRENT_LIST_FILE}" PATH) # PREFIX/lib/cmake/LithoGraphX

get_filename_component(_IMPORT_PREFIX "${_LOCAL_PATH}" PATH) # PREFIX/lib/cmake
get_filename_component(_IMPORT_PREFIX "${_IMPORT_PREFIX}" PATH) # PREFIX/lib
get_filename_component(_IMPORT_PREFIX "${_IMPORT_PREFIX}" PATH) # PREFIX

list(APPEND LithoGraphX_SEARCH_PATHS
  ${_IMPORT_PREFIX}
  @CMAKE_INSTALL_PREFIX@
  "C:/Program Files/LithoGraphX"
  "C:/Program Files (x86)/LithoGraphX")

find_program(LithoGraphX_PROGRAM LithoGraphX PATHS ${LithoGraphX_SEARCH_PATHS} PATH_SUFFIXES bin)

execute_process(COMMAND "${LithoGraphX_PROGRAM}" --user-process OUTPUT_VARIABLE LithoGraphX_USER_PROCESS_DIR OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND "${LithoGraphX_PROGRAM}" --process OUTPUT_VARIABLE LithoGraphX_PROCESS_DIR OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND "${LithoGraphX_PROGRAM}" --user-macros OUTPUT_VARIABLE LithoGraphX_USER_MACRO_DIR OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND "${LithoGraphX_PROGRAM}" --macros OUTPUT_VARIABLE LithoGraphX_MACRO_DIR OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND "${LithoGraphX_PROGRAM}" --include OUTPUT_VARIABLE LithoGraphX_INCLUDE_DIR OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND "${LithoGraphX_PROGRAM}" --libs OUTPUT_VARIABLE LithoGraphX_LIB_DIR OUTPUT_STRIP_TRAILING_WHITESPACE)

get_filename_component(LithoGraphX_BIN_DIR ${LithoGraphX_PROGRAM} PATH)

list(APPEND CMAKE_MODULE_PATH
  ${_LOCAL_PATH}
  ${LithoGraphX_LIB_DIR}/cmake)

message(STATUS "CMAKE_MODULE_PATH = ${CMAKE_MODULE_PATH}")

list(APPEND CMAKE_INCLUDE_PATH ${LithoGraphX_INCLUDE_DIR})
list(APPEND CMAKE_INCLUDE_PATH ${LithoGraphX_PROCESS_DIR}/include)
list(APPEND CMAKE_LIBRARY_PATH ${LithoGraphX_LIB_DIR})
list(APPEND CMAKE_LIBRARY_PATH ${LithoGraphX_PROCESS_DIR}/lib)
list(APPEND CMAKE_LIBRARY_PATH ${LithoGraphX_BIN_DIR})

find_package(Qt5 COMPONENTS Core Widgets Xml OpenGL PrintSupport)
find_package(OpenMP)
find_package(Eigen REQUIRED)
find_package(Thrust REQUIRED)

if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(STATUS "Setting build type to '@CMAKE_BUILD_TYPE@' as none was specified.")
  set(CMAKE_BUILD_TYPE @CMAKE_BUILD_TYPE@ CACHE STRING "Choose the type of build: Debug Release MinSizeRel or RelWithDebInfo." FORCE)
  # Set the possible values of build type for cmake-gui
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release"
    "MinSizeRel" "RelWithDebInfo")
endif()

option(LithoGraphX_USER_INSTALL "If checked, the plugin will be installed in the user folder instead of the system one" OFF)

if(NOT TARGET LGXViewer)
  add_library(LGXViewer SHARED IMPORTED)
  set(LithoGraphX_LGXViewer_DIRECTORIES ${LithoGraphX_INCLUDE_DIR})
  set_property(TARGET LGXViewer PROPERTY
    INTERFACE_INCLUDE_DIRECTORIES ${LithoGraphX_LGXViewer_DIRECTORIES})

  $<$<NOT:$<BOOL:@WIN32@>>:
  set_property(TARGET LGXViewer PROPERTY
    IMPORTED_SONAME ${LithoGraphX_LIB_DIR}/$<TARGET_SONAME_FILE_NAME:LGXViewer>)>
  $<$<BOOL:@WIN32@>:
  set_property(TARGET LGXViewer PROPERTY
    IMPORTED_IMPLIB ${LithoGraphX_LIB_DIR}/$<TARGET_LINKER_FILE_NAME:LGXViewer>)>

  set_property(TARGET LGXViewer PROPERTY
    IMPORTED_LOCATION ${LithoGraphX_LIB_DIR}/$<TARGET_FILE_NAME:LGXViewer>)
  set_property(TARGET LGXViewer PROPERTY
    INTERFACE_LINK_LIBRARIES Qt5::Core Qt5::Widgets Qt5::OpenGL Qt5::Xml Qt5::PrintSupport)
endif()

set(LithoGraphX_SHARED_DIR $<$<BOOL:@WIN32@>:${LithoGraphX_BIN_DIR}>$<$<NOT:$<BOOL:@WIN32@>>:${LithoGraphX_LIB_DIR}>)

if(NOT TARGET LithoGraphX)
  $<$<AND:$<BOOL:@THRUST_BACKEND_CUDA@>,$<BOOL:@MINGW@>>:
  if(NOT TARGET lgx_cuda)
    add_library(lgx_cuda SHARED IMPORTED)
    set_property(TARGET lgx_cuda PROPERTY
      IMPORTED_IMPLIB ${LithoGraphX_LIB_DIR}/lgx_cuda.lib)
    set_property(TARGET lgx_cuda PROPERTY
      IMPORTED_LOCATION ${LithoGraphX_SHARED_DIR}/lgx_cuda.dll)
    set_property(TARGET lgx_cuda PROPERTY INTERFACE_POSITION_INDEPENDENT_CODE ON)
  endif()
  >
  add_library(LithoGraphX SHARED IMPORTED)
  set(LithoGraphX_USE_OPENMP @USE_OPENMP@)
  set(LithoGraphX_INCLUDE_DIRECTORIES
    ${LithoGraphX_INCLUDE_DIR})
  if(EXISTS "${LithoGraphX_PROCESS_DIR}/include")
    list(APPEND LithoGraphX_INCLUDE_DIRECTORIES ${LithoGraphX_PROCESS_DIR}/include)
  endif()
  if(EXISTS "${LithoGraphX_USER_PROCESS_DIR}/include")
    list(APPEND LithoGraphX_INCLUDE_DIRECTORIES "${LithoGraphX_USER_PROCESS_DIR}/include")
  endif()
  set(lgx_libraries Qt5::Core Qt5::Widgets Qt5::Gui Qt5::OpenGL Qt5::Xml Qt5::PrintSupport)
  set(LithoGraphX_SYSTEM_INCLUDE_DIRECTORIES
    ${Eigen_INCLUDE_DIR}
    $<$<BOOL:@THRUST_BACKEND_CUDA@>:${CUDA_INCLUDE_DIRS}>
    ${THRUST_INCLUDE_DIR})
  $<$<BOOL:@THRUST_BACKEND_CUDA@>:list(APPEND lgx_libraries
    ${CUDA_LIBRARIES} $<$<BOOL:@MINGW@>:${LithoGraphX_LIB_DIR}/lgx_cuda.lib>)>
  set(LithoGraphX_COMPILE_OPTIONS -std=c++11)
  if(LithoGraphX_USE_OPENMP)
    list(APPEND LithoGraphX_COMPILE_OPTIONS ${OpenMP_CXX_FLAGS})
  endif()
  set_property(TARGET LithoGraphX PROPERTY
    INTERFACE_INCLUDE_DIRECTORIES ${LithoGraphX_INCLUDE_DIRECTORIES} ${LithoGraphX_SYSTEM_INCLUDE_DIRECTORIES})
  set_property(TARGET LithoGraphX PROPERTY
    INTERFACE_SYSTEM_INCLUDE_DIRECTORIES ${LithoGraphX_SYSTEM_INCLUDE_DIRECTORIES})
  set_property(TARGET LithoGraphX PROPERTY
    INTERFACE_COMPILE_OPTIONS ${LithoGraphX_COMPILE_OPTIONS})

  $<$<BOOL:@WIN32@>:
  set_property(TARGET LithoGraphX PROPERTY
    IMPORTED_IMPLIB ${LithoGraphX_LIB_DIR}/$<TARGET_LINKER_FILE_NAME:LithoGraphX>)>
  $<$<NOT:$<BOOL:@WIN32@>>:
  set_property(TARGET LithoGraphX PROPERTY
    IMPORTED_SONAME ${LithoGraphX_LIB_DIR}/$<TARGET_SONAME_FILE_NAME:LithoGraphX>)>

  set_property(TARGET LithoGraphX PROPERTY
    IMPORTED_LOCATION ${LithoGraphX_SHARED_DIR}/$<TARGET_FILE_NAME:LithoGraphX>)
  set_property(TARGET LithoGraphX PROPERTY
    INTERFACE_LINK_LIBRARIES ${lgx_libraries} LGXViewer)
  set_property(TARGET LithoGraphX PROPERTY INTERFACE_POSITION_INDEPENDENT_CODE ON)
endif()

$<$<BOOL:@BUILD_ITK@>:
if(LithoGraphX_REQUESTED_ITK)
  find_package(ITK 4.0 REQUIRED)
  set(LithoGraphX_ITK_INCLUDE_DIRS
    ${LithoGraphX_INCLUDE_DIR}/ITK
    ${ITK_INCLUDE_DIRS})
  if(NOT TARGET LithoGraphX::ITK)
    add_library(LithoGraphX::ITK SHARED IMPORTED)

    $<$<NOT:$<BOOL:@WIN32@>>:
    set_property(TARGET LithoGraphX::ITK PROPERTY
      IMPORTED_SONAME ${LithoGraphX_LIB_DIR}/$<TARGET_SONAME_FILE_NAME:lgxITKutil>)>
    $<$<BOOL:@WIN32@>:
    set_property(TARGET LithoGraphX::ITK PROPERTY
      IMPORTED_IMPLIB ${LithoGraphX_LIB_DIR}/$<TARGET_LINKER_FILE_NAME:lgxITKutil>)>

    set_property(TARGET LithoGraphX::ITK PROPERTY
      IMPORTED_LOCATION ${LithoGraphX_SHARED_DIR}/$<TARGET_FILE_NAME:lgxITKutil>)
    set_property(TARGET LithoGraphX::ITK PROPERTY
      INTERFACE_INCLUDE_DIRECTORIES ${LithoGraphX_ITK_INCLUDE_DIRS})
    set_property(TARGET LithoGraphX::ITK PROPERTY
      INTERFACE_LINK_LIBRARIES $<TARGET_PROPERTY:lgxITKutil,LINK_LIBRARIES>)
  endif()
endif()
>

$<$<BOOL:@BUILD_VTK@>:
if(LithoGraphX_REQUESTED_VTK)
  find_package(VTK REQUIRED)
  set(LithoGraphX_VTK_INCLUDE_DIRS
    ${LithoGraphX_INCLUDE_DIR}/VTK
    ${VTK_INCLUDE_DIRS})
  if(NOT TARGET LithoGraphX::VTK)
    add_library(LithoGraphX::VTK SHARED IMPORTED)

    $<$<NOT:$<BOOL:@WIN32@>>:
    set_property(TARGET LithoGraphX::VTK PROPERTY
      IMPORTED_SONAME ${LithoGraphX_LIB_DIR}/$<TARGET_SONAME_FILE_NAME:lgxVTKutil>)>
    $<$<BOOL:@WIN32@>:
    set_property(TARGET LithoGraphX::VTK PROPERTY
      IMPORTED_IMPLIB ${LithoGraphX_LIB_DIR}/$<TARGET_LINKER_FILE_NAME:lgxVTKutil>)>

    set_property(TARGET LithoGraphX::VTK PROPERTY
      IMPORTED_LOCATION ${LithoGraphX_SHARED_DIR}/$<TARGET_FILE_NAME:lgxVTKutil>)
    set_property(TARGET LithoGraphX::VTK PROPERTY
      INTERFACE_INCLUDE_DIRECTORIES ${LithoGraphX_VTK_INCLUDE_DIRS})
    set_property(TARGET LithoGraphX::VTK PROPERTY
      INTERFACE_LINK_LIBRARIES $<TARGET_PROPERTY:lgxVTKutil,LINK_LIBRARIES>)
  endif()
endif()
>

macro(ADD_LGX_PLUGIN plugin_name)
  set(current_var ${plugin_name}_sources)
  foreach(value ${ARGN})
    if("${value}" STREQUAL "PUBLIC_HEADER")
      set(current_var ${plugin_name}_headers)
    else()
      list(APPEND ${current_var} ${value})
    endif()
  endforeach()
  add_library(${plugin_name} MODULE ${${plugin_name}_sources} ${${plugin_name}_headers})
  target_link_libraries(${plugin_name} LithoGraphX)
  set_property(TARGET ${plugin_name} PROPERTY PUBLIC_HEADER ${${plugin_name}_headers})
  set_property(TARGET ${plugin_name} PROPERTY PREFIX "")
  if(LithoGraphX_USE_OPENMP)
    set_property(TARGET ${plugin_name} APPEND PROPERTY COMPILE_FLAGS ${OpenMP_CXX_FLAGS})
    set_property(TARGET ${plugin_name} APPEND PROPERTY LINK_FLAGS ${OpenMP_CXX_FLAGS})
  endif()
  if(LithoGraphX_USER_INSTALL)
    install(TARGETS ${plugin_name}
      DESTINATION ${LithoGraphX_USER_PROCESS_DIR}
      PUBLIC_HEADER DESTINATION ${LithoGraphX_USER_PROCESS_DIR}/include/${plugin_name})
  else()
    install(TARGETS ${plugin_name}
      DESTINATION ${LithoGraphX_PROCESS_DIR}
      PUBLIC_HEADER DESTINATION ${LithoGraphX_PROCESS_DIR}/include/${plugin_name})
  endif()
endmacro(ADD_LGX_PLUGIN)

macro(ADD_LGX_MACRO)
  if(LithoGraphX_USER_INSTALL)
    install(FILES ${ARGN} DESTINATION ${LithoGraphX_USER_MACRO_DIR})
  else()
    install(FILES ${ARGN} DESTINATION ${LithoGraphX_MACRO_DIR})
  endif()
endmacro(ADD_LGX_MACRO)
