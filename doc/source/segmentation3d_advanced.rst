*********************************
Segmenting Cells in 3D (advanced)
*********************************

For this first tutorial, we are going to look step by step into the 3D
segmentation of the cells of the embryo of Arabidopsis.

First, download the dataset from BitBucket here: `dataset`_.

.. _dataset: https://bitbucket.org/PierreBdR/lithographx/downloads/EarlyEmbryo.zip

.. |colormap-button| image:: ../../src/images/Palette.png
    :width: 24pt
    :alt: Transfer-function icon

Loading the dataset
===================

1. Launch LithoGraphX
2. Drag & drop the file ``160112_4.tif`` into the interaction area of
   LithoGraphX
3. Adjust the opacity as we have seen in the :ref:`previous tutorial
   <load-see-tutorial>`.

You should see the embryo like in Figure
:num:`segmentation3d-advanced-fig-loaded`.

.. _segmentation3d-advanced-fig-loaded:

.. figure:: images/segmentation3d-loading.png
    :alt: Loaded stack with embryo
    :align: center
    :figwidth: 50%
    :width: 100%

    Loaded stack with the embryo

Extracting the embryo
=====================

As you can see, in addition to the embryo, we have a lot of extra tissues. There
are a number of tools we can use to remove most of it. We will first use the
clipping planes and then the pixel editing tool.

Trimming with clipping planes
-----------------------------

.. _embryo-place-planes:

.. figure:: images/segmentation3d-clip1.png
    :alt: Placing the clipping plane
    :align: center
    :figwidth: 50%
    :width: 100%

    Place the clipping planes to include just the embryo itself.

1. Go to the :ref:`view-tab`
2. Show the grid of the first clipping plane.
3. Make sure ``Clip 1`` is selected in the :ref:`interaction-box`.
4. Ajusting the thickness of the slice, and moving the clipping plane with the
   control-key pressed, place the clipping planes to include the embryo (see
   Figure :num:`embryo-place-planes`)
5. Enable the clipping planes
6. Use the process ``Stack.Clip Stack`` in the ``Mesh Interaction`` folder.
7. Adjust the transfer function of the work stack: click on the
   |colormap-button| button of the work store and click on the auto-adjust
   button. At last, adjust the opacity.
8. Disable the ``Clip 1`` from the :ref:`view-tab`.

Using the Pixel Editing tool
----------------------------

.. _embryo-clean-stack:

.. figure:: images/segmentation3d-clean.png
    :alt: Cleaned stack
    :align: center
    :figwidth: 50%
    :width: 100%

    Cleaned stack

1. Select the :ref:`pixel editing tool <pixel-edit-tool>`
2. Ensure the active stack is the ``Work Stack 1``
3. In the :ref:`view-tab`, make sure the ``Fill`` check-box in the
   :ref:`view-tab-stack-edit` box is not checked.
4. Remove pixels by pressing the Alt key and clicking. All visible pixels within
   the drawn circle will be erased! After a few minutes of erasing, you should
   get something like Figure :num:`embryo-clean-stack`.
5. When you are done, copy the result to the main store with the ``Stack.Copy
   Main to Work Stack`` process in the ``Multi-stack`` folder.

Troubleshooting
^^^^^^^^^^^^^^^

* When using the pixel edit, depending on the view angle (e.g. the number of
  voxels that would need editing), the edition may be done only when you
  release the mouse button.

* If you make a mistake, there is no undo! Unless you copy the stack back
  to the main store, where you will be able to access it again. For this,
  use the process ``Stack.Copy Work to Main Stack`` in the ``Multi-stack``
  folder.

* Don't panick if you find it hard to move around. This is normal at the
  beginning, but you will get used to it. A commonly forgotten useful shortcut
  allows to turn the object in the plane of view with Left+Middle mouse click
  (look at the Mouse and Keyboard help).

Segmentation
============

Now we have a nice, isolated image. We are ready for the segmentation part.

1. To make it easier and more reproducable, we need to scale the values to use
   the whole range. Run the process ``Stack.Autoscale Stack`` from the
   ``Filters`` folder. If you changed the transfer function, you might need to
   re-adjust it.

2. The image is too noisy to be segmented as is. Run the process
   ``Stack.Gaussian Blur Stack`` in the ``Filters`` folder. For this sample, the
   parameters ``X Sigma``, ``Y Sigma`` and ``Z Sigma`` should be set to
   :math:`0.3 \mu m`

3. We will then use the ``Stack.ITK Watershed Auto Seeded`` from the
   ``ITK/Segmentation`` folder. The two first parameters should almost always be
   set to ``No``. The ``Level`` parameter is the most important one: it tells
   the algorithm the level of background noise to consider. Remember the values
   range from 0 to 65535, so 1500 is quite small. But if you didn't autoscale
   the stack before, it may become very large!

4. Use the ``Fill picked label`` tool |fill_label_img| to erase the outside
   cell: make sure no label is selected, and Alt-click on the outside cell to
   delete it. There might be some other small cells to erase.

.. |fill_label_img| image:: ../../src/images/FillLabel.png
    :width: 24pt

At that stage, the segmentation is mostly good, but some cells are too large. We
could of course edit by hand the initial stack to improve, but we are going to
use another means, more in line with what LithoGraphX is good with: interaction
between mesh and stack.
