**********************
Segmenting Cells in 3D
**********************

.. figure:: images/segmentation-3d-cover.*
    :alt: Shoot apical meristem of A. thaliana, segmented in 3D
    :align: center
    :figwidth: 85%
    :width: 100%

    Shoot apical meristem of *A. thaliana*, segmented in 3D

.. _dataset: http://www.botany.unibe.ch/deve/lithographx/Meristem3D.zip

In this section, we are going to see how we can extract the 3D shape of cells in
images of the shoot apical meristem of *Arabidopsis thaliana*, and see how we
can use an extra channel with the segmentation. To start, download this
`dataset`_, provided by Dr. Agata Burian.

Loading the data
================

For this tutorial, you can start either by loading the file
``Arabido_DR5-YFP_pi.tif`` or ``Arabido_DR5-YFP_pi_cleaned.tif``.

Cleaning the data
=================

If you have loaded ``Arabido_DR5-YFP_pi.tif``, you will see in addition to the
shoot apical meristem a few lateral organs, including those partially removed to
acquire the image. To extract only what we want to see, we need to clean the
image.

#. Copy the stack to the work store. For that, we will use the ``Stack.Autoscale
   Stack`` process: in addition to copying the stack, it will ensure we use the whole
   dynamic range of our images. This is an important step as it makes to rest of
   the process much easier.
#. Select the :ref:`pixel edit tool <pixel-edit-tool>`. In the :ref:`view-tab`
   make sure ``Fill`` checkbox in the :ref:`view-tab-stack-edit` area is not
   checked. Then, pressing the Alt key, you should see a circle above the
   drawing area. This circle marks the cylinder that will get erased. Click to
   erase.
#. When you are happy with your cleaning, save the stack!

.. note::

    If the area to erase is too large, nothing will change until you
    release the mouse button.

.. hint::

    You can also do the cleaning on the segmented stack. In this case, another
    useful tool is the :ref:`fill volume tool <fill-volume-tool>`. Ensure no
    label is currently selected by clicking on the :ref:`current label
    <current-label-button>` button. Then click on the labels you want to erase.

3D Cells Segmentation
=====================

#. Make sure the stack is in the Main store.
#. Blur the stack using the ``Gaussian Blur Stack`` process in the
   ``Filters`` process with a radius of *0.3 µm*.
#. Segment using the ``ITK Watershed Auto Seeded`` process in the
   ``ITK/Segmentation`` folder.
#. Remove external cells with the ``Erase at Border`` process in the
   ``Segmentation`` folder.
#. Extract the cell shapes with the Mesh process ``Marching Cubes 3D``
   in the ``Creation`` folder, using a cube size of *1 µm*.

.. hint::

    Use the clipping planes to scan through the volume and make sure the
    segmentation worked correctly in the depth of the tissue.

3D Signal Quantification
========================

.. figure:: images/segmentation-3d-quant.*
    :alt: Signal quantification on 3D mesh
    :align: center
    :figwidth: 75%
    :width: 100%

    Signal quantification on 3D mesh

#. Load the YFP channel in the work store of the stack 1. To do that, the
   simplest is to drag&drop the file onto the :ref:`work store area <main-tab>`.
#. Run the ``Heat map`` mesh process from the ``Heat map`` folder.
#. In the dialogue box, select the ``Volume`` heat map type and the
   ``Total signal`` visualization and make sure ``Signal average`` is not ticked.

The result is, per cell, the total amount of signal present in the YFP channel.

.. note::

    This is **not** sufficient for a proper signal quantification. This is only
    qualitative. For a proper quantification of the signal you need a reference
    channel.
