**********************
Segmenting cells in 2D
**********************

.. figure:: images/segmentation-2d-cover.*
    :alt: Shoot apical meristem of A. thaliana, with microtubules
    :align: center
    :figwidth: 85%
    :width: 100%

    Shoot apical meristem of A. thaliana, with microtubules

.. _dataset: http://www.botany.unibe.ch/deve/lithographx/Microtubules.zip

In this section, we are going to see how we can extract the 2D shape of cells in
images of the shoot apical meristem of *Arabidopsis thaliana*, and see how we
can use a marker of cortical microtubule to compute their orientation based on
the method published in [Boudaoud2014]_. To start, download this `dataset`_,
provided by Dr. Agata Burian.

To segment the cells in 2D, we need to proceed in two phases:

#. Extract the surface of the meristem
#. Segment the cells on that surface

Extracting the surface of the meristem
======================================

See figure :num:`segmentation2d-fig-seg` A-D.

#. Filter the stack using a Gaussian blur of *0.5 µm* on each dimension.
#. Extract the surface with the ``Edge Detect`` filter in the Morphology
   folder, with the default arguments.
#. Erase the bumps due to dead cells by hand, using the ``Pixel Edit`` tool
#. Extract a coarse surface using the Mesh Process ``Marching Cubes Surface``
   in the ``Creation`` folder. The cube size should be similar
   to the cell size. For this image, use a cube size of *3 µm*.
#. Select the bottom and sides with the selection tool: make sure the
   ``Mesh`` check-box is ticked and ``View`` is on ``Selected``. Then,
   orienting properly the meristem, press the ``Alt`` key and click and
   drag a rectangle to select the vertices to remove. You can extend the
   selection by pressing the ``Shift`` key while clicking. For a demonstration
   of what needs to be done, look at this video.
#. Smooth the surface with the Mesh process ``Smooth Mesh`` in the
   ``Structure`` folder. Initially, smooth three times. Then, alternate
   ``Subdivide`` and 3 passes of smoothing until you reach about 500'000
   vertices. Check the terminal for the number of vertices after
   subdivision.

.. youtube:: http://www.youtube.com/watch?v=ZbTaYITkObg
    :width: 512
    :height: 395

.. _segmentation2d-fig-seg:

.. figure:: images/segmentation-2d-segmentation.*
    :alt: 2D segmentation of the meristem
    :align: center
    :figwidth: 85%
    :width: 100%

    2D Segmentation of the meristem

Segment the cells
=================

See figure :num:`segmentation2d-fig-seg` E-F.

#. First, project the signal onto the surface with the Mesh process
   ``Project Signal`` in the ``Signal`` folder. Only change the minimum
   and maximum distances, and set them to *1 µm* and *4 µm*
   respectively. You can play with these values to see what happens when
   they change.
#. Now, we are going to segment the cells visible in the signal. For
   this use the Mesh process ``Auto-Segmentation`` in the ``Segmentation``
   folder. For the parameters, set ``Normalize`` to ``No``, ``Blur Cell
   Radius`` to *1 µm*, ``Auto-Seed Radius`` to *2 µm*, ``Border
   Distance`` to *1 µm* and ``Combine Threshold`` to *1*.
#. After a while, you will see outline of segmented cells. In the Main
   tab, select ``Labels`` as representation for the surface. And using the
   2D bucket, erase the cells above the areas where the staining didn't
   work.

Micro-tubule orientations
=========================

.. figure:: images/segmentation-2d-orientation.*
    :alt: Extracting the orientation of the microtubules
    :align: center
    :figwidth: 85%
    :width: 100%

    Extraction of the microtubules orientation.

#. Load the MBD stack: just drag and drop the file on the MorphoGraphX window.
#. Project the new signal onto the surface. Select a range from *-1 µm* to *1
   µm*. You can try different values and see which provide the best contrast.
#. Run the Mesh process ``Compute Fibril Orientations`` in the ``Cell
   Axis/Fibril Orientation`` folder. The length of the lines represent how
   consistent the microtubules' orientations are.
#. To adjust the way the orientation is displayed, run the ``Display
   Fibril Orientation`` process in the same folder.

.. [Boudaoud2014] Boudaoud, A.; Burian, A.; Borowska-Wykręt, D.; Uyttewaal, M.;
    Wrzalik, R.; Kwiatkowska, D. and Hamant, O. **FibrilTool, an ImageJ plug-in to
    quantify fibrillar structures in raw microscopy images**. *Nature Protocols*,
    2014, 9, 457-463
