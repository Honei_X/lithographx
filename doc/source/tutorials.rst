#########
Tutorials
#########

You will find here a series of tutorial to accomplish various tasks. Each
tutorial is given with a sample dataset and a LithoGraphX task to get you
started.

The datasets used in these tutorials can be found there:
http://www.lithographx.org/data-gallery

.. toctree::
    :maxdepth: 2

    load_see
    load_see_2
    segmentation3d
    segmentation3d_advanced
    segmentation2d
    growth
