**********************
Data Visualization (2)
**********************

.. figure:: images/load-see-2-cover.*
    :alt: Shoot apical meristem of A. thaliana
    :align: center
    :width: 75%

    Shoot apical meristem of `A. thaliana`, cell walls stained with PI and with
    DR5::YFP fluorescent marker. Image: Agata Burian

Introduction
============

The purpose of this exercise is to introduce the interface of LithoGraphX,
learn to segment cells in 3D and work with the heat map.

Note that you will find a lot of useful information in the ``Help`` menu,
including a user manual, a documentation of all the existing processes, and the
mouse and keyboard interactions.

LithoGraphX has a fixed number of objects that exist at any given time. They
are organized in two ``Stacks``, each stack containing two images and a mesh.
The stack defines the size, resolution and position of the images it contains.
The two images are called the ``Main`` and ``Work`` store: the main store is
where the data is loaded by default, while the work store is where the result
of any processing will be put. Only one store can be **active** at any given
time. Which is active depends on the visibility of the stores and which stack
tab is currently active. It is however, indicated on the right of the status
bar, at the bottom of the main window.

.. hint::

    In the beginning, always check which store is active before launching
    processes.

Each stack also contains a triangular mesh. To each vertex of the mesh is
attached a signal and a label. The label however, is interpreter by triangle:
if at least two vertices of a triangle have the same label, the whole
triangle is given this label. In case of tie, the triangle is considered
un-labelled. At last, to each label a heat may be associated. A mesh can be seen
either as a continuous surface, displaying the signal, label or label's heat,
or as a wireframe mesh, or both at the same time.

In LithoGraphX, loading, saving, processing, analysing is  done through
**Processes** (in fact, most menu items are simply shortcuts to processes).
There are three kind of processes: Stack, Mesh and Global. Stack processes can
only modify the stacks and stores, the Mesh processes can only modify the
meshes and Global processes can modify anything.

Loading a stack
===============

Once LithoGraphX is started, the simplest way to open a new image stack is to
drag&drop the file (tif) onto the drawing area. You can also use the
``Stack`` menu to open a file to a particular store.


Loading a second channel
========================

To load a second channel, you can either use the menus, or again drag&drop a
file. However, instead of dropping onto the drawing area, you can drop the file
onto the ``Work`` store area (see :ref:`main-tab`).

Clipping planes
===============

.. figure:: images/load-see-2-clipping.*
    :alt: Clipping planes
    :align: center
    :width: 75%

You should now have a nice image the SAM, But you cannot easily see inside! To
help with this, MorphoGraphX has few tools. The first one is three pairs of
clipping planes (see Figure~\ref{fig:clipping}). To work with the clipping
planes, they must be enabled in the ``View`` tab. Each pair of clipping plane
has its own tab, from which the user can enable it, show a grid to see its
position when not enabled, and set the distance between the two planes. To move
the planes, it must be selected in the main tab in the ``Control-Key
Interaction`` box. As the name suggest, the clipping planes are then moved by
pressing the ``Control`` key and use the mouse in the same way as for moving
the camera.

Try using the clipping planes to see where the DR5::YFP signal is with respect
to the cell layers.

Cutting Surfaces
================

.. figure:: images/load-see-2-cutting-surfaces.*
    :alt: Cutting surfaces
    :align: center
    :width: 85%

Another way to see the insides of a volume is to use cutting surfaces. Like the
clipping planes, the cutting surface need to be enabled in the ``View`` tab, at
the bottom. The cutting surface can take one of three forms: a single plane,
three orthogonal planes, or a Bézier surface. When enabled, the part of the
visible volumes that intersect the chosen surface will be drawn, ignoring their
brightness and opacity settings. Instead, the brightness and opacity of the
surface is used\footnote{To see a volume only on the cutting surface, push its
opacity slider all the way to the left, making it completely transparent.}.

As for the clipping plane, you can move the cutting surface is it is selected
in the ``Control-key interaction`` box.

At last, for the Bézier surface, if you show the grid and make sure points are
shown, you can select and move them using the mesh selection tool.

