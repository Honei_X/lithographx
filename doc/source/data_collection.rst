************************
Tips for Data Collection
************************

Data collection for use in 3D image processing softwares can be a little
different than for other uses. In most cases, people optimize data collection so
that individual slices look good to the human eye. This is not always the best
for 3D data analysis and visualization. The following are some tips to help get
the most out of the images:

* **16 bits images**. If possible collect 16 bits per channel, instead of
  8 bits. Although the pictures may look no different, the 16 bits images will
  have higher dynamic range, and it will be easier to extract features in darker
  areas of the image. If the microscope only supports 12 bits, that will still
  be better than 8 bits.

* **More slices, less averaging**. A common technique used to improve the image
  quality on confocal microscope is frame or line averaging. In this mode, the
  microscope takes each line/frame many times (typicalle 2 or 4 times), and
  creates an image by averaging each pass. Although this improves the image
  quality, it also increase the acquisition time, leading to problems such as:
  bleaching, tissue damage, movement during acquisition, ... and to avoid these,
  a common technique is to reduce the number of slices taken. When acquiring
  data for 3D image processing software such as LithoGraphX, it is recommended
  to favour increasing the number of slices rather than averaging. The noise
  will be reduced later using neighboring slices.

* **Dynamic range**. When acquiring images, it is best to maximize their dynamic
  range: that is the difference between the brightness and the darkest pixel. To
  help you optimize this, most microscope's software have a mode in which the
  saturated and black pixels are marked. When in that mode, first set the offset
  down until you see a lot of black pixels, and then increase it slowly to
  reduce their number until only a few are left. Then, increase the gain until
  many pixels are saturated, and decrease it slowly until only a few pixes are
  saturated. If you are interested in the geometry of cells, it is a good idea
  to saturate the cell walls slightly more: that will avoid issues with drops in
  signal and the exact shape will still be found at the middle of the saturated
  areas.

* **Start and end slices shouldn't contain any sign of your sample**. It is
  tempting to start the acquisition when the sample is just visible, and stop
  just before it disappear. This is a bad idea: the reconstruction algorithms
  need the extra information that "there is no more tissue here" (or at least,
  not of interest) to reconstruct their geometry properly. So make sure you
  start a few micron before your sample, and stop a few micros after.

* **Use Fiji to get TIFF files**. Most microscope formats are proprietary. The
  Loci Bioformat group has reverse engineered the formats of the most popular
  microscopes and made plugins for ImageJ. We recommend Fiji, which is
  a distribution of ImageJ that contains these, along with many other useful
  plugins. Even if your microscope generates TIFF images, you might want to open
  them with the Bioformat plugins to extract the thickness of the slices.
  LithoGraphX only recognise ImageJ generated TIFF and OME TIFF for this.

* **If voxel sizes are wrong**. The TIFF format has no standard way to specify
  the voxel size in Z. ImageJ defines its own attributes for this, and the OME
  tif format another one. LithoGraphX recognises both formats. But some
  manufacturers provide their own TIFF file, with a different specification of
  the slice thickness. In this case, LithoGraphX will be unable to read it. You
  will need to use the Stack process ``Change Voxel Size`` in the ``Canvas``
  folder and specify the ``z`` resolution.

