#-*- coding:utf-8 -*-
u'''
embedding youtube video to sphinx, allow specifications of width and height. In LaTeX documents, simply provide a link.

usage:

.. code-block:: python

   extensions = ['sphinxcontrib.youtube']


then use `youtube` directive.

.. code-block:: rst

   .. youtube:: http://www.youtube.com/watch?v=Ql9sn3aLLlI


finally, build your sphinx project.

.. code-block:: sh

   $ make html

This module is based on sphinxcontrib.youtube version 0.1.2 from @shomah4a

'''

__version__ = '0.1.0'
__author__ = 'Pierre Barbier de Reuille <pierre@barbierdereuille.net>'
__license__ = 'LGPLv3'


def setup(app):

    from . import youtube

    app.add_config_value('youtube_link', False, True)
    app.add_node(youtube.youtube,
                 html=(youtube.visit, youtube.depart),
                 latex=(youtube.link, youtube.depart),
                 text=(youtube.text, youtube.depart))
    app.add_directive('youtube', youtube.YoutubeDirective)

    app.connect('builder-inited', youtube.init)

    return {'version': __version__}

