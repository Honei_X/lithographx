#-*- coding:utf-8 -*-
from __future__ import print_function

import urlparse

from docutils import nodes
from docutils.parsers import rst
from sphinx.locale import _

youtube_link = False

class youtube(nodes.General, nodes.Element):
    pass

def init(app):
    global youtube_link
    youtube_link = 'html' in app.builder.name or app.config.youtube_link

def get_video_id(url):
    return urlparse.parse_qs(urlparse.urlparse(url).query)['v'][0]

def visit(self, node):
    if youtube_link:
        tag = u'<p>Youtube video: <a href="{0}">{0}</a></p>'.format(node['url'])
        self.body.append(tag)
    else:
        video_id = get_video_id(node['url'])
        url = u'//www.youtube.com/embed/{0}'.format(video_id)
        tag = u'''<iframe width="{1}" height="{2}" src="{0}" frameborder="0" allowfullscreen>&nbsp;</iframe>'''
        tag = tag.format(url, node['width'], node['height'])
        self.body.append(tag)

def link(self, node):
    tag = u'\n\nYoutube video: \\url{{{0}}}\n\n'.format(node['url'])
    self.body.append(tag)

def text(self, node):
    tag = u'\nYoutube video: {0}\n'.format(node['url'])
    self.body.append(tag)

def depart(self, node):
    pass

class YoutubeDirective(rst.Directive):
    name = 'youtube'
    node_class = youtube

    has_content = False
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = False
    option_spec = {'width': int, 'height': int}

    def run(self):
        width = self.options.get('width', 640)
        height = self.options.get('height', 480)
        url = self.arguments[0]
        node = self.node_class()
        node['url'] = url
        node['width'] = width
        node['height'] = height
        return [node]

