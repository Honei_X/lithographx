/*
 * This file is part of MorphoGraphX
 *
 * MorphoGraphX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MorphoGraphX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MorphoGraphX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef %ProjectName%_HPP
#define %ProjectName%_HPP

#include <Process.hpp>

namespace mgx {
namespace process {
class %ClassName% : public GlobalProcess
{
public:
    %ClassName%(const GlobalProcess& process)
        : Process(process)
          , GlobalProcess(process)
    { }

    bool operator()(const QStringList& parms)
    {
        return operator()();
    }

    bool operator()();

    QString folder() const { return "%PluginFolder%"; }
    QString name() const { return "%PluginName%"; }
    QString description() const { return "%PluginDescription%"; }
    QStringList parmNames() const { return QStringList(); }
    QStringList parmDescs() const { return QStringList(); }
    QStringList parmDefaults() const { return QStringList(); }
    ParmChoiceMap parmChoice() const {
        ParmChoiceMap map;
        return map;
    }
    QIcon icon() const
    {
        return QIcon(":/images/%ProjectName%.png");
    }

};
}
}

#endif // %ProjectName%_HPP

